services:
    prometheus:
        image: prom/prometheus:v3.1.0
        container_name: prometheus_dev
        restart: unless-stopped
        networks:
            - dev
        volumes:
            -   type: bind
                source: ./proxy/prometheus-dev.yaml
                target: /etc/prometheus/prometheus.yml
            -   type: volume
                source: prometheus-data
                target: /prometheus
        ports:
            - "9090:9090"

    node_exporter:
        image: quay.io/prometheus/node-exporter:v1.8.2
        container_name: node_exporter_dev
        command:
            - '--path.rootfs=/host'
        network_mode: host
        pid: host
        restart: unless-stopped
        volumes:
            - '/:/host:ro,rslave'

    nginx-exporter:
        image: nginx/nginx-prometheus-exporter:1.4.1
        container_name: nginx-exporter-dev
        restart: unless-stopped
        networks:
            - dev
        command:
            "--nginx.scrape-uri=http://proxy:80/stub_status"
        ports:
            - "9113:9113"

    sync-server:
        image: sync-server
        build:
            context: sync-server
            dockerfile: ./Dockerfile
        container_name: sync-server
        networks:
            - dev
        restart: unless-stopped

    proxy:
        image: phygital/proxy_dev
        container_name: proxy_dev
        build:
            context: proxy
            dockerfile: ./dev/Dockerfile
        ports:
            - "80:80"
            - "443:443"
        volumes:
            -   type: bind
                source: ./proxy/dev/demo-widgets
                target: /var/html/widgets-demo
                read_only: true
            -   type: bind
                source: ./proxy/dev/demo-widgets2
                target: /var/html/widgets-demo2
                read_only: true
            -   type: bind
                source: ./proxy/dev/widget-kemerovo
                target: /var/html/widget-kemerovo
                read_only: true
            -   type: bind
                source: ./widgets/dist
                target: /var/html/widgets
                read_only: true
            -   type: bind
                source: ./proxy/dev/nginx.conf
                target: /etc/nginx/nginx.conf
                read_only: true
            -   type: bind
                source: ./proxy/options-ssl-nginx.conf
                target: /etc/nginx/options-ssl-nginx.conf
                read_only: true
            -   type: bind
                source: /etc/letsencrypt/live/dev.phygitalsignage.io/privkey.pem
                target: /etc/nginx/cert/privkey.pem
                read_only: true
            -   type: bind
                source: /etc/letsencrypt/live/dev.phygitalsignage.io/fullchain.pem
                target: /etc/nginx/cert/fullchain.pem
                read_only: true
            -   type: bind
                source: /etc/letsencrypt/archive
                target: /etc/nginx/archive
                read_only: true
            -   type: bind
                source: ./proxy/logs
                target: /var/log/nginx/logs
        healthcheck:
            test: curl -f localhost/ & curl -f localhost/api || exit 1
            interval: 10s
            timeout: 10s
            retries: 10
        restart: always
        depends_on:
            backend:
                condition: service_healthy
        networks:
            - dev
        command: "/bin/sh -c 'while :; do sleep 6h & wait $${!}; nginx -s reload; done & nginx -g \"daemon off;\"'"
        deploy:
            resources:
                limits:
                    cpus: '1'
                    memory: 1gb
                reservations:
                    cpus: '0.1'
                    memory: 500m

    mysql:
        image: mysql:8.4.3
        container_name: mysql_dev
        restart: unless-stopped
        command:
            - '--binlog_expire_logs_seconds=604800'
        environment:
            - MYSQL_DATABASE=$DEV_MYSQL_DATABASE
            - MYSQL_USER=$MYSQL_USER
            - MYSQL_PASSWORD=$DEV_MYSQL_PASSWORD
            - MYSQL_ROOT_PASSWORD=$DEV_MYSQL_ROOT_PASSWORD
            - TZ=$TIME_ZONE
        volumes:
            -   type: bind
                source: ./db/mysql
                target: /var/lib/mysql
        networks:
            - dev
        ports:
            - "3306:3306"
        deploy:
            resources:
                limits:
                    cpus: '0.5'
                    memory: 2gb
                reservations:
                    cpus: '0.1'
                    memory: 500m

    redis:
        image: redis:7.4.2
        container_name: redis_dev
        environment:
            - REDIS_USER=$DEV_REDIS_USER
            - REDIS_USER_PASSWORD=$DEV_REDIS_USER_PASSWORD
            - REDIS_PASSWORD=$DEV_REDIS_PASSWORD
        ports:
            - "6380:6379"
        volumes:
            - ./db/redis:/data
        networks:
            - dev
        deploy:
            resources:
                limits:
                    cpus: '0.50'
                    memory: 512M
                reservations:
                    cpus: '0.25'
                    memory: 256M
        healthcheck:
            test: ["CMD", "redis-cli", "-a", "$REDIS_PASSWORD", "ping"]
            interval: 30s
            timeout: 10s
            retries: 5
        restart: unless-stopped

    backend:
        image: phygital/backend_dev
        container_name: backend_dev
        build:
            context: .
            dockerfile: ./server/docker/Dockerfile
        volumes:
            -   type: bind
                source: .
                target: /app/
        healthcheck:
            test: curl localhost:3000/api
            interval: 20s
            timeout: 10s
            retries: 10
        restart: unless-stopped
        networks:
            - dev
        env_file:
            - .env
        environment:
            TZ: UTC
            NODE_ENV: production
            STAGE: dev
        deploy:
            resources:
                limits:
                    cpus: '2'
                    memory: 2gb
                reservations:
                    cpus: '1'
                    memory: 500m

networks:
    dev:
        driver: bridge
        driver_opts:
            com.docker.network.bridge.name: "phygital_dev"

volumes:
    prometheus-data:
