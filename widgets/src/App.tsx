import { BrowserRouter, Route, Navigate, Routes } from 'react-router';

import WeatherWidget from './widgets/WeatherWidget';
import WeatherWidget2 from './widgets/WeatherWidget2';
import CurrencyWidget from './widgets/CurrencyWidget';
import WidgetsContainer from './widgets';
import TimeWidget from './widgets/TimeWidget';
import TimeWidgetTV from './widgets/TimeWidgetTV';

function App() {
    return (
        <BrowserRouter basename="/widgets">
            <Routes>
                <Route index element={<WidgetsContainer />} />
                <Route path="/time" element={<TimeWidget />} />
                <Route path="/time-tv" element={<TimeWidgetTV />} />
                <Route path="/weather" element={<WeatherWidget />} />
                <Route path="/weather2" element={<WeatherWidget2 />} />
                <Route path="/currency" element={<CurrencyWidget />} />
                <Route path="/widget-kemerovo" element={null} />
                <Route path="*" element={<Navigate to="/" replace />} />
            </Routes>
        </BrowserRouter>
    );
}

export default App;
