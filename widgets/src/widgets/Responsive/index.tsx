import React from 'react';

interface ResponsiveContainerProps {
    children: React.ReactNode;
}

const ResponsiveContainer: React.FC<ResponsiveContainerProps> = ({ children }) => {
    return (
        <div
            className="w-full  h-auto flex justify-center items-center border border-gray-300 bg-gray-100 p-4"
            style={{
                aspectRatio: '10 / 1', // Сохраняем пропорцию 720:72
            }}
        >
            {children}
        </div>
    );
};

export default ResponsiveContainer;
