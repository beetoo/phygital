import { useWeatherWidget2 } from '../../hooks/useWeatherWidget2';

const WeatherWidget2 = () => {
    const { icon, temperature, condition, cloudCover, hasData } = useWeatherWidget2();

    return (
        <div className="flex flex-col items-center justify-center min-h-screen bg-gradient-to-b from-blue-200 to-blue-500 text-white">
            {hasData ? (
                <>
                    <div className="flex items-center">
                        {icon}
                        <div className="ml-4 text-6xl">{temperature}°C</div>
                    </div>
                    <div className="mt-4 text-4xl">{condition}</div>
                    <div className="mt-2 text-2xl">Облачность: {cloudCover}%</div>
                </>
            ) : null}
        </div>
    );
};

export default WeatherWidget2;
