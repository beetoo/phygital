export interface WeatherData2 {
    current: {
        temperature: number;
        condition: string;
        cloudCover: number;
        weatherCode: number;
        icon: JSX.Element;
    };
}
