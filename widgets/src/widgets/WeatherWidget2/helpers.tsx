import { SunIcon, CloudIcon, SparklesIcon, BoltIcon } from '@heroicons/react/24/outline';

const weatherConditions = [
    { code: [0], description: 'Ясная погода' }, // Clear sky
    { code: [1], description: 'Небольшая облачность' }, // Mainly clear, partly cloudy, and overcast
    { code: [2], description: 'Переменная облачность' },
    { code: [3], description: 'Пасмурно' },
    { code: [45], description: 'Туман' }, // Fog and depositing rime fog
    { code: [48], description: 'Изморось' },
    { code: [51, 53, 55], description: 'Моросящий дождь' }, // Drizzle: Light, moderate, and dense intensity
    { code: [56, 57], description: 'Ледяная морось' }, // Freezing Drizzle: Light and dense intensity
    { code: [61], description: 'Небольшой дождь' }, // Rain: Slight, moderate and heavy intensity
    { code: [63], description: 'Дождь' },
    { code: [65], description: 'Сильный дождь' },
    { code: [66, 67], description: 'Ледяной дождь' }, // Freezing Rain: Light and heavy intensity
    { code: [71, 73, 75], description: 'Снегопад' }, // Snow fall: Slight, moderate, and heavy intensity
    { code: [77], description: 'Снег' }, // Snow grains
    { code: [80, 81, 82], description: 'Ливень' }, // Rain showers: Slight, moderate, and violent
    { code: [85, 86], description: 'Снежный ливень' }, // Snow showers slight and heavy
    { code: [95], description: 'Гроза' }, // Thunderstorm: Slight or moderate
    { code: [96, 99], description: 'Гроза с градом' }, // Thunderstorm with slight and heavy hail
];

export const getWeatherCodeInterpretation = (weatherCode: number) => {
    return weatherConditions.find(({ code }) => code.includes(weatherCode))?.description ?? '';
};

export const getWeatherIcon2 = (code: number) => {
    if (code === 0) {
        return <SunIcon className="w-16 h-16 text-yellow-400" />;
    } else if ([1, 2, 3].includes(code)) {
        return <CloudIcon className="w-16 h-16 text-gray-500" />;
    } else if ([45, 48].includes(code)) {
        return <CloudIcon className="w-16 h-16 text-gray-300" />;
    } else if ([51, 53, 55, 56, 57, 61, 63, 65, 80, 81, 82].includes(code)) {
        return <CloudIcon className="w-16 h-16 text-blue-400" />;
    } else if ([66, 67, 71, 73, 75, 77, 85, 86].includes(code)) {
        return (
            <div className="flex items-center">
                <CloudIcon className="w-16 h-16 text-white" />
                <SparklesIcon className="w-8 h-8 text-white" />
            </div>
        );
    } else if ([95, 96, 99].includes(code)) {
        return (
            <div className="flex items-center">
                <CloudIcon className="w-16 h-16 text-gray-500" />
                <BoltIcon className="w-8 h-8 text-yellow-600" />
            </div>
        );
    } else {
        return <CloudIcon className="w-16 h-16 text-gray-500" />;
    }
};
