interface WeatherFact {
    temp: number;
    icon: string;
    condition: string;
    prec_strength: number;
    prec_type: number;
    dayTime: string;
    cloudness: string;
}

interface WeatherAlert {
    type: string;
    strength: boolean;
    isNoPrec: boolean;
    isNoRule: boolean;
    isNoData: boolean;
    current: {
        icon: string;
        prec_type: number;
        prec_strength: number;
        condition: string;
        cloudness: number;
    };
    time: number;
    state: string;
    smartZoom: number;
    title: string;
}

export interface WeatherData {
    fact: WeatherFact;
    alert: WeatherAlert;
}
