export const testWeatherData = {
    fact: {
        temp: 1,
        icon: 'ovc',
        condition: 'overcast',
        prec_strength: 0,
        prec_type: 0,
        dayTime: 'day',
        cloudness: 'cloudy',
    },
    alert: {
        type: 'noprec',
        strength: false,
        isNoPrec: true,
        isNoRule: false,
        isNoData: false,
        current: {
            icon: 'ovc',
            prec_type: 0,
            prec_strength: 0,
            condition: 'overcast',
            cloudness: 1,
        },
        time: 1737296400,
        state: 'noprec',
        smartZoom: 7,
        title: 'В ближайшие 2 часа осадков не ожидается',
    },
};
