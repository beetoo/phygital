import { useState, useEffect, useLayoutEffect } from 'react';

const formatTime = (date: Date, showSeconds: boolean) => {
    const options: Intl.DateTimeFormatOptions = {
        hour: '2-digit',
        minute: '2-digit',
        second: showSeconds ? '2-digit' : undefined,
        hour12: false,
    };
    return new Intl.DateTimeFormat('ru-RU', options).format(date);
};

const formatWeekday = (date: Date) => {
    const weekday = new Intl.DateTimeFormat('ru-RU', { weekday: 'long' }).format(date);
    return weekday.charAt(0).toUpperCase() + weekday.slice(1);
};

const formatDate = (date: Date) => {
    const dayMonth = new Intl.DateTimeFormat('ru-RU', { day: '2-digit', month: 'long' }).format(date);
    return dayMonth.charAt(0).toUpperCase() + dayMonth.slice(1);
};

export const ClockWidget = ({ showSeconds = false }: { showSeconds?: boolean }) => {
    const [time, setTime] = useState(formatTime(new Date(), showSeconds));
    const [fontSize, setFontSize] = useState('1rem');

    const updateFontSize = () => {
        const { innerWidth, innerHeight } = window;
        const newFontSize = Math.min(innerWidth / 3, innerHeight / 2);
        setFontSize(`${newFontSize}px`);
    };

    useEffect(() => {
        const interval = setInterval(() => {
            setTime(formatTime(new Date(), showSeconds));
        }, 1000);

        return () => clearInterval(interval);
    }, [showSeconds]);

    useLayoutEffect(() => {
        updateFontSize();
        window.addEventListener('resize', updateFontSize);

        return () => {
            window.removeEventListener('resize', updateFontSize);
        };
    }, []);

    return (
        <div className="grow flex flex-col justify-center items-center text-white bg-black">
            <div>
                <div style={{ fontSize: `${parseFloat(fontSize) / 4}px` }}>
                    {formatWeekday(new Date())}, {formatDate(new Date())}
                </div>
                <div className="font-bold mt-5" style={{ fontSize, lineHeight: 1 }}>
                    {time}
                </div>
            </div>
        </div>
    );
};
