import { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router';

import { WeatherData } from '../widgets/WeatherWidget/types';
import { getWeatherIcon } from '../widgets/WeatherWidget/helpers.tsx';

type ReturnType = {
    icon: JSX.Element;
    temperature: WeatherData['fact']['temp'];
    condition: WeatherData['fact']['condition'];
    alertTitle: WeatherData['alert']['title'];
    hasData: boolean;
};

const SERVER_API_URL =
    import.meta.env.VITE_STAGE === 'local' ? 'http://localhost:3000/api' : 'https://dev.phygitalsignage.io/api';

export const useWeatherWidget = (): ReturnType => {
    const [weatherData, setWeatherData] = useState<WeatherData | null>(null);

    const [searchParams, setSearchParams] = useSearchParams();

    useEffect(() => {
        if (!searchParams.get('lat') || !searchParams.get('lon')) {
            setSearchParams({ lat: '55.7558', lon: '37.6173' }, { replace: true });
        }
    }, [searchParams, setSearchParams]);

    useEffect(() => {
        const fetchWeatherData = async () => {
            const lat = searchParams.get('lat');
            const lon = searchParams.get('lon');

            if (lat && lon) {
                const response = await fetch(`${SERVER_API_URL}/weather?lat=${lat}&lon=${lon}`, {
                    headers: { 'x-weather-token': import.meta.env.VITE_X_WEATHER_TOKEN },
                });
                const data = await response.json();

                setWeatherData(data);
            }
        };

        fetchWeatherData();

        const interval = setInterval(fetchWeatherData, 15 * 60_000);

        return () => {
            clearInterval(interval);
        };
    }, [searchParams, setSearchParams]);

    const icon = getWeatherIcon(weatherData?.fact.icon ?? 'ovc'); // Используйте это для выбора иконки
    const temperature = weatherData?.fact.temp ?? 0;
    const condition = weatherData?.fact.condition ?? '';
    const alertTitle = weatherData?.alert.title ?? '';

    return { icon, temperature, condition, alertTitle, hasData: !!weatherData };
};
