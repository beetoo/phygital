import { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router';
import { fetchWeatherApi } from 'openmeteo';

import { getWeatherCodeInterpretation, getWeatherIcon2 } from '../widgets/WeatherWidget2/helpers';
import { WeatherData2 } from '../widgets/WeatherWidget2/types';

type ReturnType = {
    icon: JSX.Element;
    temperature: WeatherData2['current']['temperature'];
    condition: WeatherData2['current']['condition'];
    cloudCover: WeatherData2['current']['cloudCover'];
    hasData: boolean;
};

export const useWeatherWidget2 = (): ReturnType => {
    const [weatherData, setWeatherData] = useState<WeatherData2 | null>(null);

    const [searchParams, setSearchParams] = useSearchParams();

    useEffect(() => {
        if (!searchParams.get('lat') || !searchParams.get('lon')) {
            setSearchParams({ lat: '55.7558', lon: '37.6173' }, { replace: true });
        }
    }, [searchParams, setSearchParams]);

    useEffect(() => {
        const fetchWeatherData = async () => {
            const lat = searchParams.get('lat');
            const lon = searchParams.get('lon');

            if (lon && lat) {
                const params = {
                    latitude: [lat],
                    longitude: [lon],
                    current: 'temperature_2m,weather_code,cloud_cover',
                };

                const responses = await fetchWeatherApi('https://api.open-meteo.com/v1/forecast', params);

                // Process first location. Add a for-loop for multiple locations or weather models
                const response = responses[0];

                // Attributes for timezone and location
                // const utcOffsetSeconds = response.utcOffsetSeconds();
                // const timezone = response.timezone();
                // const timezoneAbbreviation = response.timezoneAbbreviation();
                // const latitude = response.latitude();
                // const longitude = response.longitude();

                const current = response.current()!;

                const weatherCode = current.variables(1)!.value();
                const condition = getWeatherCodeInterpretation(weatherCode);
                const icon = getWeatherIcon2(weatherCode);

                const data = {
                    current: {
                        temperature: Math.round(current.variables(0)!.value()), // Current is only 1 value, therefore `.value()`
                        condition: condition,
                        cloudCover: current.variables(2)!.value(),
                        icon: icon,
                        weatherCode: weatherCode,
                    },
                };

                setWeatherData(data);
            }
        };

        fetchWeatherData();

        const interval = setInterval(fetchWeatherData, 15 * 60_000);

        return () => {
            clearInterval(interval);
        };
    }, [searchParams]);

    const icon = getWeatherIcon2(weatherData?.current.weatherCode ?? 0); // Используйте это для выбора иконки
    const temperature = weatherData?.current.temperature ?? 0;
    const condition = weatherData?.current.condition ?? '';
    const cloudCover = weatherData?.current.cloudCover ?? 0;

    return { icon, temperature, condition, cloudCover, hasData: !!weatherData };
};
