import { useEffect, useState } from 'react';

const getTime = () => {
    const date = new Date();

    const hours = date.getHours().toString().padStart(2, '0');
    const minutes = date.getMinutes().toString().padStart(2, '0');
    const seconds = date.getSeconds().toString().padStart(2, '0');

    return { hours, minutes, seconds };
};

type ReturnType = {
    hours: string;
    minutes: string;
    seconds: string;
};

export const useTimeWidget = (): ReturnType => {
    const [time, setTime] = useState(getTime());

    useEffect(() => {
        const interval = setInterval(() => {
            setTime(getTime());
        }, 1000);

        return () => {
            clearInterval(interval);
        };
    }, []);

    return time;
};
