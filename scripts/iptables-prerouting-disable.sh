#!/bin/bash

# shellcheck disable=SC2024
sudo iptables-save > /home/phygital/save
sed -i 's/-A PREROUTING/#-A PREROUTING/' /home/phygital/save
sudo iptables-restore < /home/phygital/save

sudo rm /home/phygital/save

sudo ufw disable && sleep 5 && sudo ufw --force enable
