#!/bin/bash

if curl -s ifconfig.me | grep -o "158.160.6.64" >/dev/null; then
    docker compose --file /home/phygital/phygital/docker-compose.phygital.dev.yml --project-name phygital stop proxy

    sudo certbot renew --force-renewal --pre-hook="" --post-hook=""

    docker compose --file /home/phygital/phygital/docker-compose.phygital.dev.yml --project-name phygital up -d

    sudo certbot certificates

elif curl -s ifconfig.me | grep -o "158.160.11.46" >/dev/null; then
    docker compose --file /home/phygital/phygital/docker-compose.phygital.prod.yml --project-name phygital stop proxy

    sudo certbot renew --force-renewal --pre-hook="" --post-hook=""

    docker compose --file /home/phygital/phygital/docker-compose.phygital.prod.yml --project-name phygital up -d

    sudo certbot certificates
fi
