#!/bin/bash

if curl -s ifconfig.me | grep -o "158.160.6.64" >/dev/null; then
    pnpm i && npm run build:server

    docker compose --file docker-compose.phygital.dev.yml --project-name phygital restart --no-deps backend
elif curl -s ifconfig.me | grep -o "158.160.11.46" >/dev/null; then
    pnpm i && npm run build:server

    docker compose --file docker-compose.phygital.prod.yml --project-name phygital restart --no-deps backend
fi
