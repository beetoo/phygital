#!/bin/bash

if curl -s ifconfig.me | grep -o "158.160.6.64" >/dev/null; then

    docker compose --file docker-compose.phygital.dev.yml --project-name phygital restart

elif curl -s ifconfig.me | grep -o "158.160.11.46" >/dev/null; then

    docker compose --file docker-compose.phygital.prod.yml --project-name phygital restart

fi
