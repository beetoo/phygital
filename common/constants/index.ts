export const DEFAULT_CANCEL_TIMEOUT = 3000;

export const SUPPORTED_VIDEO_FORMATS = ['.mp4'];

export const SUPPORTED_CONTENT_FORMATS = ['.mp4', '.jpg', '.jpeg', '.gif', '.png', '.wgt'];

export const pictureFormats = ['.bmp', '.tiff', '.png', '.jpg', '.jpeg', '.gif', '.webp'];

export const videoFormats = [
    '.3g2',
    '.3gp',
    '.3gp2',
    '.3gpp',
    '.3gpp2',
    '.asf',
    '.asx',
    '.avi',
    '.dat',
    '.drv',
    '.f4v',
    '.flv',
    '.gtp',
    '.h264',
    '.m4v',
    '.mkv',
    '.mod',
    '.moov',
    '.mov',
    '.mp4',
    '.mpeg',
    '.mpg',
    '.mts',
    '.rm',
    '.rmvb',
    '.spl',
    '.srt',
    '.stl',
    '.swf',
    '.ts',
    '.vcd',
    '.vid',
    '.vid',
    '.vid',
    '.vob',
    '.webm',
    '.wm',
    '.wmv',
    '.yuv',
];

export type ScreensTariffInfo = {
    screens: number;
    sum: number;
    capacity: number;
    licenseId?: string;
    minutes?: number;
};

export const screensTariffsInfoList = [
    { screens: 3, sum: 15000, capacity: 5 * 1000 },
    { screens: 10, sum: 50000, capacity: 10 * 1000 },
    { screens: 20, sum: 95000, capacity: 10 * 1000 },
    { screens: 30, sum: 130000, capacity: 25 * 1000 },
    { screens: 40, sum: 170000, capacity: 25 * 1000 },
    { screens: 50, sum: 210000, capacity: 25 * 1000 },
    { screens: 75, sum: 290000, capacity: 50 * 1000 },
    { screens: 100, sum: 380000, capacity: 50 * 1000 },
    { screens: 150, sum: 540000, capacity: 50 * 1000 },
    { screens: 200, sum: 715000, capacity: 50 * 1000 },
    { screens: 300, sum: 1050000, capacity: 100 * 1000 },
    { screens: 400, sum: 1350000, capacity: 100 * 1000 },
    { screens: 500, sum: 1650000, capacity: 100 * 1000 },
    { screens: 750, sum: 2450000, capacity: 100 * 1000 },
    { screens: 1000, sum: 3250000, capacity: 100 * 1000 },
];

export type StorageTariffInfo = { sum: number; capacity: number; screens: number; licenseId?: string };

export const storageTariffsInfoList = [
    { sum: 3990, capacity: 20 * 1000, screens: 0 },
    { sum: 3990 * 2, capacity: 40 * 1000, screens: 0 },
    { sum: 3990 * 3, capacity: 60 * 1000, screens: 0 },
    { sum: 3990 * 4, capacity: 80 * 1000, screens: 0 },
    { sum: 3990 * 5, capacity: 100 * 1000, screens: 0 },
];
