export type OrganizationLanguageType = 'en' | 'ru' | 'es';

export enum CONTENT_TYPE {
    VIDEO = 'video',
    IMAGE = 'image',
    WEB = 'web',

    WIDGET = 'widget',
    UNKNOWN = 'unknown',
}

export interface ClientOrganizationType {
    id: string;
    userId: string;
    title: string;
    name: string;
    surname: string;
    email: string;
    phone: string;
    role: UserRoleType;
    status: 'pending' | 'active';
    createdAt: Date;
    licenses: LicenseType[];
    maxScreens: number;
    limitScreens: number;
    limitStorageSize: number;
    hasActiveSubscriptions: boolean;
    signageStorageSize: number;
    billingAvailable: boolean;
    language: OrganizationLanguageType;
    licenseScreensNumber: number;
    licenseCapacityAmount: number;
    maxScreensCount: number;
    locations: LocationType[];
}

export enum LicenceTypeEnum {
    basic_screens = 'basic:screens',
    basic_storage = 'basic:storage',
    trial_screens = 'trial:screens',
    trial_storage = 'trial:storage',
    free_screens = 'free:screens',
    free_storage = 'free:storage',
}

export type LicenseStatusType = 'cancelled' | 'active' | 'renewed' | 'expired' | 'suspended' | 'pending';

export interface LicenseType {
    id: string;
    type: LicenceTypeEnum;
    screensNumber: number;
    capacity: number;
    price: number;
    startDate: Date | null;
    expirationDate: Date | null;
    description: string;
    status: LicenseStatusType;
    monthCount: number;
    partner: string;
    paymentDate: Date | null;
    invoiceId: string;
    note: string;
}

export type UserRoleType = 'admin' | 'accounting' | 'user' | 'viewonly';

export enum OrganizationProductEnum {
    PHYGITAL = 'phygital',
    MENUSIGN = 'menusign',
}

export type OrganizationStatusType = 'new' | 'paid' | 'trial' | 'partner' | 'expired' | 'cancelled';

export type OrganizationSubscriptionType = {
    inn: number;
    company: string;
    notes: string; // видно только администратору
    screens: number; // только целые значения
    storage: number; // только целые значения в байтах
    validUntil: string; // дата, у которой всегда первое число месяца. В запросе будет передаваться только YYYY-MM
};

export interface OrganizationType {
    id: string;
    name: string;
    createdAt: Date;
    status: OrganizationStatusType;
    subscription: OrganizationSubscriptionType | null;
    maxScreens: number;
    freeScreens: number;
    signageStorageSize: number;
    product: OrganizationProductEnum;
    users: UserType[];
}

export interface OrganizationInvitationType {
    id: string;
    organizationId: string;
    userId: string;
    inviterId: string;
    role: UserOrganizationRoleEnum;
    permissions: string[];
    createdAt: Date;
}

export enum UserOrganizationRoleEnum {
    member = 'member',
    admin = 'admin',
    owner = 'owner',
}

export interface UserType {
    id: string;
    name: string;
    surname: string;
    email: string;
    phone: string;
    password?: string;
    emailVerificationToken?: string;
    role: UserRoleType;
    status: 'pending' | 'active';
    language: OrganizationLanguageType;
    createdAt: Date;
    lastActiveAt: Date;
    properties: UserOrganizationPropertyType[];
    organizations: OrganizationType[];
}

export interface UserOrganizationPropertyType {
    id: string;
    organizationId: string;
    role: UserOrganizationRoleEnum;
    permissions: string[];
}

export interface ScreenTagType {
    id: string;
    name: string;
}

export interface LocationType {
    id: string;
    organizationId: string;
    name: string;
    city: string;
    address: string;
    geo_lat: string;
    geo_lon: string;
    tzLabel: string;
    tzOffset: number;
    screens: ScreenType[];
    createdAt: Date;
}

export type ScreenStatusType = 'online' | 'offline' | 'error' | 'pending';

export enum OrientationEnum {
    LANDSCAPE = 'LANDSCAPE',
    PORTRAIT = 'PORTRAIT',
    LANDSCAPE_FLIPPED = 'LANDSCAPE_FLIPPED',
    PORTRAIT_FLIPPED = 'PORTRAIT_FLIPPED',
}

export type ScreenMetaType = {
    screenshotSec: number;
    restartSec: number;
};

export interface ScreenType {
    id: string;
    organizationId: string;
    createdAt: Date;
    aliveAt: Date;
    identification: string;
    name: string;
    model: string;
    meta: ScreenMetaType;
    startTime: { hours: number; minutes: number };
    endTime: { hours: number; minutes: number };
    resolution: { width: number; height: number };
    orientation: OrientationEnum;
    availableBytes: number;
    totalBytes: number;
    prefetchTime: number | null;
    deletedAt: Date;
    location: LocationType;
    smil: string | null;
    playlists: PlaylistType[];
    log?: ScreenRequestLoggerType | undefined;
    verificationHash?: string;
    hasHistory?: boolean;
    hasMessages?: boolean;
    urlParams: { [key: string]: string | number } | null;
}

export interface ScreenRegType {
    id: string;
    identification: string;
    verificationHash: string;
    createdAt: Date;
    updatedAt: Date;
}

export interface ScreenHistoryType {
    id: string;
    screenId: string;
    status: ScreenStatusType;
    createdAt: Date;
}

export enum PlaylistUploadStatus {
    upload_error = 'upload_error',
    upload_success = 'upload_success',
    uploading = 'uploading',
}
export enum PlaylistStatus {
    initial = 'initial',
    upload_error = 'upload_error',
    upload_success = 'upload_success',
    uploading = 'uploading',
    draft = 'draft',
}

export type VirtualScreenType = { resolution: { width: number; height: number }; orientation: OrientationEnum };

export type PlaylistDtoInfo = {
    organizationId?: string;
    title: string;
    virtualScreen?: VirtualScreenType;
    draft: boolean;
};

export type PlaylistSavedScheduleType = Pick<
    PlaylistScheduleType,
    'startDay' | 'endDay' | 'startTime' | 'endTime' | 'byDay' | 'broadcastAlways'
>;

export interface PlaylistRegion {
    id: number;
    width: number;
    height: number;
    xPosition: number;
    yPosition: number;
    zIndex: number;
}

export type TemplateSectionType = {
    sectionName?: string;

    left: string; // Значение отступа слева (например, "0", "50%", "100px")
    top: string; // Значение отступа сверху (например, "0", "50%", "100px")

    width: string; // Ширина секции (например, "50%", "100px")
    height: string; // Высота секции (например, "50%", "100px")

    fit?: 'fill' | 'hidden' | 'meet' | 'slice';

    content?: ({ src: string; urlParamMode?: UrlParamModeType } | string)[]; // Ссылка на контент, который будет отображаться в секции
};

export interface TemplateType {
    id: string;
    organizationId: string;
    name: string;
    sections: TemplateSectionType[];
    createdAt: Date;
    updatedAt: Date;
}

export interface PlaylistType {
    id: string;
    organizationId: string;
    templateId: string | null;
    type: 'basic' | 'interval';
    title: string;
    createdAt: Date;
    updatedAt: Date;
    draft: boolean;
    priority: number;
    intervalDuration: number | null;
    virtualScreen?: VirtualScreenType;
    playlistItems: PlaylistItemType[];
    resolution: { width: number; height: number };

    regions: PlaylistRegion[];
    sync: boolean;
    log: boolean;
    schedule: PlaylistSavedScheduleType;
    hasHistory?: boolean;
}

type WeatherAndTrafficOperator = 'more' | 'less' | 'equally';

type WeatherAndTrafficValue =
    | 'overcast'
    | 'cloudy'
    | 'clear'
    | 'drizzle'
    | 'rain'
    | 'wet-snow'
    | 'snow'
    | 'snow-showers'
    | 'thunderstorm'
    | '[0,5]'
    | '[5.1,14]'
    | '[14.1,24]'
    | '[24.1,32]'
    | '[32.1,150]'
    | '[0,50]'
    | '[50,60]'
    | '[61,75]'
    | '[76,100]'
    | string;

type WeatherParameter =
    | 'temperature'
    | 'water_temperature'
    | 'cloud_cover'
    | 'rain_fall'
    | 'wind'
    | 'atmospheric_pressure'
    | 'humidity';

export type WeatherAndTrafficData = {
    parameter?: WeatherParameter;
    operator?: WeatherAndTrafficOperator;
    value?: WeatherAndTrafficValue;
};

export type TimeData = {
    startDate?: Date;
    endDate?: Date;
    startDay?: string;
    endDay?: string;
    startTime?: string;
    endTime?: string;
    frequency?: FrequencyType;
    byDay?: string;
};

type ScenarioCondition = 'weather' | 'traffic' | 'time';

export interface ScenarioRuleType {
    id: string;
    condition?: ScenarioCondition;
    data?: WeatherAndTrafficData | TimeData;
}

export interface ScenarioType {
    id: string;
    organizationId: string | null;
    name: string;
    description: string;
    rules: ScenarioRuleType[];
}

export type PlaylistItemPropertiesType = { recurringRule: 'frequency' | 'time'; recurringValue: number } | null;

export interface PlaylistItemType {
    id: string;
    type: 'recurring' | 'filler' | null;
    name: string;
    delay: number;
    regionId: number;
    orderIndex: number;
    playlistId: string;
    scenarioId: string | null;
    createdAt?: Date;
    updatedAt?: Date;
    properties: PlaylistItemPropertiesType;
    content: ContentType;
    scenario?: ScenarioType;
    playlist?: PlaylistType;
}

export type UrlParamModeType = 'all' | 'screen' | 'location' | 'none';

export interface ContentType {
    id: string;
    organizationId: string;
    originFilename: string;
    filename: string;
    webContentLink: string;
    type: CONTENT_TYPE;
    dimensions: { width: number; height: number };
    size: number;
    duration: number;
    bitrate: number;
    lifetime: { startDate: string; endDate: string } | null;
    src: string;
    createdAt?: Date;
    updatedAt?: Date;
    playlistItems?: PlaylistItemType[];
    folder?: FolderType;
    urlParamMode: UrlParamModeType;
}

export type PaymentStatusType = 'pending' | 'paid' | 'cancelled';

export interface PaymentType {
    id: string;
    inn: string;
    company: string;
    organizationId: string;
    invoiceNumber: string;
    status: PaymentStatusType;
    screens: number;
    pricePerScreen: number;
    total: number; // автоматически расчет исходя из числа экранов и стоимости
    days: number;
    startDate: string; // YYYY-MM-DD
    notes: string;

    createdAt: Date;
    updatedAt: Date;
}

export type MessageSenderRoleType = 'user' | 'support';

export interface SupportTicketMessageType {
    id: string;
    ticketId: string;
    senderRole: MessageSenderRoleType;
    userId: string;
    message: string;
    createdAt: Date;
    updatedAt: Date;
    readAt: Date | null;
    replyTo: string | null;
}

export type SupportTicketStatusType = 'new' | 'in_progress' | 'resolved' | 'not_resolved';

export interface SupportTicketType {
    id: string;
    userId: string;
    organizationId: string | null;
    subject: string;
    description: string;
    status: SupportTicketStatusType;
    createdAt: Date;
    updatedAt: Date;
    closedAt: Date | null;
    messages?: SupportTicketMessageType[];
}

export interface FolderType {
    id: string;
    organizationId: string;
    name: string;
    createdAt: Date;
    updatedAt: Date;
    contents: ContentType[];
}

export enum BillingTypeEnum {
    personal = 'personal',
    screens = 'screens',
    storage = 'storage',
}

export type FrequencyType = 'DAILY' | 'WEEKLY' | '';

export interface PlaylistScheduleType {
    id: string;
    startDay: string;
    endDay: string;
    startTime: string;
    endTime: string;
    broadcastAlways: boolean;
    byDay: string;
    updatedAt?: Date;
}

export type ScreenRequestType = 'HEAD' | 'GET';

export interface ScreenRequestLoggerType {
    id: string;
    identification: string;
    requestType: ScreenRequestType;
    requestJson?: string;
    updatedAt: Date;
    message?: string;
    error?: string;
}

export type AnalysisStatusType = 'initial' | 'pending' | 'processing' | 'success' | 'error';

export interface AnalysisGroupType {
    id: string;
    organizationId: string;
    name: string;
    xlsx: string;
    status: AnalysisStatusType;
    result: AnalysisGroupResultType[];
    videos: AnalysisVideoType[];
    createdAt: Date;
    snackbar: boolean;
}

export interface AnalysisVideoType {
    id: string;
    cameraId: string;
    part: number;
    originalFilename: string;
    filename: string;
    dimensions: { width: number; height: number };
    size: number;
    duration: number;
    createdAt: Date;
    status: AnalysisStatusType;
    xlsx: string;
    src: string;
    result: any;
    group: AnalysisGroupType;
    snackbar: boolean;
}

export type AnalysisGroupAttentionPeriodType = {
    id: string;
    startTime: number;
    endTime: number;
    startFrameNumber: number;
    endFrameNumber: number;
    result: AnalysisGroupResultType;
};

export type AnalysisVideoAttentionPeriodType = {
    id: string;
    startTime: number;
    endTime: number;
    startFrameNumber: number;
    endFrameNumber: number;
    result: AnalysisVideoResultType;
};

export interface AnalysisGroupResultType {
    id: string;
    personId: number;
    entryTime: number;
    exitTime: number;
    entryFrameNumber: number;
    exitFrameNumber: number;
    gender: 'male' | 'female';
    ageGroup: 'child' | 'adult' | 'elderly';
    attentionPeriods: AnalysisGroupAttentionPeriodType[];
    group: AnalysisGroupType;
}

export interface AnalysisVideoResultType {
    id: string;
    personId: number;
    entryTime: number;
    exitTime: number;
    entryFrameNumber: number;
    exitFrameNumber: number;
    gender: 'male' | 'female';
    ageGroup: 'child' | 'adult' | 'elderly';
    attentionPeriods: AnalysisVideoAttentionPeriodType[];
    video: AnalysisVideoType;
}

export interface AnalysisCameraType {
    id: string;
    cameraId: string;
    title: string;
}
