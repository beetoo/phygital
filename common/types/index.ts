import { LicenseType, OrganizationStatusType } from './entities';

export * from './entities';

export type TakeSkipAndSearchType = { take: number; skip: number; search?: string; status?: OrganizationStatusType };

export type ClientOrganizationInfo = {
    limitScreens: number;
    licenses: LicenseType[];
    limitStorageSize: number;
    usedStorageSize: number;
};

export type BalanceRequestType = 'license_creation' | 'license_renewal' | 'storage_increase';

export type ScenarioCondition = 'weather' | 'traffic' | 'time';

export type DeleteScreenTagDto = { screenId: string; tagId: string } | { screenId: string; tagId: string }[];
