import { CreateFolderDto } from '../../server/src/models/folder/dto/create-folder.dto';

export declare class AddFolderPayload extends CreateFolderDto {
    organizationId: string;
}

export declare class UpdateContentItemPayload {
    name: string;
    folderId?: string;
    webContentLink?: string;
}

export declare class UpdateContentItemsPayload {
    folderId: string;
}
