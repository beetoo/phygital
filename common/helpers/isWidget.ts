import { getExtension } from './index';

const extensions = new Set(['wgt']);

export default function isWidget(filePath: string) {
    const extension = getExtension(filePath);
    return extensions.has(extension?.slice(1)?.toLowerCase());
}
