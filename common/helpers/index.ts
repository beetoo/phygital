import { slugify } from 'transliteration';
import timezones from 'timezones-list';

import { videoFormats, pictureFormats, SUPPORTED_VIDEO_FORMATS, SUPPORTED_CONTENT_FORMATS } from '../constants';
import { CONTENT_TYPE } from '../types';

type ReturnValue = {
    isRequiredContentType: boolean;
    isRequiredVideoType: boolean;
    type: CONTENT_TYPE;
    extension: string;
    isVideo: boolean;
    isImage: boolean;
    isUnknown: boolean;
};

export const getExtension = (filePath: string) => {
    const extensionMatch = filePath?.toLowerCase()?.match(/.+(\..+)$/) ?? [];
    return extensionMatch[1]?.toLowerCase();
};

export const getContentFileInfo = (filename: string): ReturnValue => {
    const extension = getExtension(filename);
    const isVideo = videoFormats.includes(extension);
    const isImage = pictureFormats.includes(extension);
    const isUnknown = !isVideo && !isImage;
    const isRequiredContentType = SUPPORTED_CONTENT_FORMATS.includes(extension);
    const isRequiredVideoType = SUPPORTED_VIDEO_FORMATS.includes(extension);
    const type = isVideo ? CONTENT_TYPE.VIDEO : isImage ? CONTENT_TYPE.IMAGE : CONTENT_TYPE.UNKNOWN;

    return {
        isRequiredContentType,
        isRequiredVideoType,
        type,
        extension,
        isVideo,
        isImage,
        isUnknown,
    };
};

export const slugifyBeforeUpload = (filename: string): string => slugify(filename, { trim: true, separator: '_' });

export const reduceText = (text: string, limit: number): string =>
    text.length > limit ? text.slice(0, limit) + '...' : text;

export const withZero = (time: number): string => (time > 9 ? time.toString() : '0' + time.toString());

export const formatSecondsToTimeString = (seconds: number, withoutHours = false): string => {
    const minutes = withZero(Math.floor(seconds / 60) % 60);
    const sec = withZero(Math.floor(seconds % 60));

    if (withoutHours) {
        return `${minutes}:${sec}`;
    }
    const hours = withZero(Math.floor(seconds / 3600));

    return `${hours}:${minutes}:${sec}`;
};

export const isInvalidLatitude = (latitude: string) =>
    isNaN(Number(latitude)) || Number(latitude) < -90 || Number(latitude) > 90;

export const isInvalidLongitude = (latitude: string) =>
    isNaN(Number(latitude)) || Number(latitude) < -180 || Number(latitude) > 180;

export const isInvalidTimezone = (timezoneLabel: string) =>
    !timezones.some((timezone) => timezone.label === timezoneLabel);

export const getTzOffsetByTzLabel = (tzLabel: string): number => {
    const splitOffset = (timezones.find((item) => item.label === tzLabel)?.utc ?? '+00:00').split(':');

    const hour = Number(splitOffset[0]) * 60;
    const minutes = (hour > 0 ? 1 : -1) * Number(splitOffset[1]);

    return hour + minutes;
};

export const frequencyFromByDay = (byDay: string) => (byDay === 'MO,TU,WE,TH,FR,SA,SU' ? 'DAILY' : 'WEEKLY');
