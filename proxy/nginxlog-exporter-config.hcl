listen {
#  port = 4040
#  address = "127.0.0.1"
  metrics_endpoint = "/metrics"
}

namespace "nginx" {
  format = "$remote_addr - $remote_user [$time_local] \"$request\" $status $body_bytes_sent \"$http_referer\" \"$http_user_agent\" \"$http_x_forwarded_for\" Request time: $request_time"
#    source {
#      files = [
#        ".proxy/logs/access.log"
#      ]
#    }

    source {
        syslog {
            listen_address = "udp://localhost:8000"
            format = "auto"
            tags = ["nginx"]
        }
    }

  # log can be printed to std out, e.g. for debugging purposes (disabled by default)
  print_log = true
}
