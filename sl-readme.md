### Сборка образов для серверных лицензии

#### 1. Генерируем зашифрованную строку с макс. кол-вом девайсов и временем окончания лицензии

В файле xor.ts прописываем переменные maxDevices и expiredTime.

Далее запускаем команду `ts-node xor.ts` и в консоли получаем зашифрованную строку.

Эту строку копируем в файл .env.sl (или .env.eng, или .env).

#### 2. В env файл прописываем значение client и backend адреса, логин и пароль админа и т.д.
Например, для .env.sl:

SL_CLIENT_API_URL=https://demo.phygitalsignage.ru
SL_SERVER_API_URL=https://demo.phygitalsignage.ru/api

ADMIN_LOGIN=admin@phygitalsignage.io
ADMIN_PASSWORD=admin

#### 3. Создаем образы

Команда `npm run create:sl` (для серверных лицензий на удаленном сервере)

или `npm run create:sl:eng` (для английского сайта)

или `npm run create:sl:local` (для локальной машины)

В конце должны создаться три докер-контейнера, которые будут запушены на наш докер-сервер.

### Пример запуска образов на удаленном сервере

1. На удалённом сервере создаем папку phygital.

Из папки sl-server-files копируем подготовленные файлы в эту папку.

Файлы необходимы следующие:

docker-compose.phygital.sl-deploy.yml - файл для запуска проекта через docker-compose.
env.sl - файл с правильными переменными окружения для запуска серверной части.
sl-prepare-ubuntu.sh - скрипт подготавливает среду для запуска проекта, скачивает докер образы и запускает проект.
sl-compose-ubuntu.sh - скрипт обновления проекта с очисткой старых докер файлов.
sl-up-ubuntu.sh - скрипт быстрого обновления проекта без очистки старых докер файлов.

Копируем, например, такой командой:

`scp -r D://projects/phygital/sl-service-files/demo/.env.sl phygital@158.160.6.14:/home/phygital/phygital &&
scp -r D://projects/phygital/sl-service-files/demo/* phygital@158.160.6.14:/home/phygital/phygital`

2. Запускаем скрипт sl-prepare-ubuntu.sh c помощью команды:

`./sl-prepare-ubuntu.sh`

На этапе создания SSL-сертификата нужно будет ответить на вопросы программы.

Если в конце появилось сообщение, что отсутствуют права для запуска докер образов,
то нужно выйти из терминала, зайти снова и запустить скрипт заново.

3. В папку upload копируем с локальной машины папку smil.

Например, командой:

`scp -r D://projects/phygital/upload/smil phygital@158.160.6.14:/home/phygital/phygital/upload/smil`

