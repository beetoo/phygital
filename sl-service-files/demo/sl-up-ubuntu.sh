#!/bin/bash

docker compose --file docker-compose.phygital.sl-deploy.yml --project-name phygital up --force-recreate -d --build

docker container exec frontend_sl sed -i 's/http:\/\/localhost:8000/https:\/\/demo.phygitalsignage.ru/' index.html

docker container exec proxy_sl crond
