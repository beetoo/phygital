#!/bin/bash

# Установка docker
if ! which docker >/dev/null; then
  # Add Docker's official GPG key:
  sudo apt-get update
  sudo apt-get install ca-certificates curl
  sudo install -m 0755 -d /etc/apt/keyrings
  sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
  sudo chmod a+r /etc/apt/keyrings/docker.asc

  # Add the repository to Apt sources:
  echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  sudo apt-get update

  sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y

  sudo groupadd docker
  sudo usermod -aG docker $USER
  sudo systemctl enable docker.service
  sudo systemctl enable containerd.service
fi

# Установка certbot
if ! which certbot >/dev/null; then
  sudo apt install snapd -y
  sudo snap install core

  sudo snap install --classic certbot
  sudo ln -s /snap/bin/certbot /usr/bin/certbot
fi

# Установка сертификата
if [ ! -d /etc/letsencrypt/live ]; then
    sudo certbot certonly
    sudo chown -R phygital:phygital /etc/letsencrypt/live
    sudo chown -R phygital:phygital /etc/letsencrypt/archive
    sudo chmod -R 777 /etc/letsencrypt/live
    sudo chmod -R 777 /etc/letsencrypt/archive
fi

if ! grep -z '"auth": "[^"]*' /home/phygital/.docker/config.json | grep -o '[^"]*$' >/dev/null; then
    docker login docker.phygitalsignage.tech -u phygital -p phygital
fi

mkdir -p db db/mysql upload upload/smil proxy proxy/logs

docker compose --project-name phygital stop
docker system prune -a -f --volumes

docker compose --file docker-compose.phygital.sl-deploy.yml --project-name phygital up --force-recreate -d --build

docker container exec frontend_sl sed -i "s/http:\/\/localhost:8000/https:\/\/cloud.phygitalsignage.ai/" index.html

docker container exec proxy_sl crond
