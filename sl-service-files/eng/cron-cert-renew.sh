#!/bin/bash

docker compose --file /home/phygital/phygital/docker-compose.phygital.sl-deploy.yml --project-name phygital stop proxy

sudo certbot renew --force-renewal --pre-hook="" --post-hook=""

docker compose --file /home/phygital/phygital/docker-compose.phygital.sl-deploy.yml --project-name phygital up -d

docker container exec frontend_sl sed -i "s/http:\/\/localhost:8000/https:\/\/cloud.phygitalsignage.ai/" index.html
