#!/bin/bash

if ! which cron >/dev/null; then
    sudo apt install cron
fi

if ! which logrotate >/dev/null; then
    sudo apt install logrotate
fi

mkdir -p /home/phygital/phygital/proxy/logs

sudo touch /home/phygital/phygital/proxy/logs/access.log
sudo touch /home/phygital/phygital/proxy/logs/error.log

echo "/home/phygital/phygital/proxy/logs/*.log {
        su root root
        daily
        missingok
        rotate 7
        notifempty
        create 0644 root root
        compress
        dateext
        dateformat _%Y%m%d
        sharedscripts
        postrotate
                if [ -f /var/run/nginx.pid ]; then
                        kill -USR1 'cat /var/run/nginx.pid'
                fi
        endscript
}" > sudo tee /etc/logrotate.d/phygital

#crontab -l >/tmp/c1
echo "0 1 * * 4 bash /home/phygital/phygital/scripts/cron-cert-renew.sh" >>/tmp/c1
echo "50 23 * * * sudo logrotate /etc/logrotate.d/phygital" >>/tmp/c1
crontab /tmp/c1
sudo rm /tmp/c1

sudo systemctl enable cron
