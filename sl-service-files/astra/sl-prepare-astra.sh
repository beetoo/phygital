#!/bin/bash

# Установка docker
if ! which docker >/dev/null; then
  # Add Docker's official GPG key:
  sudo apt-get update
  sudo apt-get install ca-certificates curl
  sudo install -m 0755 -d /etc/apt/keyrings
  sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
  sudo chmod a+r /etc/apt/keyrings/docker.asc

  # Add the repository to Apt sources:
  echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian stretch stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  sudo apt-get update

  sudo apt-get install docker-ce docker-ce-cli containerd.io

  sudo groupadd docker
  sudo usermod -aG docker $USER
  sudo systemctl enable docker.service
  sudo systemctl enable containerd.service
fi

# Установка docker-compose
if ! which docker-compose >/dev/null; then
  sudo curl -L "https://github.com/docker/compose/releases/download/v2.24.5/docker-compose-linux-x86_64" -o /usr/local/bin/docker-compose

  sudo chmod +x /usr/local/bin/docker-compose
fi

# Установка certbot
if ! which certbot >/dev/null; then
  # Установка certbot
  echo "[+] Adding Debian Buster repo..."
  sudo sh -c "echo 'deb [trusted=yes] https://mirror.yandex.ru/debian/ buster main contrib non-free' >> /etc/apt/sources.list"
  echo "[+] Adding repo's GPG keys..."
  gpg --keyserver keyserver.ubuntu.com --recv-key 648ACFD622F3D138
  gpg -a --export 648ACFD622F3D138 | sudo apt-key add -
  gpg --keyserver keyserver.ubuntu.com --recv-key 0E98404D386FA1D9
  gpg -a --export 0E98404D386FA1D9 | sudo apt-key add -
  gpg --keyserver keyserver.ubuntu.com --recv-key DCC9EFBF77E11517
  gpg -a --export DCC9EFBF77E11517 | sudo apt-key add -
  echo "[+] Updating..."
  sudo apt update
  echo "[+] Installing snap..."
  sudo apt install snapd -y
  sudo snap install core

  sudo snap install --classic certbot
  sudo ln -s /snap/bin/certbot /usr/bin/certbot
fi

if [ ! -d /etc/letsencrypt/live ]; then
    sudo certbot certonly
    sudo chown -R phygital:phygital /etc/letsencrypt/live
    sudo chown -R phygital:phygital /etc/letsencrypt/archive
    sudo chmod -R 777 /etc/letsencrypt/live
    sudo chmod -R 777 /etc/letsencrypt/archive
fi

if ! mount | grep /sys/fs/cgroup/systemd > /dev/null; then
    sudo mkdir -p /sys/fs/cgroup/systemd
    sudo mount -t cgroup -o none,name=systemd cgroup /sys/fs/cgroup/systemd
fi

if ! grep -z '"auth": "[^"]*' /home/phygital/.docker/config.json | grep -o '[^"]*$' >/dev/null; then
    docker login docker.phygitalsignage.tech -u phygital -p phygital
fi

mkdir -p db db/mysql upload upload/smil proxy proxy/logs

docker-compose --project-name phygital stop
docker system prune -a -f --volumes

docker-compose --file docker-compose.phygital.sl-deploy.yml --project-name phygital up --force-recreate -d --build

docker container exec frontend_sl sed -i "s/http:\/\/localhost:8000/https:\/\/astra-linux.phygitalsignage.tech/" index.html

docker container exec proxy_sl crond
