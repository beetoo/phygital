### Пример запуска проекта

* На удалённом сервере создаем папку `phygital`


* Копируем в эту папку файлы:

    `docker-compose.phygital.sl-deploy.yml` - файл для запуска проекта через docker-compose.

    `env.sl` - файл с переменными окружения для запуска серверной части.

    `sl-compose-ubuntu.sh` - скрипт создает необходимые папки, скачивает образы и запускает проект.


* Прописываем в файле `docker-compose.phygital.sl-deploy.yml` пути к файлам сертификатов, находящимся на локальной машине.

    Это 13-я и 17-я строчки.

    По умолчанию это:
        `source: /etc/letsencrypt/live/ekran.avtoros.loc/privkey.pem`
    и
        `source: /etc/letsencrypt/live/ekran.avtoros.loc/fullchain.pem`

    Конфигурация прокси-сервера настроена на работу с файлами с именами `privkey.pem` и `fullchain.pem`.

    Если файлы другие, то нужно после запуска контейнера `proxy_sl` менять конфиг в файле `/etc/nginx/options-ssl-nginx.conf`.


* Запускаем скрипт `sl-compose-ubuntu.sh`


* В созданную скриптом папку `upload` копируем папку `smil` с его содержимым.
