#!/bin/bash

docker login docker.phygitalsignage.tech -u phygital -p phygital

mkdir -p db db/mysql upload upload/smil proxy proxy/logs

docker compose --project-name phygital stop
docker system prune -a -f --volumes

docker compose --file docker-compose.phygital.sl-deploy.yml --project-name phygital up --force-recreate -d --build

docker container exec frontend_sl sed -i 's/http:\/\/localhost:8000/http:\/\/ekran.avtoros.loc/' index.html
