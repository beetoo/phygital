import { useEffect, useState } from 'react';
import { useSearchParams, useNavigate } from 'react-router';
import { ArrowDownIcon, ArrowUpIcon } from '@heroicons/react/24/solid';

type CurrencyType = { charCode: string; value: number; icon: JSX.Element };

type ReturnType = {
    currencies: CurrencyType[];
};

const fetchCurrencies = async (searchParams: string | null) => {
    if (searchParams === null) return [];

    const res = await fetch('https://www.cbr-xml-daily.ru/daily_json.js');

    if (res.ok) {
        const json = await res.json();

        return searchParams
            .split(',')
            .map((currency) => {
                const info = json?.Valute[currency];

                if (info) {
                    const { Value, Previous, CharCode } = info;

                    const icon =
                        Value - Previous >= 0 ? (
                            <ArrowUpIcon className="h-10 w-10 text-green-500" />
                        ) : (
                            <ArrowDownIcon className="h-10 w-10 text-red-500" />
                        );

                    return { charCode: CharCode, value: Value.toFixed(2), icon };
                }
            })
            .filter((value) => !!value);
    } else {
        console.log('Ошибка HTTP: ' + res.status);
        return [];
    }
};

export const useCurrencyWidget = (): ReturnType => {
    const [currencies, setCurrencies] = useState<CurrencyType[]>([]);

    const [searchParams] = useSearchParams();

    const navigate = useNavigate();

    useEffect(() => {
        if (!searchParams.has('currencies')) {
            const currencies = ['USD', 'EUR', 'CNY'];
            const queryString = `?currencies=${currencies.join(',')}`;
            navigate(queryString, { replace: true });
            return;
        }
    }, [navigate, searchParams]);

    useEffect(() => {
        const fetch = () => {
            const params = searchParams.get('currencies');

            fetchCurrencies(params).then((currencies) => {
                setCurrencies(currencies);
            });
        };

        fetch();

        const interval = setInterval(fetch, 3 * 3600_000);

        return () => {
            clearInterval(interval);
        };
    }, [searchParams]);

    return { currencies };
};
