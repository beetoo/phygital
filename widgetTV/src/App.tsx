import { BrowserRouter } from 'react-router';

import TimeWidgetTV from './widgets/TimeWidgetTV';

function App() {
    return (
        <BrowserRouter>
            <TimeWidgetTV />
        </BrowserRouter>
    );
}

export default App;
