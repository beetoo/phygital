import React from 'react';
import { Link } from 'react-router';
import { ClockIcon, CurrencyDollarIcon, CloudIcon } from '@heroicons/react/24/outline';

const WidgetsContainer: React.FC = () => {
    return (
        <div className="min-h-screen bg-gradient-to-r from-gray-100 via-gray-200 to-gray-300 flex flex-col justify-center items-center">
            <nav className="p-8 mb-8 w-full max-w-md">
                <ul className="flex flex-col space-y-6 items-center">
                    <li className="w-full">
                        <Link
                            to="/time"
                            className="flex items-center justify-center w-full px-6 py-3 bg-white text-gray-800 rounded-lg shadow-md hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-gray-400 text-3xl"
                        >
                            <ClockIcon className="h-10 w-10 mr-3" />
                            Виджет времени
                        </Link>
                    </li>
                    <li className="w-full">
                        <Link
                            to="/time-tv"
                            className="flex items-center justify-center w-full px-6 py-3 bg-white text-gray-800 rounded-lg shadow-md hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-gray-400 text-3xl"
                        >
                            <ClockIcon className="h-10 w-10 mr-3" />
                            Виджет времени TV
                        </Link>
                    </li>
                    <li className="w-full">
                        <Link
                            to="/weather"
                            className="flex items-center justify-center w-full px-6 py-3 bg-white text-gray-800 rounded-lg shadow-md hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-gray-400 text-3xl"
                        >
                            <CloudIcon className="h-10 w-10 mr-3" />
                            Виджет погоды
                        </Link>
                    </li>
                    <li className="w-full">
                        <Link
                            to="/weather2"
                            className="flex items-center justify-center w-full px-6 py-3 bg-white text-gray-800 rounded-lg shadow-md hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-gray-400 text-3xl"
                        >
                            <CloudIcon className="h-10 w-10 mr-3" />
                            Виджет погоды 2
                        </Link>
                    </li>
                    <li className="w-full">
                        <Link
                            to="/currency"
                            className="flex items-center justify-center w-full px-6 py-3 bg-white text-gray-800 rounded-lg shadow-md hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-gray-400 text-3xl"
                        >
                            <CurrencyDollarIcon className="h-10 w-10 mr-3" />
                            Виджет курса валют
                        </Link>
                    </li>
                    <li className="w-full">
                        <Link
                            to="/responsive"
                            className="flex items-center justify-center w-full px-6 py-3 bg-white text-gray-800 rounded-lg shadow-md hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-gray-400 text-3xl"
                        >
                            <CurrencyDollarIcon className="h-10 w-10 mr-3" />
                            Responsive
                        </Link>
                    </li>
                </ul>
            </nav>
        </div>
    );
};

export default WidgetsContainer;
