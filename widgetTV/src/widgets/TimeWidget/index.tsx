import React from 'react';
import { useSearchParams } from 'react-router';

import { useTimeWidget } from '../../hooks/useTimeWidget';

const TimeWidget: React.FC = () => {
    const { hours, minutes, seconds } = useTimeWidget();

    const [searchParams] = useSearchParams({ h: '10' });

    const height = searchParams.get('h');

    return (
        <div
            className="flex items-center justify-center bg-gray-900"
            style={{
                height: `${height}%`,
                fontSize: `calc(${height}vh * 0.4)`,
            }}
        >
            <div className="text-white font-bold">
                <span className="inline-block">{hours}</span>:<span className="inline-block">{minutes}</span>:
                <span className="inline-block">{seconds}</span>
            </div>
        </div>
    );
};

export default TimeWidget;
