import React from 'react';

import { useCurrencyWidget } from '../../hooks/useCurrencyWidget';

const CurrencyWidget: React.FC = () => {
    const { currencies } = useCurrencyWidget();

    return (
        <div className="flex items-center justify-center min-h-screen bg-gray-900">
            {currencies.length > 0 ? (
                <div className="text-white text-center w-full px-4 md:px-0">
                    <ul className="space-y-6 md:space-y-8">
                        {currencies.map((currency, index) => (
                            <li
                                key={index}
                                className="flex items-center justify-center bg-gray-800 p-6 md:p-8 rounded-lg shadow-xl w-full max-w-2xl md:max-w-4xl mx-auto"
                            >
                                <div className="flex items-center space-x-4 md:space-x-6">
                                    <span className="text-3xl md:text-4xl font-bold">{currency.charCode}</span>
                                    <span className="text-2xl md:text-3xl text-gray-300">{currency.value}</span>
                                    {currency.icon}
                                </div>
                            </li>
                        ))}
                    </ul>
                </div>
            ) : null}
        </div>
    );
};

export default CurrencyWidget;
