import { CloudIcon, SunIcon } from '@heroicons/react/24/outline';

export const getWeatherIcon = (icon: string) => {
    switch (icon) {
        case 'ovc':
            return <CloudIcon className="h-16 w-16 text-gray-500" />;
        case 'clear':
            return <SunIcon className="h-16 w-16 text-yellow-500" />;
        // Добавьте другие случаи для различных иконок
        default:
            return <CloudIcon className="h-16 w-16 text-gray-500" />;
    }
};

const weatherConditions = [
    { condition: 'clear', rus: 'Ясная погода' },
    { condition: 'partly_cloudy', rus: 'Небольшая облачность' },
    { condition: 'cloudy', rus: 'Облачно' },
    { condition: 'overcast', rus: 'Пасмурно' },
    { condition: 'light_rain', rus: 'Небольшой дождь' },
    { condition: 'rain', rus: 'Дождь' },
    { condition: 'heavy_rain', rus: 'Сильный дождь' },
    { condition: 'sleet', rus: 'Снег с дождём' },
    { condition: 'light_snow', rus: 'Небольшой снег' },
    { condition: 'snow', rus: 'Снег' },
    { condition: 'snowfall', rus: 'Снегопад' },
    { condition: 'hail', rus: 'Град' },
    { condition: 'thunderstorm', rus: 'Гроза' },
    { condition: 'thunderstorm_with_rain', rus: 'Гроза с дождём' },
    { condition: 'thunderstorm_with_hail', rus: 'Гроза с градом' },
];

export const getWeatherCondition = (condition: string) => {
    return weatherConditions.find((value) => value.condition === condition)?.rus ?? '';
};
