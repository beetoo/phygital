import { useWeatherWidget } from '../../hooks/useWeatherWidget';
import { getWeatherCondition } from './helpers.tsx';

const WeatherWidget = () => {
    const { icon, temperature, condition, alertTitle, hasData } = useWeatherWidget();

    return (
        <div className="w-full h-screen flex flex-col items-center justify-center bg-gradient-to-b from-blue-400 to-blue-200 text-white p-10">
            {hasData ? (
                <>
                    <div className="flex items-center">
                        {icon}
                        <p className="text-6xl ml-8 drop-shadow-lg">{temperature}°C</p>
                    </div>
                    <p className="text-4xl mt-6 drop-shadow-lg">{getWeatherCondition(condition)}</p>
                    <div className="mt-10 bg-white bg-opacity-80 text-gray-800 p-8 rounded-xl shadow-xl w-3/4 text-center">
                        <p className="text-3xl">{alertTitle}</p>
                    </div>
                </>
            ) : null}
        </div>
    );
};

export default WeatherWidget;
