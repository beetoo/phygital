import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// const STAGE = process.env.VITE_STAGE;

// https://vite.dev/config/
export default defineConfig(() => {
    // const base = STAGE === 'local' ? '/widgets' : undefined;

    return {
        plugins: [react()],
        base: '',
        server: { port: 9000, open: true },
        define: {
            'process.env.VITE_STAGE': JSON.stringify(process.env.STAGE),
        },
    };
});
