#!/bin/bash

if curl -s ifconfig.me | grep -o "158.160.6.64" >/dev/null; then
    npm i

    VITE_STAGE=dev tsc -b && VITE_STAGE=dev vite build

elif curl -s ifconfig.me | grep -o "158.160.11.46" >/dev/null; then
    npm i

    VITE_STAGE=prod tsc -b && VITE_STAGE=prod vite build
fi
