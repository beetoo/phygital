const axios = require('axios');
const fs = require('fs');
const csvParser = require('csv-parser');

const apiUrl = 'https://dev.phygitalsignage.io/api/smil';
const headersTemplate = {
    versionname: '6.1.1',
    versioncode: '50',
    manufacturer: 'DIGMA',
    model: 'CITI 1314C 4G CS1275PL',
    osversion: '13',
    clid: '220fb62c-e77c-4763-b8a2-521b23d331f3',
    totalbytes: '24122490880',
    availablebytes: '22036815872',
    'accept-charset': 'UTF-8',
    accept: '*/*',
    'user-agent': 'Ktor client',
    'accept-encoding': 'gzip',
};

const readScreenIds = async (filePath) => {
    const screenIds = [];
    return new Promise((resolve, reject) => {
        fs.createReadStream(filePath)
            .pipe(csvParser())
            .on('data', (row) => {
                screenIds.push(row.ID);
            })
            .on('end', () => {
                resolve(screenIds);
            })
            .on('error', (error) => {
                reject(error);
            });
    });
};

const sendHeadRequest = async (id) => {
    const headers = {
        ...headersTemplate,
        identification: id,
    };

    try {
        await axios.head(apiUrl, { headers });
        console.log(`Отправлено для: ${id}`);
    } catch (error) {
        console.error(`Ошибка запроса ${id}: ${error.message}`);
    }
};

const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const processRequests = async () => {
    const screenIds = await readScreenIds('screen_ids.csv');

    while (true) {
        console.log(`Отправляем ${screenIds.length} HEAD`);

        const requests = screenIds.map((id) => sendHeadRequest(id));

        await Promise.all(requests);

        console.log(`HEAD отправлены`);

        await delay(1000);
    }
};

processRequests();
