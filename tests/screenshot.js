const autocannon = require('autocannon');
const path = require('path');

const filePath = path.join(__dirname, 'image.jpg');

const instance = autocannon(
    {
        url: 'https://dev.phygitalsignage.io/api/smil/screenshot',
        connections: 200,
        pipelining: 1,
        duration: 120,
        headers: {
            identification: '0c29c5b5-f190-4e35-b2d9-3e7f49ee847a',
        },
        method: 'POST',
        form: {
            file: {
                type: 'file',
                path: filePath,
            },
        },
    },
    console.log,
);

process.once('SIGINT', () => {
    instance.stop();
});

autocannon.track(instance, { renderProgressBar: true });
