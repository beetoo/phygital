const axios = require('axios');
const { smilOne, smilTwo } = require('./data');

async function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

const axiosInstance = axios.create({
    baseURL: 'http://localhost:3000',
    withCredentials: true,
    headers: {
        authorization:
            'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImU1NmM0N2I3LWI0NDUtNDNiNS1hNTA3LWM5NjM5YzU3MjFiMyIsInVzZXJJZCI6ImUyOTdiMTA1LThlZTctNGEwOS05MzhjLTBlZWMyYmJjMzRkNyIsImlhdCI6MTcyNjI5MjA4OCwiZXhwIjoxNzI4ODg0MDg4fQ.VR5Yyfxc4LlQ3U7JW2UV-zzuCwVD3mBI1qHX62Fw770',
    },
});

const locationId = '6e543d66-0bd8-4e2e-a262-dd97a01f6117';
const screenOne = { id: '1727a582-b489-40ae-b9b8-a99b08cd20b6', identification: '79com10nh4de' };
const screenTwo = { id: '83fa5def-63f8-4951-b113-79f9caca2b74', identification: '72dom14p3trf' };
const contentId = '539d3757-52be-4ccf-87fe-72568ded92af';

const getLastModified = async (identification) => {
    const response = await axiosInstance.head('api/smil', { headers: { identification } });

    return response.headers['last-modified'];
};

const checkLastModified = async (prevLastModified, text, successIfModified = true) => {
    const lastModified = await getLastModified(screenOne.identification);

    // console.log(prevLastModified, updatedLastModified);

    const isModified = prevLastModified !== lastModified;

    const modifiedText = isModified ? 'lastModified изменился.' : 'lastModified не изменился.';
    console.log(text + ':', modifiedText);

    await sleep(1000);

    return { lastModified, success: successIfModified.toString() === isModified.toString() };
};

const updateScreenSmil = async (screenId, smil) => {
    const response = await axiosInstance.patch(`api/screen/${screenId}`, { smil });
    return response.data;
};

const updateScreenUrlParams = async (screenId, urlParams) => {
    const response = await axiosInstance.patch(`api/screen/${screenId}`, { urlParams });
    return response.data;
};

const updateLocationUrlParams = async (locationId, urlParams) => {
    const response = await axiosInstance.patch(`api/location/${locationId}`, { urlParams });
    return response.data;
};

const updateContentUrlParamMode = async (contentId, urlParamMode) => {
    const response = await axiosInstance.patch(`api/content/${contentId}`, { urlParamMode });
    return response.data;
};

// 1) Экран привязан к плейлисту

let isSuccessfully = false;

async function main() {
    let initialLastModified = await getLastModified(screenOne.identification);

    console.log();

    // 1)
    await updateScreenSmil(screenOne.id, smilOne);
    let data = await checkLastModified(initialLastModified, 'Добавляем смил к экрану');

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateScreenSmil(screenOne.id, smilTwo);
    data = await checkLastModified(data.lastModified, 'Обновляем смил у экрана');

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateScreenSmil(screenOne.id, null);
    data = await checkLastModified(data.lastModified, 'Обнуляем смил у экрана');

    isSuccessfully = data.success;

    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateScreenSmil(screenOne.id, null);
    data = await checkLastModified(data.lastModified, 'Повторяем обнуление смила для экрана', false);

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    console.log();

    // 2)
    await updateScreenUrlParams(screenOne.id, { companyId: '' });
    data = await checkLastModified(data.lastModified, 'Отправляем идентичные urlParams для экрана', false);

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateScreenUrlParams(screenOne.id, { companyId: '1' });
    data = await checkLastModified(data.lastModified, 'Изменяем urlParams у экрана');

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateScreenUrlParams(screenOne.id, { companyId: '' });
    data = await checkLastModified(data.lastModified, 'Изменяем urlParams у экрана');

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    console.log();

    // 3)
    await updateScreenUrlParams(screenTwo.id, { companyId: '' });
    data = await checkLastModified(data.lastModified, 'Отправляем идентичные urlParams для второго экрана', false);

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateScreenUrlParams(screenTwo.id, { companyId: '1' });
    data = await checkLastModified(data.lastModified, 'Изменяем urlParams у второго экрана', false);

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateScreenUrlParams(screenTwo.id, { companyId: '' });
    data = await checkLastModified(data.lastModified, 'Изменяем urlParams у второго экрана', false);

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    console.log();

    // 4)
    await updateLocationUrlParams(locationId, { cityId: '' });
    data = await checkLastModified(data.lastModified, 'Отправляем идентичные urlParams для локации', false);

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateLocationUrlParams(locationId, { cityId: '1' });
    data = await checkLastModified(data.lastModified, 'Изменяем urlParams у локации');

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateLocationUrlParams(locationId, { cityId: '' });
    data = await checkLastModified(data.lastModified, 'Изменяем urlParams у локации');

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    console.log();

    // 5)
    await updateContentUrlParamMode(contentId, 'none');
    data = await checkLastModified(data.lastModified, 'Отправляем идентичный urlParamMode для веб-страницы', false);

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateContentUrlParamMode(contentId, 'all');
    data = await checkLastModified(data.lastModified, 'Изменяем urlParamMode у веб-страницы');

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateContentUrlParamMode(contentId, 'location');
    data = await checkLastModified(data.lastModified, 'Изменяем urlParamMode у веб-страницы');

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    console.log();

    // 6)
    await updateContentUrlParamMode(contentId, 'none');
    data = await checkLastModified(data.lastModified, 'Изменяем urlParamMode у веб-страницы на none');

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateScreenUrlParams(screenOne.id, { cityId: '1' });
    data = await checkLastModified(data.lastModified, 'Изменяем urlParamMode у веб-страницы при значении none', false);

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateScreenUrlParams(screenOne.id, { cityId: '' });
    data = await checkLastModified(data.lastModified, 'Изменяем urlParamMode у веб-страницы при значении none', false);

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    console.log();

    await updateContentUrlParamMode(contentId, 'all');
    data = await checkLastModified(data.lastModified, 'Изменяем urlParamMode у веб-страницы');

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateScreenUrlParams(screenOne.id, { cityId: '1' });
    data = await checkLastModified(data.lastModified, 'Изменяем urlParamMode у веб-страницы при значении all');

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    await updateScreenUrlParams(screenOne.id, { cityId: '' });
    data = await checkLastModified(data.lastModified, 'Изменяем urlParamMode у веб-страницы при значении all');

    isSuccessfully = data.success;
    if (!isSuccessfully) {
        console.log('Тест окончился неудачей');
    }

    // await updateContentUrlParamMode(contentId, 'none');
    // data = await checkLastModified(data.lastModified, 'Изменяем urlParamMode у веб-страницы');
    //
    // isSuccessfully = data.success;
    // if (!isSuccessfully) {
    //     console.log('Тест окончился неудачей');
    // }

    data = await checkLastModified(data.lastModified, 'Просто проверка', false);

    console.log();

    console.log(isSuccessfully ? 'Тест пройден успешно!' : 'Тест окончился неудачей!');
}

main();
