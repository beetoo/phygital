const autocannon = require('autocannon');

const instance = autocannon(
    {
        url: 'http://localhost:3000/api/smil',
        method: 'GET',
        connections: 200,
        pipelining: 10,
        duration: 30,
        headers: {
            identification: '4n08m1hayq1a',
            authorization:
                'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImU1NmM0N2I3LWI0NDUtNDNiNS1hNTA3LWM5NjM5YzU3MjFiMyIsInVzZXJJZCI6ImUyOTdiMTA1LThlZTctNGEwOS05MzhjLTBlZWMyYmJjMzRkNyIsImlhdCI6MTcyNzI0MzYzNywiZXhwIjoxNzI5ODM1NjM3fQ.qb0BrmDPUIvHK1zpXuoA5dbFp1ZqXBK5eSQYmINtW8c',
        },
    },
    (err, data) => console.log(data.statusCodeStats),
);

process.once('SIGINT', () => {
    instance.stop();
});

autocannon.track(instance, { renderProgressBar: true });
