const smilOne = `<smil>

    <head>
        <meta http-equiv="refresh" content="60"/>
        <meta log="true"/>
        <meta onlySmilUpdate="true"/>
        <meta orientation="LANDSCAPE"/>


        <layout>

            <root-layout width="1920" height="1080" backgroundColor="#FFFFFF" />
            <region regionName="main" left="0" top="0" width="100%" height="100%" />

        </layout>
    </head>

    <body>
        <par>
            <seq end="__prefetchEnd.endEvent">
            <seq repeatCount="indefinite">
                <video src="http://localhost:3000/api/static/smil/progress.mp4" />
            </seq>
        </seq>
            <seq>
            <prefetch src="http://localhost:3000/api/static/smil/holderimage.jpg" />
            <seq id="__prefetchEnd" dur="1s" />
        </seq>
            <par begin="__prefetchEnd.endEvent" repeatCount="indefinite">
                <seq repeatCount="indefinite">
                     <img src="http://localhost:3000/api/static/content/3191352d-4373-4b26-8e81-ced3eac3172d/girl.jpg" dur="20" region="main"></img>
                </seq>
        </par>
        </par>
    </body>

</smil>`;

const smilTwo = `<smil>

    <head>
        <meta http-equiv="refresh" content="60"/>
        <meta log="true"/>
        <meta onlySmilUpdate="true"/>
        <meta orientation="LANDSCAPE"/>


        <layout>

            <root-layout width="1920" height="1080" backgroundColor="#FFFFFF" />
            <region regionName="main" left="0" top="0" width="100%" height="100%" />

        </layout>
    </head>

    <body>
        <par>
            <seq end="__prefetchEnd.endEvent">
            <seq repeatCount="indefinite">
                <video src="http://localhost:3000/api/static/smil/progress.mp4" />
            </seq>
        </seq>
            <seq>
            <prefetch src="http://localhost:3000/api/static/smil/holderimage.jpg" />
            <seq id="__prefetchEnd" dur="1s" />
        </seq>
            <par begin="__prefetchEnd.endEvent" repeatCount="indefinite">
                <seq repeatCount="indefinite">
                     <img src="http://localhost:3003/api/static/content/3191352d-4373-4b26-8e81-ced3eac3172d/girl.jpg" dur="20" region="main"></img>
                </seq>
        </par>
        </par>
    </body>

</smil>`;

exports.smilOne = smilOne;
exports.smilTwo = smilTwo;
