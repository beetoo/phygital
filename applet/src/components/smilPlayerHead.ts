/* tslint:disable:Unnecessary semicolon missing whitespace */
// import { defaults as config } from '../../config/parameters';
import sos from '@signageos/front-applet';
// @ts-ignore
import backupImage from '../../public/backupImage/backupImage.jpg';
import { debug } from './smilPlayerTools';
const MD5 = require('md5.js')


export async function generateHeadIdentification() {

	function generateClientUuid(model: string, serialNumber: string, ethernetMac: string, wifiMac: string): string {
		let key = model + serialNumber + ethernetMac + wifiMac;
		return new MD5().update(key.toUpperCase()).digest('hex');
	}

	debug('Generated generateIdentification');

	let model = '';
	try {
		model = await sos.management.getModel()
	} catch (error) {
		debug('Unexpected error : %O', error);
	}

	let serialNumber = '';
	try {
		serialNumber = await sos.management.getSerialNumber();
	} catch (error) {
		debug('Unexpected error : %O', error);
	}

	let ethernetMac = '';
	let wifiMac = '';

	try {
		const regexMac = /^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/;
		const networkInfo = await sos.management.network.getActiveInfo();
		if (regexMac.test(networkInfo.ethernetMacAddress)){
			ethernetMac = networkInfo.ethernetMacAddress
		}
		if (regexMac.test(networkInfo.wifiMacAddress)){
			wifiMac = networkInfo.wifiMacAddress
		}
	} catch (error) {
		debug('Unexpected error : %O', error);
	}

	const clientUuid = generateClientUuid(model, serialNumber, ethernetMac, wifiMac);
	let uuid = '';
	if (sos.config.identification){
		uuid = sos.config.identification;
	} else {
		uuid = clientUuid;
	}

	debug('Generated generateIdentification %O', uuid);
	return {
		model: model,
		serialNumber: serialNumber,
		ethernetMac: ethernetMac,
		wifiMac: wifiMac,
		identification: uuid,
		clientIdentification: clientUuid
	};
}

