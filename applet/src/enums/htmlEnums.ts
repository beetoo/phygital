export enum HtmlEnum {
	img = 'img',
	ref = 'iframe',
	top = 'top',
	bottom = 'bottom',
	right = 'right',
	width = 'width',
	height = 'height',
	left = 'left',
	widgetRoot = '/index.html',
	video = 'video',
	transitionStyleId = 'transitions',
	zIndex = 'z-index',
	widgetAllow = 'geolocation *; microphone *; camera *; midi *; encrypted-media *; autoplay *;',
}

export enum ObjectFitEnum {
	fill = 'fill',
	meet = 'contain',
	meetBest = 'contain',
	cover = 'cover',
	objectFit = 'object-fit',
}
