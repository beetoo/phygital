export type SuccessStatus = {
	status: 'upload_success',
};

export type ErrorStatus = {
	status: 'upload_error',
	reason: string,
	errorMessage: string,
};

export type Status = SuccessStatus | ErrorStatus;
