# SignageOS Applet Synchronizer


Backend application that serves as a middle man for synchronizing content playback across multiple devices running [SignageOS](https://signageos.io) system.

## How it works

Applet synchronizer is using websockets to broadcast important information to all connected devices.

## Help

For more information please read the synchronization chapter in our [documentation](https://docs.signageos.io/api/sos-applet-api/#Sync_playback_across_multiple_devices).

If you have any additional question, please contact our support at [support@signageos.io](mailto:support@signageos.io).
