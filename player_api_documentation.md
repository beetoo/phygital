1)

/api/smil

HEAD - Получение информации о времени, когда последний раз обновлялся плейлист клиентом

Запрос:
  Headers:
    identification: string; // идентификатор экрана
    availableBytes: string; // доступный объём хранилища на экране
    totalBytes: string; // общий объём хранилища на экране

Ответ: время последнего обновления плейлиста
Headers: Last-Modified: Date

2)

/api/smil

GET - Получение актуального smil для данного экрана

Запрос:
  Headers:
    identification: string; // идентификатор экрана
    versionCode: string; // версия плеера на данном экране

Ответ: smil в виде строки
string

3)

/api/smil/cachecontent

GET - запрос информации, какие файлы нужно кешировать для данного экрана

Запрос:
  Headers:
    identification: string; // идентификатор экрана

Ответ: smil в виде строки
string

4)

/api/smil/status

POST - отправка текущего статуса экрана

Запрос:
  Headers:
    identification: string; // идентификатор экрана
    versionCode: string; // версия плеера на данном экране
  Body:
    status: 'upload_success' | 'upload_error'
    reason: 'DOWNLOAD'; // если 'upload_error'
    error?: string; // если 'upload_error'

Ответ: пустой
{ message: 'OK' }

5)

/api/smil/screenshot

POST - загрузка на сервер актуального скриншота данного экрана

Запрос:
  Headers:
    identification: string; // идентификатор экрана
  Body:
    file: File;

Ответ: id экрана
{ screenId: string }
