// const maxDevices = Number.MAX_SAFE_INTEGER;
// const expiredDate = '2100.12.31';

const maxDevices = 3;
const expiredDate = '2124.04.11';

const xorMap = (str: string, mode: 'encoding' | 'decoding', key = 678904) => {
    str = mode === 'encoding' ? btoa(str) : str;
    const xor = (char: string) => {
        const charCode = char.charCodeAt(0);
        const xorNumber = charCode ^ key;
        return String.fromCharCode(xorNumber);
    };

    const result = str.split('').map(xor).join('');

    return mode === 'decoding' ? atob(result) : result;
};

const maxDevicesEnc = xorMap(maxDevices.toString(), 'encoding');
const expiredDateEnc = xorMap(expiredDate, 'encoding');

console.log(`SL=${maxDevicesEnc}:${expiredDateEnc}`);
