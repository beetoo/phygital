// eslint-disable-next-line @typescript-eslint/no-var-requires
const TerserPlugin = require('terser-webpack-plugin');

module.exports = function (options) {
    return {
        ...options,
        optimization: {
            ...options.optimization,
            minimize: true,
            minimizer: [
                new TerserPlugin({
                    extractComments: false,
                    terserOptions: {
                        mangle: true,
                        keep_classnames: true,
                        // keep_fnames: true,
                    },
                }),
            ],
        },
        performance: {
            hints: false,
        },
    };
};
