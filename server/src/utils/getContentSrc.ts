import { CONTENT_STATIC_URL } from '../constants';

export const getContentSrc = (id: string, filename: string) => {
    return `${CONTENT_STATIC_URL}/${id}/${filename}`;
};
