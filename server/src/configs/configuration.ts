import { CLIENT_API_URL, SERVER_API_URL, UPLOAD_PATH, SYNC_SERVER_URL } from '../constants';

export default () => ({
    SERVER_API_URL,
    CLIENT_API_URL,
    UPLOAD_PATH,
    SYNC_SERVER_URL,
});
