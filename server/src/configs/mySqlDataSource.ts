import { DataSource } from 'typeorm';

import { MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_HOST, isLocalDevelopmentServer } from '../constants';

export default new DataSource({
    type: 'mysql',
    host: MYSQL_HOST,
    port: isLocalDevelopmentServer ? 3307 : 3306,
    username: MYSQL_USER, // ENV MYSQL_USER
    password: MYSQL_PASSWORD, // ENV MYSQL_PASSWORD
    database: MYSQL_DATABASE, // ENV MYSQL_DATABASE
    entities: [],
    synchronize: false, // isDevelopmentServer
    migrationsRun: false,
    migrations: [],
});
