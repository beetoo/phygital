import { TypeOrmModuleOptions } from '@nestjs/typeorm';

import { MYSQL_DATABASE, MYSQL_HOST, MYSQL_PASSWORD, MYSQL_USER, isLocalDevelopmentServer } from '../constants';
import { Organization } from '../models/organization/entities/organization.entity';
import { ScreenReg } from '../models/admin/screen-reg/entities/screen-reg.entity';
import { ScreenHistory } from '../models/screen/entities/screen-history.entity';
import { ScreenRequestLogger } from '../models/admin/screen-request-logger/entities/screen-request-logger.entity';
import { Screen } from '../models/screen/entities/screen.entity';
import { Location } from '../models/location/entities/location.entity';
import { Playlist } from '../models/playlist/entities/playlist.entity';
import { PlaylistItem } from '../models/playlist/entities/playlist-item.entity';
import { Scenario } from '../models/scenario/entities/scenario.entity';
import { ScenarioRule } from '../models/scenario/entities/scenario-rule.entity';
import { Content } from '../models/content/entities/content.entity';
import { Folder } from '../models/folder/entities/folder.entity';
import { User } from '../models/user/entities/user.entity';
import { UserOrganizationProperty } from '../models/user/entities/user-organization-property.entity';
import { UserActivityLog } from '../models/user/entities/user-activity-log.entity';
import { OrganizationInvitation } from '../models/organization-invitation/entities/organization.invitation';
import { Template } from '../models/template/entities/template.entity';
import { SupportTicket } from '../models/support-ticket/entities/support-ticket.entity';
import { SupportTicketMessage } from '../models/support-ticket/entities/support-ticket-message.entity';
import { Payment } from '../models/payment/entities/payment.entity';

export const mysqlTypeOrmOptions: TypeOrmModuleOptions = {
    type: 'mysql',
    host: MYSQL_HOST,
    port: isLocalDevelopmentServer ? 3307 : 3306,
    username: MYSQL_USER, // ENV MYSQL_USER
    password: MYSQL_PASSWORD, // ENV MYSQL_PASSWORD
    database: MYSQL_DATABASE, // ENV MYSQL_DATABASE
    autoLoadEntities: false,
    entities: [
        Organization,
        OrganizationInvitation,
        User,
        UserOrganizationProperty,
        UserActivityLog,
        Payment,

        Playlist,
        Template,

        Screen,
        Location,

        PlaylistItem,
        ScreenReg,
        ScreenHistory,
        Content,
        Folder,
        ScreenRequestLogger,
        Scenario,
        ScenarioRule,

        SupportTicket,
        SupportTicketMessage,
    ],
    synchronize: true, // isDevelopmentServer && process.env.synchronize !== 'false',
    retryDelay: 30000,
    retryAttempts: 10,
    // cache: true,
};
