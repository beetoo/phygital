import { MigrationInterface, QueryRunner } from 'typeorm';

export class PlaylistUploadUniqueIdx1639932199153 implements MigrationInterface {
    public up(queryRunner: QueryRunner): Promise<void> {
        return queryRunner.query(
            `CREATE UNIQUE INDEX "IDX_playlist_upload_unique_1639932199153" ON "playlist_upload" (
                        "playlistId",
                        "screenId"
                        );`,
        );
    }

    public down(queryRunner: QueryRunner): Promise<void> {
        return queryRunner.query('drop index if exists IDX_playlist_upload_unique_1639932199153');
    }
}
