# Create migration

`node --require ts-node/register ./node_modules/typeorm/cli.js --config server/src/configs/mainDbConfig.ts migration:create -n YourMigrationName`

`typeorm --config server/src/configs/mainDbConfig.ts migration:create -n ./server/src/migration/TestMigration`

Don't forget to register migration: `migrations: [PlaylistUploadUniqueIdx1639932199153]` at `dbConfig.ts` to be registered

# Run migration locally to test

`npm run typeorm migration:run`
