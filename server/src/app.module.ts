import fs from 'node:fs';
import { MailerModule } from '@nestjs-modules/mailer';
import { CacheModule } from '@nestjs/cache-manager';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { ServeStaticModule } from '@nestjs/serve-static';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { redisStore } from 'cache-manager-redis-yet';
import { RedisClientOptions } from 'redis';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import configuration from './configs/configuration';
import {
    EMAIL_ACCOUNT_LOGIN,
    EMAIL_ACCOUNT_PASSWORD,
    REDIS_HOST,
    REDIS_PORT,
    REDIS_PASSWORD,
    UPLOAD_PATH,
    UPLOAD_PATH_CONTENT,
    UPLOAD_PATH_SCREENSHOT,
} from './constants';
import { mysqlTypeOrmOptions } from './configs/mySqlTypeOrmOptions';
import { AuthModule } from './models/auth/auth.module';
import { ContentModule } from './models/content/content.module';
import { FolderModule } from './models/folder/folder.module';
import { LocationModule } from './models/location/location.module';
import { Organization } from './models/organization/entities/organization.entity';
import { OrganizationModule } from './models/organization/organization.module';
import { PlaylistModule } from './models/playlist/playlist.module';
import { Screen } from './models/screen/entities/screen.entity';
import { ScreenModule } from './models/screen/screen.module';
import { SmilModule } from './models/smil/smil.module';
import { EmailService } from './services/email.service';
import { CronMonitoringService } from './services/cron-monitoring.service';
import { AuthGuardModule } from './guards/auth-guard/auth-guard.module';
import { Playlist } from './models/playlist/entities/playlist.entity';
import { Location } from './models/location/entities/location.entity';
import { TestStatusService } from './services/test-status.service';
import { EventsService } from './services/events.service';
import { ScreenHistory } from './models/screen/entities/screen-history.entity';
import { UserModule } from './models/user/user.module';
import { JwtConfigService } from './configs/jwt-config-service';
import { User } from './models/user/entities/user.entity';
import { AdminModule } from './models/admin/admin.module';
import { UserActivityLogEventsService } from './services/user-activity-log-events.service';
import { UserActivityLog } from './models/user/entities/user-activity-log.entity';
import { DbModule } from './services/db.module';
import { ScenarioModule } from './models/scenario/scenario.module';
import { TelegramBotService } from './services/telegram-bot.service';
import { PlayerModule } from './models/player/player.module';
import { Content } from './models/content/entities/content.entity';
import { OrganizationInvitationModule } from './models/organization-invitation/organization-invitation.module';
import { LoggerMiddleware } from './middlewares/logger.middleware';
import { AdminController } from './models/admin/admin.controller';
import { ContentController } from './models/content/content.controller';
import { FolderController } from './models/folder/folder.controller';
import { LocationController } from './models/location/location.controller';
import { OrganizationController } from './models/organization/organization.controller';
import { OrganizationInvitationController } from './models/organization-invitation/organization-invitation.controller';
import { PlaylistController } from './models/playlist/playlist.controller';
import { ScenarioController } from './models/scenario/scenario.controller';
import { ScreenController } from './models/screen/screen.controller';
import { UserController } from './models/user/user.controller';
import { UserSessionsService } from './services/user-sessions.service';
import { UserSessionsMiddleware } from './middlewares/user-sessions.middleware';
import { ValidationModule } from './services/validation/validation.module';
import { TemplateModule } from './models/template/template.module';
import { Template } from './models/template/entities/template.entity';
import { SupportTicketModule } from './models/support-ticket/support-ticket.module';
import { SupportTicketMessage } from './models/support-ticket/entities/support-ticket-message.entity';
import { SupportTicketMessageController } from './models/support-ticket/support-ticket-message.controller';
import { SupportTicketMessageService } from './models/support-ticket/support-ticket-message.service';
import { PaymentModule } from './models/payment/payment.module';
import { SupportTicketController } from './models/support-ticket/support-ticket.controller';
import { PaymentController } from './models/payment/payment.controller';

@Module({
    imports: [
        AdminModule,
        AuthModule,
        OrganizationModule,
        UserModule,
        ScreenModule,
        PlaylistModule,
        TemplateModule,
        ScenarioModule,
        ContentModule,
        SmilModule,
        LocationModule,
        FolderModule,
        FolderModule,
        AuthGuardModule,
        DbModule,
        PlayerModule,
        OrganizationInvitationModule,
        ValidationModule,
        SupportTicketModule,
        PaymentModule,

        TypeOrmModule.forRoot(mysqlTypeOrmOptions),
        TypeOrmModule.forFeature([
            Organization,
            User,
            UserActivityLog,
            Screen,
            ScreenHistory,
            Location,
            Playlist,
            Content,
            Template,
            SupportTicketMessage,
        ]),
        CacheModule.register<RedisClientOptions>({
            isGlobal: true,
            store: redisStore,

            // Store-specific configuration:
            username: 'default',
            password: REDIS_PASSWORD,
            socket: {
                host: REDIS_HOST,
                port: REDIS_PORT,
            },
        }),

        // CacheModule.registerAsync({
        //     isGlobal: true,
        //
        //     useFactory: async () => {
        //         return {
        //             stores: [
        //                 new KeyvRedis(
        //                     {
        //                         url: `redis://${REDIS_HOST}:${REDIS_PORT}`,
        //                         username: 'default',
        //                         password: REDIS_PASSWORD,
        //                     },
        //                     { namespace: 'phygital' },
        //                 ),
        //             ],
        //         };
        //     },
        // }),

        ScheduleModule.forRoot(),
        ConfigModule.forRoot({
            isGlobal: true,
            envFilePath: './.env',
            load: [configuration],
        }),
        ServeStaticModule.forRoot({
            serveRoot: '/api/static',
            rootPath: UPLOAD_PATH,
            serveStaticOptions: {
                setHeaders: (res) => {
                    res.set('Access-Control-Allow-Origin', '*');
                    res.set('Access-Control-Allow-Credentials', 'false');
                },
                index: false,
            },
        }),
        MailerModule.forRoot({
            transport: `smtps://${EMAIL_ACCOUNT_LOGIN}:${EMAIL_ACCOUNT_PASSWORD}@smtp.mail.ru`,
            defaults: {
                from: `"Phygital Signage" <${EMAIL_ACCOUNT_LOGIN}>`,
            },
            template: {
                adapter: new HandlebarsAdapter(),
                options: {
                    strict: true,
                },
            },
        }),
        EventEmitterModule.forRoot(),

        JwtModule.registerAsync({
            useClass: JwtConfigService,
        }),
    ],

    controllers: [AppController, SupportTicketMessageController],

    providers: [
        AppService,
        EmailService,
        CronMonitoringService,
        EventsService,
        UserActivityLogEventsService,
        TestStatusService,
        TelegramBotService,
        UserSessionsService,
        SupportTicketMessageService,
        // ScreenRegService,
    ],
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(LoggerMiddleware, UserSessionsMiddleware)
            .forRoutes(
                AppController,
                AdminController,
                ContentController,
                FolderController,
                LocationController,
                OrganizationController,
                OrganizationInvitationController,
                PlaylistController,
                ScenarioController,
                ScreenController,
                UserController,
                SupportTicketController,
                SupportTicketMessageController,
                PaymentController,
                'auth/signout',
            );
    }

    constructor() {
        setTimeout(() => {
            this.createFolders();
        }, 5000);
    }

    private createFolders() {
        // /upload
        if (!fs.existsSync(UPLOAD_PATH)) {
            fs.mkdirSync(UPLOAD_PATH);
        }

        // /upload/content
        if (!fs.existsSync(UPLOAD_PATH_CONTENT)) {
            fs.mkdirSync(UPLOAD_PATH_CONTENT);
        }

        // /upload/screen
        if (!fs.existsSync(UPLOAD_PATH_SCREENSHOT)) {
            fs.mkdirSync(UPLOAD_PATH_SCREENSHOT);
        }
    }
}
