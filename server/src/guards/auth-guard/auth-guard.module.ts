import { Global, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';

import { JwtConfigService } from '../../configs/jwt-config-service';
import { AuthGuardService } from './auth-guard.service';
import { Organization } from '../../models/organization/entities/organization.entity';
import { Playlist } from '../../models/playlist/entities/playlist.entity';
import { Screen } from '../../models/screen/entities/screen.entity';
import { User } from '../../models/user/entities/user.entity';
import { Content } from '../../models/content/entities/content.entity';
import { Location } from '../../models/location/entities/location.entity';
import { Scenario } from '../../models/scenario/entities/scenario.entity';
import { Folder } from '../../models/folder/entities/folder.entity';
import { OrganizationInvitation } from '../../models/organization-invitation/entities/organization.invitation';
import { Template } from '../../models/template/entities/template.entity';
import { SupportTicket } from '../../models/support-ticket/entities/support-ticket.entity';
import { SupportTicketMessage } from '../../models/support-ticket/entities/support-ticket-message.entity';

@Global()
@Module({
    imports: [
        TypeOrmModule.forFeature([
            Organization,
            OrganizationInvitation,
            User,
            Playlist,
            Screen,
            Location,
            Folder,
            Content,
            Scenario,
            Template,
            SupportTicket,
            SupportTicketMessage,
        ]),
        JwtModule.registerAsync({
            useClass: JwtConfigService,
        }),
    ],
    providers: [AuthGuardService],
    exports: [AuthGuardService],
})
export class AuthGuardModule {}
