import { CanActivate, ExecutionContext, Inject, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { isBefore } from 'date-fns';

import { AuthGuardService } from './auth-guard.service';
import { extractTokenFromHeader } from '../../helpers';
import { PUBLIC_KEY } from '../../decorators/public.decorator';
import { ADMIN_KEY } from '../../decorators/admin.decorator';
import { isLocalDevelopmentServer, isProductionServer, SL_EXPIRED_DATE } from '../../constants';
import { ACCOUNTING_KEY } from '../../decorators/accounting.decorator';
import { CHECK_PERMISSION_KEY, ControllerAction } from '../../decorators/check-permission.decorator';
import { VIEW_ONLY_KEY } from '../../decorators/view-only.decorator';
import { DEVELOPMENT_ONLY_KEY } from '../../decorators/development-only.decorator';
import { WITH_USER_FROM_TOKEN_KEY } from '../../decorators/with-user-from-token.decorator';
import { USER_ONLY_KEY } from '../../decorators/user-only.decorator';
import { ADMIN_ONLY_KEY } from '../../decorators/admin-only.decorator';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        @Inject(AuthGuardService) private readonly authGuardService: AuthGuardService,
        private reflector: Reflector,
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        if (!isBefore(new Date(), SL_EXPIRED_DATE)) {
            return false;
        }

        const ctx = context.switchToHttp();
        const request = ctx.getRequest<Request>();

        if (isLocalDevelopmentServer) {
            console.log(request.method, request.url, request.body);
        }

        const isDevelopmentOnly = this.extractMetadata(DEVELOPMENT_ONLY_KEY, context);

        if (isDevelopmentOnly && isProductionServer) {
            return false;
        }

        // проверка наличия организации для /api/organization/:id/*
        await this.authGuardService.checkOrganizationExists(
            request.body.organizationId || request.params.organizationId,
            request.url,
        );

        const isPublic = this.extractMetadata(PUBLIC_KEY, context);

        // /*
        // /smil/*
        // /player/*
        // /auth/*
        if (isPublic) {
            return true;
        }

        const token = extractTokenFromHeader(request);
        const user = await this.authGuardService.getUserFromToken(token);

        if (!user || user.status === 'pending') return false;

        this.authGuardService.updateOrganizationRequestBody(user, request);

        // const onlyRoles = this.extractMetadata(ONLY_ROLE_KEY, context);
        //
        // console.log('onlyRoles', onlyRoles);

        const isUserOnly = this.extractMetadata(USER_ONLY_KEY, context);
        if (isUserOnly && user.role !== 'user') {
            return false;
        }

        const isAdminOnly = this.extractMetadata(ADMIN_ONLY_KEY, context);
        if (isAdminOnly && user.role !== 'admin') {
            return false;
        }

        const isViewOnly = this.extractMetadata(VIEW_ONLY_KEY, context);

        if (isViewOnly && user.role === 'viewonly') {
            return true;
        }

        const isAccounting = this.extractMetadata(ACCOUNTING_KEY, context);

        // /admin/organizations
        // /organization/payments (GET,POST)
        if (isAccounting && user.role === 'accounting') {
            return true;
        }

        const isAdmin = this.extractMetadata(ADMIN_KEY, context);

        // /admin/*
        if (isAdmin && user.role === 'admin') {
            return true;
        }

        if (request.url.startsWith('/api/admin') && user.role === 'user') {
            return false;
        }

        const withUserFromToken = this.extractMetadata(WITH_USER_FROM_TOKEN_KEY, context);

        if (withUserFromToken) {
            request.body = { ...request.body, user };
        }

        const { result, organizationId = '' } = await this.authGuardService.validateByPersonalIds(user, request);

        if (!result) {
            return false;
        }

        const checkedActions = this.extractMetadata<ControllerAction[]>(CHECK_PERMISSION_KEY, context);

        // /organization/*
        // /content/*
        // /folder/*
        // /location/*
        // /playlist/*
        // /scenario/*
        // /screen/*
        // /payments/*
        return this.authGuardService.validateByActions(user, organizationId, checkedActions);
    }

    private extractMetadata<T = boolean>(key: string, context: ExecutionContext) {
        return this.reflector.getAllAndOverride<T>(key, [context.getHandler(), context.getClass()]) ?? false;
    }
}
