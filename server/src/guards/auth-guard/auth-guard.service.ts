import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Request } from 'express';
import { uniq } from 'lodash';

import { Organization } from '../../models/organization/entities/organization.entity';
import { Playlist } from '../../models/playlist/entities/playlist.entity';
import { Screen } from '../../models/screen/entities/screen.entity';
import { Content } from '../../models/content/entities/content.entity';
import { User } from '../../models/user/entities/user.entity';
import { Location } from '../../models/location/entities/location.entity';
import { Scenario } from '../../models/scenario/entities/scenario.entity';
import { Folder } from '../../models/folder/entities/folder.entity';
import { OrganizationInvitation } from '../../models/organization-invitation/entities/organization.invitation';
import { ControllerAction } from '../../decorators/check-permission.decorator';
import { getPermissionsWithGlobalRoles } from '../../helpers/getPermissions';
import { ValidationService } from '../../services/validation/validation.service';
import { Template } from '../../models/template/entities/template.entity';
import { SupportTicket } from '../../models/support-ticket/entities/support-ticket.entity';
import { MessageSenderRoleType } from '../../../../common/types';
import { SupportTicketMessage } from '../../models/support-ticket/entities/support-ticket-message.entity';

@Injectable()
export class AuthGuardService {
    constructor(
        @InjectRepository(Organization)
        private organizationRepository: Repository<Organization>,
        @InjectRepository(OrganizationInvitation)
        private organizationInvitationRepository: Repository<OrganizationInvitation>,
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(Playlist)
        private playlistRepository: Repository<Playlist>,
        @InjectRepository(Template)
        private templateRepository: Repository<Template>,
        @InjectRepository(Scenario)
        private scenarioRepository: Repository<Scenario>,
        @InjectRepository(Screen)
        private screenRepository: Repository<Screen>,
        @InjectRepository(Location)
        private locationRepository: Repository<Location>,
        @InjectRepository(Folder)
        private folderRepository: Repository<Folder>,
        @InjectRepository(Content)
        private contentRepository: Repository<Content>,
        @InjectRepository(SupportTicket)
        private ticketRepository: Repository<SupportTicket>,
        @InjectRepository(SupportTicketMessage)
        private ticketMessageRepository: Repository<SupportTicketMessage>,
        private jwtService: JwtService,
        private readonly validationService: ValidationService,
    ) {}

    async checkOrganizationExists(organizationId: string, requestUrl: string) {
        if (organizationId && requestUrl.startsWith('/api/organization/')) {
            const organizationCount = await this.organizationRepository.countBy({ id: organizationId });

            if (organizationCount === 0) {
                throw this.validationService.notFoundException({ organization: 'NOT_FOUND' });
            }
        }
    }

    async getUserFromToken(token: string | undefined) {
        try {
            if (!token) {
                return null;
            }

            const { userId: tokenUserId } = this.jwtService.verify(token);

            return await this.userRepository.findOne({
                where: { id: tokenUserId },
                relations: { organizations: true },
            });
        } catch {
            return null;
        }
    }

    validateByActions(user: User, organizationId: string, checkedActions: ControllerAction[] | false) {
        if (!checkedActions) return true;

        const userProperty = user?.properties.find((property) => property.organizationId === organizationId);

        const userPermissions = getPermissionsWithGlobalRoles(user.role, userProperty?.role, userProperty?.permissions);

        return userPermissions.some((permission) => checkedActions.includes(permission as ControllerAction));
    }

    async validateByPersonalIds(user: User, request: Request): Promise<{ result: boolean; organizationId?: string }> {
        const userOrganizationIds = user.organizations.map(({ id }) => id);

        const userId = request.body.userId || request.params.userId;

        let organizationId = request.body.organizationId || request.params.organizationId;

        const email = request.body.email || request.params.email;

        const organizationInvitationId = request.params.organizationInvitationId;

        const playlistId =
            request.body.playlistId ||
            request.params.playlistId ||
            (typeof request.body.playlistIds === 'string' ? request.body.playlistIds : undefined);

        const playlistIds =
            (Array.isArray(request.body.playlistIds) ? request.body.playlistIds : undefined) ||
            request.params.playlistIds;

        const screenId = request.body.screenId || request.params.screenId;
        const screenIds =
            request.body.screenIds ||
            request.params.screenIds ||
            request.body.deleteScreenTagDto?.map((dto: { screenId: string }) => dto.screenId);

        const folderId = request.body.folderId || request.params.folderId || request.query.folderId;

        const contentId = request.body.contentId || request.params.contentId;
        const contentIds = request.body.contentIds || request.params.contentIds;

        const locationId = request.body.locationId || request.params.locationId;

        const scenarioId = request.body.scenarioId || request.params.scenarioId;

        const templateId = request.params.templateId;

        const ticketId = request.params.ticketId;

        const messageId = request.params.messageId;

        const isAdmin = user.role === 'admin';
        const isViewOnly = user.role === 'viewonly';

        const isAdminOrViewOnly = request.method === 'GET' ? isAdmin || isViewOnly : isAdmin;

        if (!organizationId) {
            if (organizationInvitationId) {
                const invitation = await this.organizationInvitationRepository.findOneBy({
                    id: organizationInvitationId,
                });

                return { result: isAdminOrViewOnly || invitation?.userId === user.id };
            }

            if (playlistId) {
                const playlist = await this.playlistRepository.findOneBy({ id: playlistId });
                organizationId = playlist?.organizationId ?? '';

                if (!userOrganizationIds.includes(organizationId)) {
                    return { result: isAdminOrViewOnly };
                }
            }

            if (templateId) {
                const template = await this.templateRepository.findOneBy({ id: templateId });
                organizationId = template?.organizationId ?? '';

                if (!userOrganizationIds.includes(organizationId)) {
                    return { result: isAdminOrViewOnly };
                }
            }

            if (playlistIds) {
                if (!Array.isArray(playlistIds)) return { result: false };

                if (playlistIds.length > 0) {
                    const playlists = await this.playlistRepository.findBy({ id: In(playlistIds) });

                    if (
                        playlistIds.length === playlists.length &&
                        playlists.every((playlist) => userOrganizationIds.includes(playlist.organizationId))
                    ) {
                        organizationId = playlists.at(0)?.organizationId ?? '';
                    } else {
                        return { result: isAdminOrViewOnly };
                    }
                }
            }

            if (screenId) {
                const screen = await this.screenRepository.findOneBy({ id: screenId });
                organizationId = screen?.organizationId ?? '';

                if (!userOrganizationIds.includes(organizationId)) {
                    return { result: isAdminOrViewOnly };
                }
            }

            if (screenIds) {
                if (!Array.isArray(screenIds)) return { result: false };

                if (screenIds.length > 0) {
                    const screens = await this.screenRepository.findBy({ id: In(screenIds) });

                    if (
                        uniq(screenIds).length === screens.length &&
                        screens.every((screen) => userOrganizationIds.includes(screen.organizationId))
                    ) {
                        organizationId = screens.at(0)?.organizationId ?? '';
                    } else {
                        return { result: isAdminOrViewOnly };
                    }
                }
            }

            if (locationId) {
                const location = await this.locationRepository.findOneBy({ id: locationId });

                organizationId = location?.organizationId ?? '';

                if (!userOrganizationIds.includes(organizationId)) {
                    return { result: isAdminOrViewOnly };
                }
            }

            if (folderId) {
                const folder = await this.folderRepository.findOneBy({ id: folderId });

                organizationId = folder?.organizationId ?? '';

                if (!userOrganizationIds.includes(organizationId)) {
                    return { result: isAdminOrViewOnly };
                }
            }

            if (scenarioId) {
                const scenario = await this.scenarioRepository.findOneBy({ id: scenarioId });

                organizationId = scenario?.organizationId ?? '';

                if (!userOrganizationIds.includes(organizationId)) {
                    return { result: isAdminOrViewOnly };
                }
            }

            if (contentId) {
                const content = await this.contentRepository.findOneBy({ id: contentId });

                organizationId = content?.organizationId ?? '';

                if (!userOrganizationIds.includes(organizationId)) {
                    return { result: isAdminOrViewOnly };
                }
            }

            if (contentIds) {
                if (!Array.isArray(contentIds)) return { result: false };

                if (contentIds.length > 0) {
                    const contents = await this.contentRepository.findBy({ id: In(contentIds) });

                    if (
                        contentIds.length === contents.length &&
                        contents.every((content) => userOrganizationIds.includes(content.organizationId))
                    ) {
                        organizationId = contents.at(0)?.organizationId ?? '';
                    } else {
                        return { result: isAdminOrViewOnly };
                    }
                }
            }

            if (email) {
                const organization = await this.organizationRepository.findOneBy({ users: { email } });

                organizationId = organization?.id ?? '';

                if (!userOrganizationIds.includes(organizationId)) {
                    return { result: isAdminOrViewOnly };
                }
            }

            if (ticketId) {
                const relatedUserIds = (
                    await this.userRepository.findBy({ properties: { organizationId: In(userOrganizationIds) } })
                ).map((user) => user.id);

                const ticketCount = await this.ticketRepository.countBy({ id: ticketId, userId: In(relatedUserIds) });

                return { result: ticketCount > 0 || isAdminOrViewOnly };
            }

            if (messageId) {
                const relatedUserIds = (
                    await this.userRepository.findBy({ properties: { organizationId: In(userOrganizationIds) } })
                ).map((user) => user.id);

                const messageCount = await this.ticketMessageRepository.countBy({
                    id: messageId,
                    ticket: { userId: In(relatedUserIds) },
                });

                return { result: messageCount > 0 || isAdminOrViewOnly };
            }

            if (userId) {
                return { result: isAdminOrViewOnly || userId === user.id };
            }
        }

        return organizationId
            ? { result: isAdminOrViewOnly || userOrganizationIds.includes(organizationId), organizationId }
            : { result: true };
    }

    updateOrganizationRequestBody(user: User, request: Request) {
        // GET api/organization/:organizationId - получение организации юзером
        if (request.method === 'GET' && request.url.match(/\/api\/organization\/.+$/) !== null) {
            request.body = { ...request.body, user };
        }

        // POST api/ticket - создание тикета юзером
        if (request.method === 'POST' && request.url.match(/\/api\/ticket$/) !== null) {
            request.body = { ...request.body, userId: user.id };
        }

        // GET api/ticket/:organizationId/message - создание сообщения пользователем
        if (request.method === 'POST' && request.url.match(/\/api\/ticket\/.+$/) !== null) {
            const senderRole: MessageSenderRoleType = user.role === 'admin' ? 'support' : 'user';

            request.body = { ...request.body, userId: user.id, senderRole };
        }

        // PATCH api/organization/:organizationId - обновление организации
        if (request.method === 'PATCH' && request.url.match(/\/api\/organization\/.+$/) !== null) {
            request.body = { ...request.body, userRole: user.role };

            if (user.role !== 'admin') {
                request.body = {
                    ...request.body,
                    status: undefined,
                    product: undefined,
                    freeScreens: undefined,
                    subscription: undefined,
                };
            }
        }

        // POST api/organization/:organizationId/invite - создание приглашения
        if (request.method === 'POST' && request.url.match(/\/api\/organization\/.+\/invite$/) !== null) {
            request.body = { ...request.body, inviterId: user.id };
        }

        if (request.method === 'POST' && request.url.match(/\/api\/organization\/.+\/leave$/) !== null) {
            request.body = { ...request.body, user };
        }

        if (request.method === 'POST' && request.url === '/api/organization') {
            request.body = { ...request.body, userId: user.id };
        }

        // обновление юзера
        if (request.method === 'PATCH' && request.url.match(/\/api\/user\/.+$/) !== null) {
            if (user.role !== 'admin') {
                request.body = { ...request.body, role: undefined };
            }
        }
    }
}
