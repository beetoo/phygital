import { Controller, Get, Req, Query, Headers } from '@nestjs/common';
import { Request } from 'express';
import axios from 'axios';

import { AppService } from './app.service';
import { extractTokenFromHeader } from './helpers';
import { Public } from './decorators/public.decorator';
import { DevelopmentOnly } from './decorators/development-only.decorator';
import { ValidationService } from './services/validation/validation.service';
import { X_WEATHER_TOKEN } from './constants';

@Public()
@Controller()
export class AppController {
    constructor(
        private readonly appService: AppService,
        private readonly validationService: ValidationService,
    ) {}

    @Get()
    hello() {
        return 'Hello World!';
    }

    @Get('me')
    getMe(@Req() req: Request) {
        const token = extractTokenFromHeader(req);

        if (!token) {
            throw this.validationService.unauthorizedException({ token: 'NOT_FOUND' });
        }

        return this.appService.findUser(token);
    }

    @DevelopmentOnly()
    @Get('weather')
    async getWeather(@Query('lon') lon: string, @Query('lat') lat: string, @Headers('x-weather-token') token: string) {
        if (token !== X_WEATHER_TOKEN) {
            throw this.validationService.forbiddenException({ token: 'INVALID' });
        }

        return (await axios(`https://yandex.ru/pogoda/front/maps/prec-alert?lat=${lat}&lon=${lon}`)).data;
    }
}
