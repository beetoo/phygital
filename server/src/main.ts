import { HttpAdapterHost, NestFactory, Reflector } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import cookieParser from 'cookie-parser';
import { json } from 'express';
import requestIp from 'request-ip';

import { AppModule } from './app.module';
import { ALLOWED_CLIENT_HOSTS, SERVER_API_PORT } from './constants';
import { ClusterService } from './services/cluster.service';
import { HttpExceptionFilter } from './filters/http-exception.filter';
import { TelegramBotService } from './services/telegram-bot.service';
import { AuthGuard } from './guards/auth-guard/auth.guard';
import { AuthGuardService } from './guards/auth-guard/auth-guard.service';

async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule, {
        logger: ['log', 'error', 'warn', 'debug', 'verbose'],
    });
    app.setGlobalPrefix('api');
    app.enableCors({ origin: ['*', ...ALLOWED_CLIENT_HOSTS], credentials: true });
    app.use(cookieParser());
    app.use(json({ limit: '25mb' }));
    app.use(requestIp.mw());

    const authGuardService = app.get(AuthGuardService);
    const reflector = app.get(Reflector);
    app.useGlobalGuards(new AuthGuard(authGuardService, reflector));

    const httpAdapterHost = app.get(HttpAdapterHost);
    const telegramBotService = app.get(TelegramBotService);
    app.useGlobalFilters(new HttpExceptionFilter(httpAdapterHost, telegramBotService));

    await app.listen(SERVER_API_PORT);
}

// bootstrap();

ClusterService.clusterize(bootstrap);
