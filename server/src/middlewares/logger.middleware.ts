import { Injectable, NestMiddleware } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Request, Response, NextFunction } from 'express';

import { User } from '../models/user/entities/user.entity';
import { UserSessionsService } from '../services/user-sessions.service';
import { getPermissionsWithGlobalRoles } from '../helpers/getPermissions';
import { ControllerAction } from '../decorators/check-permission.decorator';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        private userSessionsService: UserSessionsService,
    ) {}

    async use(request: Request, response: Response, next: NextFunction) {
        const user = await this.userSessionsService.getUserFromRequest(request);

        if (user) {
            Promise.resolve().then(async () => {
                await this.userRepository.update(user.id, { lastActiveAt: new Date() });
            });

            this.filterSubscriptionField(user, request, response);
        }

        next();
    }

    private filterSubscriptionField(user: User, request: Request, response: Response) {
        const isAdmin = user.role === 'admin';
        const hasViewSubscription = this.hasViewSubscriptionPermission(user, request);

        const originalJson = response.json;

        response.json = (body: any) => {
            // Логика фильтрации полей
            const filteredBody = this.filterFields(body, isAdmin, hasViewSubscription);

            // Возвращаем модифицированный JSON
            return originalJson.call(response, filteredBody);
        };
    }

    private filterFields(data: any, isAdmin: boolean, hasViewSubscription: boolean): any {
        // Пример фильтрации
        if (Array.isArray(data)) {
            return data.map((item) => this.filterObject(item, isAdmin, hasViewSubscription));
        } else if (typeof data === 'object' && data !== null) {
            return this.filterObject(data, isAdmin, hasViewSubscription);
        }
        return data;
    }

    private filterObject(obj: any, isAdmin: boolean, hasViewSubscription: boolean): any {
        if (obj?.organization?.subscription) {
            const subscription = hasViewSubscription ? obj.organization.subscription : undefined;

            return {
                ...obj,
                organization: {
                    ...obj.organization,
                    subscription: { ...subscription, notes: isAdmin ? subscription?.notes : undefined },
                },
            };
        } else if (obj?.subscription) {
            const subscription = hasViewSubscription ? obj.subscription : undefined;

            return { ...obj, subscription: { ...subscription, notes: isAdmin ? subscription?.notes : undefined } };
        }

        return obj;
    }

    private hasViewSubscriptionPermission(user: User, request: Request) {
        const organizationId = request.body.organizationId || request.params.organizationId;

        const userProperty = user.properties.find((p) => p.organizationId === organizationId);

        const globalRolePermissions = getPermissionsWithGlobalRoles(
            user.role,
            userProperty?.role,
            userProperty?.permissions,
        );

        return globalRolePermissions.includes(ControllerAction.view_subscriptions);
    }
}
