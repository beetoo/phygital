import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';

import { UserSessionsService } from '../services/user-sessions.service';

@Injectable()
export class UserSessionsMiddleware implements NestMiddleware {
    constructor(private userSessionsService: UserSessionsService) {}

    async use(request: Request, _: Response, next: NextFunction) {
        await this.userSessionsService.checkUserSessionsAndGetUser(request);

        next();
    }
}
