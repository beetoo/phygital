export const getFileTypeModule = async () => {
    return await (eval(`import('file-type')`) as Promise<{ fileTypeFromFile: any }>);
};
