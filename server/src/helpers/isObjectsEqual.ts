export const isObjectsEqual = (
    o1: { [key: string]: string | number } | null,
    o2: { [key: string]: string | number } | null,
) => {
    if (o1 == null && o2 === null) {
        return true;
    } else if ((o1 === null && o2 !== null) || (o1 !== null && o2 === null)) {
        return false;
    } else {
        let p;

        if (o1 !== null) {
            for (p in o1) {
                if (p in o1) {
                    if (o1?.[p] !== o2?.[p]) {
                        return false;
                    }
                }
            }
        }

        if (o2 !== null) {
            for (p in o2) {
                if (p in o2) {
                    if (o1?.[p] !== o2?.[p]) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
};
