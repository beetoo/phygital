import fs from 'node:fs';
import { rimraf } from 'rimraf';

export const deleteFile = async (filepath: string) => {
    if (!fs.existsSync(filepath)) return;

    const serviceFolders = ['content', 'upload', 'screen', 'smil', 'temp'];
    const lastFolder =
        filepath
            .split('/')
            .filter((part) => !!part)
            .at(-1) ?? '';

    if (fs.statSync(filepath).isDirectory() && serviceFolders.includes(lastFolder)) return;

    try {
        await rimraf(filepath);
    } catch (err) {
        console.log(err);
    }
};
