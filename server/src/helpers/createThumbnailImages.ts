import path from 'node:path';
import ffmpeg from 'fluent-ffmpeg';
import ffmpegPath from '@ffmpeg-installer/ffmpeg';

import isImage from '../../../common/helpers/isImage';
import isVideo from '../../../common/helpers/isVideo';

type Resolution = { width?: number | '?'; height?: number | '?' };

const createResizedImage = (
    filePath: string,
    outputFilePath: string,
    { width = -1, height = -1 }: Resolution,
): Promise<string> =>
    new Promise((resolve) => {
        ffmpeg(filePath)
            .setFfmpegPath(ffmpegPath.path)
            .outputOptions(`-vf scale=${width}:${height}`)
            .on('end', () => {
                resolve(outputFilePath);
            })
            .on('error', (err) => {
                console.error(err);
                resolve('');
            })
            .save(outputFilePath);
    });

const createScreenshotImage = (
    filePath: string,
    outputFilePath: string,
    { width = '?', height = '?' }: Resolution,
): Promise<string> =>
    new Promise((resolve) => {
        ffmpeg(filePath)
            .setFfmpegPath(ffmpegPath.path)
            .takeScreenshots({ timemarks: ['0'], filename: outputFilePath, size: `${width}x${height}` })
            .on('end', () => {
                resolve(outputFilePath);
            })
            .on('error', (err) => {
                console.error(err);
                resolve('');
            });
    });

export const createThumbnailImages = async (filePath: string) => {
    const extension = path.extname(filePath).toLowerCase();

    const addedExtension = isImage(filePath) ? extension : isVideo(filePath) ? '.jpg' : '';

    if (!addedExtension) return;

    const thumbnailFilePath = filePath.replace(extension, '') + '_thumbnail' + addedExtension;

    if (isImage(filePath)) {
        await createResizedImage(filePath, thumbnailFilePath, { width: 300 });
    } else if (isVideo(filePath)) {
        await createScreenshotImage(filePath, thumbnailFilePath, { width: 300 });
    }
};
