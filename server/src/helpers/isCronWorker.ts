import cluster from 'node:cluster';

import { redisStore } from './redisClient';

export const isCronWorker = async () => {
    const store = await redisStore;
    const cronWorkerId = Number(await store.get('cronWorkerId'));

    return cluster.isPrimary || cluster.worker?.id === cronWorkerId;
};
