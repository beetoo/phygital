import { isCronWorker } from './isCronWorker';

export const withWorkerCheck = async (func: any) => {
    if (!(await isCronWorker())) return;

    return func();
};
