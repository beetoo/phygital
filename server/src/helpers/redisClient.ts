import { redisStore as store } from 'cache-manager-redis-yet';

import { REDIS_HOST, REDIS_PASSWORD, isLocalDevelopmentServer } from '../constants';

export class RedisClient {
    static async createRedisClient() {
        const config = {
            username: 'default',
            password: REDIS_PASSWORD,
            socket: {
                host: REDIS_HOST,
                port: isLocalDevelopmentServer ? 6380 : 6379,
            },
            ttl: 0,
        };

        // listen for redis connection error event
        return await store(config);
    }
}

export const redisStore = RedisClient.createRedisClient();
