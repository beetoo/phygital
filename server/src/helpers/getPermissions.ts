import { permissionsData } from '../constants/permissionsData';

import { UserOrganizationRoleEnum } from '../../../common/types';

const getPermissionsArray = (
    role: 'admin' | 'viewonly' | 'accounting' | 'user' | UserOrganizationRoleEnum | undefined,
    permissions: string[] = [],
) => {
    const permissionsObject = role ? (permissionsData[role === 'admin' ? 'global_admin' : role] ?? []) : [];
    const validPermissionKeys = Object.keys(permissionsObject);

    const toBeAddedPermissions = validPermissionKeys.filter(
        (key) => permissionsObject[key as keyof typeof permissionsObject] === 1,
    );

    const toBeDeletedPermissions = validPermissionKeys.filter(
        (key) => permissionsObject[key as keyof typeof permissionsObject] === 0,
    );

    return permissions
        .filter((permission) => validPermissionKeys.includes(permission))
        .filter((permission) => !toBeDeletedPermissions.includes(permission))
        .concat(toBeAddedPermissions);
};

export const getPermissions = (role: UserOrganizationRoleEnum, permissions: string[] = []) => {
    if (!permissions) {
        permissions = [];
    }

    const organizationRolePermissions = getPermissionsArray(role, permissions);

    return organizationRolePermissions.reduce(
        (permissions, permission) => (permissions.includes(permission) ? permissions : [...permissions, permission]),
        [] as string[],
    );
};

export const getPermissionsWithGlobalRoles = (
    globalRole: 'admin' | 'viewonly' | 'accounting' | 'user',
    organizationRole: UserOrganizationRoleEnum | undefined,
    permissions: string[] = [],
) => {
    if (!permissions) {
        permissions = [];
    }

    const globalRolePermissions = getPermissionsArray(globalRole);

    const organizationRolePermissions = getPermissionsArray(organizationRole, permissions);

    return [...globalRolePermissions, ...organizationRolePermissions].reduce(
        (permissions, permission) => (permissions.includes(permission) ? permissions : [...permissions, permission]),
        [] as string[],
    );
};

export const getMayBeAddedPermissions = (role: UserOrganizationRoleEnum, permissions: string[] = []) => {
    const permissionsObject = permissionsData[role];

    const validPermissionKeys = Object.keys(permissionsObject);

    const mayBeAddedPermissions = validPermissionKeys.filter(
        (key) => permissionsObject[key as keyof typeof permissionsObject] === 3,
    );

    return permissions.filter((permission) => mayBeAddedPermissions.includes(permission));
};
