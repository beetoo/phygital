import ffprobeStatic from 'ffprobe-static';
import ffmpeg from 'fluent-ffmpeg';
import {
    addDays,
    addMinutes,
    addYears,
    differenceInSeconds,
    format,
    isFriday,
    isMonday,
    isSaturday,
    isSunday,
    isThursday,
    isTuesday,
    isWednesday,
    isWithinInterval,
    startOfDay,
} from 'date-fns';
import { orderBy } from 'lodash';
import { Request } from 'express';

import { Screen } from '../models/screen/entities/screen.entity';
import { ScreenRequestLogger } from '../models/admin/screen-request-logger/entities/screen-request-logger.entity';
import {
    PlaylistItemType,
    PlaylistSavedScheduleType,
    PlaylistType,
    ScenarioRuleType,
    ScreenType,
    TimeData,
    UrlParamModeType,
    WeatherAndTrafficData,
} from '../../../common/types';
import { frequencyFromByDay } from '../../../common/helpers';
import { Playlist } from '../models/playlist/entities/playlist.entity';
import { calculateIntervalPlaylist } from '../models/smil/helpers';

export const getUniqueNameUsingPostfix = (namesArray: string[], newName: string, prevName = ''): string => {
    if (newName === prevName) {
        return newName;
    }

    const repeatedNamesNumber = namesArray
        .filter((name) => prevName !== name && name.startsWith(newName) && !!name.match(/.+(\W\(\d+\))?$/))
        .reduce((acc, curr) => (acc.includes(curr) ? acc : [...acc, curr]), [] as string[])?.length;
    // newName = `${newName.charAt(0).toUpperCase()}${newName.slice(1)}`;
    return repeatedNamesNumber > 0 ? `${newName} (${repeatedNamesNumber + 1})` : newName;
};

export const getChangePasswordTimeText = (isEng: boolean, timezoneOffset: number) => {
    const addZero = (num: number): string => (num > 9 ? num.toString() : '0' + num);

    const currentDate = addMinutes(new Date(), timezoneOffset);

    const weekDays = isEng
        ? ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        : ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];

    const months = isEng
        ? [
              'January',
              'February',
              'March',
              'April',
              'May',
              'June',
              'July',
              'August',
              'September',
              'October',
              'November',
              'December',
          ]
        : [
              'января',
              'февраля',
              'марта',
              'апреля',
              'мая',
              'июня',
              'июля',
              'августа',
              'сентября',
              'октября',
              'ноября',
              'декабря',
          ];

    const weekDay = weekDays[currentDate.getDay() - 1];

    const year = currentDate.getFullYear();
    const day = currentDate.getDate();
    const month = months[currentDate.getMonth()];

    const hours = addZero(currentDate.getHours());
    const minutes = addZero(currentDate.getMinutes());
    const seconds = addZero(currentDate.getSeconds());

    // (Пятница, 17 сентября 2021 в 12:13:56)
    return `(${weekDay}, ${day} ${month} ${year} в ${hours}:${minutes}:${seconds})`;
};

const isAllowedPeriod = (currentDate: Date, startDate: Date, endDate: Date, broadcastAlways: boolean) => {
    endDate = broadcastAlways ? addYears(endDate, 100) : endDate;

    try {
        return isWithinInterval(currentDate, { start: startDate, end: endDate });
    } catch {
        return false;
    }
};

const isAllowedTime = (currentDate: Date, startDate: Date, endDate: Date) => {
    // Извлекаем часы и минуты из времени начала
    const currentHours = currentDate.getHours();
    const currentMinutes = currentDate.getMinutes();

    // Извлекаем часы и минуты из времени начала
    const startHours = startDate.getHours();
    const startMinutes = startDate.getMinutes();

    // Извлекаем часы и минуты из времени окончания
    const endHours = endDate.getHours();
    const endMinutes = endDate.getMinutes();

    // Преобразуем время в минуты с начала дня
    const currentTime = currentHours * 60 + currentMinutes;
    const startTime = startHours * 60 + startMinutes;
    const endTime = endHours * 60 + endMinutes;

    // Если время окончания раньше или равно времени начала, считаем окончание следующим днем
    if (endTime <= startTime) {
        return currentTime >= startTime || currentTime < endTime;
    }

    // Проверяем, входит ли текущее время в диапазон
    return currentTime >= startTime && currentTime < endTime;
};

const isAllowedWeekDay = (currentDay: Date, byDay: string): boolean =>
    (byDay.includes('MO') && isMonday(currentDay)) ||
    (byDay.includes('TU') && isTuesday(currentDay)) ||
    (byDay.includes('WE') && isWednesday(currentDay)) ||
    (byDay.includes('TH') && isThursday(currentDay)) ||
    (byDay.includes('FR') && isFriday(currentDay)) ||
    (byDay.includes('SA') && isSaturday(currentDay)) ||
    (byDay.includes('SU') && isSunday(currentDay));

export const playlistShouldBeRun = (
    screen: ScreenType,
    { startDay, endDay, startTime, endTime, broadcastAlways, byDay }: PlaylistSavedScheduleType,
    addedMinutes = 0,
) => {
    const currentDate = addMinutes(new Date(), (screen.location?.tzOffset ?? 180) + addedMinutes);

    const startDate = new Date(`${startDay}T${startTime}`);
    let endDate = new Date(`${endDay}T${endTime}`);

    // Плейлист не подошел по дате работы
    if (!isAllowedPeriod(currentDate, startDate, endDate, broadcastAlways)) {
        return false;
    }

    // Плейлист не подошел по времени работы
    if (!isAllowedTime(currentDate, startDate, endDate)) {
        return false;
    }

    if (!isWithinScreenTimeInterval(currentDate, screen)) {
        return false;
    }

    const frequency = frequencyFromByDay(byDay);

    switch (frequency) {
        // Если частота Каждую неделю
        case 'WEEKLY':
            return isAllowedWeekDay(currentDate, byDay);
        // Если частота Без повтора или Каждый день
        default:
            return true;
    }
};

const isWithinScreenTimeInterval = (currentDate: Date, screen: ScreenType | undefined): boolean => {
    const startOfCurrentDay = startOfDay(currentDate);

    const startTime = addMinutes(
        startOfCurrentDay,
        (screen?.startTime.hours ?? 0) * 60 + (screen?.startTime.minutes ?? 0),
    );
    const endTime = addMinutes(startOfCurrentDay, (screen?.endTime.hours ?? 0) * 60 + (screen?.endTime.minutes ?? 0));

    return isAllowedTime(currentDate, startTime, endTime);
};

const lifeTimeFilter = (currentDate: Date, playlistItems: PlaylistItemType[]) => {
    return playlistItems.filter((item) =>
        item.content.lifetime
            ? isWithinInterval(currentDate, {
                  start: item.content.lifetime.startDate,
                  end: addDays(new Date(item.content.lifetime.endDate), 1),
              })
            : true,
    );
};

export const getCurrentTimePlaylist = (
    screen: ScreenType | undefined | null,
    addedMinutes = 0,
): PlaylistType | undefined => {
    if (!screen || !screen.playlists || screen.playlists?.length === 0) {
        return;
    }

    const currentDate = addMinutes(new Date(), (screen.location?.tzOffset ?? 180) + addedMinutes);

    let playlists = screen.playlists
        .map((playlist) => ({ ...playlist, playlistItems: lifeTimeFilter(currentDate, playlist.playlistItems) }))
        .filter((playlist) => !playlist?.draft && playlist?.playlistItems?.length > 0)
        .filter((playlist) => playlistShouldBeRun(screen, playlist.schedule, addedMinutes));

    if (playlists.length > 1) {
        // фильтр по приоритету и времени
        playlists = orderBy(playlists, (playlist) => playlist.priority, ['desc']);
    }

    const playlist = playlists[0];

    return playlist?.playlistItems?.length > 0
        ? {
              ...playlist,
              playlistItems: playlist.playlistItems.sort(
                  ({ orderIndex: prevOrderIndex }, { orderIndex }) => prevOrderIndex - orderIndex,
              ),
          }
        : playlist;
};

export const isWeatherAndTrafficData = (data: Required<ScenarioRuleType>['data']): data is WeatherAndTrafficData =>
    'parameter' in data || 'operator' in data || 'value' in data;

export const isTimeData = (data: Required<ScenarioRuleType>['data']): data is TimeData =>
    'startDay' in data &&
    'endDay' in data &&
    'startTime' in data &&
    'endTime' in data &&
    'frequency' in data &&
    'byDay' in data;

export const playlistItemIsNotNull = (item: PlaylistItemType | null): item is PlaylistItemType => !!item;

export const mapScreensWithLogs = (screens: Screen[], requestLogs: ScreenRequestLogger[]) => {
    return screens.map((screen) => {
        const headLog = requestLogs.find(
            ({ identification, requestType }) => screen.identification === identification && requestType === 'HEAD',
        );

        const getLog = requestLogs.find(
            ({ identification, requestType }) => screen.identification === identification && requestType === 'GET',
        );

        const log = { ...headLog, message: getLog?.message };

        return {
            ...screen,
            log,
        };
    });
};

const xorMap = (str: string, mode: 'encoding' | 'decoding', key = 678904) => {
    str = mode === 'encoding' ? btoa(str) : str;
    const xor = (char: string) => {
        const charCode = char.charCodeAt(0);
        const xorNumber = charCode ^ key;
        return String.fromCharCode(xorNumber);
    };

    const result = str.split('').map(xor).join('');

    return mode === 'decoding' ? atob(result) : result;
};

export const xorToObj = (str: string | undefined) => {
    if (!str) {
        return { maxDevices: 0, expiredDate: new Date('2024.03.15') };
    }

    const [maxDevices, expiredDate] = str.split(':');

    try {
        return {
            maxDevices: Number(xorMap(maxDevices, 'decoding')),
            expiredDate: new Date(xorMap(expiredDate, 'decoding')),
        };
    } catch {
        return { maxDevices: 0, expiredDate: new Date('2024.03.15') };
    }
};

export const getPlayerVersion = (identification: string, screenLogs: ScreenRequestLogger[]) => {
    const requestJson = screenLogs.find((log) => log.identification === identification)?.requestJson ?? '';

    const match = requestJson?.split('versionname":"')[1]?.match(/(.+?)".+/);

    return match?.at(1) ?? null;
};

export const formatUrlForWebContent = (url: string, screen: Screen, urlParamMode: UrlParamModeType = 'none') => {
    if (url.startsWith('file://')) {
        return url;
    }

    // 1
    const screenUrlParams = screen.urlParams ?? {};
    const locationUrlParams = screen.location.urlParams ?? {};

    let urlParams = {};
    if (urlParamMode === 'screen') {
        urlParams = {
            screenId: screen.id,
            locationId: screen.location.id,
            ...screenUrlParams,
        };
    } else if (urlParamMode === 'location') {
        urlParams = {
            screenId: screen.id,
            locationId: screen.location.id,
            ...locationUrlParams,
        };
    } else if (urlParamMode === 'all') {
        urlParams = {
            screenId: screen.id,
            locationId: screen.location.id,
            ...locationUrlParams,
            ...screenUrlParams,
        };
    }

    // 2
    const { origin, pathname, search } = new URL(url);

    const searchObject = Object.fromEntries(new URLSearchParams(search));

    const searchParams = new URLSearchParams({ ...searchObject, ...urlParams }).toString();

    const arr = Array.from(new URLSearchParams(searchParams));

    if (arr.length === 0) {
        return origin + pathname;
    }

    let newSearchParams = '?';

    arr.forEach((val, index, arr) => {
        const key = val[0];
        const value = val[1];

        newSearchParams += `${key}=${value}`;

        if (index < arr.length - 1) {
            newSearchParams += '&amp;';
        }
    });

    return origin + pathname + newSearchParams;
};

export const extractTokenFromHeader = (request: Request): string | undefined => {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? token : undefined;
};

export const getVideoDimensions = (
    filePath: string,
): Promise<{
    width: number;
    height: number;
    duration: number;
    size: number;
    bitrate: number;
    memoryUsage: number;
}> =>
    new Promise((resolve) => {
        ffmpeg(filePath)
            .setFfprobePath(ffprobeStatic.path)
            .ffprobe((err, metadata) => {
                if (err || !metadata) {
                    resolve({ width: 0, height: 0, duration: 0, size: 0, bitrate: 0, memoryUsage: 0 });
                }

                const videoStream = metadata.streams.find((stream) => stream.codec_type === 'video');

                if (videoStream && metadata.format) {
                    const width = videoStream.width ?? 0;
                    const height = videoStream.height ?? 0;
                    const duration = Math.round(Number(metadata.format.duration));
                    const size = metadata.format.size;
                    const bitrate = Math.round(Number(videoStream.bit_rate) / 1000);

                    const colorDepth = videoStream?.pix_fmt;
                    const fps = eval(<string>videoStream.r_frame_rate); // Преобразуем строковое значение в число

                    // Предполагаем, что формат yuv420p (8 бит на компонент)
                    const bytesPerPixel = colorDepth === 'yuv420p' ? 1.5 : 3; // 1.5 байта на пиксель для yuv420p

                    // Рассчитываем объем памяти для одного кадра
                    const memoryPerFrame = width * height * bytesPerPixel; // в байтах

                    // Рассчитываем объем памяти в секунду
                    const memoryPerSecond = memoryPerFrame * fps;

                    if (width && height && duration && size && bitrate && memoryPerSecond) {
                        resolve({ width, height, duration, size, bitrate, memoryUsage: memoryPerSecond });
                    }
                }
            });
    });

export const mapScreens = (screens: Screen[], screenLogs: ScreenRequestLogger[]) =>
    screens.map(
        ({ id, createdAt, aliveAt, identification, name, resolution, orientation, isEmpty, smil, location }) => ({
            id,
            createdAt,
            aliveAt,
            identification,
            status: differenceInSeconds(new Date(), new Date(aliveAt)) > 180 ? 'offline' : 'online',
            name,
            resolution,
            orientation,
            hasCustomSmil: !!smil,
            isEmpty,
            appVersion: getPlayerVersion(identification, screenLogs),
            location: location
                ? {
                      ...location,
                      organizationId: undefined,
                      createdAt: undefined,
                      urlParams: undefined,
                      variables: undefined,
                  }
                : undefined,
        }),
    );

export const mapOneScreen = (screen: Screen, screenLogs: ScreenRequestLogger[]) => ({
    ...screen,
    status: differenceInSeconds(new Date(), new Date(screen.aliveAt)) > 180 ? 'offline' : 'online',
    appVersion: getPlayerVersion(screen.identification, screenLogs),
    organizationId: undefined,
    startTime: undefined,
    endTime: undefined,
    model: undefined,
    deletedAt: undefined,
});

const getPlaylistItemsDuration = (
    items: PlaylistItemType[],
    playlistType: PlaylistType['type'],
    intervalDuration: number | null,
) => {
    if (playlistType === 'interval' && intervalDuration !== null) {
        items = calculateIntervalPlaylist(items, intervalDuration);
    }

    return items.reduce(
        (sum, { delay, content: { type, duration } }) => sum + (type === 'video' ? duration : delay),
        0,
    );
};

export const mapPlaylists = (playlists: Playlist[]) =>
    playlists.map(({ playlistItems, screens, ...playlist }) => ({
        ...playlist,
        hasHistory: false,
        itemCount: playlistItems.length,
        screenCount: screens.length,
        duration: getPlaylistItemsDuration(playlistItems, playlist.type, playlist.intervalDuration),
        size: playlistItems.reduce((sum, { content: { size } }) => sum + size, 0),
        organizationId: undefined,
    }));

export const getDefaultSchedule = () => {
    const defaultDay = format(new Date(), 'yyyy-MM-dd');

    return {
        startDay: defaultDay,
        endDay: defaultDay,
        startTime: '00:00',
        endTime: '00:00',
        byDay: 'MO,TU,WE,TH,FR,SA,SU',
        broadcastAlways: true,
    };
};

export const withContentLinks = (screen: Screen) => {
    const contentLinks = screen.playlists
        .filter((playlist) => !playlist.draft)
        .flatMap((playlist) => playlist.playlistItems)
        .map((item) => item.content.src)
        .reduce((links, link) => (links.includes(link) ? links : [...links, link]), [] as string[])
        .filter((link) => link);

    return { ...screen, contentLinks };
};

export const formatScreenForSmil = (screen: Screen) => {
    return {
        ...screen,
        createdAt: undefined,
        aliveAt: undefined,
        status: undefined,
        name: undefined,
        model: undefined,
        deletedAt: undefined,
        logger: undefined,
        location: {
            ...screen.location,
            organizationId: undefined,
            name: undefined,
            city: undefined,
            address: undefined,
            createdAt: undefined,
            tzLabel: undefined,
        },
        playlists: screen.playlists.map((playlist) => ({
            ...playlist,
            organizationId: undefined,
            uploads: undefined,
            createdAt: undefined,
            updatedAt: undefined,
            log: undefined,
            resolution: undefined,
            playlistItems: playlist.playlistItems.map((playlistItem) => ({
                ...playlistItem,
                playlistId: undefined,
                name: undefined,
                scenarioId: undefined,
                createdAt: undefined,
                updatedAt: undefined,
                content: {
                    ...playlistItem.content,
                    src: playlistItem.content.src,
                    organizationId: undefined,
                    createdAt: undefined,
                    updatedAt: undefined,
                    originFilename: undefined,
                    bitrate: undefined,
                },
            })),
        })),
    };
};
