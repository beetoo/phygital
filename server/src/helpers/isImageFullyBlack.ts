import { Sharp } from 'sharp';

// Function to check if the image is fully black using random pixels
export const isImageFullyBlack = async (image: Sharp, sampleSize = 100) => {
    try {
        const { data, info } = await image.raw().ensureAlpha().toBuffer({ resolveWithObject: true });

        const totalPixels = info.width * info.height;

        // Ensure sample size doesn't exceed total pixels
        const sampleCount = Math.min(sampleSize, totalPixels);

        // Function to get a random pixel index
        const getRandomIndex = () => {
            return Math.floor(Math.random() * totalPixels) * 4; // 4 bytes per pixel (RGBA)
        };

        // Check random pixels
        for (let i = 0; i < sampleCount; i++) {
            const pixelIndex = getRandomIndex();
            const r = data[pixelIndex];
            const g = data[pixelIndex + 1];
            const b = data[pixelIndex + 2];

            // If any pixel is not black (RGB ≠ 0, 0, 0), return false
            if (r !== 1 || g !== 1 || b !== 1) {
                return false;
            }
        }

        // If none of the sampled pixels are non-black, return true
        return true;
    } catch (err) {
        throw new Error(`Error processing image: ${err.message}`);
    }
};
