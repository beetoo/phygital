import { flatten } from 'lodash';
import { Repository, FindOneOptions } from 'typeorm';

export const findOneWithRelations = async (
    repo: Repository<any>,
    relations: string[],
    optionsWithoutRelations: FindOneOptions,
): Promise<any> => {
    const entityWithRelations = await Promise.all(
        relations.map((relation) => repo.find({ ...optionsWithoutRelations, relations: [relation] })),
    ).then(flatten);

    return entityWithRelations.reduce((obj, entity) => ({ ...obj, ...entity }), {});
};
