import { flatten, groupBy, map, merge } from 'lodash';
import { FindManyOptions, In, Repository } from 'typeorm';

export const findWithRelations = async <T>(
    repo: Repository<any>,
    relations: string[],
    optionsWithoutRelations?: FindManyOptions,
): Promise<any> => {
    const entities = await repo.find(optionsWithoutRelations);
    const entitiesIds = entities.map(({ id }) => id);
    const entitiesIdsWithRelations = await Promise.all(
        relations.map((relation) =>
            repo.find({ where: { id: In(entitiesIds) }, select: { id: true }, relations: [relation] }),
        ),
    ).then(flatten);
    const entitiesAndRelations = entitiesIdsWithRelations.concat(entities);

    const entitiesAndRelationsById = groupBy(entitiesAndRelations, 'id');
    return map(entitiesAndRelationsById, (entityAndRelations) => merge({}, ...entityAndRelations));
};
