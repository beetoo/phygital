import { flatten, groupBy, map, merge } from 'lodash';
import { FindManyOptions, In, Repository } from 'typeorm';

export const findWithRelations = async (
    repo: Repository<any>,
    relations: string[],
    optionsWithoutRelations?: FindManyOptions,
): Promise<any> => {
    const entities = await repo.find({ ...optionsWithoutRelations, relations: [relations[0]] });
    const entitiesIds = entities.map(({ id }) => id);
    const entitiesIdsWithRelations = await Promise.all(
        relations
            .slice(1)
            .map((relation) =>
                repo.find({ where: { id: In(entitiesIds) }, select: { id: true }, relations: [relation] }),
            ),
    ).then(flatten);
    const entitiesAndRelations = entitiesIdsWithRelations.concat(entities);

    const entitiesAndRelationsById = groupBy(entitiesAndRelations, 'id');
    return map(entitiesAndRelationsById, (entityAndRelations) => merge({}, ...entityAndRelations));
};
