import { Controller, Param, Post, Delete } from '@nestjs/common';
import { validate } from 'uuid';

import { OrganizationInvitationService } from './organization-invitation.service';
import { CheckPermission, ControllerAction } from '../../decorators/check-permission.decorator';
import { ValidationService } from '../../services/validation/validation.service';

@Controller('organizationInvitation')
export class OrganizationInvitationController {
    constructor(
        private readonly organizationInvitationService: OrganizationInvitationService,
        private readonly validationService: ValidationService,
    ) {}

    @Post(':organizationInvitationId/accept')
    acceptInvitation(@Param('organizationInvitationId') organizationInvitationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationInvitationId), {
            organizationInvitationId: 'INVALID',
        });

        return this.organizationInvitationService.acceptInvitation(organizationInvitationId);
    }

    @Post(':organizationInvitationId/reject')
    rejectInvitation(@Param('organizationInvitationId') organizationInvitationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationInvitationId), {
            organizationInvitationId: 'INVALID',
        });

        return this.organizationInvitationService.rejectInvitation(organizationInvitationId);
    }

    @CheckPermission(ControllerAction.invite_user)
    @Delete(':organizationInvitationId')
    deleteInvitation(@Param('organizationInvitationId') organizationInvitationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationInvitationId), {
            organizationInvitationId: 'INVALID',
        });

        return this.organizationInvitationService.deleteInvitation(organizationInvitationId);
    }
}
