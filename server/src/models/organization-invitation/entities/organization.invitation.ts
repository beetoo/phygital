import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';

import { OrganizationInvitationType, UserOrganizationRoleEnum } from '../../../../../common/types';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class OrganizationInvitation implements OrganizationInvitationType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    organizationId: string;

    @Column()
    userId: string;

    @Column()
    inviterId: string;

    @Column({ type: 'enum', enum: UserOrganizationRoleEnum })
    role: UserOrganizationRoleEnum;

    @Column({ type: 'json' })
    permissions: string[];

    @CreateDateColumn()
    createdAt: Date;
}
