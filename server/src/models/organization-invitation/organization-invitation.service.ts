import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { OrganizationInvitation } from './entities/organization.invitation';
import { User } from '../user/entities/user.entity';
import { UserOrganizationProperty } from '../user/entities/user-organization-property.entity';
import { Organization } from '../organization/entities/organization.entity';
import { ValidationService } from '../../services/validation/validation.service';

@Injectable()
export class OrganizationInvitationService {
    constructor(
        @InjectRepository(Organization)
        private organizationRepository: Repository<Organization>,
        @InjectRepository(OrganizationInvitation)
        private organizationInvitationRepository: Repository<OrganizationInvitation>,
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(UserOrganizationProperty)
        private userOrganizationPropertyRepository: Repository<UserOrganizationProperty>,
        private readonly validationService: ValidationService,
    ) {}

    async acceptInvitation(organizationInvitationId: string) {
        const invitation = await this.organizationInvitationRepository.findOneBy({ id: organizationInvitationId });

        if (!invitation) {
            throw this.validationService.notFoundException({ invitation: 'NOT_FOUND' });
        }

        const { userId, organizationId, role, permissions } = invitation;

        const [user, organization] = await Promise.all([
            this.userRepository.findOne({ where: { id: userId }, relations: { organizations: true } }),
            this.organizationRepository.findOneBy({ id: organizationId }),
        ]);

        if (!user) {
            throw this.validationService.badRequestException({ user: 'NOT_FOUND' });
        }

        if (!organization) {
            throw this.validationService.badRequestException({ organization: 'NOT_FOUND' });
        }

        // сохраняем организацию для юзера
        user.organizations = [...user.organizations, organization];
        await this.userRepository.save(user);

        // сохраняем свойства юзера в этой организации
        const userProperty = new UserOrganizationProperty();

        userProperty.user = user;
        userProperty.organizationId = organizationId;
        userProperty.permissions = permissions;
        userProperty.role = role;

        await this.userOrganizationPropertyRepository.save(userProperty);

        // удаляем приглашение
        await this.organizationInvitationRepository.remove(invitation);

        return { userId };
    }

    rejectInvitation(organizationInvitationId: string) {
        return this.removeInvitation(organizationInvitationId);
    }

    deleteInvitation(organizationInvitationId: string) {
        return this.removeInvitation(organizationInvitationId);
    }

    private async removeInvitation(organizationInvitationId: string) {
        const invitation = await this.organizationInvitationRepository.findOneBy({ id: organizationInvitationId });

        if (!invitation) {
            throw this.validationService.notFoundException({ invitation: 'NOT_FOUND' });
        }

        await this.organizationInvitationRepository.remove(invitation);

        return { organizationInvitationId };
    }
}
