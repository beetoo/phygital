import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { OrganizationInvitationService } from './organization-invitation.service';
import { OrganizationInvitationController } from './organization-invitation.controller';
import { OrganizationInvitation } from './entities/organization.invitation';
import { User } from '../user/entities/user.entity';
import { UserOrganizationProperty } from '../user/entities/user-organization-property.entity';
import { Organization } from '../organization/entities/organization.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Organization, OrganizationInvitation, User, UserOrganizationProperty])],
    controllers: [OrganizationInvitationController],
    providers: [OrganizationInvitationService],
})
export class OrganizationInvitationModule {}
