import { PlaylistItemPropertiesType } from '../../../../../common/types';

type PlaylistItemDtoType = {
    id?: string;
    type: 'recurring' | 'filler' | null;
    properties: PlaylistItemPropertiesType;
    order: number;
    duration: number;
    scenarioId: string;
    contentId: string;
    regionId: number;
};

export class UpdatePlaylistItemsDto {
    items: PlaylistItemDtoType[];
}
