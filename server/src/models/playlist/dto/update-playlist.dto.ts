import { PlaylistRegion } from '../../../../../common/types';

export type UpdatePlaylistDto = {
    intervalDuration?: number;
    title?: string;
    draft?: boolean;
    resolution?: { width: number; height: number };
    sync?: boolean;
    regions?: PlaylistRegion[];
    templateId?: string | null;
};
