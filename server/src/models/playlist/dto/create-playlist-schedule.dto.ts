export class CreatePlaylistScheduleDto {
    id?: string;
    readonly startDay: string;
    readonly endDay: string;
    readonly startTime: string;
    readonly endTime: string;
    readonly broadcastAlways: boolean;
    readonly byDay: string;
    screenId: string;
}
