import { PartialType } from '@nestjs/mapped-types';
import { CreatePlaylistScheduleDto } from './create-playlist-schedule.dto';

export class UpdatePlaylistScheduleDto extends PartialType(CreatePlaylistScheduleDto) {}
