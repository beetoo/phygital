export class UpdatePlaylistsPriorityDto {
    priorityInfo: { id: string; priority: number }[];
}
