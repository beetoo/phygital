import { PlaylistType } from '../../../../../common/types';

export const defaultRegions = (playlistResolution: PlaylistType['resolution'] = { width: 1920, height: 1080 }) => [
    {
        id: 1,
        width: playlistResolution?.width ?? 1920,
        height: playlistResolution?.height ?? 1080,
        xPosition: 0,
        yPosition: 0,
        zIndex: 1,
    },
];
