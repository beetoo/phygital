import {
    Column,
    CreateDateColumn,
    Entity,
    JoinTable,
    ManyToMany,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

import { PlaylistItem } from './playlist-item.entity';
import { Screen } from '../../screen/entities/screen.entity';
import { PlaylistRegion, PlaylistSavedScheduleType, PlaylistType } from '../../../../../common/types';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class Playlist implements PlaylistType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    organizationId: string;

    @Column({ type: String, nullable: true })
    templateId!: string | null;

    @Column({ default: 'basic' })
    type: 'basic' | 'interval';

    @Column()
    title: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column({ type: 'json', nullable: true })
    resolution: { width: number; height: number };

    @Column({ type: 'json', nullable: true })
    regions: PlaylistRegion[];

    @Column({ default: false })
    draft: boolean;

    @Column({ nullable: true, unsigned: true })
    priority: number;

    @Column({ type: Number, nullable: true })
    intervalDuration: number | null;

    @OneToMany(() => PlaylistItem, (playlistItem) => playlistItem.playlist, { cascade: true })
    playlistItems: PlaylistItem[];

    @Column({ default: false })
    sync: boolean;

    @Column({ default: false })
    log: boolean;

    @Column('json', { nullable: true })
    schedule: PlaylistSavedScheduleType;

    @ManyToMany(() => Screen, (screen) => screen.playlists)
    @JoinTable()
    screens: Screen[];
}
