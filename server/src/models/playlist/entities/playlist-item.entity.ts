import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, CreateDateColumn, UpdateDateColumn } from 'typeorm';

import { PlaylistItemPropertiesType, PlaylistItemType } from '../../../../../common/types';
import { Playlist } from './playlist.entity';
import { Scenario } from '../../scenario/entities/scenario.entity';
import { Content } from '../../content/entities/content.entity';

@Entity({ orderBy: { orderIndex: 'ASC' } })
export class PlaylistItem implements PlaylistItemType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ default: '' })
    name: string;

    @Column({ type: String, nullable: true })
    type: 'recurring' | 'filler' | null;

    @Column({ default: 1, unsigned: true })
    regionId: number;

    @Column({ unsigned: true })
    delay: number;

    @Column({ unsigned: true })
    orderIndex: number;

    @Column({ unique: false, nullable: true })
    playlistId: string;

    @Column({ type: String, nullable: true })
    scenarioId!: string | null;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column({ type: 'json', nullable: true })
    properties: PlaylistItemPropertiesType;

    @ManyToOne(() => Content, (content) => content.playlistItems)
    content: Content;

    @ManyToOne(() => Scenario, (scenario) => scenario.playlistItems)
    scenario: Scenario;

    @ManyToOne(() => Playlist, (playlist) => playlist.playlistItems, {
        onDelete: 'CASCADE',
        orphanedRowAction: 'delete',
    })
    playlist: Playlist;
}
