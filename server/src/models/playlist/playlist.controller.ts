import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { validate } from 'uuid';

import { PlaylistService } from './playlist.service';
import { UpdatePlaylistDto } from './dto/update-playlist.dto';
import { UpdatePlaylistScheduleDto } from './dto/update-playlist-schedule.dto';
import { UpdatePlaylistItemsDto } from './dto/update-playlist-content.dto';
import { CreatePlaylistScheduleDto } from './dto/create-playlist-schedule.dto';
import { CheckPermission, ControllerAction } from '../../decorators/check-permission.decorator';
import { ValidationService } from '../../services/validation/validation.service';

@Controller('playlist')
export class PlaylistController {
    constructor(
        private readonly playlistService: PlaylistService,
        private readonly validationService: ValidationService,
    ) {}

    @CheckPermission(ControllerAction.view_playlists)
    @Get(':playlistId')
    findPlaylist(@Param('playlistId') playlistId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(playlistId), { playlistId: 'INVALID' });

        return this.playlistService.findOnePlaylist(playlistId);
    }

    @CheckPermission(ControllerAction.update_playlist_content)
    @Post(':playlistId/content')
    updatePlaylistContent(
        @Param('playlistId') playlistId: string,
        @Body() updatePlaylistItemsDto: UpdatePlaylistItemsDto,
    ) {
        this.validationService.throwBadRequestExceptionIf(!validate(playlistId), { playlistId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!Array.isArray(updatePlaylistItemsDto.items), {
            items: 'INVALID',
        });

        return this.playlistService.updateContent(playlistId, updatePlaylistItemsDto);
    }

    @CheckPermission(ControllerAction.update_playlist)
    @Post(':playlistId/schedule')
    updatePlaylistSchedule(
        @Param('playlistId') playlistId: string,
        @Body() updatePlaylistScheduleDto: UpdatePlaylistScheduleDto,
    ) {
        const { startDay, endDay, startTime, endTime, broadcastAlways, byDay } = updatePlaylistScheduleDto;

        this.validationService.throwBadRequestExceptionIf(!validate(playlistId), { playlistId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!startDay, { startDay: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!endDay, { endDay: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!startTime, { startTime: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!endTime, { endTime: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(typeof broadcastAlways !== 'boolean', {
            broadcastAlways: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(
            !!byDay && !byDay.split(',').every((value) => ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU'].includes(value)),
            { byDay: 'INVALID' },
        );

        return this.playlistService.updatePlaylistSchedule(
            playlistId,
            updatePlaylistScheduleDto as CreatePlaylistScheduleDto,
        );
    }

    @CheckPermission(ControllerAction.create_playlist)
    @Post(':playlistId/copy')
    copyPlaylist(@Param('playlistId') playlistId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(playlistId), { playlistId: 'INVALID' });

        return this.playlistService.copyPlaylist(playlistId);
    }

    @CheckPermission(ControllerAction.update_playlist_screens)
    @Post(':playlistId/screens')
    addScreensToPlaylist(@Param('playlistId') playlistId: string, @Body('screenIds') screenIds: string[]) {
        this.validationService.throwBadRequestExceptionIf(!validate(playlistId), { playlistId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(
            !Array.isArray(screenIds) || !screenIds.every((id) => validate(id)),
            {
                screenIds: 'INVALID',
            },
        );

        return this.playlistService.addScreensToPlaylist(playlistId, screenIds);
    }

    @CheckPermission(ControllerAction.update_playlist)
    @Patch(':playlistId')
    updatePlaylist(@Param('playlistId') playlistId: string, @Body() updatePlaylistDto: UpdatePlaylistDto) {
        const { intervalDuration, title, draft, resolution, sync, regions, templateId } = updatePlaylistDto;

        this.validationService.throwBadRequestExceptionIf(!validate(playlistId), { playlistId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(
            intervalDuration !== undefined && (isNaN(intervalDuration) || intervalDuration < 0),
            { intervalDuration: 'INVALID' },
        );
        this.validationService.throwBadRequestExceptionIf(!!title && title.length === 0, { title: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!['boolean', 'undefined'].includes(typeof draft), {
            draft: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(!['boolean', 'undefined'].includes(typeof sync), {
            sync: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(
            !!resolution &&
                (!['number'].includes(typeof resolution.width) || !['number'].includes(typeof resolution.height)),
            { resolution: 'INVALID' },
        );
        this.validationService.throwBadRequestExceptionIf(
            !!regions &&
                regions?.some(
                    (region) =>
                        !['number'].includes(typeof region.id) ||
                        !['number'].includes(typeof region.width) ||
                        !['number'].includes(typeof region.height) ||
                        !['number'].includes(typeof region.xPosition) ||
                        !['number'].includes(typeof region.yPosition) ||
                        !['number'].includes(typeof region.zIndex),
                ),
            { regions: 'INVALID' },
        );
        this.validationService.throwBadRequestExceptionIf(!!templateId && !validate(templateId), {
            templateId: 'INVALID',
        });

        return this.playlistService.updatePlaylist(playlistId, updatePlaylistDto);
    }

    @CheckPermission(ControllerAction.delete_playlist)
    @Patch('delete/playlists')
    remove(@Body('playlistIds') playlistIds: string | string[]) {
        this.validationService.throwBadRequestExceptionIf(
            Array.isArray(playlistIds)
                ? playlistIds.some((playlistId) => !validate(playlistId))
                : !validate(playlistIds),
            { playlistIds: 'INVALID' },
        );

        return this.playlistService.remove(playlistIds);
    }
}
