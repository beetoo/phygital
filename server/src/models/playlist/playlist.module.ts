import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { PlaylistService } from './playlist.service';
import { PlaylistController } from './playlist.controller';
import { Content } from '../content/entities/content.entity';
import { Playlist } from './entities/playlist.entity';
import { Screen } from '../screen/entities/screen.entity';
import { PlaylistItem } from './entities/playlist-item.entity';
import { Scenario } from '../scenario/entities/scenario.entity';
import { ScreenRequestLogger } from '../admin/screen-request-logger/entities/screen-request-logger.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Playlist, PlaylistItem, Screen, Content, Scenario, ScreenRequestLogger])],
    controllers: [PlaylistController],
    providers: [PlaylistService],
})
export class PlaylistModule {}
