import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { In, Repository } from 'typeorm';
import { isEqual, uniq } from 'lodash';

import { getUniqueNameUsingPostfix, mapScreens } from '../../helpers';
import { Content } from '../content/entities/content.entity';
import { Screen } from '../screen/entities/screen.entity';
import { UpdatePlaylistDto } from './dto/update-playlist.dto';
import { Playlist } from './entities/playlist.entity';
import { Scenario } from '../scenario/entities/scenario.entity';
import { PlaylistItem } from './entities/playlist-item.entity';
import { CreatePlaylistScheduleDto } from './dto/create-playlist-schedule.dto';
import { UpdatePlaylistItemsDto } from './dto/update-playlist-content.dto';
import { ScreenRequestLogger } from '../admin/screen-request-logger/entities/screen-request-logger.entity';
import { PlaylistItemType } from '../../../../common/types';
import { defaultRegions } from './helpers';
import { ValidationService } from '../../services/validation/validation.service';

@Injectable()
export class PlaylistService {
    constructor(
        @InjectRepository(Playlist)
        private playlistRepository: Repository<Playlist>,
        @InjectRepository(PlaylistItem)
        private playlistItemRepository: Repository<PlaylistItemType>,
        @InjectRepository(Scenario)
        private scenarioRepository: Repository<Scenario>,
        @InjectRepository(Screen)
        private screenRepository: Repository<Screen>,
        @InjectRepository(Content)
        private contentRepository: Repository<Content>,
        @InjectRepository(ScreenRequestLogger)
        private screenRequestLoggerRepository: Repository<ScreenRequestLogger>,
        private eventEmitter: EventEmitter2,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
        private readonly validationService: ValidationService,
    ) {}

    async copyPlaylist(playlistId: string) {
        const copiedPlaylist = await this.playlistRepository.findOne({
            where: { id: playlistId },
            relations: {
                screens: true,
                playlistItems: { content: true, scenario: true },
            },
        });

        if (!copiedPlaylist) {
            throw this.validationService.notFoundException({ playlist: 'NOT_FOUND' });
        }

        const { organizationId, title, resolution, sync, log, schedule, playlistItems, screens, regions } =
            copiedPlaylist;

        const priorities = (
            await this.playlistRepository.find({
                where: { organizationId },
                select: { priority: true },
            })
        ).map(({ priority }) => priority);

        const maxPriority = priorities.length > 0 ? Math.max(...priorities) + 1 : 1;

        // создаём новый плейлист
        let playlist = new Playlist();
        playlist.organizationId = organizationId;
        playlist.title = 'Копия ' + title;
        playlist.draft = true;
        playlist.regions = regions;
        playlist.priority = maxPriority;
        playlist.resolution = resolution;
        playlist.sync = sync;
        playlist.log = log;
        playlist.schedule = schedule;
        playlist.screens = screens;

        playlist = await this.playlistRepository.save(playlist);

        if (playlistItems.length > 0) {
            const newPlaylistItems: PlaylistItem[] = [];

            for (const prevPlaylistItem of playlistItems) {
                const { name, delay, orderIndex, content, scenario, scenarioId, regionId } = prevPlaylistItem;

                const playlistItem = new PlaylistItem();
                playlistItem.regionId = regionId;
                playlistItem.name = name;
                playlistItem.delay = delay;
                playlistItem.orderIndex = orderIndex;
                playlistItem.playlistId = playlist.id;
                playlistItem.playlist = playlist;
                playlistItem.content = content;
                playlistItem.scenario = scenario;
                playlistItem.scenarioId = scenarioId;

                newPlaylistItems.push(playlistItem);
            }

            await this.playlistItemRepository.save(newPlaylistItems);
        }

        const screenIds = playlist.screens.map((screen) => screen?.id).filter((id) => id);

        this.eventEmitter.emit('screen.json.update', { screenIds });

        return { playlistId: playlist.id };
    }

    async updatePlaylistSchedule(playlistId: string, playlistScheduleDto: CreatePlaylistScheduleDto) {
        const playlist = await this.playlistRepository.findOne({
            where: { id: playlistId },
            relations: { screens: true },
        });

        if (!playlist) {
            throw this.validationService.notFoundException({ playlist: 'NOT_FOUND' });
        }

        const { startDay, endDay, startTime, endTime, byDay, broadcastAlways } = playlistScheduleDto;

        playlist.schedule = { startDay, endDay, startTime, endTime, byDay, broadcastAlways };
        playlist.updatedAt = new Date();

        await this.playlistRepository.save(playlist);

        const screenIds = playlist.screens.map((screen) => screen?.id);

        // кол-во экранов в плейлисте не меняется
        this.eventEmitter.emit('screen.json.update', { screenIds });

        return playlist.schedule;
    }

    async addScreensToPlaylist(playlistId: string, screenIds: string[]) {
        const parentPlaylist = await this.playlistRepository.findOne({
            where: { id: playlistId },
        });

        if (!parentPlaylist) {
            throw this.validationService.notFoundException({ playlist: 'NOT_FOUND' });
        }

        const prevScreenIds = uniq([
            ...(
                await this.screenRepository.find({
                    where: { playlists: { id: playlistId } },
                })
            ).map(({ id }) => id),
            ...screenIds,
        ]);

        if (screenIds.length === 0) {
            parentPlaylist.screens = [];
            await this.playlistRepository.save(parentPlaylist);

            // кол-во экранов в плейлисте меняется
            this.eventEmitter.emit('screen.json.update', { screenIds: prevScreenIds });

            return parentPlaylist;
        }

        const screens = await this.screenRepository.find({
            where: {
                id: In(screenIds),
            },
        });

        this.validationService.throwForbiddenExceptionIf(
            !screens.every((screen) => screen.organizationId === parentPlaylist.organizationId),
            { screens: 'SCREENS_AND_PLAYLIST_BELONG_TO_DIFFERENT_ORGANIZATIONS' },
        );

        this.validationService.throwBadRequestExceptionIf(screens.length !== screenIds.length, {
            screens: 'NOT_FOUND',
        });

        parentPlaylist.screens = screens;

        const updatedPlaylist = await this.playlistRepository.save({ ...parentPlaylist, updatedAt: new Date() });

        // кол-во экранов в плейлисте меняется
        this.eventEmitter.emit('screen.json.update', { screenIds: prevScreenIds });

        return { ...updatedPlaylist };
    }

    async findOnePlaylist(playlistId: string) {
        const playlist = await this.playlistRepository.findOne({
            where: { id: playlistId },
            relations: {
                screens: { location: true },
                playlistItems: { content: true, scenario: true },
            },
        });

        if (!playlist) {
            throw this.validationService.notFoundException({ playlist: 'NOT_FOUND' });
        }

        const items = playlist.playlistItems.map(
            ({ scenario, delay, orderIndex, content, name: _, ...playlistItem }) => {
                return {
                    ...playlistItem,
                    scenarioId: scenario?.id ?? null,
                    duration: delay,
                    order: orderIndex,
                    content: content ? { ...content, src: content.src, playlistItems: undefined } : null,
                };
            },
        );

        const identifications = playlist.screens.map(({ identification }) => identification);
        const screenLogs = await this.screenRequestLoggerRepository.findBy({ identification: In(identifications) });

        const scenarios = playlist.playlistItems
            .map((item) => item.scenario)
            .filter((scenario) => scenario)
            .reduce((scenarios, scenario) => {
                if (scenarios.some((item) => item.id === scenario.id)) {
                    return scenarios;
                }
                return [...scenarios, scenario];
            }, [] as Scenario[]);

        return {
            ...playlist,
            screens: mapScreens(playlist.screens, screenLogs),
            scenarios,
            items,
        };
    }

    async updateContent(playlistId: string, { items: playlistItemsDto }: UpdatePlaylistItemsDto) {
        const playlist = await this.playlistRepository.findOne({
            where: { id: playlistId },
            relations: { screens: true, playlistItems: true },
        });

        if (!playlist) {
            throw this.validationService.notFoundException({ playlist: 'NOT_FOUND' });
        }

        // playlistItemsDto = playlistItemsDto.filter((item) =>
        //     playlist.regions.some((region) => region.id === item.regionId),
        // );

        const screenIds = playlist.screens.map(({ id }) => id);

        const playlistItemsDtoWithIds = playlistItemsDto.filter((item) => item.id);

        // удаление существующих элементов, которых нет в dto
        const removablePlaylistItems = playlist.playlistItems.filter(
            ({ id }) => !playlistItemsDtoWithIds.map(({ id }) => id).includes(id),
        );
        await this.playlistItemRepository.remove(removablePlaylistItems);

        // обновляем или создаем элементы
        for (const playlistItemDto of playlistItemsDto) {
            const playlistItem = playlistItemDto.id
                ? ((await this.playlistItemRepository.findOne({
                      where: { id: playlistItemDto.id },
                      relations: { scenario: true },
                  })) ?? new PlaylistItem())
                : new PlaylistItem();

            playlistItem.regionId = playlistItemDto.regionId;
            playlistItem.type = playlistItemDto.type;
            playlistItem.properties = playlistItemDto.properties;
            playlistItem.scenarioId = playlistItemDto.scenarioId;
            playlistItem.delay = playlistItemDto.duration;
            playlistItem.orderIndex = playlistItemDto.order;
            playlistItem.playlistId = playlistId;
            playlistItem.playlist = playlist;
            playlistItem.scenario = undefined;

            const content = playlistItemDto.contentId
                ? await this.contentRepository.findOneBy({ id: playlistItemDto.contentId })
                : null;

            if (content) {
                playlistItem.content = content;
            }

            if (playlistItemDto.scenarioId) {
                const scenario = await this.scenarioRepository.findOneBy({
                    id: playlistItemDto.scenarioId,
                    organizationId: playlist.organizationId,
                });

                if (scenario) {
                    playlistItem.scenario = scenario;
                }
            }

            await this.playlistItemRepository.save(playlistItem);
        }

        if (playlistItemsDto.length > 0) {
            // обновление updatedAt у плейлиста
            await this.playlistRepository.update(playlistId, { updatedAt: new Date() });

            this.eventEmitter.emit('screen.json.update', { screenIds });
        }

        // формирование ответа
        const [contents, plItems] = await Promise.all([
            this.contentRepository.find({ relations: { playlistItems: true } }),
            this.playlistItemRepository.find({
                where: { playlistId },
                relations: { scenario: true },
            }),
        ]);

        const items = plItems.map(({ scenario, delay, orderIndex, name: _, ...playlistItem }) => {
            const content =
                contents.find(({ playlistItems }) => playlistItems.find(({ id }) => playlistItem.id === id)) ??
                ({} as Content);

            return {
                ...playlistItem,
                contentId: content.id ?? null,
                scenarioId: scenario?.id ?? null,
                duration: delay,
                order: orderIndex,
            };
        });

        return { items };
    }

    async updatePlaylist(
        playlistId: string,
        { intervalDuration, title, draft, resolution, sync, regions, templateId }: UpdatePlaylistDto,
    ) {
        const playlist = await this.playlistRepository.findOne({
            where: { id: playlistId },
            relations: { screens: true, playlistItems: true },
        });

        if (!playlist) {
            throw this.validationService.notFoundException({ playlist: 'NOT_FOUND' });
        }

        if (title) {
            const { organizationId } = playlist;
            // меняем имя на уникальное, если такое уже есть
            const playlists = await this.playlistRepository.find({ where: { organizationId } });
            const playlistNames = playlists.map(({ title }) => title);

            playlist.title = getUniqueNameUsingPostfix(playlistNames, title, playlist.title).slice(0, 125);
        }

        const prevIntervalDuration = playlist.intervalDuration;
        const prevDraft = playlist.draft;
        const prevSync = playlist.sync;
        const prevResolution = playlist.resolution;
        const prevRegions = playlist.regions;
        const prevPlaylistItems = playlist.playlistItems;
        const prevTemplateId = playlist.templateId;

        if (intervalDuration !== undefined) {
            if (playlist.type === 'interval' && intervalDuration < 15) {
                intervalDuration = 15;
            }

            playlist.intervalDuration = intervalDuration;
        }

        if (draft !== undefined) {
            playlist.draft = draft;
        }

        if (resolution) {
            const prevPlaylistResolution = playlist.resolution;

            playlist.resolution = resolution;

            const changeRegionsRequired =
                playlist.regions.length === 1 &&
                playlist.regions[0].width === prevPlaylistResolution.width &&
                playlist.regions[0].height === prevPlaylistResolution.height;

            if (changeRegionsRequired) {
                playlist.regions = (playlist.regions ?? defaultRegions(playlist.resolution)).map((region) => ({
                    ...region,
                    width: playlist.resolution.width,
                    height: playlist.resolution.height,
                }));
            }
        }

        if (sync !== undefined) {
            playlist.sync = sync;

            if (!playlist.sync) {
                await this.cacheManager.del(`player:smil:syncGroupIds:${playlistId}`);
            }
        }

        if (regions !== undefined) {
            playlist.regions = regions && regions.length > 0 ? regions : defaultRegions(playlist.resolution);

            playlist.playlistItems =
                regions.length > 0
                    ? playlist.playlistItems.filter((item) =>
                          playlist.regions?.some((region) => region.id === item.regionId),
                      )
                    : [];
        }

        if (templateId !== undefined) {
            playlist.templateId = templateId;
        }

        await this.playlistRepository.save(playlist);

        const updateCacheRequired =
            prevIntervalDuration !== playlist.intervalDuration ||
            prevDraft !== playlist.draft ||
            prevSync !== playlist.sync ||
            !isEqual(prevResolution, playlist.resolution) ||
            !isEqual(prevRegions, playlist.regions) ||
            !isEqual(prevPlaylistItems, playlist.playlistItems) ||
            prevTemplateId !== playlist.templateId;

        const screenIds = playlist.screens.map((screen) => screen?.id);

        if (updateCacheRequired) {
            this.eventEmitter.emit('screen.json.update', { screenIds });
        }

        return { playlistId };
    }

    async remove(playlistIds: string | string[]) {
        const playlists = await this.playlistRepository.find({
            where: { id: Array.isArray(playlistIds) ? In(playlistIds) : playlistIds },
            relations: { screens: true },
        });

        const ids = Array.isArray(playlistIds) ? playlistIds : [playlistIds];

        const deletePromises = ids.map(async (playlistId) => {
            await this.cacheManager.del(`player:smil:syncGroupIds:${playlistId}`);
            await this.cacheManager.get(`triggeredItems:${playlistId}`);
        });

        await Promise.all(deletePromises);

        // удаление плейлиста
        await this.playlistRepository.delete(playlistIds);

        const screenIds = playlists.flatMap((playlist) => playlist.screens).map((screen) => screen?.id);

        this.eventEmitter.emit('screen.json.update', { screenIds });

        return { message: 'OK' };
    }
}
