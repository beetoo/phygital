import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';

import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { Organization } from '../organization/entities/organization.entity';
import { JwtConfigService } from '../../configs/jwt-config-service';
import { EmailService } from '../../services/email.service';
import { User } from '../user/entities/user.entity';
import { UserActivityLogEmitService } from '../../services/user-activity-log-emit.service';
import { TelegramBotService } from '../../services/telegram-bot.service';
import { UserSessionsService } from '../../services/user-sessions.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([Organization, User]),
        JwtModule.registerAsync({
            useClass: JwtConfigService,
        }),
    ],
    controllers: [AuthController],
    providers: [AuthService, EmailService, UserActivityLogEmitService, TelegramBotService, UserSessionsService],
})
export class AuthModule {}
