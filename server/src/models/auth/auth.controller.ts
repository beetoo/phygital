import { Controller, Get, Post, Body, Param, Req } from '@nestjs/common';
import { Request } from 'express';
import { isEmail } from 'validator';

import { AuthService } from './auth.service';
import { CreateAuthDto } from './dto/create-auth.dto';
import { SignInDto } from './dto/sign-in.dto';
import { OrganizationProductEnum } from '../../../../common/types';
import { UserActivityLogEmitService } from '../../services/user-activity-log-emit.service';
import { Public } from '../../decorators/public.decorator';
import { UserSessionsService } from '../../services/user-sessions.service';
import { ValidationService } from '../../services/validation/validation.service';

@Public()
@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
        private readonly userLogEmitService: UserActivityLogEmitService,
        private readonly userSessionsService: UserSessionsService,
        private readonly validationService: ValidationService,
    ) {}

    @Post('registration')
    async register(@Body() createAuthDto: CreateAuthDto, @Req() request: Request) {
        const { name, surname, email, password, product } = createAuthDto;

        this.validationService.throwBadRequestExceptionIf(!name, { name: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!surname, { surname: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!email, { email: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!password, { password: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(
            !!product && !Object.values(OrganizationProductEnum).includes(product),
            {
                product: 'INVALID',
            },
        );

        const { userId, role, token, status } = await this.authService.createUser(createAuthDto);

        this.userSessionsService.addUserSession(token, request).finally();

        if (token) {
            return {
                userId,
                role,
                name,
                surname,
                email,
                token,
                status,
            };
        } else {
            return { email };
        }
    }

    @Post('signin')
    async signIn(@Body() signInDto: SignInDto, @Req() request: Request) {
        this.validationService.throwBadRequestExceptionIf(!signInDto.email, { email: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!signInDto.password, { password: 'IS_REQUIRED' });

        const { userId, role, email, token, name, surname, status } = await this.authService.signIn(signInDto);

        Promise.resolve().then(async () => {
            await this.userLogEmitService.emitLogEvent('user.signedIn', request, userId);
            await this.userSessionsService.addUserSession(token, request);
        });

        return {
            userId,
            role,
            name,
            surname,
            email,
            status,
            token,
        };
    }

    @Get('signout')
    async signOut(@Req() request: Request) {
        Promise.resolve().then(async () => {
            await this.userLogEmitService.emitLogEvent('user.signedOut', request);
            await this.userSessionsService.removeUserSession(request);
        });

        return { message: 'OK' };
    }

    @Get('confirm-registration/:emailVerificationToken/check')
    async confirmRegistration(
        @Param('emailVerificationToken') emailVerificationToken: string,
        @Req() request: Request,
    ) {
        this.validationService.throwBadRequestExceptionIf(!emailVerificationToken, {
            emailVerificationToken: 'IS_REQUIRED',
        });
        this.validationService.throwBadRequestExceptionIf(emailVerificationToken.length !== 64, {
            emailVerificationToken: 'INVALID',
        });

        const { token } = await this.authService.confirmRegistration(emailVerificationToken);

        this.userSessionsService.updateUserSession(token, request).finally();

        return { token };
    }

    // отправка письма о восстановлении пароля
    @Post('recovery-password')
    sendResetPasswordEmail(@Body('email') email: string) {
        this.validationService.throwBadRequestExceptionIf(!email, { email: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!isEmail(email), { email: 'INVALID' });

        return this.authService.sendResetPasswordEmail(email);
    }

    @Post('recovery-password/:token')
    async updatePassword(
        @Param('token') authToken: string,
        @Body('password') password: string,
        @Body('timezoneOffset') timezoneOffset: number,
        @Req() request: Request,
    ) {
        this.validationService.throwBadRequestExceptionIf(!authToken, { token: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(authToken.length !== 64, { token: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!password, { password: 'IS_REQUIRED' });

        const { userId, role, token, name, surname, email } = await this.authService.updatePassword(
            authToken,
            password,
            timezoneOffset,
        );

        this.userSessionsService.addUserSession(token, request).finally();

        return {
            userId,
            role,
            name,
            surname,
            email,
            token,
        };
    }
}
