export const DEFAULT_EXPIRES_TOKEN_TIME = 86_400_000 * 30;

const expiresTokenTime = Number(process.env.EXPIRES_TOKEN_TIME ?? DEFAULT_EXPIRES_TOKEN_TIME);

export const getExpiresTime = (): Date => new Date(Date.now() + expiresTokenTime);
