import { OrganizationProductEnum } from '../../../../../common/types';

export class CreateAuthDto {
    readonly title: string;
    readonly name: string;
    readonly surname: string;
    readonly phone: string;
    readonly email: string;
    readonly password: string;
    readonly product?: OrganizationProductEnum;
}
