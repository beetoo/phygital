import cluster from 'node:cluster';
import { randomBytes } from 'node:crypto';
import { Inject, Injectable } from '@nestjs/common';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { Cache } from 'cache-manager';
import { Repository } from 'typeorm';
import bcrypt from 'bcryptjs';
import { addHours, format } from 'date-fns';

import { SignInDto } from './dto/sign-in.dto';
import { EmailService } from '../../services/email.service';
import { User } from '../user/entities/user.entity';
import { isProductionServer } from '../../constants';
import { CreateAuthDto } from './dto/create-auth.dto';
import { TelegramBotService } from '../../services/telegram-bot.service';
import { ValidationService } from '../../services/validation/validation.service';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        private jwtService: JwtService,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
        private emailService: EmailService,
        private telegramBotService: TelegramBotService,
        private readonly validationService: ValidationService,
    ) {}

    async createUser({ name, surname, email, password }: CreateAuthDto) {
        email = email.slice(0, 125).trim();
        name = name.slice(0, 125).trim();
        surname = surname.slice(0, 125).trim();

        // ищем юзера по email в нашей базе данных
        let user = await this.userRepository.findOneBy({ email });

        // если юзер существует, то возвращаем ошибку
        this.validationService.throwBadRequestExceptionIf(!!user, { email: 'EMAIL_ALREADY_EXISTS' });

        const emailVerificationToken = randomBytes(32).toString('hex');
        const passwordHash = await bcrypt.hash(password, 10);

        user = new User();

        user.name = name;
        user.surname = surname;
        user.email = email;
        user.status = 'pending';
        user.password = passwordHash;
        user.emailVerificationToken = emailVerificationToken;

        user = await this.userRepository.save(user);

        // есть
        const token = this.jwtService.sign({ userId: user.id });

        await this.sendRegistrationNotificationTelegramMessage(name, surname, email);
        await this.emailService.sendConfirmRegistrationEmail(email, emailVerificationToken);

        return {
            userId: user.id,
            role: user.role,
            name: user.name,
            surname: user.surname,
            email: user.email,
            status: user.status,
            token,
        };
    }

    async signIn({ email, password }: SignInDto) {
        email = email.trim();

        // ищем пользователя по email
        const user = await this.userRepository.findOneBy({ email });

        if (!user) {
            throw this.validationService.badRequestException({ email: 'NOT_FOUND' });
        }

        const { name, surname, role, status } = user;

        const isPasswordMatch = await bcrypt.compare(password, user.password);

        this.validationService.throwUnauthorizedExceptionIf(!isPasswordMatch, { password: 'INVALID_PASSWORD' });

        // есть
        const token = this.jwtService.sign({ userId: user.id });

        return { userId: user.id, role, token, name, surname, email: user.email, status };
    }

    async sendResetPasswordEmail(email: string) {
        email = email.trim();

        const user = await this.userRepository.findOneBy({ email });

        if (!user) {
            throw this.validationService.badRequestException({ email: 'NOT_FOUND' });
        }

        const token = randomBytes(32).toString('hex');

        await this.cacheManager.set(token, { email }, 3600 * 1000);

        try {
            await this.emailService.sendResetPasswordEmail(email, token);
        } catch {
            //
        }

        return { message: 'OK' };
    }

    async updatePassword(token: string, password: string, timezoneOffset = 180) {
        const cacheValue = (await this.cacheManager.get(token)) as {
            email: string;
        };

        this.validationService.throwNotFoundExceptionIf(!cacheValue, { token: 'NOT_FOUND' });

        await this.cacheManager.del(token);

        const user = await this.userRepository.findOneBy({ email: cacheValue.email });

        if (!user) {
            throw this.validationService.badRequestException({ email: 'NOT_FOUND' });
        }

        const { email, name, surname, role } = user;

        user.password = await bcrypt.hash(password, 10);
        user.status = 'active';

        await this.userRepository.save(user);

        const authToken = this.jwtService.sign({ userId: user.id });

        try {
            await this.emailService.sendChangePasswordEmail(name, email, timezoneOffset);
        } catch {
            //
        }

        return {
            userId: user.id,
            role,
            email,
            name,
            surname,
            token: authToken,
        };
    }

    async confirmRegistration(emailVerificationToken: string) {
        const user = await this.userRepository.findOneBy({ emailVerificationToken });

        if (!user) {
            throw this.validationService.notFoundException({ emailVerificationToken: 'NOT_FOUND' });
        }

        user.status = 'active';
        user.emailVerificationToken = '';

        await this.userRepository.save(user);

        // есть
        const token = this.jwtService.sign({ userId: user.id });

        return { token };
    }

    async sendRegistrationNotificationTelegramMessage(name: string, surname: string, email: string) {
        if (isProductionServer) {
            console.log('Telegram bot send message on worker id', cluster.worker?.id);

            const text = `Новый пользователь\n\n${name} ${surname}\n${email}\n\n${format(addHours(new Date(), 3), 'dd/MM/yyyy HH:mm')} (по МСК)`;

            await this.telegramBotService.sendMessage(text);
        }
    }
}
