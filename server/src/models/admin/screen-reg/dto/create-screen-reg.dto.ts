export class CreateScreenRegDto {
    readonly identification: string;
    readonly verificationHash: string;
}
