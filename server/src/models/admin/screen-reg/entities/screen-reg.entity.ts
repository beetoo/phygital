import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, DeleteDateColumn } from 'typeorm';

import { ScreenRegType } from '../../../../../../common/types';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class ScreenReg implements ScreenRegType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ default: '' })
    identification: string;

    @Column({ default: '' })
    verificationHash: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;
}
