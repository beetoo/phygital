import fs from 'node:fs';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { ScreenReg } from './entities/screen-reg.entity';
import { generateHashImage } from './helpers/image-hash';
import { UPLOAD_PATH } from '../../../constants';
import { crc32 } from '../helpers';

@Injectable()
export class ScreenRegService {
    constructor(
        @InjectRepository(ScreenReg)
        private screenRegRepository: Repository<ScreenReg>,
    ) {}

    async addScreenRegVerification(identification: string): Promise<ScreenReg> {
        const item = new ScreenReg();
        item.identification = identification;
        item.verificationHash = crc32(identification).toString(16).toUpperCase();
        await this.screenRegRepository.save(item);
        const path = `${UPLOAD_PATH}/smil/hash/`;
        fs.mkdir(path, { recursive: true }, (err) => {
            if (err) {
                console.error('Cannot create directory', err);
            }
        });
        await generateHashImage(path, item.identification, item.verificationHash, '');
        return item;
    }

    getScreenRegVerification(identification: string): Promise<ScreenReg | null> {
        return this.screenRegRepository.findOneBy({ identification });
    }
}
