import { createCanvas, SKRSContext2D } from '@napi-rs/canvas';
import { promises } from 'node:fs';

const wrapText = function (
    ctx: SKRSContext2D,
    text: string,
    x: number,
    y: number,
    maxWidth: number,
    lineHeight: number,
): [any[], number] {
    // First, split the words by spaces
    const words = text.split(' ');
    // Then we'll make a few variables to store info about our line
    let line = '';
    let testLine = '';
    // wordArray is what we'l' return, which will hold info on
    // the line text, along with its x and y starting position
    const wordArray: (string | number)[][] = [];
    // totalLineHeight will hold info on the line height
    let totalLineHeight = 0;

    // Next we iterate over each word
    for (let n = 0; n < words.length; n++) {
        // And test out its length
        testLine += `${words[n]} `;
        const metrics = ctx.measureText(testLine);
        const testWidth = metrics.width;
        // If it's too long, then we start a new line
        if (testWidth > maxWidth && n > 0) {
            wordArray.push([line, x, y]);
            y += lineHeight;
            totalLineHeight += lineHeight;
            line = `${words[n]} `;
            testLine = `${words[n]} `;
        } else {
            // Otherwise we only have one line!
            line += `${words[n]} `;
        }
        // Whenever all the words are done, we push whatever is left
        if (n === words.length - 1) {
            wordArray.push([line, x, y]);
        }
    }

    // And return the words in array, along with the total line height
    // which will be (totalLines - 1) * lineHeight
    return [wordArray, totalLineHeight];
};

// This functiona accepts 5 arguments:
// canonicalName: this is the name we'll use to save our image
// gradientColors: an array of two colors, i.e. [ '#ffffff', '#000000' ], used for our gradient
// articleName: the title of the article or site you want to appear in the image
// articleCategory: the category which that article sits in - or the subtext of the article
// emoji: the emoji you want to appear in the image.
export const generateHashImage = async function (
    folder: string,
    canonicalName: string,
    articleName: string,
    articleCategory: string,
) {
    const width = 1920;
    const height = 1080;
    const canvas = createCanvas(width, height);
    const ctx = canvas.getContext('2d');

    // Add our title text
    ctx.font = '95px InterBold';
    ctx.fillStyle = 'white';
    ctx.fillStyle = 'rgba(255, 255, 255, 0.5)';
    const wrappedText = wrapText(ctx, articleName, (width - 460) / 2, 1200, 1200, 100);
    wrappedText[0].forEach(function (item) {
        // We will fill our text which is item[0] of our array, at coordinates [x, y]
        // x will be item[1] of our array
        // y will be item[2] of our array, minus the line height (wrappedText[1]), minus the height of the emoji (200px)
        ctx.fillText(item[0], item[1], item[2] - wrappedText[1] - 200); // 200 is height of an emoji
    });

    // Add our category text to the canvas
    ctx.font = '50px InterMedium';
    ctx.fillStyle = 'rgba(255, 255, 255, 0.5)';
    ctx.fillText(articleCategory, (width - 400) / 2, 1000 - wrappedText[1] - 100); // 853 - 200 for emoji, -100 for line height of 1
    const pngData = await canvas.encode('png'); // JPEG, AVIF and WebP are also supported
    await promises.writeFile(folder + '/' + canonicalName + '.png', pngData);
    return 'Images have been successfully created!';
};
