import { Controller, Get, Query } from '@nestjs/common';

import { AdminService } from './admin.service';
import { CONTENT_TYPE, OrganizationStatusType, PaymentStatusType } from '../../../../common/types';
import { Admin } from '../../decorators/admin.decorator';
import { Accounting } from '../../decorators/accounting.decorator';
import { ViewOnly } from '../../decorators/view-only.decorator';
import { ValidationService } from '../../services/validation/validation.service';
import {
    GetActivityLogsQueryType,
    GetContentQueryType,
    GetOrganizationsQueryType,
    GetPaymentsQueryType,
    GetScreensQueryType,
    GetTicketsQueryType,
    GetUsersQueryType,
} from './types';

@Admin()
@ViewOnly()
@Controller('admin')
export class AdminController {
    constructor(
        private readonly adminService: AdminService,
        private readonly validationService: ValidationService,
    ) {}

    @Accounting()
    @Get('organizations')
    getOrganizations(@Query() { search, status, ...rest }: GetOrganizationsQueryType) {
        const { take, skip } = this.checkTakeAndSkipConditions(rest);

        this.validationService.throwBadRequestExceptionIf(
            !!status &&
                !(['new', 'trial', 'paid', 'partner', 'expired', 'cancelled'] as OrganizationStatusType[]).includes(
                    status,
                ),
            {
                status: 'INVALID',
            },
        );

        return this.adminService.findOrganizations({ take, skip, status, search });
    }

    @Get('users')
    getUsers(@Query() { search, ...rest }: GetUsersQueryType) {
        const { take, skip } = this.checkTakeAndSkipConditions(rest);

        return this.adminService.findUsers({ take, skip, search });
    }

    @Get('screens')
    getScreens(@Query() { connected, status, isEmpty, ...rest }: GetScreensQueryType) {
        const { take, skip } = this.checkTakeAndSkipConditions(rest);

        this.validationService.throwBadRequestExceptionIf(!!connected && !['true', 'false'].includes(connected), {
            connected: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(
            !!status && !status.split(',').every((status) => ['online', 'offline', 'error'].includes(status)),
            {
                status: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(!!isEmpty && !['true', 'false'].includes(isEmpty), {
            isEmpty: 'INVALID',
        });

        return this.adminService.findScreens({ take, skip, status, connected, isEmpty });
    }

    // @Get('subscriptions')
    // getSubscriptions(@Query() { sortBy, order, ...rest }: GetSubscriptionsQueryType) {
    //     const { take, skip } = this.checkTakeAndSkipConditions(rest);
    //
    //     this.validationService.throwBadRequestExceptionIf(!!rest.paid && !['true', 'false'].includes(rest.paid), {
    //         paid: 'INVALID',
    //     });
    //     this.validationService.throwBadRequestExceptionIf(
    //         sortBy && !['createdAt', 'startDate', 'expirationDate'].includes(sortBy),
    //         {
    //             sortBy: 'INVALID',
    //         },
    //     );
    //     this.validationService.throwBadRequestExceptionIf(
    //         order && !['asc', 'desc'].includes((order as string).toLowerCase()),
    //         {
    //             order: 'INVALID',
    //         },
    //     );
    //
    //     const paid = rest.paid === undefined ? undefined : rest.paid === 'true';
    //
    //     return this.adminService.findSubscriptions({ take, skip, paid, order, sortBy });
    // }

    @Get('payments')
    getPayments(@Query() { status, ...rest }: GetPaymentsQueryType) {
        const { take, skip } = this.checkTakeAndSkipConditions(rest);

        this.validationService.throwBadRequestExceptionIf(
            !!status &&
                !status
                    .split(',')
                    .every((status) =>
                        (['pending', 'paid', 'cancelled'] as PaymentStatusType[]).includes(status as PaymentStatusType),
                    ),
            {
                status: 'INVALID',
            },
        );

        return this.adminService.findPayments({ take, skip, status });
    }

    @Get('content')
    getContent(@Query() { sortBy, order, type, ...rest }: GetContentQueryType) {
        const { take, skip } = this.checkTakeAndSkipConditions(rest);

        this.validationService.throwBadRequestExceptionIf(sortBy && !['size'].includes(sortBy), {
            sortBy: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(
            order && !['asc', 'desc'].includes((order as string).toLowerCase()),
            {
                order: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(
            !!type &&
                !(type.toLowerCase().split(',') as CONTENT_TYPE[]).every((val) =>
                    (['web', 'image', 'video'] as CONTENT_TYPE[]).includes(val),
                ),
            {
                type: 'INVALID',
            },
        );

        return this.adminService.findContent({ take, skip, sortBy, order, type });
    }

    @Get('logs')
    getUserActivityLogs(@Query() { order, ...rest }: GetActivityLogsQueryType) {
        const { take, skip } = this.checkTakeAndSkipConditions(rest);

        this.validationService.throwBadRequestExceptionIf(
            order && !['asc', 'desc'].includes((order as string).toLowerCase()),
            {
                order: 'INVALID',
            },
        );

        return this.adminService.findActivityLogs({ take, skip, order });
    }

    @Get('tickets')
    getTickets(@Query() { status, ...rest }: GetTicketsQueryType) {
        const { take, skip } = this.checkTakeAndSkipConditions(rest);

        this.validationService.throwBadRequestExceptionIf(
            !!status &&
                !status
                    .split(',')
                    .every((status) => ['resolved', 'not_resolved', 'new', 'in_progress'].includes(status)),
            {
                status: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(!!rest.closed && !['true', 'false'].includes(rest.closed), {
            closed: 'INVALID',
        });

        const closed = rest.closed === undefined ? undefined : rest.closed === 'true';

        return this.adminService.findTickets({ take, skip, status, closed });
    }

    private checkTakeAndSkipConditions(data: { take: string; skip: string }) {
        const take = Number(data.take);
        const skip = Number(data.skip);

        this.validationService.throwBadRequestExceptionIf(isNaN(take) || take < 0, { take: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(isNaN(skip) || skip < 0, { skip: 'INVALID' });

        return { take, skip };
    }
}
