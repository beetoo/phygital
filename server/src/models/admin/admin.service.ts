import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Not, Repository, In, IsNull, Like, FindManyOptions } from 'typeorm';
import { differenceInSeconds, format } from 'date-fns';

import { Organization } from '../organization/entities/organization.entity';
import { Screen } from '../screen/entities/screen.entity';
import { ScreenRequestLogger } from './screen-request-logger/entities/screen-request-logger.entity';
import { getOwnerUser } from '../organization/helpers';
import { Content } from '../content/entities/content.entity';
import { User } from '../user/entities/user.entity';
import { UserActivityLog } from '../user/entities/user-activity-log.entity';
import { getPlayerVersion } from '../../helpers';
import { SupportTicket } from '../support-ticket/entities/support-ticket.entity';
import { Payment } from '../payment/entities/payment.entity';
import {
    GetActivityLogsDataType,
    GetContentDataType,
    GetOrganizationsDataType,
    GetPaymentsDataType,
    GetScreensDataType,
    GetTicketsDataType,
    GetUsersDataType,
} from './types';

@Injectable()
export class AdminService {
    constructor(
        @InjectRepository(Organization)
        private organizationRepository: Repository<Organization>,
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(Screen)
        private screenRepository: Repository<Screen>,
        @InjectRepository(Content)
        private contentRepository: Repository<Content>,
        @InjectRepository(ScreenRequestLogger)
        private screenRequestLoggerRepository: Repository<ScreenRequestLogger>,
        @InjectRepository(UserActivityLog)
        private userActivityLogRepository: Repository<UserActivityLog>,
        @InjectRepository(SupportTicket)
        private ticketRepository: Repository<SupportTicket>,
        @InjectRepository(Payment)
        private paymentRepository: Repository<Payment>,
    ) {}

    async findOrganizations({ take, skip, search, status }: GetOrganizationsDataType) {
        const options: FindManyOptions<Organization> = {
            where: [
                { id: search ? search : undefined, status: status ? status : undefined },
                {
                    users: { email: search ? Like(`%${search}%`) : undefined },
                    status: status ? status : undefined,
                },
                { name: search ? Like(`%${search}%`) : undefined, status: status ? status : undefined },
            ],
            select: {
                id: true,
                name: true,
                createdAt: true,
                product: true,
                status: true,
                freeScreens: true,
                subscription: {},
            },
            relations: { users: true },
        };

        const organizationsCount = await this.organizationRepository.count({ ...options, relations: undefined });

        if (organizationsCount === 0) {
            return {
                count: 0,
                organizations: [],
            };
        }

        const organizations = await this.organizationRepository.find({ ...options, take, skip });

        if (organizations.length === 0) {
            return {
                count: organizationsCount,
                organizations: [],
            };
        }

        const organizationIds = organizations.map(({ id }) => id);

        const [screens, content] = await Promise.all([
            this.screenRepository.findBy({ organizationId: In(organizationIds) }),
            this.contentRepository.findBy({ organizationId: In(organizationIds) }),
        ]);

        const mappedOrganizations = organizations.map((organization) => {
            const { id, name, product, createdAt, status, freeScreens, subscription } = organization;

            const user = getOwnerUser(organization);

            const { screens: licenses, storage } = subscription ?? { screens: freeScreens, storage: 0 };

            const isOrganizationValid = !(status === 'expired' || status === 'cancelled');

            const total = isOrganizationValid ? storage : 0;

            return {
                id,
                title: name,
                email: user?.email ?? '',
                product,
                status,
                createdAt: format(createdAt, 'dd-MM-yyyy'),
                subscription,
                licenses,
                screensNumber: screens.filter(({ organizationId }) => organizationId === id).length,
                href: `organizations/${id}/main`,
                storage: {
                    used: content?.reduce((acc, curr) => acc + (curr.organizationId === id ? curr.size : 0), 0),
                    total,
                },
            };
        });

        return {
            count: organizationsCount,
            organizations: mappedOrganizations,
        };
    }

    async findUsers({ take, skip, search }: GetUsersDataType) {
        const options: FindManyOptions<User> = {
            where: [
                { email: search ? Like(`%${search}%`) : undefined },
                { name: search ? Like(`%${search}%`) : undefined },
            ],
            select: {
                id: true,
                name: true,
                surname: true,
                email: true,
                phone: true,
                role: true,
                status: true,
                createdAt: true,
                language: true,
                lastActiveAt: true,
            },
            relations: { organizations: true },
        };

        const usersCount = await this.userRepository.count(options);

        if (usersCount === 0) {
            return { count: 0, users: [] };
        }

        const users = await this.userRepository.find({ ...options, take, skip });

        return { count: usersCount, users };
    }

    async findScreens({ skip, take, status, connected, isEmpty }: GetScreensDataType) {
        const options: FindManyOptions<Screen> = {
            where: {
                organizationId: !connected ? undefined : connected === 'true' ? Not(IsNull()) : IsNull(),
            },
        };

        let screens = (await this.screenRepository.find(options))
            .map((screen) => ({
                ...screen,
                status: differenceInSeconds(new Date(), new Date(screen.aliveAt)) > 180 ? 'offline' : 'online',
            }))
            .filter((screen) => (status ? status.split(',').includes(screen.status) : true));

        let screensCount = screens.length;

        if (screens.length === 0) {
            return {
                count: 0,
                screens: [],
            };
        }

        if (isEmpty) {
            screens = screens.filter((screen) =>
                isEmpty === 'true' ? screen.isEmpty : isEmpty === 'false' ? !screen.isEmpty : true,
            );

            screensCount = screens.length;
        }

        screens = screens = screens.slice(skip, skip + take);

        if (screens.length === 0) {
            return {
                count: screensCount,
                screens: [],
            };
        }

        const identifications = screens.map(({ identification }) => identification);
        const organizationIds = screens.map(({ organizationId }) => organizationId);

        const [screenLogs, organizations] = await Promise.all([
            this.screenRequestLoggerRepository.findBy({ identification: In(identifications) }),
            this.organizationRepository.findBy({ id: In(organizationIds) }),
        ]);

        const mappedScreens = screens.map(({ id, organizationId, identification, ...screen }) => {
            const currentOrganization = organizations.find(({ id }) => id === organizationId);
            const organization = currentOrganization
                ? { id: currentOrganization.id, name: currentOrganization.name }
                : null;

            return {
                id,
                identification,
                ...screen,
                organization,
                appVersion: getPlayerVersion(identification, screenLogs),
            };
        });

        return {
            count: screensCount,
            screens: mappedScreens,
        };
    }

    async findPayments({ skip, take, status }: GetPaymentsDataType) {
        const options: FindManyOptions<Payment> = {
            where: {
                status: status ? In(status.split(',')) : undefined,
            },
            relations: { organization: true },
        };

        const paymentCount = await this.paymentRepository.count(options);

        if (paymentCount === 0) {
            return {
                count: 0,
                payments: [],
            };
        }

        const payments = (await this.paymentRepository.find({ ...options, take, skip })).map(
            ({ total, organization, organizationId, ...payment }) => ({
                ...payment,
                total,
                organization: organization ? { id: organization.id, name: organization.name } : null,
            }),
        );

        if (payments.length === 0) {
            return {
                count: paymentCount,
                payments: [],
            };
        }

        return {
            count: paymentCount,
            payments,
        };
    }

    async findContent({ skip, take, type, sortBy, order = 'desc' }: GetContentDataType) {
        const options: FindManyOptions<Content> = {
            where: type ? { type: In(type.split(',')) } : undefined,
            order: sortBy ? { [sortBy]: order } : undefined,
        };

        const contentCount = await this.contentRepository.count(options);

        if (contentCount === 0) {
            return {
                count: 0,
                content: [],
            };
        }

        const content = await this.contentRepository.find({ ...options, take, skip });

        if (content.length === 0) {
            return {
                count: contentCount,
                content: [],
            };
        }

        const organizationIds = content.map(({ organizationId }) => organizationId);

        const organizations = await this.organizationRepository.find({
            where: { id: In(organizationIds) },
            select: { id: true, name: true },
        });

        const mappedContent = content.map(({ organizationId, ...contentItem }) => ({
            ...contentItem,
            organization: organizations.find((organization) => organization.id === organizationId) ?? null,
        }));

        return {
            count: contentCount,
            content: mappedContent,
        };
    }

    async findActivityLogs({ skip, take, order = 'desc' }: GetActivityLogsDataType) {
        const activityLogsCount = await this.userActivityLogRepository.count();

        if (activityLogsCount === 0) {
            return {
                count: 0,
                logs: [],
            };
        }

        const activityLogs = await this.userActivityLogRepository.find({ order: { timestamp: order }, take, skip });

        return {
            count: activityLogsCount,
            logs: activityLogs,
        };
    }

    async findTickets({ skip, take, status, closed }: GetTicketsDataType) {
        const options: FindManyOptions<SupportTicket> = {
            where: {
                status: status ? In(status.split(',')) : undefined,
                closedAt: closed === undefined ? undefined : closed ? Not(IsNull()) : IsNull(),
            },
            order: { messages: { createdAt: 'DESC' } },
            relations: { messages: true },
        };

        const ticketCount = await this.ticketRepository.countBy({
            status: status ? In(status.split(',')) : undefined,
            closedAt: closed === undefined ? undefined : closed ? Not(IsNull()) : IsNull(),
        });

        if (ticketCount === 0) {
            return {
                count: 0,
                tickets: [],
            };
        }

        const [rawTickets, users] = await Promise.all([
            this.ticketRepository.find(options),
            this.userRepository.find({
                select: { id: true, name: true, surname: true, email: true, phone: true },
            }),
        ]);

        const slicedTickets = rawTickets.slice(skip, skip + take);

        if (slicedTickets.length === 0) {
            return {
                count: ticketCount,
                tickets: [],
            };
        }

        const tickets = slicedTickets.map(({ messages, userId, ...ticket }) => {
            const newMessageCount = messages.filter(
                (message) => message.senderRole === 'user' && message.readAt === null,
            ).length;

            const messageCount = messages.length;

            const user = users.find((user) => user.id === userId);

            return {
                ...ticket,
                newMessageCount,
                messageCount,
                user: user ? { ...user, properties: undefined } : undefined,
            };
        });

        return {
            count: ticketCount,
            tickets,
        };
    }
}
