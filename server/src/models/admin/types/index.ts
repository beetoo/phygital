import { FindOptionsOrderValue } from 'typeorm';
import {
    CONTENT_TYPE,
    OrganizationStatusType,
    PaymentStatusType,
    SupportTicketStatusType,
} from '../../../../../common/types';

export type GetOrganizationsQueryType = {
    take: string;
    skip: string;
    search?: string;
    status?: OrganizationStatusType;
};

export type GetOrganizationsDataType = {
    take: number;
    skip: number;
    search?: string;
    status?: OrganizationStatusType;
};

export type GetUsersQueryType = {
    take: string;
    skip: string;
    search?: string;
};

export type GetUsersDataType = {
    take: number;
    skip: number;
    search?: string;
};

export type GetScreensQueryType = {
    take: string;
    skip: string;
    connected: 'true' | 'false' | null;
    status?: 'online' | 'offline' | 'error';
    isEmpty?: 'true' | 'false';
};

export type GetScreensDataType = {
    take: number;
    skip: number;
    connected: 'true' | 'false' | null;
    status?: 'online' | 'offline' | 'error';
    isEmpty?: 'true' | 'false';
};

export type GetSubscriptionsQueryType = {
    take: string;
    skip: string;
    sortBy: 'createdAt' | 'startDate' | 'expirationDate';
    order: FindOptionsOrderValue;
    paid?: 'true' | 'false';
};

export type GetSubscriptionsParams = {
    take: number;
    skip: number;
    sortBy: 'createdAt' | 'startDate' | 'expirationDate';
    order: FindOptionsOrderValue;
    paid?: boolean;
};

export type GetContentQueryType = {
    take: string;
    skip: string;
    sortBy: 'size';
    order: FindOptionsOrderValue;
    type: CONTENT_TYPE;
};

export type GetContentDataType = {
    take: number;
    skip: number;
    sortBy: 'size';
    order: FindOptionsOrderValue;
    type: CONTENT_TYPE;
};

export type GetActivityLogsQueryType = {
    take: string;
    skip: string;
    order: FindOptionsOrderValue;
};

export type GetActivityLogsDataType = {
    take: number;
    skip: number;
    order: FindOptionsOrderValue;
};

export type GetTicketsQueryType = {
    take: string;
    skip: string;
    status?: SupportTicketStatusType;
    closed?: 'true' | 'false';
};

export type GetTicketsDataType = {
    take: number;
    skip: number;
    status?: SupportTicketStatusType;
    closed?: boolean;
};

export type GetPaymentsQueryType = {
    take: string;
    skip: string;
    status?: PaymentStatusType;
};

export type GetPaymentsDataType = {
    take: number;
    skip: number;
    status?: PaymentStatusType;
};
