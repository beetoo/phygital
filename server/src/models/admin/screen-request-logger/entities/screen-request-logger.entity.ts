import { Column, Entity, Index, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

import { ScreenRequestLoggerType } from '../../../../../../common/types';

@Entity()
@Index(['identification'])
export class ScreenRequestLogger implements ScreenRequestLoggerType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    identification: string;

    @Column({ nullable: true })
    screenId: string;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column()
    requestType: 'HEAD' | 'GET';

    @Column({ type: 'text', nullable: true })
    requestJson?: string;

    @Column({ type: 'text', nullable: true })
    message?: string;

    @Column({ type: 'text', nullable: true })
    error?: string;
}
