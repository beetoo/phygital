import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { ScreenRequestLogger } from './entities/screen-request-logger.entity';

@Injectable()
export class ScreenRequestLoggerService {
    constructor(
        @InjectRepository(ScreenRequestLogger)
        private screenRequestLoggerRepository: Repository<ScreenRequestLogger>,
    ) {}

    async createOrUpdateLoggerError(
        identification: string,
        screenId: string,
        requestType: 'HEAD' | 'GET',
        requestJson: string,
        error: string,
    ) {
        return this.createOrUpdateLogger(identification, screenId, requestType, requestJson, '', error);
    }

    async createOrUpdateLoggerSuccess(
        identification: string,
        screenId: string,
        requestType: 'HEAD' | 'GET',
        requestJson: string,
        message: string,
    ) {
        return this.createOrUpdateLogger(identification, screenId, requestType, requestJson, message, '');
    }

    private async createOrUpdateLogger(
        identification: string,
        screenId: string,
        requestType: 'HEAD' | 'GET',
        requestJson: string,
        message: string,
        error: string,
    ) {
        const screenLogger = await this.screenRequestLoggerRepository.findOne({
            where: {
                identification: identification,
                requestType: requestType,
            },
        });

        if (screenLogger) {
            if (!screenLogger.screenId) {
                screenLogger.screenId = screenId;
            }
            screenLogger.requestJson = new Date() + '\n' + requestJson;
            screenLogger.error = error;
            screenLogger.message = message;
            await this.screenRequestLoggerRepository.save(screenLogger, { reload: true });
        } else {
            const newScreenLogger = new ScreenRequestLogger();
            newScreenLogger.identification = identification;
            newScreenLogger.screenId = screenId;
            newScreenLogger.requestType = requestType;
            newScreenLogger.requestJson = requestJson;
            newScreenLogger.message = message;
            newScreenLogger.error = error;
            await this.screenRequestLoggerRepository.save(newScreenLogger);
        }
    }
}
