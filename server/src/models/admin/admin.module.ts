import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { Organization } from '../organization/entities/organization.entity';
import { Screen } from '../screen/entities/screen.entity';
import { ScreenRequestLogger } from './screen-request-logger/entities/screen-request-logger.entity';
import { Content } from '../content/entities/content.entity';
import { User } from '../user/entities/user.entity';
import { UserActivityLog } from '../user/entities/user-activity-log.entity';
import { SupportTicket } from '../support-ticket/entities/support-ticket.entity';
import { Payment } from '../payment/entities/payment.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Organization,
            User,
            UserActivityLog,
            Screen,
            Content,
            ScreenRequestLogger,
            SupportTicket,
            Payment,
        ]),
    ],
    controllers: [AdminController],
    providers: [AdminService],
})
export class AdminModule {}
