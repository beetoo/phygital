import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne } from 'typeorm';
import { Expose } from 'class-transformer';

import { Organization } from '../../organization/entities/organization.entity';
import { OrganizationType, PaymentStatusType, PaymentType } from '../../../../../common/types';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class Payment implements PaymentType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    organizationId: string;

    @Column()
    inn: string;

    @Column()
    company: string;

    @Column()
    invoiceNumber: string;

    @Column({ type: String, default: 'pending' })
    status: PaymentStatusType;

    @Column({ default: 0, unsigned: true })
    screens: number;

    @Column({ default: 0, unsigned: true })
    pricePerScreen: number;

    @Column({ unsigned: true })
    days: number;

    @Column()
    startDate: string;

    @Column()
    notes: string;

    @Expose()
    get total() {
        return this.screens * this.pricePerScreen;
    }

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(() => Organization, (organization) => organization.payments, { onDelete: 'CASCADE' })
    organization: OrganizationType;
}
