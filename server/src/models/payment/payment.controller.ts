import { Controller, Get, Body, Patch, Param, Delete } from '@nestjs/common';
import { validate } from 'uuid';
import { isValid } from 'date-fns';

import { PaymentService } from './payment.service';
import { UpdatePaymentDto } from './dto/update-payment.dto';
import { Admin } from '../../decorators/admin.decorator';
import { Accounting } from '../../decorators/accounting.decorator';
import { ValidationService } from '../../services/validation/validation.service';
import { CheckPermission, ControllerAction } from '../../decorators/check-permission.decorator';
import { PaymentStatusType } from '../../../../common/types';

@Admin()
@Accounting()
@Controller('payment')
export class PaymentController {
    constructor(
        private readonly paymentService: PaymentService,
        private readonly validationService: ValidationService,
    ) {}

    @CheckPermission(ControllerAction.view_subscriptions)
    @Get(':paymentId')
    findOnePayment(@Param('paymentId') paymentId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(paymentId), { paymentId: 'INVALID' });

        return this.paymentService.findOnePayment(paymentId);
    }

    @CheckPermission(ControllerAction.update_subscription)
    @Patch(':paymentId')
    updatePayment(@Param('paymentId') paymentId: string, @Body() updatePaymentDto: UpdatePaymentDto) {
        const { inn, company, invoiceNumber, status, screens, pricePerScreen, days, startDate, notes } =
            updatePaymentDto;

        this.validationService.throwBadRequestExceptionIf(!validate(paymentId), { paymentId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(inn !== undefined && inn.length === 0, {
            inn: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(company !== undefined && company.length === 0, {
            company: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(invoiceNumber !== undefined && invoiceNumber.length === 0, {
            invoiceNumber: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(
            !!status && !(['pending', 'paid', 'cancelled'] as PaymentStatusType[]).includes(status),
            { status: 'INVALID' },
        );

        const isFloatNumber = (num: number) => num % 1 !== 0;

        this.validationService.throwBadRequestExceptionIf(
            screens !== undefined && (isNaN(screens) || screens < 0 || isFloatNumber(screens)),
            {
                screens: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(
            pricePerScreen !== undefined &&
                (isNaN(pricePerScreen) || pricePerScreen < 0 || isFloatNumber(pricePerScreen)),
            {
                pricePerScreen: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(
            days !== undefined && (isNaN(days) || days < 0 || isFloatNumber(days)),
            {
                days: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(!!startDate && !isValid(new Date(startDate)), {
            startDate: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(notes !== undefined && notes.length === 0, {
            notes: 'INVALID',
        });

        return this.paymentService.update(paymentId, updatePaymentDto);
    }

    @CheckPermission(ControllerAction.delete_subscription)
    @Delete(':paymentId')
    removePayment(@Param('paymentId') paymentId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(paymentId), { paymentId: 'INVALID' });

        return this.paymentService.remove(paymentId);
    }
}
