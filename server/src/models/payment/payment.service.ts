import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UpdatePaymentDto } from './dto/update-payment.dto';
import { ValidationService } from '../../services/validation/validation.service';
import { Payment } from './entities/payment.entity';

@Injectable()
export class PaymentService {
    constructor(
        @InjectRepository(Payment)
        private paymentRepository: Repository<Payment>,
        private readonly validationService: ValidationService,
    ) {}

    async findOnePayment(paymentId: string) {
        const payment = await this.paymentRepository.findOne({
            where: { id: paymentId },
            relations: { organization: true },
        });

        if (!payment) {
            throw this.validationService.notFoundException({ payment: 'NOT_FOUND' });
        }

        return { ...payment, total: payment.total, organization: payment.organization, organizationId: undefined };
    }

    async update(paymentId: string, updatePaymentDto: UpdatePaymentDto) {
        const payment = await this.paymentRepository.findOneBy({ id: paymentId });

        if (!payment) {
            throw this.validationService.notFoundException({ payment: 'NOT_FOUND' });
        }

        const { inn, company, invoiceNumber, status, screens, pricePerScreen, days, startDate, notes } =
            updatePaymentDto;

        if (inn) {
            payment.inn = inn;
        }

        if (company) {
            payment.company = company;
        }

        if (invoiceNumber) {
            payment.invoiceNumber = invoiceNumber;
        }

        if (status) {
            payment.status = status;
        }

        if (screens !== undefined) {
            payment.screens = screens;
        }

        if (pricePerScreen !== undefined) {
            payment.pricePerScreen = pricePerScreen;
        }

        if (days !== undefined) {
            payment.days = days;
        }

        if (startDate) {
            payment.startDate = startDate;
        }

        if (notes) {
            payment.notes = notes;
        }

        return this.paymentRepository.save(payment);
    }

    async remove(paymentId: string) {
        const payment = await this.paymentRepository.findOneBy({ id: paymentId });

        if (!payment) {
            throw this.validationService.notFoundException({ payment: 'NOT_FOUND' });
        }

        await this.paymentRepository.remove(payment);

        return { paymentId };
    }
}
