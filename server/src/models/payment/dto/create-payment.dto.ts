import { PaymentStatusType } from '../../../../../common/types';

export class CreatePaymentDto {
    inn: string;
    company: string;
    invoiceNumber: string;
    status: PaymentStatusType;
    screens: number;
    pricePerScreen: number;
    days: number;
    startDate: string; // YYYY-MM-DD
    notes: string;
}
