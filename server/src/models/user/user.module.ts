import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';

import { UserService } from './user.service';
import { UserController } from './user.controller';
import { User } from './entities/user.entity';
import { JwtConfigService } from '../../configs/jwt-config-service';
import { EmailService } from '../../services/email.service';
import { Organization } from '../organization/entities/organization.entity';
import { OrganizationInvitation } from '../organization-invitation/entities/organization.invitation';
import { SupportTicket } from '../support-ticket/entities/support-ticket.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([Organization, User, OrganizationInvitation, SupportTicket]),
        JwtModule.registerAsync({
            useClass: JwtConfigService,
        }),
    ],
    controllers: [UserController],
    providers: [UserService, EmailService],
})
export class UserModule {}
