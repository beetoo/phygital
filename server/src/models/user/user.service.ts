import { randomBytes } from 'node:crypto';
import { HttpException, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { Repository, Not, In } from 'typeorm';
import bcrypt from 'bcryptjs';
import { Request } from 'express';

import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { EmailService } from '../../services/email.service';
import { UserType } from '../../../../common/types';
import { extractTokenFromHeader } from '../../helpers';
import { OrganizationInvitation } from '../organization-invitation/entities/organization.invitation';
import { Organization } from '../organization/entities/organization.entity';
import { UserSession } from '../../services/user-sessions.service';
import { ValidationService } from '../../services/validation/validation.service';
import { SupportTicket } from '../support-ticket/entities/support-ticket.entity';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(Organization)
        private organizationRepository: Repository<Organization>,
        @InjectRepository(OrganizationInvitation)
        private organizationInvitationRepository: Repository<OrganizationInvitation>,
        @InjectRepository(SupportTicket)
        private ticketRepository: Repository<SupportTicket>,
        private emailService: EmailService,
        private jwtService: JwtService,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
        private readonly validationService: ValidationService,
    ) {}

    async findOne(userId: string) {
        const user = await this.userRepository.findOneBy({ id: userId });

        if (!user) {
            throw this.validationService.notFoundException({ user: 'NOT_FOUND' });
        }

        const tmp: Partial<UserType> = { ...user };
        delete tmp.password;
        delete tmp.emailVerificationToken;
        delete tmp.properties;

        return tmp;
    }

    async findUserOrganizations(userId: string) {
        const user = await this.userRepository.findOne({ where: { id: userId }, relations: { organizations: true } });

        if (!user) {
            throw this.validationService.notFoundException({ user: 'NOT_FOUND' });
        }

        return user.organizations.map((organization) => ({
            ...organization,
            role: user.properties.find(({ organizationId }) => organizationId === organization.id)?.role ?? 'member',
        }));
    }

    async findUserSessions(userId: string) {
        const user = await this.userRepository.findOneBy({ id: userId });

        if (!user) {
            throw this.validationService.notFoundException({ user: 'NOT_FOUND' });
        }

        const userSessions = (await this.cacheManager.get<UserSession[]>(`user:sessions:${user.id}`)) ?? [];

        return userSessions.map(({ token: _, ...session }) => session);
    }

    async findUserInvitations(userId: string) {
        const userCount = await this.userRepository.countBy({ id: userId });

        if (userCount === 0) {
            throw this.validationService.notFoundException({ user: 'NOT_FOUND' });
        }

        const invitations = await this.organizationInvitationRepository.find({
            where: { userId },
            select: { id: true, organizationId: true, inviterId: true, createdAt: true },
        });

        if (invitations.length === 0) {
            return [];
        }

        const invitationOrganizationIds = invitations.map(({ organizationId }) => organizationId);
        const invitationInviterIds = invitations.map(({ inviterId }) => inviterId);

        const [organizations, inviters] = await Promise.all([
            this.organizationRepository.find({
                where: { id: In(invitationOrganizationIds) },
                select: { id: true, name: true },
            }),
            this.userRepository.find({
                where: { id: In(invitationInviterIds) },
                select: { id: true, name: true, surname: true, email: true },
            }),
        ]);

        return invitations.map((invitation) => {
            const organizationName = organizations.find(({ id }) => id === invitation.organizationId)?.name;
            const inviter = inviters.find(({ id }) => id === invitation.inviterId);

            return {
                ...invitation,
                organizationId: undefined,
                inviterId: undefined,
                organizationName,
                inviter: {
                    name: inviter?.name,
                    surname: inviter?.surname,
                    email: inviter?.email,
                },
            };
        });
    }

    async update(userId: string, updateUserDto: UpdateUserDto, request: Request) {
        const { name, surname, email, phone, language, status, role } = updateUserDto;

        const user = await this.userRepository.findOneBy({ id: userId });

        if (!user) {
            throw this.validationService.notFoundException({ user: 'NOT_FOUND' });
        }

        if (email && user.email !== email) {
            const isEmailAlreadyExists = (await this.userRepository.findBy({ id: Not(userId) })).some(
                (user) => user.email.toLowerCase() === email.toLowerCase(),
            );

            this.validationService.throwBadRequestExceptionIf(isEmailAlreadyExists, { email: 'EMAIL_ALREADY_EXISTS' });

            user.status = 'pending';
            user.email = email;
            user.emailVerificationToken = randomBytes(32).toString('hex');
        }

        user.name = name || user.name;
        user.surname = surname || user.surname;
        user.phone = phone || user.phone;
        user.language = language || user.language;
        user.role = role || user.role;

        const token = extractTokenFromHeader(request);
        const tokenUser = await this.getUserFromToken(token);

        if (tokenUser?.role === 'admin') {
            user.status = status || user.status;
        }

        const savedUser = await this.userRepository.save(user);

        if (savedUser.emailVerificationToken) {
            try {
                await this.emailService.sendConfirmRegistrationEmail(user.email, user.emailVerificationToken);
            } catch (err) {
                throw new HttpException(err.message, err.status);
            }
        }

        const tmp: Partial<UserType> = { ...savedUser };
        delete tmp.password;
        delete tmp.emailVerificationToken;
        delete tmp.properties;

        return tmp;
    }

    async updatePassword(
        userId: string,
        requestRole: string,
        { password, currentPassword }: { password: string; currentPassword: string },
    ) {
        const user = await this.userRepository.findOneBy({ id: userId });

        if (!user) {
            throw this.validationService.notFoundException({ user: 'NOT_FOUND' });
        }

        if (requestRole !== 'admin') {
            const isPasswordMatch = await bcrypt.compare(currentPassword, user.password);

            this.validationService.throwBadRequestExceptionIf(!isPasswordMatch, { password: 'INVALID' });
        }

        user.password = await bcrypt.hash(password, 10);

        const savedUser = await this.userRepository.save(user);

        const tmp: Partial<UserType> = { ...savedUser };
        delete tmp.password;
        delete tmp.emailVerificationToken;
        delete tmp.properties;

        return tmp;
    }

    async sendConfirmRegistrationEmail(userId: string) {
        const user = await this.userRepository.findOneBy({ id: userId });

        if (!user) {
            throw this.validationService.notFoundException({ user: 'NOT_FOUND' });
        }

        this.validationService.throwBadRequestExceptionIf(user.status !== 'pending', {
            email: 'EMAIL_IS_ALREADY_VERIFIED',
        });

        try {
            if (user.emailVerificationToken) {
                await this.emailService.sendConfirmRegistrationEmail(user.email, user.emailVerificationToken);
            }

            return { message: 'OK' };
        } catch {
            return { message: 'FAIL' };
        }
    }

    async getUserFromToken(token: string | undefined) {
        if (!token) {
            return null;
        }

        try {
            const { userId } = await this.jwtService.verify(token);

            return await this.userRepository.findOneBy({ id: userId });
        } catch {
            return null;
        }
    }

    async findUserTickets(userId: string) {
        const user = await this.userRepository.findOneBy({ id: userId });

        if (!user) {
            throw this.validationService.notFoundException({ user: 'NOT_FOUND' });
        }

        const organizationIds = user.properties.map((property) => property.organizationId);

        const relatedUserIds = (
            await this.userRepository.findBy({ properties: { organizationId: In(organizationIds) } })
        ).map((user) => user.id);

        return (
            await this.ticketRepository.find({ where: [{ userId: In(relatedUserIds) }], relations: { messages: true } })
        ).map(({ messages, userId, ...ticket }) => {
            const newMessageCount = messages.filter(
                (message) => message.senderRole === 'support' && message.readAt === null,
            ).length;

            const messageCount = messages.length;

            return { ...ticket, newMessageCount, messageCount };
        });
    }
}
