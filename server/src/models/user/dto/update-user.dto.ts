import { PartialType } from '@nestjs/mapped-types';

import { CreateUserDto } from './create-user.dto';
import { OrganizationLanguageType } from '../../../../../common/types';

export class UpdateUserDto extends PartialType(CreateUserDto) {
    readonly name: string;
    readonly surname: string;
    readonly email: string;
    readonly phone: string;
    readonly language: OrganizationLanguageType;
    readonly status: 'pending' | 'active';
    readonly role: 'admin' | 'user';
}
