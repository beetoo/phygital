import { Body, Controller, Get, Param, Patch, Post, Req } from '@nestjs/common';
import { validate } from 'uuid';
import { isEmail } from 'validator';
import { Request } from 'express';

import { UserService } from './user.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { extractTokenFromHeader } from '../../helpers';
import { OrganizationLanguageType, UserRoleType } from '../../../../common/types';
import { Public } from '../../decorators/public.decorator';
import { ViewOnly } from '../../decorators/view-only.decorator';
import { ValidationService } from '../../services/validation/validation.service';

@Controller('user')
export class UserController {
    constructor(
        private readonly userService: UserService,
        private readonly validationService: ValidationService,
    ) {}

    @ViewOnly()
    @Get(':userId')
    findOne(@Param('userId') userId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });

        return this.userService.findOne(userId);
    }

    @ViewOnly()
    @Get(':userId/organizations')
    findUserOrganizations(@Param('userId') userId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });

        return this.userService.findUserOrganizations(userId);
    }

    @ViewOnly()
    @Get(':userId/sessions')
    getUserSessions(@Param('userId') userId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });

        return this.userService.findUserSessions(userId);
    }

    @Get(':userId/invitations')
    findUserInvitations(@Param('userId') userId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });

        return this.userService.findUserInvitations(userId);
    }

    @Public()
    @Post(':userId/email/verification/resend')
    sendConfirmRegistrationEmail(@Param('userId') userId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });

        return this.userService.sendConfirmRegistrationEmail(userId);
    }

    @Patch(':userId')
    async update(@Param('userId') userId: string, @Body() updateUserDto: UpdateUserDto, @Req() req: Request) {
        const { name, surname, email, phone, language, status, role } = updateUserDto;

        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!!name && name.trim() === '', { name: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!!surname && surname.trim() === '', { surname: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!!email && !isEmail(email.trim()), { email: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!!phone && phone.trim() === '', { phone: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(
            language && !(['ru', 'en', 'es'] as OrganizationLanguageType[]).includes(language),
            { language: 'INVALID' },
        );
        this.validationService.throwBadRequestExceptionIf(status && !['pending', 'active'].includes(status), {
            status: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(
            role && !(['admin', 'user', 'accounting', 'viewonly'] as UserRoleType[]).includes(role),
            { role: 'INVALID' },
        );

        return this.userService.update(userId, updateUserDto, req);
    }

    @Post(':userId/password')
    async updatePassword(
        @Param('userId') userId: string,
        @Body('currentPassword') currentPassword: string,
        @Body('password') password: string,
        @Req() req: Request,
    ) {
        const token = extractTokenFromHeader(req);
        const tokenUser = await this.userService.getUserFromToken(token);

        if (!tokenUser) {
            throw this.validationService.badRequestException({ user: 'NOT_FOUND' });
        }

        this.validationService.throwForbiddenExceptionIf(tokenUser.role !== 'admin' && !currentPassword, {
            currentPassword: 'NOT_FOUND',
        });

        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!password, { password: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(password.length < 8, { password: 'INVALID' });

        return this.userService.updatePassword(userId, tokenUser.role, { password, currentPassword });
    }

    @Get(':userId/tickets')
    getUserTickets(@Param('userId') userId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });

        return this.userService.findUserTickets(userId);
    }
}
