import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, ManyToMany, OneToMany } from 'typeorm';

import { Organization } from '../../organization/entities/organization.entity';
import { UserOrganizationProperty } from './user-organization-property.entity';
import { UserType } from '../../../../../common/types';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class User implements UserType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ default: '' })
    name: string;

    @Column({ default: '' })
    surname: string;

    @Column({ unique: true, nullable: true })
    email: string;

    @Column({ default: '' })
    phone: string;

    @Column({ default: '' })
    password: string;

    @Column({ default: 'user', nullable: true })
    role: 'admin' | 'accounting' | 'user' | 'viewonly';

    @Column({ default: 'pending', nullable: true })
    status: 'active' | 'pending';

    @Column({ default: '' })
    emailVerificationToken: string;

    @OneToMany(() => UserOrganizationProperty, (property) => property.user, { eager: true })
    properties: UserOrganizationProperty[];

    @CreateDateColumn()
    createdAt: Date;

    @Column({ default: 'ru' })
    language: 'en' | 'ru' | 'es';

    @Column({ nullable: true })
    lastActiveAt: Date;

    @ManyToMany(() => Organization, (organization) => organization.users)
    organizations: Organization[];
}
