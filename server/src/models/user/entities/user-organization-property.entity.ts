import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

import { User } from './user.entity';
import { UserOrganizationPropertyType, UserOrganizationRoleEnum } from '../../../../../common/types';

@Entity()
export class UserOrganizationProperty implements UserOrganizationPropertyType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ nullable: true })
    organizationId: string;

    @Column({ type: 'enum', enum: UserOrganizationRoleEnum, default: UserOrganizationRoleEnum.owner })
    role: UserOrganizationRoleEnum;

    @Column({ type: 'json', nullable: true })
    permissions: string[];

    @ManyToOne(() => User, (user) => user.properties, { cascade: ['remove'] })
    user: User;
}
