import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';

@Entity({ orderBy: { timestamp: 'DESC' } })
export class UserActivityLog {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    userId: string;

    @Column()
    event: string;

    @Column()
    userAgent: string;

    @Column()
    ip: string;

    @CreateDateColumn()
    timestamp: Date;
}
