import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';
import { validate } from 'uuid';

import { SupportTicketService } from './support-ticket.service';
import { CreateSupportTicketDto } from './dto/create-support-ticket.dto';
import { UpdateSupportTicketDto } from './dto/update-support-ticket.dto';
import { ValidationService } from '../../services/validation/validation.service';
import { CreateSupportTicketMessageDto } from './dto/create-support-ticket-message.dto';
import { CloseSupportTicketDto } from './dto/close-support-ticket.dto';
import { UserOnly } from '../../decorators/user-only.decorator';
import { AdminOnly } from '../../decorators/admin-only.decorator';
import { WithUserFromToken } from '../../decorators/with-user-from-token.decorator';
import { User } from '../user/entities/user.entity';

@Controller('ticket')
export class SupportTicketController {
    constructor(
        private readonly supportTicketService: SupportTicketService,
        private readonly validationService: ValidationService,
    ) {}

    @WithUserFromToken()
    @Get(':ticketId')
    getTicket(@Param('ticketId') ticketId: string, @Body('user') tokenUser: User) {
        this.validationService.throwBadRequestExceptionIf(!validate(ticketId), { ticketId: 'INVALID' });

        return this.supportTicketService.findTicket(ticketId, tokenUser);
    }

    @UserOnly()
    @Post()
    createTicket(@Body() createSupportTicketDto: CreateSupportTicketDto) {
        const { userId, organizationId, subject, description } = createSupportTicketDto;

        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });

        const isOrganizationIdInvalid =
            organizationId !== undefined && organizationId !== null && !validate(organizationId);
        this.validationService.throwBadRequestExceptionIf(isOrganizationIdInvalid, { organizationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!subject, { subject: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!description, { description: 'IS_REQUIRED' });

        return this.supportTicketService.createTicket(createSupportTicketDto);
    }

    @Post(':ticketId/message')
    createTicketMessage(
        @Param('ticketId') ticketId: string,
        @Body() createSupportTicketMessageDto: CreateSupportTicketMessageDto,
    ) {
        const { userId, senderRole, message, replyTo } = createSupportTicketMessageDto;

        this.validationService.throwBadRequestExceptionIf(!validate(ticketId), { ticketId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!['support', 'user'].includes(senderRole), {
            senderRole: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(!message, { message: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(replyTo !== undefined && !validate(replyTo), {
            replyTo: 'INVALID',
        });

        return this.supportTicketService.createMessage(ticketId, createSupportTicketMessageDto);
    }

    @AdminOnly()
    @Patch(':ticketId')
    updateTicket(@Param('ticketId') ticketId: string, @Body() updateSupportTicketDto: UpdateSupportTicketDto) {
        const { status } = updateSupportTicketDto;

        this.validationService.throwBadRequestExceptionIf(!validate(ticketId), { ticketId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(
            !!status && !['resolved', 'not_resolved', 'new', 'in_progress'].includes(status),
            {
                status: 'INVALID',
            },
        );

        return this.supportTicketService.updateTicket(ticketId, updateSupportTicketDto);
    }

    @Post(':ticketId/close')
    closeTicket(@Param('ticketId') ticketId: string, @Body() closeSupportTicketDto: CloseSupportTicketDto) {
        const { status } = closeSupportTicketDto;

        this.validationService.throwBadRequestExceptionIf(!validate(ticketId), { ticketId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!['resolved', 'not_resolved'].includes(status), {
            status: 'INVALID',
        });

        return this.supportTicketService.closeTicket(ticketId, closeSupportTicketDto);
    }

    @Post(':ticketId/open')
    openTicket(@Param('ticketId') ticketId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(ticketId), { ticketId: 'INVALID' });

        return this.supportTicketService.openTicket(ticketId);
    }
}
