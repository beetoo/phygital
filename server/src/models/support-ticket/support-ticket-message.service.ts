import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { ValidationService } from '../../services/validation/validation.service';
import { SupportTicketMessage } from './entities/support-ticket-message.entity';
import { User } from '../user/entities/user.entity';

@Injectable()
export class SupportTicketMessageService {
    constructor(
        @InjectRepository(SupportTicketMessage)
        private messageRepository: Repository<SupportTicketMessage>,
        private readonly validationService: ValidationService,
    ) {}

    async readMessage(messageId: string, tokenUser: User) {
        const message = await this.messageRepository.findOneBy({ id: messageId });

        if (!message) {
            throw this.validationService.notFoundException({ message: 'NOT_FOUND' });
        }

        if (
            (tokenUser.role === 'admin' && message.senderRole === 'support') ||
            (tokenUser.role === 'user' && message.senderRole === 'user')
        ) {
            throw this.validationService.forbiddenException({ message: 'READ_IS_FORBIDDEN' });
        }

        await this.messageRepository.save({ ...message, readAt: new Date() });

        return { messageId };
    }

    async deleteMessage(messageId: string) {
        const message = await this.messageRepository.findOneBy({ id: messageId });

        if (!message) {
            throw this.validationService.notFoundException({ message: 'NOT_FOUND' });
        }

        if (message.senderRole === 'user') {
            throw this.validationService.forbiddenException({ message: 'DELETION_IS_FORBIDDEN' });
        }

        await this.messageRepository.remove(message);

        return { messageId };
    }
}
