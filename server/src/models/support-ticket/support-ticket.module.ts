import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { SupportTicketService } from './support-ticket.service';
import { SupportTicketController } from './support-ticket.controller';
import { SupportTicket } from './entities/support-ticket.entity';
import { SupportTicketMessage } from './entities/support-ticket-message.entity';
import { TelegramBotService } from '../../services/telegram-bot.service';
import { User } from '../user/entities/user.entity';

@Module({
    imports: [TypeOrmModule.forFeature([SupportTicket, SupportTicketMessage, User])],
    controllers: [SupportTicketController],
    providers: [SupportTicketService, TelegramBotService],
})
export class SupportTicketModule {}
