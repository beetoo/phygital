import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';

import { CreateSupportTicketDto } from './dto/create-support-ticket.dto';
import { UpdateSupportTicketDto } from './dto/update-support-ticket.dto';
import { ValidationService } from '../../services/validation/validation.service';
import { SupportTicket } from './entities/support-ticket.entity';
import { CreateSupportTicketMessageDto } from './dto/create-support-ticket-message.dto';
import { SupportTicketMessage } from './entities/support-ticket-message.entity';
import { CloseSupportTicketDto } from './dto/close-support-ticket.dto';
import { TelegramBotService } from '../../services/telegram-bot.service';
import { CLIENT_API_URL } from '../../constants';
import { User } from '../user/entities/user.entity';

@Injectable()
export class SupportTicketService {
    constructor(
        @InjectRepository(SupportTicket)
        private ticketRepository: Repository<SupportTicket>,
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(SupportTicketMessage)
        private ticketMessageRepository: Repository<SupportTicketMessage>,
        private readonly validationService: ValidationService,
        private readonly telegramBotService: TelegramBotService,
    ) {}

    async findTicket(ticketId: string, tokenUser: User) {
        const ticket = await this.ticketRepository.findOne({ where: { id: ticketId }, relations: { messages: true } });

        if (!ticket) {
            throw this.validationService.notFoundException({ ticket: 'NOT_FOUND' });
        }

        const userIds = ticket?.messages.map(({ userId }) => userId) ?? [];
        const users = await this.userRepository.findBy({ id: In([ticket.userId, ...userIds]) });

        const isAdmin = tokenUser.role === 'admin';

        const user = users.find((user) => user.id === ticket.userId);

        return {
            ...ticket,
            user: this.formattedUser(user, isAdmin),
            messages: ticket.messages.map(({ userId, ...message }) => {
                const user = users.find((user) => user.id === userId);

                return { ...message, user: this.formattedUser(user, isAdmin) };
            }),
            userId: undefined,
        };
    }

    async createTicket({ userId, organizationId, subject, description }: CreateSupportTicketDto) {
        const { id } = await this.ticketRepository.save({ userId, organizationId, subject, description });

        const text = `Новый запрос в поддержку:\n${CLIENT_API_URL}/admin/tickets/${id}`;
        await this.telegramBotService.sendMessage(text);

        return { ticketId: id };
    }

    async createMessage(ticketId: string, { userId, senderRole, message, replyTo }: CreateSupportTicketMessageDto) {
        const ticket = await this.ticketRepository.findOneBy({ id: ticketId });

        if (!ticket) {
            throw this.validationService.notFoundException({ ticket: 'NOT_FOUND' });
        }

        const ticketMessage = new SupportTicketMessage();
        ticketMessage.userId = userId;
        ticketMessage.senderRole = senderRole;
        ticketMessage.ticketId = ticketId;
        ticketMessage.message = message;
        ticketMessage.replyTo = replyTo ?? null;

        const { id: messageId } = await this.ticketMessageRepository.save(ticketMessage);

        return { messageId };
    }

    async updateTicket(ticketId: string, { status }: UpdateSupportTicketDto) {
        let ticket = await this.ticketRepository.findOneBy({ id: ticketId });

        if (!ticket) {
            throw this.validationService.notFoundException({ ticket: 'NOT_FOUND' });
        }

        if (ticket.closedAt !== null) {
            throw this.validationService.badRequestException({ ticket: 'MUST_BE_NOT_CLOSED' });
        }

        if (status) {
            ticket.status = status;
        }

        await this.ticketRepository.save(ticket);

        return { ticketId };
    }

    async closeTicket(ticketId: string, { status }: CloseSupportTicketDto) {
        const ticket = await this.ticketRepository.findOneBy({ id: ticketId });

        if (!ticket) {
            throw this.validationService.notFoundException({ ticket: 'NOT_FOUND' });
        }

        if (ticket.closedAt) {
            throw this.validationService.badRequestException({ ticket: 'ALREADY_CLOSED' });
        }

        ticket.status = status;
        ticket.closedAt = new Date();

        await this.ticketRepository.save(ticket);

        return { ticketId };
    }

    async openTicket(ticketId: string) {
        const ticket = await this.ticketRepository.findOneBy({ id: ticketId });

        if (!ticket) {
            throw this.validationService.notFoundException({ ticket: 'NOT_FOUND' });
        }

        if (ticket.closedAt === null) {
            throw this.validationService.badRequestException({ ticket: 'ALREADY_OPENED' });
        }

        ticket.status = 'in_progress';
        ticket.closedAt = null;

        await this.ticketRepository.save(ticket);

        return { ticketId };
    }

    formattedUser(user: User | undefined, isAdmin: boolean) {
        if (!user) {
            return null;
        }

        if (!isAdmin) {
            return { id: user.id, name: user.name, surname: user.surname?.charAt(0) };
        } else {
            return {
                id: user.id,
                name: user.name,
                surname: user.surname,
                email: user.email,
                phone: user.phone,
            };
        }
    }
}
