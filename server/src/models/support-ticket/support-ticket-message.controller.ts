import { Controller, Post, Param, Delete, Body } from '@nestjs/common';
import { validate } from 'uuid';

import { ValidationService } from '../../services/validation/validation.service';
import { SupportTicketMessageService } from './support-ticket-message.service';
import { WithUserFromToken } from '../../decorators/with-user-from-token.decorator';
import { User } from '../user/entities/user.entity';
import { AdminOnly } from '../../decorators/admin-only.decorator';

@Controller('message')
export class SupportTicketMessageController {
    constructor(
        private readonly supportTicketMessageService: SupportTicketMessageService,
        private readonly validationService: ValidationService,
    ) {}

    @WithUserFromToken()
    @Post(':messageId/read')
    readMessage(@Param('messageId') messageId: string, @Body('user') tokenUser: User) {
        this.validationService.throwBadRequestExceptionIf(!validate(messageId), { messageId: 'INVALID' });

        return this.supportTicketMessageService.readMessage(messageId, tokenUser);
    }

    @AdminOnly()
    @Delete(':messageId')
    deleteTicket(@Param('messageId') messageId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(messageId), { messageId: 'INVALID' });

        return this.supportTicketMessageService.deleteMessage(messageId);
    }
}
