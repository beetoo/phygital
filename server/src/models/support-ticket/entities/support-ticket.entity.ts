import { CreateDateColumn, UpdateDateColumn, Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

import { SupportTicketMessage } from './support-ticket-message.entity';
import { SupportTicketStatusType, SupportTicketType } from '../../../../../common/types';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class SupportTicket implements SupportTicketType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    userId: string;

    @Column({ type: String, nullable: true })
    organizationId: string | null;

    @Column()
    subject: string;

    @Column({ type: 'text' })
    description: string;

    @Column({ type: String, default: 'new' })
    status: SupportTicketStatusType;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column({ type: Date, nullable: true })
    closedAt: Date | null;

    @OneToMany(() => SupportTicketMessage, (message) => message.ticket, { cascade: true })
    messages: SupportTicketMessage[];
}
