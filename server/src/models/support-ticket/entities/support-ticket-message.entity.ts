import { CreateDateColumn, UpdateDateColumn, Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

import { SupportTicket } from './support-ticket.entity';
import { MessageSenderRoleType, SupportTicketMessageType } from '../../../../../common/types';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class SupportTicketMessage implements SupportTicketMessageType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    ticketId: string;

    @Column()
    userId: string;

    @Column({ type: String })
    senderRole: MessageSenderRoleType;

    @Column({ type: 'text' })
    message: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column({ type: Date, nullable: true })
    readAt: Date | null;

    @Column({ type: String, nullable: true })
    replyTo: string | null;

    @ManyToOne(() => SupportTicket, (ticket) => ticket.messages, { onDelete: 'CASCADE' })
    ticket: SupportTicket;
}
