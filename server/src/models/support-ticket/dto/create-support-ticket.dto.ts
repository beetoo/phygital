export class CreateSupportTicketDto {
    organizationId?: string | null;
    userId: string;
    subject: string;
    description: string;
}
