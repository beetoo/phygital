import { SupportTicketStatusType } from '../../../../../common/types';

export class CloseSupportTicketDto {
    status: SupportTicketStatusType;
}
