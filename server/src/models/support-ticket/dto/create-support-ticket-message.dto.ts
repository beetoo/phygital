import { MessageSenderRoleType } from '../../../../../common/types';

export class CreateSupportTicketMessageDto {
    userId: string;
    senderRole: MessageSenderRoleType;

    message: string;
    replyTo?: string;
}
