import { PartialType } from '@nestjs/mapped-types';

import { CreateSupportTicketDto } from './create-support-ticket.dto';
import { SupportTicketStatusType } from '../../../../../common/types';

export class UpdateSupportTicketDto extends PartialType(CreateSupportTicketDto) {
    status?: SupportTicketStatusType;
}
