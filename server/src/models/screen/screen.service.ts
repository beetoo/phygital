import fs from 'node:fs';
import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { FindManyOptions, In, IsNull, Not, Repository } from 'typeorm';
import * as uniqid from 'uniqid';

import { Location } from '../location/entities/location.entity';
import { Playlist } from '../playlist/entities/playlist.entity';
import { CreateScreenDto } from './dto/create-screen.dto';
import { UpdateScreenDto } from './dto/update-screen.dto';
import { Screen } from './entities/screen.entity';
import { ScreenRequestLogger } from '../admin/screen-request-logger/entities/screen-request-logger.entity';
import { mapOneScreen, mapPlaylists, mapScreensWithLogs } from '../../helpers';
import { ScreenReg } from '../admin/screen-reg/entities/screen-reg.entity';
import { ScreenHistory } from './entities/screen-history.entity';
import { UpdateScreensDto } from './dto/update-screens.dto';
import { deleteFile } from '../../helpers/deleteFile';
import { HelperService } from '../../services/helper.service';
import { isLocalDevelopmentServer, SCREENSHOT_STATIC_URL, UPLOAD_PATH_SCREENSHOT } from '../../constants';
import { ValidationService } from '../../services/validation/validation.service';
import { defaultScreenMeta } from './helpers';

@Injectable()
export class ScreenService {
    constructor(
        @InjectRepository(Screen)
        private screenRepository: Repository<Screen>,
        @InjectRepository(ScreenHistory)
        private screenHistoryRepository: Repository<ScreenHistory>,
        @InjectRepository(Playlist)
        private playlistRepository: Repository<Playlist>,
        @InjectRepository(Location)
        private locationRepository: Repository<Location>,
        @InjectRepository(ScreenRequestLogger)
        private screenLoggerRepository: Repository<ScreenRequestLogger>,
        @InjectRepository(ScreenReg)
        private screenRegRepository: Repository<ScreenReg>,
        private eventEmitter: EventEmitter2,
        private helperService: HelperService,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
        private readonly validationService: ValidationService,
    ) {}

    async addScreen(screenDto: CreateScreenDto) {
        // ищем локацию с такой же организацией и именем и присваиваем её девайсу
        const screenLocation = await this.locationRepository.findOneBy({
            id: screenDto.locationId,
            organizationId: screenDto.organizationId,
        });

        if (!screenLocation) {
            throw this.validationService.badRequestException({ location: 'LOCATION_NOT_FOUND' });
        }

        const screenReg = await this.screenRegRepository.findOneBy({ verificationHash: screenDto.verificationHash });

        console.log('addScreen ' + JSON.stringify(screenReg));

        if (isLocalDevelopmentServer || screenDto.verificationHash.toLowerCase().includes('test')) {
            return await this.addTestPhygitalScreenSOS(screenDto, screenLocation);
        } else if (screenReg) {
            return await this.addPhygitalScreenSOS(screenDto, screenReg, screenLocation);
        } else {
            throw this.validationService.badRequestException({ hash: 'INVALID_HASH' });
        }
    }

    private async addPhygitalScreenSOS(screenDto: CreateScreenDto, screenReg: ScreenReg, screenLocation: Location) {
        // создаём объект экрана
        console.log('addPhygitalScreenSOS 1');
        let screen = new Screen();

        screen = {
            ...screen,
            ...screenDto,
            name: screenDto.name.slice(0, 125),
            identification: screenReg.identification,
            meta: defaultScreenMeta(),
            startTime: { hours: 0, minutes: 0 },
            endTime: { hours: 0, minutes: 0 },
            resolution: { width: 1920, height: 1080 },
            location: screenLocation,
            isEmpty: true,
        };

        console.log('addPhygitalScreenSOS 2');
        // сохраняем
        const { id } = await this.screenRepository.save(screen);
        console.log('addPhygitalScreenSOS 3');

        await this.screenRegRepository.softRemove(screenReg);
        console.log('addPhygitalScreenSOS 3');

        this.eventEmitter.emit('screen.json.create', { screenId: id });

        return { id };
    }

    private async addTestPhygitalScreenSOS(screenDto: CreateScreenDto, screenLocation: Location) {
        // создаём объект экрана
        let screen = new Screen();

        screen = {
            ...screen,
            ...screenDto,
            name: screenDto.name.slice(0, 125),
            identification: uniqid.process(),
            meta: defaultScreenMeta(),
            startTime: { hours: 0, minutes: 0 },
            endTime: { hours: 0, minutes: 0 },
            resolution: { width: 1920, height: 1080 },
            location: screenLocation,
            isEmpty: true,
        };

        // сохраняем
        const { id, identification } = await this.screenRepository.save(screen);

        // создаем screenReg
        const screenReg = new ScreenReg();
        screenReg.verificationHash = screenDto.verificationHash;
        screenReg.identification = identification;
        await this.screenRegRepository.save(screenReg);

        // и сразу удаляем его
        await this.screenRegRepository.softRemove(screenReg);

        this.eventEmitter.emit('screen.json.create', { screenId: id });

        return { id };
    }

    async detachAllPlaylists(screenId: string) {
        const screen = await this.screenRepository.findOneBy({ id: screenId });

        if (!screen) {
            throw this.validationService.notFoundException({ screen: 'NOT_FOUND' });
        }

        screen.playlists = [];
        await this.screenRepository.save(screen);

        this.eventEmitter.emit('screen.json.update', { screenId });

        return { screenId };
    }

    async findOneScreen(screenId: string) {
        const options: FindManyOptions<Omit<Screen, 'log' | 'hasHistory'>> = {
            where: { id: screenId },
            relations: {
                playlists: { playlistItems: { content: true }, screens: true },
                location: true,
            },
        };

        let screen = await this.screenRepository.findOne(options);

        if (!screen) {
            throw this.validationService.notFoundException({ screen: 'NOT_FOUND' });
        }

        const [requestLogs, screenReg, screensHistory] = await Promise.all([
            this.screenLoggerRepository.findBy({ identification: screen.identification }),
            this.screenRegRepository.findOne({
                where: { identification: screen.identification, deletedAt: Not(IsNull()) },
                withDeleted: true,
            }),
            this.screenHistoryRepository.findBy({ screenId: screen.id }),
        ]);

        screen = mapScreensWithLogs([screen], requestLogs).at(0) as Screen;

        return {
            ...mapOneScreen(screen, requestLogs),
            playlists: mapPlaylists(screen.playlists),
            verificationHash: screenReg?.verificationHash ?? '',
            hasHistory: screensHistory.length > 0,
        };
    }

    async updateScreen(
        screenId: string,
        {
            name,
            resolution,
            orientation,
            startTime,
            endTime,
            locationId,
            prefetchTime,
            smil,
            urlParams,
            meta,
        }: UpdateScreenDto,
    ) {
        const screen = await this.screenRepository.findOne({
            where: { id: screenId },
            relations: { playlists: { playlistItems: { content: true, scenario: true } } },
        });

        if (!screen) {
            throw this.validationService.notFoundException({ screen: 'NOT_FOUND' });
        }

        if (name) {
            screen.name = name.slice(0, 125);
        }

        if (resolution) {
            if (screen.resolution.width !== resolution.width || screen.resolution.height !== resolution.height) {
                await this.updatePlaylistUpdatedAtTime(screenId);
            }

            screen.resolution = resolution;
        }

        if (orientation) {
            if (screen.orientation !== orientation) {
                await this.updatePlaylistUpdatedAtTime(screenId);
            }

            screen.orientation = orientation;
        }

        if (startTime) {
            screen.startTime = startTime;
        }

        if (endTime) {
            screen.endTime = endTime;
        }

        if (locationId) {
            // ищем локацию с такой же организацией и именем и присваиваем её девайсу
            const screenLocation = await this.locationRepository.findOneBy({
                id: locationId,
                organizationId: screen.organizationId,
            });

            if (!screenLocation) {
                throw this.validationService.badRequestException({ location: 'LOCATION_NOT_FOUND' });
            }

            screen.location = screenLocation;
        }

        if (prefetchTime !== undefined) {
            screen.prefetchTime = prefetchTime;
        }

        if (smil !== undefined) {
            screen.smil = smil;
        }

        if (urlParams !== undefined) {
            screen.urlParams = urlParams;
        }

        if (meta !== undefined) {
            screen.meta = meta;
        }

        await this.screenRepository.save(screen);

        this.eventEmitter.emit('screen.json.update', { screenId });

        return { id: screenId };
    }

    async updateScreens({ screenIds, locationId }: UpdateScreensDto) {
        const screens = await this.screenRepository.find({
            where: { id: In(screenIds) },
            relations: { location: true },
        });

        // если экраны принадлежат не одной организации
        const isSameOrganization = screens.every(
            (screen, _, array) => screen && screen.organizationId === array.at(0)?.organizationId,
        );

        this.validationService.throwBadRequestExceptionIf(!isSameOrganization, {
            screens: 'BELONGS_TO_DIFFERENT_ORGANIZATIONS',
        });

        for (const screen of screens) {
            if (locationId) {
                const location = await this.locationRepository.findOneBy({
                    id: locationId,
                    organizationId: screen.organizationId,
                });

                if (!location) {
                    throw this.validationService.badRequestException({ location: 'LOCATION_NOT_FOUND' });
                }

                screen.location = location;
            }
        }

        await this.screenRepository.save(screens);

        return { screenIds };
    }

    private async updatePlaylistUpdatedAtTime(screenId: string) {
        const playlists = await this.playlistRepository.findBy({ screens: { id: screenId } });

        const updatePromises = playlists.map((playlist) =>
            this.playlistRepository.update(playlist.id, { updatedAt: new Date() }),
        );

        await Promise.all(updatePromises);

        this.eventEmitter.emit('screen.json.update', { screenId });
    }

    async removeScreens(screenIds: string[]) {
        await this.verifyScreensExists(screenIds);

        const screens = await this.screenRepository.findBy({ id: In(screenIds) });

        const screenIdentifications = screens.map(({ identification }) => identification);

        const deletePromises = screenIdentifications.map(async (identification) => {
            await this.cacheManager.del(`player:data:${identification}`);
            await this.cacheManager.del(`player:blackScreenshotCount:${identification}`);
            await this.cacheManager.del(`player:screenAliveAt:${identification}`);
            await this.cacheManager.del(`player:screenshotSize:${identification}`);
            await this.cacheManager.del(`player:smil:${identification}`);
            await this.cacheManager.del(`player:status:${identification}`);
        });

        await Promise.all([
            ...deletePromises,
            // удаление логов экрана
            this.screenLoggerRepository.delete({ identification: In(screenIdentifications) }),
            // удаление записей регистрации экранов
            this.screenRegRepository.delete({ identification: In(screenIdentifications) }),
            // удаление записей истории
            this.screenHistoryRepository.delete({ screenId: In(screenIds) }),
            // удаление экранов
            await this.screenRepository.delete({ id: In(screenIds) }),
        ]);

        const deleteScreenshotPromises = screenIdentifications.map((identification) =>
            deleteFile(`${UPLOAD_PATH_SCREENSHOT}/${identification}`),
        );

        // удаление скриншотов
        await Promise.all(deleteScreenshotPromises);

        this.eventEmitter.emit('screen.json.delete', { identifications: screenIdentifications });

        return { screenIds };
    }

    async verifyScreensExists(screenIds: string[]) {
        if (screenIds.length > 0) {
            const count = await this.screenRepository.countBy({ id: In(screenIds) });
            if (count < screenIds.length) {
                throw this.validationService.notFoundException({ screens: 'NOT_FOUND' });
            }
        }
    }

    async changeVerificationHash(screenId: string, verificationHash: string) {
        const [screen, screenReg] = await Promise.all([
            this.screenRepository.findOneBy({ id: screenId }),
            this.screenRegRepository.findOneBy({ verificationHash }),
        ]);

        if (!screen) {
            throw this.validationService.notFoundException({ screen: 'NOT_FOUND' });
        }

        if (!screenReg) {
            throw this.validationService.badRequestException({ screenReg: 'NOT_FOUND' });
        }

        // меняем идентификатор у экрана
        screen.identification = screenReg.identification;
        await this.screenRepository.save(screen);

        // и сразу удаляем его
        await this.screenRegRepository.softRemove(screenReg);

        return { screenId, verificationHash };
    }

    async getScreenHistory(screenId: string) {
        const screen = await this.screenRepository.findOne({
            where: { id: screenId },
            select: { id: true, identification: true, createdAt: true, aliveAt: true },
        });

        if (!screen) {
            throw this.validationService.notFoundException({ screen: 'NOT_FOUND' });
        }

        await this.helperService.updateScreenStatus(screen.identification, screen, true);

        return await this.screenHistoryRepository.find({
            where: { screenId },
            select: { id: true, status: true, createdAt: true },
        });
    }

    getScreenshot(identification: string) {
        const screenshotFolderPath = `${UPLOAD_PATH_SCREENSHOT}/${identification}`;

        if (!fs.existsSync(screenshotFolderPath)) {
            return { src: null };
        }

        let modifiedAt;

        try {
            const statInfo = fs.statSync(`${screenshotFolderPath}/screenshot.jpeg`);

            modifiedAt = Math.round(statInfo.ctimeMs);
        } catch {
            //
        }

        return {
            src: `${SCREENSHOT_STATIC_URL}/${identification}/screenshot.jpeg?timestamp=${Date.now()}${modifiedAt ? `&modifiedAt=${modifiedAt}` : ''}`,
        };
    }

    async clearCache(screenId: string) {
        const screen = await this.screenRepository.findOneBy({ id: screenId });

        if (!screen) {
            throw this.validationService.notFoundException({ screen: 'NOT_FOUND' });
        }

        await this.cacheManager.del(`player:data:${screen.identification}`);
        await this.cacheManager.del(`player:smil:${screen.identification}`);
        await this.cacheManager.del(`player:blackScreenshotCount:${screen.identification}`);

        return { screenId };
    }
}
