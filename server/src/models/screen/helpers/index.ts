import { ScreenMetaType } from '../../../../../common/types';

export const defaultScreenMeta = (): ScreenMetaType => ({
    screenshotSec: 30,
    restartSec: 15,
});
