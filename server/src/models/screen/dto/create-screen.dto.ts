export class CreateScreenDto {
    readonly organizationId: string;
    readonly locationId: string;
    readonly verificationHash: string;
    readonly name: string;

    readonly playlistId?: string;
}
