import { PartialType } from '@nestjs/mapped-types';

import { CreateScreenDto } from './create-screen.dto';
import { OrientationEnum, ScreenMetaType } from '../../../../../common/types';

export class UpdateScreenDto extends PartialType(CreateScreenDto) {
    readonly name?: string;
    readonly startTime?: { hours: number; minutes: number };
    readonly endTime?: { hours: number; minutes: number };
    readonly orientation?: OrientationEnum;
    readonly resolution?: { width: number; height: number };
    readonly locationId?: string;
    readonly prefetchTime?: number | null;
    readonly smil?: string | null;
    readonly urlParams?: { [key: string]: string | number } | null;
    readonly meta?: ScreenMetaType;
}
