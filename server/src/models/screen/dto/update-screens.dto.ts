export class UpdateScreensDto {
    readonly screenIds: string[];
    readonly locationId?: string;
}
