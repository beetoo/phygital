import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';

import { ScreenHistoryType } from '../../../../../common/types';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class ScreenHistory implements ScreenHistoryType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    screenId: string;

    @Column()
    status: 'online' | 'offline' | 'error' | 'pending';

    @CreateDateColumn()
    createdAt: Date;
}
