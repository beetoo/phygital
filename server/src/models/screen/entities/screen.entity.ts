import {
    Entity,
    Column,
    ManyToOne,
    PrimaryGeneratedColumn,
    DeleteDateColumn,
    CreateDateColumn,
    ManyToMany,
} from 'typeorm';

import { Location } from '../../location/entities/location.entity';
import { Playlist } from '../../playlist/entities/playlist.entity';
import { OrientationEnum, ScreenMetaType, ScreenType } from '../../../../../common/types';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class Screen implements ScreenType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ nullable: true })
    organizationId: string;

    @CreateDateColumn()
    createdAt: Date;

    @Column({ nullable: true })
    aliveAt: Date;

    @Column({ default: '' })
    identification: string;

    @Column()
    name: string;

    @Column({ default: '' })
    model: string;

    @Column({ type: 'json', nullable: true })
    meta: ScreenMetaType;

    @Column({ type: 'json', nullable: true })
    startTime: { hours: number; minutes: number };

    @Column({ type: 'json', nullable: true })
    endTime: { hours: number; minutes: number };

    @Column({ type: 'json', nullable: true })
    resolution: { width: number; height: number };

    @Column({ type: 'enum', enum: OrientationEnum, default: OrientationEnum['LANDSCAPE'] })
    orientation: OrientationEnum;

    @Column({ type: 'bigint', default: 0, unsigned: true })
    availableBytes: number;

    @Column({ type: 'bigint', default: 0, unsigned: true })
    totalBytes: number;

    @Column({ type: 'int', unsigned: true, nullable: true })
    prefetchTime: number | null;

    @DeleteDateColumn()
    deletedAt: Date;

    @Column({ type: 'json', nullable: true })
    urlParams: { [key: string]: string | number } | null;

    @Column({ type: 'text', nullable: true })
    smil: string | null;

    @ManyToOne(() => Location, (location) => location.screens, {
        onDelete: 'SET NULL',
    })
    location: Location;

    @Column({ default: true })
    logger: boolean;

    @Column({ default: false })
    isEmpty: boolean;

    @ManyToMany(() => Playlist, (playlist) => playlist.screens, { onDelete: 'CASCADE' })
    playlists: Playlist[];
}
