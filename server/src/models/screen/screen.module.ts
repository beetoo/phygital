import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Location } from '../location/entities/location.entity';
import { Playlist } from '../playlist/entities/playlist.entity';
import { Screen } from './entities/screen.entity';
import { ScreenReg } from '../admin/screen-reg/entities/screen-reg.entity';
import { ScreenController } from './screen.controller';
import { ScreenService } from './screen.service';
import { ScreenRequestLogger } from '../admin/screen-request-logger/entities/screen-request-logger.entity';
import { ScreenHistory } from './entities/screen-history.entity';
import { HelperService } from '../../services/helper.service';

@Module({
    imports: [TypeOrmModule.forFeature([Screen, ScreenReg, ScreenHistory, Playlist, Location, ScreenRequestLogger])],
    controllers: [ScreenController],
    providers: [ScreenService, HelperService],
})
export class ScreenModule {}
