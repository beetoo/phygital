import { Body, Controller, Get, Param, Patch, Post, Headers } from '@nestjs/common';
import { validate } from 'uuid';

import { CreateScreenDto } from './dto/create-screen.dto';
import { UpdateScreenDto } from './dto/update-screen.dto';
import { ScreenService } from './screen.service';
import { OrientationEnum } from '../../../../common/types';
import { UpdateScreensDto } from './dto/update-screens.dto';
import { CheckPermission, ControllerAction } from '../../decorators/check-permission.decorator';
import { ValidationService } from '../../services/validation/validation.service';

@Controller('screen')
export class ScreenController {
    constructor(
        private readonly screenService: ScreenService,
        private readonly validationService: ValidationService,
    ) {}

    @CheckPermission(ControllerAction.connect_screen)
    @Post()
    create(@Body() createScreenDto: CreateScreenDto) {
        const { organizationId, locationId, name, verificationHash } = createScreenDto;

        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!validate(locationId), { locationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!name, { name: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!verificationHash, { verificationHash: 'IS_REQUIRED' });

        return this.screenService.addScreen(createScreenDto);
    }

    @CheckPermission(ControllerAction.view_screens)
    @Get(':screenId')
    findOneScreen(@Param('screenId') screenId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(screenId), { screenId: 'INVALID' });

        return this.screenService.findOneScreen(screenId);
    }

    @CheckPermission(ControllerAction.update_playlist_screens)
    @Post(':screenId/playlists/detach/all')
    detachAllPlaylists(@Param('screenId') screenId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(screenId), { screenId: 'INVALID' });

        return this.screenService.detachAllPlaylists(screenId);
    }

    @CheckPermission(ControllerAction.update_screen)
    @Patch(':screenId')
    updateScreen(@Param('screenId') screenId: string, @Body() updateScreenDto: UpdateScreenDto) {
        const { name, resolution, orientation, startTime, endTime, locationId, prefetchTime, smil, urlParams, meta } =
            updateScreenDto;

        this.validationService.throwBadRequestExceptionIf(!validate(screenId), { screenId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!!name && name.length === 0, { name: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!!resolution && (!resolution.width || !resolution.height), {
            resolution: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(
            !!orientation && !Object.values(OrientationEnum).includes(orientation),
            {
                orientation: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(
            !!startTime && (isNaN(startTime.hours) || isNaN(startTime.minutes)),
            {
                startTime: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(
            !!endTime && (isNaN(endTime.hours) || isNaN(endTime.minutes)),
            {
                endTime: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(!!locationId && !validate(locationId), {
            locationId: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(
            prefetchTime !== undefined && prefetchTime !== null && (isNaN(prefetchTime) || prefetchTime < 0),
            { prefetchTime: 'INVALID' },
        );
        this.validationService.throwBadRequestExceptionIf(
            smil !== undefined && typeof smil !== 'string' && smil !== null,
            {
                smil: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(
            urlParams !== undefined &&
                (typeof urlParams !== 'object' || Array.isArray(urlParams) || urlParams === null),
            { urlParams: 'INVALID' },
        );
        this.validationService.throwBadRequestExceptionIf(
            meta !== undefined && (meta === null || typeof meta !== 'object'),
            {
                meta: 'INVALID',
            },
        );

        const isScreenshotMetaInvalid =
            meta !== undefined &&
            (!Object.keys(meta).includes('screenshotSec') ||
                typeof meta?.screenshotSec !== 'number' ||
                meta.screenshotSec < 5 ||
                meta.screenshotSec > 2147483640);

        this.validationService.throwBadRequestExceptionIf(isScreenshotMetaInvalid, {
            meta: { screenshotSec: 'INVALID' },
        });

        const isRestartMetaInvalid =
            meta !== undefined &&
            (!Object.keys(meta).includes('restartSec') ||
                typeof meta?.restartSec !== 'number' ||
                meta.restartSec < 5 ||
                meta.restartSec > 2147483640);

        this.validationService.throwBadRequestExceptionIf(isRestartMetaInvalid, { meta: { restartSec: 'INVALID' } });

        return this.screenService.updateScreen(screenId, updateScreenDto);
    }

    @CheckPermission(ControllerAction.update_screen)
    @Patch('update/screens')
    updateMultiplyScreens(@Body() updateScreenDto: UpdateScreensDto) {
        const { screenIds, locationId } = updateScreenDto;

        this.validationService.throwBadRequestExceptionIf(
            !Array.isArray(screenIds) || screenIds.some((screenId) => !validate(screenId)),
            {
                screenIds: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(!!locationId && !validate(locationId), {
            locationId: 'INVALID',
        });

        return this.screenService.updateScreens(updateScreenDto);
    }

    @CheckPermission(ControllerAction.update_screen)
    @Patch(':screenId/hash')
    async changeVerificationHash(
        @Param('screenId') screenId: string,
        @Body('verificationHash') verificationHash: string,
    ) {
        this.validationService.throwBadRequestExceptionIf(!validate(screenId), { screenId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!verificationHash, { verificationHash: 'IS_REQUIRED' });

        return this.screenService.changeVerificationHash(screenId, verificationHash);
    }

    @CheckPermission(ControllerAction.delete_screen)
    @Patch('delete/screens')
    remove(@Body('screenIds') screenIds: string[]) {
        this.validationService.throwBadRequestExceptionIf(
            !Array.isArray(screenIds) || screenIds.some((screenId) => !validate(screenId)),
            {
                screenIds: 'INVALID',
            },
        );

        return this.screenService.removeScreens(screenIds);
    }

    @CheckPermission(ControllerAction.view_screens)
    @Get(':screenId/history')
    getScreenHistory(@Param('screenId') screenId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(screenId), { screenId: 'INVALID' });

        return this.screenService.getScreenHistory(screenId);
    }

    @CheckPermission(ControllerAction.view_screens)
    @Get(':screenId/screenshot')
    getScreenshot(@Param('screenId') screenId: string, @Headers('identification') identification: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(screenId), { screenId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!identification, { identification: 'IS_REQUIRED' });

        return this.screenService.getScreenshot(identification);
    }

    @CheckPermission(ControllerAction.update_screen, ControllerAction.clear_screen_cache)
    @Post(':screenId/clearCache')
    clearCache(@Param('screenId') screenId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(screenId), { screenId: 'INVALID' });

        return this.screenService.clearCache(screenId);
    }
}
