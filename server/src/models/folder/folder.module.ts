import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FolderService } from './folder.service';
import { FolderController } from './folder.controller';
import { Folder } from './entities/folder.entity';
import { ContentService } from '../content/content.service';
import { Content } from '../content/entities/content.entity';
import { PlaylistItem } from '../playlist/entities/playlist-item.entity';
import { Playlist } from '../playlist/entities/playlist.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Folder, Content, Playlist, PlaylistItem])],
    controllers: [FolderController],
    providers: [FolderService, ContentService],
})
export class FolderModule {}
