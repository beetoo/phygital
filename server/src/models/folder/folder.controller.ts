import { Body, Controller, Delete, Param, Patch, Query } from '@nestjs/common';
import { validate } from 'uuid';

import { FolderService } from './folder.service';
import { UpdateFolderDto } from './dto/update-folder.dto';
import { CheckPermission, ControllerAction } from '../../decorators/check-permission.decorator';
import { ValidationService } from '../../services/validation/validation.service';

@Controller('folder')
export class FolderController {
    constructor(
        private readonly folderService: FolderService,
        private readonly validationService: ValidationService,
    ) {}

    @CheckPermission(ControllerAction.update_content)
    @Patch(':folderId')
    async update(@Param('folderId') folderId: string, @Body() updateFolderDto: UpdateFolderDto) {
        const { name } = updateFolderDto;

        this.validationService.throwBadRequestExceptionIf(!validate(folderId), { folderId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!name, { name: 'IS_REQUIRED' });

        return this.folderService.update(folderId, updateFolderDto);
    }

    @CheckPermission(ControllerAction.delete_content)
    @Delete()
    async remove(@Query('folderId') folderId: string | string[]) {
        const isValidId = Array.isArray(folderId) ? folderId.every((val) => validate(val)) : validate(folderId);

        this.validationService.throwBadRequestExceptionIf(!isValidId, { folderId: 'INVALID' });

        return this.folderService.remove(folderId);
    }
}
