import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

import { ContentType, FolderType } from '../../../../../common/types';

import { Content } from '../../content/entities/content.entity';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class Folder implements FolderType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    organizationId: string;

    @Column({ default: '' })
    name: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany(() => Content, (content) => content.folder)
    contents: ContentType[];
}
