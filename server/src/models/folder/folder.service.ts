import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';

import { UpdateFolderDto } from './dto/update-folder.dto';
import { Folder } from './entities/folder.entity';
import { ContentService } from '../content/content.service';
import { ValidationService } from '../../services/validation/validation.service';

@Injectable()
export class FolderService {
    constructor(
        @InjectRepository(Folder)
        private folderRepository: Repository<Folder>,
        private contentService: ContentService,
        private readonly validationService: ValidationService,
    ) {}

    async update(folderId: string, { name }: UpdateFolderDto) {
        const folder = await this.folderRepository.findOneBy({ id: folderId });

        if (!folder) {
            throw this.validationService.notFoundException({ folder: 'NOT_FOUND' });
        }

        await this.folderRepository.update(folderId, { name: name.slice(0, 125) });

        return { id: folderId };
    }

    async remove(folderId: string | string[]) {
        const folderIds = Array.isArray(folderId) ? folderId : [folderId];

        const folders = await this.folderRepository.find({
            where: { id: In(folderIds) },
            relations: { contents: true },
        });

        this.validationService.throwBadRequestExceptionIf(folders.length === 0, {
            folders: 'NOT_FOUND',
        });

        const contentIds = folders.flatMap(({ contents }) => contents).map(({ id }) => id);

        await this.contentService.removeContentByIds(contentIds);

        await this.folderRepository.delete(folderId);

        return { message: 'OK' };
    }
}
