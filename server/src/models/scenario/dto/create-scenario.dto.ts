import { ScenarioRule } from '../entities/scenario-rule.entity';

export class CreateScenarioDto {
    readonly name: string;
    readonly description: string;
    readonly rules: ScenarioRule[];
}
