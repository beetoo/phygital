import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { uniq } from 'lodash';

import { Scenario } from './entities/scenario.entity';
import { CreateScenarioDto } from './dto/create-scenario.dto';
import { Playlist } from '../playlist/entities/playlist.entity';
import { PlaylistItem } from '../playlist/entities/playlist-item.entity';
import { ValidationService } from '../../services/validation/validation.service';

@Injectable()
export class ScenarioService {
    constructor(
        @InjectRepository(Playlist)
        private playlistRepository: Repository<Playlist>,
        @InjectRepository(PlaylistItem)
        private playlistItemRepository: Repository<PlaylistItem>,
        @InjectRepository(Scenario)
        private scenarioRepository: Repository<Scenario>,
        private eventEmitter: EventEmitter2,
        private readonly validationService: ValidationService,
    ) {}

    async updateScenario(scenarioId: string, { name, description, rules: rulesDto }: CreateScenarioDto) {
        let scenario = await this.scenarioRepository.findOneBy({ id: scenarioId });

        if (!scenario) {
            throw this.validationService.notFoundException({ scenario: 'NOT_FOUND' });
        }

        const playlists = await this.playlistRepository.findBy({ playlistItems: { scenarioId } });

        for (const playlist of playlists) {
            // обновление updatedAt у плейлиста
            await this.playlistRepository.update(playlist.id, { updatedAt: new Date() });
        }

        scenario.name = name || scenario.name;
        scenario.description = description || scenario.description;
        scenario.rules = rulesDto || scenario.rules;

        scenario = await this.scenarioRepository.save(scenario);

        // кол-во экранов в плейлисте не меняется
        this.eventEmitter.emit('screen.json.update', { playlistIds: playlists.map(({ id }) => id) });

        return scenario;
    }

    async getScenario(scenarioId: string) {
        const scenario = await this.scenarioRepository.findOne({
            where: { id: scenarioId },
            relations: { playlistItems: { playlist: true } },
        });

        if (!scenario) {
            throw this.validationService.notFoundException({ scenario: 'NOT_FOUND' });
        }

        return scenario;
    }

    async removeScenario(scenarioId: string) {
        const scenario = await this.scenarioRepository.findOneBy({ id: scenarioId });

        if (!scenario) {
            throw this.validationService.notFoundException({ scenario: 'NOT_FOUND' });
        }

        const playlistItemsWithScenario = await this.playlistItemRepository.findBy({ scenarioId });

        const updatePromises = playlistItemsWithScenario.map((item) =>
            this.playlistItemRepository.update(item.id, { scenarioId: null }),
        );

        await Promise.all(updatePromises);

        await this.scenarioRepository.delete({ id: scenarioId });

        const playlistIds = uniq(playlistItemsWithScenario.map(({ playlistId }) => playlistId));

        await this.playlistRepository.update({ id: In(playlistIds) }, { updatedAt: new Date() });

        // кол-во экранов в плейлисте не меняется
        this.eventEmitter.emit('screen.json.update', { playlistIds });

        return { scenarioId };
    }
}
