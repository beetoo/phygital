import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

import { ScenarioType } from '../../../../../common/types';
import { PlaylistItem } from '../../playlist/entities/playlist-item.entity';
import { ScenarioRule } from './scenario-rule.entity';

@Entity()
export class Scenario implements ScenarioType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ nullable: true })
    organizationId: string;

    @Column({ default: '' })
    name: string;

    @Column({ default: '' })
    description: string;

    @OneToMany(() => ScenarioRule, (scenarioRule) => scenarioRule.scenario, { eager: true, cascade: true })
    rules: ScenarioRule[];

    @OneToMany(() => PlaylistItem, (playlistItem) => playlistItem.scenario)
    playlistItems: PlaylistItem[];
}
