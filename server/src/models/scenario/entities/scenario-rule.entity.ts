import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';

import { Scenario } from './scenario.entity';
import { ScenarioRuleType, WeatherAndTrafficData, TimeData } from '../../../../../common/types';

@Entity({ orderBy: { condition: 'DESC' } })
export class ScenarioRule implements ScenarioRuleType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ default: '' })
    condition: 'weather' | 'traffic' | 'time';

    @Column({ type: 'json', nullable: true })
    data: WeatherAndTrafficData | TimeData;

    @ManyToOne(() => Scenario, (scenario) => scenario.rules, { onDelete: 'CASCADE' })
    scenario: Scenario;
}
