import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { validate } from 'uuid';

import { ScenarioService } from './scenario.service';
import { CreateScenarioDto } from './dto/create-scenario.dto';
import { CheckPermission, ControllerAction } from '../../decorators/check-permission.decorator';
import { ValidationService } from '../../services/validation/validation.service';

@Controller('scenario')
export class ScenarioController {
    constructor(
        private readonly scenarioService: ScenarioService,
        private readonly validationService: ValidationService,
    ) {}

    @CheckPermission(ControllerAction.update_scenario)
    @Post(':scenarioId')
    updateScenario(@Param('scenarioId') scenarioId: string, @Body() updateScenarioDto: CreateScenarioDto) {
        const { name, rules } = updateScenarioDto;

        this.validationService.throwBadRequestExceptionIf(!validate(scenarioId), { scenarioId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!name, { name: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!Array.isArray(rules), { rules: 'INVALID' });

        return this.scenarioService.updateScenario(scenarioId, updateScenarioDto);
    }

    @CheckPermission(ControllerAction.view_scenarios)
    @Get(':scenarioId')
    getScenario(@Param('scenarioId') scenarioId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(scenarioId), { scenarioId: 'INVALID' });

        return this.scenarioService.getScenario(scenarioId);
    }

    @CheckPermission(ControllerAction.delete_scenario)
    @Delete(':scenarioId')
    removeScenario(@Param('scenarioId') scenarioId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(scenarioId), { scenarioId: 'INVALID' });

        return this.scenarioService.removeScenario(scenarioId);
    }
}
