import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Playlist } from '../playlist/entities/playlist.entity';
import { PlaylistItem } from '../playlist/entities/playlist-item.entity';
import { Scenario } from './entities/scenario.entity';
import { ScenarioController } from './scenario.controller';
import { ScenarioService } from './scenario.service';

@Module({
    imports: [TypeOrmModule.forFeature([Scenario, Playlist, PlaylistItem])],
    controllers: [ScenarioController],
    providers: [ScenarioService],
})
export class ScenarioModule {}
