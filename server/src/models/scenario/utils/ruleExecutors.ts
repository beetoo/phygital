import { WeatherService } from '../../../services/weather.service';
import { playlistShouldBeRun, isTimeData, isWeatherAndTrafficData } from '../../../helpers';
import { Screen } from '../../screen/entities/screen.entity';
import { ScenarioRuleType, TimeData, ScenarioCondition, PlaylistSavedScheduleType } from '../../../../../common/types';

export type RuleExecutor = (ctx: RuleExecutionContext) => Promise<boolean>;

export type RuleExecutionContext = {
    geo: { lat: number; lon: number };
    screen: Screen;
    data: Required<ScenarioRuleType>['data'];
    services: {
        weatherService: WeatherService;
    };
    withEquallyOperator: boolean;
};

const ruleExecutors: Record<ScenarioCondition, RuleExecutor> = {
    weather: async (ctx: RuleExecutionContext) => {
        if (!isWeatherAndTrafficData(ctx.data)) {
            throw new Error('Context data must be WeatherAndTrafficData typed');
        }
        const { lat, lon } = ctx.geo;
        if (Number.isNaN(lat) || Number.isNaN(lon)) {
            throw new Error('Geo info not provided');
        }
        const { temp, condition } = await ctx.services.weatherService.getWeatherToPoint(lat, lon, ctx.screen);
        const { value, parameter, operator } = ctx.data;

        // температура
        if (parameter === 'temperature') {
            const tempValue = parseInt(value as string);

            if (operator === 'equally') {
                return ctx.withEquallyOperator ? true : temp === tempValue;
            }
            if (operator === 'more') {
                return temp >= tempValue;
            } else if (operator === 'less') {
                return temp <= tempValue;
            }
        }
        // облачность
        if (parameter === 'cloud_cover') {
            if (value === 'clear') {
                return condition === 'clear';
            }
            if (value === 'cloudy') {
                return condition === 'cloudy' || condition === 'partly-cloudy';
            }
            if (value === 'overcast') {
                return condition === 'overcast';
            }
        }
        // осадки
        if (parameter === 'rain_fall') {
            if (value === 'drizzle') {
                return condition === 'drizzle' || condition === 'light-rain';
            }
            if (value === 'rain') {
                return (
                    condition === 'rain' ||
                    condition === 'moderate-rain' ||
                    condition === 'heavy-rain' ||
                    condition === 'continuous-heavy-rain' ||
                    condition === 'showers' ||
                    condition === 'hail'
                );
            }
            if (value === 'wet-snow') {
                return condition === 'wet-snow';
            }
            if (value === 'snow') {
                return condition === 'light-snow' || condition === 'snow';
            }
            if (value === 'snow-showers') {
                return condition === 'snow-showers';
            }
            if (value === 'thunderstorm') {
                return (
                    condition === 'hail' ||
                    condition === 'thunderstorm' ||
                    condition === 'thunderstorm-with-rain' ||
                    condition === 'thunderstorm-with-hail'
                );
            }
        }

        throw new Error('Unimplemented');
    },
    traffic: (ctx: RuleExecutionContext) => {
        console.log('traffic ctx', ctx);
        throw new Error('Unimplemented');
    },
    time: async (ctx: RuleExecutionContext) => {
        const { startDay, endDay, startTime, endTime, byDay } = ctx.data as TimeData;
        const { screen } = ctx;
        if (isTimeData(ctx.data))
            return playlistShouldBeRun(screen, {
                startDay,
                endDay,
                startTime,
                endTime,
                byDay,
                broadcastAlways: false,
            } as PlaylistSavedScheduleType);

        return false;
        // throw new Error('Unimplemented');
    },
};

export const getRuleExecutor = (type: ScenarioCondition): RuleExecutor | null => ruleExecutors[type] ?? null;
