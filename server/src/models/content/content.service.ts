import path from 'node:path';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { In, Repository } from 'typeorm';
import { uniq } from 'lodash';
import { isBefore, isMatch } from 'date-fns';

import { Content } from './entities/content.entity';
import { PlaylistItem } from '../playlist/entities/playlist-item.entity';
import { UPLOAD_PATH_CONTENT } from '../../constants';
import { UpdateContentDto } from './dto/update-content.dto';
import { Folder } from '../folder/entities/folder.entity';
import { Playlist } from '../playlist/entities/playlist.entity';
import { formatWebContentLink } from './helpers';
import { deleteFile } from '../../helpers/deleteFile';
import { ContentType } from '../../../../common/types';
import { ValidationService } from '../../services/validation/validation.service';

@Injectable()
export class ContentService {
    constructor(
        @InjectRepository(Folder)
        private folderRepository: Repository<Folder>,
        @InjectRepository(Content)
        private contentRepository: Repository<Content>,
        @InjectRepository(PlaylistItem)
        private playlistItemRepository: Repository<PlaylistItem>,
        @InjectRepository(Playlist)
        private playlistRepository: Repository<Playlist>,
        private eventEmitter: EventEmitter2,
        private readonly validationService: ValidationService,
    ) {}

    async getOneContentItem(contentId: string) {
        const [contentItem, playlists] = await Promise.all([
            this.contentRepository.findOne({
                where: { id: contentId },
                relations: { folder: true },
            }),
            this.playlistRepository.findBy({
                playlistItems: { content: { id: contentId } },
            }),
        ]);

        if (!contentItem) {
            throw this.validationService.notFoundException({ contentItem: 'NOT_FOUND' });
        }

        const tmp: Partial<ContentType> = { ...contentItem };
        delete tmp.filename;
        delete tmp.webContentLink;

        return { ...tmp, src: contentItem.src, playlists: Object.keys(tmp).length > 0 ? playlists : undefined };
    }

    async updateContentItem(contentId: string, { name, folderId, lifetime }: UpdateContentDto) {
        const contentItem = await this.contentRepository.findOne({
            where: { id: contentId },
            relations: { playlistItems: true },
        });

        if (!contentItem) {
            throw this.validationService.notFoundException({ contentItem: 'NOT_FOUND' });
        }

        const oldFilename = contentItem.originFilename;

        if (name && name !== oldFilename) {
            const fileExtension = path.parse(oldFilename).ext;
            contentItem.originFilename = name.slice(0, 125) + fileExtension;
        }
        if (folderId) {
            const folder = await this.folderRepository.findOneBy({ id: folderId });

            if (!folder) {
                throw this.validationService.badRequestException({ folder: 'NOT_FOUND' });
            }

            contentItem.folder = folder;
        }

        if (lifetime !== undefined) {
            contentItem.lifetime = lifetime;
        }

        await this.contentRepository.save(contentItem);

        if (lifetime !== undefined) {
            const playlistIds = contentItem?.playlistItems
                .map((item) => item.playlistId)
                .reduce((ids, id) => (ids.includes(id) ? ids : [...ids, id]), [] as string[]);

            this.eventEmitter.emit('screen.json.update', { playlistIds });
        }

        return { id: contentId };
    }

    async updateWebContentItem(contentId: string, { name, link, urlParamMode, lifetime }: UpdateContentDto) {
        const contentItem = await this.contentRepository.findOne({
            where: { id: contentId },
            relations: { playlistItems: true },
        });

        if (!contentItem) {
            throw this.validationService.notFoundException({ contentItem: 'NOT_FOUND' });
        }

        const oldFilename = contentItem.originFilename;

        if (name && name !== oldFilename) {
            contentItem.originFilename = name.slice(0, 125);
            contentItem.filename = name.slice(0, 125);
        }

        if (link) {
            link = formatWebContentLink(link).slice(0, 1000);

            if (link && link !== contentItem.webContentLink) {
                contentItem.webContentLink = link;
            }
        }

        if (urlParamMode) {
            contentItem.urlParamMode = urlParamMode;
        }

        if (lifetime !== undefined) {
            contentItem.lifetime = lifetime;
        }

        await this.contentRepository.save(contentItem);

        if (lifetime !== undefined || urlParamMode) {
            const playlistIds = contentItem?.playlistItems
                .map((item) => item.playlistId)
                .reduce((ids, id) => (ids.includes(id) ? ids : [...ids, id]), [] as string[]);

            this.eventEmitter.emit('screen.json.update', { playlistIds });
        }

        return { id: contentId };
    }

    async updateMultiplyContentItems(contentIds: string[], { folderId }: UpdateContentDto) {
        const contentItems = await this.contentRepository.findBy({ id: In(contentIds) });

        // если экраны принадлежат не одной организации
        const isSameOrganization = contentItems.every(
            (item, _, array) => item && item.organizationId === array.at(0)?.organizationId,
        );

        this.validationService.throwBadRequestExceptionIf(!isSameOrganization, {
            content: 'BELONGS_TO_DIFFERENT_ORGANIZATIONS',
        });

        if (contentItems.length > 0 && folderId) {
            const folder = await this.folderRepository.findOneBy({ id: folderId });

            if (!folder) {
                throw this.validationService.badRequestException({ folder: 'NOT_FOUND' });
            }

            for (const contentItem of contentItems) {
                contentItem.folder = folder;
                await this.contentRepository.save(contentItem);
            }
        }

        return { contentIds };
    }

    async removeContentByIds(contentIds: string[]) {
        const playlistIds: string[] = [];

        const deleteContentItem = async (contentId: string) => {
            const playlistItems = await this.playlistItemRepository.findBy({ content: { id: contentId } });

            for (const { id, playlistId } of playlistItems) {
                // удаляем запись о всех playlistItems с этим contentId
                await this.playlistItemRepository.delete(id);

                const playlistItemsNumber = await this.playlistItemRepository.countBy({ playlistId });

                const playlist = await this.playlistRepository.findOneBy({ id: playlistId });

                if (playlist) {
                    playlist.draft = playlistItemsNumber === 0;
                    playlist.updatedAt = new Date();
                    await this.playlistRepository.save(playlist);

                    playlistIds.push(playlist.id);
                }
            }

            // удаляем запись об этом файле
            await this.contentRepository.delete(contentId);

            // удаляем файл из хранилища
            await deleteFile(`${UPLOAD_PATH_CONTENT}/${contentId}/`);
        };

        for (const contentId of contentIds) {
            await deleteContentItem(contentId);
        }

        // кол-во экранов в плейлисте не меняется
        this.eventEmitter.emit('screen.json.update', { playlistIds: uniq(playlistIds) });

        return { contentIds };
    }

    isLifeTimeValid(lifetime: { startDate: string; endDate: string } | null) {
        if (lifetime === null) {
            return true;
        }

        const { startDate, endDate } = lifetime;

        const startDateSplit = startDate.split('-');

        if (
            startDateSplit.length !== 3 ||
            startDateSplit[0].length !== 4 ||
            startDateSplit[1].length !== 2 ||
            startDateSplit[2].length !== 2 ||
            !isMatch(startDate, 'yyyy-MM-dd')
        ) {
            return false;
        }

        const endDateSplit = endDate.split('-');

        if (
            endDateSplit.length !== 3 ||
            endDateSplit[0].length !== 4 ||
            endDateSplit[1].length !== 2 ||
            endDateSplit[2].length !== 2 ||
            !isMatch(endDate, 'yyyy-MM-dd')
        ) {
            return false;
        }

        return !isBefore(new Date(endDate), new Date(startDate));
    }
}
