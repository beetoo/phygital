import fs from 'node:fs';
import sharp from 'sharp';

import { slugifyBeforeUpload } from '../../../../../common/helpers';
import { UPLOAD_PATH_CONTENT } from '../../../constants';

export const getImageResolution = async (filePath: string) => {
    const buffer = fs.readFileSync(filePath);

    const image = sharp(buffer);
    const { width = 0, height = 0, channels: bytesPerPixel = 0 } = await image.metadata();

    const memoryUsage = width * height * bytesPerPixel;

    return {
        width,
        height,
        duration: 0,
        memoryUsage,
    };
};

export const decodeOriginFilename = (originFilename: string) => Buffer.from(originFilename, 'latin1').toString('utf8');

export const saveFileToStorage = (id: string, file: Express.Multer.File, filename = '') => {
    fs.mkdirSync(`${UPLOAD_PATH_CONTENT}/${id}`, { recursive: true });

    filename = filename || slugifyBeforeUpload(decodeOriginFilename(file.originalname));

    const filePath = `${UPLOAD_PATH_CONTENT}/${id}/${filename}`;

    fs.copyFileSync(file.path, filePath);
    fs.rmSync(file.path);

    return filePath;
};

export const formatWebContentLink = (link: string) => {
    if (link.startsWith('http://')) {
        return link;
    } else if (link.startsWith('https://')) {
        return link;
    }

    return `https://${link}`;
};
