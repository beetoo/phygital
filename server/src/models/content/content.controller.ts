import { Body, Controller, Get, Param, Patch, Post } from '@nestjs/common';
import { validate } from 'uuid';

import { ContentService } from './content.service';
import { UpdateContentDto } from './dto/update-content.dto';
import { CheckPermission, ControllerAction } from '../../decorators/check-permission.decorator';
import { ValidationService } from '../../services/validation/validation.service';
import { UrlParamModeType } from '../../../../common/types';

@Controller('content')
export class ContentController {
    constructor(
        private readonly contentService: ContentService,
        private readonly validationService: ValidationService,
    ) {}

    @CheckPermission(ControllerAction.view_content)
    @Get(':contentId')
    getContentItem(@Param('contentId') contentId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(contentId), { contentId: 'INVALID' });

        return this.contentService.getOneContentItem(contentId);
    }

    @CheckPermission(ControllerAction.update_content)
    @Patch('update')
    async updateMultiplyContentItems(
        @Body('contentIds') contentIds: string[],
        @Body() updateContentDto: UpdateContentDto,
    ) {
        const { folderId } = updateContentDto;

        this.validationService.throwBadRequestExceptionIf(!contentIds.every((val) => validate(val)), {
            contentIds: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(!!folderId && !validate(folderId), {
            folderId: 'INVALID',
        });

        return this.contentService.updateMultiplyContentItems(contentIds, updateContentDto);
    }

    @CheckPermission(ControllerAction.update_content)
    @Patch(':contentId')
    async updateContentItem(@Param('contentId') contentId: string, @Body() updateContentDto: UpdateContentDto) {
        const { name, urlParamMode, lifetime } = updateContentDto;

        this.validationService.throwBadRequestExceptionIf(!validate(contentId), { contentId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(name !== undefined && !name, { name: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(
            urlParamMode !== undefined &&
                !(['all', 'screen', 'location', 'none'] as UrlParamModeType[]).includes(urlParamMode),
            { urlParamMode: 'INVALID' },
        );
        this.validationService.throwBadRequestExceptionIf(
            lifetime !== undefined && !this.contentService.isLifeTimeValid(lifetime),
            { lifetime: 'INVALID' },
        );

        return this.contentService.updateContentItem(contentId, updateContentDto);
    }

    @CheckPermission(ControllerAction.update_webpage)
    @Patch('web/:contentId')
    async updateWebContentItem(@Param('contentId') contentId: string, @Body() updateContentDto: UpdateContentDto) {
        const { name, link, urlParamMode, lifetime } = updateContentDto;

        this.validationService.throwBadRequestExceptionIf(!validate(contentId), { contentId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(name !== undefined && !name, { name: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!!link && link.length === 0, { link: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(
            urlParamMode !== undefined &&
                !(['all', 'screen', 'location', 'none'] as UrlParamModeType[]).includes(urlParamMode),
            { urlParamMode: 'INVALID' },
        );
        this.validationService.throwBadRequestExceptionIf(
            lifetime !== undefined && !this.contentService.isLifeTimeValid(lifetime),
            { lifetime: 'INVALID' },
        );

        return this.contentService.updateWebContentItem(contentId, updateContentDto);
    }

    @CheckPermission(ControllerAction.delete_content)
    @Post('delete')
    async removeContentItems(@Body('contentIds') contentIds: string[]) {
        this.validationService.throwBadRequestExceptionIf(!contentIds.every((id) => validate(id)), {
            contentIds: 'INVALID',
        });

        return this.contentService.removeContentByIds(contentIds);
    }
}
