import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ContentService } from './content.service';
import { ContentController } from './content.controller';
import { Content } from './entities/content.entity';
import { PlaylistItem } from '../playlist/entities/playlist-item.entity';
import { Playlist } from '../playlist/entities/playlist.entity';
import { Folder } from '../folder/entities/folder.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Folder, Content, Playlist, PlaylistItem])],
    controllers: [ContentController],
    providers: [ContentService],
})
export class ContentModule {}
