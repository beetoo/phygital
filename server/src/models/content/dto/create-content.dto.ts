import { DeepPartial } from 'typeorm';

import { CONTENT_TYPE } from '../../../../../common/types';
import { PlaylistItem } from '../../playlist/entities/playlist-item.entity';
import { Folder } from '../../folder/entities/folder.entity';

export class CreateContentDto {
    id: string;
    organizationId: string;
    originFilename: string;
    dimensions: { width: number; height: number };
    type: CONTENT_TYPE;
    size: number;
    duration: number;
    playlistItems?: DeepPartial<PlaylistItem>[] | undefined;
    folder?: Folder | DeepPartial<Folder> | undefined;
}
