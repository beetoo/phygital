import { UrlParamModeType } from '../../../../../common/types';

export class UpdateContentDto {
    name?: string;
    folderId?: string;
    link?: string;
    urlParamMode?: UrlParamModeType;
    lifetime?: { startDate: string; endDate: string } | null;
}
