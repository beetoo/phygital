import { Expose } from 'class-transformer';
import { Entity, Column, PrimaryColumn, OneToMany, ManyToOne, CreateDateColumn, UpdateDateColumn } from 'typeorm';

import { PlaylistItem } from '../../playlist/entities/playlist-item.entity';
import { Folder } from '../../folder/entities/folder.entity';
import { getContentSrc } from '../../../utils/getContentSrc';
import { CONTENT_TYPE, PlaylistItemType, FolderType, ContentType, UrlParamModeType } from '../../../../../common/types';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class Content implements ContentType {
    @PrimaryColumn()
    id: string;

    @Column()
    organizationId: string;

    @Column({ default: '' })
    originFilename: string;

    @Column()
    filename: string;

    @Column({ type: 'enum', enum: CONTENT_TYPE })
    type: CONTENT_TYPE;

    @Column({ type: 'json', nullable: true })
    dimensions: { width: number; height: number };

    @Column({
        type: 'bigint',
        default: 0,
        unsigned: true,
        transformer: { from: (value: string) => Number(value), to: (value) => value },
    })
    size: number;

    @Column({ default: 0, unsigned: true })
    duration: number;

    @Column({ nullable: true, unsigned: true })
    bitrate: number;

    @Column({ type: 'json', nullable: true })
    lifetime: { startDate: string; endDate: string } | null;

    @Column({ type: 'text', nullable: true })
    webContentLink: string;

    @Expose()
    get src(): string {
        if (this.type === CONTENT_TYPE.WEB) {
            return this.webContentLink ?? '';
        }
        return getContentSrc(this.id, this.filename);
    }

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column({ type: String, default: 'none' })
    urlParamMode: UrlParamModeType;

    @OneToMany(() => PlaylistItem, (playlistItem) => playlistItem.content, { cascade: true })
    playlistItems: PlaylistItemType[];

    @ManyToOne(() => Folder, (folder) => folder.contents, { nullable: true })
    folder: FolderType;
}
