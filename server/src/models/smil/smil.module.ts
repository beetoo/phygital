import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HttpModule } from '@nestjs/axios';

import { ScreenRequestLogger } from '../admin/screen-request-logger/entities/screen-request-logger.entity';
import { ScreenRequestLoggerService } from '../admin/screen-request-logger/screen-request-logger.service';
import { Playlist } from '../playlist/entities/playlist.entity';
import { Screen } from '../screen/entities/screen.entity';
import { SmilController } from './smil.controller';
import { SmilService } from './smil.service';
import { Scenario } from '../scenario/entities/scenario.entity';
import { ScenarioRule } from '../scenario/entities/scenario-rule.entity';
import { TriggerService } from '../../services/trigger.service';
import { WeatherService } from '../../services/weather.service';
import { ScreenRegService } from '../admin/screen-reg/screen-reg.service';
import { ScreenReg } from '../admin/screen-reg/entities/screen-reg.entity';
import { ScreenHistory } from '../screen/entities/screen-history.entity';
import { HelperService } from '../../services/helper.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Screen,
            ScreenHistory,
            ScreenRequestLogger,
            Scenario,
            ScenarioRule,
            ScreenReg,
            Playlist,
        ]),
        HttpModule,
    ],
    controllers: [SmilController],
    providers: [
        SmilService,
        ScreenRequestLoggerService,
        TriggerService,
        WeatherService,
        ScreenRegService,
        HelperService,
    ],
})
export class SmilModule {}
