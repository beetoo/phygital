import { Controller, Get, Head, Headers, Post, Req, Res, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Request, Response } from 'express';

import { SmilService } from './smil.service';
import { ScreenRequestLoggerService } from '../admin/screen-request-logger/screen-request-logger.service';
import { WeatherService } from '../../services/weather.service';
import { isLocalDevelopmentServer } from '../../constants';
import { Public } from '../../decorators/public.decorator';
import { HelperService } from '../../services/helper.service';
import { ValidationService } from '../../services/validation/validation.service';

@Public()
@Controller('smil')
export class SmilController {
    constructor(
        private readonly smilService: SmilService,
        private readonly screenLogger: ScreenRequestLoggerService,
        private readonly weatherService: WeatherService,
        private readonly helperService: HelperService,
        private readonly validationService: ValidationService,
    ) {}

    private static requestJsonText(req: Request): string {
        return JSON.stringify({
            headers: req.headers,
            originalUrl: req.originalUrl,
            params: req.params,
            query: req.query,
            body: req.body,
            method: req.method,
        });
    }

    @Head()
    async getHeadSmil(
        @Headers('identification') identification: string,
        @Headers('availableBytes') availableBytes: number,
        @Headers('totalBytes') totalBytes: number,
        @Headers('cpuUsagePercent') cpuUsagePercent: number,
        @Headers('ramUsagePercent') ramUsagePercent: number,
        @Headers('ramTotalMB') ramTotalMB: number,
        @Req() req: Request,
        @Res({ passthrough: true }) res: Response,
    ) {
        if (identification === 'afe12aa6-517e-48e2-a517-c2ef3f737185') {
            console.log('availableBytes', availableBytes);
            console.log('totalBytes', totalBytes);
            console.log('cpuUsagePercent', cpuUsagePercent);
            console.log('ramUsagePercent', ramUsagePercent);
            console.log('ramTotalMB', ramTotalMB);
        }

        const screen = identification ? await this.smilService.infoScreensForSmil(identification) : null;

        try {
            const { lastModified } = await this.smilService.smilDataForIdentification(screen, identification, false);

            res.header('Last-Modified', lastModified);
            res.contentType('application/smil');

            const message = `last modified (${lastModified})`;

            Promise.resolve().then(async () => {
                await this.smilService.updateScreenBytesInfo(identification, { availableBytes, totalBytes });
                await this.smilService.updateScreenAliveAt(identification);
                await this.helperService.updateScreenStatus(identification, screen);
                await this.screenLogger.createOrUpdateLoggerSuccess(
                    identification,
                    screen ? screen.id : '',
                    'HEAD',
                    SmilController.requestJsonText(req),
                    message,
                );
            });
        } catch (e) {
            if (isLocalDevelopmentServer) {
                console.log(e);
            }

            Promise.resolve().then(async () => {
                await this.screenLogger.createOrUpdateLoggerError(
                    identification,
                    screen ? screen.id : '',
                    'HEAD',
                    SmilController.requestJsonText(req),
                    e.toString(),
                );
            });

            throw e;
        }
    }

    @Get()
    async getSmil(
        @Headers('identification') identification: string,
        @Headers('versionCode') versionCode: string,
        @Res({ passthrough: true }) res: Response,
        @Req() req: Request,
    ) {
        if (!identification) {
            return `<smil></smil>`;
        }

        const screen = await this.smilService.infoScreensForSmil(identification);

        try {
            const { lastModified, smil, hasCustomSmil } = await this.smilService.smilDataForIdentification(
                screen,
                identification,
                true,
            );

            res.header('Last-Modified', lastModified);
            res.contentType('application/smil');

            Promise.resolve().then(async () => {
                await this.screenLogger.createOrUpdateLoggerSuccess(
                    identification,
                    screen?.id ?? '',
                    'GET',
                    SmilController.requestJsonText(req),
                    smil,
                );
            });

            let fixSmil = smil;
            //fix for player version 6.2.0
            if (!hasCustomSmil && versionCode === '70') {
                if (smil.indexOf('PORTRAIT_FLIPPED') > 0) {
                    fixSmil = smil.replace('"PORTRAIT_FLIPPED"', '"PORTRAIT"');
                } else if (smil.indexOf('PORTRAIT') > 0) {
                    fixSmil = smil.replace('"PORTRAIT"', '"PORTRAIT_FLIPPED"');
                }
            }

            return fixSmil;
        } catch (e) {
            Promise.resolve().then(() => {
                this.screenLogger.createOrUpdateLoggerError(
                    identification,
                    screen?.id ?? '',
                    'GET',
                    SmilController.requestJsonText(req),
                    e.toString(),
                );
            });

            throw e;
        }
    }

    @Get('cachecontent')
    getCacheContentSmil(@Headers('identification') identification: string) {
        return this.smilService.getCacheContentSmil(identification);
    }

    @Post('status')
    storeUploadStatus() {
        // @Headers('versionCode') versionCode: string,
        // @Headers('identification') identification: string,
        // @Body() playlistUploadDto: any,

        return { message: 'OK' };

        // const screen = await this.smilService.infoScreensForSmil(identification);
        // const updateHistory = isLocalDevelopmentServer || isDevProductionServer || Number(versionCode) > 30;
    }

    @Get('w')
    async getW(@Res({ passthrough: true }) res: Response) {
        res.send(await this.weatherService.getWeatherToPoint(55.78108933370161, 37.74282999067608));
    }

    @UseInterceptors(FileInterceptor('file'))
    @Post('screenshot')
    addScreenshot(@Headers('identification') identification: string, @UploadedFile() file: Express.Multer.File) {
        this.validationService.throwBadRequestExceptionIf(!identification, { identification: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!file, { file: 'IS_REQUIRED' });

        return this.smilService.addScreenshot(identification, file);
    }

    @Get('test')
    getTestSmil() {
        return `<smil>
    <head>
        <meta http-equiv="refresh" content="60"/>
        <meta log="true"/>
        <meta onlySmilUpdate="true"/>
        <meta orientation="LANDSCAPE"/>

        <layout>
            <root-layout width="1920" height="1080" backgroundColor="#FFFFFF" />
            <region regionName="left" left="0" top="0" width="1440" height="1080" />
            <region regionName="topRight" left="1440" top="0" width="480" height="240" />
            <region regionName="middleRight" left="1440" top="240" width="480" height="480" />
            <region regionName="bottomRight" left="1440" top="720" width="480" height="360" />
        </layout>
    </head>

    <body>
        <par>
            <seq end="__prefetchEnd.endEvent">
                <seq repeatCount="indefinite">
                    <video src="http://localhost:3000/api/static/smil/progress_vertical.mp4" region="left" />
                </seq>
            </seq>
            <seq>
                <prefetch src="http://localhost:3000/api/static/smil/holderimage_vertical.jpg" />
                <seq id="__prefetchEnd" dur="1s" />
            </seq>
            <par begin="__prefetchEnd.endEvent" repeatCount="indefinite">
                <seq repeatCount="indefinite">
                    <img src="http://localhost:3000/api/static/smil/holderimage_vertical.jpg" dur="60" region="left"></img>
                </seq>
            </par>
        </par>
    </body>
</smil>`;
    }
}
