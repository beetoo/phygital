import { PartialType } from '@nestjs/mapped-types';
import { CreateSmilDto } from './create-smil.dto';

export class UpdateSmilDto extends PartialType(CreateSmilDto) {}
