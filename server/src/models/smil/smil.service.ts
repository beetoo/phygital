import fs from 'node:fs';
import { Inject, Injectable } from '@nestjs/common';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EventEmitter2 } from '@nestjs/event-emitter';
import hash from 'object-hash';
import { mkdirp } from 'mkdirp';
import sharp from 'sharp';

import { TriggerService } from '../../services/trigger.service';
import { Screen } from '../screen/entities/screen.entity';
import { SMILSerializable } from './types/commonModels';
import { SMILImage, SMILRef, SMILVideo } from './types/mediaModels';
import { ParPlaylistElement, SeqPlaylistElement } from './types/playlistModels';
import { LayoutObject, RegionObject, RootLayout, SynchronizeConfig, XmlSmilObject } from './types/xmlJsonModels';
import { CONTENT_TYPE, PlaylistType } from '../../../../common/types';
import { SERVER_API_URL, SYNC_SERVER_URL, UPLOAD_PATH_SCREENSHOT } from '../../constants';
import { formatScreenForSmil, formatUrlForWebContent, getCurrentTimePlaylist, withContentLinks } from '../../helpers';
import { ScreenRegService } from '../admin/screen-reg/screen-reg.service';
import { ScreenReg } from '../admin/screen-reg/entities/screen-reg.entity';
import { getContentSrc } from '../../utils/getContentSrc';
import { calculateIntervalPlaylist } from './helpers';
import { Playlist } from '../playlist/entities/playlist.entity';
import { isImageFullyBlack } from '../../helpers/isImageFullyBlack';
import { defaultRegions } from '../playlist/helpers';

const kbInPx = 13319 / (1920 * 1080); // кб в 1 пикселе

@Injectable()
export class SmilService {
    constructor(
        @InjectRepository(Screen)
        private screenRepository: Repository<Screen>,
        @InjectRepository(Playlist)
        private playlistRepository: Repository<Playlist>,
        private triggerService: TriggerService,
        private screenRegService: ScreenRegService,
        private eventEmitter: EventEmitter2,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
    ) {}

    async smilDataForIdentification(screen: Screen | null, identification: string, generatorSmilContent: boolean) {
        //Devices new
        if (!screen) {
            return this.smilDataForScreenReg(identification, generatorSmilContent);
        } else {
            return this.smilDataForScreen(screen);
        }
    }

    private async smilDataForScreenReg(identification: string, generatorSmilContent: boolean) {
        let screenReg = await this.screenRegService.getScreenRegVerification(identification);
        if (!screenReg) {
            screenReg = await this.screenRegService.addScreenRegVerification(identification);
        }

        const lastModified = screenReg.createdAt;
        const smil = generatorSmilContent ? this.generatorSmilReg(screenReg) : '';

        return {
            lastModified: lastModified.toUTCString(),
            smil,
            currentPlaylist: undefined,
            hasCustomSmil: false,
        };
    }

    private async calculateCurrentPlaylist(screen: Screen) {
        let currentPlaylist = getCurrentTimePlaylist(screen);

        // it respects playlist's schedule and triggers (Rules, Scenarios)

        if (currentPlaylist) {
            const hasItemScenarios = (currentPlaylist.playlistItems ?? [])?.some((item) => item.scenario !== null);

            if (hasItemScenarios) {
                currentPlaylist = await this.triggerService.getTriggeredPlaylist(screen, currentPlaylist);
            }
        }

        return currentPlaylist;
    }

    private async smilDataForScreen(screen: Screen) {
        let smil;
        let currentPlaylist;

        if (screen.smil) {
            smil = screen.smil;
        } else {
            currentPlaylist = await this.calculateCurrentPlaylist(screen);
            smil = await this.generatorSmil(screen, currentPlaylist);
        }

        const prevSmil = (await this.cacheManager.get(`player:smil:${screen.identification}`)) as {
            hash: string;
            modifiedAt: string;
            smil: string;
        };

        let lastModified;

        const smilHash = hash(smil);

        if (!prevSmil || prevSmil.hash !== smilHash) {
            lastModified = new Date();

            if (smil) {
                await this.cacheManager.set(
                    `player:smil:${screen.identification}`,
                    {
                        hash: smilHash,
                        modifiedAt: lastModified,
                        smil,
                    },
                    0,
                );

                // обновление значения isEmpty
                await this.screenRepository.update(screen.id, { isEmpty: !currentPlaylist });

                console.log(`Last-Modified for identification ${screen.identification} was changed.`);
            }
        } else {
            lastModified = new Date(prevSmil.modifiedAt);
        }

        return {
            lastModified: lastModified.toUTCString(),
            smil,
            currentPlaylist,
            hasCustomSmil: !!screen.smil,
        };
    }

    async infoScreensForSmil(identification: string) {
        const screenCache = (await this.cacheManager.get(`player:data:${identification}`)) as { screen: Screen | null };

        let screen = screenCache?.screen;

        if (!screen) {
            screen = await this.screenRepository.findOne({
                where: { identification },
                relations: {
                    location: true,
                    playlists: { playlistItems: { content: true, scenario: true } },
                },
            });

            if (screen) {
                screen = formatScreenForSmil(withContentLinks(screen)) as unknown as Screen;

                await this.cacheManager.set(
                    `player:data:${identification}`,
                    {
                        hash: hash(screen),
                        modifiedAt: new Date(),
                        screen,
                    },
                    0,
                );
            }
        }

        return screen;
    }

    private async generatorSmil(screen: Screen, currentPlaylist: PlaylistType | undefined) {
        const defaultRegion = 'main';

        const {
            resolution: { width, height },
            orientation,
        } = screen;

        const isVerticalOrientation =
            (height > width && orientation.includes('LANDSCAPE')) ||
            (width >= height && orientation.includes('PORTRAIT'));

        const defaultResolutions = [
            { width: 1280, height: 720 },
            { width: 1920, height: 1080 },
            { width: 3840, height: 2160 },
        ];

        const isLedScreen = !defaultResolutions.some(
            (resolution) =>
                (resolution.width === width && resolution.height === height) ||
                (resolution.width === height && resolution.height === width),
        );

        let sequence = [] as SMILSerializable[];
        let syncConfig = new SynchronizeConfig();

        const results: SeqPlaylistElement[] = [];
        const regions: RegionObject[] = [];

        const playlistRegions =
            currentPlaylist?.regions && Array.isArray(currentPlaylist.regions) && currentPlaylist.regions.length > 0
                ? currentPlaylist?.regions
                : defaultRegions();

        if (currentPlaylist) {
            let playlistItems = currentPlaylist.playlistItems ?? [];

            if (currentPlaylist.type === 'interval') {
                playlistItems = calculateIntervalPlaylist(playlistItems, currentPlaylist.intervalDuration ?? 0);
            }

            playlistRegions.forEach((region, _, arr) => {
                const regionPlaylistItems = playlistItems.filter((item) => (item.regionId ?? 1) === region.id);

                regionPlaylistItems.forEach(
                    ({ delay, regionId, content: { id, filename, type, src, webContentLink, urlParamMode } }) => {
                        src = src ? src : type === CONTENT_TYPE.WEB ? webContentLink : getContentSrc(id, filename);

                        const regionName = arr.length === 1 ? defaultRegion : `region_${regionId}`;

                        let smilLink;

                        if (type === CONTENT_TYPE.VIDEO) {
                            smilLink = new SMILVideo(src, regionName);
                        } else if (type === CONTENT_TYPE.IMAGE) {
                            smilLink = new SMILImage(src, delay.toString(), regionName);
                        } else {
                            smilLink = new SMILRef(
                                formatUrlForWebContent(src, screen, urlParamMode),
                                delay.toString(),
                                regionName,
                            );
                        }

                        sequence.push(smilLink);
                    },
                );

                const result = new SeqPlaylistElement(sequence);
                results.push(result);
                sequence = [];
            });

            if (currentPlaylist.sync) {
                let syncGroupIds = await this.cacheManager.get<string>(
                    `player:smil:syncGroupIds:${currentPlaylist.id}`,
                );

                if (!syncGroupIds) {
                    const playlist = await this.playlistRepository.findOne({
                        where: { id: currentPlaylist.id },
                        relations: { screens: true },
                    });

                    syncGroupIds = playlist?.screens.map(({ id }) => id).join(';') ?? '';

                    await this.cacheManager.set(`player:smil:syncGroupIds:${currentPlaylist.id}`, syncGroupIds, 0);
                }

                if (syncGroupIds.split(';').length > 1) {
                    syncConfig = new SynchronizeConfig(SYNC_SERVER_URL, currentPlaylist.id, syncGroupIds);
                }
            }
        } else {
            const img = new SMILImage(
                `${SERVER_API_URL}/static/smil/holderimage${isVerticalOrientation ? '_vertical' : ''}.jpg`,
                '60',
                defaultRegion,
            );

            sequence.push(img);
            const result = new SeqPlaylistElement(sequence);
            results.push(result);

            const region = new RegionObject(defaultRegion, 0, 0, width, height, false);
            regions.push(region);
        }

        const enabledLogger = currentPlaylist?.log ?? true;
        const enabledSync = currentPlaylist?.sync ?? false;

        const rootLayout = new RootLayout(width, height, '#FFFFFF', isLedScreen, screen.organizationId);

        if (currentPlaylist) {
            playlistRegions.forEach(({ id, width, height, xPosition: left, yPosition: top }, _, arr) => {
                const regionName = arr.length === 1 ? defaultRegion : `region_${id}`;
                const regionWidth = arr.length === 1 ? '100%' : width;
                const regionHeight = arr.length === 1 ? '100%' : height;

                const region = new RegionObject(regionName, left, top, regionWidth, regionHeight, enabledSync);

                regions.push(region);
            });
        }

        const layoutObject = new LayoutObject(rootLayout, regions);

        const xmlSmilObject = new XmlSmilObject(
            60,
            syncConfig,
            layoutObject,
            results,
            enabledLogger,
            orientation,
            isVerticalOrientation,
            screen.meta, // 2147483640 - полное отключение
        );

        return xmlSmilObject.serialize();
    }

    private generatorSmilReg(screenReg: ScreenReg) {
        const defaultRegion = 'main';
        const hashRegion = 'hash';

        const width = 1920;
        const height = 1080;
        const orientation = 'LANDSCAPE';

        console.log(JSON.stringify(screenReg));

        const isVerticalOrientation =
            (height > width && orientation.includes('LANDSCAPE')) ||
            (width >= height && orientation.includes('PORTRAIT'));

        const parSequence = [] as SMILSerializable[];
        const syncConfig = new SynchronizeConfig();
        const img = new SMILImage(
            `${SERVER_API_URL}/static/smil/holderimage${isVerticalOrientation ? '_vertical' : ''}.jpg`,
            '30',
            defaultRegion,
        );
        const hashImg = new SMILImage(
            `${SERVER_API_URL}/static/smil/hash/${screenReg.identification}.png`,
            '30',
            hashRegion,
        );
        parSequence.push(img);
        parSequence.push(hashImg);

        const result = new ParPlaylistElement(parSequence);

        const rootLayout = new RootLayout(width, height, '#FFFFFF', false, '');
        const regionObject = new RegionObject(defaultRegion, 0, 0, '100%', '100%', false);
        const hashObject = new RegionObject(hashRegion, 0, 0, '100%', '100%', false);

        const layoutObject = new LayoutObject(rootLayout, [regionObject, hashObject]);

        const xmlSmilObject = new XmlSmilObject(
            30,
            syncConfig,
            layoutObject,
            result,
            false,
            orientation,
            isVerticalOrientation,
        );

        return xmlSmilObject.serialize();
    }

    addScreenshot(identification: string, file: Express.Multer.File) {
        const folderPath = `${UPLOAD_PATH_SCREENSHOT}/${identification}`;

        const MAX_BLACK_SCREENSHOT_COUNT = 8;
        const MAX_RANDOM_PERCENT = 40;

        Promise.resolve().then(async () => {
            try {
                await mkdirp(folderPath);

                const screenshotPath = `${folderPath}/screenshot.jpeg`;
                await fs.promises.writeFile(screenshotPath, file.buffer);

                const prevScreenshotSize =
                    (await this.cacheManager.get<number>(`player:screenshotSize:${identification}`)) ?? 0;

                await this.cacheManager.set(`player:screenshotSize:${identification}`, file.size, 0);

                const blackScreenshotCount =
                    (await this.cacheManager.get<number>(`player:blackScreenshotCount:${identification}`)) ?? 0;
                const clearCacheCount =
                    (await this.cacheManager.get<number>(`player:clearCacheCount:${identification}`)) ?? 0;

                if (file.size !== prevScreenshotSize) {
                    await this.resetBlackScreenshotCount(identification, blackScreenshotCount, clearCacheCount);
                    return;
                }

                const image = sharp(file.buffer);
                const { width = 0, height = 0 } = await image.metadata();
                const maxScreenshotSize = Math.round(width * height * kbInPx);

                if (file.size > maxScreenshotSize) {
                    await this.resetBlackScreenshotCount(identification, blackScreenshotCount, clearCacheCount);
                    return;
                }

                if (blackScreenshotCount > MAX_BLACK_SCREENSHOT_COUNT) return;

                if (Math.random() < Math.floor((1 - MAX_RANDOM_PERCENT / 100) * 1000) / 1000) return;

                if (await isImageFullyBlack(image)) {
                    if (blackScreenshotCount === MAX_BLACK_SCREENSHOT_COUNT) {
                        this.eventEmitter.emit('screen.blackScreenshot', { identification, buffer: file.buffer });
                    }

                    await this.cacheManager.set(
                        `player:blackScreenshotCount:${identification}`,
                        blackScreenshotCount + 1,
                        0,
                    );
                } else {
                    await this.resetBlackScreenshotCount(identification, blackScreenshotCount, clearCacheCount);
                }
            } catch (error) {
                console.error('Error in addScreenshot:', error);
            }
        });

        return { identification };
    }

    private async resetBlackScreenshotCount(
        identification: string,
        blackScreenshotCount: number,
        clearCacheCount: number,
    ) {
        if (blackScreenshotCount > 0) {
            await this.cacheManager.set(`player:blackScreenshotCount:${identification}`, 0, 0);
        }

        if (clearCacheCount > 0) {
            // сброс счётчика clearCache
            await this.cacheManager.set(`player:clearCacheCount:${identification}`, 0, 0);
        }
    }

    async getCacheContentSmil(identification: string) {
        const screen = await this.infoScreensForSmil(identification);

        const contentSrc: string[] = [];

        if (screen?.prefetchTime === null) {
            const playlistContentSrc = screen.playlists
                .flatMap((playlist) => playlist.playlistItems)
                .filter(
                    ({ content }) =>
                        content.type === CONTENT_TYPE.IMAGE ||
                        content.type === CONTENT_TYPE.VIDEO ||
                        content.type === CONTENT_TYPE.WIDGET,
                )
                .map(({ content }) => (content.src ? content.src : getContentSrc(content.id, content.filename)));

            for (const src of playlistContentSrc) {
                if (!contentSrc.includes(src)) {
                    contentSrc.push(src);
                }
            }
        } else {
            const prefetchMinutes = Math.ceil((screen?.prefetchTime ?? 5 * 60 * 60) / 60);

            for (let addedMinutes = 0; addedMinutes <= prefetchMinutes; addedMinutes++) {
                const currentPlaylist = getCurrentTimePlaylist(screen, addedMinutes);
                const playlistContentSrc =
                    currentPlaylist?.playlistItems
                        ?.filter(
                            ({ content }) =>
                                content.type === CONTENT_TYPE.IMAGE ||
                                content.type === CONTENT_TYPE.VIDEO ||
                                content.type === CONTENT_TYPE.WIDGET,
                        )
                        .map(({ content }) =>
                            content.src ? content.src : getContentSrc(content.id, content.filename),
                        ) ?? [];

                for (const src of playlistContentSrc) {
                    if (!contentSrc.includes(src)) {
                        contentSrc.push(src);
                    }
                }
            }
        }

        let smil = '';
        smil += '<smil>' + '\n';
        for (const src of contentSrc) {
            smil += `<prefetch src="${src}" />` + '\n';
        }
        smil += '</smil>';

        return smil;
    }

    async updateScreenAliveAt(identification: string) {
        if (!identification) return;

        await this.screenRepository.update({ identification }, { aliveAt: new Date() });
    }

    async updateScreenBytesInfo(
        identification: string,
        { availableBytes, totalBytes }: { availableBytes: number; totalBytes: number },
    ) {
        if (!identification || isNaN(availableBytes) || isNaN(totalBytes)) return;

        const prevScreenCache = (await this.cacheManager.get(`player:data:${identification}`)) as { screen: Screen };

        if (!prevScreenCache || !prevScreenCache.screen) return;

        const { availableBytes: prevAvailableBytes, totalBytes: prevTotalBytes } = prevScreenCache.screen;

        if (isNaN(prevAvailableBytes) || isNaN(prevTotalBytes)) return;

        if (availableBytes !== prevAvailableBytes || totalBytes !== prevTotalBytes) {
            await Promise.all([
                this.screenRepository.update({ identification }, { availableBytes, totalBytes }),
                this.cacheManager.set(
                    `player:data:${identification}`,
                    {
                        ...prevScreenCache,
                        screen: { ...prevScreenCache.screen, availableBytes, totalBytes },
                    },
                    0,
                ),
            ]);
        }
    }

    // private async generatorSmilOld(screen: Screen, currentPlaylist: PlaylistType | undefined) {
    //     const cacheTemplate = await this.cacheManager.get<{ hash: string; template: Template }>(
    //         `playlist:template:${currentPlaylist?.id}`,
    //     );
    //
    //     const templateSections = cacheTemplate?.template?.sections?.map((section) =>
    //         section?.content?.every((value) => typeof value === 'string')
    //             ? { ...section, content: section?.content?.map((src) => ({ src, urlParamMode: 'none' })) }
    //             : section,
    //     );
    //
    //     const defaultRegion = 'main';
    //
    //     const defaultRegions = templateSections
    //         ? templateSections
    //             ?.filter(({ content }) => !content || content.length === 0)
    //             .map(({ fit }, index) => ({ region: `region_${index}`, fit }))
    //         : [{ region: defaultRegion, fit: undefined }];
    //
    //     const {
    //         resolution: { width, height },
    //         orientation,
    //     } = screen;
    //
    //     const isVerticalOrientation =
    //         (height > width && orientation.includes('LANDSCAPE')) ||
    //         (width >= height && orientation.includes('PORTRAIT'));
    //
    //     const defaultResolutions = [
    //         { width: 1280, height: 720 },
    //         { width: 1920, height: 1080 },
    //         { width: 3840, height: 2160 },
    //     ];
    //
    //     const isLedScreen = !defaultResolutions.some(
    //         (resolution) =>
    //             (resolution.width === width && resolution.height === height) ||
    //             (resolution.width === height && resolution.height === width),
    //     );
    //
    //     const sequence = [] as SMILSerializable[];
    //     let syncConfig = new SynchronizeConfig();
    //
    //     if (currentPlaylist) {
    //         let playlistItems = currentPlaylist.playlistItems;
    //
    //         if (currentPlaylist.type === 'interval') {
    //             playlistItems = calculateIntervalPlaylist(playlistItems, currentPlaylist.intervalDuration ?? 0);
    //         }
    //
    //         playlistItems.forEach(({ delay, content: { id, filename, type, src, webContentLink, urlParamMode } }) => {
    //             src = src ? src : type === CONTENT_TYPE.WEB ? webContentLink : getContentSrc(id, filename);
    //
    //             for (const { region, fit } of defaultRegions) {
    //                 let smilLink;
    //
    //                 if (type === CONTENT_TYPE.VIDEO) {
    //                     smilLink = new SMILVideo(src, region);
    //                 } else if (type === CONTENT_TYPE.IMAGE) {
    //                     smilLink = new SMILImage(src, delay.toString(), region);
    //                 } else {
    //                     smilLink = new SMILRef(
    //                         formatUrlForWebContent(src, screen, urlParamMode),
    //                         delay.toString(),
    //                         region,
    //                         fit,
    //                     );
    //                 }
    //
    //                 sequence.push(smilLink);
    //             }
    //         });
    //
    //         if (currentPlaylist.sync) {
    //             let syncGroupIds = await this.cacheManager.get<string>(
    //                 `player:smil:syncGroupIds:${currentPlaylist.id}`,
    //             );
    //
    //             if (!syncGroupIds) {
    //                 const playlist = await this.playlistRepository.findOne({
    //                     where: { id: currentPlaylist.id },
    //                     relations: { screens: true },
    //                 });
    //
    //                 syncGroupIds = playlist?.screens.map(({ id }) => id).join(';') ?? '';
    //
    //                 await this.cacheManager.set(`player:smil:syncGroupIds:${currentPlaylist.id}`, syncGroupIds, 0);
    //             }
    //
    //             syncConfig = new SynchronizeConfig(SYNC_SERVER_URL, currentPlaylist.id, syncGroupIds, screen.id);
    //         }
    //     } else {
    //         const img = new SMILImage(
    //             `${SERVER_API_URL}/static/smil/holderimage${isVerticalOrientation ? '_vertical' : ''}.jpg`,
    //             '60',
    //             defaultRegion,
    //         );
    //
    //         sequence.push(img);
    //     }
    //
    //     const enabledLogger = currentPlaylist?.log ?? true;
    //     const enabledSync = currentPlaylist?.sync ?? false;
    //     const result = new SeqPlaylistElement(sequence);
    //
    //     const rootLayout = new RootLayout(width, height, '#FFFFFF', isLedScreen, screen.organizationId);
    //
    //     const regions: RegionObject[] = [];
    //     const additionalSequence: PlayElement[] = [];
    //
    //     if (!templateSections) {
    //         const regionObject = new RegionObject(defaultRegion, 0, 0, '100%', '100%', enabledSync);
    //
    //         regions.push(regionObject);
    //     } else {
    //         // создаем ссылки
    //         templateSections.forEach(
    //             ({ sectionName, left, top, width, height, fit, content: sectionContent }, index) => {
    //                 const region = new RegionObject(
    //                     sectionName ?? `region_${index}`,
    //                     left,
    //                     top,
    //                     width,
    //                     height,
    //                     enabledSync,
    //                 );
    //                 regions.push(region);
    //
    //                 const content = sectionContent as { src: string; urlParamMode?: UrlParamModeType }[];
    //
    //                 if (content) {
    //                     const duration = content.length === 1 ? '15' : '15';
    //
    //                     for (const { src, urlParamMode } of content) {
    //                         let ref;
    //
    //                         if (isVideo(src)) {
    //                             ref = new SMILVideo(src, sectionName ?? `region_${index}`);
    //                         } else if (isImage(src)) {
    //                             ref = new SMILImage(src, duration, sectionName ?? `region_${index}`);
    //                         } else {
    //                             ref = new SMILRef(
    //                                 formatUrlForWebContent(src, screen, urlParamMode),
    //                                 duration,
    //                                 sectionName ?? `region_${index}`,
    //                                 fit,
    //                             );
    //                         }
    //
    //                         additionalSequence.push(ref);
    //                     }
    //                 }
    //             },
    //         );
    //     }
    //
    //     const playlist = additionalSequence.length > 0 ? [result, new ParPlaylistElement(additionalSequence)] : result;
    //
    //     const layoutObject = new LayoutObject(rootLayout, regions);
    //
    //     const xmlSmilObject = new XmlSmilObject(
    //         60,
    //         syncConfig,
    //         layoutObject,
    //         playlist,
    //         enabledLogger,
    //         orientation,
    //         isVerticalOrientation,
    //         screen.meta, // 2147483640 - полное отключение
    //     );
    //
    //     return xmlSmilObject.serialize();
    // }

    // private async getLastModified(screen: Screen, currentPlaylist?: PlaylistType): Promise<Date> {
    //     const screenSmilLastModified = screen.smilModifiedAt ?? new Date(0);
    //     const screenUrlParamsLastModified = screen.urlParamsModifiedAt ?? new Date(0);
    //     const playlistLastModified = currentPlaylist?.updatedAt || new Date(0);
    //
    //     // lastModified
    //     let fakeLastModified;
    //
    //     let realLastModified;
    //     if (screen.smil) {
    //         realLastModified = screenSmilLastModified;
    //     } else {
    //         const webItems =
    //             currentPlaylist?.playlistItems.filter((item) => item.content.type === CONTENT_TYPE.WEB) ?? [];
    //
    //         // updatedAt у плейлиста
    //         realLastModified = playlistLastModified;
    //
    //         if (webItems.length > 0) {
    //             // находим все modifiedAt
    //             const urlParamModeLastModifiedValues = webItems
    //                 .filter((item) => item.content.urlParamModeModifiedAt)
    //                 .map((item) => item.content.urlParamModeModifiedAt);
    //
    //             const lastModifiedValues = [
    //                 ...urlParamModeLastModifiedValues,
    //                 screenUrlParamsLastModified,
    //                 playlistLastModified,
    //             ].map((value) => (typeof value !== 'object' ? new Date(value) : value));
    //
    //             // и выбираем из них самый новый
    //             realLastModified = max(lastModifiedValues) ?? new Date(0);
    //         }
    //     }
    //
    //     const smilCache = await this.getSmilCache(screen.identification);
    //
    //     realLastModified = typeof realLastModified !== 'object' ? new Date(realLastModified) : realLastModified;
    //
    //     if (smilCache && smilCache.realLastModified.valueOf() === realLastModified.valueOf()) {
    //         fakeLastModified = smilCache.fakeLastModified;
    //     } else {
    //         let updatedSmilCache = smilCache || new SmilCache();
    //
    //         fakeLastModified = new Date();
    //         updatedSmilCache.identification = screen.identification;
    //         updatedSmilCache.realLastModified = realLastModified;
    //         updatedSmilCache.fakeLastModified = fakeLastModified;
    //         updatedSmilCache = await this.smilCacheRepository.save(updatedSmilCache);
    //
    //         await this.cacheManager.set(`smilCache:${screen.identification}`, updatedSmilCache, 0);
    //
    //         if (isLocalDevelopmentServer) {
    //             console.log(`Last-Modified for identification ${screen.identification} was changed.`);
    //         }
    //     }
    //
    //     return fakeLastModified;
    // }
}
