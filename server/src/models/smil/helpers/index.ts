import { PlaylistItemType } from '../../../../../common/types';

interface ItemWithTimeSlots extends PlaylistItemType {
    startTimes?: number[];
}

const filterByType = (items: ItemWithTimeSlots[], type: 'recurring' | 'filler' | null) =>
    items.filter((item) => (item.type ?? 'filler') === type).sort((a, b) => a.orderIndex - b.orderIndex);

export const getItemsDuration = (items: ItemWithTimeSlots[]) =>
    items.reduce((sum, { delay, content: { type, duration } }) => sum + (type === 'video' ? duration : delay), 0);

const addStartTimeSlots = (items: ItemWithTimeSlots[], intervalDuration: number) =>
    items.map((item) => {
        if (!item.properties) {
            return { ...item, startTimes: [0] };
        }

        const startTimes: number[] = [];

        const intervalNumber =
            item.properties.recurringRule === 'frequency'
                ? Math.floor(intervalDuration / item.properties.recurringValue)
                : item.properties.recurringValue * 60;

        for (let i = 0; i < intervalDuration; i += intervalNumber) {
            startTimes.push(i);
        }

        return { ...item, startTimes };
    });

const getAllStartTimeSlots = (items: ItemWithTimeSlots[]) =>
    items
        .flatMap((item) => item.startTimes ?? [])
        .reduce((times, time) => (times.includes(time) ? times : times.concat(time)), [] as number[])
        .sort((a, b) => a - b);

const getAllDurationTimeFillers = (items: PlaylistItemType[], intervalDuration: number) => {
    const repeatNumber = Math.ceil(intervalDuration / getItemsDuration(items));

    const fillers: PlaylistItemType[] = [];

    for (let i = 0; i < repeatNumber; i++) {
        fillers.push(...items);
    }

    return fillers;
};

export const calculateIntervalPlaylist = (playlistItems: PlaylistItemType[], intervalDuration: number) => {
    intervalDuration = intervalDuration * 60; // seconds

    let recurringItems = filterByType(playlistItems, 'recurring');
    const fillerItems = filterByType(playlistItems, 'filler');

    // если нет recurring files, то возвращаем все (остальные) playlistItems
    if (recurringItems.length === 0 || fillerItems.length === 0) {
        return playlistItems;
    }

    const recurringItemsDuration = getItemsDuration(recurringItems);

    // если продолжительность recurring files больше intervalDuration, то возвращаем только recurringItems
    if (recurringItemsDuration >= intervalDuration) {
        return recurringItems;
    }

    const noItemsWithProperties = !recurringItems.some((item) => item.properties);

    // если отсутствуют items с properties
    if (noItemsWithProperties) {
        const fillersDurationTime = intervalDuration - recurringItemsDuration;

        if (fillersDurationTime > 0) {
            const allDurationTimeFillers = getAllDurationTimeFillers(fillerItems, fillersDurationTime);

            const fillers: PlaylistItemType[] = [];

            for (let i = 0; i < allDurationTimeFillers.length; i++) {
                if (getItemsDuration(fillers) < fillersDurationTime) {
                    fillers.push(allDurationTimeFillers[i]);
                    i++;
                }
            }

            recurringItems.push(...fillers);
        }

        return recurringItems;
    }

    const allDurationTimeFillers = getAllDurationTimeFillers(fillerItems, intervalDuration);

    recurringItems = addStartTimeSlots(recurringItems, intervalDuration);

    const startTimeSlots = getAllStartTimeSlots(recurringItems);

    const items: PlaylistItemType[] = [];

    let fillerSliceIndex = 0;

    startTimeSlots.forEach((startTime, index, arr) => {
        const nextStartTime = arr[index + 1];

        let slotDuration = nextStartTime !== undefined ? nextStartTime - startTime : intervalDuration - startTime;

        const slotItems = recurringItems.filter((item) => item?.startTimes?.includes(startTime));

        if (slotItems.length > 0) {
            slotDuration -= getItemsDuration(slotItems);

            items.push(...slotItems);
        }

        if (slotDuration > 0) {
            const fillers: PlaylistItemType[] = [];

            for (let i = fillerSliceIndex; i < allDurationTimeFillers.length; i++) {
                if (getItemsDuration(fillers) < slotDuration) {
                    fillers.push(allDurationTimeFillers[i]);
                    fillerSliceIndex++;
                }
            }

            items.push(...fillers);
        }
    });

    return items;
};
