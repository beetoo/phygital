import { MediaObject } from './commonModels';
import { SERVER_API_URL } from '../../../constants';

export interface ISMILVideo extends MediaObject {
    region: string;
}

export class SMILVideo implements ISMILVideo {
    constructor(
        readonly src: string,
        public region: string,
    ) {}

    serialize(): string {
        return `<video src="${this.src}" region="${this.region}"></video>`;
    }
}

export interface ISMILImage extends MediaObject {
    dur: string;
    region: string;
}

export class SMILImage implements ISMILImage {
    constructor(
        readonly src: string,
        public dur: string,
        public region: string,
    ) {}

    serialize(): string {
        return `<img src="${this.src}" dur="${this.dur}" region="${this.region}"></img>`;
    }
}

export class SMILRef implements ISMILImage {
    constructor(
        readonly src: string,
        public dur: string,
        public region: string,
    ) {}

    serialize(): string {
        return `<ref src="${this.src}" dur="${this.dur}" region="${this.region}"></ref>`;
    }
}
