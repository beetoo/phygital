export interface SMILSerializable {
    serialize(): string;
}

export type PlayElement = SMILSerializable;

export interface MediaObject extends PlayElement {
    src: string;
}

export interface Playlist extends PlayElement {
    begin?: string;
    end?: string;
    repeatCount?: number | string;
    playElementes: Array<PlayElement>;
}
