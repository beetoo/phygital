import { SMILSerializable, Playlist, PlayElement, MediaObject } from './commonModels';
import { SMILImage, SMILVideo } from './mediaModels';
import { ParPlaylistElement, SeqPlaylistElement } from './playlistModels';
import { SERVER_API_URL } from '../../../constants';
import { defaultScreenMeta } from '../../screen/helpers';
import isWidget from '../../../../../common/helpers/isWidget';

export interface ISynchronizeConfig extends SMILSerializable {
    syncServerUrl: string;
    syncGroupName: string;
    syncGroupIds: string;
    syncDeviceId: string;
}

export class SynchronizeConfig implements ISynchronizeConfig {
    constructor(
        public syncServerUrl: string = '',
        public syncGroupName: string = '',
        public syncGroupIds: string = '',
        public syncDeviceId: string = '',
    ) {}

    serialize(): string {
        if (this.syncServerUrl && this.syncGroupName && this.syncGroupIds && this.syncDeviceId) {
            return `<meta syncServerUrl="${this.syncServerUrl}" syncGroupName="${this.syncGroupName}" syncGroupIds="${this.syncGroupIds}" syncDeviceId="${this.syncDeviceId}"/>`;
        } else if (this.syncServerUrl && this.syncGroupName && this.syncGroupIds) {
            return `<meta syncServerUrl="${this.syncServerUrl}" syncGroupName="${this.syncGroupName}" syncGroupIds="${this.syncGroupIds}"/>`;
        } else {
            return ``;
        }
    }
}

export interface ILayoutObject extends SMILSerializable {
    region: Array<IRegionObject>;
    rootLayout: IRootLayout;
}

export class LayoutObject implements ILayoutObject {
    constructor(
        public rootLayout: IRootLayout,
        public region: Array<IRegionObject>,
    ) {}

    serialize(): string {
        //  ${this.region.map((it) => it.serialize())}

        const breakString = '\n            ';

        return `
            ${this.rootLayout.serialize()}
            ${this.region.reduce((regions, it, idx, arr) => regions + it.serialize() + (idx !== arr.length - 1 ? breakString : ''), '')}
            `;
    }
}

export interface IRootLayout extends SMILSerializable {
    width: number;
    height: number;
    backgroundColor: string;
}

export class RootLayout implements IRootLayout {
    constructor(
        public width: number,
        public height: number,
        public backgroundColor: string,
        public isLedScreen: boolean,
        public organizationId: string,
    ) {}

    serialize(): string {
        const isUHD = this.width > 1920 || this.height > 1080;

        let width = this.isLedScreen ? this.width : isUHD ? 3840 : 1920;
        let height = this.isLedScreen ? this.height : isUHD ? 2160 : 1080;

        if (this.organizationId === 'a9a1451a-959b-4e15-a372-265b5f0f79e0') {
            width = this.width;
            height = this.height;
        }

        return `<root-layout width="${width}" height="${height}" backgroundColor="${this.backgroundColor}" />`;
    }
}

export interface IRegionObject extends SMILSerializable {
    regionName: string;
    left: string | number;
    top: string | number;
    width: string | number;
    height: string | number;
    'z-index'?: number;
    fit?: string;
    backgroundColor?: string;
    sync?: boolean;
}

export class RegionObject implements IRegionObject {
    constructor(
        public regionName: string,
        public left: string | number,
        public top: string | number,
        public width: string | number,
        public height: string | number,
        public sync: boolean,
    ) {}

    serialize(): string {
        let syncInfo = '';
        if (this.sync) syncInfo = 'sync="true"';
        return `<region regionName="${this.regionName}" left="${this.left}" top="${this.top}" width="${this.width}" height="${this.height}" ${syncInfo}/>`;
    }
}

export interface IXmlSmilObject extends SMILSerializable {
    refresh: number;
    layout: ILayoutObject;
    playlist: Playlist | Playlist[];
}

export class XmlSmilObject implements IXmlSmilObject {
    constructor(
        public refresh: number,
        public syncConfig: ISynchronizeConfig,
        public layout: ILayoutObject,
        public playlist: Playlist | Playlist[],
        public enabledLogger: boolean,
        public orientation: string,
        public isVerticalOrientation: boolean,
        public screenMeta = defaultScreenMeta(),
    ) {}

    serialize(): string {
        return `<smil>
            ${this.smilHead()}
            ${this.smilBody()}
        \n</smil>`;
    }

    private smilHead() {
        return `
    <head>
        ${this.smilRefreshMeta()}
        ${this.smilLoggerMeta()}
        ${this.smilOnlySmilUpdateMeta()}
        ${this.smilOrientationMeta()}
        ${this.smilSynchronizeMeta()}
        ${this.smilScreenshotMeta()}
        ${this.smilRestartMeta()}
        ${this.smilLayout()}
    </head>`;
    }

    private smilRefreshMeta() {
        return `<meta http-equiv="refresh" content="${this.refresh}"/>`;
    }

    private smilLoggerMeta() {
        if (this.enabledLogger) return `<meta log="true"/>`;
        else return ``;
    }

    private smilOnlySmilUpdateMeta() {
        return `<meta onlySmilUpdate="true"/>`;
    }

    private smilOrientationMeta() {
        return `<meta orientation="${this.orientation}"/>`;
    }

    private smilScreenshotMeta() {
        return `<meta screenshot="${this.screenMeta?.screenshotSec ?? defaultScreenMeta().screenshotSec}"/>`;
    }

    private smilRestartMeta() {
        let restartMeta = (this.screenMeta?.restartSec ?? defaultScreenMeta().restartSec) - 5;

        if (restartMeta <= 0) {
            restartMeta = 0;
        }

        return `<meta restart="${restartMeta}"/>`;
    }

    private smilSynchronizeMeta() {
        return this.syncConfig.serialize();
    }

    private smilLayout() {
        return `
        <layout>
            ${this.layout.serialize()}
        </layout>`;
    }

    private smilBody() {
        return `
    <body>
        <par>
            ${this.smilInto()}
            ${this.smilPrefetch()}
            ${this.smilContent()}
        </par>
    </body>`;
    }

    private smilInto() {
        return `<seq end="__prefetchEnd.endEvent">
            <seq repeatCount="indefinite">
                <video src="${SERVER_API_URL}/static/smil/progress${
                    this.isVerticalOrientation ? '_vertical' : ''
                }.mp4" />
            </seq>
        </seq>`;
    }

    private smilPrefetch() {
        const playlist = Array.isArray(this.playlist)
            ? this.playlist.flatMap((pl) => pl.playElementes)
            : this.playlist.playElementes;

        return `<seq>
            ${Array.from(this.prefetchMediaObject(playlist))
                .map((src) => `<prefetch src="${src}" />`)
                .join('\n')}
            <seq id="__prefetchEnd" dur="1s" />
        </seq>`;
    }

    private smilContent() {
        const playlist = Array.isArray(this.playlist)
            ? this.playlist.reduce((acc, pl) => acc + pl.serialize() + '\n', '')
            : this.playlist.serialize();

        return `<par begin="__prefetchEnd.endEvent" repeatCount="indefinite">
                ${playlist}
        </par>`;
    }

    private prefetchMediaObject(playElementes: Array<PlayElement>): Set<string> {
        const result: Set<string> = new Set<string>();
        for (const playElement of playElementes) {
            if (
                playElement instanceof SMILVideo ||
                playElement instanceof SMILImage ||
                isWidget((playElement as MediaObject).src)
            ) {
                result.add((playElement as MediaObject).src);
            } else if (playElement instanceof ParPlaylistElement || playElement instanceof SeqPlaylistElement) {
                this.prefetchMediaObject((playElement as Playlist).playElementes).forEach((it) => result.add(it));
            }
        }
        return result;
    }
}
