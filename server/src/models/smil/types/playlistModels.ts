import { Playlist, PlayElement } from './commonModels';

export type ISeqPlaylistElement = Playlist;

export class SeqPlaylistElement implements ISeqPlaylistElement {
    constructor(
        public playElementes: Array<PlayElement>,
        public repeatCount: string = 'indefinite',
    ) {}

    serialize(): string {
        return `<seq repeatCount="${this.repeatCount}">
                    ${this.playElementes.map((it) => it.serialize()).join('\n')}
                </seq>`;
    }
}

export type IParPlaylistElement = Playlist;

export class ParPlaylistElement implements IParPlaylistElement {
    constructor(
        public playElementes: Array<PlayElement>,
        public repeatCount: string = 'indefinite',
    ) {}

    serialize(): string {
        return `<par repeatCount="${this.repeatCount}">
                    ${this.playElementes.map((it) => it.serialize()).join('\n')}
                </par>`;
    }
}
