import { PartialType } from '@nestjs/mapped-types';

import { CreateOrganizationDto } from './create-organization.dto';
import { OrganizationStatusType, OrganizationSubscriptionType, UserRoleType } from '../../../../../common/types';

export class UpdateOrganizationDto extends PartialType(CreateOrganizationDto) {
    readonly name?: string;
    readonly status?: OrganizationStatusType;
    readonly freeScreens?: number;
    readonly subscription?: OrganizationSubscriptionType;
    readonly userRole?: UserRoleType;
}
