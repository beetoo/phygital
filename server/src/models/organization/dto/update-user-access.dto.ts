import { UserOrganizationRoleEnum } from '../../../../../common/types';

export class UpdateUserAccessDto {
    readonly userId: string;
    readonly role: UserOrganizationRoleEnum;
    readonly permissions: string[];
}
