import { OrganizationProductEnum } from '../../../../../common/types';

export class CreateOrganizationByUserDto {
    readonly name: string;
    readonly product: OrganizationProductEnum;
    readonly userId: string;
}
