export class CreateOrganizationTimeDto {
    readonly date: string;
    readonly seconds: number;
}
