import { UserOrganizationRoleEnum } from '../../../../../common/types';

export class CreateInviteDto {
    readonly email: string;
    readonly inviterId: string;
    readonly role: UserOrganizationRoleEnum;
    readonly permissions?: string[];
}
