export class CreatePlaylistDto {
    readonly name: string;
    readonly type: 'basic' | 'interval';
}
