import { OrganizationProductEnum } from '../../../../../common/types';

export class CreateOrganizationDto {
    readonly name: string;
    readonly surname: string;
    readonly email: string;
    readonly password: string;
    readonly product?: OrganizationProductEnum;
}
