import { PartialType } from '@nestjs/mapped-types';
import { CreateOrganizationDto } from './create-organization.dto';
import { OrganizationLanguageType } from '../../../../../common/types';

export class UpdateUserAndOrganizationDto extends PartialType(CreateOrganizationDto) {
    readonly id: string;
    readonly userId: string;
    readonly title?: string;
    readonly role?: 'admin' | 'accounting' | 'user';
    readonly name?: string;
    readonly surname?: string;
    readonly phone?: string;
    readonly maxScreens?: number;
    readonly maxStorageSize?: number;
    readonly password?: string;
    readonly newPassword?: string;
    readonly billingAvailable?: boolean;
    readonly language?: OrganizationLanguageType;
}
