import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { OrganizationService } from './organization.service';
import { OrganizationController } from './organization.controller';
import { Organization } from './entities/organization.entity';
import { Content } from '../content/entities/content.entity';
import { Screen } from '../screen/entities/screen.entity';
import { Playlist } from '../playlist/entities/playlist.entity';
import { Scenario } from '../scenario/entities/scenario.entity';
import { ScreenRequestLogger } from '../admin/screen-request-logger/entities/screen-request-logger.entity';
import { Folder } from '../folder/entities/folder.entity';
import { Location } from '../location/entities/location.entity';
import { User } from '../user/entities/user.entity';
import { UserOrganizationProperty } from '../user/entities/user-organization-property.entity';
import { OrganizationInvitation } from '../organization-invitation/entities/organization.invitation';
import { Template } from '../template/entities/template.entity';
import { SupportTicket } from '../support-ticket/entities/support-ticket.entity';
import { Payment } from '../payment/entities/payment.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Organization,
            OrganizationInvitation,
            User,
            UserOrganizationProperty,
            Content,
            Folder,
            Location,
            Screen,
            Template,
            Playlist,
            Scenario,
            ScreenRequestLogger,
            SupportTicket,
            Payment,
        ]),
    ],
    controllers: [OrganizationController],
    providers: [OrganizationService],
})
export class OrganizationModule {}
