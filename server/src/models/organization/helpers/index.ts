import { OrganizationType, UserOrganizationRoleEnum } from '../../../../../common/types';

export const getOwnerUser = (organization: OrganizationType) =>
    organization.users.find((user) =>
        user.properties.find((property) => property.role === UserOrganizationRoleEnum.owner),
    );
