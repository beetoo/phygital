import path from 'node:path';
import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Put,
    Query,
    UploadedFile,
    UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { validate } from 'uuid';
import { isEmail } from 'validator';
import { isValid } from 'date-fns';

import { FindOrganizationScreensQuery, OrganizationService } from './organization.service';
import { UpdatePlaylistsPriorityDto } from '../playlist/dto/update-playlists-priority.dto';
import { UpdateOrganizationDto } from './dto/update-organization.dto';
import { CreateWebContentDto } from '../content/dto/create-web-content.dto';
import { SUPPORTED_CONTENT_FORMATS } from '../../../../common/constants';
import { deleteFile } from '../../helpers/deleteFile';
import { getFileTypeModule } from '../../helpers/modules';
import { CreateScenarioDto } from '../scenario/dto/create-scenario.dto';
import { CreateOrganizationByUserDto } from './dto/create-organization-by-user.dto';
import { CreateFolderDto } from '../folder/dto/create-folder.dto';
import { User } from '../user/entities/user.entity';
import { CreatePlaylistDto } from './dto/create-playlist.dto';
import { Accounting } from '../../decorators/accounting.decorator';
import { CreateInviteDto } from './dto/create-invite.dto';
import { UpdateUserAccessDto } from './dto/update-user-access.dto';
import { CheckPermission, ControllerAction } from '../../decorators/check-permission.decorator';
import { ViewOnly } from '../../decorators/view-only.decorator';
import { ValidationService } from '../../services/validation/validation.service';
import { CreateTemplateDto } from '../template/dto/create-template.dto';
import { CreatePaymentDto } from '../payment/dto/create-payment.dto';
import { OrganizationProductEnum, OrganizationSubscriptionType, PaymentStatusType } from '../../../../common/types';
import isWidget from '../../../../common/helpers/isWidget';

@Controller('organization')
export class OrganizationController {
    constructor(
        private readonly organizationService: OrganizationService,
        private readonly validationService: ValidationService,
    ) {}

    // действие доступно всем
    @Post()
    createOrganizationByUser(@Body() createOrganizationDto: CreateOrganizationByUserDto) {
        const { userId, name, product } = createOrganizationDto;

        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!name, { name: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!Object.values(OrganizationProductEnum).includes(product), {
            product: 'INVALID',
        });

        return this.organizationService.createOrganizationByUser(createOrganizationDto);
    }

    @ViewOnly()
    @CheckPermission(ControllerAction.invite_user)
    @Get(':organizationId/invitations')
    findInvitations(@Param('organizationId') organizationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.findOrganizationInvitations(organizationId);
    }

    @CheckPermission(ControllerAction.update_organization)
    @Patch(':organizationId')
    updateOrganization(
        @Param('organizationId') organizationId: string,
        @Body() updateOrganizationDto: UpdateOrganizationDto,
    ) {
        const { name, status, product, freeScreens, subscription } = updateOrganizationDto;

        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(name !== undefined && !name, { name: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(status !== undefined && !status, { status: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(
            !!product && !Object.values(OrganizationProductEnum).includes(product),
            {
                product: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(
            freeScreens !== undefined && (isNaN(freeScreens) || freeScreens < 0),
            {
                freeScreens: 'INVALID',
            },
        );

        const { errors, isValid } = this.subscriptionValidation(subscription);

        this.validationService.throwBadRequestExceptionIf(!isValid, {
            subscription: errors.reduce((acc, val) => Object.assign(acc, val), {}),
        });

        return this.organizationService.updateOrganization(organizationId, updateOrganizationDto);
    }

    private subscriptionValidation(subscription: OrganizationSubscriptionType | null | undefined) {
        const errors: { [key: string]: any }[] = [];

        if (subscription === undefined || subscription === null) {
            return { errors, isValid: true };
        }

        const validKeys = ['inn', 'company', 'notes', 'screens', 'storage', 'validUntil'];
        const existedKeys = Object.keys(subscription);

        const isAllKeysValid =
            validKeys.length === existedKeys.length &&
            existedKeys.every((existedKey) => validKeys.includes(existedKey));

        if (!isAllKeysValid) {
            return { errors: [{ subscriptionFields: 'INVALID' }], isValid: false };
        }

        const isFloatNumber = (num: number) => num % 1 !== 0;

        for (const key of existedKeys) {
            const value = subscription[key];

            if (key === 'inn' && value !== null) {
                if (typeof value !== 'number' || isNaN(value) || value < 0 || isFloatNumber(value)) {
                    errors.push({ inn: 'INVALID' });
                }
            } else if (key === 'company') {
                if (typeof value !== 'string') {
                    errors.push({ company: 'INVALID' });
                }
            } else if (key === 'notes') {
                if (typeof value !== 'string') {
                    errors.push({ notes: 'INVALID' });
                }
            } else if (key === 'screens' && value !== null) {
                if (typeof value !== 'number' || isNaN(value) || value < 0 || isFloatNumber(value)) {
                    errors.push({ screens: 'INVALID' });
                }
            } else if (key === 'storage' && value !== null) {
                if (typeof value !== 'number' || isNaN(value) || value < 0 || isFloatNumber(value)) {
                    errors.push({ storage: 'INVALID' });
                }
            } else if (key === 'validUntil') {
                if (!isValid(new Date(value + '-01'))) {
                    errors.push({ validUntil: 'INVALID' });
                }
            }
        }

        return { errors, isValid: errors.length === 0 };
    }

    @CheckPermission(ControllerAction.invite_user)
    @Post(':organizationId/invite')
    createInvitation(@Param('organizationId') organizationId: string, @Body() createInviteDto: CreateInviteDto) {
        const { email, inviterId, role, permissions } = createInviteDto;

        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!isEmail(email ?? ''), { email: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!validate(inviterId), { inviterId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!['member', 'admin'].includes(role), { role: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!!permissions && !Array.isArray(permissions), {
            permissions: 'INVALID',
        });

        return this.organizationService.createInvite(organizationId, createInviteDto);
    }

    @CheckPermission(ControllerAction.update_user_access)
    @Post(':organizationId/members/updateAccess')
    updateAccess(@Param('organizationId') organizationId: string, @Body() updateUserAccessDto: UpdateUserAccessDto) {
        const { userId, role, permissions } = updateUserAccessDto;

        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!['member', 'admin'].includes(role), { role: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!Array.isArray(permissions), { permissions: 'INVALID' });

        return this.organizationService.updateUserAccess(organizationId, updateUserAccessDto);
    }

    @CheckPermission(ControllerAction.revoke_access)
    @Post(':organizationId/revokeAccess')
    revokeAccess(@Param('organizationId') organizationId: string, @Body('userId') userId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });

        return this.organizationService.revokeOrganizationAccess(organizationId, userId);
    }

    @CheckPermission(ControllerAction.set_owner)
    @Post(':organizationId/setOwner')
    setOwner(@Param('organizationId') organizationId: string, @Body('userId') userId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!validate(userId), { userId: 'INVALID' });

        return this.organizationService.setOrganizationOwner(organizationId, userId);
    }

    @Accounting()
    @CheckPermission(ControllerAction.view_organization)
    @Get(':organizationId')
    findOneOrganization(@Param('organizationId') organizationId: string, @Body('user') user: User) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.findOrganization(organizationId, user);
    }

    @CheckPermission(ControllerAction.leave_organization)
    @Post(':organizationId/leave')
    leaveOrganization(@Param('organizationId') organizationId: string, @Body('user') user: User) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.leaveOrganization(organizationId, user);
    }

    @CheckPermission(ControllerAction.create_playlist)
    @Post(':organizationId/playlists')
    createPlaylist(@Param('organizationId') organizationId: string, @Body() createPlaylistDto: CreatePlaylistDto) {
        const { name, type } = createPlaylistDto;

        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!name, { name: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(type !== undefined && !['basic', 'interval'].includes(type), {
            type: 'INVALID',
        });

        return this.organizationService.createPlaylist(organizationId, { name, type });
    }

    @CheckPermission(ControllerAction.view_playlists)
    @Get(':organizationId/playlists')
    findPlaylists(@Param('organizationId') organizationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.findOrganizationPlaylists(organizationId);
    }

    @CheckPermission(ControllerAction.view_scenarios)
    @Get(':organizationId/scenarios')
    findScenarios(@Param('organizationId') organizationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.findOrganizationScenarios(organizationId);
    }

    @CheckPermission(ControllerAction.create_scenario)
    @Post(':organizationId/scenarios')
    createScenario(@Param('organizationId') organizationId: string, @Body() createScenarioDto: CreateScenarioDto) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.createOrganizationScenario(organizationId, createScenarioDto);
    }

    @CheckPermission(ControllerAction.view_screens)
    @Get(':organizationId/screens')
    findScreens(@Param('organizationId') organizationId: string, @Query() queryValues: FindOrganizationScreensQuery) {
        const { locationId, isAdmin, status, isEmpty } = queryValues;

        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(
            locationId !== undefined && (locationId === 'all' ? false : !validate(locationId)),
            {
                locationId: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(!!isAdmin && !['true', 'false'].includes(isAdmin), {
            isAdmin: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(
            !!status && !status.split(',').every((status) => ['online', 'offline', 'error'].includes(status)),
            {
                status: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(!!isEmpty && !['true', 'false'].includes(isEmpty), {
            isEmpty: 'INVALID',
        });

        return this.organizationService.findOrganizationScreens(organizationId, queryValues);
    }

    @CheckPermission(ControllerAction.view_locations)
    @Get(':organizationId/locations')
    findLocations(@Param('organizationId') organizationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.findOrganizationLocations(organizationId);
    }

    @CheckPermission(ControllerAction.create_location)
    @Post(':organizationId/locations')
    createLocation(@Param('organizationId') organizationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.createLocation(organizationId);
    }

    @CheckPermission(ControllerAction.view_content)
    @Get(':organizationId/folders')
    findOrganizationFolders(@Param('organizationId') organizationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.findOrganizationFolders(organizationId);
    }

    @CheckPermission(ControllerAction.view_content)
    @Get(':organizationId/content')
    findContent(@Param('organizationId') organizationId: string, @Query('withPlaylists') withPlaylists: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.findOrganizationContent(organizationId, ['1', 'true'].includes(withPlaylists));
    }

    @CheckPermission(ControllerAction.update_content)
    @Post(':organizationId/folders')
    createFolder(@Param('organizationId') organizationId: string, @Body() createFolderDto: CreateFolderDto) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.createFolder(organizationId, createFolderDto);
    }

    @CheckPermission(ControllerAction.update_playlist)
    @Put(':organizationId/playlists/priority')
    updatePlaylistPriority(
        @Param('organizationId') organizationId: string,
        @Body() updatePlaylistsPriorityDto: UpdatePlaylistsPriorityDto,
    ) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.updatePlaylistPriority(organizationId, updatePlaylistsPriorityDto);
    }

    @CheckPermission(ControllerAction.view_users)
    @Get(':organizationId/members')
    getOrganizationMembers(@Param('organizationId') organizationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.findOrganizationMembers(organizationId);
    }

    @Accounting()
    @CheckPermission(ControllerAction.view_subscriptions)
    @Get(':organizationId/payments')
    async getOrganizationPayments(@Param('organizationId') organizationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.findPayments(organizationId);
    }

    @Accounting()
    @CheckPermission(ControllerAction.create_subscription)
    @Post(':organizationId/payments')
    async createPayment(@Param('organizationId') organizationId: string, @Body() createPaymentDto: CreatePaymentDto) {
        const { inn, company, invoiceNumber, status, screens, pricePerScreen, days, startDate, notes } =
            createPaymentDto;

        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!inn, { inn: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!company, { company: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!invoiceNumber, { invoiceNumber: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(
            !(['pending', 'paid', 'cancelled'] as PaymentStatusType[]).includes(status),
            { status: 'INVALID' },
        );

        const isFloatNumber = (num: number) => num % 1 !== 0;

        this.validationService.throwBadRequestExceptionIf(isNaN(screens) || screens < 0 || isFloatNumber(screens), {
            screens: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(
            isNaN(pricePerScreen) || pricePerScreen < 0 || isFloatNumber(pricePerScreen),
            {
                pricePerScreen: 'INVALID',
            },
        );
        this.validationService.throwBadRequestExceptionIf(isNaN(days) || days < 0 || isFloatNumber(days), {
            days: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(!isValid(new Date(startDate)), { startDate: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!notes, { notes: 'IS_REQUIRED' });

        return this.organizationService.createPayment(organizationId, createPaymentDto);
    }

    @CheckPermission(ControllerAction.delete_organization)
    @Delete(':organizationId')
    remove(@Param('organizationId') organizationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.remove(organizationId);
    }

    @CheckPermission(ControllerAction.upload_content)
    @Post(':organizationId/content/media')
    @UseInterceptors(FileInterceptor('file', { dest: './upload/temp' }))
    async uploadFile(
        @Param('organizationId') organizationId: string,
        @UploadedFile() file: Express.Multer.File,
        @Body('folderId') folderId: string,
    ) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!file, { file: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!!folderId && !validate(folderId), { folderId: 'INVALID' });

        // 4гб максимум
        if (file.size > 4 * 1024 * 1024 * 1024) {
            await deleteFile(file.path);

            throw this.validationService.badRequestException({ file: 'IS_TOO_LARGE' });
        }

        const { fileTypeFromFile } = await getFileTypeModule();
        const fileType = await fileTypeFromFile(file.path);
        const extensionFromMime = fileType ? `.${fileType.ext}` : '';

        if (!isWidget(file.originalname)) {
            if (!extensionFromMime || !SUPPORTED_CONTENT_FORMATS.includes(extensionFromMime)) {
                await deleteFile(file.path);

                throw this.validationService.badRequestException({ file: 'INVALID_FORMAT' });
            }
        }

        const extensionFromName = path.parse(file.originalname).ext;

        if (!extensionFromName || !SUPPORTED_CONTENT_FORMATS.includes(extensionFromName)) {
            file = {
                ...file,
                originalname: extensionFromName
                    ? file.originalname.replace(extensionFromName, extensionFromMime)
                    : `${file.originalname}${extensionFromMime}`,
            };
        }

        const { total, used } = await this.organizationService.getAllContentSize(organizationId);

        if (file.size > total - used) {
            await deleteFile(file.path);

            throw this.validationService.badRequestException({ storage: 'INSUFFICIENT_STORAGE' });
        }

        return this.organizationService.addUploadedFile(organizationId, file, folderId);
    }

    @CheckPermission(ControllerAction.create_webpage)
    @Post(':organizationId/content/web')
    async createWebContent(
        @Param('organizationId') organizationId: string,
        @Body() createWebContentDto: CreateWebContentDto,
    ) {
        const { name, link } = createWebContentDto;

        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!name, { name: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!link, { link: 'IS_REQUIRED' });

        return this.organizationService.addWebContent(organizationId, createWebContentDto);
    }

    @CheckPermission(ControllerAction.create_playlist)
    @Post(':organizationId/templates')
    async createTemplate(
        @Param('organizationId') organizationId: string,
        @Body() createTemplateDto: CreateTemplateDto,
    ) {
        const { name, sections } = createTemplateDto;

        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!name, { name: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!sections, { sections: 'IS_REQUIRED' });

        const isSectionsInvalid =
            !Array.isArray(sections) ||
            sections.length === 0 ||
            sections.some((section) => (section.content ? section.content.length === 0 : false)) ||
            sections.some(
                ({ sectionName, content, ...section }) =>
                    (sectionName !== undefined && !sectionName) ||
                    !Object.hasOwn(section, 'left') ||
                    !Object.hasOwn(section, 'top') ||
                    !Object.hasOwn(section, 'width') ||
                    !Object.hasOwn(section, 'height') ||
                    (content !== undefined && !Array.isArray(content)),
            );

        this.validationService.throwBadRequestExceptionIf(isSectionsInvalid, { sections: 'INVALID' });

        return this.organizationService.addTemplate(organizationId, createTemplateDto);
    }

    @CheckPermission(ControllerAction.view_playlists)
    @Get(':organizationId/templates')
    findTemplates(@Param('organizationId') organizationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.findOrganizationTemplates(organizationId);
    }

    @Get(':organizationId/tickets')
    findOrganizationTickets(@Param('organizationId') organizationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(organizationId), { organizationId: 'INVALID' });

        return this.organizationService.findTickets(organizationId);
    }
}
