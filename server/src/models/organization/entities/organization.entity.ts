import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, OneToMany, ManyToMany, JoinTable } from 'typeorm';

import { User } from '../../user/entities/user.entity';
import { Payment } from '../../payment/entities/payment.entity';
import {
    OrganizationProductEnum,
    OrganizationStatusType,
    OrganizationSubscriptionType,
    OrganizationType,
} from '../../../../../common/types';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class Organization implements OrganizationType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ nullable: true })
    name: string;

    @CreateDateColumn()
    createdAt: Date;

    @Column({ type: String, default: 'new' })
    status: OrganizationStatusType;

    @Column({ type: 'json', nullable: true })
    subscription: OrganizationSubscriptionType | null;

    @OneToMany(() => Payment, (payment) => payment.organization, { cascade: true })
    payments: Payment[];

    @Column({ default: 0, unsigned: true })
    maxScreens: number;

    @Column({ default: 0, unsigned: true })
    freeScreens: number;

    @Column({ default: 0, unsigned: true })
    signageStorageSize: number;

    @Column({ type: 'enum', enum: OrganizationProductEnum, default: OrganizationProductEnum.PHYGITAL })
    product: OrganizationProductEnum;

    @ManyToMany(() => User, (user) => user.organizations)
    @JoinTable()
    users: User[];
}
