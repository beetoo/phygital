export type GetOrganizationTimeQueryDto = { year?: string; month?: string; date?: string };
