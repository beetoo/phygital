import path from 'node:path';
import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, In, Repository } from 'typeorm';
import { differenceInSeconds } from 'date-fns';
import { v4 as uuidv4 } from 'uuid';
import getVideoDurationInSeconds from 'get-video-duration';
import * as uniqid from 'uniqid';

import { Organization } from './entities/organization.entity';
import { Content } from '../content/entities/content.entity';
import { Screen } from '../screen/entities/screen.entity';
import { Playlist } from '../playlist/entities/playlist.entity';
import { ScreenRequestLogger } from '../admin/screen-request-logger/entities/screen-request-logger.entity';
import { Folder } from '../folder/entities/folder.entity';
import { Location } from '../location/entities/location.entity';
import { deleteFile } from '../../helpers/deleteFile';
import { getOwnerUser } from './helpers';
import {
    getDefaultSchedule,
    getUniqueNameUsingPostfix,
    getVideoDimensions,
    mapPlaylists,
    mapScreens,
    mapScreensWithLogs,
} from '../../helpers';
import { User } from '../user/entities/user.entity';
import { UserOrganizationProperty } from '../user/entities/user-organization-property.entity';
import { UpdatePlaylistsPriorityDto } from '../playlist/dto/update-playlists-priority.dto';
import { UpdateOrganizationDto } from './dto/update-organization.dto';
import { CreateWebContentDto } from '../content/dto/create-web-content.dto';
import { decodeOriginFilename, formatWebContentLink, getImageResolution, saveFileToStorage } from '../content/helpers';
import { slugifyBeforeUpload } from '../../../../common/helpers';
import { createThumbnailImages } from '../../helpers/createThumbnailImages';
import isVideo from '../../../../common/helpers/isVideo';
import { Scenario } from '../scenario/entities/scenario.entity';
import { CreateScenarioDto } from '../scenario/dto/create-scenario.dto';
import { CreateOrganizationByUserDto } from './dto/create-organization-by-user.dto';
import { CreateFolderDto } from '../folder/dto/create-folder.dto';
import { UPLOAD_PATH_CONTENT } from '../../constants';
import { CONTENT_TYPE, ContentType, UserOrganizationRoleEnum } from '../../../../common/types';
import { CreatePlaylistDto } from './dto/create-playlist.dto';
import { defaultRegions } from '../playlist/helpers';
import { OrganizationInvitation } from '../organization-invitation/entities/organization.invitation';
import { CreateInviteDto } from './dto/create-invite.dto';
import { UpdateUserAccessDto } from './dto/update-user-access.dto';
import { getMayBeAddedPermissions, getPermissions, getPermissionsWithGlobalRoles } from '../../helpers/getPermissions';
import { ValidationService } from '../../services/validation/validation.service';
import { Template } from '../template/entities/template.entity';
import { CreateTemplateDto } from '../template/dto/create-template.dto';
import { SupportTicket } from '../support-ticket/entities/support-ticket.entity';
import { Payment } from '../payment/entities/payment.entity';
import { CreatePaymentDto } from '../payment/dto/create-payment.dto';
import isWidget from '../../../../common/helpers/isWidget';

export type FindOrganizationScreensQuery = {
    locationId?: string;
    isAdmin?: string;
    status?: string;
    isEmpty?: string;
};

@Injectable()
export class OrganizationService {
    constructor(
        @InjectRepository(Organization)
        private organizationRepository: Repository<Organization>,
        @InjectRepository(OrganizationInvitation)
        private organizationInvitationRepository: Repository<OrganizationInvitation>,
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(UserOrganizationProperty)
        private userOrganizationPropertyRepository: Repository<UserOrganizationProperty>,
        @InjectRepository(Content)
        private contentRepository: Repository<Content>,
        @InjectRepository(Folder)
        private folderRepository: Repository<Folder>,
        @InjectRepository(Screen)
        private screenRepository: Repository<Screen>,
        @InjectRepository(Template)
        private templateRepository: Repository<Template>,
        @InjectRepository(Location)
        private locationRepository: Repository<Location>,
        @InjectRepository(Playlist)
        private playlistRepository: Repository<Playlist>,
        @InjectRepository(Scenario)
        private scenarioRepository: Repository<Scenario>,
        @InjectRepository(ScreenRequestLogger)
        private screenLoggerRepository: Repository<ScreenRequestLogger>,
        @InjectRepository(SupportTicket)
        private ticketRepository: Repository<SupportTicket>,
        @InjectRepository(Payment)
        private paymentRepository: Repository<Payment>,
        private eventEmitter: EventEmitter2,
        private readonly validationService: ValidationService,
    ) {
        // if (isCronWorker()) {
        //     this.addTestScreens();
        // }
    }

    async createOrganizationByUser({ name, product, userId }: CreateOrganizationByUserDto) {
        const user = await this.userRepository.findOneBy({ id: userId });

        if (!user) {
            throw this.validationService.badRequestException({ user: 'NOT_FOUND' });
        }

        // создаем организацию
        let organization = new Organization();
        organization.name = name;
        organization.product = product;
        organization.users = [user];
        organization = await this.organizationRepository.save(organization);

        // добавляем userProperty
        const userProperty = new UserOrganizationProperty();
        userProperty.organizationId = organization.id;
        userProperty.role = UserOrganizationRoleEnum.owner;
        userProperty.user = user;
        await this.userOrganizationPropertyRepository.save(userProperty);

        return this.organizationRepository.findOne({
            where: { id: organization.id },
            relations: { users: true },
        });
    }

    async createPlaylist(organizationId: string, { name, type }: CreatePlaylistDto) {
        const priorities = (
            await this.playlistRepository.find({
                where: { organizationId },
                select: { priority: true },
            })
        ).map(({ priority }) => priority);

        const maxPriority = priorities.length > 0 ? Math.max(...priorities) + 1 : 1;

        const playlist = await this.playlistRepository.save({
            organizationId,
            type,
            intervalDuration: type === 'interval' ? 15 : null,
            title: name,
            draft: true,
            priority: maxPriority,
            schedule: getDefaultSchedule(),
            resolution: { width: 1920, height: 1080 },
            regions: defaultRegions({ width: 1920, height: 1080 }),
        });

        return this.playlistRepository.findOneBy({ id: playlist.id });
    }

    async createInvite(organizationId: string, createInviteDto: CreateInviteDto) {
        const { email, inviterId, role, permissions } = createInviteDto;

        const user = await this.userRepository.findOneBy({ email });

        if (!user) {
            throw this.validationService.badRequestException({ user: 'NOT_FOUND' });
        }

        const isAlreadyMember = user.properties.some((property) => property.organizationId === organizationId);

        this.validationService.throwBadRequestExceptionIf(isAlreadyMember, {
            email: 'USER_IS_ALREADY_ORGANIZATION_MEMBER',
        });

        const userInvitation = await this.organizationInvitationRepository.findOneBy({
            userId: user.id,
            organizationId,
        });

        this.validationService.throwBadRequestExceptionIf(!!userInvitation, { email: 'USER_HAS_PENDING_INVITATION' });

        const invite = new OrganizationInvitation();

        invite.organizationId = organizationId;
        invite.userId = user.id;
        invite.inviterId = inviterId;
        invite.role = role;
        invite.permissions = getMayBeAddedPermissions(role, permissions);

        return await this.organizationInvitationRepository.save(invite);
    }

    async revokeOrganizationAccess(organizationId: string, userId: string) {
        const user = await this.userRepository.findOne({
            where: { id: userId },
            relations: { organizations: true },
        });

        if (!user) {
            throw this.validationService.badRequestException({ user: 'NOT_FOUND' });
        }

        const isOwner = user.properties.some(
            (property) => property.organizationId === organizationId && property.role === 'owner',
        );

        this.validationService.throwBadRequestExceptionIf(isOwner, { email: 'USER_IS_OWNER' });

        const hasOrganization = user.organizations.some(({ id }) => id === organizationId);

        if (hasOrganization) {
            user.organizations = user.organizations.filter(({ id }) => id !== organizationId);
            await this.userRepository.save(user);
        }

        const hasProperty = user.properties.some((property) => property.organizationId === organizationId);

        if (hasProperty) {
            await this.userOrganizationPropertyRepository.delete({ user: { id: userId }, organizationId });
        }

        return { userId: user.id };
    }

    async setOrganizationOwner(organizationId: string, userId: string) {
        const [user, owner] = await Promise.all([
            this.userRepository.findOneBy({ id: userId }),
            this.userRepository.findOneBy({ properties: { organizationId, role: UserOrganizationRoleEnum.owner } }),
        ]);

        if (!user) {
            throw this.validationService.badRequestException({ user: 'NOT_FOUND' });
        }

        const isOrganizationMember = user.properties.some((property) => property.organizationId === organizationId);

        this.validationService.throwBadRequestExceptionIf(!isOrganizationMember, {
            email: 'USER_IS_NOT_ORGANIZATION_MEMBER',
        });

        if (!owner) {
            throw this.validationService.badRequestException({ email: 'USER_IS_NOT_OWNER' });
        }

        const userProperty = user.properties.find((property) => property.organizationId === organizationId);

        if (userProperty && userProperty.role !== UserOrganizationRoleEnum.owner) {
            await this.userOrganizationPropertyRepository.update(userProperty.id, {
                role: UserOrganizationRoleEnum.owner,
            });

            const ownerProperty = owner.properties.find((property) => property.organizationId === organizationId);

            if (ownerProperty) {
                await this.userOrganizationPropertyRepository.update(ownerProperty.id, {
                    role: UserOrganizationRoleEnum.admin,
                });
            }
        }

        return { userId: user.id };
    }

    async updateUserAccess(organizationId: string, userAccessDto: UpdateUserAccessDto) {
        const { userId, role, permissions } = userAccessDto;

        const user = await this.userRepository.findOneBy({ id: userId });

        if (!user) {
            throw this.validationService.badRequestException({ user: 'NOT_FOUND' });
        }

        const userProperty = user.properties.find((property) => property.organizationId === organizationId);

        if (!userProperty) {
            throw this.validationService.badRequestException({ email: 'USER_IS_NOT_ORGANIZATION_MEMBER' });
        }

        if (userProperty.role === UserOrganizationRoleEnum.member) {
            await this.userOrganizationPropertyRepository.update(userProperty.id, {
                role,
                permissions: getMayBeAddedPermissions(role, permissions),
            });
        }

        return { userId };
    }

    async findPayments(organizationId: string) {
        return (await this.paymentRepository.findBy({ organizationId })).map(({ total, ...payment }) => ({
            ...payment,
            total,
        }));
    }

    async findOrganizationMembers(organizationId: string) {
        return (
            await this.userRepository.find({
                where: { properties: { organizationId } },
                select: {
                    id: true,
                    name: true,
                    surname: true,
                    email: true,
                    properties: true,
                },
            })
        ).map(({ properties, ...user }) => {
            const property = properties.find((property) => property.organizationId === organizationId);

            const userRole = property?.role ?? UserOrganizationRoleEnum.member;

            return {
                ...user,
                role: userRole,
                permissions: getPermissions(userRole, property?.permissions),
            };
        });
    }

    async findOrganization(organizationId: string, user: User) {
        const organization = await this.organizationRepository.findOneBy({ id: organizationId });

        if (!organization) {
            throw this.validationService.notFoundException({ organization: 'NOT_FOUND' });
        }

        const userProperty = user.properties.find((p) => p.organizationId === organizationId);

        const globalRolePermissions = getPermissionsWithGlobalRoles(
            user.role,
            userProperty?.role,
            userProperty?.permissions,
        );

        const currentUser = {
            role: user.role === 'admin' ? 'su' : user.role,
            permissions: globalRolePermissions,
        };

        return {
            id: organization.id,
            name: organization.name,
            createdAt: organization.createdAt,
            status: organization.status,
            product: organization.product,
            freeScreens: organization.freeScreens,
            subscription: organization.subscription,
            currentUser,
        };
    }

    async leaveOrganization(organizationId: string, { id: userId }: User) {
        const organization = await this.organizationRepository.findOne({
            where: { id: organizationId },
            relations: { users: true },
        });

        if (!organization) {
            throw this.validationService.notFoundException({ organization: 'NOT_FOUND' });
        }

        await this.userOrganizationPropertyRepository.delete({ organizationId, user: { id: userId } });

        organization.users = organization.users.filter((user) => user.id !== userId);
        await this.organizationRepository.save(organization);

        return {
            organizationId,
        };
    }

    async updateOrganization(
        organizationId: string,
        { name, status, product, freeScreens, subscription, userRole }: UpdateOrganizationDto,
    ) {
        let organization = await this.organizationRepository.findOneBy({ id: organizationId });

        if (!organization) {
            throw this.validationService.notFoundException({ organization: 'NOT_FOUND' });
        }

        organization.name = name || organization.name;
        organization.status = status || organization.status;
        organization.product = product || organization.product;
        organization.freeScreens = freeScreens !== undefined ? freeScreens : organization.freeScreens;

        if (subscription === null) {
            organization.subscription = null;
        } else if (subscription !== undefined) {
            const screens = subscription.screens ?? 0;
            const storage = subscription.storage ?? 0;

            organization.subscription = {
                ...subscription,
                validUntil: subscription.validUntil + '-01',
                screens,
                storage,
            };
        }

        organization = await this.organizationRepository.save(organization);

        return {
            ...organization,
            subscription: {
                ...organization.subscription,
                notes: userRole === 'admin' ? organization.subscription?.notes : undefined,
            },
        };
    }

    async updatePlaylistPriority(organizationId: string, { priorityInfo }: UpdatePlaylistsPriorityDto) {
        const playlists = (await this.playlistRepository.findBy({ organizationId })).map((playlist) => ({
            ...playlist,
            priority: priorityInfo.find((info) => info.id === playlist.id)?.priority ?? 0,
        }));

        await this.playlistRepository.save(playlists);

        this.eventEmitter.emit('screen.json.update', { playlistIds: playlists.map(({ id }) => id) });

        return { message: 'OK' };
    }

    async remove(organizationId: string) {
        // находим список экранов организации
        const screensCount = await this.screenRepository.countBy({ organizationId });

        this.validationService.throwForbiddenExceptionIf(screensCount > 0, { reason: 'ORGANIZATION_HAS_SCREENS' });

        // удаляем все плейлисты организации
        await this.playlistRepository.delete({ organizationId });

        const [content, tickets] = await Promise.all([
            this.contentRepository.findBy({ organizationId }),
            this.ticketRepository.findBy({ organizationId }),
            // удаляем все сценарии
            this.scenarioRepository.delete({ organizationId }),
            // удаление точек
            this.locationRepository.delete({ organizationId }),
        ]);

        // находим весь список контента этой организации и удаляем его из хранилища
        const deleteFilePromises = content.map(({ id }) => deleteFile(`${UPLOAD_PATH_CONTENT}/${id}`));

        const withoutOrganizationTickets = tickets.map((ticket) => ({ ...ticket, organizationId: null }));

        await Promise.all([
            this.ticketRepository.save(withoutOrganizationTickets),
            ...deleteFilePromises,
            // удаление контента
            this.contentRepository.delete({ organizationId }).then(() => {
                // удаление папки
                this.folderRepository.delete({ organizationId });
            }),
            // удаляем саму организацию
            this.userOrganizationPropertyRepository.delete({ organizationId }),
        ]);

        // удаляем саму организацию
        await this.organizationRepository.delete(organizationId);

        return { message: 'OK' };
    }

    async createFolder(organizationId: string, { name }: CreateFolderDto) {
        const organization = await this.organizationRepository.findOne({
            where: { id: organizationId },
            relations: { users: true },
        });

        if (!organization) {
            throw this.validationService.notFoundException({ organization: 'NOT_FOUND' });
        }

        const user = getOwnerUser(organization);

        const { id } = await this.folderRepository.save({
            organizationId,
            name: name?.trim() || (user?.language === 'ru' ? 'Новая папка' : 'New Location'),
        });

        return { id };
    }

    async findOrganizationPlaylists(organizationId: string) {
        const options: FindManyOptions<Playlist> = {
            where: { organizationId },
            select: {
                id: true,
                title: true,
                type: true,
                intervalDuration: true,
                createdAt: true,
                updatedAt: true,
                resolution: { width: true, height: true },
                draft: true,
                priority: true,
                sync: true,
                log: true,
                schedule: {
                    byDay: true,
                    endDay: true,
                    endTime: true,
                    startDay: true,
                    startTime: true,
                    broadcastAlways: true,
                },
            },
            relations: { screens: true, playlistItems: { content: true } },
        };

        const playlists = await this.playlistRepository.find(options);

        return mapPlaylists(playlists);
    }

    async findOrganizationInvitations(organizationId: string) {
        const [invitations, users] = await Promise.all([
            this.organizationInvitationRepository.findBy({ organizationId }),
            this.userRepository.find(),
        ]);

        return invitations.map(({ id, inviterId, userId, role, permissions, createdAt }) => {
            const inviter = users.find((user) => user.id === inviterId);
            const user = users.find((user) => user.id === userId);

            return {
                id,
                createdAt,
                inviter: inviter
                    ? { id: inviter.id, name: inviter.name, surname: inviter.surname, email: inviter.email }
                    : null,
                user: user ? { id: user.id, name: user.name, surname: user.surname, email: user.email } : null,
                role,
                permissions,
            };
        });
    }

    async findOrganizationScenarios(organizationId: string) {
        return await this.scenarioRepository.find({
            where: { organizationId },
            relations: { playlistItems: true },
        });
    }

    async createOrganizationScenario(
        organizationId: string,
        { name, description, rules: rulesDto }: CreateScenarioDto,
    ) {
        const scenario = new Scenario();
        scenario.organizationId = organizationId;
        scenario.name = name;
        scenario.description = description;
        scenario.rules = rulesDto;

        const { id, rules, ...rest } = await this.scenarioRepository.save(scenario);

        return { id, ...rest, rules: rules.map(({ id, ...rule }) => ({ id, ...rule })) };
    }

    async findOrganizationScreens(
        organizationId: string,
        { locationId, isAdmin, status, isEmpty }: FindOrganizationScreensQuery,
    ) {
        const options: FindManyOptions<Omit<Screen, 'log' | 'hasHistory'>> = {
            where: {
                organizationId,
                location: locationId && locationId !== 'all' ? { id: locationId } : undefined,
            },
            select: {
                id: true,
                createdAt: true,
                aliveAt: true,
                name: true,
                resolution: { width: true, height: true },
                orientation: true,
                identification: true,
                isEmpty: true,
                smil: true,
                location: {
                    id: true,
                    name: true,
                    city: true,
                    address: true,
                    geo_lat: true,
                    geo_lon: true,
                    tzLabel: true,
                    tzOffset: true,
                },
            },
            relations: {
                location: true,
            },
        };

        const screensCount = await this.screenRepository.count({ ...options, relations: undefined });

        if (screensCount === 0) {
            return [];
        }

        let screens = await this.screenRepository.find(options);

        if (isEmpty) {
            screens = screens.filter((screen) =>
                isEmpty === 'true' ? screen.isEmpty : isEmpty === 'false' ? !screen.isEmpty : true,
            );
        }

        if (screens.length === 0) {
            return [];
        }

        const identifications = screens.map(({ identification }) => identification);

        const screenLogs = await this.screenLoggerRepository.findBy({ identification: In(identifications) });

        if (isAdmin === 'true') {
            screens = mapScreensWithLogs(screens, screenLogs) as Screen[];
        }

        return mapScreens(screens, screenLogs).filter((screen) =>
            status ? status.split(',').includes(screen.status) : true,
        );
    }

    async findOrganizationLocations(organizationId: string) {
        const locations = await this.locationRepository.find({
            where: { organizationId },
            select: {
                id: true,
                name: true,
                address: true,
                geo_lat: true,
                geo_lon: true,
                tzLabel: true,
                tzOffset: true,
                createdAt: true,
            },
            relations: { screens: true },
        });

        return locations.map(({ city, address, screens, ...location }) => {
            const onlineScreenCount = screens.filter(
                (screen) => differenceInSeconds(new Date(), new Date(screen.aliveAt)) <= 180,
            ).length;

            const offlineScreenCount = screens.filter(
                (screen) => differenceInSeconds(new Date(), new Date(screen.aliveAt)) > 180,
            ).length;

            return {
                ...location,
                address: city ? `${city}, ${address}` : address,
                screenCount: {
                    online: onlineScreenCount,
                    offline: offlineScreenCount,
                    error: 0,
                },
            };
        });
    }

    async createLocation(organizationId: string) {
        // находим все имена локаций этой организации
        const locations = await this.locationRepository.find({ where: { organizationId } });
        const locationNames = locations.map(({ name }) => name);

        // добавляем postfix к имени, если нужно
        const name = getUniqueNameUsingPostfix(locationNames, 'Новая локация').slice(0, 125);

        // и сохраняем локацию
        const { id } = await this.locationRepository.save({
            organizationId,
            name,
            tzLabel: 'Europe/Moscow (GMT+03:00)',
            tzOffset: 180,
        });

        return { id, name };
    }

    async findOrganizationFolders(organizationId: string) {
        return (
            await this.folderRepository.find({
                where: { organizationId },
                relations: { contents: true },
            })
        ).map((folder) => ({
            ...folder,
            contents: folder.contents.map(({ src, ...content }) => {
                const tmp: Partial<ContentType> = { ...content };
                delete tmp.filename;
                delete tmp.webContentLink;

                return { ...tmp, src };
            }),
        }));
    }

    private async fillOriginFilename() {
        const content = await this.contentRepository.find({ where: { originFilename: '' } });

        if (content.length > 0) {
            const newContent = content.map(({ filename, originFilename, ...rest }) => ({
                filename,
                originFilename: originFilename === '' ? filename : originFilename,
                ...rest,
            }));

            await this.contentRepository.save(newContent);
        }
    }

    async findOrganizationContent(organizationId: string, withPlaylists: boolean): Promise<Partial<ContentType>[]> {
        await this.fillOriginFilename();

        const contentItems = await this.contentRepository.find({
            where: { organizationId },
            relations: { folder: true, playlistItems: withPlaylists ? { playlist: true } : false },
        });

        return contentItems.map(({ src, ...rest }) => {
            const tmp: Partial<ContentType> = { ...rest };
            delete tmp.filename;
            delete tmp.webContentLink;
            delete tmp.playlistItems;

            const playlists = withPlaylists
                ? rest.playlistItems
                      .filter((item) => item.playlist)
                      .map((item) => ({ ...item.playlist, uploads: undefined }))
                : undefined;

            return { ...tmp, src, playlists };
        });
    }

    async getAllContentSize(organizationId: string) {
        const [content, organization] = await Promise.all([
            this.contentRepository.findBy({ organizationId }),
            this.organizationRepository.findOneBy({ id: organizationId }),
        ]);

        const contentSize = content.reduce((acc, curr) => acc + curr.size, 0);

        const { storage } = organization?.subscription ?? { storage: 0 };

        const isOrganizationValid = !(organization?.status === 'expired' || organization?.status === 'cancelled');

        const total = isOrganizationValid ? storage * 1024 * 1024 : 0;

        return {
            used: contentSize,
            total,
        };
    }

    async addUploadedFile(organizationId: string, file: Express.Multer.File, folderId?: string) {
        const id = uuidv4();

        const content = new Content();

        const extensionFromName = path.parse(file.originalname).ext;

        content.id = id;
        content.organizationId = organizationId;
        content.originFilename = decodeOriginFilename(file.originalname);
        content.filename = slugifyBeforeUpload(content.originFilename);
        content.filename = content.filename.replace(extensionFromName, uniqid.process('_') + extensionFromName);
        content.type = isWidget(file.originalname)
            ? CONTENT_TYPE.WIDGET
            : isVideo(file.originalname)
              ? CONTENT_TYPE.VIDEO
              : CONTENT_TYPE.IMAGE;
        content.size = file.size;

        if (folderId) {
            const folder = await this.folderRepository.findOneBy({ id: folderId, organizationId });

            if (!folder) {
                throw this.validationService.badRequestException({ folder: 'FOLDER_NOT_FOUND' });
            }

            content.folder = folder;
        }

        if (content.type === CONTENT_TYPE.IMAGE) {
            const { width, height } = await getImageResolution(file.path);

            content.dimensions = { width, height };
            content.duration = 0;
        } else if (content.type === CONTENT_TYPE.VIDEO) {
            const { width, height, bitrate } = await getVideoDimensions(file.path);

            content.dimensions = { width, height };
            content.bitrate = bitrate;
            content.duration = await getVideoDurationInSeconds(file.path);
        }

        const filePath = saveFileToStorage(id, file, content.filename);
        await createThumbnailImages(filePath);

        const savedContent = await this.contentRepository.save(content);
        const tmp: Partial<ContentType> = { ...savedContent };
        delete tmp.filename;
        delete tmp.webContentLink;
        delete tmp.playlistItems;

        return { ...tmp, src: savedContent?.src };
    }

    async addWebContent(organizationId: string, { name, link }: CreateWebContentDto) {
        const { id } = await this.contentRepository.save({
            id: uuidv4(),
            organizationId,
            originFilename: name.slice(0, 125),
            filename: name.slice(0, 125),
            webContentLink: formatWebContentLink(link),
            type: CONTENT_TYPE.WEB,
            size: 0,
        });

        return { id };
    }

    async addTemplate(organizationId: string, { name, sections }: CreateTemplateDto) {
        const { id } = await this.templateRepository.save({
            organizationId,
            name,
            sections,
        });

        return { templateId: id };
    }

    findOrganizationTemplates(organizationId: string) {
        return this.templateRepository.findBy({ organizationId });
    }

    async findTickets(organizationId: string) {
        const ticketsCount = await this.ticketRepository.countBy({ organizationId });

        if (ticketsCount === 0) {
            return [];
        }

        const [tickets, users] = await Promise.all([
            this.ticketRepository.find({ where: { organizationId }, relations: { messages: true } }),
            this.userRepository.find({
                select: { id: true, name: true, surname: true, email: true, phone: true },
            }),
        ]);

        return tickets.map(({ messages, userId, ...ticket }) => {
            const newMessageCount = messages.filter(
                (message) => message.senderRole === 'support' && message.readAt === null,
            ).length;

            const messageCount = messages.length;

            const user = users.find((user) => user.id === userId);

            return {
                ...ticket,
                newMessageCount,
                messageCount,
                user: user ? { ...user, properties: undefined } : undefined,
            };
        });
    }

    async createPayment(organizationId: string, createPaymentDto: CreatePaymentDto) {
        const organization = await this.organizationRepository.findOneBy({ id: organizationId });

        if (!organization) {
            throw this.validationService.notFoundException({ organization: 'NOT_FOUND' });
        }

        const { inn, company, invoiceNumber, status, screens, pricePerScreen, days, startDate, notes } =
            createPaymentDto;

        const { id } = await this.paymentRepository.save({
            organizationId,
            inn,
            company,
            invoiceNumber,
            status,
            screens,
            pricePerScreen,
            days,
            startDate,
            notes,
            organization,
        });

        return { paymentId: id };
    }
}
