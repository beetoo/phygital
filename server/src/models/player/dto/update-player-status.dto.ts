export class UpdatePlayerStatusDto {
    identification: string;
    versionName: string;
    totalBytes: number;
    availableBytes: number;
}
