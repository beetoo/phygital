import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Response } from 'express';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import hash from 'object-hash';

import { UpdatePlayerStatusDto } from './dto/update-player-status.dto';
import { ScreenRegService } from '../admin/screen-reg/screen-reg.service';
import { Screen } from '../screen/entities/screen.entity';
import { formatScreenForSmil, getCurrentTimePlaylist, withContentLinks } from '../../helpers';
import { HelperService } from '../../services/helper.service';
import { isDevProductionServer } from '../../constants';
import { ValidationService } from '../../services/validation/validation.service';

type ScreenCacheType = {
    screen: Screen;
    hash: string;
    modifiedAt: Date;
};

@Injectable()
export class PlayerService {
    constructor(
        @InjectRepository(Screen)
        private screenRepository: Repository<Screen>,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
        private screenRegService: ScreenRegService,
        private helperService: HelperService,
        private readonly validationService: ValidationService,
    ) {}

    async getScreenJsonCache({
        identification,
        dataHash,
        response,
    }: {
        identification: string;
        dataHash?: string;
        response: Response;
    }) {
        const screenCache = (await this.cacheManager.get(`player:data:${identification}`)) as ScreenCacheType;

        if (!screenCache || dataHash !== screenCache.hash) {
            return this.getScreenJson(identification, screenCache);
        } else if (dataHash === screenCache.hash) {
            response.status(HttpStatus.NOT_MODIFIED);
        }

        return screenCache;
    }

    async initPlayer(identification: string, response: Response) {
        const screen = await this.screenRepository.findOneBy({ identification });

        if (screen) {
            response.status(HttpStatus.CONFLICT);
        } else {
            const screenReg =
                (await this.screenRegService.getScreenRegVerification(identification)) ??
                (await this.screenRegService.addScreenRegVerification(identification));

            return { connectionCode: screenReg.verificationHash };
        }
    }

    async setPlayerStatus(updatePlayerStatusDto: UpdatePlayerStatusDto, response: Response) {
        const { identification, totalBytes, availableBytes } = updatePlayerStatusDto;

        const screen = await this.screenRepository.findOneBy({ identification });

        if (!screen) {
            response.status(HttpStatus.UNAUTHORIZED);
        } else {
            Promise.resolve().then(async () => {
                await this.screenRepository.update(screen.id, {
                    // versionName,
                    totalBytes,
                    availableBytes,
                    aliveAt: new Date(),
                });

                await this.helperService.updateScreenStatus(identification, screen);
            });

            response.status(HttpStatus.OK);
        }
    }

    private async getScreenJson(identification: string, screenCache: ScreenCacheType | undefined) {
        let screen = await this.screenRepository.findOne({
            where: { identification },
            relations: {
                location: true,
                playlists: { playlistItems: { content: true, scenario: true } },
            },
        });

        if (!screen) {
            if (isDevProductionServer) {
                throw this.validationService.notFoundException({ screen: 'NOT_FOUND' });
            } else {
                throw this.validationService.unauthorizedException({ screen: 'NOT_FOUND' });
            }
        }

        // обновление значения isEmpty
        await this.screenRepository.update(screen.id, { isEmpty: !getCurrentTimePlaylist(screen) });

        screen = formatScreenForSmil(withContentLinks(screen)) as unknown as Screen;

        if (hash(screen) !== screenCache?.hash) {
            await this.cacheManager.set(
                `player:data:${screen.identification}`,
                {
                    hash: hash(screen),
                    modifiedAt: new Date(),
                    screen,
                },
                0,
            );
        }

        return this.cacheManager.get(`player:data:${screen.identification}`);
    }
}
