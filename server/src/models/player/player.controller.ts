import { Body, Controller, Post, Res } from '@nestjs/common';
import { Response } from 'express';

import { PlayerService } from './player.service';
import { UpdatePlayerStatusDto } from './dto/update-player-status.dto';
import { Public } from '../../decorators/public.decorator';
import { ValidationService } from '../../services/validation/validation.service';

@Public()
@Controller('player')
export class PlayerController {
    constructor(
        private readonly playerService: PlayerService,
        private readonly validationService: ValidationService,
    ) {}

    // new api
    @Post('json')
    async getScreenJsonCache(
        @Res({ passthrough: true }) response: Response,
        @Body('identification') identification: string,
        @Body('dataHash') dataHash?: string,
    ) {
        this.validationService.throwBadRequestExceptionIf(!identification, { identification: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!['string'].includes(typeof identification), {
            identification: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(
            dataHash !== undefined && !['string'].includes(typeof dataHash),
            {
                dataHash: 'INVALID',
            },
        );

        return this.playerService.getScreenJsonCache({ identification, dataHash, response });
    }

    @Post('init')
    async initPlayer(@Res({ passthrough: true }) response: Response, @Body('identification') identification: string) {
        this.validationService.throwBadRequestExceptionIf(!identification, { identification: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!['string'].includes(typeof identification), {
            identification: 'INVALID',
        });

        return this.playerService.initPlayer(identification, response);
    }

    @Post('status')
    async setPlayerStatus(
        @Res({ passthrough: true }) response: Response,
        @Body() updatePlayerStatusDto: UpdatePlayerStatusDto,
    ) {
        const { identification, totalBytes, availableBytes } = updatePlayerStatusDto;

        this.validationService.throwBadRequestExceptionIf(!identification, { identification: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!['string'].includes(typeof identification), {
            identification: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(isNaN(totalBytes), { totalBytes: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(isNaN(availableBytes), { availableBytes: 'INVALID' });

        return this.playerService.setPlayerStatus(updatePlayerStatusDto, response);
    }
}
