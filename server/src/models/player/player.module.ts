import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { PlayerService } from './player.service';
import { PlayerController } from './player.controller';
import { Screen } from '../screen/entities/screen.entity';
import { ScreenReg } from '../admin/screen-reg/entities/screen-reg.entity';
import { ScreenRegService } from '../admin/screen-reg/screen-reg.service';
import { HelperService } from '../../services/helper.service';
import { ScreenHistory } from '../screen/entities/screen-history.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Screen, ScreenHistory, ScreenReg])],
    controllers: [PlayerController],
    providers: [PlayerService, ScreenRegService, HelperService],
})
export class PlayerModule {}
