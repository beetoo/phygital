import { Column, OneToMany, PrimaryGeneratedColumn, Entity, CreateDateColumn } from 'typeorm';

import { LocationType } from '../../../../../common/types';
import { isSLEngServer } from '../../../constants';
import { Screen } from '../../screen/entities/screen.entity';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class Location implements LocationType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    organizationId: string;

    @Column()
    name: string;

    @Column({ default: '' })
    city: string;

    @Column({ default: '' })
    address: string;

    @Column({ default: '' })
    geo_lat: string;

    @Column({ default: '' })
    geo_lon: string;

    @Column({ default: isSLEngServer ? 'Europe/London (GMT+00:00)' : 'Europe/Moscow (GMT+03:00)' })
    tzLabel: string;

    @Column({ default: isSLEngServer ? 0 : 180, unsigned: true })
    tzOffset: number;

    @OneToMany(() => Screen, (screen) => screen.location)
    screens: Screen[];

    @CreateDateColumn()
    createdAt: Date;

    @Column({ type: 'json', nullable: true })
    urlParams: { [key: string]: string | number } | null;

    @Column({ type: 'json', nullable: true })
    variables: { [key: string]: any }[] | null;
}
