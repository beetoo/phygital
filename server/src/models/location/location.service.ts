import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { In, Repository } from 'typeorm';

import { getUniqueNameUsingPostfix, mapScreens } from '../../helpers';
import { UpdateLocationDto } from './dto/update-location.dto';
import { Location } from './entities/location.entity';
import { isObjectsEqual } from '../../helpers/isObjectsEqual';
import { Screen } from '../screen/entities/screen.entity';
import { getTzOffsetByTzLabel } from '../../../../common/helpers';
import { ScreenRequestLogger } from '../admin/screen-request-logger/entities/screen-request-logger.entity';
import { ValidationService } from '../../services/validation/validation.service';

@Injectable()
export class LocationService {
    constructor(
        @InjectRepository(Location)
        private locationRepository: Repository<Location>,
        @InjectRepository(Screen)
        private screenRepository: Repository<Screen>,
        @InjectRepository(ScreenRequestLogger)
        private screenRequestLoggerRepository: Repository<ScreenRequestLogger>,
        private eventEmitter: EventEmitter2,
        private readonly validationService: ValidationService,
    ) {}

    async findLocation(locationId: string) {
        const location = await this.locationRepository.findOne({
            where: { id: locationId },
            relations: { screens: true },
        });

        if (!location) {
            throw this.validationService.notFoundException({ location: 'NOT_FOUND' });
        }

        const identifications = location.screens.map(({ identification }) => identification);
        const screenLogs = await this.screenRequestLoggerRepository.findBy({ identification: In(identifications) });

        return {
            ...location,
            screens: mapScreens(location.screens, screenLogs),
        };
    }

    async update(locationId: string, { name, address, geo_lat, geo_lon, tzLabel, urlParams }: UpdateLocationDto) {
        const location = await this.locationRepository.findOneBy({ id: locationId });

        if (!location) {
            throw this.validationService.notFoundException({ location: 'NOT_FOUND' });
        }

        if (name) {
            // находим все имена локаций этой организации
            const locations = await this.locationRepository.find({
                where: { organizationId: location?.organizationId },
            });
            const locationNames = locations.map(({ name }) => name);

            name = getUniqueNameUsingPostfix(locationNames, name, location?.name).slice(0, 125);
        }

        location.name = name ? name : location.name;
        location.city = '';

        location.address = address ? address : location.address;
        location.geo_lat = geo_lat ? geo_lat : location.geo_lat;
        location.geo_lon = geo_lon ? geo_lon : location.geo_lon;
        location.tzLabel = tzLabel ? tzLabel : location.tzLabel;
        location.tzOffset = tzLabel ? getTzOffsetByTzLabel(tzLabel) : location.tzOffset;

        const prevUrlParams = location.urlParams;

        if (urlParams !== undefined) {
            location.urlParams = urlParams;
        }

        await this.locationRepository.save(location);

        if (!isObjectsEqual(prevUrlParams, location.urlParams)) {
            const screens = await this.screenRepository.findBy({ location: { id: locationId } });

            const screenIds = screens.map(({ id }) => id);

            this.eventEmitter.emit('screen.json.update', { screenIds });
        }

        return { id: locationId, name };
    }

    async remove(locationId: string) {
        const location = await this.locationRepository.findOne({
            where: { id: locationId },
            relations: { screens: true },
        });

        if (!location) {
            throw this.validationService.notFoundException({ location: 'NOT_FOUND' });
        }

        this.validationService.throwBadRequestExceptionIf(location.screens && location.screens.length > 0, {
            location: 'HAS_SCREENS',
        });

        await this.locationRepository.delete(locationId);

        return { locationId };
    }

    async isLocationHasScreen(locationId: string) {
        const location = await this.locationRepository.findOne({
            where: { id: locationId },
            relations: { screens: true },
        });

        if (!location) {
            throw this.validationService.notFoundException({ location: 'NOT_FOUND' });
        }

        return location.screens.length > 0;
    }
}
