import { Body, Controller, Delete, Get, Param, Patch } from '@nestjs/common';
import { validate } from 'uuid';

import { UpdateLocationDto } from './dto/update-location.dto';
import { LocationService } from './location.service';
import { isInvalidLatitude, isInvalidLongitude, isInvalidTimezone } from '../../../../common/helpers';
import { CheckPermission, ControllerAction } from '../../decorators/check-permission.decorator';
import { ValidationService } from '../../services/validation/validation.service';

@Controller('location')
export class LocationController {
    constructor(
        private readonly locationService: LocationService,
        private readonly validationService: ValidationService,
    ) {}

    @CheckPermission(ControllerAction.view_locations)
    @Get(':locationId')
    findOneLocation(@Param('locationId') locationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(locationId), { locationId: 'INVALID' });

        return this.locationService.findLocation(locationId);
    }

    @CheckPermission(ControllerAction.update_location)
    @Patch(':locationId')
    async update(@Param('locationId') locationId: string, @Body() updateLocationDto: UpdateLocationDto) {
        const { geo_lat, geo_lon, tzLabel, urlParams } = updateLocationDto;

        this.validationService.throwBadRequestExceptionIf(!validate(locationId), { locationId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(!!geo_lat && !geo_lon, { geo_lon: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(!!geo_lon && !geo_lat, { geo_lat: 'IS_REQUIRED' });
        this.validationService.throwBadRequestExceptionIf(
            urlParams !== undefined &&
                (typeof urlParams !== 'object' || Array.isArray(urlParams) || urlParams === null),
            {
                urlParams: 'INVALID',
            },
        );

        this.validationService.throwBadRequestExceptionIf(!!tzLabel && isInvalidTimezone(tzLabel), {
            timezone: 'INVALID',
        });
        this.validationService.throwBadRequestExceptionIf(
            !!geo_lat && !!geo_lon && (isInvalidLatitude(geo_lat) || isInvalidLongitude(geo_lon)),
            {
                latitude: geo_lat && isInvalidLatitude(geo_lat) ? 'INVALID' : undefined,
                longitude: geo_lon && isInvalidLongitude(geo_lon) ? 'INVALID' : undefined,
            },
        );

        return this.locationService.update(locationId, updateLocationDto);
    }

    @CheckPermission(ControllerAction.delete_location)
    @Delete(':locationId')
    async remove(@Param('locationId') locationId: string) {
        this.validationService.throwBadRequestExceptionIf(!validate(locationId), { locationId: 'INVALID' });

        if (await this.locationService.isLocationHasScreen(locationId)) {
            throw this.validationService.badRequestException({ location: 'LOCATION_HAS_SCREENS' });
        }

        return this.locationService.remove(locationId);
    }
}
