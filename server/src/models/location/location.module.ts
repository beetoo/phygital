import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { LocationService } from './location.service';
import { LocationController } from './location.controller';
import { Location } from './entities/location.entity';
import { Screen } from '../screen/entities/screen.entity';
import { ScreenRequestLogger } from '../admin/screen-request-logger/entities/screen-request-logger.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Location, Screen, ScreenRequestLogger])],
    controllers: [LocationController],
    providers: [LocationService],
})
export class LocationModule {}
