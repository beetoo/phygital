import { PartialType } from '@nestjs/mapped-types';
import { CreateLocationDto } from './create-location.dto';

export class UpdateLocationDto extends PartialType(CreateLocationDto) {
    readonly locationId: string;
    readonly name: string;
    readonly address?: string;
    readonly geo_lat?: string;
    readonly geo_lon?: string;
    readonly tzLabel: string;
    readonly urlParams?: { [key: string]: string | number } | null;
}
