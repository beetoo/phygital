import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

import { TemplateSectionType, TemplateType } from '../../../../../common/types';

@Entity({ orderBy: { createdAt: 'DESC' } })
export class Template implements TemplateType {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    organizationId: string;

    @Column()
    name: string;

    @Column({ type: 'json', nullable: true })
    sections: TemplateSectionType[];

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;
}
