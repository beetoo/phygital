import { Controller, Body, Patch, Param } from '@nestjs/common';
import { validate } from 'uuid';

import { TemplateService } from './template.service';
import { UpdateTemplateDto } from './dto/update-template.dto';
import { ValidationService } from '../../services/validation/validation.service';
import { CheckPermission, ControllerAction } from '../../decorators/check-permission.decorator';

@Controller('template')
export class TemplateController {
    constructor(
        private readonly templateService: TemplateService,
        private readonly validationService: ValidationService,
    ) {}

    @CheckPermission(ControllerAction.update_playlist)
    @Patch(':templateId')
    update(@Param('templateId') templateId: string, @Body() updateTemplateDto: UpdateTemplateDto) {
        const { name, sections } = updateTemplateDto;

        this.validationService.throwBadRequestExceptionIf(!validate(templateId), { templateId: 'INVALID' });
        this.validationService.throwBadRequestExceptionIf(name !== undefined && !name, { name: 'IS_REQUIRED' });

        const isSectionsInvalid =
            !Array.isArray(sections) ||
            sections.length === 0 ||
            sections.some((section) => (section.content ? section.content.length === 0 : false)) ||
            sections.some(
                ({ sectionName, content, ...section }) =>
                    (sectionName !== undefined && !sectionName) ||
                    !Object.hasOwn(section, 'left') ||
                    !Object.hasOwn(section, 'top') ||
                    !Object.hasOwn(section, 'width') ||
                    !Object.hasOwn(section, 'height') ||
                    (content !== undefined && !Array.isArray(content)),
            );

        this.validationService.throwBadRequestExceptionIf(sections !== undefined && isSectionsInvalid, {
            sections: 'INVALID',
        });

        return this.templateService.update(templateId, updateTemplateDto);
    }
}
