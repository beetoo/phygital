import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { isEqual } from 'lodash';

import { UpdateTemplateDto } from './dto/update-template.dto';
import { Template } from './entities/template.entity';
import { ValidationService } from '../../services/validation/validation.service';
import { Playlist } from '../playlist/entities/playlist.entity';

@Injectable()
export class TemplateService {
    constructor(
        @InjectRepository(Template)
        private templateRepository: Repository<Template>,
        @InjectRepository(Playlist)
        private playlistRepository: Repository<Playlist>,
        private readonly validationService: ValidationService,
        private eventEmitter: EventEmitter2,
    ) {}
    async update(templateId: string, { name, sections }: UpdateTemplateDto) {
        let template = await this.templateRepository.findOneBy({ id: templateId });

        if (!template) {
            throw this.validationService.notFoundException({ template: 'NOT_FOUND' });
        }

        if (name) {
            template.name = name || template.name;
        }

        const prevSections = template.sections;

        if (sections) {
            template.sections = sections;
        }

        template = await this.templateRepository.save(template);

        if (!isEqual(prevSections, template.sections)) {
            const playlistIds = (await this.playlistRepository.findBy({ templateId })).map(({ id }) => id);

            this.eventEmitter.emit('screen.json.update', { playlistIds });
        }

        return template;
    }
}
