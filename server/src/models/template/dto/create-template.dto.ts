import { TemplateSectionType } from '../../../../../common/types';

export class CreateTemplateDto {
    name: string;
    sections: TemplateSectionType[];
}
