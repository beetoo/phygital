import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { IsNull, Repository, In, Not } from 'typeorm';

import { User } from './models/user/entities/user.entity';
import { UserType } from '../../common/types';
import { ValidationService } from './services/validation/validation.service';
import { SupportTicketMessage } from './models/support-ticket/entities/support-ticket-message.entity';

@Injectable()
export class AppService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(SupportTicketMessage)
        private messageRepository: Repository<SupportTicketMessage>,
        private jwtService: JwtService,
        private readonly validationService: ValidationService,
    ) {}

    async findUser(token: string) {
        let userId: string;

        try {
            const payload = this.jwtService.verify(token);
            userId = payload.userId;
        } catch {
            throw this.validationService.unauthorizedException({ token: 'INVALID' });
        }

        if (!userId) {
            throw this.validationService.unauthorizedException({ token: 'INVALID' });
        }

        const user = await this.userRepository.findOneBy({ id: userId });

        if (!user) {
            throw this.validationService.notFoundException({ user: 'NOT_FOUND' });
        }

        const organizationIds = user.properties.map((property) => property.organizationId);

        const relatedUserIds = (
            await this.userRepository.findBy({ properties: { organizationId: In(organizationIds) } })
        ).map((user) => user.id);

        const newMessageCount = await this.messageRepository.count({
            where: {
                ticket: { userId: In(relatedUserIds) },
                senderRole: user.role === 'user' ? 'support' : 'user',
                readAt: IsNull(),
            },
        });

        const tmp: Partial<UserType> = { ...user };
        delete tmp.password;
        delete tmp.emailVerificationToken;
        delete tmp.properties;
        delete tmp.organizations;

        return { ...tmp, newMessageCount };
    }
}
