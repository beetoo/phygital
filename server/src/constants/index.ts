import dotenv from 'dotenv';
import { xorToObj } from '../helpers';

const getEnvPath = (stage: string) => {
    if (stage === 'sl') return './.env.sl';
    if (stage === 'sl-eng') return './.env.eng';
    return './.env';
};

dotenv.config({ path: getEnvPath(process.env.STAGE as string) });

export const isDevProductionServer = process.env.NODE_ENV === 'production' && process.env.STAGE === 'dev';
export const isProductionServer = process.env.NODE_ENV === 'production' && process.env.STAGE === 'prod';
export const isSLServer = process.env.NODE_ENV === 'production' && process.env.STAGE === 'sl';
export const isSLEngServer = process.env.NODE_ENV === 'production' && process.env.STAGE === 'sl-eng';

export const isLocalDevelopmentServer = !isDevProductionServer && !isProductionServer && !isSLEngServer && !isSLServer;

export const { maxDevices: MAX_SL_DEVICES, expiredDate: SL_EXPIRED_DATE } = xorToObj(process.env.SL);

export const X_WEATHER_TOKEN = process.env.X_WEATHER_TOKEN || '';

export const SERVER_API_PORT = Number(process.env.SERVER_API_PORT || 3000);

export const ALLOWED_CLIENT_HOSTS = [
    'content://',
    'content://io.signageos.android.fileprovider',
    process.env.DEV_CLIENT_API_URL,
    process.env.DEV_PROD_CLIENT_API_URL,
    process.env.PROD_CLIENT_API_URL,
    process.env.SL_CLIENT_API_URL,
    'https://beta.phygitalsignage.io',
    'https://dev.phygital.pages.dev',
    'https://cloud.menusign.ru',
    'https://menusign.ru',
    'https://fijee.ru',
    'http://localhost:9000',
].filter<string>((value): value is string => !!value);

export const EMAIL_ACCOUNT_LOGIN = process.env.EMAIL_ACCOUNT_LOGIN;
export const EMAIL_ACCOUNT_PASSWORD = process.env.EMAIL_ACCOUNT_PASSWORD;
export const EMAIL_REQUIRED = EMAIL_ACCOUNT_LOGIN && EMAIL_ACCOUNT_PASSWORD;

export const UPLOAD_PATH = './upload';
export const UPLOAD_PATH_CONTENT = './upload/content';
export const UPLOAD_PATH_SCREENSHOT = './upload/screen';
export const UPLOAD_PATH_TEMP = './upload/temp';

export const EMAIL_TEMPLATES_PATH = isSLServer || isSLEngServer ? './email-templates' : './server/src/email-templates';

// mysql settings
export const MYSQL_HOST = isLocalDevelopmentServer ? 'localhost' : 'mysql';
export const MYSQL_DATABASE = isProductionServer
    ? process.env.PROD_MYSQL_DATABASE
    : isDevProductionServer
      ? process.env.DEV_MYSQL_DATABASE
      : process.env.LOCAL_MYSQL_DATABASE;

export const MYSQL_USER = process.env.MYSQL_USER;
export const MYSQL_PASSWORD = isProductionServer
    ? process.env.PROD_MYSQL_PASSWORD
    : isDevProductionServer
      ? process.env.DEV_MYSQL_PASSWORD
      : isSLServer || isSLEngServer
        ? process.env.MYSQL_PASSWORD
        : process.env.LOCAL_MYSQL_PASSWORD;

export const REDIS_HOST = isLocalDevelopmentServer ? 'localhost' : 'redis';
export const REDIS_PORT = isLocalDevelopmentServer ? 6380 : 6379;
export const REDIS_PASSWORD = isProductionServer
    ? process.env.PROD_REDIS_USER_PASSWORD
    : isDevProductionServer
      ? process.env.DEV_REDIS_USER_PASSWORD
      : process.env.LOCAL_REDIS_USER_PASSWORD;

export const SERVER_API_URL =
    (isProductionServer
        ? process.env.PROD_SERVER_API_URL
        : isSLServer || isSLEngServer
          ? process.env.SL_SERVER_API_URL
          : isDevProductionServer
            ? process.env.DEV_PROD_SERVER_API_URL
            : process.env.DEV_SERVER_API_URL) ?? 'https://cloud.phygitalsignage.io/api';

export const CLIENT_API_URL =
    (isProductionServer
        ? process.env.PROD_CLIENT_API_URL
        : isSLServer || isSLEngServer
          ? process.env.SL_CLIENT_API_URL
          : isDevProductionServer
            ? process.env.DEV_PROD_CLIENT_API_URL
            : process.env.DEV_CLIENT_API_URL) ?? 'https://cloud.phygitalsignage.io';

export const TELEGRAM_BOT_TOKEN = process.env.TELEGRAM_BOT_TOKEN ?? '';
export const TELEGRAM_CHAT_ID = process.env.TELEGRAM_CHAT_ID ?? '';
export const TELEGRAM_CHAT_FOR_TESTS_ID = process.env.TELEGRAM_CHAT_FOR_TESTS_ID ?? '';

export const SYNC_SERVER_URL =
    isProductionServer || isSLEngServer || isSLServer
        ? process.env.PROD_SYNC_SERVER_URL
        : process.env.DEV_SYNC_SERVER_URL;

export const CONTENT_STATIC_URL = `${SERVER_API_URL}/static/content`;

export const SCREENSHOT_STATIC_URL = `${SERVER_API_URL}/static/screen`;

//----- BILLING ---------

export const BILLING_URL = 'https://auth.robokassa.ru/Merchant/Index.aspx';
export const BILLING_LOGIN =
    isProductionServer || isSLEngServer || isSLServer
        ? process.env.PROD_BILLING_ROBO_LOGIN_NEW
        : process.env.DEV_BILLING_ROBO_LOGIN;
export const BILLING_SECRET_1 =
    isProductionServer || isSLEngServer || isSLServer
        ? process.env.PROD_BILLING_SECRET_1_NEW
        : process.env.DEV_BILLING_SECRET_1;
export const BILLING_SECRET_2 =
    isProductionServer || isSLEngServer || isSLServer
        ? process.env.PROD_BILLING_SECRET_2_NEW
        : process.env.DEV_BILLING_SECRET_2;

export const JIVO_SECRET_KEY =
    isProductionServer || isDevProductionServer
        ? (process.env.JIVO_PROD_SECRET_KEY ?? '')
        : isLocalDevelopmentServer
          ? (process.env.JIVO_LOCAL_SECRET_KEY ?? '')
          : '';

export const D_ID_TOKEN = process.env.D_ID_TOKEN;

export const YANDEX_SPEECH_KIT_TTS_API_KEY = process.env.YANDEX_SPEECH_KIT_TTS_API_KEY ?? '';
export const YANDEX_SPEECH_KIT_STT_API_KEY = process.env.YANDEX_SPEECH_KIT_STT_API_KEY ?? '';

export const YANDEX_STORAGE_ACCESS_KEY_ID = process.env.YANDEX_STORAGE_ACCESS_KEY_ID ?? '';
export const YANDEX_STORAGE_SECRET_ACCESS_KEY = process.env.YANDEX_STORAGE_SECRET_ACCESS_KEY ?? '';

export const DEV_PROD_SERVER_API_URL = process.env.DEV_PROD_SERVER_API_URL ?? '';
