import { ExceptionFilter, Catch, ArgumentsHost, HttpException, HttpStatus } from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';

import { TelegramBotService } from '../services/telegram-bot.service';
import { isDevProductionServer, isProductionServer } from '../constants';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
    constructor(
        private readonly httpAdapterHost: HttpAdapterHost,
        private readonly telegramBotService: TelegramBotService,
    ) {}

    async catch(exception: any, host: ArgumentsHost): Promise<any> {
        const { httpAdapter } = this.httpAdapterHost;

        const ctx = host.switchToHttp();

        const statusCode =
            exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

        const message = exception instanceof HttpException ? exception.getResponse() : exception.toString();

        const requestUrl = httpAdapter.getRequestUrl(ctx.getRequest());

        const isStaticRequest = requestUrl.startsWith('/api/static/');

        if (isStaticRequest) {
            const errorWith = (text: string) => typeof message === 'string' && message.includes(text);

            if (errorWith('ENOENT')) {
                return httpAdapter.reply(
                    ctx.getResponse(),
                    { statusCode: HttpStatus.NOT_FOUND, message: exception?.message },
                    HttpStatus.NOT_FOUND,
                );
            }

            if (errorWith('RangeNotSatisfiableError')) {
                return httpAdapter.reply(
                    ctx.getResponse(),
                    {
                        statusCode: HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE,
                        message: exception?.message,
                    },
                    HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE,
                );
            }
        }

        if (statusCode === 500) {
            const responseBody = {
                statusCode,
                message,
                path: requestUrl,
            };

            const { path } = responseBody;

            const method = httpAdapter.getRequestMethod(ctx.getRequest());

            const prefix = isProductionServer ? 'Prod: ' : isDevProductionServer ? 'Dev: ' : '';

            const text = `${prefix}500 error:\ntimestamp: ${new Date().toISOString()}\nrequest: ${method} ${path}\n${message}`;
            await this.telegramBotService.sendTestMessage(text);

            return httpAdapter.reply(ctx.getResponse(), responseBody, statusCode);
        }

        httpAdapter.reply(ctx.getResponse(), { statusCode, message }, statusCode);
    }
}
