import { performance } from 'node:perf_hooks';
import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OnEvent } from '@nestjs/event-emitter';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { Repository, In } from 'typeorm';
import hash from 'object-hash';

import { Screen } from '../models/screen/entities/screen.entity';
import { Playlist } from '../models/playlist/entities/playlist.entity';
import { CLIENT_API_URL, isLocalDevelopmentServer, isProductionServer } from '../constants';
import { TelegramBotService } from './telegram-bot.service';
import { formatScreenForSmil, withContentLinks } from '../helpers';
import { Organization } from '../models/organization/entities/organization.entity';
import { Template } from '../models/template/entities/template.entity';

const checkedOrganizationIds = [
    '3dcbe4b8-1b32-4654-86f4-afa8d334c5f5', // Баланс Кофе Ростов
    '3b47891a-feff-4e5f-bc74-fe8da762ca81', // local
];

@Injectable()
export class EventsService {
    constructor(
        @InjectRepository(Organization)
        private organizationRepository: Repository<Organization>,
        @InjectRepository(Screen)
        private screenRepository: Repository<Screen>,
        @InjectRepository(Template)
        private templateRepository: Repository<Template>,
        @InjectRepository(Playlist)
        private playlistRepository: Repository<Playlist>,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
        private telegramBotService: TelegramBotService,
    ) {}

    async clearCache(screen: Screen, organizationName: string) {
        const { id, organizationId, identification } = screen;

        const blackScreenshotCount =
            (await this.cacheManager.get<number>(`player:blackScreenshotCount:${identification}`)) ?? 0;

        if (blackScreenshotCount > 0) {
            await this.cacheManager.del(`player:data:${identification}`);
            await this.cacheManager.del(`player:smil:${identification}`);
            await this.cacheManager.del(`player:blackScreenshotCount:${identification}`);

            if (isLocalDevelopmentServer) {
                console.log(`Cache for identification ${identification} cleared!`);
            }

            const sendPhotoTimeout = setTimeout(async () => {
                const blackScreenshotCount =
                    (await this.cacheManager.get<number>(`player:blackScreenshotCount:${identification}`)) ?? 0;

                const clearCacheCount =
                    (await this.cacheManager.get<number>(`player:clearCacheCount:${identification}`)) ?? 0;

                if (blackScreenshotCount === 0 && clearCacheCount > 0) {
                    const text = `Кэш очищен для:\n${CLIENT_API_URL}/organizations/${organizationId}/screens/${id}/screenshot\nОрганизация: ${organizationName}`;

                    await this.telegramBotService.sendTestMessage(text);

                    // сброс счётчика clearCache
                    await this.cacheManager.set(`player:clearCacheCount:${identification}`, 0, 0);
                }

                clearTimeout(sendPhotoTimeout);
            }, 60_000);
        }
    }

    @OnEvent('screen.blackScreenshot')
    async blackScreenshotMessage({ identification, buffer }: { identification: string; buffer: Buffer }) {
        const screen = await this.screenRepository.findOneBy({ identification });

        if (screen) {
            const { id, organizationId } = screen;

            const organization = organizationId
                ? await this.organizationRepository.findOneBy({ id: organizationId })
                : null;

            const text = `Возможно, чёрный экран:\n${CLIENT_API_URL}/organizations/${organizationId}/screens/${id}/screenshot\nОрганизация: ${organization?.name ?? ''}`;

            if (checkedOrganizationIds.includes(organizationId)) {
                const clearCacheCount =
                    (await this.cacheManager.get<number>(`player:clearCacheCount:${identification}`)) ?? 0;

                if (clearCacheCount > 0) {
                    return;
                }

                // устанавливаем счётчик clearCache на 1
                await this.cacheManager.set(`player:clearCacheCount:${identification}`, 1, 0);

                await this.clearCache(screen, organization?.name ?? '');
            } else {
                await this.telegramBotService.sendTestPhoto(buffer, text);
            }
        }
    }

    @OnEvent('screen.json.create')
    async createScreenJsonCache({ screenId }: { screenId: string }) {
        const screen = await this.screenRepository.findOne({
            where: { id: screenId },
            relations: {
                location: true,
                playlists: { playlistItems: { content: true, scenario: true } },
            },
        });

        if (screen) {
            await this.cacheManager.set(
                `player:data:${screen.identification}`,
                {
                    hash: hash(screen),
                    modifiedAt: new Date(),
                    screen,
                },
                0,
            );
        }
    }

    @OnEvent('screen.json.update')
    async updateScreenJsonCache({
        screenId,
        screenIds,
        identification,
        identifications,
        playlistId,
        playlistIds,
    }: {
        screenId: string;
        screenIds: string[];
        identification: string;
        identifications: string[];
        playlistId: string;
        playlistIds: string[];
    }) {
        const startTime = performance.now();

        let orgId;
        let screensLength = 0;

        let wasUpdate = false;

        if (screenId || identification) {
            const screen = await this.getScreen({ screenId, identification });

            if (screen) {
                wasUpdate = true;
                orgId = screen.organizationId;
                screensLength = 1;

                await this.cacheManager.set(
                    `player:data:${screen.identification}`,
                    {
                        hash: hash(screen),
                        modifiedAt: new Date(),
                        screen,
                    },
                    0,
                );
            }
        } else if (playlistId || screenIds || identifications || playlistIds) {
            const screens = await this.getScreens({ screenIds, identifications, playlistId, playlistIds });

            orgId = screens.at(0)?.organizationId;
            screensLength = screens.length;

            wasUpdate = screens.length > 0;

            for (const screen of screens) {
                await this.cacheManager.set(
                    `player:data:${screen.identification}`,
                    {
                        hash: hash(screen),
                        modifiedAt: new Date(),
                        screen,
                    },
                    0,
                );
            }
        }

        await this.sendTestTelegramMessage(
            startTime,
            'Время',
            `Кэш обновлен:\norgId: ${orgId}\nКол-во экранов: ${screensLength}\n`,
            wasUpdate,
        );

        if (playlistId || playlistIds || screenId || screenIds) {
            const plIds = In(Array.isArray(playlistIds) ? playlistIds : [playlistId]);
            const scrIds = In(Array.isArray(screenIds) ? screenIds : [screenId]);
            const idents = In(Array.isArray(identification) ? identifications : [identification]);

            const playlists = await this.playlistRepository.find({
                where: [{ id: plIds }, { screens: { id: scrIds } }, { screens: { identification: idents } }],
                relations: { screens: true },
            });

            for (const playlist of playlists) {
                const { id, templateId, sync } = playlist;

                const cacheTemplate = await this.cacheManager.get<{ hash: string; template: Template }>(
                    `playlist:template:${id}`,
                );
                const template = templateId
                    ? await this.templateRepository.findOne({ where: { id: templateId }, select: { sections: true } })
                    : null;

                if (cacheTemplate?.hash !== hash(template)) {
                    await this.cacheManager.set(`playlist:template:${id}`, { template, hash: hash(template) }, 0);
                }

                if (sync) {
                    const syncGroupIds = playlist.screens.map(({ id }) => id).join(';');
                    await this.cacheManager.set(`player:smil:syncGroupIds:${playlist.id}`, syncGroupIds, 0);
                }
            }
        }
    }

    @OnEvent('screen.json.delete')
    async deleteScreenJsonCache({ identifications }: { identifications: string[] }) {
        for (const identification of identifications) {
            await this.cacheManager.del(`player:data:${identification}`);
        }
    }

    private async getScreen({ screenId, identification }: { screenId?: string; identification?: string }) {
        const screen = await this.screenRepository.findOne({
            where: [{ id: screenId }, { identification }],
            relations: {
                location: true,
                playlists: { playlistItems: { content: true, scenario: true } },
            },
        });

        return screen ? formatScreenForSmil(withContentLinks(screen)) : null;
    }

    private async getScreens({
        screenIds,
        identifications,
        playlistId,
        playlistIds,
    }: {
        screenIds?: string[];
        identifications?: string[];
        playlistId: string;
        playlistIds?: string[];
    }) {
        const screens = await this.screenRepository.find({
            where: [
                { id: screenIds ? In(screenIds) : undefined },
                { identification: identifications ? In(identifications) : undefined },
                { playlists: playlistId ? { id: playlistId } : undefined },
                { playlists: playlistIds ? { id: In(playlistIds) } : undefined },
            ],
            relations: {
                location: true,
                playlists: { playlistItems: { content: true, scenario: true } },
            },
        });

        return screens.map((screen) => formatScreenForSmil(withContentLinks(screen)));
    }

    private async sendTestTelegramMessage(startTime: number, message: string, prevMessage = '', send = false) {
        const endTime = performance.now();

        const deltaMs = (endTime - startTime) / 1000;

        const text = prevMessage + `${message}: ${deltaMs.toFixed(3)} sec\n`;

        if (send && deltaMs >= 1) {
            if (isLocalDevelopmentServer) {
                console.log(text);
            }

            if (isProductionServer) {
                await this.telegramBotService.sendTestMessage(text);
            }
        } else {
            return text;
        }
    }
}
