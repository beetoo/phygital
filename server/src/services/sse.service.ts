import { Injectable } from '@nestjs/common';
import { EventEmitter } from 'events';
import { Request, Response } from 'express';

type DataType = {
    organizationId: string;
    type: string;
};

@Injectable()
export class SseService extends EventEmitter {
    constructor() {
        super();
    }

    init(req: Request, res: Response) {
        res.setHeader('Content-Type', 'text/event-stream');
        res.setHeader('Cache-Control', 'no-cache');
        res.setHeader('Connection', 'keep-alive');
        res.setHeader('X-Accel-Buffering', 'no');

        const dataListener = (data: DataType) => {
            res.write(`data: ${JSON.stringify(data)}\n`);
            res.write('\n');
        };

        this.on('data', dataListener);

        req.on('close', () => {
            this.removeListener('data', dataListener);
        });
    }

    send(data: DataType) {
        this.emit('data', data);
    }
}
