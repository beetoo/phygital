import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';

import { Screen } from '../models/screen/entities/screen.entity';
import { ValidationService } from './validation/validation.service';

@Injectable()
export class WeatherService {
    constructor(
        private httpService: HttpService,
        private readonly validationService: ValidationService,
    ) {}

    async getWeatherToPoint(
        lat: number,
        lon: number,
        screen?: Screen,
    ): Promise<{ temp: number; condition: string; cloudness: string }> {
        return (
            screen?.location.variables?.find((v) => v.weather)?.weather.value ||
            (await this.getWeatherToPointFromServer(lat, lon))
        );
    }

    private async getWeatherToPointFromServer(lat: number, lon: number) {
        let weatherResponse;
        try {
            const url = `https://yandex.ru/pogoda/front/maps/prec-alert?lat=${lat}&lon=${lon}`;
            weatherResponse = await this.httpService.get(url).toPromise();
        } catch (e) {
            throw this.validationService.notFoundException({ weatherServer: e.message });
        }

        if (!weatherResponse?.data) {
            throw this.validationService.badRequestException({ weatherData: 'IS_EMPTY' });
        }

        return {
            temp: weatherResponse?.data?.fact.temp,
            condition: weatherResponse?.data?.fact.condition,
            cloudness: weatherResponse?.data?.fact.cloudness,
        };
    }
}

// const weatherDataExample = {
//     fact: {
//         temp: 25,
//         icon: 'bkn_d',
//         condition: 'cloudy',
//         dayTime: 'day',
//         cloudness: 'partly',
//     },
//     alert: {
//         type: 'noprec',
//         strength: false,
//         isNoPrec: true,
//         isNoRule: false,
//         isNoData: false,
//         current: {
//             icon: 'bkn_d',
//             prec_type: 0,
//             prec_strength: 0,
//             condition: 'cloudy',
//             cloudness: 0.5,
//         },
//         time: 1657298400,
//         state: 'noprec',
//         smartZoom: 10,
//         title: 'В ближайшие 2 часа осадков не ожидается',
//     },
// };
