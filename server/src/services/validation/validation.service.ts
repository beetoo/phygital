import {
    BadRequestException,
    NotFoundException,
    Injectable,
    ForbiddenException,
    UnauthorizedException,
} from '@nestjs/common';

@Injectable()
export class ValidationService {
    throwBadRequestExceptionIf(condition: boolean, errors: { [key: string]: string | undefined | object }) {
        if (condition) {
            throw this.badRequestException(errors);
        }
    }

    throwNotFoundExceptionIf(condition: boolean, errors: { [key: string]: string | undefined | object }) {
        if (condition) {
            throw this.notFoundException(errors);
        }
    }

    throwForbiddenExceptionIf(condition: boolean, errors: { [key: string]: string | undefined | object }) {
        if (condition) {
            throw this.forbiddenException(errors);
        }
    }

    throwUnauthorizedExceptionIf(condition: boolean, errors: { [key: string]: string | undefined | object }) {
        if (condition) {
            throw this.unauthorizedException(errors);
        }
    }

    badRequestException(errors: { [key: string]: string | undefined | object }) {
        return new BadRequestException({
            message: 'Bad request',
            errors,
        });
    }

    notFoundException(errors: { [key: string]: string | undefined | object }) {
        return new NotFoundException({
            message: 'Not found',
            errors,
        });
    }

    forbiddenException(errors: { [key: string]: string | undefined | object }) {
        return new ForbiddenException({
            message: 'Forbidden resource',
            errors,
        });
    }

    unauthorizedException(errors: { [key: string]: string | undefined | object }) {
        return new UnauthorizedException({
            message: 'Unauthorized',
            errors,
        });
    }
}
