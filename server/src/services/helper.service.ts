import { Inject, Injectable } from '@nestjs/common';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { differenceInSeconds } from 'date-fns';

import { ScreenHistory } from '../models/screen/entities/screen-history.entity';
import { Screen } from '../models/screen/entities/screen.entity';

@Injectable()
export class HelperService {
    constructor(
        @InjectRepository(Screen)
        private screenRepository: Repository<Screen>,
        @InjectRepository(ScreenHistory)
        private screenHistoryRepository: Repository<ScreenHistory>,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
    ) {}

    async updateScreenStatus(identification: string, screen: Screen | null, screenHistoryRequest = false) {
        if (!identification || !screen) return;

        const prevAliveAt =
            (await this.cacheManager.get<Date>(`player:screenAliveAt:${identification}`)) ??
            screen.createdAt ??
            (await this.screenRepository.findOneBy({ identification }))?.createdAt;

        const prevStatus = await this.cacheManager.get<'offline' | 'online'>(`player:status:${identification}`);

        if (!screenHistoryRequest) {
            await this.cacheManager.set(`player:screenAliveAt:${identification}`, new Date(), 0);
        }

        const status = differenceInSeconds(new Date(), new Date(prevAliveAt)) > 180 ? 'offline' : 'online';

        if (screen.id && prevStatus !== status) {
            const lastScreenHistoryItem = await this.screenHistoryRepository.findOne({
                where: { screenId: screen.id },
                select: { id: true, status: true, createdAt: true },
                order: { createdAt: 'DESC' },
            });

            if (lastScreenHistoryItem?.status !== status) {
                await this.cacheManager.set(`player:status:${screen.identification}`, status, 0);

                await this.screenHistoryRepository.save({ screenId: screen.id, status, createdAt: prevAliveAt });
            }
        }
    }
}
