import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Cron } from '@nestjs/schedule';
import { Repository } from 'typeorm';
import axios from 'axios';
import { validate } from 'uuid';

import { Screen } from '../models/screen/entities/screen.entity';
import { isLocalDevelopmentServer, SERVER_API_URL } from '../constants';
import { isCronWorker } from '../helpers/isCronWorker';

@Injectable()
export class TestStatusService {
    private lastModifiedInfo: { identification: string; lastModified: string }[];
    constructor(
        @InjectRepository(Screen)
        private screenRepository: Repository<Screen>,
    ) {
        this.lastModifiedInfo = [];
    }

    // каждую минуту
    @Cron('0,30 * * * * *', { disabled: !isLocalDevelopmentServer })
    async headRequest() {
        if (!(await isCronWorker())) return;

        const screens = (await this.screenRepository.find()).filter(({ identification }) => !validate(identification));

        for (const { identification } of screens) {
            let response;

            try {
                response = await axios.head(`${SERVER_API_URL}/smil`, {
                    headers: { identification, availableBytes: 5, totalBytes: 10 },
                });
                // console.log('HEAD', `${SERVER_API_URL}/smil`, `identification: ${identification}`);
            } catch (err) {
                console.log('HEAD', `${SERVER_API_URL}/smil`, `identification: ${identification}`, err.message);
                continue;
            }

            const lastModified = response.headers['last-modified'];
            const lastModifiedInfo = this.lastModifiedInfo.find((info) => info.identification === identification);

            if (!lastModifiedInfo) {
                this.lastModifiedInfo = this.lastModifiedInfo.concat({ identification, lastModified });
            } else if (lastModifiedInfo.lastModified !== lastModified) {
                this.lastModifiedInfo = this.lastModifiedInfo.map((info) =>
                    info.identification === identification ? { ...info, lastModified } : info,
                );

                try {
                    await axios.get(`${SERVER_API_URL}/smil`, { headers: { identification } });
                } catch (err) {
                    console.log('GET', `${SERVER_API_URL}/smil`, err.message);
                    continue;
                }

                try {
                    await axios.post(
                        `${SERVER_API_URL}/smil/status`,
                        { status: 'upload_success' },
                        { headers: { identification } },
                    );
                } catch (err) {
                    console.log('POST', `${SERVER_API_URL}/smil/status`, err.message);
                }
            }
        }
    }
}
