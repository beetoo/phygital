import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Request } from 'express';

import { User } from '../models/user/entities/user.entity';
import { extractTokenFromHeader } from '../helpers';

@Injectable()
export class UserActivityLogEmitService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        private jwtService: JwtService,
        private readonly eventEmitter: EventEmitter2,
    ) {}

    async emitLogEvent(event: string, request: Request, userId?: string) {
        userId = userId || (await this.getUserInfoFromRequest(request))?.id;

        if (!userId) return;

        try {
            this.eventEmitter.emit('user.log.event', {
                userId: userId || (await this.getUserInfoFromRequest(request))?.id,
                event,
                userAgent: request.headers['user-agent'],
                ip: request.clientIp,
            });
        } catch (err) {
            console.error(err);
        }
    }

    private async getUserInfoFromRequest(req: Request) {
        const token = extractTokenFromHeader(req);

        if (!token) {
            return null;
        }

        let decodedId: string;
        try {
            const { userId } = this.jwtService.verify(token);
            decodedId = userId;
        } catch {
            return null;
        }

        return this.userRepository.findOneBy({ id: decodedId });
    }
}
