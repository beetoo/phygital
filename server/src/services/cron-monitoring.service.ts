import cluster from 'node:cluster';
import fs from 'node:fs';
import { Inject, Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, IsNull, Not, Repository } from 'typeorm';
import { differenceInMonths } from 'date-fns';
import axios from 'axios';
import hash from 'object-hash';
import { validate } from 'uuid';

import { Screen } from '../models/screen/entities/screen.entity';
import { ScreenHistory } from '../models/screen/entities/screen-history.entity';
import { isCronWorker } from '../helpers/isCronWorker';
import { Location } from '../models/location/entities/location.entity';
import { UPLOAD_PATH_CONTENT, UPLOAD_PATH_TEMP } from '../constants';
import { deleteFile } from '../helpers/deleteFile';
import { Content } from '../models/content/entities/content.entity';

const moscowCoordinates = {
    // по дефолту координаты цента Москвы
    geo_lat: '55.752004',
    geo_lon: '37.617734',
};

@Injectable()
export class CronMonitoringService {
    constructor(
        @InjectRepository(Screen)
        private screenRepository: Repository<Screen>,
        @InjectRepository(Content)
        private contentRepository: Repository<Content>,
        @InjectRepository(Location)
        private locationRepository: Repository<Location>,
        @InjectRepository(ScreenHistory)
        private screenHistoryRepository: Repository<ScreenHistory>,
        private eventEmitter: EventEmitter2,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
    ) {
        setTimeout(async () => {
            await this.checkUploadFolder();
        }, 5000);
    }

    // каждые 10 минут
    @Cron('*/10 * * * *')
    async updateWeatherCron() {
        if (!(await isCronWorker())) return;

        const options: FindManyOptions<Screen> = {
            where: {
                location: Not(IsNull()),
                playlists: { playlistItems: { scenario: { rules: { condition: 'weather' } } } },
            },
            relations: { location: true },
        };

        const screens = await this.screenRepository.find(options);

        const locations = screens
            .map(({ location }) =>
                location?.geo_lat && location?.geo_lat
                    ? location
                    : {
                          ...location,
                          geo_lat: moscowCoordinates.geo_lat,
                          geo_lon: moscowCoordinates.geo_lon,
                      },
            )
            .reduce(
                (acc: Location[], location) => (acc.some((loc) => loc?.id === location.id) ? acc : [...acc, location]),
                [],
            );

        const prevData: any[] = [];

        let error = false;

        let screenIds: string[] = [];

        for (const location of locations) {
            if (error) continue;

            const { geo_lon, geo_lat, variables: prevVariables } = location;

            const prevWeatherData = prevData.find(
                ({ coordinates }) => coordinates?.geo_lat === geo_lat && coordinates?.geo_lon === geo_lon,
            );

            const variables = prevVariables?.filter((variable) => !variable?.weather) ?? [];

            if (prevWeatherData) {
                variables.push(prevWeatherData.weather);
            } else {
                try {
                    const response = await axios.get(
                        `https://yandex.ru/pogoda/front/maps/prec-alert?lat=${geo_lat}&lon=${geo_lon}`,
                    );

                    // const response = { data: { fact: { temp: 16, condition: '', cloudness: '' } } } as any;

                    const weatherValue = {
                        temp: response.data.fact.temp,
                        condition: response.data.fact.condition,
                        cloudness: response.data.fact.cloudness,
                    };

                    const weatherVariable = { weather: { value: weatherValue, readOnly: true, updatedAt: new Date() } };

                    variables.push(weatherVariable);

                    prevData.push({ coordinates: { geo_lat, geo_lon }, ...weatherVariable });
                } catch (err) {
                    error = true;

                    console.log('Weather cron error: ', err);

                    this.addLog(
                        './server/weather-cron-logs',
                        `Error message: ${err?.message} at ` + new Date().toUTCString(),
                    );
                }
            }

            if (!error) {
                const checkHash = false;

                if (!checkHash) {
                    await this.locationRepository.update(location.id, { variables });
                    continue;
                }

                const identification = screens.find((screen) => screen.location.id === location.id)?.identification;

                const weatherValue = variables?.find((v) => v.weather)?.weather.value;
                const cacheWeatherValue = (
                    (await this.cacheManager.get(`player:data:${identification}`)) as { screen: Screen }
                )?.screen.location.variables?.find((v) => v.weather)?.weather.value;

                if (weatherValue && cacheWeatherValue && hash(weatherValue) !== hash(cacheWeatherValue)) {
                    screenIds = screenIds.concat(screens.map(({ id }) => id));

                    await this.locationRepository.update(location.id, { variables });
                }
            }
        }

        if (!error) {
            const screens = await this.screenRepository.find(options);

            const checkHash = true;

            const screenIds = (
                await Promise.all(
                    screens.map(async (screen) => {
                        if (!checkHash) {
                            return screen.id;
                        }

                        const weatherValue = screen.location.variables?.find((v) => v.weather)?.weather.value;

                        const cacheWeatherValue = (
                            (await this.cacheManager.get(`player:data:${screen.identification}`)) as { screen: Screen }
                        )?.screen.location.variables?.find((v) => v.weather)?.weather.value;

                        if (weatherValue && cacheWeatherValue && hash(weatherValue) !== hash(cacheWeatherValue)) {
                            return screen.id;
                        }
                    }),
                )
            ).filter((id) => id);

            this.eventEmitter.emit('screen.json.update', { screenIds });
        }
    }

    // каждый день в час ночи
    @Cron('0 1 * * *')
    async deleteOldScreenHistory() {
        if (!(await isCronWorker())) return;

        const timeFilter = (arr: ScreenHistory[]) =>
            arr.filter(
                ({ createdAt, screenId }) =>
                    arr.filter((history) => history.screenId === screenId).length > 1 &&
                    differenceInMonths(new Date(), createdAt) > 0,
            );

        const removedScreenHistory = timeFilter(await this.screenHistoryRepository.find());

        await this.screenHistoryRepository.remove(removedScreenHistory);
    }

    // каждые 5 минут лог - проверка работы CronWorker
    @Cron('*/5 * * * *')
    async cronWorkerIdLog() {
        if (!(await isCronWorker())) return;

        const cronWorkerId = await this.cacheManager.get('cronWorkerId');

        console.log(
            'Cron Worker ID is',
            cluster.isPrimary ? 'primary' : cluster.worker?.id,
            'at',
            new Date().toUTCString() + '.',
            'Saved ID is',
            cronWorkerId,
        );
    }

    private addLog(path: string, message: string) {
        let logs = '';

        fs.access(path, fs.constants.R_OK | fs.constants.W_OK, (err) => {
            if (err) return;

            fs.readFile(path, (err, data) => {
                if (err) return;

                logs = data.toString();

                if (logs) {
                    logs += '\n';
                }

                fs.writeFile(path, logs + message, () => {});
            });
        });
    }

    async checkUploadFolder() {
        if (!(await isCronWorker())) return;

        const deleteFoldersRequired = false;
        const deleteFilesRequired = false;
        const deleteTempFilesRequired = true;

        const notExistedInDbFolders = (
            await Promise.all(
                fs
                    .readdirSync(UPLOAD_PATH_CONTENT)
                    .filter((id) => validate(id) && fs.statSync(`${UPLOAD_PATH_CONTENT}/${id}`).isDirectory())
                    .map(async (id) => ((await this.contentRepository.findOneBy({ id })) === null ? id : null)),
            )
        )
            .filter((id) => id !== null)
            .map((id) => `${UPLOAD_PATH_CONTENT}/${id}`);

        if (notExistedInDbFolders.length > 0) {
            console.log('Not existed in database folders are', notExistedInDbFolders);

            if (deleteFoldersRequired) {
                await Promise.all(notExistedInDbFolders.map((path) => deleteFile(path)));

                console.log('Not existed in database folders have been deleted.');
            }
        } else {
            console.log("Not existed in database folders don't exist.");
        }

        const filesInUploadFolder = fs
            .readdirSync(UPLOAD_PATH_CONTENT)
            .filter((id) => id !== '.gitkeep' && fs.statSync(`${UPLOAD_PATH_CONTENT}/${id}`).isFile())
            .map((id) => `${UPLOAD_PATH_CONTENT}/${id}`);

        if (filesInUploadFolder.length > 0) {
            console.log('Files in upload folder are', filesInUploadFolder);

            if (deleteFilesRequired) {
                await Promise.all(filesInUploadFolder.map((path) => deleteFile(path)));

                console.log('Files in upload folder have been deleted.');
            }
        } else {
            console.log("Files in upload folder don't exist.");
        }

        const tempFiles = fs
            .readdirSync(UPLOAD_PATH_TEMP)
            .filter((id) => id !== '.gitkeep' && fs.statSync(`${UPLOAD_PATH_TEMP}/${id}`).isFile())
            .map((id) => `${UPLOAD_PATH_TEMP}/${id}`);

        if (tempFiles.length > 0) {
            console.log('Temp files are', tempFiles);

            if (deleteTempFilesRequired) {
                await Promise.all(tempFiles.map((path) => deleteFile(path)));

                console.log('Temp files have been deleted.');
            }
        } else {
            console.log("Temp files don't exist.");
        }
    }

    async checkEmptyFolders() {
        if (!(await isCronWorker())) return;

        const emptyContentFolders = fs
            .readdirSync(UPLOAD_PATH_CONTENT)
            .map((id) => `${UPLOAD_PATH_CONTENT}/${id}`)
            .filter((path) => fs.statSync(path).isDirectory() && this.isEmptyDir(path));

        // .map((path) => deleteFile(path));

        if (emptyContentFolders.length > 0) {
            const deleteRequired = false;

            console.log('Empty Folders are', emptyContentFolders);

            if (deleteRequired) {
                await Promise.all(emptyContentFolders.map((path) => deleteFile(path)));

                console.log('Empty Folders have been deleted.');
            }
        } else {
            console.log("Empty Folders don't exist.");
        }
    }

    private isEmptyDir(path: string) {
        try {
            const directory = fs.opendirSync(path);
            const entry = directory.readSync();
            directory.closeSync();

            return entry === null;
        } catch {
            return false;
        }
    }
}
