import { Inject, Injectable } from '@nestjs/common';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import hash from 'object-hash';

import { WeatherService } from './weather.service';
import { Screen } from '../models/screen/entities/screen.entity';
import { getRuleExecutor, RuleExecutionContext } from '../models/scenario/utils/ruleExecutors';
import { playlistItemIsNotNull } from '../helpers';
import { PlaylistItemType, PlaylistType, ScenarioRuleType, WeatherAndTrafficData } from '../../../common/types';

@Injectable()
export class TriggerService {
    constructor(
        private readonly weatherService: WeatherService,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
    ) {}

    private async executeRule(rule: ScenarioRuleType, ctx: RuleExecutionContext): Promise<boolean> {
        const executor = rule.condition ? getRuleExecutor(rule.condition) : null;
        if (executor) {
            try {
                return await executor(ctx);
            } catch (e) {
                console.error(`Error at executor for type "${rule.condition}"`, e);
                return false;
            }
        } else {
            console.warn(`TriggerService do not support type "${rule.condition}" to be executed`);
            return false;
        }
    }

    private async getTriggeredItems(screen: Screen, playlist: PlaylistType): Promise<Array<PlaylistItemType>> {
        const geo =
            screen?.location?.geo_lat && screen?.location?.geo_lon
                ? {
                      lat: parseFloat(screen?.location.geo_lat),
                      lon: parseFloat(screen?.location.geo_lon),
                  }
                : {
                      // по дефолту координаты цента Москвы
                      lat: 55.752004,
                      lon: 37.617734,
                  };

        return Promise.all<PlaylistItemType | null>(
            playlist.playlistItems.map(async (item) => {
                const rules = item.scenario?.rules;

                if (rules?.length) {
                    let isItemEnabled = true;

                    const weatherRules = rules.filter(
                        (rule) =>
                            rule.condition === 'weather' &&
                            (rule.data as WeatherAndTrafficData).parameter === 'temperature',
                    );

                    const withEquallyOperator =
                        weatherRules.length === 2 &&
                        weatherRules.some((rule) => (rule.data as WeatherAndTrafficData).operator === 'equally');

                    for (const rule of rules) {
                        const isRuleMatched =
                            rule.data &&
                            (await this.executeRule(rule, {
                                geo,
                                data: rule.data,
                                screen,
                                services: { weatherService: this.weatherService },
                                withEquallyOperator,
                            }));

                        if (!isRuleMatched) {
                            isItemEnabled = false;
                        }
                    }

                    if (isItemEnabled) {
                        return item;
                    }

                    //no rules triggered for PlaylistItem, skip it
                    return null;
                } else {
                    //no rules defined for PlaylistItem, it must be active always
                    return item;
                }
            }),
        ).then((items) => items.filter(playlistItemIsNotNull));
    }

    async getTriggeredPlaylist(screen: Screen, playlist: PlaylistType): Promise<PlaylistType | undefined> {
        const triggeredItems = await this.getTriggeredItems(screen, playlist);

        if (triggeredItems.length === 0) {
            return;
        }

        const prevTriggeredItemsCache = (await this.cacheManager.get(`triggeredItems:${playlist.id}`)) as {
            items: PlaylistItemType[];
            hash: string;
        };

        const prevTriggeredItemsHash = prevTriggeredItemsCache?.hash;

        if (prevTriggeredItemsHash !== hash(triggeredItems)) {
            // need always save cache
            await this.cacheManager.set(
                `triggeredItems:${playlist.id}`,
                { items: triggeredItems, hash: hash(triggeredItems) },
                0,
            );
        }

        return {
            ...playlist,
            playlistItems: triggeredItems.sort(
                ({ orderIndex: prevOrderIndex }, { orderIndex }) => prevOrderIndex - orderIndex,
            ),
        };
    }
}
