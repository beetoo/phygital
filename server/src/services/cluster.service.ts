import * as process from 'node:process';
import cluster from 'node:cluster';
import os from 'node:os';
import { Injectable } from '@nestjs/common';

import { isProductionServer, isDevProductionServer, isLocalDevelopmentServer } from '../constants';
import { redisStore } from '../helpers/redisClient';

const getNumCPUs = () => {
    if (isProductionServer) {
        return 5;
    } else if (isDevProductionServer) {
        return 2;
    } else if (isLocalDevelopmentServer) {
        return os.availableParallelism() > 2 ? 2 : 1;
    } else {
        return os.availableParallelism() > 2 ? 2 : 1;
    }
};

const numCPUs = getNumCPUs();

@Injectable()
export class ClusterService {
    constructor() {}

    static async clusterize(callback: any) {
        if (cluster.isPrimary && numCPUs > 1) {
            console.log(`PRIMARY SERVER (${process.pid}) IS RUNNING`);

            const store = await redisStore;

            for (let i = 0; i < numCPUs; i++) {
                const worker = cluster.fork();
                if (i === 0) {
                    await store.set('cronWorkerId', worker.id);
                }
            }

            cluster.on('exit', async (worker) => {
                console.log(`Worker ${worker.process.pid} with id ${worker.id} died. Restarting`);
                const restartedWorker = cluster.fork();

                await store.set('cronWorkerId', restartedWorker.id);
            });
        } else {
            console.log(`Cluster server started on PID ${process.pid}`);
            callback();
        }
    }
}
