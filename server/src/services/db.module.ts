import { Module } from '@nestjs/common';
import { createPool, PoolOptions } from 'mysql2/promise';
import { isLocalDevelopmentServer, MYSQL_DATABASE, MYSQL_HOST, MYSQL_PASSWORD, MYSQL_USER } from '../constants';

export const MYSQL_CONNECTION = 'MYSQL_CONNECTION';

const dbProvider = {
    provide: MYSQL_CONNECTION,
    useValue: createPool({
        host: MYSQL_HOST,
        port: isLocalDevelopmentServer ? 3307 : 3306,
        user: MYSQL_USER, // ENV MYSQL_USER
        password: MYSQL_PASSWORD, // ENV MYSQL_PASSWORD
        database: MYSQL_DATABASE, // ENV MYSQL_DATABASE
        rowsAsArray: true,
    } as PoolOptions),
};

@Module({
    providers: [dbProvider],
    exports: [dbProvider],
})
export class DbModule {}
