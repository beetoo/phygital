import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OnEvent } from '@nestjs/event-emitter';
import { Repository } from 'typeorm';

import { UserActivityLog } from '../models/user/entities/user-activity-log.entity';

@Injectable()
export class UserActivityLogEventsService {
    constructor(
        @InjectRepository(UserActivityLog)
        private userActivityLogRepository: Repository<UserActivityLog>,
    ) {}

    @OnEvent('user.log.event')
    async addNewScreenStatus(createUserActivityLogDto: UserActivityLog) {
        await this.userActivityLogRepository.save(createUserActivityLogDto);
    }
}
