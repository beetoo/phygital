import { Injectable } from '@nestjs/common';
import TelegramBot from 'node-telegram-bot-api';

import {
    isLocalDevelopmentServer,
    TELEGRAM_BOT_TOKEN,
    TELEGRAM_CHAT_FOR_TESTS_ID,
    TELEGRAM_CHAT_ID,
} from '../constants';

@Injectable()
export class TelegramBotService {
    private readonly bot: TelegramBot;

    constructor() {
        this.bot = new TelegramBot(TELEGRAM_BOT_TOKEN);
    }

    async sendTestMessage(text: string) {
        if (isLocalDevelopmentServer) {
            return console.log(text);
        }

        try {
            await this.bot.sendMessage(TELEGRAM_CHAT_FOR_TESTS_ID, text);
        } catch (err) {
            console.log(err.message);
        }
    }

    async sendTestPhoto(buffer: Buffer, text: string) {
        if (isLocalDevelopmentServer) {
            return console.log(text);
        }

        try {
            await this.bot.sendPhoto(
                TELEGRAM_CHAT_FOR_TESTS_ID,
                buffer,
                { caption: text },
                { filename: 'screenshot.jpg', contentType: 'image/jpeg' },
            );
        } catch (err) {
            await this.bot.sendMessage(TELEGRAM_CHAT_FOR_TESTS_ID, text);
            console.log(err.message);
        }
    }

    async sendMessage(text: string) {
        if (isLocalDevelopmentServer) {
            return console.log(text);
        }

        try {
            await this.bot.sendMessage(TELEGRAM_CHAT_ID, text);
        } catch (err) {
            console.log(err.message);
        }
    }
}
