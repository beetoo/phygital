import { Inject, Injectable } from '@nestjs/common';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { Request } from 'express';

import { User } from '../models/user/entities/user.entity';
import { extractTokenFromHeader } from '../helpers';
import { ValidationService } from './validation/validation.service';

export type UserSession = {
    id: string;
    ip: string;
    token: string;
    userAgent: string;
    createdAt: string;
};

@Injectable()
export class UserSessionsService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
        private jwtService: JwtService,
        private readonly validationService: ValidationService,
    ) {}

    async addUserSession(token: string | undefined, request: Request) {
        const user = await this.getUserFromToken(token);

        if (user) {
            const userSessions = (await this.cacheManager.get<UserSession[]>(`user:sessions:${user.id}`)) ?? [];

            await this.cacheManager.set(
                `user:sessions:${user.id}`,
                [
                    ...userSessions,
                    {
                        id: uuidv4(),
                        token,
                        ip: request.clientIp,
                        userAgent: request.headers['user-agent'],
                        createdAt: new Date(),
                    },
                ],
                0,
            );
        }

        return user;
    }

    async removeUserSession(request: Request) {
        const token = extractTokenFromHeader(request);
        const user = await this.getUserFromToken(token);

        if (user) {
            const userSessions = (
                (await this.cacheManager.get<UserSession[]>(`user:sessions:${user.id}`)) ?? []
            ).filter((session) => session.token !== token);

            await this.cacheManager.set(`user:sessions:${user.id}`, userSessions, 0);
        }

        return user;
    }

    async updateUserSession(token: string, request: Request) {
        await this.removeUserSession(request);
        await this.addUserSession(token, request);
    }

    async checkUserSessionsAndGetUser(request: Request) {
        const token = extractTokenFromHeader(request);
        const user = await this.getUserFromToken(token);

        if (user && request.baseUrl !== '/api/auth/signout') {
            const userSessions = (await this.cacheManager.get<UserSession[]>(`user:sessions:${user.id}`)) ?? [];

            const hasToken = userSessions.some((session) => session.token === token);

            this.validationService.throwUnauthorizedExceptionIf(!hasToken, { token: 'NOT_FOUND' });
        }
    }

    getUserFromToken(token: string | undefined) {
        try {
            if (!token) {
                return null;
            }

            const { userId } = this.jwtService.verify(token);

            return this.userRepository.findOneBy({ id: userId });
        } catch {
            return null;
        }
    }

    getUserFromRequest(request: Request) {
        const token = extractTokenFromHeader(request);

        return this.getUserFromToken(token);
    }
}
