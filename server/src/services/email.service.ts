import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { CLIENT_API_URL, EMAIL_REQUIRED, EMAIL_TEMPLATES_PATH, isSLEngServer } from '../constants';
import { Organization } from '../models/organization/entities/organization.entity';
import { getChangePasswordTimeText } from '../helpers';
import { getOwnerUser } from '../models/organization/helpers';

@Injectable()
export class EmailService {
    constructor(
        private readonly mailerService: MailerService,
        @InjectRepository(Organization)
        private organizationRepository: Repository<Organization>,
    ) {}

    async isEnglish(email: string): Promise<boolean> {
        if (isSLEngServer) {
            return true;
        }

        const organization = await this.organizationRepository.findOne({
            where: { users: { email } },
            relations: { users: true },
        });

        if (organization) {
            const user = getOwnerUser(organization);

            return user?.language === 'en' || user?.language === 'es';
        }

        return false;
    }

    async sendConfirmRegistrationEmail(email: string, emailVerificationCode: string) {
        if (!EMAIL_REQUIRED) return;

        const isEng = await this.isEnglish(email);

        const href = `${CLIENT_API_URL}/account/email/verify/${emailVerificationCode}`;

        this.mailerService.sendMail({
            to: email,
            subject: isEng ? 'Registration confirmation' : 'Подтверждение почты в Фиджитал',
            template: EMAIL_TEMPLATES_PATH + `/confirmRegistrationEmail${isEng ? '_eng' : ''}.hbs`,
            attachments: [
                {
                    filename: 'LogoForEmail2.png',
                    path: EMAIL_TEMPLATES_PATH + '/assets/LogoForEmail2.png',
                    cid: 'logo',
                },
            ],
            context: {
                href,
            },
        });
    }

    async sendResetPasswordEmail(email: string, token: string) {
        if (!EMAIL_REQUIRED) return;

        const isEng = await this.isEnglish(email);

        const href = `${CLIENT_API_URL}/account/password/recovery/${token}`;

        this.mailerService.sendMail({
            to: email,
            subject: isEng ? 'Resetting Password' : 'Восстановление пароля',
            template: EMAIL_TEMPLATES_PATH + `/resetPasswordEmail${isEng ? '_eng' : ''}.hbs`,
            attachments: [
                {
                    filename: 'LogoForEmail2.png',
                    path: EMAIL_TEMPLATES_PATH + '/assets/LogoForEmail2.png',
                    cid: 'logo',
                },
            ],
            context: {
                href,
            },
        });
    }

    async sendChangePasswordEmail(title: string, email: string, timezoneOffset: number) {
        if (!EMAIL_REQUIRED) return;

        const isEng = await this.isEnglish(email);

        this.mailerService.sendMail({
            to: email,
            subject: isEng ? 'Password has been changed' : 'Пароль успешно изменён',
            template: EMAIL_TEMPLATES_PATH + `/changePasswordEmail${isEng ? '_eng' : ''}.hbs`,
            attachments: [
                {
                    filename: 'LogoForEmail.png',
                    path: EMAIL_TEMPLATES_PATH + '/assets/LogoForEmail.png',
                    cid: 'logo',
                },
            ],
            context: {
                title,
                changePasswordTimeText: getChangePasswordTimeText(isEng, timezoneOffset),
            },
        });
    }
}
