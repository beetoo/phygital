import { OnGatewayConnection, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { OnEvent } from '@nestjs/event-emitter';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Server, Socket } from 'socket.io';

import { Organization } from '../models/organization/entities/organization.entity';
import { CLIENT_API_URL } from '../constants';
import { PlaylistUploadStatus, ScreenStatusType } from '../../../common/types';

type UpdatePlaylistStatus = {
    organizationId: string;
    playlistId: string;
    status: PlaylistUploadStatus;
};

type UpdateScreenStatus = {
    organizationId: string;
    screenId: string;
    status: ScreenStatusType;
};

@WebSocketGateway({ path: '/ws', cors: { origin: CLIENT_API_URL, credentials: true } })
export class EventsGateway implements OnGatewayConnection {
    private adminIds: string[];
    constructor(
        @InjectRepository(Organization)
        private organizationRepository: Repository<Organization>,
    ) {
        this.organizationRepository.findBy({ users: { role: In(['admin', 'accounting']) } }).then((organizations) => {
            this.adminIds = organizations.map((organization) => organization.id);
        });
    }

    @WebSocketServer()
    server: Server;

    handleConnection(client: Socket): any {
        const organizationId = client.handshake.auth.organizationId;

        if (organizationId) {
            client.join(organizationId);
        } else {
            client.disconnect();
        }
    }

    @OnEvent('playlist.status.update')
    async handlePlaylistStatusUpdate({ organizationId, playlistId, status }: UpdatePlaylistStatus) {
        await this.emitEvent('playlist.status', organizationId, { playlistId, status });
    }

    @OnEvent('screen.status.update')
    async handleScreenStatusUpdate({ organizationId, screenId, status }: UpdateScreenStatus) {
        await this.emitEvent('screen.status', organizationId, { screenId, status });
    }

    private async emitEvent(event: string, room: string, payload: unknown) {
        if (!this.server.of('/').adapter.rooms.get(room)) {
            const organization = await this.organizationRepository.findOneBy({ id: room });

            if (organization) {
                this.server.to(organization.id).emit(event, payload);
            }
        } else {
            this.server.to(room).emit(event, payload);
        }

        for (const adminId of this.adminIds) {
            this.server.to(adminId).emit(event, payload);
        }
    }
}
