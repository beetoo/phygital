import { SetMetadata } from '@nestjs/common';

export enum ControllerAction {
    view_organization = 'view_organization',
    update_organization = 'update_organization',
    delete_organization = 'delete_organization',
    leave_organization = 'leave_organization',
    invite_user = 'invite_user',
    update_user_access = 'update_user_access',
    revoke_access = 'revoke_access',
    set_owner = 'set_owner',
    view_screens = 'view_screens',
    connect_screen = 'connect_screen',
    update_screen = 'update_screen',
    clear_screen_cache = 'clear_screen_cache',
    delete_screen = 'delete_screen',
    create_playlist = 'create_playlist',
    view_playlists = 'view_playlists',
    update_playlist = 'update_playlist',
    update_playlist_content = 'update_playlist_content',
    update_playlist_screens = 'update_playlist_screens',
    delete_playlist = 'delete_playlist',
    view_scenarios = 'view_scenarios',
    create_scenario = 'create_scenario',
    update_scenario = 'update_scenario',
    delete_scenario = 'delete_scenario',
    view_content = 'view_content',
    upload_content = 'upload_content',
    update_content = 'update_content',
    delete_content = 'delete_content',
    view_locations = 'view_locations',
    create_location = 'create_location',
    update_location = 'update_location',
    delete_location = 'delete_location',
    create_webpage = 'create_webpage',
    update_webpage = 'update_webpage',
    delete_webpage = 'delete_webpage',
    view_users = 'view_users',
    view_subscriptions = 'view_subscriptions',
    create_subscription = 'create_subscription',
    update_subscription = 'update_subscription',
    delete_subscription = 'delete_subscription',

    // create_ticket = 'create_ticket',
    // update_ticket = 'update_ticket',

    create_ticket_message = 'create_ticket_message',
}

export const CHECK_PERMISSION_KEY = 'check_permission';

export const CheckPermission = (...actions: ControllerAction[]) => SetMetadata(CHECK_PERMISSION_KEY, actions);
