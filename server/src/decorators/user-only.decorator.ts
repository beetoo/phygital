import { SetMetadata } from '@nestjs/common';

export const USER_ONLY_KEY = 'user_only';

export const UserOnly = () => SetMetadata(USER_ONLY_KEY, true);
