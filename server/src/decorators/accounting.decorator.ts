import { SetMetadata } from '@nestjs/common';

export const ACCOUNTING_KEY = 'accounting';

export const Accounting = () => SetMetadata(ACCOUNTING_KEY, true);
