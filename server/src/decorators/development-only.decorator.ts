import { SetMetadata } from '@nestjs/common';

export const DEVELOPMENT_ONLY_KEY = 'development_only';

export const DevelopmentOnly = () => SetMetadata(DEVELOPMENT_ONLY_KEY, true);
