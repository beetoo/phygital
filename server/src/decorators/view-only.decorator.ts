import { SetMetadata } from '@nestjs/common';

export const VIEW_ONLY_KEY = 'view_only';

export const ViewOnly = () => SetMetadata(VIEW_ONLY_KEY, true);
