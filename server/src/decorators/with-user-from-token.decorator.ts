import { SetMetadata } from '@nestjs/common';

export const WITH_USER_FROM_TOKEN_KEY = 'with_user_from_token';

export const WithUserFromToken = () => SetMetadata(WITH_USER_FROM_TOKEN_KEY, true);
