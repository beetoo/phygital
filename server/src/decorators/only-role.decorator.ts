import { SetMetadata } from '@nestjs/common';
import { UserRoleType } from '../../../common/types';

export const ONLY_ROLE_KEY = 'only_role';

export const OnlyRole = (...roles: UserRoleType[]) => SetMetadata(ONLY_ROLE_KEY, roles);
