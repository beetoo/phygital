import { resolve } from 'node:path';
import { path as ROOT_PATH } from 'app-root-path';

export const BUILD = resolve(ROOT_PATH, 'client', 'build');
export const SOURCE = resolve(ROOT_PATH, 'client', 'src');
export const STATIC = resolve(ROOT_PATH, 'client', 'static');
