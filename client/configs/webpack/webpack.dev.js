import { merge } from 'webpack-merge';

import commonConfig from './webpack.common';
import * as modules from './modules';

export default () =>
    merge(
        commonConfig(),
        { mode: 'development', devtool: 'inline-source-map' },
        modules.loadDevTypeScript(),
        modules.loadDevCss(),
    );
