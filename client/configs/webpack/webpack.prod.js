import { merge } from 'webpack-merge';

import getCommonConfig from './webpack.common';
import * as modules from './modules';

export default () => {
    const { ANALYZE, STAGE } = process.env;

    return merge(
        getCommonConfig(),
        { mode: 'production', devtool: false },
        modules.cleanBuildDirectory(),
        modules.loadProdTypeScript(),
        modules.loadProdCss(),
        modules.optimizeBuild(),
        modules.connectBuildProgressIndicator(),
        ANALYZE ? modules.setupBuildAnalysis() : {},
        STAGE === 'sl' && modules.obfuscator(),
    );
};
