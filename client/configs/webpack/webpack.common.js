import { merge } from 'webpack-merge';

import * as modules from './modules';
import { BUILD, SOURCE } from './constants';

export default () => {
    const { NODE_ENV, STAGE } = process.env;
    const DEV = NODE_ENV === 'development';

    return merge(
        {
            entry: {
                main: SOURCE + '/index.tsx',
            },
            output: {
                path: BUILD,
                filename: DEV ? 'js/[name].bundle.js' : 'js/[name].[contenthash:5].js',
                chunkFilename: DEV ? 'js/[name].bundle.js' : 'js/[name].[contenthash:5].bundle.js',
                assetModuleFilename: DEV ? 'assets/[name][ext][query]' : 'assets/[name].[hash][ext][query]',
                publicPath: '/',
            },
            resolve: {
                extensions: ['.ts', '.tsx', '.js', '.json', '.css'],
            },
        },
        modules.loadImages(),
        modules.loadFonts(),
        modules.loadAudio(),
        modules.setupHtml(STAGE),
        modules.setupEnvVariables(),
    );
};
