import { merge } from 'webpack-merge';
// import ForkTsCheckerWebpackPlugin from 'react-dev-utils/ForkTsCheckerWebpackPlugin';

const loadTypeScript = ({ sourceMap = false } = { sourceMap: false }) =>
    merge(
        {
            module: {
                rules: [
                    {
                        test: /\.(ts|js)x?$/,
                        exclude: /node_modules/,
                        use: 'swc-loader',
                    },
                ],
            },
            // plugins: [
            //     new ForkTsCheckerWebpackPlugin({
            //         eslint: {
            //             files: '{server/src,client/src,apps,libs,test}/**/*.{ts,tsx}', // required - same as command `eslint ./src/**/*.{ts,tsx,js,jsx} --ext .ts,.tsx,.js,.jsx`
            //         },
            //     }),
            // ],
        },
        sourceMap && {
            module: {
                rules: [
                    {
                        enforce: 'pre',
                        test: /\.js$/,
                        loader: 'source-map-loader',
                    },
                ],
            },
            ignoreWarnings: [/Failed to parse source map/],
        },
    );

export const loadDevTypeScript = () => loadTypeScript({ sourceMap: true });
export const loadProdTypeScript = () => loadTypeScript({ sourceMap: false });
