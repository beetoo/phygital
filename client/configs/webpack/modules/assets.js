import HtmlWebpackPlugin from 'html-webpack-plugin';
import FaviconsWebpackPlugin from 'favicons-webpack-plugin';

import { STATIC, SOURCE } from '../constants';

export const setupHtml = (stage) => ({
    plugins: [
        new HtmlWebpackPlugin({
            title: stage === 'sl-eng' ? 'Phygital Signage' : 'Фиджитал',
            template: STATIC + '/template.html',
            templateParameters: {
                isDev: stage === 'local',
                isProd: stage === 'prod' || stage === 'dev',
                isSl: stage.startsWith('sl'),
            },
        }),
        new FaviconsWebpackPlugin({
            logo: SOURCE + '/assets/images/favicon2.svg',
            prefix: '/assets/favicons/',
            mode: stage === 'local' ? 'light' : 'webapp',
        }),
    ],
});

export const loadImages = () => ({
    module: {
        rules: [
            {
                test: /\.(ico|gif|png|jpg|jpeg|svg)$/,
                type: 'asset/resource',
                generator: {
                    filename: 'assets/images/[name].[hash][ext]',
                },
            },
        ],
    },
});

export const loadFonts = () => ({
    module: {
        rules: [
            {
                test: /\.(woff(2)?|eot|ttf|otf)$/,
                type: 'asset/resource',
                generator: {
                    filename: 'assets/fonts/[name].[hash][ext]',
                },
            },
        ],
    },
});

export const loadAudio = () => ({
    module: {
        rules: [
            {
                test: /\.(mp3|mp4|wav|ogg)$/,
                type: 'asset/resource',
                generator: {
                    filename: 'assets/media/[name].[hash][ext]',
                },
            },
        ],
    },
});
