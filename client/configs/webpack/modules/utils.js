import { DefinePlugin } from 'webpack';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import WebpackBar from 'webpackbar';
import Dotenv from 'dotenv-webpack';
import WebpackObfuscator from 'webpack-obfuscator';

export const setupEnvVariables = () => ({
    plugins: [
        new Dotenv({ path: './.env' }),
        new DefinePlugin({
            NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            STAGE: JSON.stringify(process.env.STAGE),
        }),
    ],
});

export const cleanBuildDirectory = () => ({
    plugins: [new CleanWebpackPlugin({ cleanStaleWebpackAssets: true })],
});

export const setupBuildAnalysis = () => ({
    plugins: [
        new BundleAnalyzerPlugin({
            analyzerMode: 'disabled',
            openAnalyzer: false,
            generateStatsFile: true,
        }),
    ],
});

export const connectBuildProgressIndicator = () => ({
    plugins: [new WebpackBar()],
});

export const obfuscator = () => ({
    plugins: [new WebpackObfuscator({ rotateStringArray: true, optionsPreset: 'low-obfuscation' })],
});
