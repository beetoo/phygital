import TerserPlugin from 'terser-webpack-plugin';

export const optimizeBuild = () => ({
    optimization: {
        runtimeChunk: true,
        splitChunks: {
            chunks: 'all',
        },
        minimize: true,
        minimizer: [new TerserPlugin({ extractComments: true })],
        emitOnErrors: false,
    },
    performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000,
    },
});
