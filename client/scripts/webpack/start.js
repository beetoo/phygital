import webpack from 'webpack';
import DevServer from 'webpack-dev-server';
import openBrowser from 'react-dev-utils/openBrowser';
import chalk from 'chalk';

import getDevConfig from '../../configs/webpack/webpack.dev';
import { HOST, PORT } from './constants';

const compiler = webpack(getDevConfig());

// const HOST_SERVER = '0.0.0.0';

const server = new DevServer(
    {
        historyApiFallback: true,
        host: HOST,
        port: PORT,
        hot: true,
    },
    compiler,
);

(async () => {
    await server.start();

    console.log(`${chalk.greenBright('Server listening on')} ${chalk.blueBright(`http://${HOST}:${PORT}`)}`);
    openBrowser(`http://${HOST}:${PORT}`);
})();
