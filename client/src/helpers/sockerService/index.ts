import { io, Socket } from 'socket.io-client';

import { baseURL } from '../httpService/baseUrl';
import { isDevelopmentMode } from '../../constants/environments';

export class SocketService {
    static socket: Socket | null = null;

    static createConnection(organizationId: string): void {
        this.socket = io(baseURL.replace('/api', ''), { path: '/ws', withCredentials: true, auth: { organizationId } });

        this.socket.on('connect', () => {
            if (isDevelopmentMode) {
                console.log('connected');
            }
        });

        this.socket.on('disconnect', (err) => {
            if (isDevelopmentMode) {
                console.log('disconnected', err);
            }
        });
    }
}
