import axios, { AxiosInstance, InternalAxiosRequestConfig, RawAxiosRequestConfig } from 'axios';
import { AuthResponse, CancellableResponse, SuccessResponse } from '../../types';
import {
    OrganizationType,
    PlaylistType,
    FolderType,
    ScreenTagType,
    LocationType,
    ContentType,
    ScreenType,
    PlaylistItemType,
    PlaylistScheduleType,
    AnalysisVideoType,
} from '../../../../common/types';
import { DataSuggestion } from '../../resolvers/screensResolvers';
import { ScreenLogsClientType } from '../../@pages/AdminPage/types';
import { baseURL } from './baseUrl';

type Body =
    | {
          [key: string]: string | number | unknown;
      }
    | FormData
    | string;

export type HttpServiceResponse =
    | SuccessResponse
    | AuthResponse
    | OrganizationType
    | OrganizationType[]
    | ScreenType
    | ScreenTagType[]
    | ContentType[]
    | PlaylistType[]
    | PlaylistItemType[]
    | LocationType[]
    | { locationId: string }
    | FolderType[]
    | CancellableResponse
    | PlaylistScheduleType[]
    | ScreenLogsClientType[]
    | { id: string; name: string }
    | { id: string | string[] }
    | DataSuggestion[]
    | AnalysisVideoType[];

export const isCancellableResponse = (response: HttpServiceResponse): response is CancellableResponse =>
    'cancelId' in response;

class HttpService {
    private axiosInstance: AxiosInstance;

    constructor() {
        this.axiosInstance = axios.create({
            baseURL,
            withCredentials: true,
        });

        this.axiosInstance.interceptors.request.use((config: InternalAxiosRequestConfig) => {
            config.headers.set('authorization', 'Bearer ' + localStorage.getItem('token'));
            return config;
        });
    }

    async get(url: string, config?: RawAxiosRequestConfig): Promise<HttpServiceResponse> {
        try {
            const response = await this.axiosInstance.get(url, config);
            return response?.data;
        } catch (err) {
            throw err.response?.data;
        }
    }

    async post(url: string, body: Body, config?: RawAxiosRequestConfig): Promise<HttpServiceResponse> {
        try {
            const response = await this.axiosInstance.post(url, body, config);
            return response?.data;
        } catch (err) {
            throw err.response?.data;
        }
    }

    async put(url: string, body: Body, config?: RawAxiosRequestConfig): Promise<HttpServiceResponse> {
        try {
            const response = await this.axiosInstance.put(url, body, config);
            return response?.data;
        } catch (err) {
            throw err.response?.data;
        }
    }

    async patch(url: string, body: Body, config?: RawAxiosRequestConfig): Promise<HttpServiceResponse> {
        try {
            const response = await this.axiosInstance.patch(url, body, config);
            return response?.data;
        } catch (err) {
            throw err.response?.data;
        }
    }

    async delete(url: string, config?: RawAxiosRequestConfig): Promise<HttpServiceResponse> {
        try {
            const response = await this.axiosInstance.delete(url, config);
            return response?.data;
        } catch (err) {
            throw err.response?.data;
        }
    }
}

export const httpService = new HttpService();
