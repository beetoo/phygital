import {
    isDevProductionClient,
    isProductionClient,
    isProductionEngClient,
    isSLClient,
} from '../../constants/environments';

const SL_SERVER_API_URL = (window as any)?.globalConfig?.SL_SERVER_API_URL;

export const baseURL =
    (isProductionClient
        ? process.env.PROD_SERVER_API_URL
        : isDevProductionClient
          ? process.env.DEV_PROD_SERVER_API_URL
          : isProductionEngClient || isSLClient
            ? SL_SERVER_API_URL
            : process.env.DEV_SERVER_API_URL) ?? 'https://cloud.phygitalsignage.io/api';
