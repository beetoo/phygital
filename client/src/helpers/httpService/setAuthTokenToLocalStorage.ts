import { AuthResponse } from '../../types';

export const setAuthTokenToLocalStorage = (response: AuthResponse): void => {
    if (response.token) {
        localStorage.setItem('token', response.token);
    } else {
        localStorage.removeItem('token');
    }
};
