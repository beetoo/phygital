type TaskPromise = (value?: unknown) => Promise<unknown>;

class PromiseQueue {
    private total: number;
    private todo: Map<string, TaskPromise>;
    private running: TaskPromise[];
    private readonly count: number;

    constructor(concurrentCount = 1) {
        this.todo = new Map();
        this.total = this.todo.size;
        this.running = [];
        this.count = concurrentCount;
    }

    private runNext() {
        return this.todo.size > 0 && this.running.length < this.count;
    }

    run() {
        if (this.runNext()) {
            // берем первый промис id из списка
            const uploadableFileId = [...this.todo.keys()].shift();
            const promise = this.todo.get(uploadableFileId as string) as TaskPromise;
            this.todo.delete(uploadableFileId as string);
            // и добавляем его в список запущенных
            this.running.push(promise);
            // запускаем его
            promise().then(() => {
                // Убираем его из списка запущенных
                this.running.shift();
                // повторяем функцию run
                this.run();
            });
        }
    }

    add(promiseIds: string[], promise: (val?: unknown) => TaskPromise) {
        // добавляем промис в список, если его там нет
        for (const id of promiseIds) {
            if (!this.todo.has(id)) {
                this.todo.set(id, promise(id));
            }
        }
    }

    delete(promiseId: string) {
        // удаляем промис из списка
        this.todo.delete(promiseId);
    }
}

export const promiseQueue = new PromiseQueue();
