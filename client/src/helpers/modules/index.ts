export const getPhosphorIconsModule = async (): Promise<any> => {
    return await (eval(`import('@phosphor-icons/react')`) as Promise<{ fileTypeFromFile: any }>);
};
