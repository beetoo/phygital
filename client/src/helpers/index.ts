import { addDays, differenceInDays, format, isBefore, isEqual } from 'date-fns';
import sortBy from 'lodash/sortBy';
import { AppointmentModel } from '@devexpress/dx-react-scheduler';
import { RRule, Weekday } from 'rrule';
import { NumberFormatValues } from 'react-number-format';

import { CreatePlaylistScheduleDto } from '../../../server/src/models/playlist-schedule/dto/create-playlist-schedule.dto';
import { PlaylistItemWithScenario } from '../@pages/PlaylistsPage/components/PlaylistStepTwo/hooks/useStepTwoPlaylistItems';
import { RESOLUTIONS } from '../constants';
import { OrientationTypeEng, OrientationRusType } from '../@pages/ScreensPage/types';
import { frequencyFromByDay, getContentFileInfo, withZero } from '../../../common/helpers';
import {
    PlaylistItemType,
    ScreenType,
    TakeSkipAndSearchType,
    CONTENT_TYPE,
    OrientationEnum,
} from '../../../common/types';

export const getImageOrVideoDimensionAndDuration = (
    file: File,
): Promise<{ width: number; height: number; duration: number }> =>
    new Promise((resolve, reject) => {
        const { type } = getContentFileInfo(file.name);

        if (type === CONTENT_TYPE.IMAGE) {
            const img = new Image();
            img.src = window.URL.createObjectURL(file);

            img.onload = () => {
                resolve({ width: img.width, height: img.height, duration: 0 });
            };

            img.onerror = () => {
                reject(null);
            };
        } else if (type === CONTENT_TYPE.VIDEO) {
            const url = window.URL.createObjectURL(file);
            const video = document.createElement('video');
            video.src = url;

            video.onloadedmetadata = () => {
                resolve({ width: video.videoWidth, height: video.videoHeight, duration: video?.duration });
            };

            video.onerror = () => {
                reject(null);
            };
        }
    });

export const screenTimeValueToString = (
    screen: ScreenType | undefined,
): { screenStartTime: string; screenEndTime: string } => {
    const startTimeHours = withZero(screen?.startTime.hours ?? 0);
    const startTimeMinutes = withZero(screen?.startTime.minutes ?? 0);
    const endTimeHours = withZero(screen?.endTime.hours ?? 0);
    const endTimeMinutes = withZero(screen?.endTime.minutes ?? 0);

    const screenStartTime = `${startTimeHours}:${startTimeMinutes}`;
    const screenEndTime = `${endTimeHours}:${endTimeMinutes}`;

    return {
        screenStartTime,
        screenEndTime,
    };
};

export const startTimeStringMoreThanEndTimeString = (startTime: string, endTime: string): boolean => {
    const startTimeHours = Number(startTime.slice(0, 2)) || 0;
    const startTimeMinutes = Number(startTime.slice(3)) || 0;
    const endTimeHours = Number(endTime.slice(0, 2)) || 0;
    const endTimeMinutes = Number(endTime.slice(3)) || 0;
    return endTimeHours < startTimeHours || (endTimeHours === startTimeHours && endTimeMinutes <= startTimeMinutes);
};

export const addTimeFromAnotherDate = (initialDay: string, endTime: string): Date => {
    const initialDate = toDate(initialDay, '00:00');
    const updatedDate = toDate(initialDay, endTime);

    if (isBefore(updatedDate, initialDate) || isEqual(updatedDate, initialDate)) {
        return addDays(updatedDate, 1);
    }

    return updatedDate;
};

type Props = {
    playlistDtoItems: PlaylistItemType[];
    from: number;
    to: number;
};

export const sortAndIndexItems = (
    items: (PlaylistItemWithScenario | PlaylistItemType)[],
): (PlaylistItemWithScenario | PlaylistItemType)[] =>
    sortBy(items, (o) => o.orderIndex).map(({ ...rest }, index) => ({
        ...rest,
        orderIndex: index,
    }));

export const getPlaylistDtoItemsWithScenarios = (playlistDtoItems: PlaylistItemType[]): PlaylistItemWithScenario[] => {
    let scenarioId = '';
    let tmp = {} as PlaylistItemWithScenario;
    let orderIndex = 0;
    return playlistDtoItems.reduce((acc: PlaylistItemWithScenario[], curr: PlaylistItemType) => {
        if (!curr.scenario) {
            orderIndex++;
            acc.push(curr);
        } else {
            if (scenarioId !== curr.scenario.id) {
                tmp = { orderIndex, scenario: curr.scenario, scenarioId: curr.scenario.id, playlistItems: [curr] };
                acc.push(tmp);
                orderIndex++;
            } else {
                acc = acc.map((item) =>
                    item.scenario?.id === curr.scenario?.id
                        ? {
                              ...item,
                              scenarioId: curr.scenario?.id ?? null,
                              playlistItems: [...('playlistItems' in item ? item.playlistItems : []), curr],
                          }
                        : item,
                );
            }
            scenarioId = curr.scenario.id;
        }
        return acc;
    }, []);
};

export const flatPlaylistItemsWithScenarios = (items: PlaylistItemWithScenario[]): PlaylistItemType[] =>
    items
        .flatMap((item) => ('playlistItems' in item ? [...item.playlistItems] : item))
        .map((item, orderIndex) => ({ ...item, orderIndex }));

export const getReorderedItems = ({ playlistDtoItems, from, to }: Props): PlaylistItemType[] => {
    // обновляем все orderIndex от 0 до последнего индекса
    const formattedPlaylistDtoItems = sortAndIndexItems(playlistDtoItems) as PlaylistItemType[];

    const lastPartFrom = from > to ? from : to;
    const firstPartTo = from > to ? to : from;

    const firstPart = formattedPlaylistDtoItems.filter(({ orderIndex }) => orderIndex < firstPartTo);

    const reorderedItems = formattedPlaylistDtoItems
        .filter(({ orderIndex }) =>
            from < to ? orderIndex <= to && orderIndex >= from : orderIndex >= to && orderIndex <= from,
        )
        .map(({ id, orderIndex, ...rest }) =>
            orderIndex === from
                ? { id, orderIndex: to, ...rest }
                : { id, orderIndex: orderIndex + (from < to ? -1 : 1), ...rest },
        );

    const lastPart = formattedPlaylistDtoItems.filter(({ orderIndex }) => orderIndex > lastPartFrom);

    return sortBy([...firstPart, ...reorderedItems, ...lastPart], (o) => o.orderIndex);
};

export const getReorderedItemsWithScenarios = ({ playlistDtoItems, from, to }: Props): PlaylistItemType[] => {
    // обновляем все orderIndex от 0 до последнего индекса
    const formattedPlaylistDtoItems = sortAndIndexItems(playlistDtoItems);

    const playlistItemsWithScenarios = sortAndIndexItems(
        getPlaylistDtoItemsWithScenarios(formattedPlaylistDtoItems as PlaylistItemType[]),
    );

    const mapCallbackNew = ({ orderIndex, ...rest }: PlaylistItemType) =>
        orderIndex === from ? { orderIndex: to, ...rest } : { orderIndex: orderIndex + (from < to ? -1 : 1), ...rest };

    const lastPartFrom = from > to ? from : to;
    const firstPartTo = from > to ? to : from;

    const firstPart = playlistItemsWithScenarios.filter(({ orderIndex }) => orderIndex < firstPartTo);

    const reorderedItems = playlistItemsWithScenarios
        .filter(({ orderIndex }) =>
            from < to ? orderIndex <= to && orderIndex >= from : orderIndex >= to && orderIndex <= from,
        )
        .map(mapCallbackNew);

    const lastPart = playlistItemsWithScenarios.filter(({ orderIndex }) => orderIndex > lastPartFrom);

    const reorderedPlaylistItemsWithScenarios = sortBy(
        [...firstPart, ...reorderedItems, ...lastPart],
        (o) => o.orderIndex,
    );

    return flatPlaylistItemsWithScenarios(reorderedPlaylistItemsWithScenarios);
};

export const getAspectRatio = (width: number, height: number): { widthRatio: number; heightRatio: number } => {
    const getHighestDividableNumber = (width: number, height: number): number => {
        if (height === 0) {
            return width;
        }

        return getHighestDividableNumber(height, width % height);
    };

    const dividableNumber = getHighestDividableNumber(width, height);

    let widthRatio = width / dividableNumber;
    let heightRatio = height / dividableNumber;

    if (widthRatio === 8) {
        widthRatio *= 2;
        heightRatio *= 2;
    }

    return { widthRatio, heightRatio };
};

export const getAspectRatioString = (width: number, height: number): string => {
    const { widthRatio, heightRatio } = getAspectRatio(width, height);

    return `${widthRatio}:${heightRatio}`;
};

export const getAspectRatioWidthAndHeight = (
    width: number,
    height: number,
    { maxWidth, maxHeight, isResizing }: { maxWidth: number; maxHeight: number; isResizing: boolean },
): { width: number | undefined; height: number | undefined } => {
    const { widthRatio, heightRatio } = getAspectRatio(width, height);

    const ratio = widthRatio / heightRatio;

    const areaWidth = maxHeight * ratio <= maxWidth ? Math.trunc(maxHeight * ratio) : maxWidth;
    const areaHeight = Math.trunc(areaWidth / ratio);

    return {
        width: areaWidth === 0 ? undefined : areaWidth,
        height: isResizing || areaHeight === 0 ? undefined : areaHeight,
    };
};

export const getTakeAndSkip = (rowsOnPage: number, currentPage: number): TakeSkipAndSearchType => ({
    skip: rowsOnPage * (currentPage - 1),
    take: rowsOnPage,
});

const fromSelectToDbOrientationValue = (orientation: OrientationRusType | OrientationTypeEng): OrientationEnum => {
    switch (orientation) {
        case 'Не поворачивать':
        case "Don't turn":
            return OrientationEnum.LANDSCAPE;
        case '90°':
            return OrientationEnum.PORTRAIT;
        case '180°':
            return OrientationEnum['LANDSCAPE_FLIPPED'];
        case '270°':
            return OrientationEnum['PORTRAIT_FLIPPED'];
        default:
            return OrientationEnum.LANDSCAPE;
    }
};

type ResolutionAndOrientationProps = {
    activeScreenResolution?: { width: number; height: number };
    screenResolution: string;
    customResolutionWidth: string;
    customResolutionHeight: string;
    screenOrientation: string;
};

export const getResolutionAndOrientation = ({
    activeScreenResolution,
    screenResolution,
    customResolutionWidth,
    customResolutionHeight,
    screenOrientation,
}: ResolutionAndOrientationProps): { resolution: { width: number; height: number }; orientation: OrientationEnum } => {
    let width = activeScreenResolution?.width ?? 1920,
        height = activeScreenResolution?.height ?? 1080;
    if (screenResolution === RESOLUTIONS.FULL_HD) {
        width = 1920;
        height = 1080;
    } else if (screenResolution === RESOLUTIONS.HD) {
        width = 1280;
        height = 720;
    } else if (screenResolution === RESOLUTIONS.UHD) {
        width = 3840;
        height = 2160;
    } else if (+customResolutionWidth > 0 && +customResolutionHeight > 0) {
        width = +customResolutionWidth;
        height = +customResolutionHeight;
    }

    const resolution = { width, height };

    const orientation = fromSelectToDbOrientationValue(screenOrientation as OrientationRusType);

    return { resolution, orientation };
};

export const toDate = (day: string | undefined, time: string | undefined): Date => {
    try {
        return day && time ? new Date(`${day}T${time}`) : new Date();
    } catch {
        return new Date();
    }
};

const byDayToRRule = (byDay: string) =>
    byDay === ''
        ? []
        : (byDay.split(',').map((day) => {
              if (day === 'MO') {
                  return RRule.MO;
              }
              if (day === 'TU') {
                  return RRule.TU;
              }
              if (day === 'WE') {
                  return RRule.WE;
              }
              if (day === 'TH') {
                  return RRule.TH;
              }
              if (day === 'FR') {
                  return RRule.FR;
              }
              if (day === 'SA') {
                  return RRule.SA;
              }
              if (day === 'SU') {
                  return RRule.SU;
              }
          }) as Weekday[]);

const freqToRRule = (frequency: string) => {
    switch (frequency) {
        case 'DAILY':
            return RRule.DAILY;
        case 'WEEKLY':
            return RRule.WEEKLY;
        default:
            return RRule.DAILY;
    }
};

export const startTimeMoreThanEndTime = (startDate: Date, endDate: Date): boolean => {
    const startTimeHours = Number(format(startDate, 'HH')) || 0;
    const startTimeMinutes = Number(format(startDate, 'mm')) || 0;
    const endTimeHours = Number(format(endDate, 'HH')) || 0;
    const endTimeMinutes = Number(format(endDate, 'mm')) || 0;
    return endTimeHours < startTimeHours || (endTimeHours === startTimeHours && endTimeMinutes <= startTimeMinutes);
};

const toRRule = (byDay: string, startDate: Date, endDate: Date, broadcastAlways: boolean) => {
    const isStartTimeMoreThanEndTime = startTimeMoreThanEndTime(startDate, endDate);
    const startDay = new Date(format(startDate, 'yyyy-MM-dd'));
    const endDay = broadcastAlways
        ? null
        : addDays(new Date(format(endDate, 'yyyy-MM-dd')), isStartTimeMoreThanEndTime ? 0 : 1);
    const count = endDay ? differenceInDays(endDay, startDay) : null;

    const rule = new RRule({
        freq: freqToRRule(frequencyFromByDay(byDay)),
        count,
        // interval: 5,
        byweekday: byDayToRRule(byDay),
        dtstart: startDay,
        until: endDay,
    });
    return rule.toString();
};

export const schedulesToAppointments = (schedules: CreatePlaylistScheduleDto[]): AppointmentModel[] =>
    schedules.map(({ startDay, endDay, startTime, endTime, broadcastAlways, byDay, ...rest }) => {
        const startDate = toDate(startDay, startTime);
        const endDate = toDate(endDay, endTime);

        const rRule = toRRule(byDay, startDate, endDate, broadcastAlways);

        return {
            ...rest,
            startDate,
            endDate: addTimeFromAnotherDate(startDay, endTime),
            rRule,
        };
    });

export const isTimeAllowed = ({ value, floatValue }: NumberFormatValues): boolean => {
    // max = 24h 00m 00s
    if (floatValue !== undefined) {
        if (value.length === 1 && floatValue > 2) {
            return false;
        }
        if (value.length === 2 && floatValue > 23) {
            return false;
        }
        if (value.length === 3 && floatValue > 235) {
            return false;
        }
        if (value.length === 4 && floatValue > 2359) {
            return false;
        }
        if (value.length === 5 && floatValue > 23595) {
            return false;
        }
        if (value.length === 6 && (floatValue > 235959 || floatValue < 1)) {
            return false;
        }
    }

    return true;
};
