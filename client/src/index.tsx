import React from 'react';
import { createRoot } from 'react-dom/client';

import App from './App';

import { isDevProductionClient } from './constants/environments';

import './theme/init.css';
import './utils/i18n';

if (isDevProductionClient) {
    const head = document.querySelector('head');
    const meta = document.createElement('meta');
    meta.name = 'robots';
    meta.content = 'noindex';
    head?.appendChild(meta);
}

const container = document.getElementById('app') as HTMLElement;
const root = createRoot(container);
root.render(<App />);
