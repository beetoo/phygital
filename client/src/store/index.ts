import { createStore, applyMiddleware, Store } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { composeWithDevToolsDevelopmentOnly } from '@redux-devtools/extension';
import { rootReducer } from '../reducers';
import { rootEpic } from '../epics';

const epicMiddleware = createEpicMiddleware();

const composeEnhancers = composeWithDevToolsDevelopmentOnly({});

export default (function (): Store {
    const store = createStore(rootReducer, composeEnhancers(applyMiddleware(epicMiddleware)));

    epicMiddleware.run(rootEpic);

    return store;
})();
