import { useCallback, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectActivePlaylistId, selectPlaylistDto, selectPlaylists } from '../@pages/PlaylistsPage/selectors';
import { usePlaylistStep } from '../@pages/PlaylistsPage/hooks/usePlaylistStep';
import { useCustomRouter } from './useCustomRouter';

type ReturnValue = {
    isPlaylistStepModeEnabled: boolean;
    isCreatePlaylistStep: boolean;
    isPlaylistStepOne: boolean;
    isPlaylistStepTwo: boolean;
    isPlaylistStepThree: boolean;

    playlistTitle: string;
    isThirdStepEnabled: boolean;

    moveToPlaylistStepOne: () => void;
    moveToPlaylistStepTwo: () => void;
    moveToPlaylistStepThree: () => void;

    t: TFunction;
};

export const useHeaderPlaylistStepMode = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'header' });

    const { pushToPage } = useCustomRouter();

    const { playlistStep, isPlaylistStepModeEnabled } = usePlaylistStep();

    const activePlaylistId = useSelector(selectActivePlaylistId);
    const playlists = useSelector(selectPlaylists);
    const { playlistDtoInfo, playlistDtoItems } = useSelector(selectPlaylistDto);

    const isCreatePlaylistStep = playlistStep.includes('creating');

    const isPlaylistStepOne = playlistStep.includes('one');
    const isPlaylistStepTwo = playlistStep.includes('two');
    const isPlaylistStepThree = playlistStep.includes('three');

    const isThirdStepEnabled = playlistDtoItems.length > 0;

    const playlistTitle = useMemo(
        () =>
            isCreatePlaylistStep
                ? playlistDtoInfo.title
                : (playlists.find(({ id }) => id === activePlaylistId)?.title ?? ''),
        [activePlaylistId, isCreatePlaylistStep, playlistDtoInfo.title, playlists],
    );

    const moveToPlaylistStepOne = useCallback(() => {
        if (!isPlaylistStepOne) {
            pushToPage(isCreatePlaylistStep ? 'playlists/creating_one' : 'playlists/editing_one');
        }
    }, [isCreatePlaylistStep, isPlaylistStepOne, pushToPage]);

    const moveToPlaylistStepTwo = useCallback(() => {
        pushToPage(isCreatePlaylistStep ? 'playlists/creating_two' : 'playlists/editing_two');
    }, [isCreatePlaylistStep, pushToPage]);

    const moveToPlaylistStepThree = useCallback(() => {
        if (isThirdStepEnabled) {
            pushToPage(isCreatePlaylistStep ? 'playlists/creating_three' : 'playlists/editing_three');
        }
    }, [isCreatePlaylistStep, isThirdStepEnabled, pushToPage]);

    return {
        isPlaylistStepModeEnabled,
        isCreatePlaylistStep,
        isPlaylistStepOne,
        isPlaylistStepTwo,
        isPlaylistStepThree,

        playlistTitle,
        isThirdStepEnabled,

        moveToPlaylistStepOne,
        moveToPlaylistStepTwo,
        moveToPlaylistStepThree,

        t,
    };
};
