import { ChangeEvent, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { getScreensInfo } from '../@pages/ScreensPage/helpers';
import { setShowModal } from '../components/ModalsContainer/actions';
import { MODALS } from '../components/ModalsContainer/types';
import { selectActiveLocation, selectUpdatingLocationStatus } from '../@pages/ScreensPage/selectors';
import { setUpdateLocation } from '../@pages/ScreensPage/actions';
import { selectAuthUserRole } from '../selectors';
import { USER_ROLE } from '../constants';
import { isInvalidLatitude, isInvalidLongitude } from '../../../common/helpers';

type ReturnValue = {
    screensNumber: number;
    locationName: string;
    locationAddress: string;
    geo_lat: string;
    geo_lon: string;
    userRole: USER_ROLE;
    onLocationNameChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onLocationAddressChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onLocationLatitudeChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onLocationLongitudeChange: (e: ChangeEvent<HTMLInputElement>) => void;
    latitudeError: string;
    longitudeError: string;
    tzLabel: string;
    onTimezoneChange: (event: ChangeEvent<HTMLInputElement>) => void;
    handleSubmit: () => void;
    isLocationSubmitDisabled: boolean;
    isSubmitButtonShown: boolean;
    openDeleteLocationAlert: () => void;
    t: TFunction;
};

export const useLocationMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'screens' });

    const dispatch = useDispatch();

    const userRole = useSelector(selectAuthUserRole);
    const updatingLocation = useSelector(selectUpdatingLocationStatus);
    const {
        id: activeLocationId,
        name: activeLocationName,
        address: activeLocationAddress,
        geo_lat: activeGeo_lat,
        geo_lon: activeGeo_lon,
        tzLabel: activeTzLabel,
        screens,
    } = useSelector(selectActiveLocation);

    const [locationName, setLocationName] = useState(activeLocationName);
    const [locationAddress, setLocationAddress] = useState(activeLocationAddress);
    const [geo_lat, setGeo_lat] = useState(activeGeo_lat);
    const [geo_lon, setGeo_lon] = useState(activeGeo_lon);
    const [tzLabel, setTzLabel] = useState('');

    const [latitudeError, setLatitudeError] = useState('');
    const [longitudeError, setLongitudeError] = useState('');

    const [isSubmitButtonShown, setIsSubmitButtonShown] = useState(false);

    useEffect(() => {
        setLocationName(activeLocationName);
        setLocationAddress(activeLocationAddress);
        setGeo_lat(activeGeo_lat);
        setGeo_lon(activeGeo_lon);
        setTzLabel(activeTzLabel);
    }, [activeGeo_lat, activeGeo_lon, activeLocationAddress, activeLocationName, activeTzLabel]);

    const onLocationNameChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setLocationName(value);
    }, []);

    const onLocationAddressChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setLocationAddress(value);
    }, []);

    const onLocationLatitudeChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            setLatitudeError(isInvalidLatitude(value) ? t('invalidCoordinates') : '');

            setGeo_lat(value);
        },
        [t],
    );

    const onLocationLongitudeChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            setLongitudeError(isInvalidLongitude(value) ? t('invalidCoordinates') : '');

            setGeo_lon(value);
        },
        [t],
    );

    const onTimezoneChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setTzLabel(value);
    }, []);

    const handleSubmit = useCallback(() => {
        const updateLocationDto = {
            locationId: activeLocationId,
            name: locationName,
            address: locationAddress,
            geo_lat,
            geo_lon,
            tzLabel,
        };

        dispatch(setUpdateLocation(updateLocationDto));
    }, [activeLocationId, dispatch, geo_lat, geo_lon, locationAddress, locationName, tzLabel]);

    const openDeleteLocationAlert = useCallback(() => {
        dispatch(setShowModal(MODALS.DELETE_LOCATION));
    }, [dispatch]);

    const { screensNumber } = getScreensInfo(screens);

    const isLocationSubmitDisabled = useMemo(
        () =>
            updatingLocation ||
            Boolean(latitudeError) ||
            Boolean(longitudeError) ||
            (geo_lat && !geo_lon) ||
            (geo_lon && !geo_lat) ||
            locationName.trim() === '' ||
            (locationName.trim() === activeLocationName &&
                locationAddress.trim() === activeLocationAddress.trim() &&
                geo_lat.trim() === activeGeo_lat &&
                geo_lon.trim() === activeGeo_lon &&
                tzLabel.trim() === activeTzLabel),
        [
            activeGeo_lat,
            activeGeo_lon,
            activeLocationAddress,
            activeLocationName,
            activeTzLabel,
            geo_lat,
            geo_lon,
            latitudeError,
            locationAddress,
            locationName,
            longitudeError,
            tzLabel,
            updatingLocation,
            userRole,
        ],
    );

    const prevLocationId = useRef(activeLocationId);
    useEffect(() => {
        if (prevLocationId.current !== activeLocationId) {
            setIsSubmitButtonShown(false);

            prevLocationId.current = activeLocationId;
        } else {
            setTimeout(() => {
                setIsSubmitButtonShown(!isLocationSubmitDisabled);
            }, 0);
        }
    }, [activeLocationId, isLocationSubmitDisabled]);

    return {
        screensNumber,
        locationName,
        locationAddress,
        geo_lat,
        geo_lon,
        userRole,
        onLocationNameChange,
        onLocationAddressChange,
        onLocationLatitudeChange,
        onLocationLongitudeChange,
        latitudeError,
        longitudeError,
        tzLabel,
        onTimezoneChange,
        handleSubmit,
        isLocationSubmitDisabled,
        isSubmitButtonShown,
        openDeleteLocationAlert,
        t,
    };
};
