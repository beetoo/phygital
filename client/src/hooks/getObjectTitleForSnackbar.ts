export const getObjectTitleForSnackbar = (_title: string): string => {
    if (!_title) return '';

    const title = _title.trim();
    return title.length <= 20 ? title : `${title.substr(0, 20)}...`.trim();
};
