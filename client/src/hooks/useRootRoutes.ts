import { useEffect, useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { selectAuthorizationStatus, selectAuthStatus, selectAuthUserRole } from '../selectors';
import { selectProfileTab } from '../@pages/ProfilePage/selectors';
import { setProfileTab } from '../@pages/ProfilePage/actions';
import { ProfileTabType } from '../@pages/ProfilePage/types';
import { USER_ROLE } from '../constants';

type ReturnValue = {
    isAuth: boolean;
    isAdminOrManager: boolean;
    isNotConfirmedEmail: boolean;
    initialPath: string;
    defaultPage: string;
};

export const useRootRoutes = (): ReturnValue => {
    const dispatch = useDispatch();

    const isAuth = useSelector(selectAuthStatus);
    const userRole = useSelector(selectAuthUserRole);
    const isNotConfirmedEmail = useSelector(selectAuthorizationStatus) !== 'active';
    const isAdminOrManager = userRole === USER_ROLE.ADMIN || userRole === USER_ROLE.MANAGER;
    const profileTab = useSelector(selectProfileTab);

    const { pathname } = useLocation();

    const initialPath = isAuth ? (isAdminOrManager ? `/organizations` : '/main') : '/signin';

    const defaultPage = useMemo(() => {
        const lastPage = localStorage.getItem('lastPage') ?? 'main';
        if (lastPage?.startsWith('profile')) {
            return 'profile';
        } else {
            return lastPage;
        }
    }, []);

    useEffect(() => {
        const lastPage = localStorage.getItem('lastPage');
        if (lastPage?.startsWith('profile')) {
            dispatch(setProfileTab(lastPage.replace('profile:', '') as ProfileTabType));
        }
    }, [dispatch]);

    useEffect(() => {
        if (
            !isAdminOrManager &&
            isAuth &&
            !(pathname.endsWith('signin') || pathname.endsWith('signup') || pathname.includes('confirm-registration'))
        ) {
            const split = pathname.split('/');
            const page = split[split.length - 1];
            if (page.endsWith('profile')) {
                localStorage.setItem('lastPage', `${page}:${profileTab}`);
            } else {
                localStorage.setItem('lastPage', page);
            }
        }
    }, [isAdminOrManager, isAuth, pathname, profileTab]);

    return {
        isAuth,
        isAdminOrManager,
        isNotConfirmedEmail,
        initialPath,
        defaultPage,
    };
};
