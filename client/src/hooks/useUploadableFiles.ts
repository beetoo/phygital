import { useEffect, useMemo, useRef, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { isEqual } from 'lodash';

import { selectUploadableFiles } from '../components/ModalsContainer/selectors';
import { getFilesizeString } from '../@pages/PlaylistsPage/helpers';
import { UploadableFile } from '../components/ModalsContainer/types';
import { setUploadableFiles } from '../components/ModalsContainer/actions';
import { promiseQueue } from '../helpers/PromiseQueue';
import { CONTENT_UPLOAD_WARNINGS } from '../@pages/ContentPage/constants';
import { CONTENT_TYPE } from '../../../common/types';
import { getContentFileInfo, formatSecondsToTimeString } from '../../../common/helpers';
import { useCustomRouter } from './useCustomRouter';

export type IterableUploadableFile = {
    id: string;
    name: string;
    size: string;
    type: CONTENT_TYPE;
    duration?: string;
    src?: string;
    progress: number;
    warning?: CONTENT_UPLOAD_WARNINGS;
    done: boolean;
    error: boolean;
    part?: number;
};

type ReturnValue = {
    uploadableFiles: UploadableFile[];
    iterableUploadableFiles: IterableUploadableFile[];
    uploadableFilesWithoutWarnings: IterableUploadableFile[];
    isMoreThanOneUploadableFile: boolean;
    deleteFile: (uploadableFileId: string) => () => void;
};

export const useUploadableFiles = (): ReturnValue => {
    const dispatch = useDispatch();

    const { currentPage } = useCustomRouter();

    const uploadableFiles = useSelector(selectUploadableFiles);
    const prevIterableFiles = useRef<IterableUploadableFile[]>([]);

    const iterableUploadableFiles: IterableUploadableFile[] = useMemo(() => {
        return uploadableFiles.map(({ id, file, duration, progress, warning, done, error }) => {
            const iterableUploadableFile: IterableUploadableFile | undefined = prevIterableFiles.current.find(
                (file: IterableUploadableFile) => file.id === id,
            );

            if (iterableUploadableFile) {
                return { ...iterableUploadableFile, progress, done, error };
            }

            const { type } = getContentFileInfo(file.name);
            const item = {
                id,
                name: file.name,
                size: getFilesizeString(file.size),
                type,
                progress,
                warning,
                done,
                error,
            };

            return currentPage === 'uploaded_videos'
                ? { ...item, duration: formatSecondsToTimeString(duration as number) }
                : { ...item, src: window.URL.createObjectURL(file) };
        });
    }, [currentPage, uploadableFiles]);

    const uploadableFilesWithoutWarnings: IterableUploadableFile[] = useMemo(
        () => iterableUploadableFiles.filter(({ warning }) => !warning),
        [iterableUploadableFiles],
    );

    const deleteFile = useCallback(
        (uploadableFileId: string) => () => {
            promiseQueue.delete(uploadableFileId);
            dispatch(setUploadableFiles(uploadableFiles.filter(({ id }) => uploadableFileId !== id)));
        },
        [dispatch, uploadableFiles],
    );

    useEffect(() => {
        if (!isEqual(iterableUploadableFiles, prevIterableFiles.current)) {
            prevIterableFiles.current = iterableUploadableFiles;
        }
    }, [iterableUploadableFiles]);

    const isMoreThanOneUploadableFile = uploadableFiles.length > 0;

    return {
        uploadableFiles,
        iterableUploadableFiles,
        uploadableFilesWithoutWarnings,
        isMoreThanOneUploadableFile,
        deleteFile,
    };
};
