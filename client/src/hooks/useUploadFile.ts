import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AxiosProgressEvent } from 'axios';

import { setStartUploadContentItems } from '../@pages/ContentPage/actions';
import { httpService } from '../helpers/httpService';
import { setUploadProgress } from '../components/ModalsContainer/actions';
import { selectCurrentOrganization } from '../@pages/ProfilePage/selectors';
import { selectActiveFolderId, selectUploadingContentStatus } from '../@pages/ContentPage/selectors';
import { selectUploadableFiles } from '../components/ModalsContainer/selectors';

type ReturnValue = {
    uploadFile: (uploadableFileId: string) => () => Promise<void>;
};

export const useUploadFile = (): ReturnValue => {
    const dispatch = useDispatch();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const activeFolderId = useSelector(selectActiveFolderId);
    const uploadableFiles = useSelector(selectUploadableFiles);
    const isUploadingContent = useSelector(selectUploadingContentStatus);

    const uploadFile = useCallback(
        (uploadableFileId: string) => async () => {
            if (!isUploadingContent) {
                dispatch(setStartUploadContentItems(true));
            }

            for (const { id, file, dimensions, duration } of uploadableFiles.filter(
                ({ id, warning }) => !warning && uploadableFileId === id,
            )) {
                const formData = new FormData();

                formData.append('organizationId', organizationId);
                formData.append('folderId', activeFolderId);
                formData.append('uploadedInfo', JSON.stringify({ dimensions, duration }));
                formData.append('files', file);

                try {
                    await httpService.post('/content', formData, {
                        headers: { 'Content-Type': 'multipart/form-data' },
                        maxRate: [
                            100 * 1024, // 100KB/s upload limit,
                            100 * 1024, // 100KB/s download limit
                        ],
                        onUploadProgress: (progressEvent: AxiosProgressEvent) => {
                            if (progressEvent.total) {
                                const progress = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                                dispatch(setUploadProgress({ id, progress, error: false, done: false }));
                            }
                        },
                    });
                    dispatch(setUploadProgress({ id, progress: 100, error: false, done: true }));
                } catch (error) {
                    dispatch(setUploadProgress({ id, progress: 0, error: true, done: false }));
                }
            }
        },
        [activeFolderId, dispatch, isUploadingContent, organizationId, uploadableFiles],
    );

    return { uploadFile };
};
