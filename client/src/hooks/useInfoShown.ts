import { useCallback, useEffect, useRef, useState } from 'react';

type ReturnValue = {
    isInfoShown: boolean;
    showHideInfo: () => void;
};

export const useInfoShown = (activeId: string): ReturnValue => {
    const [isInfoShown, setIsInfoShown] = useState(false);

    const showHideInfo = useCallback(() => {
        setIsInfoShown((prev) => !prev);
    }, []);

    const prevActiveId = useRef(activeId);
    useEffect(() => {
        if (prevActiveId.current !== activeId) {
            setIsInfoShown(false);
            prevActiveId.current = activeId;
        }
    }, [activeId]);

    return {
        isInfoShown,
        showHideInfo,
    };
};
