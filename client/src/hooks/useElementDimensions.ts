import { useCallback, useEffect, useState } from 'react';

type WindowDimensions = {
    width: number;
    height: number;
    isResizing: boolean;
};

export const useElementDimensions = (element: Element | null, customEvent = ''): WindowDimensions => {
    const [elementDimensions, setElementDimensions] = useState({ width: 0, height: 0 });
    const [isResizing, setIsResizing] = useState(false);

    useEffect(() => {
        if (element) {
            setElementDimensions({
                width: element.clientWidth,
                height: element.clientHeight,
            });
        }
    }, [element]);

    const getElementDimensions = useCallback(
        (): Omit<WindowDimensions, 'isResizing'> => ({
            width: element ? element.clientWidth : 0,
            height: element ? element.clientHeight : 0,
        }),
        [element],
    );

    useEffect(() => {
        let timeout: number;
        if (element) {
            window.addEventListener('resize', () => {
                setElementDimensions(getElementDimensions());

                setIsResizing(true);
                window.clearTimeout(timeout);
                timeout = window.setTimeout(() => {
                    setIsResizing(false);
                }, 250);
            });
        }

        return (): void => window.removeEventListener('resize', () => setElementDimensions(getElementDimensions()));
    }, [element, getElementDimensions]);

    useEffect(() => {
        if (!customEvent) return;
        const listener = () => {
            setTimeout(() => {
                setElementDimensions(getElementDimensions());
            }, 500);
        };

        if (element) {
            window.addEventListener(customEvent, listener);
        }

        return (): void => window.removeEventListener(customEvent, listener);
    }, [customEvent, element, getElementDimensions]);

    return { ...elementDimensions, isResizing };
};
