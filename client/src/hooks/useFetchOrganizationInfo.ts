import { useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { selectCurrentOrganization } from '../@pages/ProfilePage/selectors';
import { httpService } from '../helpers/httpService';
import { ClientOrganizationInfo } from '../../../common/types';

const initialOrganizationInfo: ClientOrganizationInfo = {
    limitScreens: 0,
    licenses: [],
    limitStorageSize: 0,
    usedStorageSize: 0,
};

type ReturnValue = {
    organizationInfo: ClientOrganizationInfo;
};

export const useFetchOrganizationInfo = (): ReturnValue => {
    const { id: organizationId } = useSelector(selectCurrentOrganization);

    const [organizationInfo, setOrganizationInfo] = useState<ClientOrganizationInfo>(initialOrganizationInfo);

    const fetchOrganizationInfo = useCallback(() => {
        if (organizationId) {
            httpService.get(`organization/info/${organizationId}`).then((info) => {
                setOrganizationInfo(info as ClientOrganizationInfo);
            });
        }
    }, [organizationId]);

    useEffect(fetchOrganizationInfo, [fetchOrganizationInfo]);

    return { organizationInfo };
};
