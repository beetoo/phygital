import { useState, useCallback } from 'react';

type ReturnType = {
    isModalOpen: boolean;
    openModal: () => void;
    activeId: string;
    openModalWithId: (id: string) => () => void;
    closeModal: () => void;
};

export const useModal = (): ReturnType => {
    const [isModalOpen, setModalOpen] = useState(false);
    const [activeId, setActiveId] = useState('');

    const openModal = useCallback(() => {
        setModalOpen(true);
    }, []);

    const openModalWithId = useCallback(
        (id: string) => () => {
            setActiveId(id);
            setModalOpen(true);
        },
        [],
    );

    const closeModal = useCallback(() => {
        setModalOpen(false);
    }, []);

    return {
        isModalOpen,
        openModal,
        activeId,
        openModalWithId,
        closeModal,
    };
};
