import { useTranslation } from 'react-i18next';

import { CONTENT_TYPE, PlaylistItemType } from '../../../common/types';
import {
    getCreateOrUpdateTimeString,
    getFilesizeString,
    getFileTypeAndExtensionString,
} from '../@pages/PlaylistsPage/helpers';
import { formatSecondsToTimeString } from '../../../common/helpers';

type ReturnValue = {
    playlistItemName: string;
    playlistItemFileSize: string;
    playlistItemFileTypeAndExtension: string;
    isImageType: boolean;
    isVideoType: boolean;
    isWebType: boolean;
    playlistItemDimensions: string;
    playlistItemOrientation: string;
    playlistItemDuration: string;
    playlistItemPlaceIndex: number;
    playlistItemSrc: string;
    playlistItemCreatedAt: string;
    playlistItemUpdatedAt: string;
};

// имя, тип, размер, разрешение, ориентация, длительность, место в плейлисте

export const usePlaylistItemInfo = (playlistItem: PlaylistItemType | undefined): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlists' });

    const { name, content, delay, orderIndex, createdAt, updatedAt } = playlistItem ?? {
        name: '',
        delay: 0,
        orderIndex: 0,
        createdAt: '',
        updatedAt: '',
        content: {
            id: '',
            src: '',
            type: '',
            size: 0,
            duration: 0,
            dimensions: { width: 0, height: 0 },
        },
    };

    const playlistItemFileTypeAndExtension = getFileTypeAndExtensionString(name, content?.type, t);

    // without extension
    const playlistItemName = name.replace(/\.[^/.]+$/, '');

    const playlistItemSrc = content?.src;

    const playlistItemFileSize = getFilesizeString(content?.size);

    const playlistItemWidth = content?.dimensions?.width ?? 0;
    const playlistItemHeight = content?.dimensions?.height ?? 0;

    const playlistItemDimensions = `${playlistItemWidth} × ${playlistItemHeight}`;

    const playlistItemOrientation =
        playlistItemWidth === playlistItemHeight
            ? t('square')
            : playlistItemWidth > playlistItemHeight
              ? t('horizontal')
              : t('portrait');

    const playlistItemDuration = formatSecondsToTimeString(
        content?.type === CONTENT_TYPE.IMAGE || content?.type === CONTENT_TYPE.WEB
            ? delay
            : content?.type === CONTENT_TYPE.VIDEO
              ? (content?.duration ?? 0)
              : 0,
    );
    const playlistItemPlaceIndex = orderIndex + 1;

    const playlistItemCreatedAt = getCreateOrUpdateTimeString(createdAt ?? new Date());
    const playlistItemUpdatedAt = getCreateOrUpdateTimeString(updatedAt ?? new Date());

    const isImageType = content?.type === CONTENT_TYPE.IMAGE;
    const isVideoType = content?.type === CONTENT_TYPE.VIDEO;
    const isWebType = content?.type === CONTENT_TYPE.WEB;

    return {
        playlistItemName,
        playlistItemSrc,
        playlistItemFileTypeAndExtension,
        playlistItemFileSize,
        playlistItemDimensions,
        playlistItemOrientation,
        playlistItemDuration,
        playlistItemPlaceIndex,
        playlistItemCreatedAt,
        playlistItemUpdatedAt,
        isImageType,
        isVideoType,
        isWebType,
    };
};
