import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import { useTranslation } from 'react-i18next';

import { selectDeleteContentItemStatus, selectUploadingContentStatus } from '../@pages/ContentPage/selectors';
import {
    selectDeletePlaylistItemSuccessStatus,
    selectDeletePlaylistsSuccessStatus,
} from '../@pages/PlaylistsPage/selectors';
import { Snackbar } from '../ui-kit';

export const useStartDeleteSnackbars = (): void => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const isDeletePlaylistSuccessStatus = useSelector(selectDeletePlaylistsSuccessStatus);
    const isDeletePlaylistItemSuccessStatus = useSelector(selectDeletePlaylistItemSuccessStatus);
    const isUploadingContent = useSelector(selectUploadingContentStatus);
    const isDeleteContent = useSelector(selectDeleteContentItemStatus);

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    useEffect(() => {
        if (isDeletePlaylistSuccessStatus) {
            const closeSnackbarKey = enqueueSnackbar(
                <Snackbar title={t('playlistDelete')} onClose={() => closeSnackbar(closeSnackbarKey)} />,
                {
                    key: 'PlaylistDeleted',
                    autoHideDuration: 3000,
                },
            );
        }
    }, [closeSnackbar, enqueueSnackbar, isDeletePlaylistSuccessStatus, t]);

    useEffect(() => {
        if (isDeletePlaylistItemSuccessStatus) {
            const closeSnackbarKey = enqueueSnackbar(
                <Snackbar title={t('contentDelete')} onClose={() => closeSnackbar(closeSnackbarKey)} />,
                {
                    key: 'ContentDeleted',
                    autoHideDuration: 3000,
                },
            );
        }
    }, [closeSnackbar, enqueueSnackbar, isDeletePlaylistItemSuccessStatus, t]);

    useEffect(() => {
        if (isDeleteContent) {
            const closeSnackbarKey = enqueueSnackbar(
                <Snackbar title={t('contentBeingDeleted')} onClose={() => closeSnackbar(closeSnackbarKey)} />,
                {
                    key: 'DeleteContent',
                    autoHideDuration: 3000,
                },
            );
        }
    }, [closeSnackbar, enqueueSnackbar, isDeleteContent, isUploadingContent, t]);
};
