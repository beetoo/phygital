import { useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';

import noPlaylistsPlaceholder from '../assets/screen_placeholder.png';
import offlineScreenPlaceholder from '../assets/offlineScreenPlaceholder.png';
import { selectActiveScreenId, selectScreens } from '../@pages/ScreensPage/selectors';
import { httpService } from '../helpers/httpService';

type ReturnValue = {
    screenshotSrc: string;
};

export const useGetScreenshots = (timeout: number, screenId?: string): ReturnValue => {
    let activeScreenId = useSelector(selectActiveScreenId);
    const screens = useSelector(selectScreens);

    const isScreenMenuAndListIdEqual = activeScreenId ? screenId === activeScreenId : false;

    activeScreenId = screenId || activeScreenId;

    const activeScreen = useMemo(() => screens.find(({ id }) => id === activeScreenId), [activeScreenId, screens]);

    const isScreenActive = useMemo(() => activeScreen?.status === 'online', [activeScreen?.status]);

    const screenHasPlaylist = useMemo(
        () => activeScreen?.schedules && activeScreen.schedules.length > 0,
        [activeScreen?.schedules],
    );

    const [screenshotSrc, setScreenshotSrc] = useState(noPlaylistsPlaceholder);
    const [fetchScreenshotError, setFetchScreenshotError] = useState(false);

    timeout = useMemo(
        () => (fetchScreenshotError ? 120 : isScreenMenuAndListIdEqual ? 5 : timeout) * 1000,
        [fetchScreenshotError, isScreenMenuAndListIdEqual, timeout],
    );

    const getScreenshot = useCallback(() => {
        if (!activeScreenId && !activeScreen?.identification) return;

        httpService
            .get(`screen/${activeScreenId}/screenshot`, {
                headers: { identification: activeScreen?.identification },
            })
            .then((response) => {
                const src = (response as { src: string }).src;
                setScreenshotSrc(src ?? noPlaylistsPlaceholder);
                setFetchScreenshotError(!src);
            })
            .catch(() => {
                setScreenshotSrc(noPlaylistsPlaceholder);
                setFetchScreenshotError(true);
            });
    }, [activeScreen?.identification, activeScreenId]);

    useEffect(() => {
        if (!isScreenActive) {
            return setScreenshotSrc(offlineScreenPlaceholder);
        } else if (!screenHasPlaylist) {
            return setScreenshotSrc(noPlaylistsPlaceholder);
        }

        if (!fetchScreenshotError) {
            getScreenshot();
        }

        const interval = setInterval(getScreenshot, timeout);

        return () => {
            clearInterval(interval);
        };
    }, [getScreenshot, fetchScreenshotError, isScreenActive, screenHasPlaylist, timeout]);

    return {
        screenshotSrc,
    };
};
