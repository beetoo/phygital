import { ChangeEvent, KeyboardEvent, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import orderBy from 'lodash/orderBy';
import intersection from 'lodash/intersection';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectCurrentOrganization } from '../@pages/ProfilePage/selectors';
import {
    selectActiveScreen,
    selectLocations,
    selectUpdateScreenStatus,
    selectUpdateScreenSuccessStatus,
} from '../@pages/ScreensPage/selectors';
import { formatScreenOrientation, formatScreenResolution } from '../@pages/ScreensPage/helpers';
import { OrientationTypeEng, OrientationRusType } from '../@pages/ScreensPage/types';
import {
    LocationType,
    OrientationEnum,
    PlaylistScheduleType,
    ScreenRequestLoggerType,
    ScreenTagType,
} from '../../../common/types';
import {
    setActiveScreenId,
    setClearScreensSuccess,
    setDeleteScreenTags,
    setUpdateScreen,
} from '../@pages/ScreensPage/actions';
import { setActiveScheduleScreenId } from '../@pages/DxSchedulePage/actions';
import { setActivePlaylistId } from '../@pages/PlaylistsPage/actions';
import { setShowModal, setShowModalNew } from '../components/ModalsContainer/actions';
import { MODALS } from '../components/ModalsContainer/types';
import { getResolutionAndOrientation } from '../helpers';
import { useInfoShown } from './useInfoShown';
import { useCustomRouter } from './useCustomRouter';
import { HistoryItem, useHistoryItems } from '../@pages/PlaylistsPage/hooks/useHistoryItems';
import { selectAuthUserRole } from '../selectors';
import { USER_ROLE } from '../constants';
import { isDevelopmentMode } from '../constants/environments';
import { ScreenMessageItem, useScreenMessages } from '../@pages/ScreensPage/hooks/useScreenMessages';

type ScreenMenuTab = 'settings' | 'messages' | 'history';

type ReturnValue = {
    userRole: USER_ROLE;
    activeScreenStatus: string;
    locationDescription: string;
    savedTags: ScreenTagType[];

    hasCustomResolutionInput: boolean;
    isSubmitButtonDisabled: boolean;
    isSubmitButtonShown: boolean;

    screenName: string;
    customResolutionWidth: string;
    customResolutionHeight: string;
    tag: string;
    notSavedTags: string[];
    log: ScreenRequestLoggerType | undefined;
    availableBytesInfo: string;
    usedBytesInfo: string;
    totalBytesInfo: string;
    verificationHash: string | undefined;
    hasCapacityInfo: boolean;

    onScreenNameChange: (event: ChangeEvent<HTMLInputElement>) => void;

    openChangePlayerModal: () => void;

    schedules: PlaylistScheduleType[];
    goToScreenSchedule: () => void;
    isNoSchedules: boolean;
    isSchedulesListOpen: boolean;
    onSwitchSchedulesList: () => void;
    goToPlaylist: (playlistId: string) => () => void;

    screenResolutionValue: '' | string;
    onScreenResolutionSelect: (event: SelectChangeEvent) => void;
    onCustomResolutionWidthChange: (event: ChangeEvent<HTMLInputElement>) => void;
    onCustomResolutionHeightChange: (event: ChangeEvent<HTMLInputElement>) => void;

    screenOrientationValue: OrientationRusType | OrientationTypeEng | '';
    onScreenOrientationSelect: (event: SelectChangeEvent) => void;

    onTagChange: (event: ChangeEvent<HTMLInputElement>) => void;
    onTagEnter: (event: KeyboardEvent<HTMLInputElement>) => void;
    onDeleteNotSavedTag: (deletedTag: string) => () => void;
    onDeleteSavedTag: (deletedTagId: string) => () => void;
    handleSubmit: () => void;

    selectableLocations: LocationType[];
    locationSelectValue: '' | string;
    setActiveLocationId: (locationId: string) => () => void;
    onLocationSelect: (event: SelectChangeEvent) => void;

    openDeleteScreenModal: () => void;

    isInfoShown: boolean;
    showHideInfo: () => void;

    screenMenuTab: ScreenMenuTab;
    setActiveScreenMenuTab: (tab: ScreenMenuTab) => () => void;
    hasHistory: boolean | undefined;
    screenHistoryItems: HistoryItem[];

    hasMessages: boolean | undefined;
    screenMessages: ScreenMessageItem[];

    tScreens: TFunction;
    tScreensModal: TFunction;
};

export const useScreenMenu = (): ReturnValue => {
    const { t: tScreens } = useTranslation('translation', { keyPrefix: 'screens' });
    const { t: tScreensModal } = useTranslation('translation', { keyPrefix: 'screensModal' });

    const dispatch = useDispatch();

    const userRole = useSelector(selectAuthUserRole);
    const {
        id: activeScreenId,
        name: activeScreenName,
        status: activeScreenStatus,
        resolution: activeScreenResolution,
        orientation: activeScreenOrientation,
        availableBytes,
        totalBytes,
        tags: activeScreenTags,
        location,
        schedules,
        log,
        verificationHash,
        hasHistory,
        hasMessages,
    } = useSelector(selectActiveScreen);

    const locations = useSelector(selectLocations);

    const { language } = useSelector(selectCurrentOrganization);
    const updatingScreen = useSelector(selectUpdateScreenStatus);

    const isUpdateScreenSuccess = useSelector(selectUpdateScreenSuccessStatus);

    const { pushToPage } = useCustomRouter();

    const { formattedScreenResolution: initialScreenResolution } = formatScreenResolution(activeScreenResolution);
    const { formattedScreenOrientation: initialScreenOrientation } = formatScreenOrientation(
        activeScreenOrientation as OrientationEnum,
        language,
    );

    const [screenName, setScreenName] = useState(activeScreenName);

    const [screenResolutionValue, setScreenResolutionValue] = useState<'' | string>(initialScreenResolution);

    const [hasCustomResolutionInput, setHasCustomResolutionInput] = useState(false);
    const [customResolutionWidth, setCustomResolutionWidth] = useState('');
    const [customResolutionHeight, setCustomResolutionHeight] = useState('');

    const [screenOrientationValue, setScreenOrientationValue] = useState<OrientationRusType | OrientationTypeEng | ''>(
        initialScreenOrientation,
    );

    const [tag, setTag] = useState('');
    const [notSavedTags, setNotSavedTags] = useState<string[]>([]);

    const [locationId, setLocationId] = useState('');
    const [locationSelectValue, setLocationSelectValue] = useState<'' | string>('');

    const [savedTags, setSavedTags] = useState<ScreenTagType[]>([]);
    const [deletableTags, setDeletableTags] = useState<{ screenId: string; tagId: string }[]>([]);
    const [isSchedulesListOpen, setIsSchedulesListOpen] = useState(false);

    const [isSubmitButtonShown, setIsSubmitButtonShown] = useState(false);

    const screenResolution = useMemo(() => [screenResolutionValue].join(), [screenResolutionValue]);
    const screenOrientation = useMemo(() => [screenOrientationValue].join(), [screenOrientationValue]);

    const [screenMenuTab, setScreenMenuTab] = useState<ScreenMenuTab>('settings');
    const { historyItems: screenHistoryItems } = useHistoryItems({
        type: 'screen',
        fetchedId: activeScreenId,
        fetch: screenMenuTab === 'history',
    });

    const { screenMessages } = useScreenMessages({ fetchedId: activeScreenId, fetch: screenMenuTab === 'messages' });

    const setActiveScreenMenuTab = useCallback(
        (tab: ScreenMenuTab) => () => {
            setScreenMenuTab(tab);
        },
        [],
    );

    useEffect(() => {
        if (activeScreenId) {
            setScreenMenuTab('settings');
        }
    }, [activeScreenId, setActiveScreenMenuTab]);

    const openChangePlayerModal = useCallback(() => {
        dispatch(setShowModalNew(MODALS.CHANGE_PLAYER_MODAL, null));
    }, [dispatch]);

    const selectableLocations = useMemo(
        () => locations.filter(({ screens }) => screens.every(({ id }) => id !== activeScreenId)),
        [activeScreenId, locations],
    );

    const { isInfoShown, showHideInfo } = useInfoShown(activeScreenId);

    const setActiveLocationId = useCallback(
        (locationId: string) => () => {
            setLocationId(locationId);
        },
        [],
    );

    const onLocationSelect = useCallback(({ target: { value } }: SelectChangeEvent) => {
        setLocationSelectValue(value);
    }, []);

    const screenTags = useMemo(() => orderBy(activeScreenTags, ['name'], ['asc']), [activeScreenTags]);

    useEffect(() => {
        if (activeScreenId) {
            setSavedTags(screenTags);
            setDeletableTags([]);
            setNotSavedTags([]);
        }
    }, [activeScreenId, screenTags]);

    // после переноса экрана обнуляем screenId
    useEffect(() => {
        if (isUpdateScreenSuccess && locationId) {
            dispatch(setActiveScreenId());
            dispatch(setClearScreensSuccess());
        }
    }, [dispatch, isUpdateScreenSuccess, locationId]);

    useEffect(() => {
        // обнуляем значения
        if (isUpdateScreenSuccess) {
            setNotSavedTags([]);
            setLocationId('');
            setLocationSelectValue('');
            setDeletableTags([]);
            dispatch(setClearScreensSuccess());
        }
    }, [dispatch, isUpdateScreenSuccess, locationId]);

    useEffect(() => {
        // при выделении экрана меняем значения на соответствующие свойства этого экранов
        setScreenName(activeScreenName);
        setScreenResolutionValue(initialScreenResolution);
        setScreenOrientationValue(initialScreenOrientation);
        setHasCustomResolutionInput(false);
        setNotSavedTags([]);
    }, [activeScreenName, initialScreenOrientation, initialScreenResolution]);

    useEffect(() => {
        // очищаем инпуты перед их открытием
        if (hasCustomResolutionInput) {
            setCustomResolutionWidth('');
            setCustomResolutionHeight('');
        }
    }, [hasCustomResolutionInput]);

    useEffect(() => {
        // если не сохранённые теги пересекаются с сохранёнными, то очищаем не сохранённые,
        // так как это означает, что они удачно были сохранены
        const isAddedNewTag =
            intersection(
                activeScreenTags.map(({ name }) => name),
                notSavedTags,
            ).length > 0;

        if (isAddedNewTag) {
            setNotSavedTags([]);
        }
    }, [activeScreenTags, notSavedTags]);

    const goToScreenSchedule = useCallback(() => {
        dispatch(setActiveScheduleScreenId(activeScreenId, true));
        pushToPage('dx_schedule');
    }, [activeScreenId, dispatch, pushToPage]);

    const goToPlaylist = useCallback(
        (playlistId: string) => () => {
            dispatch(setActivePlaylistId(playlistId));
            pushToPage('playlists');
        },
        [dispatch, pushToPage],
    );

    const onSwitchSchedulesList = useCallback(() => {
        setIsSchedulesListOpen((prev) => !prev);
    }, []);

    const isNoSchedules = useMemo(() => schedules.length === 0, [schedules.length]);

    const onScreenNameChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setScreenName(value);
    }, []);

    const onScreenResolutionSelect = useCallback(
        ({ target: { value } }: SelectChangeEvent) => {
            setHasCustomResolutionInput(value === tScreensModal('resolutionCustom'));
            setScreenResolutionValue(value);
        },
        [tScreensModal],
    );

    const onCustomResolutionWidthChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setCustomResolutionWidth(value);
    }, []);

    const onCustomResolutionHeightChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setCustomResolutionHeight(value);
    }, []);

    const onScreenOrientationSelect = useCallback(({ target: { value } }: SelectChangeEvent) => {
        setScreenOrientationValue(value as OrientationRusType);
    }, []);

    const onTagChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setTag(value);
    }, []);

    const onTagEnter = useCallback(
        ({ key }: KeyboardEvent<HTMLInputElement>) => {
            if (
                key === 'Enter' &&
                tag.trim() !== '' &&
                ![
                    ...activeScreenTags.map(({ name }) => name.toLowerCase()),
                    ...notSavedTags.map((tag) => tag.toLowerCase()),
                ].includes(tag.trim().toLowerCase())
            ) {
                setNotSavedTags([...notSavedTags, tag]);
                setTag('');
            }
        },
        [activeScreenTags, notSavedTags, tag],
    );

    const onDeleteNotSavedTag = useCallback(
        (deletedTag: string) => () => {
            setNotSavedTags(notSavedTags.filter((tag) => tag !== deletedTag));
        },
        [notSavedTags],
    );

    const onDeleteSavedTag = useCallback(
        (deletedTagId: string) => () => {
            setSavedTags(savedTags.filter(({ id }) => id !== deletedTagId));
            setDeletableTags([...deletableTags, { screenId: activeScreenId, tagId: deletedTagId }]);
        },
        [activeScreenId, deletableTags, savedTags],
    );

    const resolutionAndOrientation = useMemo(
        () =>
            getResolutionAndOrientation({
                activeScreenResolution,
                screenResolution,
                customResolutionWidth,
                customResolutionHeight,
                screenOrientation,
            }),
        [activeScreenResolution, customResolutionHeight, customResolutionWidth, screenOrientation, screenResolution],
    );

    const handleSubmit = useCallback(() => {
        const { resolution, orientation } = resolutionAndOrientation;

        const updateScreenDto = {
            name: screenName,
            resolution,
            orientation,
            startTime: { hours: 0, minutes: 0 },
            endTime: { hours: 0, minutes: 0 },
            tags: notSavedTags,
            locationId,
        };

        dispatch(setUpdateScreen(activeScreenId, updateScreenDto));

        if (deletableTags.length > 0) {
            dispatch(setDeleteScreenTags(deletableTags));
        }
    }, [activeScreenId, deletableTags, dispatch, locationId, notSavedTags, resolutionAndOrientation, screenName]);

    const openDeleteScreenModal = useCallback(() => {
        dispatch(setShowModal(MODALS.DELETE_SCREEN));
    }, [dispatch]);

    const locationDescription = `${location?.name}${location?.city ? ',' : ''} ${location?.city}${
        location?.address ? ',' : ''
    } ${location?.address}`;

    const isInputValuesNotValid = useMemo(
        () =>
            (screenName.trim() === '' || activeScreenName.toLowerCase() === screenName.toLowerCase()) &&
            initialScreenResolution === screenResolution &&
            initialScreenOrientation === screenOrientation &&
            notSavedTags.length === 0 &&
            locationId === '' &&
            deletableTags.length === 0,
        [
            activeScreenName,
            deletableTags.length,
            initialScreenOrientation,
            initialScreenResolution,
            locationId,
            notSavedTags.length,
            screenName,
            screenOrientation,
            screenResolution,
        ],
    );

    const customResolutionWidthNumber = Number(customResolutionWidth.trim());
    const customResolutionHeightNumber = Number(customResolutionHeight.trim());

    const isCustomResolutionValuesNotValid = useMemo(
        () =>
            screenResolution === tScreensModal('resolutionCustom') &&
            (customResolutionWidth.trim() === '' ||
                customResolutionHeight.trim() === '' ||
                customResolutionWidthNumber === 0 ||
                customResolutionHeightNumber === 0 ||
                isNaN(customResolutionWidthNumber) ||
                isNaN(customResolutionHeightNumber) ||
                (customResolutionWidth === activeScreenResolution?.width.toString() &&
                    customResolutionHeight === activeScreenResolution?.height.toString())),
        [
            activeScreenResolution?.height,
            activeScreenResolution?.width,
            customResolutionHeight,
            customResolutionHeightNumber,
            customResolutionWidth,
            customResolutionWidthNumber,
            screenResolution,
            tScreensModal,
        ],
    );

    const isSubmitButtonDisabled = updatingScreen || isInputValuesNotValid || isCustomResolutionValuesNotValid;

    const prevScreenId = useRef(activeScreenId);
    useEffect(() => {
        if (prevScreenId.current !== activeScreenId) {
            setIsSubmitButtonShown(false);

            prevScreenId.current = activeScreenId;
        } else {
            setIsSubmitButtonShown(!isSubmitButtonDisabled);
        }
    }, [activeScreenId, isSubmitButtonDisabled]);

    const hasCapacityInfo = isDevelopmentMode || availableBytes > 0 || totalBytes > 0;

    const availableBytesMb = Math.round(availableBytes / (1024 * 1024));
    const totalBytesMb = Math.round(totalBytes / (1024 * 1024));
    const usedBytesMb = totalBytesMb - availableBytesMb;

    const availableBytesInfo =
        availableBytesMb >= 1000
            ? `${(availableBytesMb / 1024).toFixed(3)} ${tScreens('gb')}`
            : `${availableBytesMb} ${tScreens('mb')}`;
    const totalBytesInfo =
        totalBytesMb >= 1000
            ? `${(totalBytesMb / 1024).toFixed(3)} ${tScreens('gb')}`
            : `${totalBytesMb} ${tScreens('mb')}`;
    const usedBytesInfo =
        usedBytesMb >= 1000
            ? `${(usedBytesMb / 1024).toFixed(3)} ${tScreens('gb')}`
            : `${usedBytesMb} ${tScreens('mb')}`;

    return {
        userRole,
        activeScreenStatus,
        locationDescription,
        savedTags,

        hasCustomResolutionInput,
        isSubmitButtonDisabled,
        isSubmitButtonShown,

        screenName,
        customResolutionWidth,
        customResolutionHeight,
        tag,
        notSavedTags,
        log,
        availableBytesInfo,
        usedBytesInfo,
        totalBytesInfo,
        hasCapacityInfo,
        verificationHash,

        schedules,
        onScreenNameChange,
        goToScreenSchedule,
        isSchedulesListOpen,
        onSwitchSchedulesList,
        goToPlaylist,

        openChangePlayerModal,

        screenResolutionValue,
        onScreenResolutionSelect,

        screenOrientationValue,
        onScreenOrientationSelect,
        onCustomResolutionWidthChange,
        onCustomResolutionHeightChange,
        onTagChange,
        onTagEnter,
        onDeleteNotSavedTag,
        onDeleteSavedTag,
        handleSubmit,

        selectableLocations,
        locationSelectValue,
        setActiveLocationId,
        onLocationSelect,

        isNoSchedules,

        openDeleteScreenModal,

        isInfoShown,
        showHideInfo,

        screenMenuTab,
        setActiveScreenMenuTab,
        hasHistory,
        screenHistoryItems,

        hasMessages,
        screenMessages,

        tScreens,
        tScreensModal,
    };
};
