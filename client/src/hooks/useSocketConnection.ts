import { useEffect, useMemo, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { SocketService } from '../helpers/sockerService';
import { setGetPlaylistsSuccess } from '../@pages/PlaylistsPage/actions';
import { selectPlaylists } from '../@pages/PlaylistsPage/selectors';
import { selectScreens } from '../@pages/ScreensPage/selectors';
import { setGetScreensSuccess } from '../@pages/ScreensPage/actions';
import { PlaylistStatus, ScreenStatusType } from '../../../common/types';
import { selectAuthOrganizationId } from '../selectors';
import { usePlaylistStep } from '../@pages/PlaylistsPage/hooks/usePlaylistStep';

export const useSocketConnection = (): void => {
    const dispatch = useDispatch();

    const { pathname } = useLocation();

    const { playlistStep } = usePlaylistStep();

    const authOrganizationId = useSelector(selectAuthOrganizationId);
    const playlists = useSelector(selectPlaylists);
    const screens = useSelector(selectScreens);

    const [socketConnected, setSocketConnected] = useState(false);

    const connectRequired = useMemo(
        () => authOrganizationId && (pathname.endsWith('screens') || (pathname.endsWith('playlists') && !playlistStep)),
        [authOrganizationId, pathname, playlistStep],
    );

    useEffect(() => {
        if (connectRequired) {
            SocketService.createConnection(authOrganizationId);
            setSocketConnected(true);
        }

        return () => {
            SocketService.socket?.disconnect();
            setSocketConnected(false);
        };
    }, [connectRequired, authOrganizationId]);

    const playlistConnectionAvailable = useMemo(
        () => socketConnected && playlists.length > 0,
        [playlists.length, socketConnected],
    );

    useEffect(() => {
        if (!playlistConnectionAvailable) return;

        SocketService.socket?.removeListener('playlist.status');
        SocketService.socket?.on(
            'playlist.status',
            ({ playlistId, status }: { playlistId: string; status: PlaylistStatus }) => {
                dispatch(
                    setGetPlaylistsSuccess(
                        playlists.map((playlist) => (playlist.id === playlistId ? { ...playlist, status } : playlist)),
                    ),
                );
            },
        );
    }, [dispatch, playlistConnectionAvailable, playlists]);

    const screenConnectionAvailable = useMemo(
        () => socketConnected && (pathname.endsWith('organizations') || screens.length > 0),
        [pathname, screens.length, socketConnected],
    );

    useEffect(() => {
        if (!screenConnectionAvailable) return;

        SocketService.socket?.removeListener('screen.status');
        SocketService.socket?.on(
            'screen.status',
            ({ screenId, status }: { screenId: string; status: ScreenStatusType }) => {
                dispatch(
                    setGetScreensSuccess(
                        screens.map((screen) => (screen.id === screenId ? { ...screen, status } : screen)),
                    ),
                );
            },
        );
    }, [dispatch, screenConnectionAvailable, screens]);
};
