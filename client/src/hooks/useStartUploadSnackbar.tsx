import React, { useEffect, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { SnackbarKey, useSnackbar } from 'notistack';

import { UploadingSnackbar } from '../components/UploadingSnackbar';

import { selectUploadingContentStatus } from '../@pages/ContentPage/selectors';
import { selectUploadableFiles } from '../components/ModalsContainer/selectors';

export const useStartUploadSnackbar = (): void => {
    const uploadableFiles = useSelector(selectUploadableFiles);
    const isUploadingContent = useSelector(selectUploadingContentStatus);

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const onCloseSnackbar = useCallback(
        (snackbarKey: SnackbarKey) => {
            closeSnackbar(snackbarKey);
        },
        [closeSnackbar],
    );

    useEffect(() => {
        let snackbarKey: SnackbarKey = 'UploadingContent';

        if (isUploadingContent) {
            snackbarKey = enqueueSnackbar(<UploadingSnackbar onClose={() => onCloseSnackbar(snackbarKey)} />, {
                key: 'UploadingContent',
                persist: true,
            });
        }

        if (uploadableFiles.length === 0) {
            onCloseSnackbar(snackbarKey);
        }
    }, [enqueueSnackbar, isUploadingContent, onCloseSnackbar, uploadableFiles.length]);
};
