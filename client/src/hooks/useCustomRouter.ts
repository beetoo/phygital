import { useCallback } from 'react';
import { useNavigate, useMatch, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { USER_ROLE } from '../constants';
import { selectCurrentOrganization } from '../@pages/ProfilePage/selectors';
import { selectAuthUserRole } from '../selectors';
import { ActivePage } from '../components/OrganizationContainer/types';
import { setShowModal } from '../components/ModalsContainer/actions';
import { MODALS } from '../components/ModalsContainer/types';
import { usePlaylistStep } from '../@pages/PlaylistsPage/hooks/usePlaylistStep';

type ReturnValue = {
    currentPage: ActivePage;
    pushToPage: (pageGoingTo?: ActivePage, organizationId?: string) => void;
    selectPage: (page: ActivePage, isNavigate?: boolean) => () => void;
    isAdminOrManager: boolean;
};

export const useCustomRouter = (): ReturnValue => {
    const dispatch = useDispatch();

    const navigate = useNavigate();
    const { pathname } = useLocation();

    const { playlistStep } = usePlaylistStep();

    const { id } = useSelector(selectCurrentOrganization);
    const userRole = useSelector(selectAuthUserRole);
    const isAdminOrManager = userRole === USER_ROLE.ADMIN || userRole === USER_ROLE.MANAGER;

    const {
        params: { page, subpage },
    } = (useMatch(isAdminOrManager ? '/organizations/:organizationId/:page/:subpage?' : '/:page/:subpage?') as {
        params: { page: ActivePage; subpage: unknown };
    }) ?? {
        params: { page: '' },
    };

    const currentPage = pathname.endsWith('organizations')
        ? 'organizations'
        : ((subpage ? `${page}/${subpage}` : page) as ActivePage);

    const pushToPage = useCallback(
        (page: ActivePage, organizationId = id) => {
            navigate(userRole && isAdminOrManager ? `/organizations/${organizationId}/${page}` : '/' + page);
        },
        [navigate, userRole, isAdminOrManager, id],
    );

    const selectPage = useCallback(
        (page: ActivePage, isNavigate = false) =>
            () => {
                if (playlistStep) {
                    dispatch(setShowModal(MODALS.CANCEL_CREATION_PLAYLIST, '', page));
                } else {
                    isNavigate ? navigate('/' + page) : pushToPage(page);
                }
            },
        [dispatch, navigate, playlistStep, pushToPage],
    );

    return {
        currentPage,
        pushToPage,
        selectPage,
        isAdminOrManager,
    };
};
