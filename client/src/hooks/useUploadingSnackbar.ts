import { useCallback, useState, useMemo, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import { isEqual } from 'lodash';

import { UploadProgress } from '../components/ModalsContainer/types';
import { setUploadableFiles, setUploadProgress } from '../components/ModalsContainer/actions';
import { setGetContentItems, setGetFolders, setStartUploadContentItems } from '../@pages/ContentPage/actions';
import { selectUploadProgress } from '../components/ModalsContainer/selectors';
import { selectCurrentOrganization } from '../@pages/ProfilePage/selectors';
import { promiseQueue } from '../helpers/PromiseQueue';
import { ActivePage } from '../components/OrganizationContainer/types';
import { IterableUploadableFile, useUploadableFiles } from './useUploadableFiles';
import { useCustomRouter } from './useCustomRouter';
import { selectUploadingContentStatus } from '../@pages/ContentPage/selectors';
import { useUploadFile } from './useUploadFile';

type Props = {
    onClose?: () => void;
};

type ReturnValue = {
    currentPage: ActivePage;
    uploadableFilesWithoutWarnings: IterableUploadableFile[];
    isExpanded: boolean;
    setExpanded: () => void;
    setOvered: (id: string) => void;
    uploadedFilesNumber: number;
    allUploadableFilesNumber: number;
    overallProgress: number;
    restartUploading: (uploadableFileId?: string) => () => void;
    deleteFile: (uploadableFileId: string) => () => void;
    isFileInQueue: (uploadableFileId: string) => boolean;
    isFileBeingUploaded: (uploadableFileId: string) => boolean;
    isOveredFileInQueueOrBeingUploaded: (uploadableFileId: string) => boolean;
    isFileSuccessfullyUploaded: (uploadableFileId: string) => boolean;
    isFileWithError: (uploadableFileId: string) => boolean;
    isFileOveredWithError: (uploadableFileId: string) => boolean;
    isSomeFilesBeingUploaded: boolean;
    isAllFilesSuccessfullyUploaded: boolean;
    isSomeFilesSuccessfullyUploaded: boolean;
    isAllFilesNotUploaded: boolean;
    isMoreThanOneUploadableFile: boolean;
    onCloseSnackbar: () => void;
    t: TFunction;
};

export const useUploadingSnackbar = ({ onClose }: Props): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const dispatch = useDispatch();

    const { currentPage: page } = useCustomRouter();
    const currentPageRef = useRef(page);
    const currentPage = currentPageRef.current;

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const uploadProgress = useSelector(selectUploadProgress);
    const isUploadingContent = useSelector(selectUploadingContentStatus);

    const { uploadableFiles, uploadableFilesWithoutWarnings, isMoreThanOneUploadableFile, deleteFile } =
        useUploadableFiles();

    const { uploadFile } = useUploadFile();

    const [isExpanded, setIsExpanded] = useState(uploadableFilesWithoutWarnings.length > 0);
    const [overedId, setOveredId] = useState('');

    const setExpanded = useCallback(() => {
        setIsExpanded((prev) => !prev);
    }, []);

    const setOvered = useCallback((id: string) => {
        setOveredId(id);
    }, []);

    const restartUploading = useCallback(
        (uploadableFileId: string) => () => {
            dispatch(setUploadProgress({ id: uploadableFileId, progress: 0, error: false, done: false }));
            promiseQueue.add([uploadableFileId], uploadFile);
            promiseQueue.run();
        },
        [dispatch, uploadFile],
    );

    const isFileInQueue = useCallback(
        (uploadableFileId: string) =>
            uploadableFilesWithoutWarnings.some(
                ({ id, progress, error }) => overedId !== id && uploadableFileId === id && progress === 0 && !error,
            ),
        [uploadableFilesWithoutWarnings, overedId],
    );

    const isFileBeingUploaded = useCallback(
        (uploadableFileId: string) =>
            uploadableFilesWithoutWarnings.some(
                ({ id, progress, error }) => uploadableFileId === id && progress > 0 && progress < 100 && !error,
            ),
        [uploadableFilesWithoutWarnings],
    );

    const isOveredFileInQueueOrBeingUploaded = useCallback(
        (uploadableFileId: string) =>
            uploadableFilesWithoutWarnings.some(
                ({ id, progress, error }) => overedId === id && uploadableFileId === id && progress === 0 && !error,
            ),
        [uploadableFilesWithoutWarnings, overedId],
    );

    const isFileSuccessfullyUploaded = useCallback(
        (uploadableFileId: string) =>
            uploadableFilesWithoutWarnings.some(
                ({ id, progress, error }) => uploadableFileId === id && progress === 100 && !error,
            ),
        [uploadableFilesWithoutWarnings],
    );

    const isFileWithError = useCallback(
        (uploadableFileId: string) =>
            uploadableFilesWithoutWarnings.some(({ id, error }) => overedId !== id && uploadableFileId === id && error),
        [uploadableFilesWithoutWarnings, overedId],
    );

    const isFileOveredWithError = useCallback(
        (uploadableFileId: string) =>
            uploadableFilesWithoutWarnings.some(({ id, error }) => overedId === id && uploadableFileId === id && error),
        [uploadableFilesWithoutWarnings, overedId],
    );

    const { uploadedFilesNumber, overallProgress, allUploadableFilesNumber } = useMemo(() => {
        const uploadedFilesNumber = uploadableFilesWithoutWarnings.filter(({ done }) => done).length;
        const allUploadableFilesNumber = uploadableFilesWithoutWarnings.length;
        return {
            uploadedFilesNumber,
            overallProgress: Math.round((uploadedFilesNumber * 100) / allUploadableFilesNumber),
            allUploadableFilesNumber,
        };
    }, [uploadableFilesWithoutWarnings]);

    const isSomeFilesBeingUploaded = useMemo(
        () => uploadableFilesWithoutWarnings.some(({ progress }) => progress > 0 && progress < 100),
        [uploadableFilesWithoutWarnings],
    );

    const isAllFilesSuccessfullyUploaded = useMemo(
        () => uploadableFilesWithoutWarnings.every(({ done }) => done),
        [uploadableFilesWithoutWarnings],
    );

    const isSomeFilesSuccessfullyUploaded = useMemo(
        () =>
            uploadableFilesWithoutWarnings.some(({ error }) => error) &&
            !uploadableFilesWithoutWarnings.some(({ progress }) => progress > 0 && progress < 100) &&
            !uploadableFilesWithoutWarnings.every(({ error }) => error),
        [uploadableFilesWithoutWarnings],
    );

    const isAllFilesNotUploaded = useMemo(
        () => uploadableFilesWithoutWarnings.every(({ error }) => error),
        [uploadableFilesWithoutWarnings],
    );

    // обновляем uploadableFiles
    const prevUploadProgress = useRef<UploadProgress>();
    useEffect(() => {
        if (uploadProgress && !isEqual(uploadProgress, prevUploadProgress.current)) {
            prevUploadProgress.current = uploadProgress;
            const updatedUploadableFiles = uploadableFiles.map((uploadableFile) => {
                const { id, progress, done, error } = uploadProgress;
                return uploadableFile.id === id ? { ...uploadableFile, progress, done, error } : uploadableFile;
            });

            dispatch(setUploadableFiles(updatedUploadableFiles));
        }
    }, [dispatch, uploadProgress, uploadableFiles]);

    const getUploadedItems = useCallback(() => {
        dispatch(setGetFolders(organizationId));
        dispatch(setGetContentItems(organizationId));
    }, [dispatch, organizationId]);

    // getVideoItems между загрузками файлов, если очередной файл грузится более 3 секунд
    const itemsUploaded = useMemo(
        () => uploadableFiles.filter(({ progress, done }) => progress === 100 && done).length,
        [uploadableFiles],
    );

    const prevItemsUploaded = useRef(itemsUploaded);

    useEffect(() => {
        let timeout: number;

        if (itemsUploaded < uploadableFiles.length && itemsUploaded !== prevItemsUploaded.current) {
            prevItemsUploaded.current = itemsUploaded;

            timeout = window.setTimeout(() => {
                getUploadedItems();
            }, 3000);
        }

        return () => {
            if (timeout) {
                window.clearTimeout(timeout);
            }
        };
    }, [getUploadedItems, itemsUploaded, uploadableFiles.length]);

    const onCloseSnackbar = useCallback(() => {
        if (isUploadingContent) {
            dispatch(setStartUploadContentItems(false));
            dispatch(setUploadProgress());
            getUploadedItems();
        }
    }, [dispatch, getUploadedItems, isUploadingContent]);

    // getVideoItems после загрузки последнего по списку файла
    const isAllItemsUploaded = useMemo(
        () => isUploadingContent && itemsUploaded === uploadableFiles.length,
        [isUploadingContent, itemsUploaded, uploadableFiles.length],
    );

    useEffect(() => {
        if (isAllItemsUploaded) {
            onCloseSnackbar();
        }
    }, [isAllItemsUploaded, onCloseSnackbar]);

    // закрываем снекбар через 3 сек, если все файлы загрузились и не было ошибок
    const isAllItemsUploadedWithourError = useMemo(
        () => !isUploadingContent && itemsUploaded === uploadableFiles.length,
        [isUploadingContent, itemsUploaded, uploadableFiles.length],
    );

    useEffect(() => {
        let timeout: number;

        if (isAllItemsUploadedWithourError) {
            timeout = window.setTimeout(() => {
                onClose && onClose();
            }, 3000);
        }
        return () => {
            if (timeout) {
                window.clearTimeout(timeout);
            }
        };
    }, [isAllItemsUploadedWithourError, onClose]);

    useEffect(() => {
        let listener: () => void;
        if (overedId !== '') {
            listener = () => setOvered('');
            document.addEventListener('mouseover', listener, false);
        }

        return () => {
            if (listener) {
                document.removeEventListener('mouseover', listener);
            }
        };
    }, [overedId, setOvered]);

    return {
        currentPage,
        uploadableFilesWithoutWarnings,
        isExpanded,
        setExpanded,
        setOvered,
        uploadedFilesNumber,
        allUploadableFilesNumber,
        overallProgress,
        restartUploading,
        deleteFile,
        isMoreThanOneUploadableFile,
        isFileInQueue,
        isFileBeingUploaded,
        isOveredFileInQueueOrBeingUploaded,
        isFileSuccessfullyUploaded,
        isFileWithError,
        isFileOveredWithError,
        isSomeFilesBeingUploaded,
        isAllFilesSuccessfullyUploaded,
        isSomeFilesSuccessfullyUploaded,
        isAllFilesNotUploaded,
        onCloseSnackbar,
        t,
    };
};
