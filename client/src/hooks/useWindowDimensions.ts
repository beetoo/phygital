import { useCallback, useEffect, useRef } from 'react';

import { useDebouncedState } from './useDebouncedState';

interface WindowDimensions {
    width: number;
    height: number;
}

interface ReturnValue extends WindowDimensions {
    isResizing: boolean;
    isWidthResizing: boolean;
    isHeightResizing: boolean;
}

export const useWindowDimensions = (debounce = 1000): ReturnValue => {
    const hasWindow = typeof window !== 'undefined';

    const isResizing = useRef(false);
    const isWidthResizing = useRef(false);
    const isHeightResizing = useRef(false);

    const [windowDimensions, setWindowDimensions] = useDebouncedState(
        {
            width: hasWindow ? window.innerWidth : 0,
            height: hasWindow ? window.innerHeight : 0,
        },
        debounce,
    );

    const getWindowDimensions = useCallback(
        (): WindowDimensions => ({
            width: hasWindow ? window.innerWidth : 0,
            height: hasWindow ? window.innerHeight : 0,
        }),
        [hasWindow],
    );

    useEffect(() => {
        let timeout: number;
        if (hasWindow) {
            window.addEventListener('resize', () => {
                const { width, height } = getWindowDimensions();

                isWidthResizing.current = width !== windowDimensions.width;
                isHeightResizing.current = height !== windowDimensions.height;
                isResizing.current = true;

                setWindowDimensions({ width, height });

                window.clearTimeout(timeout);
                timeout = window.setTimeout(() => {
                    isWidthResizing.current = false;
                    isHeightResizing.current = false;
                    isResizing.current = false;
                }, 250);
            });
        }

        return (): void => window.removeEventListener('resize', () => setWindowDimensions(getWindowDimensions()));
    }, [getWindowDimensions, hasWindow, setWindowDimensions, windowDimensions.height, windowDimensions.width]);

    return {
        ...windowDimensions,
        isResizing: isResizing.current,
        isWidthResizing: isWidthResizing.current,
        isHeightResizing: isHeightResizing.current,
    };
};
