import { useEffect, useState, ReactElement } from 'react';
import { OverflowTooltipFontProps } from '../ui-kit';

type ReturnValue = {
    isTextOverflow: boolean;
};

export const useTextOverflow = (
    children: ReactElement | null,
    { fontSize, fontWeight, fontFamily = 'Inter, sans-serif', fontStyle = 'normal' }: OverflowTooltipFontProps,
): ReturnValue => {
    const [isTextOverflow, setIsTextOverflow] = useState(false);

    useEffect(() => {
        if (children) {
            const copy = document.createElement('span');
            copy.innerText = children.props.value;
            copy.style.visibility = 'hidden';
            copy.style.fontSize = fontSize;
            copy.style.fontWeight = fontWeight;
            copy.style.fontFamily = fontFamily;
            copy.style.fontStyle = fontStyle;
            document.getElementById('app')?.appendChild(copy);

            const elements = document.getElementsByClassName(children.props.className);
            const arr = Array.from(elements);
            const element = arr[arr.length - 1];

            if (element) {
                setIsTextOverflow(copy.offsetWidth > element.clientWidth);
            }

            copy.remove();
        }
    }, [children, fontFamily, fontSize, fontStyle, fontWeight]);

    return {
        isTextOverflow,
    };
};
