import { useState, useEffect } from 'react';
import { debounceTime, Subject } from 'rxjs';

export const useDebouncedState = <T = any>(defaultValue: T, delay: number): [T, (v: T) => void] => {
    const [value, setValue] = useState<T>(defaultValue);
    const [value$] = useState(() => new Subject<T>());

    useEffect(() => {
        const sub = value$.pipe(debounceTime(delay)).subscribe(setValue);
        return () => sub.unsubscribe();
    }, [delay, value$]);

    return [value, (v) => value$.next(v)];
};
