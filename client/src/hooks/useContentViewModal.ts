import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { ViewItemType } from '../components/ModalsContainer/types';
import {
    selectContentViewIndex,
    selectContentViewItems,
    selectContentViewModalStatus,
} from '../components/ModalsContainer/selectors';
import { setCloseContentViewModal, setContentViewIndex } from '../components/ModalsContainer/actions';
import { useWindowDimensions } from './useWindowDimensions';
import isVideo from '../../../common/helpers/isVideo';
import { CONTENT_TYPE } from '../../../common/types';
import isImage from '../../../common/helpers/isImage';

type ReturnValue = {
    isContentViewModalOpen: boolean;
    viewItem: ViewItemType;
    moveToRight: () => void;
    moveToLeft: () => void;
    closeImageViewModal: () => void;
    isHorizontalView: boolean;
    isBackForwardButtonsShown: boolean;
    windowWidth: number;
    windowHeight: number;
};

export const useContentViewModal = (): ReturnValue => {
    const dispatch = useDispatch();
    const currentViewIndex = useSelector(selectContentViewIndex);
    const viewItems = useSelector(selectContentViewItems).map((item) =>
        item.type
            ? item
            : {
                  ...item,
                  type: isVideo(item.src)
                      ? CONTENT_TYPE.VIDEO
                      : isImage(item.src)
                        ? CONTENT_TYPE.IMAGE
                        : CONTENT_TYPE.UNKNOWN,
              },
    );
    const isContentViewModalOpen = useSelector(selectContentViewModalStatus);

    const { width: windowWidth, height: windowHeight } = useWindowDimensions();

    const closeImageViewModal = useCallback(() => {
        dispatch(setCloseContentViewModal());
    }, [dispatch]);

    const moveToLeft = useCallback(() => {
        if (currentViewIndex > 0) {
            dispatch(setContentViewIndex(currentViewIndex - 1));
        } else if (currentViewIndex === 0) {
            dispatch(setContentViewIndex(viewItems.length - 1));
        }
    }, [currentViewIndex, dispatch, viewItems.length]);

    const moveToRight = useCallback(() => {
        if (currentViewIndex < viewItems.length - 1) {
            dispatch(setContentViewIndex(currentViewIndex + 1));
        } else {
            dispatch(setContentViewIndex(0));
        }
    }, [currentViewIndex, dispatch, viewItems.length]);

    const viewItem = useMemo(() => viewItems[currentViewIndex], [currentViewIndex, viewItems]);

    const isHorizontalView = useMemo(
        () => (viewItem?.dimensions ? viewItem?.dimensions?.width > viewItem?.dimensions?.height : true),

        [viewItem?.dimensions],
    );

    // закрываем окно при клике на любой области вне окна
    useEffect(() => {
        if (isContentViewModalOpen) {
            document.addEventListener('click', closeImageViewModal, false);
        }

        return () => {
            document.removeEventListener('click', closeImageViewModal, false);
        };
    }, [closeImageViewModal, isContentViewModalOpen]);

    const isBackForwardButtonsShown = viewItems.length > 1;

    return {
        isContentViewModalOpen,
        viewItem,
        moveToLeft,
        moveToRight,
        closeImageViewModal,
        isHorizontalView,
        isBackForwardButtonsShown,
        windowWidth,
        windowHeight,
    };
};
