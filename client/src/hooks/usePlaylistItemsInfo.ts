import { useMemo } from 'react';

import { PlaylistItemType, CONTENT_TYPE } from '../../../common/types';
import { getFilesizeString } from '../@pages/PlaylistsPage/helpers';
import { formatSecondsToTimeString } from '../../../common/helpers';

type ReturnValue = {
    playlistItemsDuration: string;
    playlistItemsFilesSize: string;
};

// общее время, общий размер

export const usePlaylistItemsInfo = (playlistItems: PlaylistItemType[]): ReturnValue => {
    const playlistItemsDuration = useMemo(() => {
        const duration =
            playlistItems
                .map(({ delay, content }) => ({
                    delay,
                    duration: content?.duration ?? 0,
                    type: content?.type ?? 'unknown',
                }))
                .reduce(
                    (prev, { delay, duration, type }) =>
                        type === CONTENT_TYPE.IMAGE || type === CONTENT_TYPE.WEB ? prev + delay : prev + duration,
                    0,
                ) ?? 0;
        return formatSecondsToTimeString(duration ?? 0);
    }, [playlistItems]);

    const playlistItemsFilesSize = useMemo(() => {
        const uniqueContentIds = new Set<string>();

        let totalSize = 0;

        playlistItems.forEach((item) => {
            const content = item.content;
            if (content && !uniqueContentIds.has(content.id)) {
                uniqueContentIds.add(content.id);
                totalSize += content.size || 0;
            }
        });

        return getFilesizeString(totalSize);
    }, [playlistItems]);

    return {
        playlistItemsDuration,
        playlistItemsFilesSize,
    };
};
