import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { format } from 'date-fns';

import { httpService } from '../helpers/httpService';
import { selectAuthOrganizationInfo } from '../selectors';

let timeout: number;
let seconds = 0;
let startTime = 0;
let organizationId = '';

const fetchTime = () => {
    if (seconds > 0) {
        httpService
            .post(`organization/${organizationId}/time`, {
                date: format(new Date(), 'yyyy-MM-dd'),
                seconds: Math.round(seconds),
            })
            .catch(() => {})
            .finally(() => {
                startTime = 0;
                seconds = 0;
            });
    }
};

const onClickHandler = () => {
    window.clearTimeout(timeout);

    startTime = startTime || Date.now();
    seconds = (Date.now() - startTime) / 1000;

    timeout = window.setTimeout(fetchTime, 30000);
};

export const useOrganizationTimeUsage = (): void => {
    const { id, role } = useSelector(selectAuthOrganizationInfo);

    useEffect(() => {
        if (id && role === 'user') {
            organizationId = id;

            window.addEventListener('visibilitychange', fetchTime, false);
            document.addEventListener('click', onClickHandler, false);
        } else {
            if (organizationId) {
                fetchTime();

                window.removeEventListener('visibilitychange', fetchTime, false);
                document.removeEventListener('click', onClickHandler, false);

                organizationId = '';
            }
        }
    }, [id, role]);
};
