import { MouseEvent as ReactMouseEvent, RefObject, useCallback, useEffect, useRef, useState, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectCurrentOrganization } from '../@pages/ProfilePage/selectors';
import { selectOnBoardingStatus, selectAuthUserRole } from '../selectors';
import {
    setDisableOnboarding,
    setEnableOnboarding,
    setHelpIconRef,
    setOnBoardingToggleRef,
    setSignOutProcess,
} from '../actions';
import { setShowModal } from '../components/ModalsContainer/actions';
import { MODALS } from '../components/ModalsContainer/types';
import { setProfileTab } from '../@pages/ProfilePage/actions';
import { ActivePage } from '../components/OrganizationContainer/types';
import { isProductionEngClient } from '../constants/environments';
import { useCustomRouter } from './useCustomRouter';
import { ProfileTabType } from '../@pages/ProfilePage/types';
import { USER_ROLE } from '../constants';
import { usePlaylistStep } from '../@pages/PlaylistsPage/hooks/usePlaylistStep';

type ReturnType = {
    organizationEmail: string;
    headerTitle: string;
    userRole: USER_ROLE;
    isHeaderMenuShown: boolean;
    goToMainPage: () => void;
    goToProfileTab: (tab: ProfileTabType) => () => void;
    goToInfoPage: () => void;
    showHideHeaderMenu: (e: ReactMouseEvent<HTMLDivElement>) => void;
    handleLogout: () => void;

    isOnboardingEnabled: boolean;
    toggleOnboarding: () => void;
    onBoardingToggleRef: RefObject<HTMLDivElement>;
    helpIconRef: RefObject<SVGSVGElement>;
    isOrganizationsPage: boolean;

    isRuLang: boolean;
    t: TFunction;
};

export const useMainHeader = (): ReturnType => {
    const { t } = useTranslation('translation', { keyPrefix: 'header' });

    const dispatch = useDispatch();

    const { playlistStep } = usePlaylistStep();

    const { pushToPage, currentPage: page } = useCustomRouter();

    const {
        email: organizationEmail,
        name: organizationName,
        surname: organizationSurname,
        language,
    } = useSelector(selectCurrentOrganization);
    const userRole = useSelector(selectAuthUserRole);

    const [isHeaderMenuShown, setIsHeaderMenuShown] = useState(false);

    const onBoardingToggleRef = useRef<HTMLDivElement>(null);
    const helpIconRef = useRef<SVGSVGElement>(null);

    const isRuLang = isProductionEngClient ? false : language === 'ru';

    const isOrganizationsPage = page === 'organizations';

    const headerTitle = useMemo(
        () =>
            organizationName && organizationSurname
                ? `${organizationName} ${organizationSurname}`
                : t('nameNotProvided'),
        [organizationName, organizationSurname, t],
    );

    useEffect(() => {
        if (onBoardingToggleRef.current) {
            dispatch(setOnBoardingToggleRef(onBoardingToggleRef.current));
        }
        dispatch(setHelpIconRef(isRuLang ? helpIconRef.current : null));
    }, [dispatch, isRuLang]);

    //  в любом месте документа - убираем меню
    useEffect(() => {
        let listener: (e: MouseEvent) => void;

        if (isHeaderMenuShown) {
            listener = () => {
                setIsHeaderMenuShown(false);
            };

            document.addEventListener('click', listener, false);
        }

        return () => {
            document.removeEventListener('click', listener, false);
        };
    }, [isHeaderMenuShown]);

    // изменение страницы - убираем меню
    useEffect(() => {
        if (page) {
            setIsHeaderMenuShown(false);
        }
    }, [page]);

    const goToPage = useCallback(
        (page: ActivePage) => {
            if (playlistStep) {
                dispatch(setShowModal(MODALS.CANCEL_CREATION_PLAYLIST, '', page));
            } else {
                pushToPage(page);
            }
        },
        [dispatch, playlistStep, pushToPage],
    );

    const goToMainPage = useCallback(() => {
        goToPage('main');
    }, [goToPage]);

    const goToProfileTab = useCallback(
        (tab: ProfileTabType) => () => {
            goToPage('profile');
            dispatch(setProfileTab(tab));
        },
        [dispatch, goToPage],
    );

    const goToInfoPage = useCallback(() => {
        goToPage('info');
    }, [goToPage]);

    const handleLogout = useCallback(() => {
        if (playlistStep) {
            dispatch(setShowModal(MODALS.CANCEL_CREATION_PLAYLIST, '', 'signin'));
        } else {
            localStorage.removeItem('lastPage');
            dispatch(setSignOutProcess());
        }
    }, [dispatch, playlistStep]);

    const showHideHeaderMenu = useCallback(
        (event: ReactMouseEvent<HTMLDivElement>) => {
            event.stopPropagation();
            setIsHeaderMenuShown(!isHeaderMenuShown);
        },
        [isHeaderMenuShown],
    );

    const isOnboardingEnabled = useSelector(selectOnBoardingStatus);

    const toggleOnboarding = useCallback(() => {
        if (isOnboardingEnabled) {
            dispatch(setDisableOnboarding());
        } else {
            dispatch(setEnableOnboarding());
        }
    }, [dispatch, isOnboardingEnabled]);

    return {
        handleLogout,
        organizationEmail,
        headerTitle,
        userRole,
        isHeaderMenuShown,
        showHideHeaderMenu,
        isOnboardingEnabled,
        toggleOnboarding,
        goToMainPage,
        goToProfileTab,
        goToInfoPage,
        onBoardingToggleRef,
        helpIconRef,
        isRuLang,
        isOrganizationsPage,
        t,
    };
};
