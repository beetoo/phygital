import { useCallback, useRef } from 'react';

type ReturnValue = [onSingleClickHandler: () => void, onDoubleClickHandler: () => void];

export const useSingleDoubleClick = (singleClickFunc: () => unknown, doubleClickFunc: () => unknown): ReturnValue => {
    const delay = 200;

    const timerRef = useRef(0);
    const preventRef = useRef(false);

    const onSingleClickHandler = useCallback(() => {
        timerRef.current = window.setTimeout(() => {
            if (!preventRef.current) {
                singleClickFunc();
            }
        }, delay);
    }, [singleClickFunc]);

    const onDoubleClickHandler = useCallback(() => {
        window.clearTimeout(timerRef.current);
        preventRef.current = true;
        doubleClickFunc();
        window.setTimeout(() => {
            preventRef.current = false;
        }, delay);
    }, [doubleClickFunc]);

    return [onSingleClickHandler, onDoubleClickHandler];
};
