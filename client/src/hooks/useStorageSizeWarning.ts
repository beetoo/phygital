import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TFunction } from 'i18next';
import { useTranslation } from 'react-i18next';

import { setHideModal, setShowModalNew } from '../components/ModalsContainer/actions';
import { MODALS } from '../components/ModalsContainer/types';
import { selectCurrentOrganization } from '../@pages/ProfilePage/selectors';

type ReturnValue = {
    isWarningShown: boolean;
    storageUsagePercent: number;
    closeModal: () => void;
    openSupportModal: () => void;
    closeWarning: () => void;
    t: TFunction;
};

export const useStorageSizeWarning = (alreadyUploadedFilesSize: number): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'contentModal' });

    const dispatch = useDispatch();

    const { limitStorageSize } = useSelector(selectCurrentOrganization);

    const storageUsagePercent = Math.round((alreadyUploadedFilesSize * 100) / limitStorageSize);

    const [isWarningShown, setIsWarningShown] = useState(false);

    const closeModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    const openSupportModal = useCallback(() => {
        dispatch(
            setShowModalNew(MODALS.SUPPORT_MODAL, {
                onClose: () => {
                    dispatch(setShowModalNew(MODALS.UPLOAD_CONTENT_MODAL, null));
                },
            }),
        );
    }, [dispatch]);

    const closeWarning = useCallback(() => {
        localStorage.setItem('size_warning_closed', 'true');
        setIsWarningShown(false);
    }, []);

    useEffect(() => {
        if (storageUsagePercent >= 50) {
            setIsWarningShown(!localStorage.getItem('size_warning_closed'));
        } else {
            setIsWarningShown(false);
            localStorage.removeItem('size_warning_closed');
        }
    }, [storageUsagePercent]);

    return {
        isWarningShown,
        storageUsagePercent,
        closeModal,
        openSupportModal,
        closeWarning,
        t,
    };
};
