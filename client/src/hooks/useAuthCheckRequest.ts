import { useEffect } from 'react';
import { useMatch } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';

import { selectCurrentOrganization } from '../@pages/ProfilePage/selectors';
import { setAuthCheckProcess } from '../actions';
import { selectAuthCheckingStatus, selectAuthStatus } from '../selectors';
import { httpService } from '../helpers/httpService';
import {
    isDevelopmentClient,
    isDevProductionClient,
    isProductionClient,
    isProductionEngClient,
} from '../constants/environments';

type Args = {
    authChecking: boolean;
    isAuth: boolean;
};

export const useAuthCheckRequest = (): Args => {
    const dispatch = useDispatch();

    const confirmRegistrationMatch = useMatch('/confirm-registration/*');
    const recoveryPasswordMatch = useMatch('/recovery-password/*');

    const authChecking = useSelector(selectAuthCheckingStatus);
    const isAuth = useSelector(selectAuthStatus);
    const { id: organizationId, email, phone, name } = useSelector(selectCurrentOrganization);

    useEffect(() => {
        let interval: number;

        if (organizationId) {
            if (isProductionClient || isDevProductionClient || isDevelopmentClient) {
                httpService.get(`organization/token/${organizationId}`).then((token) => {
                    interval = window.setInterval(() => {
                        const jivo_api = (window as any)?.jivo_api;

                        if (jivo_api) {
                            jivo_api.setUserToken(token);
                            jivo_api.setContactInfo({
                                name,
                                email,
                                phone,
                            });
                            window.clearInterval(interval);
                        }
                    }, 200);
                });
            }
        }
    }, [email, name, organizationId, phone]);

    useEffect(() => {
        if (confirmRegistrationMatch || recoveryPasswordMatch) {
            return;
        }
        dispatch(setAuthCheckProcess());
    }, [confirmRegistrationMatch, dispatch, recoveryPasswordMatch]);

    const { i18n } = useTranslation();
    const { language } = useSelector(selectCurrentOrganization);

    useEffect(() => {
        if (isProductionEngClient) {
            return;
        }

        if (isAuth) {
            if (language) {
                i18n.changeLanguage(language);
            }
        } else {
            const navigatorLanguage = window.navigator.language;
            if (navigatorLanguage) {
                const language = navigatorLanguage.includes('ru')
                    ? 'ru'
                    : navigatorLanguage.includes('es')
                      ? 'es'
                      : 'en';
                i18n.changeLanguage(language);
            }
        }
    }, [i18n, isAuth, language]);

    return { authChecking, isAuth };
};
