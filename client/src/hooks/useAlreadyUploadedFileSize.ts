import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { httpService } from '../helpers/httpService';
import { selectCurrentOrganization } from '../@pages/ProfilePage/selectors';
import { setGetOrganizationSuccess } from '../@pages/ProfilePage/actions';

type ReturnValue = {
    alreadyUploadedFilesSize: number;
    limitStorageSize: number;
    isStorageLimitExceeded: boolean;
};

export const useAlreadyUploadedFileSize = (): ReturnValue => {
    const dispatch = useDispatch();

    const organization = useSelector(selectCurrentOrganization);

    const { id: organizationId, limitStorageSize } = organization;

    const savedFilesSize = Number(localStorage.getItem('fs') ?? '0');
    const prevUploadedFilesSize = isNaN(savedFilesSize) ? 0 : savedFilesSize;

    const [alreadyUploadedFilesSize, setAlreadyUploadedFilesSize] = useState(prevUploadedFilesSize);

    useEffect(() => {
        if (!organizationId) return;

        httpService.get(`organization/${organizationId}/storage`).then((data) => {
            const { hasActiveSubscriptions, ...response } = data as {
                used: number;
                total: number;
                hasActiveSubscriptions: boolean;
            };

            const used = response.used / (1024 * 1024);
            const limitStorageSize = response.total / (1024 * 1024);

            if (prevUploadedFilesSize !== Math.round(used)) {
                setAlreadyUploadedFilesSize(Math.round(used));
                localStorage.setItem('fs', used.toString());
            }

            if (organization.limitStorageSize !== limitStorageSize) {
                dispatch(setGetOrganizationSuccess({ ...organization, limitStorageSize }));
            }

            if (organization.hasActiveSubscriptions !== hasActiveSubscriptions) {
                dispatch(setGetOrganizationSuccess({ ...organization, hasActiveSubscriptions }));
            }
        });
    }, [dispatch, limitStorageSize, organization, organizationId, prevUploadedFilesSize]);

    const isStorageLimitExceeded = alreadyUploadedFilesSize >= organization.limitStorageSize;

    return { alreadyUploadedFilesSize, limitStorageSize, isStorageLimitExceeded };
};
