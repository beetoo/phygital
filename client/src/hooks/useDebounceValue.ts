import { useEffect, useState } from 'react';

export const useDebounceValue = (value: unknown, delay: number): any => {
    const [debouncedValue, setDebouncedValue] = useState(value);

    useEffect(() => {
        const timeout = window.setTimeout(() => {
            setDebouncedValue(value);
        }, delay);

        return () => {
            window.clearTimeout(timeout);
        };
    }, [delay, value]);

    return debouncedValue;
};
