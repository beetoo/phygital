import React, { ReactNode } from 'react';
import Modal from 'react-modal';

type Props = {
    children: ReactNode;
    isOpen: boolean;
    overlayBackground?: string;
};

const customStyles = (
    overlayBackground = 'rgba(21, 22, 29, 0.8)',
    // overlayBackground = 'rgba(15, 16, 31, 0.51)'
) => ({
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',

        width: '100%',
        height: '100%',

        background: 'transparent',
    },
    overlay: {
        background: overlayBackground,
        zIndex: 999,
    },
});

const ModalTemplate: React.FC<Props> = ({ children, isOpen, overlayBackground }: Props) => {
    return isOpen ? (
        <Modal isOpen={isOpen} style={customStyles(overlayBackground)} ariaHideApp={false}>
            {children}
        </Modal>
    ) : null;
};

export default ModalTemplate;
