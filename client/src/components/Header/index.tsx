import React from 'react';
import { clsx } from 'clsx';

import { Tooltip } from '../../ui-kit';
import { useHeaderPlaylistStepMode } from '../../hooks/useHeaderPlaylistStepMode';

import css from './style.m.css';

const MainHeader: React.FC = () => {
    const {
        isPlaylistStepModeEnabled,
        isCreatePlaylistStep,
        isPlaylistStepOne,
        isPlaylistStepTwo,
        isPlaylistStepThree,

        playlistTitle,
        isThirdStepEnabled,

        moveToPlaylistStepOne,
        moveToPlaylistStepTwo,
        moveToPlaylistStepThree,

        t,
    } = useHeaderPlaylistStepMode();

    return isPlaylistStepModeEnabled ? (
        <header className={css.mainHeader}>
            <div className={css.mainHeaderSteps}>
                <div className={css.wrapperBlockCreatePlaylist}>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <div style={{ display: 'flex' }}>
                            <div className={css.wrapCreatePlaylistName}>
                                <h2 className={css.createPlaylistName}>
                                    {isCreatePlaylistStep ? `${t('playlistCreation')}` : `${t('playlistEditing')}`}
                                    <Tooltip title={playlistTitle} placement="bottom">
                                        <span>{playlistTitle}</span>
                                    </Tooltip>
                                </h2>
                            </div>

                            <div className={css.wrapperSteps}>
                                <div
                                    className={clsx(
                                        css.blockCreatePlaylistSteps,
                                        css.blockCreatePlaylistStepDivider,
                                        css.blockCreatePlaylistStepDividerCurrent,
                                        !isPlaylistStepOne && css.stepEnabled,
                                    )}
                                    onClick={moveToPlaylistStepOne}
                                >
                                    <p
                                        className={clsx(
                                            css.createPlaylistStepNumber,
                                            isPlaylistStepOne
                                                ? css.createPlaylistStepNumberCurrent
                                                : css.createPlaylistStepNumberPrevious,
                                        )}
                                    >
                                        1
                                    </p>
                                    <p className={clsx(css.createPlaylistStepName, css.createPlaylistStepNameCurrent)}>
                                        {t('screens')}
                                    </p>
                                </div>
                                <div
                                    className={clsx(
                                        css.blockCreatePlaylistSteps,
                                        css.blockCreatePlaylistStepDivider,
                                        isThirdStepEnabled && css.blockCreatePlaylistStepDividerCurrent,
                                        !isPlaylistStepTwo && css.stepEnabled,
                                    )}
                                    onClick={moveToPlaylistStepTwo}
                                >
                                    <p
                                        className={clsx(css.createPlaylistStepNumber, {
                                            [css.createPlaylistStepNumberCurrent]: isPlaylistStepTwo,
                                            [css.createPlaylistStepNumberPrevious]: !isPlaylistStepTwo,
                                        })}
                                    >
                                        2
                                    </p>
                                    <p className={clsx(css.createPlaylistStepName, css.createPlaylistStepNameCurrent)}>
                                        {t('content')}
                                    </p>
                                </div>
                                <div
                                    className={clsx(css.blockCreatePlaylistSteps, {
                                        [css.stepEnabled]: !isPlaylistStepThree && isThirdStepEnabled,
                                    })}
                                    onClick={moveToPlaylistStepThree}
                                >
                                    <p
                                        className={clsx(css.createPlaylistStepNumber, {
                                            [css.createPlaylistStepNumberCurrent]: isPlaylistStepThree,
                                            [css.createPlaylistStepNumberPrevious]:
                                                !isPlaylistStepThree && isThirdStepEnabled,
                                        })}
                                    >
                                        3
                                    </p>

                                    <p
                                        className={clsx(
                                            css.createPlaylistStepName,
                                            isThirdStepEnabled && css.createPlaylistStepNameCurrent,
                                        )}
                                    >
                                        {t('schedule')}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    ) : null;
};

export default MainHeader;
