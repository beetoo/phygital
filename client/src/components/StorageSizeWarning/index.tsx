import React from 'react';
import { Warning, X } from '@phosphor-icons/react';

import { useStorageSizeWarning } from '../../hooks/useStorageSizeWarning';

import css from './style.m.css';

type Props = {
    alreadyUploadedFilesSize: number;
};

const StorageSizeWarning: React.FC<Props> = ({ alreadyUploadedFilesSize }) => {
    const { isWarningShown, storageUsagePercent, closeModal, openSupportModal, closeWarning, t } =
        useStorageSizeWarning(alreadyUploadedFilesSize);

    return isWarningShown ? (
        <div className={css.containerInfo}>
            <div className={css.containerInfoText}>
                <div className={css.containerInfoSvg}>
                    <Warning size={24} weight="bold" />
                </div>
                <p>
                    {t('availableSpaceHasBeenUsed', { percent: storageUsagePercent })}
                    {/*Использовано {isLimitReached ? 100 : storageUsagePercent}% доступного пространства. Когда свободное*/}
                    {/*место закончится, вы не сможете создавать, редактировать и загружать файлы.*/}
                </p>

                <div onClick={closeWarning}>
                    <X className={css.closeButton} size={16} weight="bold" />
                </div>
            </div>
            <div className={css.containerInfoBtn}>
                <button onClick={closeModal} className={css.btnLimit}>
                    {t('freeUpStorageSpace')}
                </button>
                <button onClick={openSupportModal} className={css.btnLimit}>
                    {t('expandStorage')}
                </button>
            </div>
        </div>
    ) : null;
};

export default StorageSizeWarning;
