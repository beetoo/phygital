import React from 'react';
import ErrorRoundedIcon from '@mui/icons-material/ErrorRounded';
import { useTranslation } from 'react-i18next';

import { Snackbar } from '../../ui-kit';

export const ErrorSnackbar: React.FC<{ onClose: () => void }> = ({ onClose }) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    return (
        <Snackbar
            startAdornment={<ErrorRoundedIcon color="error" />}
            title={t('errorTryAgain') + '.'}
            onClose={onClose}
        />
    );
};
