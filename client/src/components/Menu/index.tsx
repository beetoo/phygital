import React from 'react';
import { clsx } from 'clsx';

import LogoSmall from '../../assets/logos/LogoSquare.svg';

import { useMenu } from './hooks/useMenu';
import { SignageMenuNarrow } from './components/SignageMenuNarrow';
import { SignageMenu } from './components/SignageMenu';

import css from './style.m.css';

const Menu: React.FC = () => {
    const {
        phygitalMode,
        isPlaylistStepModeEnabled,
        isNarrowMenu,
        language,
        logoImage,
        onLogoClick,
        isHeaderMenuShown,
        showHideHeaderMenu,
    } = useMenu();

    return (
        <>
            {isNarrowMenu ? (
                <div className={clsx(css.menuNavigationCreatePlaylist, isPlaylistStepModeEnabled && css.stepMode)}>
                    <div className={css.containerLogoType}>
                        <a onClick={onLogoClick}>
                            <img src={LogoSmall} width="39" height="44" alt="" draggable="false" />
                        </a>
                    </div>

                    {phygitalMode === 'signage' && (
                        <SignageMenuNarrow
                            isHeaderMenuShown={isHeaderMenuShown}
                            showHideHeaderMenu={showHideHeaderMenu}
                        />
                    )}
                </div>
            ) : (
                <div className={css.menuNavigation}>
                    <div className={css.containerLogoType}>
                        <a onClick={onLogoClick}>
                            <img src={logoImage} width={language === 'ru' ? 144 : 122} alt="" draggable="false" />
                        </a>
                    </div>

                    {phygitalMode === 'signage' && (
                        <SignageMenu isHeaderMenuShown={isHeaderMenuShown} showHideHeaderMenu={showHideHeaderMenu} />
                    )}
                </div>
            )}
        </>
    );
};

export default Menu;
