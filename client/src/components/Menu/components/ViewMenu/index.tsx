import React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { clsx } from 'clsx';

import { useCustomRouter } from '../../../../hooks/useCustomRouter';
import { isDevelopmentMode } from '../../../../constants/environments';
import { selectAuthUserRole } from '../../../../selectors';

import css from '../../style.m.css';

export const ViewMenu: React.FC = () => {
    const { selectPage, currentPage } = useCustomRouter();

    const userRole = useSelector(selectAuthUserRole);

    const { t } = useTranslation('translation', { keyPrefix: 'menu' });

    return (
        <ul className={css.siteNavigation}>
            {(userRole === 'admin' || isDevelopmentMode) && (
                <li className={clsx(css.siteNavigationItem, css.siteNavigationDisabled)}>
                    <a className={css.iconVideoCamera}>
                        {t('connectedCameras')}
                        {/* Подключенные камеры */}
                    </a>
                    <span className={css.wrapperCreateTriggerStatus}>
                        {t('soon')}
                        {/* Скоро */}
                    </span>
                </li>
            )}
            {(userRole === 'admin' || isDevelopmentMode) && (
                <li
                    className={clsx(
                        css.siteNavigationItem,
                        currentPage === 'uploaded_videos' && css.siteNavigationCurrent,
                    )}
                >
                    <a className={css.iconYoutubeLogo} onClick={selectPage('uploaded_videos')}>
                        {t('uploadedVideos')}
                        {/* Загруженные видео */}
                    </a>
                </li>
            )}
        </ul>
    );
};
