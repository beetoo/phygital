import React, { MouseEvent as ReactMouseEvent } from 'react';
import { Buildings, Lifebuoy, Wrench, Globe } from '@phosphor-icons/react';
import { useTranslation } from 'react-i18next';
import { clsx } from 'clsx';

import { Tooltip } from '../../../../ui-kit';
import { useCustomRouter } from '../../../../hooks/useCustomRouter';
import { useSupportModal } from '../../hooks/useSupportModal';
import { isDevelopmentMode } from '../../../../constants/environments';
import { HouseIcon, DesktopIcon, PlayIcon, FolderOpenIcon, CalendarIcon } from '../../icons';
import ProfileDropdown from '../ProfileDropdown';
import signUserIcon from '../../../../assets/signUser.svg';

import css from '../../style.m.css';

type Props = {
    isHeaderMenuShown: boolean;
    showHideHeaderMenu: (e: ReactMouseEvent<HTMLDivElement>) => void;
};

export const SignageMenuNarrow: React.FC<Props> = ({ isHeaderMenuShown, showHideHeaderMenu }) => {
    const { t } = useTranslation('translation', { keyPrefix: 'menu' });

    const { selectPage, currentPage, isAdminOrManager } = useCustomRouter();

    const { openSupportModal } = useSupportModal();

    return (
        <div className={css.containerSiteNavigation}>
            <ul className={css.siteNavigation}>
                {isAdminOrManager && (
                    <div className={css.tooltipSiteNavigation} onClick={selectPage('organizations', true)}>
                        <li className={clsx(currentPage === 'organizations' && css.siteNavigationCurrent)}>
                            <Tooltip title={t('organizations')} placement="right">
                                <a style={{ height: '24px' }}>
                                    <Buildings size={24} />
                                </a>
                            </Tooltip>
                        </li>
                    </div>
                )}
                {currentPage !== 'organizations' && (
                    <>
                        <div className={css.tooltipSiteNavigation} onClick={selectPage('main')}>
                            <li className={clsx(currentPage === 'main' && css.siteNavigationCurrent)}>
                                <Tooltip title={t('main')} placement="right">
                                    <a>
                                        <HouseIcon />
                                    </a>
                                </Tooltip>
                            </li>
                        </div>
                        {isDevelopmentMode && (
                            <div className={css.tooltipSiteNavigation} onClick={selectPage('main-new')}>
                                <li className={clsx(currentPage === 'main-new' && css.siteNavigationCurrent)}>
                                    <Tooltip title={t('main') + ' (new)'} placement="right">
                                        <a>
                                            <HouseIcon />
                                        </a>
                                    </Tooltip>
                                </li>
                            </div>
                        )}
                        <div className={css.tooltipSiteNavigation} onClick={selectPage('screens')}>
                            <li className={clsx(currentPage === 'screens' && css.siteNavigationCurrent)}>
                                <Tooltip title={t('screens')} placement="right">
                                    <a>
                                        <DesktopIcon />
                                    </a>
                                </Tooltip>
                            </li>
                        </div>
                        <div className={css.tooltipSiteNavigation} onClick={selectPage('playlists')}>
                            <li className={clsx(currentPage === 'playlists' && css.siteNavigationCurrent)}>
                                <Tooltip title={t('playlists')} placement="right">
                                    <a>
                                        <PlayIcon />
                                    </a>
                                </Tooltip>
                            </li>
                        </div>
                        <div className={css.tooltipSiteNavigation} onClick={selectPage('content')}>
                            <li className={clsx(currentPage === 'content' && css.siteNavigationCurrent)}>
                                <Tooltip title={t('content')} placement="right">
                                    <a>
                                        <FolderOpenIcon />
                                    </a>
                                </Tooltip>
                            </li>
                        </div>

                        <div className={css.tooltipSiteNavigation} onClick={selectPage('web-content')}>
                            <li className={clsx(currentPage === 'web-content' && css.siteNavigationCurrent)}>
                                <Tooltip title={t('webContent')} placement="right">
                                    <a style={{ height: '24px' }}>
                                        <Globe size={24} />
                                    </a>
                                </Tooltip>
                            </li>
                        </div>

                        <div className={css.tooltipSiteNavigation} onClick={selectPage('dx_schedule')}>
                            <li className={clsx(currentPage === 'dx_schedule' && css.siteNavigationCurrent)}>
                                <Tooltip title={t('schedule')} placement="right">
                                    <a>
                                        <CalendarIcon />
                                    </a>
                                </Tooltip>
                            </li>
                        </div>

                        {isAdminOrManager ? (
                            <div
                                className={clsx(
                                    css.siteNavigationItem,
                                    currentPage === 'administration' && css.siteNavigationCurrent,
                                )}
                                onClick={selectPage('administration')}
                            >
                                <li>
                                    <Tooltip title={t('administration')} placement="right">
                                        <a style={{ height: '24px' }}>
                                            <Wrench size={24} />
                                        </a>
                                    </Tooltip>
                                </li>
                            </div>
                        ) : (
                            <div className={css.tooltipSiteNavigation} onClick={openSupportModal}>
                                <li>
                                    <Tooltip title={t('support')} placement="right">
                                        <a style={{ height: '24px' }}>
                                            <Lifebuoy size={24} />
                                        </a>
                                    </Tooltip>
                                </li>
                            </div>
                        )}
                    </>
                )}
            </ul>
            <ul className={clsx(css.siteNavigation, css.siteNavigationProfile)}>
                <div className={css.tooltipSiteNavigation} onClick={showHideHeaderMenu}>
                    <li>
                        <Tooltip title={t('profile')} placement="right">
                            <a style={{ height: '24px' }}>
                                <img src={signUserIcon} width="24" height="24" alt="" />
                            </a>
                        </Tooltip>
                        <ProfileDropdown isHeaderMenuShown={isHeaderMenuShown} />
                    </li>
                </div>
            </ul>
        </div>
    );
};
