import React, { ComponentProps } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { Tour } from '../../../Tour';
import { TourBody } from '../../../Tour/components/TourBody';
import { TourHeader } from '../../../Tour/components/TourHeader';
import { useCustomRouter } from '../../../../hooks/useCustomRouter';
import { selectScreens } from '../../../../@pages/ScreensPage/selectors';
import { selectPlaylists } from '../../../../@pages/PlaylistsPage/selectors';
import { selectPlaylistsPageLinkRef, selectScreensPageLinkRef } from '../../../../selectors';
import { usePlaylistStep } from '../../../../@pages/PlaylistsPage/hooks/usePlaylistStep';

import learner from '../../images/learner.png';
import teacher from '../../images/teacher.png';

type Steps = ComponentProps<typeof Tour>['steps'];

export const MenuTour: React.FC = () => {
    const { currentPage } = useCustomRouter();

    const { playlistStep } = usePlaylistStep();

    const screens = useSelector(selectScreens);
    const playlists = useSelector(selectPlaylists);

    const screensPageLinkRef = useSelector(selectScreensPageLinkRef);
    const playlistsPageLinkRef = useSelector(selectPlaylistsPageLinkRef);

    const { t } = useTranslation('translation', { keyPrefix: 'learningHints' });

    const steps = React.useMemo<Steps>(
        () => [
            screensPageLinkRef && currentPage === 'playlists' && playlistStep === '' && screens.length === 0
                ? {
                      name: 'screenPageLinkInPlaylists',
                      target: screensPageLinkRef,
                      placement: 'right-start',
                      title: (
                          <TourHeader>
                              <img src={learner} width={32} height={32} alt="" />
                              <div style={{ marginLeft: '15px' }}>
                                  {t('textNine')}
                                  {/* Начните с добавления экрана */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              {t('textTen')}
                              {/* Первым делом добавьте экраны, чтобы создавать и загружать на них плейлисты. */}
                          </TourBody>
                      ),
                  }
                : null,
            playlistsPageLinkRef && currentPage === 'screens' && screens.length > 0 && playlists.length === 0
                ? {
                      name: 'playlistsPageLinkInScreens',
                      target: playlistsPageLinkRef,
                      placement: 'right-start',
                      title: (
                          <TourHeader>
                              <img src={teacher} width={32} height={32} alt="" />
                              <div style={{ marginLeft: '15px' }}>
                                  {t('textFourteen')}
                                  {/* Теперь создайте плейлист */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              {t('textFifteen')}
                              {/* Чтобы запустить контент на экран, создайте плейлист. Для этого перейдите в раздел
                              «Плейлисты». */}
                          </TourBody>
                      ),
                  }
                : null,
        ],
        [currentPage, playlistStep, playlists.length, playlistsPageLinkRef, screens.length, screensPageLinkRef, t],
    );

    return <Tour steps={steps} name="menuTour" />;
};
