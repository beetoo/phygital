import React, { MouseEvent as ReactMouseEvent } from 'react';
import { Buildings, Lifebuoy, Wrench, Globe } from '@phosphor-icons/react';
import { clsx } from 'clsx';

import { useSignageMenu } from '../../hooks/useSignageMenu';
import { useSupportModal } from '../../hooks/useSupportModal';
import { isDevelopmentMode } from '../../../../constants/environments';
import { HouseIcon, DesktopIcon, PlayIcon, FolderOpenIcon, CalendarIcon } from '../../icons';
import signUserIcon from '../../../../assets/signUser.svg';
import ProfileDropdown from '../ProfileDropdown';

import css from '../../style.m.css';

type Props = {
    isHeaderMenuShown: boolean;
    showHideHeaderMenu: (e: ReactMouseEvent<HTMLLIElement>) => void;
};

export const SignageMenu: React.FC<Props> = ({ isHeaderMenuShown, showHideHeaderMenu }) => {
    const { isAdminOrManager, selectPage, currentPage, screensPageLinkRef, playlistsPageLinkRef, t } = useSignageMenu();

    const { openSupportModal } = useSupportModal();

    return (
        <div className={css.containerSiteNavigation}>
            <ul className={css.siteNavigation}>
                {isAdminOrManager && (
                    <li
                        className={clsx(currentPage === 'organizations' && css.siteNavigationCurrent)}
                        onClick={selectPage('organizations', true)}
                    >
                        <a style={{ height: '24px' }}>
                            <Buildings size={24} />
                        </a>
                        <a>{t('organizations')}</a>
                    </li>
                )}
                {currentPage !== 'organizations' && (
                    <>
                        <li
                            className={clsx(currentPage === 'main' && css.siteNavigationCurrent)}
                            onClick={selectPage('main')}
                        >
                            <HouseIcon />
                            <a>{t('main')}</a>
                        </li>
                        {isDevelopmentMode && (
                            <li
                                className={clsx(currentPage === 'main-new' && css.siteNavigationCurrent)}
                                onClick={selectPage('main-new')}
                            >
                                <HouseIcon />
                                <a>{t('main') + ' (new)'}</a>
                            </li>
                        )}
                        <li
                            ref={screensPageLinkRef}
                            className={clsx(currentPage === 'screens' && css.siteNavigationCurrent)}
                            onClick={selectPage('screens')}
                        >
                            <DesktopIcon />
                            <a>{t('screens')}</a>
                        </li>

                        <li
                            ref={playlistsPageLinkRef}
                            className={clsx(currentPage === 'playlists' && css.siteNavigationCurrent)}
                            onClick={selectPage('playlists')}
                        >
                            <PlayIcon />
                            <a>{t('playlists')}</a>
                        </li>
                        <li
                            className={clsx(currentPage === 'content' && css.siteNavigationCurrent)}
                            onClick={selectPage('content')}
                        >
                            <FolderOpenIcon />
                            <a>{t('content')}</a>
                        </li>

                        <li
                            className={clsx(currentPage === 'web-content' && css.siteNavigationCurrent)}
                            onClick={selectPage('web-content')}
                        >
                            <a style={{ height: '24px' }}>
                                <Globe size={24} />
                            </a>
                            <a>{t('webContent')}</a>
                        </li>

                        <li
                            className={clsx(currentPage === 'dx_schedule' && css.siteNavigationCurrent)}
                            onClick={selectPage('dx_schedule')}
                        >
                            <CalendarIcon />
                            <a>{t('schedule')}</a>
                        </li>

                        {isAdminOrManager ? (
                            <li
                                className={clsx(currentPage === 'administration' && css.siteNavigationCurrent)}
                                onClick={selectPage('administration')}
                            >
                                <a style={{ height: '24px' }}>
                                    <Wrench size={24} />
                                </a>
                                <a>{t('administration')}</a>
                            </li>
                        ) : (
                            <li onClick={openSupportModal} style={{ userSelect: 'none' }}>
                                <a style={{ height: '24px' }}>
                                    <Lifebuoy size={24} />
                                </a>
                                <a>{t('support')}</a>
                            </li>
                        )}
                    </>
                )}
            </ul>

            <ul className={clsx(css.siteNavigation, css.siteNavigationProfile)}>
                <li className={clsx(isHeaderMenuShown && css.siteNavigationCurrent)} onClick={showHideHeaderMenu}>
                    <a style={{ height: '24px' }}>
                        <img src={signUserIcon} width="24" height="24" alt="" />
                    </a>
                    <a>{t('profile')}</a>
                    <ProfileDropdown isHeaderMenuShown={isHeaderMenuShown} />
                </li>
            </ul>
        </div>
    );
};
