import React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import { Tooltip } from '../../../../ui-kit';
import { isDevelopmentMode } from '../../../../constants/environments';
import { selectAuthUserRole } from '../../../../selectors';

import css from '../../style.m.css';

export const ViewMenuNarrow: React.FC = () => {
    const userRole = useSelector(selectAuthUserRole);

    const { t } = useTranslation('translation', { keyPrefix: 'menu' });

    return (
        <ul className={css.siteNavigation}>
            {(userRole === 'admin' || isDevelopmentMode) && (
                <div className={css.tooltipSiteNavigation}>
                    <li className={css.siteNavigationItem}>
                        <Tooltip
                            title={t('connectedCameras')}
                            // "Подключенные камеры"
                            placement="right"
                        >
                            <a className={css.iconVideoCamera} />
                        </Tooltip>
                    </li>
                </div>
            )}
            {(userRole === 'admin' || isDevelopmentMode) && (
                <div className={css.tooltipSiteNavigation}>
                    <li className={css.siteNavigationItem}>
                        <Tooltip
                            title={t('uploadedVideos')}
                            // "Загруженные видео"
                            placement="right"
                        >
                            <a className={css.iconYoutubeLogo} />
                        </Tooltip>
                    </li>
                </div>
            )}
        </ul>
    );
};
