import React from 'react';
import { clsx } from 'clsx';

import { useProfileDropdown } from '../../hooks/useProfileDropdown';
import { isSLClientMode } from '../../../../constants/environments';

import css from '../../../Header/style.m.css';

type Props = {
    isHeaderMenuShown: boolean;
};

const ProfileDropdown: React.FC<Props> = ({ isHeaderMenuShown }) => {
    const { isOrganizationsPage, handleLogout, organizationEmail, headerTitle, goToProfileTab, t } =
        useProfileDropdown();

    return (
        <div className={css.dropdown}>
            <div className={clsx(css.profileMenu, isHeaderMenuShown && css.shown)} onClick={(e) => e.stopPropagation()}>
                <h3 className={clsx(css.profileUserName, !organizationEmail && css.noEmail)}>{headerTitle}</h3>
                <span className={css.profileUserLogo}>{(headerTitle?.charAt(0) ?? '').toUpperCase()}</span>
                <p className={css.profileUserEmail}>{organizationEmail}</p>
                {!isOrganizationsPage && (
                    <>
                        <p className={css.profileUser} onClick={goToProfileTab('profile')}>
                            {t('settings')}
                        </p>

                        {!isSLClientMode && (
                            <p className={css.tariffSection} onClick={goToProfileTab('tariff_plan')}>
                                {t('balance')}
                            </p>
                        )}
                    </>
                )}

                <p className={css.logOut} onClick={handleLogout}>
                    {t('exit')}
                </p>
            </div>
        </div>
    );
};

export default ProfileDropdown;
