import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import { setHideModal, setShowModalNew } from '../../ModalsContainer/actions';
import { MODALS } from '../../ModalsContainer/types';
import { TFunction } from 'i18next';
import { selectModalData } from '../../ModalsContainer/selectors';

type ReturnValue = {
    openSupportModal: () => void;
    closeSupportModal: () => void;

    t: TFunction;
};

export const useSupportModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'menu' });

    const dispatch = useDispatch();

    const { onClose } = (useSelector(selectModalData) as { onClose: () => void }) || { onClose: undefined };

    const openSupportModal = useCallback(() => {
        dispatch(setShowModalNew(MODALS.SUPPORT_MODAL, null));
    }, [dispatch]);

    const closeSupportModal = useCallback(() => {
        onClose ? onClose() : dispatch(setHideModal());
    }, [dispatch, onClose]);

    return {
        openSupportModal,
        closeSupportModal,
        t,
    };
};
