import { MouseEvent as ReactMouseEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import logoSignageRus from '../../../assets/logos/logo_signage_rus_lowercase.svg';
import logoEng from '../../../assets/logos/Logo_1.svg';
import { selectTransitionStatus } from '../../../@pages/DxSchedulePage/selectors';
import { setSelectedPlaylistIds, setSelectedPlaylistItemIds } from '../../../@pages/PlaylistsPage/actions';
import {
    setActiveContentItemId,
    setActiveFolderId,
    setSelectedContentItemIds,
} from '../../../@pages/ContentPage/actions';
import { setActiveLocationId, setActiveScreenId, setSelectedScreenIds } from '../../../@pages/ScreensPage/actions';
import { selectAddLocationSuccessStatus, selectAddScreenSuccessStatus } from '../../../@pages/ScreensPage/selectors';
import { setActiveScheduleScreenId } from '../../../@pages/DxSchedulePage/actions';
import { selectAuthUserRole } from '../../../selectors';
import { useCustomRouter } from '../../../hooks/useCustomRouter';
import { useWindowDimensions } from '../../../hooks/useWindowDimensions';
import { selectCurrentOrganization } from '../../../@pages/ProfilePage/selectors';
import { OrganizationLanguageType } from '../../../../../common/types';
import { USER_ROLE } from '../../../constants';
import { usePlaylistStep } from '../../../@pages/PlaylistsPage/hooks/usePlaylistStep';

type ReturnValue = {
    isPlaylistStepModeEnabled: boolean;
    isNarrowMenu: boolean;
    phygitalMode: 'signage';
    language: OrganizationLanguageType;
    logoImage: string;
    onLogoClick: () => void;
    isHeaderMenuShown: boolean;
    showHideHeaderMenu: (e: ReactMouseEvent<HTMLDivElement | HTMLLIElement>) => void;
};

export const useMenu = (): ReturnValue => {
    const dispatch = useDispatch();

    const { currentPage, selectPage } = useCustomRouter();
    const [prevPage, setPrevPage] = useState('');

    const { width } = useWindowDimensions(50);

    const { playlistStep, isPlaylistStepModeEnabled } = usePlaylistStep();

    const { language } = useSelector(selectCurrentOrganization);
    const addLocationSuccess = useSelector(selectAddLocationSuccessStatus);
    const addScreenSuccess = useSelector(selectAddScreenSuccessStatus);
    const hasTransition = useSelector(selectTransitionStatus);
    const userRole = useSelector(selectAuthUserRole);

    const phygitalMode = 'signage';

    const isAdminOrManager = userRole === USER_ROLE.ADMIN || userRole === USER_ROLE.MANAGER;

    const logoImage = language === 'ru' ? logoSignageRus : logoEng;

    const [isHeaderMenuShown, setIsHeaderMenuShown] = useState(false);

    useEffect(() => {
        if (currentPage !== prevPage) {
            dispatch(setSelectedScreenIds());
            dispatch(setSelectedPlaylistIds());
            dispatch(setSelectedPlaylistItemIds());
            dispatch(setSelectedContentItemIds());
            if (!addLocationSuccess || !addScreenSuccess) {
                dispatch(setActiveLocationId('all'));
                dispatch(setActiveScreenId());
            }

            dispatch(setActiveFolderId());
            dispatch(setActiveContentItemId());

            if (!hasTransition) {
                dispatch(setActiveScheduleScreenId());
            }

            setPrevPage(currentPage);
        }
    }, [addLocationSuccess, addScreenSuccess, currentPage, dispatch, hasTransition, prevPage]);

    const isNarrowMenu = useMemo(() => playlistStep !== '' || width <= 1365, [playlistStep, width]);

    const onLogoClick = useCallback(() => {
        if (isAdminOrManager) {
            selectPage('organizations', true);
        } else {
            selectPage('main')();
        }
    }, [isAdminOrManager, selectPage]);

    //  в любом месте документа - убираем меню
    useEffect(() => {
        let listener: (e: MouseEvent) => void;

        if (isHeaderMenuShown) {
            listener = () => {
                setIsHeaderMenuShown(false);
            };

            document.addEventListener('click', listener, false);
        }

        return () => {
            document.removeEventListener('click', listener, false);
        };
    }, [isHeaderMenuShown]);

    // изменение страницы - убираем меню
    useEffect(() => {
        if (currentPage) {
            setIsHeaderMenuShown(false);
        }
    }, [currentPage]);

    const showHideHeaderMenu = useCallback(
        (event: ReactMouseEvent<HTMLDivElement>) => {
            event.stopPropagation();
            setIsHeaderMenuShown(!isHeaderMenuShown);
        },
        [isHeaderMenuShown],
    );

    return {
        isPlaylistStepModeEnabled,
        isNarrowMenu,
        phygitalMode,
        language,
        logoImage,
        onLogoClick,
        isHeaderMenuShown,
        showHideHeaderMenu,
    };
};
