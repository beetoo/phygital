import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectCurrentOrganization } from '../../../@pages/ProfilePage/selectors';
import { selectAuthOrganizationInfo } from '../../../selectors';
import { setSignOutProcess } from '../../../actions';
import { MODALS } from '../../ModalsContainer/types';
import { setProfileTab } from '../../../@pages/ProfilePage/actions';
import { ActivePage } from '../../OrganizationContainer/types';
import { useCustomRouter } from '../../../hooks/useCustomRouter';
import { ProfileTabType } from '../../../@pages/ProfilePage/types';
import { setShowModal } from '../../ModalsContainer/actions';
import { usePlaylistStep } from '../../../@pages/PlaylistsPage/hooks/usePlaylistStep';

type ReturnType = {
    isOrganizationsPage: boolean;
    organizationEmail: string;
    headerTitle: string;
    goToMainPage: () => void;
    goToProfileTab: (tab: ProfileTabType) => () => void;
    handleLogout: () => void;

    t: TFunction;
};

export const useProfileDropdown = (): ReturnType => {
    const { t } = useTranslation('translation', { keyPrefix: 'header' });

    const dispatch = useDispatch();

    const { pushToPage, currentPage } = useCustomRouter();

    const { playlistStep } = usePlaylistStep();

    const organizationInfo = useSelector(selectCurrentOrganization);
    const authOrganizationInfo = useSelector(selectAuthOrganizationInfo);

    const isOrganizationsPage = currentPage === 'organizations';

    const {
        email: organizationEmail,
        name: organizationName,
        surname: organizationSurname,
    } = isOrganizationsPage ? authOrganizationInfo : organizationInfo;

    const headerTitle = useMemo(
        () =>
            organizationName && organizationSurname
                ? `${organizationName} ${organizationSurname}`
                : t('nameNotProvided'),
        [organizationName, organizationSurname, t],
    );

    const goToPage = useCallback(
        (page: ActivePage) => {
            if (playlistStep) {
                dispatch(setShowModal(MODALS.CANCEL_CREATION_PLAYLIST, '', page));
            } else {
                pushToPage(page);
            }
        },
        [dispatch, playlistStep, pushToPage],
    );

    const goToMainPage = useCallback(() => {
        goToPage('main');
    }, [goToPage]);

    const goToProfileTab = useCallback(
        (tab: ProfileTabType) => () => {
            goToPage('profile');
            dispatch(setProfileTab(tab));
        },
        [dispatch, goToPage],
    );

    const handleLogout = useCallback(() => {
        if (playlistStep) {
            dispatch(setShowModal(MODALS.CANCEL_CREATION_PLAYLIST, '', 'signin'));
        } else {
            localStorage.removeItem('lastPage');
            dispatch(setSignOutProcess());
        }
    }, [dispatch, playlistStep]);

    return {
        isOrganizationsPage,
        handleLogout,
        organizationEmail,
        headerTitle,
        goToMainPage,
        goToProfileTab,
        t,
    };
};
