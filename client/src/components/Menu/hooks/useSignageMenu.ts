import { RefObject, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { useCustomRouter } from '../../../hooks/useCustomRouter';
import { selectScreens } from '../../../@pages/ScreensPage/selectors';
import { selectPlaylists } from '../../../@pages/PlaylistsPage/selectors';
import { setPlaylistsPageLinkRef, setScreensPageLinkRef } from '../../../actions';
import { ActivePage } from '../../OrganizationContainer/types';
import { selectAuthUserRole } from '../../../selectors';
import { USER_ROLE } from '../../../constants';

type ReturnValue = {
    currentPage: ActivePage;
    selectPage: (page: ActivePage, isNavigate?: boolean) => () => void;
    screensPageLinkRef: RefObject<HTMLLIElement>;
    playlistsPageLinkRef: RefObject<HTMLLIElement>;
    isAdminOrManager: boolean;
    t: TFunction;
};

export const useSignageMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'menu' });

    const { selectPage, currentPage } = useCustomRouter();

    // Обучение

    const dispatch = useDispatch();

    const screens = useSelector(selectScreens);
    const playlists = useSelector(selectPlaylists);
    const userRole = useSelector(selectAuthUserRole);

    const isAdminOrManager = userRole === USER_ROLE.ADMIN || userRole === USER_ROLE.MANAGER;

    const screensPageLinkRef = useRef<HTMLLIElement>(null);
    const playlistsPageLinkRef = useRef<HTMLLIElement>(null);

    const screensPageLink = screensPageLinkRef.current;
    const playlistPageLink = playlistsPageLinkRef.current;

    useEffect(() => {
        dispatch(setScreensPageLinkRef(screensPageLink));
        dispatch(setPlaylistsPageLinkRef(playlistPageLink));
    }, [dispatch, playlistPageLink, screensPageLink, screens.length, playlists.length]);

    return {
        currentPage,
        selectPage,
        screensPageLinkRef,
        playlistsPageLinkRef,
        isAdminOrManager,
        t,
    };
};
