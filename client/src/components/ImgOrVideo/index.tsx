import React, { CSSProperties, useState } from 'react';

import { getExtension } from '../../../../common/helpers';
import isImage from '../../../../common/helpers/isImage';
import isVideo from '../../../../common/helpers/isVideo';

type ImageProps = {
    src: string;
    style?: CSSProperties;
    width?: string | number;
    height?: string | number;
    title?: string;
    className?: string;
    onLoad?: () => void;
    onError?: () => void;
};

type Props = {
    src: string;
    width?: string | number;
    height?: string | number;
    title?: string;
    classNameForImg?: string;
    classNameForVideo?: string;
    native?: boolean;
};

const Image: React.FC<ImageProps> = ({ src, className, width, height, onLoad, onError, title }) => (
    <img
        className={className}
        src={src}
        width={width}
        height={height}
        onLoad={onLoad}
        onError={onError}
        loading="lazy"
        title={title}
        alt=""
    />
);

const ImgOrVideo: React.FC<Props> = ({
    src,
    width,
    height,
    title,
    classNameForImg,
    classNameForVideo,
    native = false,
}) => {
    const [isLoadedBlur, setIsLoadedBlur] = useState<boolean>(false);
    const [hasBlurError, setHasBlurError] = useState<boolean>(false);
    const [hasThumbnailError, setHasThumbnailError] = useState<boolean>(false);

    const extension = getExtension(src);
    const addedExtension = isImage(src) ? getExtension(src) : '.jpg';

    const blurImage = src?.replace(extension, '_blur' + addedExtension);
    const thumbnailImage = src?.replace(extension, '_thumbnail' + addedExtension);

    if (native) {
        return isImage(src) ? (
            <Image className={classNameForImg} src={src} width={width} height={height} title={title} />
        ) : isVideo(src) ? (
            <video className={classNameForVideo} src={src} width={width} height={height} title={title} />
        ) : null;
    }

    if (hasThumbnailError) {
        return isImage(src) ? (
            <Image className={classNameForImg} src={src} width={width} height={height} title={title} />
        ) : (
            <video className={classNameForVideo} src={src} width={width} height={height} title={title} />
        );
    }

    if (!isLoadedBlur && !hasBlurError) {
        return (
            <Image
                style={{ backdropFilter: 'blur(40px)' }}
                className={classNameForImg}
                src={blurImage}
                onLoad={() => setIsLoadedBlur(true)}
                onError={() => setHasBlurError(true)}
                width={width}
                height={height}
                title={title}
            />
        );
    }

    return (
        <Image
            className={classNameForImg}
            src={thumbnailImage}
            onError={() => setHasThumbnailError(true)}
            width={width}
            height={height}
            title={title}
        />
    );
};

export default ImgOrVideo;
