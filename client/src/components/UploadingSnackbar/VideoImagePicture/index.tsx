import React, { memo } from 'react';

import { CONTENT_TYPE } from '../../../../../common/types';

import css from '../style.m.css';

type Props = {
    type: CONTENT_TYPE;
    src: string;
};

export const VideoImagePicture: React.FC<Props> = memo(({ type, src }) =>
    src ? (
        <div className={css.fieldUploadContentImg}>
            {type === CONTENT_TYPE.IMAGE ? (
                <img style={{ borderRadius: '2px' }} src={src} width="32px" height="32px" alt="" />
            ) : type === CONTENT_TYPE.VIDEO ? (
                <video style={{ borderRadius: '2px' }} src={src} width="32px" height="32px" />
            ) : null}
        </div>
    ) : null,
);

VideoImagePicture.displayName = 'VideoImagePicture';
