import React from 'react';
import { styled } from '@mui/material';
import { clsx } from 'clsx';

import {
    Snackbar,
    Tooltip,
    SelectIcon,
    UploadCancelIcon,
    UploadErrorIcon,
    UploadReloadIcon,
    UploadSuccessIcon,
    UploadWarningIcon,
} from '../../ui-kit';
import { SnackbarProgress } from '../../ui-kit/Snackbar/components/SnackbarProgress';
import { useUploadingSnackbar } from '../../hooks/useUploadingSnackbar';
import { VideoImagePicture } from './VideoImagePicture';

import css from './style.m.css';

const Extra = styled('div')(`max-width: 440px;`);

export const UploadingSnackbar: React.FC<{ title?: string; onClose?: () => void }> = ({ onClose }) => {
    const {
        currentPage,
        isExpanded,
        setExpanded,
        setOvered,
        uploadedFilesNumber,
        allUploadableFilesNumber,
        overallProgress,
        restartUploading,
        deleteFile,
        uploadableFilesWithoutWarnings,
        isMoreThanOneUploadableFile,
        isFileInQueue,
        isFileBeingUploaded,
        isOveredFileInQueueOrBeingUploaded,
        isFileSuccessfullyUploaded,
        isFileWithError,
        isFileOveredWithError,
        isSomeFilesBeingUploaded,
        isAllFilesSuccessfullyUploaded,
        isSomeFilesSuccessfullyUploaded,
        isAllFilesNotUploaded,
        onCloseSnackbar,
        t,
    } = useUploadingSnackbar({ onClose });

    return (
        <Snackbar
            PaperStyle={{ width: '440px' }}
            title={
                isAllFilesNotUploaded
                    ? isMoreThanOneUploadableFile
                        ? `${t('textTen')}`
                        : `${t('textEleven')}`
                    : isAllFilesSuccessfullyUploaded
                      ? isMoreThanOneUploadableFile
                          ? `${t('textNine')}`
                          : `${t('textEight')}`
                      : `${t('textTwelve')} ${uploadedFilesNumber} ${t('textThirteen')} ${allUploadableFilesNumber}`
            }
            onClose={() => {
                onCloseSnackbar();
                onClose && onClose();
            }}
            isCloseButtonDisabled={isSomeFilesBeingUploaded}
            startAdornment={
                isAllFilesNotUploaded ? (
                    <UploadErrorIcon
                        sx={{
                            width: '32px',
                            height: '32px',
                            marginLeft: '6px',
                            marginRight: '2px',
                        }}
                    />
                ) : isSomeFilesSuccessfullyUploaded ? (
                    <UploadWarningIcon
                        sx={{
                            width: '32px',
                            height: '32px',
                            marginLeft: '6px',
                            marginRight: '2px',
                        }}
                    />
                ) : isAllFilesSuccessfullyUploaded ? (
                    <UploadSuccessIcon
                        sx={{
                            width: '32px',
                            height: '32px',
                            marginLeft: '6px',
                            marginRight: '2px',
                        }}
                    />
                ) : (
                    <SnackbarProgress
                        sx={{
                            width: '32px',
                            height: '32px',
                            marginLeft: '6px',
                            marginRight: '2px',
                        }}
                        variant="determinate"
                        value={overallProgress}
                    />
                )
            }
            endAdornment={
                <SelectIcon
                    sx={{
                        '&.MuiSvgIcon-root': {
                            width: '32px',
                            height: '32px',
                            marginRight: '11px',
                            cursor: 'pointer',
                        },
                    }}
                    transform={`rotate(${isExpanded ? 180 : 0})`}
                    stroke="#C2C4D4"
                    onClick={setExpanded}
                />
            }
            content={
                isExpanded && (
                    <Extra>
                        <div className={css.wrapperUploadContentSnackbar}>
                            {uploadableFilesWithoutWarnings.map(({ id, name, size, duration, src, type, progress }) => (
                                <div
                                    key={id}
                                    className={css.wrapperUploadContent}
                                    onMouseOver={(event) => {
                                        event.stopPropagation();
                                        setOvered(id);
                                    }}
                                >
                                    <VideoImagePicture type={type} src={src ?? ''} />
                                    <div className={css.fieldUploadContentInfoAll}>
                                        <div
                                            className={clsx(
                                                css.fieldUploadContentInfoText,
                                                currentPage === 'uploaded_videos' && css.withoutPicture,
                                            )}
                                        >
                                            <p className={css.uploadContentInfoText}>{name}</p>
                                            {(isFileWithError(id) || isFileOveredWithError(id)) && (
                                                <p className={css.uploadContentInfoTextError}>
                                                    {t('textFourteen')}
                                                    {/* Ошибка загрузки */}
                                                </p>
                                            )}
                                        </div>
                                        <div className={css.fieldUploadContentInfoSize}>
                                            <p className={css.uploadContentInfoText}>
                                                <span>{currentPage === 'uploaded_videos' ? duration : size}</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div className={css.fieldUploadContentBtn}>
                                        {isFileInQueue(id) && (
                                            <Tooltip
                                                title={t('tooltipCancelUpload')}
                                                // "Отменить загрузку"
                                                placement="top"
                                            >
                                                <SnackbarProgress variant="determinate" value={0} />
                                            </Tooltip>
                                        )}
                                        {isFileBeingUploaded(id) && (
                                            <SnackbarProgress variant="determinate" value={progress} />
                                        )}
                                        {isOveredFileInQueueOrBeingUploaded(id) && (
                                            <UploadCancelIcon
                                                className={css.uploadIconHover}
                                                onClick={deleteFile(id)}
                                            />
                                        )}

                                        {isFileSuccessfullyUploaded(id) && <UploadSuccessIcon />}
                                        {isFileWithError(id) && <UploadErrorIcon />}
                                        {isFileOveredWithError(id) && (
                                            <Tooltip
                                                title={t('tooltipUploadAgain')}
                                                // "Повторить загрузку"
                                                placement="top"
                                            >
                                                <UploadReloadIcon
                                                    className={css.uploadIconHover}
                                                    onClick={restartUploading(id)}
                                                />
                                            </Tooltip>
                                        )}
                                    </div>
                                </div>
                            ))}
                        </div>
                    </Extra>
                )
            }
        />
    );
};
