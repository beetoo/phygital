import React, { ReactNode } from 'react';
import { CircularProgress } from '@mui/material';

import css from './style.m.css';

type Props = {
    children: ReactNode;
    loading: boolean;
    delay?: number;
    hide?: boolean;
};

const Pendable: React.FC<Props> = ({ loading, children, hide = false }: Props) =>
    hide ? (
        <>
            {loading ? (
                <div className={css.spinner}>
                    <CircularProgress />
                </div>
            ) : (
                children
            )}
        </>
    ) : (
        <>
            <CircularProgress />
            {children}
        </>
    );

export default Pendable;
