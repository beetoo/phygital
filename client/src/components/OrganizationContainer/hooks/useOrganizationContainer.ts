import { useCallback, useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { selectAuthOrganizationId, selectAuthStatus, selectAuthUserRole } from '../../../selectors';
import { USER_ROLE } from '../../../constants';
import { setGetOrganization } from '../../../@pages/ProfilePage/actions';
import { ActivePage } from '../types';
import { useCustomRouter } from '../../../hooks/useCustomRouter';
import { useMobileDetection } from '../../../hooks/useMobileDetection';
import { isDevProductionClient } from '../../../constants/environments';
import { usePlaylistStep } from '../../../@pages/PlaylistsPage/hooks/usePlaylistStep';

type ReturnType = {
    isAuth: boolean;
    isWelcomeMobilePageOpen: boolean;
    closeWelcomeMobilePage: () => void;
    isMobileVersionRequired: boolean;
    currentPage: ActivePage;
};

export const useOrganizationContainer = (): ReturnType => {
    const dispatch = useDispatch();

    const { pushToPage, currentPage } = useCustomRouter();

    const { organizationId: orgId } = useParams() as { organizationId: string };

    const { playlistStep } = usePlaylistStep();

    const isAuth = useSelector(selectAuthStatus);
    const userRole = useSelector(selectAuthUserRole);
    const authId = useSelector(selectAuthOrganizationId);

    const isAdmin = userRole === USER_ROLE.ADMIN;

    const organizationId = isAdmin || userRole === USER_ROLE.MANAGER ? orgId : (authId ?? '');

    const fromPageRef = useRef<ActivePage>('');

    const fromPage = fromPageRef.current;

    const [isWelcomeMobilePageOpen, setIsWelcomeMobilePageOpen] = useState(true);

    const closeWelcomeMobilePage = useCallback(() => {
        setIsWelcomeMobilePageOpen(false);
    }, []);

    const { isMobileVersion } = useMobileDetection();

    const isMobileVersionRequired = isDevProductionClient ? false : isMobileVersion && userRole === USER_ROLE.USER;

    useEffect(() => {
        if (currentPage) {
            fromPageRef.current = currentPage;
        }
    }, [currentPage]);

    // при загрузке главной страницы запрашиваем информацию по организации
    useEffect(() => {
        if (organizationId) {
            dispatch(setGetOrganization(organizationId));
        }
    }, [dispatch, organizationId]);

    // если мы начинаем создавать плейлист, то сразу возвращаемся на страницу плейлистов
    useEffect(() => {
        if (['profile', 'screens'].includes(fromPage) && playlistStep === 'creating_one') {
            pushToPage('playlists');
        }
    }, [fromPage, playlistStep, pushToPage]);

    // при любом изменении страницы мы обнуляем создание плейлиста
    useEffect(() => {
        if (fromPage !== currentPage && ['main', 'profile', 'screens'].includes(currentPage)) {
            if (playlistStep) {
                pushToPage('playlists');
            }
        }
    }, [currentPage, fromPage, playlistStep, pushToPage]);

    return {
        currentPage,
        isAuth,
        isMobileVersionRequired,
        isWelcomeMobilePageOpen,
        closeWelcomeMobilePage,
    };
};
