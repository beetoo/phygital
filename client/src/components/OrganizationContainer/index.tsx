import React from 'react';
import { Outlet } from 'react-router-dom';

import MainPageNew from '../../@pages/MainPageNew';
import Menu from '../Menu';
import MainHeader from '../Header';
import MobileWelcomePage from '../../@pages/MainPage/components/MobileWelcomePage';
import MobileDashboardPage from '../../@pages/MainPage/components/MobileDashboardPage';
import { useOrganizationContainer } from './hooks/useOrganizationContainer';

import css from './style.m.css';

const OrganizationContainer: React.FC = () => {
    const { currentPage, isMobileVersionRequired, isWelcomeMobilePageOpen, closeWelcomeMobilePage } =
        useOrganizationContainer();

    return isMobileVersionRequired ? (
        isWelcomeMobilePageOpen ? (
            <MobileWelcomePage {...{ closeWelcomeMobilePage }} />
        ) : (
            <MobileDashboardPage />
        )
    ) : (
        <>
            {currentPage === 'main-new' ? (
                <main className={css.main}>
                    <MainPageNew />
                </main>
            ) : (
                <>
                    <MainHeader />
                    <main className={css.mainOld}>
                        <Menu />
                        <Outlet />
                    </main>
                </>
            )}
        </>
    );
};

export default OrganizationContainer;
