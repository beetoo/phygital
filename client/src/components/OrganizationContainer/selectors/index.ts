import { StoreType } from '../../../types';

// получение списка организаций
export const selectOrganizationsNameList = ({
    pages: {
        admin: { organizations },
    },
}: StoreType): string[] => organizations.map(({ title }) => title);
