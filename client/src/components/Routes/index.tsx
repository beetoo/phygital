import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';

import SignUpPage from '../../@pages/SignUp';
import SignInPage from '../../@pages/SignIn';
import OrganizationContainer from '../OrganizationContainer';
import ConfirmRegistration from '../../@pages/SignUp/components/ConfirmRegistration';
import MainPage from '../../@pages/MainPage';
import MainPageNew from '../../@pages/MainPageNew';
import ProfilePage from '../../@pages/ProfilePage';
import ScreensPage from '../../@pages/ScreensPage';
import PlaylistsPage from '../../@pages/PlaylistsPage';
import PlaylistsPriorityPage from '../../@pages/PlaylistsPriorityPage';
import ContentPage from '../../@pages/ContentPage';
import AdministrationPage from '../../@pages/AdminPage';
import DxSchedulePage from '../../@pages/DxSchedulePage';
import ResetPasswordPage from '../../@pages/RecoveryPassword';
import ChangePasswordForm from '../../@pages/RecoveryPassword/components/ChangePasswordForm';
import { useSocketConnection } from '../../hooks/useSocketConnection';
import { useRootRoutes } from '../../hooks/useRootRoutes';
import { isDevelopmentMode } from '../../constants/environments';
import { useOrganizationTimeUsage } from '../../hooks/useOrganizationTimeUsage';
import ConfirmRegistrationAuthorizationModal from '../ModalsContainer/components/ConfirmRegistrationAuthorizationModal';
import PlaylistStepsContainer from '../../@pages/PlaylistsPage/components/PlaylistStepsContainer';
import { playlistSteps } from '../../@pages/PlaylistsPage/hooks/usePlaylistStep';

const RootRoutes: React.FC = () => {
    const { isAuth, isAdminOrManager, isNotConfirmedEmail, initialPath, defaultPage } = useRootRoutes();

    useSocketConnection();

    useOrganizationTimeUsage();

    return (
        <Routes>
            <Route path="/">
                <Route index element={<Navigate to={initialPath} />} />
                <Route path="confirm-registration/:confirmationCode" element={<ConfirmRegistration />} />

                {isAuth ? (
                    isNotConfirmedEmail ? (
                        <Route path="/">
                            <Route path="main" element={<ConfirmRegistrationAuthorizationModal />} />
                            <Route path="*" element={<Navigate to="/main" />} />
                        </Route>
                    ) : (
                        <Route path={isAdminOrManager ? '/organizations' : '/'} element={<OrganizationContainer />}>
                            <Route index element={<MainPage />} />
                            <Route path={isAdminOrManager ? ':organizationId/*' : '*'}>
                                <Route path="main" element={<MainPage />} />
                                {isDevelopmentMode && <Route path="main-new" element={<MainPageNew />} />}
                                <Route path="profile" element={<ProfilePage />} />
                                <Route path="screens" element={<ScreensPage />} />
                                <Route path="playlists" element={<PlaylistsPage />} />
                                {playlistSteps.map((step) => (
                                    <Route key={step} path={`playlists/${step}`} element={<PlaylistStepsContainer />} />
                                ))}

                                <Route path="playlists/priority" element={<PlaylistsPriorityPage />} />
                                <Route path="content" element={<ContentPage />} />
                                <Route path="web-content" element={<ContentPage />} />
                                <Route path="dx_schedule" element={<DxSchedulePage />} />
                                <Route path="administration" element={<AdministrationPage />} />

                                <Route path="*" element={<Navigate to={defaultPage} />} />
                            </Route>
                        </Route>
                    )
                ) : (
                    <Route path="/">
                        <Route path="signin" element={<SignInPage />} />
                        <Route path="signup" element={<SignUpPage />} />
                        <Route path="recovery-password" element={<ResetPasswordPage />} />
                        <Route path="recovery-password/:token" element={<ChangePasswordForm />} />
                        <Route path="*" element={<Navigate to="signin" />} />
                    </Route>
                )}

                <Route path="*" element={<Navigate to={initialPath} />} />
            </Route>
        </Routes>
    );
};

export default RootRoutes;
