import React from 'react';

import { HorizontalDesktopIcon, VerticalDesktopIcon } from '../../ui-kit';
import { OrientationTypeEng, OrientationRusType } from '../../@pages/ScreensPage/types';
import { TriggerOrientationSelectType } from '../../@pages/PlaylistsPage/types';

type Props = {
    orientation: OrientationRusType | OrientationTypeEng | TriggerOrientationSelectType | '';
};

type Orientations = Props['orientation'][];

const OrientationIcon: React.FC<Props> = ({ orientation }) => {
    if ((['Не поворачивать', "Don't turn"] as Orientations).includes(orientation)) {
        return <HorizontalDesktopIcon />;
    } else if ((['180°'] as Orientations).includes(orientation)) {
        return <HorizontalDesktopIcon transform="rotate(180)" />;
    } else if ((['90°'] as Orientations).includes(orientation)) {
        return <VerticalDesktopIcon />;
    } else if ((['270°'] as Orientations).includes(orientation)) {
        return <VerticalDesktopIcon transform="rotate(180)" />;
    }

    return null;
};

export default OrientationIcon;
