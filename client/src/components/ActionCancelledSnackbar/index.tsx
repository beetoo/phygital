import React from 'react';
import InfoRoundedIcon from '@mui/icons-material/InfoRounded';
import { useTranslation } from 'react-i18next';

import { Snackbar } from '../../ui-kit';

export const ActionCancelledSnackbar: React.FC<{ onClose: () => void }> = ({ onClose }) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    return (
        <Snackbar
            startAdornment={<InfoRoundedIcon htmlColor="white" />}
            title={t('actionCanceled') + '.'}
            onClose={onClose}
        />
    );
};
