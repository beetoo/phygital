import React from 'react';

import RootRoutes from '../Routes';

import { useAuthCheckRequest } from '../../hooks/useAuthCheckRequest';
import { ModalsWithAuthContainer } from '../ModalsContainer';

const MainLayout: React.FC = () => {
    const { isAuth, authChecking } = useAuthCheckRequest();

    return authChecking ? (
        <div style={{ height: '100%', backgroundColor: '#f7f7f9' }} />
    ) : (
        <>
            <RootRoutes />
            {isAuth ? <ModalsWithAuthContainer /> : null}
        </>
    );
};

export default MainLayout;
