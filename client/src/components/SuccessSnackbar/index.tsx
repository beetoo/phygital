import React from 'react';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

import { Snackbar } from '../../ui-kit';

export const SuccessSnackbar: React.FC<{ title: string; onClose: () => void }> = ({ title, onClose }) => (
    <Snackbar startAdornment={<CheckCircleIcon color="success" />} title={title} onClose={onClose} />
);
