import { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setSkipOnBoarding } from '../../../actions';
import { selectOnBoardingStatus } from '../../../selectors';

import { Step } from '../index';
import { getStepStoreName } from '../utils/getStepStoreName';

export enum StepStatus {
    INITIAL = 'INITIAL',
    SHOWN = 'SHOWN',
    FOLDED = 'FOLDED', //folded to snackbar, not yet used
}

const getTourStepStatus = (tourName: string, stepName: string): StepStatus => {
    switch (localStorage.getItem(getStepStoreName(tourName, stepName))) {
        case 'SHOWN':
            return StepStatus.SHOWN;
        case 'FOLDED':
            return StepStatus.FOLDED;
        default:
            return StepStatus.INITIAL;
    }
};

const readStepsStatuses = (steps: Array<Step>, tourName: string) =>
    steps.reduce((result, { name: stepName }) => {
        result.set(stepName, getTourStepStatus(tourName, stepName));
        return result;
    }, new Map<string, StepStatus>());

type ReturnValue = {
    tourStepsStatuses: Map<string, StepStatus>;
    updateTourStepsStatuses: () => void;
    markStepAsShown: (stepName: string) => void;
};

export const useTourStepsStatuses = (tourName: string, stepsRaw: Array<Step | null>): ReturnValue => {
    const dispatch = useDispatch();

    const steps = useMemo(() => stepsRaw.filter((step): step is Step => step != null), [stepsRaw]);

    const [tourStepsStatuses, setTourStepsStatuses] = useState(() => readStepsStatuses(steps, tourName));

    // обнуляем skipOnBoardingStatus
    useEffect(() => {
        if (tourStepsStatuses.get('onBoardingSkip') === StepStatus.SHOWN) {
            dispatch(setSkipOnBoarding(false));
        }
    }, [dispatch, tourStepsStatuses]);

    const updateTourStepsStatuses = useCallback(() => {
        setTourStepsStatuses(readStepsStatuses(steps, tourName));
    }, [steps, tourName]);

    //it will force to readStepsStatuses from localStorage, cause onboarding status change clears steps localStorage values
    const onboardingStatus = useSelector(selectOnBoardingStatus);

    useEffect(() => {
        updateTourStepsStatuses();
    }, [steps, tourName, onboardingStatus, updateTourStepsStatuses]);

    const markStepAsShown = useCallback(
        (stepName: string) => localStorage.setItem(getStepStoreName(tourName, stepName), StepStatus.SHOWN),
        [tourName],
    );

    return {
        tourStepsStatuses,
        updateTourStepsStatuses,
        markStepAsShown,
    };
};
