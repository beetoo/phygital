import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Step } from '@aborovsky/react-joyride';

import { selectOnBoardingStatus } from '../../../selectors';
import { selectCurrentModal } from '../../ModalsContainer/selectors';

export const useRun = (steps: Array<Step>): boolean => {
    const isOnboardingEnabled = useSelector(selectOnBoardingStatus);
    const modalStatus = useSelector(selectCurrentModal) !== '';

    const [run, setRun] = useState(false);

    useEffect(() => {
        if (modalStatus) {
            setRun(false);
        } else if (!steps.length) {
            setRun(false);
        } else {
            setRun(isOnboardingEnabled);
        }
    }, [isOnboardingEnabled, modalStatus, steps.length]);

    return run;
};
