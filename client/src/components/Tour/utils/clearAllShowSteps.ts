export const TOUR_KEY_PREFIX = 'phygitaltour:';

export const clearAllShowSteps = (): void =>
    Object.keys(localStorage)
        .filter((key) => key.startsWith(TOUR_KEY_PREFIX))
        .forEach((key) => localStorage.removeItem(key));
