import { TOUR_KEY_PREFIX } from './clearAllShowSteps';

export const getStepStoreName = (tourName: string, stepName: string): string =>
    `${TOUR_KEY_PREFIX}${tourName}:${stepName}`;
