import React, { PropsWithChildren } from 'react';
import css from './tourStep.m.css';

export const TourStep: React.FC<PropsWithChildren> = ({ children }) => {
    return <div className={css.tourStep}>{children}</div>;
};
