import React, { CSSProperties, PropsWithChildren } from 'react';
import css from './tourHeader.m.css';

type Props = PropsWithChildren<{ style?: CSSProperties }>;

export const TourHeader: React.FC<Props> = ({ children, style }) => {
    return (
        <div style={{ ...style }} className={css.tourHeader}>
            {children}
        </div>
    );
};
