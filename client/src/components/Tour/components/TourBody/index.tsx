import React, { PropsWithChildren } from 'react';
import css from './tourBody.m.css';

export const TourBody: React.FC<PropsWithChildren> = ({ children }) => {
    return <div className={css.tourBody}>{children}</div>;
};
