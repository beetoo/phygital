import React, { ComponentProps } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import Joyride, { Locale, Props, Step as StepBase, Styles } from '@aborovsky/react-joyride';

import { selectOnBoardingStatus, selectSkipOnBoardingStatus } from '../../selectors';
import { setOnboardingDisabled } from '../../actions';
import { Beacon } from './Beacon';
import { useRun } from './hooks/useRun';
import { StepStatus, useTourStepsStatuses } from './hooks/useTourStepsStatuses';

import './joyride-override.css';

export interface Step<T extends HTMLElement = HTMLElement> extends Omit<StepBase, 'target'> {
    target: string | T | undefined | null | SVGSVGElement;
    name: string;
}

type StepNotNullTarget<T extends HTMLElement = HTMLElement> = Omit<Step<T>, 'target'> & {
    target: NonNullable<Step<T>['target']>;
};

const fixCSSClassName = (className: string) => className.replace(/@/g, '\\@');

const locale = (t: TFunction): Locale => ({
    back: 'Назад',
    close: t('gotIt'),
    last: 'В конец',
    next: 'Далее',
    open: 'Открыть',
    skip: t('turnOffTutorial'),
});

const styles: Styles = {
    options: {
        backgroundColor: '#1F36E5',
        arrowColor: '#3356F9',
        overlayColor: '#00023e26',
        primaryColor: '#1F36E5',
        textColor: '#fff',
        width: 352,
        zIndex: 1000,
    },
};

type TourProps = Omit<Props, 'steps'> & { name: string; steps: Array<Step | null> };

type StepBaseWithName = StepBase & { name: string };

export const Tour: React.FC<TourProps> = ({
    steps: stepsRaw,
    callback: _callback,
    run: externalRun,
    name: tourName,
    ...props
}) => {
    const isOnBoardingEnabled = useSelector(selectOnBoardingStatus);
    const isOnBoardingSkipped = useSelector(selectSkipOnBoardingStatus);
    const { tourStepsStatuses, markStepAsShown, updateTourStepsStatuses } = useTourStepsStatuses(tourName, stepsRaw);

    const { t } = useTranslation('translation', { keyPrefix: 'learningHintsModal' });

    const steps = React.useMemo<Array<StepBaseWithName>>(
        () =>
            stepsRaw
                .filter((step): step is StepNotNullTarget => step != null && step.target != null)
                .filter(({ name: stepName }) => tourStepsStatuses.get(stepName) != StepStatus.SHOWN)
                .map(
                    ({ target, ...step }) =>
                        ({
                            placement: 'right-start',
                            ...step,
                            hideCloseButton: true,
                            target: typeof target === 'string' ? fixCSSClassName(target) : target,
                            disableBeacon: true,
                            showSkipButton: true,
                            showSkipButtonForLastStep: true,
                            hideBackButton: true,
                            scrollToStep: false,
                            floaterProps: {
                                disableAnimation: true,
                            },
                        }) as StepBaseWithName,
                ),
        [stepsRaw, tourStepsStatuses],
    );

    const run = useRun(steps);

    const dispatch = useDispatch();

    const callback = React.useCallback<NonNullable<ComponentProps<typeof Joyride>['callback']>>(
        (data) => {
            if (data.action === 'skip' && data.lifecycle === 'complete') {
                // Tour skipped globally, нажали 'Закрыть обучение'
                return dispatch(setOnboardingDisabled());
            } else if (data.action === 'close' && data.type === 'step:after') {
                markStepAsShown(steps[data.index].name);
            } else if (data.action === 'close' && data.type === 'tour:end') {
                updateTourStepsStatuses();
            }
            _callback?.(data);
        },
        [_callback, dispatch, markStepAsShown, steps, updateTourStepsStatuses],
    );

    const isTourEnabled = ((isOnBoardingSkipped && tourName === 'mainPage') || isOnBoardingEnabled) && steps.length > 0;

    return isTourEnabled ? (
        <Joyride
            beaconComponent={Beacon}
            styles={styles}
            steps={steps}
            run={externalRun !== undefined ? externalRun : run && externalRun}
            debug={process.env.NODE_ENV !== 'production'}
            locale={locale(t)}
            showSkipButton={true}
            showSkipButtonForLastStep={true}
            hideBackButton={true}
            disableOverlay={true}
            {...props}
            callback={callback}
        />
    ) : null;
};
