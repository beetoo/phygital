import { BeaconRenderProps } from '@aborovsky/react-joyride';
import React, { ElementType, forwardRef } from 'react';

import css from './beacon.m.css';

export const Beacon: ElementType<BeaconRenderProps> = forwardRef(() => <span className={css.beacon} />);

Beacon.displayName = 'Beacon';
