import React, { PropsWithChildren } from 'react';
import { useSelector } from 'react-redux';

import StorageIsFullWarningModal from '../StorageIsFullWarningModal';
import NoActiveStorageSubscriptionsWarningModal from '../NoActiveStorageSubscriptionsWarningModal';
import { selectCurrentOrganization } from '../../../../@pages/ProfilePage/selectors';
import { isSLClientMode } from '../../../../constants/environments';

type Props = {
    alreadyUploadedFilesSize: number;
};

// @ts-ignore
const UploadModalsSizeWarningWrapper: React.FC<PropsWithChildren<Props>> = ({ alreadyUploadedFilesSize, children }) => {
    const { limitStorageSize, hasActiveSubscriptions } = useSelector(selectCurrentOrganization);

    const isNotActiveStorageSubscriptions = !isSLClientMode && !hasActiveSubscriptions;

    const isLimitReached = alreadyUploadedFilesSize >= limitStorageSize;

    return isNotActiveStorageSubscriptions ? (
        <NoActiveStorageSubscriptionsWarningModal />
    ) : isLimitReached ? (
        <StorageIsFullWarningModal />
    ) : (
        children
    );
};

export default UploadModalsSizeWarningWrapper;
