import React from 'react';
import { X } from '@phosphor-icons/react';

import { useNoActiveStorageSubscriptionsModal } from '../../hooks/useNoActiveSubscriptionWarningModal';

import css from './style.m.css';

const NoActiveStorageSubscriptionsWarningModal: React.FC = () => {
    const { goToContentPage, openSupportModal, closeModal, t } = useNoActiveStorageSubscriptionsModal();

    return (
        <div className={css.limitReachedModal}>
            <div className={css.limitReached}>
                <h2>{t('noActiveSubscription')}</h2>
                <div className={css.containerInfoText}>
                    <p>{t('youCantDownload')}</p>

                    <div onClick={closeModal}>
                        <X className={css.closeButton} size={16} weight="bold" />
                    </div>
                </div>
                <div className={css.containerInfoBtn}>
                    <button onClick={goToContentPage} className={css.btnLimit}>
                        {t('cancel')}
                    </button>
                    <button onClick={openSupportModal} className={css.btnLimit}>
                        {t('subscribe')}
                    </button>
                </div>
            </div>
        </div>
    );
};

export default NoActiveStorageSubscriptionsWarningModal;
