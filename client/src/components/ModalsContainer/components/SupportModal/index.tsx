import React from 'react';
import { clsx } from 'clsx';

import { useSupportModal } from '../../../Menu/hooks/useSupportModal';

import css from './style.m.css';

const SupportModal: React.FC = () => {
    const { closeSupportModal, t } = useSupportModal();

    return (
        <div className={css.outerContainer}>
            <div className={css.container}>
                <h1 className={css.title}>{t('support')}</h1>
                <p className={css.titleItem}>{t('technicalSupport')}</p>
                <p className={css.titleItem1}>
                    <a className={css.menuNavigationFeedBackLink} href="mailto:support@phygitalsignage.io">
                        support@phygitalsignage.io
                    </a>
                </p>
                <p className={css.titleItem1}>+7 499 705-04-52</p>
                <p className={css.titleItem1}>
                    {t('telegramSupportBot')}:{' '}
                    <a
                        className={css.telegramBotLink}
                        target="_blank"
                        href="https://t.me/PhygitalSignage_support_bot"
                        rel="noreferrer"
                    >
                        @PhygitalSignage_support_bot
                    </a>
                </p>
                <p className={clsx(css.titleItem1, css.titleItem2)}>
                    {t('openingHours')}: {t('monFri')} 10:00 - 18:00 {t('moscowTime')}
                </p>

                <p className={css.titleItem}>{t('paymentAndSubscriptionChanges')}</p>
                <p className={css.titleItem1}>
                    <a className={css.menuNavigationFeedBackLink} href="mailto:hello@phygitalsignage.io">
                        hello@phygitalsignage.io
                    </a>
                </p>
                <p className={css.titleItem1}>+7 499 705-04-52</p>
                <p className={clsx(css.titleItem1, css.titleItem2)}>
                    {t('openingHours')}: {t('monFri')} 10:00 - 18:00 {t('moscowTime')}
                </p>
                <div className={css.actionContainer}>
                    <button className={css.btnClose} onClick={closeSupportModal}>
                        {t('close')}
                    </button>
                </div>
            </div>
        </div>
    );
};

export default SupportModal;
