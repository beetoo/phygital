import React from 'react';
import { useTranslation } from 'react-i18next';
import { clsx } from 'clsx';

import { useTvBoxSecondInfoModal } from '../../hooks/useTvBoxSecondInfoModal';

import css from './style.m.css';

const TvBoxSecondInfoModal: React.FC = () => {
    const { openTvBoxRequestModal, closeModal } = useTvBoxSecondInfoModal();

    const { t } = useTranslation('translation', { keyPrefix: 'onboarding' });

    return (
        <div className={css.outerContainer}>
            <div className={css.container}>
                <div className={css.imgContainer} />
                <h1 className={css.title}>
                    {t('textFive')}
                    <br />
                    {t('textSix')}
                    {/* Еще нет экранов?
                    <br />
                    Закажите экраны «Phygital POS» */}
                </h1>
                <p className={clsx(css.titleItem, css.titleItem1)}>
                    {t('textSeven')}
                    {/* Экраны «Phygital POS» — профессиональная панель по цене бытового ТВ с предустановленным Phygital
                    Signage. Позволяет использовать все функции Phygital Signage, а также дистанционное включение и
                    выключение экрана по расписанию. */}
                </p>
                <div className={css.actionContainer}>
                    <button className={css.btnLearnDetails} onClick={openTvBoxRequestModal}>
                        {t('btnLearnMore')}
                        {/* Узнать детали */}
                    </button>
                    <button className={css.btnClose} onClick={closeModal}>
                        {t('btnLater')}
                        {/* Закрыть */}
                    </button>
                </div>
            </div>
        </div>
    );
};

export default TvBoxSecondInfoModal;
