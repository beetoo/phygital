import React from 'react';
import { clsx } from 'clsx';

import PhoneInputCustom from '../../../../ui-kit/PhoneInputCustom';
import { CancelButton } from '../../../../ui-kit';
import { useBalanceRequestFormModal } from '../../hooks/useBalanceRequestFormModal';

import css from './style.m.css';

const BalanceRequestFormModal: React.FC = () => {
    const {
        title,
        email,
        phone,
        messageText,
        onChangeTitle,
        onChangeEmail,
        onChangePhone,
        onChangeMessageText,
        sendEmailMessage,
        isSubmitSendEmailButtonDisabled,
        emailInputError,
        phoneInputError,
        closeModal,
        t,
    } = useBalanceRequestFormModal();

    return (
        <form className={css.popupSendLetter} onSubmit={(e) => e.preventDefault()}>
            <h2>{t('infoForCommunication')}</h2>

            <div className={css.wrapperSendLetter}>
                <div className={css.labelSendLetter}>
                    <label>{t('name')} *</label>
                </div>
                <div>
                    <input
                        className={css.popupSendLetterInput}
                        placeholder=""
                        value={title}
                        onChange={onChangeTitle}
                        maxLength={125}
                    />
                </div>
            </div>
            <div className={css.wrapperSendLetter}>
                <div className={css.labelSendLetter}>
                    <label>{t('email')} *</label>
                </div>
                <div>
                    <input
                        className={clsx(
                            css.popupSendLetterInput,
                            emailInputError && clsx(css.popupSendLetterInputError, css.iconWarning),
                        )}
                        placeholder="myemail@address.ru"
                        value={email}
                        onChange={onChangeEmail}
                        maxLength={125}
                    />
                    {emailInputError && <div className={css.errorSendLetter}>{emailInputError}</div>}
                </div>
            </div>
            <div className={css.wrapperSendLetter}>
                <div className={css.labelSendLetter}>
                    <label>
                        {t('phoneNumber')} *{/* Номер телефона * */}
                    </label>
                </div>
                <div>
                    <PhoneInputCustom
                        style={{ width: '360px' }}
                        {...{ phone, onPhoneInputChange: onChangePhone, phoneInputError }}
                    />
                    {phoneInputError && <div className={css.errorSendLetter}>{phoneInputError}</div>}
                </div>
            </div>
            <div className={css.labelSendLetter}>
                <label>{t('message')}</label>
            </div>
            <div className={css.wrapperLetter}>
                <textarea
                    className={css.letter}
                    name="comment"
                    placeholder={t('enterYourText') as string}
                    value={messageText}
                    onChange={onChangeMessageText}
                />
            </div>

            <div className={css.fieldBtnLetter}>
                <CancelButton text={t('btnCancel')} onClick={closeModal} />

                <button
                    type="submit"
                    className={css.buttonSend}
                    onClick={sendEmailMessage}
                    disabled={isSubmitSendEmailButtonDisabled}
                >
                    <img alt="" />
                    {t('btnSend')}
                </button>
            </div>
        </form>
    );
};

export default BalanceRequestFormModal;
