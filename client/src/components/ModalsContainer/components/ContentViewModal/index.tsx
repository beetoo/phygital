import React from 'react';
import ReactPlayer from 'react-player';

import ModalTemplate from '../../../ModalTemplate';
import { Tooltip } from '../../../../ui-kit';
import { useContentViewModal } from '../../../../hooks/useContentViewModal';
import { CONTENT_TYPE } from '../../../../../../common/types';

import css from './style.m.css';

const ContentViewModal: React.FC = () => {
    const {
        isContentViewModalOpen,
        viewItem,
        moveToLeft,
        moveToRight,
        closeImageViewModal,
        isHorizontalView,
        isBackForwardButtonsShown,
        windowHeight,
    } = useContentViewModal();

    const size514 = Math.round(windowHeight * 0.542) + 'px';
    const size453 = Math.round(windowHeight * (0.463 + (windowHeight - 768) * 0.000082)) + 'px';
    const size767 = Math.round(windowHeight * 0.79) + 'px';

    return (
        <ModalTemplate isOpen={isContentViewModalOpen}>
            <div
                className={css.slider}
                style={{ width: isHorizontalView ? '1045px' : size514 }}
                onClick={(e) => e.stopPropagation()}
            >
                <div className={css.sliderList} style={{ width: isHorizontalView ? '913px' : size514 }}>
                    <div className={css.textSlider} style={{ width: isHorizontalView ? '853px' : size453 }}>
                        {viewItem?.name}
                    </div>
                    {viewItem?.type === CONTENT_TYPE.IMAGE ? (
                        <img
                            className={css.contentImage}
                            src={viewItem?.src}
                            width={isHorizontalView ? '913px' : size514}
                            height={isHorizontalView ? '514px' : size767}
                            alt=""
                        />
                    ) : viewItem?.type === CONTENT_TYPE.VIDEO ? (
                        <ReactPlayer width="100%" height="100%" url={viewItem?.src} controls={true} playing={true} />
                    ) : viewItem?.type === CONTENT_TYPE.WEB ? (
                        <iframe
                            className={css.contentImage}
                            src={viewItem?.src}
                            width={isHorizontalView ? '913px' : size514}
                            height={isHorizontalView ? '514px' : size767}
                        />
                    ) : null}
                    <Tooltip title="Закрыть" placement="top">
                        <span
                            className={css.iconXwhite}
                            style={{ right: isHorizontalView ? '79px' : '12px' }}
                            onClick={closeImageViewModal}
                        />
                    </Tooltip>
                </div>
                {isBackForwardButtonsShown && (
                    <>
                        <Tooltip title="Предыдущий" placement="top-end">
                            <a
                                className={css.prev}
                                style={{ left: isHorizontalView ? '0' : '-66px' }}
                                onClick={moveToLeft}
                            />
                        </Tooltip>

                        <Tooltip title="Следующий" placement="top-start">
                            <a
                                className={css.next}
                                style={{ right: isHorizontalView ? '0' : '-66px' }}
                                onClick={moveToRight}
                            />
                        </Tooltip>
                    </>
                )}
            </div>
        </ModalTemplate>
    );
};

export default ContentViewModal;
