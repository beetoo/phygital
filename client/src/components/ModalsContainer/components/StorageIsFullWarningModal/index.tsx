import React from 'react';
import { X } from '@phosphor-icons/react';

import { useStorageIsFullWarningModal } from '../../hooks/useStorageIsFullWarningModal';

import css from './style.m.css';

const StorageIsFullWarningModal: React.FC = () => {
    const { goToContentPage, openSupportModal, closeModal, t } = useStorageIsFullWarningModal();

    return (
        <div className={css.limitReachedModal}>
            <div className={css.limitReached}>
                <h2>{t('storageFull')}</h2>
                <div className={css.containerInfoText}>
                    <p>{t('toUploadContent')}</p>

                    <div onClick={closeModal}>
                        <X className={css.closeButton} size={16} weight="bold" />
                    </div>
                </div>
                <div className={css.containerInfoBtn}>
                    <button onClick={goToContentPage} className={css.btnLimit}>
                        {t('freeUpStorageSpace')}
                    </button>
                    <button onClick={openSupportModal} className={css.btnLimit}>
                        {t('expandStorage')}
                    </button>
                </div>
            </div>
        </div>
    );
};

export default StorageIsFullWarningModal;
