import React from 'react';
import { clsx } from 'clsx';

import PhoneInputCustom from '../../../../ui-kit/PhoneInputCustom';
import { useUserRequestModal } from '../../hooks/useUserRequestModal';

import css from './style.m.css';

type Props = {
    title: string;
};

const UserRequestModal: React.FC<Props> = ({ title }) => {
    const {
        userName,
        phone,
        organizationTitle,
        email,

        onChangeName,
        onChangeTitle,
        onChangeEmail,
        onChangePhone,

        phoneInputError,
        emailInputError,

        sendRequest,
        isSubmitSendRequestButtonDisabled,

        closeModal,

        t,
    } = useUserRequestModal();

    return (
        <form className={css.popupSendLetter} onSubmit={(e) => e.preventDefault()}>
            <h2>{title}</h2>
            <h3>
                {t('textAdvice')}
                {/* Введите данные и мы свяжемся с вами */}
            </h3>

            <div className={css.wrapperSendLetter}>
                <div className={css.labelSendLetter}>
                    <label>{t('name')}</label>
                </div>
                <div>
                    <input
                        className={css.popupSendLetterInput}
                        placeholder={t('enterName') as string}
                        value={userName}
                        onChange={onChangeName}
                        maxLength={125}
                    />
                </div>
            </div>
            <div className={css.wrapperSendLetter}>
                <div className={css.labelSendLetter}>
                    <label>{t('phoneNumber')}</label>
                </div>
                <div>
                    <PhoneInputCustom
                        style={{ width: '360px' }}
                        {...{ phone, onPhoneInputChange: onChangePhone, phoneInputError }}
                    />
                    {phoneInputError && <div className={css.errorSendLetter}>{phoneInputError}</div>}
                </div>
            </div>
            <div className={css.wrapperSendLetter}>
                <div className={css.labelSendLetter}>
                    <label>{t('companyName')}</label>
                </div>
                <div>
                    <input
                        className={css.popupSendLetterInput}
                        placeholder={t('enterName') as string}
                        value={organizationTitle}
                        onChange={onChangeTitle}
                        maxLength={125}
                    />
                </div>
            </div>
            <div className={css.wrapperSendLetter}>
                <div className={css.labelSendLetter}>
                    <label>{t('email')}</label>
                </div>
                <div>
                    <input
                        className={clsx(
                            css.popupSendLetterInput,
                            emailInputError && clsx(css.popupSendLetterInputError, css.iconWarning),
                        )}
                        placeholder="myemail@address.ru"
                        value={email}
                        onChange={onChangeEmail}
                        maxLength={125}
                    />
                    {emailInputError && <div className={css.errorSendLetter}>{emailInputError}</div>}
                </div>
            </div>

            <div className={css.fieldBtnLetter}>
                <button
                    type="submit"
                    className={css.buttonSend}
                    onClick={sendRequest}
                    disabled={isSubmitSendRequestButtonDisabled}
                >
                    <img alt="" />
                    {t('btnSend')}
                </button>

                <button type="button" className={css.buttonCancel} onClick={closeModal}>
                    {t('btnCancel')}
                </button>
            </div>
        </form>
    );
};

export default UserRequestModal;
