import React from 'react';

import PhoneInputCustom from '../../../../ui-kit/PhoneInputCustom';
import { useRequestStorageAmountModal } from '../../hooks/useRequestStorageAmountModal';

import css from './style.m.css';

const RequestStorageAmountModal: React.FC = () => {
    const {
        phone,
        onChangePhone,
        phoneInputError,

        sendRequest,
        isSendRequestButtonDisabled,

        closeModal,
        t,
    } = useRequestStorageAmountModal();

    return (
        <form className={css.popupSendLetter} onSubmit={(e) => e.preventDefault()}>
            <h2>
                {t('textFive')}
                {/* Запросить увеличение объёма памяти */}
            </h2>
            <p>
                {t('textSix')}
                {/* Мы свяжемся с вами по телефону, чтобы уточнить детали */}
            </p>

            <div className={css.wrapperSendLetter}>
                <div className={css.labelSendLetter}>
                    <label>
                        {t('phoneNumber')}
                        {/* Номер телефона * */}
                    </label>
                </div>
                <div>
                    <PhoneInputCustom
                        style={{ width: '360px' }}
                        validCheck={true}
                        {...{ phone, onPhoneInputChange: onChangePhone, phoneInputError }}
                    />
                    {phoneInputError && <div className={css.errorSendLetter}>{phoneInputError}</div>}
                </div>
            </div>

            <div className={css.fieldBtnLetter}>
                <button type="button" className={css.buttonCancel} onClick={closeModal}>
                    {t('btnCancel')}
                    {/* Отменить */}
                </button>

                <button
                    type="submit"
                    className={css.buttonSend}
                    onClick={sendRequest}
                    disabled={isSendRequestButtonDisabled}
                >
                    <img alt="" />
                    {t('btnSend')}
                    {/* Отправить */}
                </button>
            </div>
        </form>
    );
};

export default RequestStorageAmountModal;
