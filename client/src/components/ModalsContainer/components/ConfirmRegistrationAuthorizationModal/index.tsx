import React from 'react';

import ModalTemplate from '../../../../components/ModalTemplate';
import { useConfirmRegistrationAuthorizationModal } from '../../hooks/useConfirmRegistrationAuthorizationModal';

import css from './style.m.css';

const ConfirmRegistrationAuthorizationModal: React.FC = () => {
    const {
        activeEmail,
        sendConfirmEmailButtonText,
        sendConfirmRegistrationEmail,
        isSendConfirmEmailButtonDisabled,
        handleLogout,
        t,
    } = useConfirmRegistrationAuthorizationModal();

    return (
        <ModalTemplate isOpen={true} overlayBackground="#f7f7f9">
            <div className={css.wrapConfirmEmail}>
                <div className={css.confirmEmail}>
                    <h2>
                        {t('confirmYourEmail')}
                        {/* Подтвердите вашу почту */}
                    </h2>
                    <p className={css.confirmEmailText}>
                        {/* На адрес */}
                        {t('textNine')} <span>{activeEmail}</span> {t('textTen')}
                        {/* мы отправили письмо с ссылкой. Пройдите&nbsp;по&nbsp;ссылке,&nbsp;чтобы
                        завершить регистрацию. */}
                    </p>
                    <div className={css.confirmEmailItem}>
                        <button
                            className={css.btnOk}
                            onClick={sendConfirmRegistrationEmail}
                            disabled={isSendConfirmEmailButtonDisabled}
                        >
                            {sendConfirmEmailButtonText}
                        </button>
                        <p className={css.confirmEmailQuestion} onClick={handleLogout}>
                            <a>{t('exit')}</a>
                        </p>
                    </div>
                </div>
            </div>
        </ModalTemplate>
    );
};

export default ConfirmRegistrationAuthorizationModal;
