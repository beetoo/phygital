import React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import { selectModalData } from '../../selectors';

import css from './style.m.css';

const DeleteGeneralModal: React.FC = () => {
    const { t } = useTranslation('translation', { keyPrefix: 'modals' });

    const { subject, onDelete, onClose } = useSelector(selectModalData) as {
        subject: string;
        onDelete: () => void;
        onClose: () => void;
    };

    return (
        <div className={css.popupDeleteScreen}>
            <h2>{t('deleteSubject', { subject: t(subject) })}</h2>
            <p>
                {t('thisActionCannotBeUndone')}
                <br />
                {t('areYouSureYouWantToDeleteThisSubject', { subject: t(subject) })}
            </p>
            <div className={css.containerBtn}>
                <a className={css.btnCancel}>
                    <button className={css.buttonCancel} onClick={onClose}>
                        {t('cancel')}
                    </button>
                </a>
                <a>
                    <button className={css.buttonDelete} onClick={onDelete}>
                        {t('delete')}
                    </button>
                </a>
            </div>
        </div>
    );
};

export default DeleteGeneralModal;
