import React from 'react';
import { clsx } from 'clsx';

import { useTvBoxInfoModal } from '../../hooks/useTvBoxInfoModal';

import css from './style.m.css';

const TvBoxInfoModal: React.FC = () => {
    const { openTvBoxRequestModal, openTvBoxSecondModal, t } = useTvBoxInfoModal();

    return (
        <div className={css.outerContainer}>
            <div className={css.container}>
                <div className={css.imgContainer} />
                <h1 className={css.title}>
                    {t('textOne')}
                    <br />
                    {t('textTwo')}
                    {/* Есть экраны?
                    <br />
                    Закажите плеер «Phygital Play» */}
                </h1>
                <p className={clsx(css.titleItem, css.titleItem1)}>
                    {t('textThree')}
                    {/* Плеер «Phygital Play» для plug-n-play подключения любого <br />
                    ТВ с HDMI к платформе Phygital Signage. */}
                </p>
                <p className={css.titleItem}>
                    {t('textFour')}
                    {/* Цена плеера — 5 500 ₽. Или оформите по подписке всего <br />
                    за 450 ₽ в месяц. */}
                </p>
                <div className={css.actionContainer}>
                    <button className={css.btnLearnDetails} onClick={openTvBoxRequestModal}>
                        {t('btnLearnMore')}
                        {/* Узнать детали */}
                    </button>
                    <button className={css.btnClose} onClick={openTvBoxSecondModal}>
                        {t('btnNoScreens')}
                        {/* Нет экранов */}
                    </button>
                </div>
            </div>
        </div>
    );
};

export default TvBoxInfoModal;
