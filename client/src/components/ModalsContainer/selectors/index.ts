import { StoreType } from '../../../types';
import { MODALS, UploadableFile, UploadProgress, UploadVideoTab, ViewItemType } from '../types';
import { ActivePage } from '../../OrganizationContainer/types';

export const selectCurrentModal = ({
    pages: {
        organization: {
            modals: { modal },
        },
    },
}: StoreType): MODALS | '' => modal;

export const selectPrevModal = ({
    pages: {
        organization: {
            modals: { prevModal },
        },
    },
}: StoreType): MODALS | '' => prevModal;

export const selectPageGoingTo = ({
    pages: {
        organization: {
            modals: { pageGoingTo },
        },
    },
}: StoreType): ActivePage => pageGoingTo;

export const selectModalData = ({
    pages: {
        organization: {
            modals: { modalData },
        },
    },
}: StoreType): unknown => modalData;

export const selectSavedWithModalId = ({
    pages: {
        organization: {
            modals: { savedId },
        },
    },
}: StoreType): string | string[] => savedId;

export const selectActiveUploadVideoTab = ({
    pages: {
        organization: {
            modals: { uploadVideoTab },
        },
    },
}: StoreType): UploadVideoTab => uploadVideoTab;

export const selectUploadGroupId = ({
    pages: {
        organization: {
            modals: { uploadGroupId },
        },
    },
}: StoreType): string => uploadGroupId;

export const selectUploadableFiles = ({
    pages: {
        organization: {
            modals: { uploadableFiles },
        },
    },
}: StoreType): UploadableFile[] => uploadableFiles;

export const selectUploadProgress = ({
    pages: {
        organization: {
            modals: { uploadProgress },
        },
    },
}: StoreType): UploadProgress => uploadProgress;

export const selectClosedUploadingPlaylistSnackbars = ({
    pages: {
        organization: {
            modals: { closedSnackbars },
        },
    },
}: StoreType): string[] => closedSnackbars;

export const selectContentViewModalStatus = ({
    pages: {
        organization: {
            modals: { isContentViewModalOpen },
        },
    },
}: StoreType): boolean => isContentViewModalOpen;

export const selectContentViewItems = ({
    pages: {
        organization: {
            modals: { viewItems },
        },
    },
}: StoreType): ViewItemType[] => viewItems;

export const selectContentViewIndex = ({
    pages: {
        organization: {
            modals: { viewIndex },
        },
    },
}: StoreType): number => viewIndex;
