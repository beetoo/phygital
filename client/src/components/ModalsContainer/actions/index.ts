import { MODALS, UploadableFile, UploadProgress, UploadVideoTab, ViewItemType } from '../types';
import { ActivePage } from '../../OrganizationContainer/types';

export const SHOW_MODAL = 'SHOW_MODAL';
export const SHOW_MODAL_NEW = 'SHOW_MODAL_NEW';
export const HIDE_MODAL = 'HIDE_MODAL';

export const OPEN_CONTENT_VIEW_MODAL = 'OPEN_CONTENT_VIEW_MODAL';
export const CLOSE_CONTENT_VIEW_MODAL = 'CLOSE_CONTENT_VIEW_MODAL';
export const SET_CONTENT_VIEW_INDEX = 'SET_CONTENT_VIEW_INDEX';

export const SET_UPLOAD_VIDEO_TAB = 'SET_UPLOAD_VIDEO_TAB';
export const SET_UPLOADABLE_FILES = 'SET_UPLOADABLE_FILES';
export const SET_UPLOAD_PROGRESS = 'SET_UPLOAD_PROGRESS';

export const SET_CLOSED_UPLOADING_PLAYLIST_SNACKBARS = 'SET_CLOSED_UPLOADING_PLAYLIST_SNACKBARS';

export const setShowModal = (
    modal: MODALS | '',
    prevModal: MODALS | '' = '',
    pageGoingTo: ActivePage = '',
    savedId: string | string[] = '',
) => ({ type: SHOW_MODAL, payload: { modal, prevModal, pageGoingTo, savedId } }) as const;

export const setShowModalNew = (modal: MODALS | '', modalData: unknown) =>
    ({ type: SHOW_MODAL_NEW, payload: { modal, modalData } }) as const;
export const setHideModal = () => ({ type: HIDE_MODAL }) as const;

export const setOpenContentViewModal = (viewItems: ViewItemType[], viewIndex: number) =>
    ({ type: OPEN_CONTENT_VIEW_MODAL, payload: { viewItems, viewIndex } }) as const;
export const setCloseContentViewModal = () => ({ type: CLOSE_CONTENT_VIEW_MODAL }) as const;

export const setContentViewIndex = (viewIndex: number) =>
    ({ type: SET_CONTENT_VIEW_INDEX, payload: { viewIndex } }) as const;

export const setActiveUploadVideoTab = (tab: UploadVideoTab, groupId = '') =>
    ({ type: SET_UPLOAD_VIDEO_TAB, payload: { tab, groupId } }) as const;
export const setUploadableFiles = (files: UploadableFile[] = []) =>
    ({ type: SET_UPLOADABLE_FILES, payload: { files } }) as const;
export const setUploadProgress = (uploadProgress: UploadProgress = undefined) =>
    ({ type: SET_UPLOAD_PROGRESS, payload: { uploadProgress } }) as const;

export const setClosedUploadingPlaylistSnackbars = (snackbarKeys: string[] = []) =>
    ({ type: SET_CLOSED_UPLOADING_PLAYLIST_SNACKBARS, payload: { snackbarKeys } }) as const;
