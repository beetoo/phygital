import React from 'react';

import DeleteLocationModal from '../../@pages/ScreensPage/components/DeleteLocationModal';
import AddScreenModal from '../../@pages/ScreensPage/components/AddScreenModal';
import DeleteScreenModal from '../../@pages/ScreensPage/components/DeleteScreenModal';
import DeleteScreensModal from '../../@pages/ScreensPage/components/DeleteScreensModal';
import DeletePlaylistAlert from '../../@pages/PlaylistsPage/components/DeletePlaylistAlert';
import DeleteMultiplyPlaylistAlert from '../../@pages/PlaylistsPage/components/DeleteMultyplyPlaylistsAlert';
import DeletePlaylistItemAlert from '../../@pages/PlaylistsPage/components/DeletePlaylistItemAlert';
import DeleteMultiplyPlaylistItemsAlert from '../../@pages/PlaylistsPage/components/DeleteMultiplyPlaylistItemsAlert';
import StartPlaylistModal from '../../@pages/PlaylistsPage/components/StartPlaylistModal';
import DeleteContentItemAlert from '../../@pages/ContentPage/components/DeleteContentItemAlert';
import CancelCreationPlaylistAlert from '../../@pages/PlaylistsPage/components/CancelCreationPlaylistAlert';
import DeleteFolderModal from '../../@pages/ContentPage/components/DeleteFolderModal';
import TriggerScenarioSettingModal from '../../@pages/PlaylistsPage/components/PlaylistStepTwo/components/TriggerScenarioSettingModal';
import ModalTemplate from '../ModalTemplate';
import DocumentationModals from '../../@pages/ProfilePage/components/DocumentationModals';
import UploadContentModal from '../../@pages/ContentPage/components/UploadContentModal';
import UserRequestModal from './components/UserRequestModal';
import BalanceRequestFormModal from './components/BalanceRequestFormModal';
import ContentViewModal from './components/ContentViewModal';
import DeleteGeneralModal from './components/DeleteGeneralModal';
import SupportModal from './components/SupportModal';
import RequestStorageAmountModal from './components/RequestStorageAmountModal';
import MaxScreensAlertModal from '../../@pages/ScreensPage/components/AddScreenModal/components/MaxScreensAlertModal';
import DraftPlaylistSaveModal from '../../@pages/PlaylistsPage/components/PlaylistStepThree/components/DraftPlaylistSaveModal';
import AddWebContentModal from '../../@pages/ContentPage/components/AddWebContentModal';
import AddWebContentToPlaylistModal from '../../@pages/ContentPage/components/AddWebContentToPlaylistModal';
import ChangePlayerModal from '../../@pages/ScreensPage/components/ChangePlayerModal';

import { useStartDeleteSnackbars } from '../../hooks/useStartDeleteSnackbars';
import { useStartUploadSnackbar } from '../../hooks/useStartUploadSnackbar';
import { useModals } from './hooks/useModals';
import { MODALS } from './types';

export const ModalsWithAuthContainer: React.FC = () => {
    const { currentModal, authorizationStatus, isSomeNewModalsOpen, isAddScreenModalOpen, t } = useModals();

    useStartUploadSnackbar();
    useStartDeleteSnackbars();

    return authorizationStatus === 'active' ? (
        <>
            <DeleteLocationModal />
            {isAddScreenModalOpen && <AddScreenModal />}
            <DeletePlaylistAlert />
            <DeletePlaylistItemAlert />
            <DeleteMultiplyPlaylistItemsAlert />
            <StartPlaylistModal />
            <ContentViewModal />
            <CancelCreationPlaylistAlert />

            <ModalTemplate isOpen={isSomeNewModalsOpen}>
                <DocumentationModals />
                {currentModal === MODALS.DELETE_MULTIPLY_PLAYLISTS && <DeleteMultiplyPlaylistAlert />}
                {currentModal === MODALS.DELETE_CONTENT_FOLDER && <DeleteFolderModal />}
                {currentModal === MODALS.DELETE_CONTENT_ITEM && <DeleteContentItemAlert />}
                {currentModal === MODALS.BALANCE_REQUEST_FORM_MODAL && <BalanceRequestFormModal />}
                {currentModal === MODALS.DELETE_SCREEN && <DeleteScreenModal />}
                {currentModal === MODALS.DELETE_SCREENS && <DeleteScreensModal />}
                {currentModal === MODALS.TRIGGER_SCENARIO_SETTING && <TriggerScenarioSettingModal />}
                {currentModal === MODALS.TV_BOX_REQUEST_MODAL && <UserRequestModal title={t('requestForAdvice')} />}
                {currentModal === MODALS.MAX_SCREENS_ALERT_MODAL && <MaxScreensAlertModal />}
                {currentModal === MODALS.UPLOAD_CONTENT_MODAL && <UploadContentModal />}
                {currentModal === MODALS.PHYGITAL_VIEW_REQUEST_MODAL && (
                    <UserRequestModal title={t('requestForPhygitalView')} />
                )}
                {currentModal === MODALS.REQUEST_STORAGE_AMOUNT_MODAL && <RequestStorageAmountModal />}
                {currentModal === MODALS.DELETE_GENERAL_MODAL && <DeleteGeneralModal />}
                {currentModal === MODALS.SUPPORT_MODAL && <SupportModal />}
                {currentModal === MODALS.DRAFT_PLAYLIST_SAVE_MODAL && <DraftPlaylistSaveModal />}
                {currentModal === MODALS.ADD_WEB_CONTENT_MODAL && <AddWebContentModal />}
                {currentModal === MODALS.ADD_WEB_CONTENT_TO_PLAYLIST_MODAL && <AddWebContentToPlaylistModal />}
                {currentModal === MODALS.CHANGE_PLAYER_MODAL && <ChangePlayerModal />}
            </ModalTemplate>
        </>
    ) : null;
};
