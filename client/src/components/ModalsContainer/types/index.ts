import {
    setClosedUploadingPlaylistSnackbars,
    setHideModal,
    setShowModal,
    setCloseContentViewModal,
    setContentViewIndex,
    setOpenContentViewModal,
    setUploadableFiles,
    setUploadProgress,
    setActiveUploadVideoTab,
    setShowModalNew,
} from '../actions';

import { ActivePage } from '../../OrganizationContainer/types';
import { CONTENT_UPLOAD_WARNINGS } from '../../../@pages/ContentPage/constants';
import { CONTENT_TYPE } from '../../../../../common/types';

export enum MODALS {
    PRIVACY_POLICY = 'PRIVACY_POLICY',
    CANCEL_CREATION_PLAYLIST = 'CANCEL_CREATION_PLAYLIST',
    DELETE_CONTENT_ITEM = 'DELETE_CONTENT_ITEM',
    DELETE_LOCATION = 'DELETE_LOCATION',
    DELETE_SCREEN = 'DELETE_SCREEN',
    DELETE_SCREENS = 'DELETE_SCREENS',
    DELETE_MULTIPLY_PLAYLISTS = 'DELETE_MULTIPLY_PLAYLISTS',
    DELETE_MULTIPLY_PLAYLIST_ITEMS = 'DELETE_MULTIPLY_PLAYLIST_ITEMS',
    PRIVACY_POLICY_DOCUMENTATION = 'PRIVACY_POLICY_DOCUMENTATION',
    DOCUMENTATION_MIT_LICENSE = 'DOCUMENTATION_MIT_LICENSE',
    DOCUMENTATION_COPYRIGHT = 'DOCUMENTATION_COPYRIGHT',
    PUBLIC_OFFER_DOCUMENTATION = 'PUBLIC_OFFER_DOCUMENTATION',
    DELETE_CONTENT_FOLDER = 'DELETE_CONTENT_FOLDER',
    TRIGGER_SCENARIO_SETTING = 'TRIGGER_SCENARIO_SETTING',
    BALANCE_REQUEST_FORM_MODAL = 'BALANCE_REQUEST_FORM_MODAL',
    TV_BOX_INFO_MODAL = 'TV_BOX_INFO_MODAL',
    TV_BOX_SECOND_INFO_MODAL = 'TV_BOX_SECOND_INFO_MODAL',
    TV_BOX_REQUEST_MODAL = 'TV_BOX_REQUEST_MODAL',
    PHYGITAL_VIEW_REQUEST_MODAL = 'PHYGITAL_VIEW_REQUEST_MODAL',
    CONNECT_SCREEN_INFO_MODAL = 'CONNECT_SCREEN_INFO_MODAL',
    ADD_SCREEN_MODAL = 'ADD_SCREEN_MODAL',
    UPLOAD_CONTENT_MODAL = 'UPLOAD_CONTENT_MODAL',
    REQUEST_STORAGE_AMOUNT_MODAL = 'REQUEST_STORAGE_AMOUNT_MODAL',
    DELETE_GENERAL_MODAL = 'DELETE_GENERAL_MODAL',
    SUPPORT_MODAL = 'SUPPORT_MODAL',
    MAX_SCREENS_ALERT_MODAL = 'MAX_SCREENS_ALERT_MODAL',
    DRAFT_PLAYLIST_SAVE_MODAL = 'DRAFT_PLAYLIST_SAVE_MODAL',
    ADD_WEB_CONTENT_MODAL = 'ADD_WEB_CONTENT_MODAL',
    ADD_WEB_CONTENT_TO_PLAYLIST_MODAL = 'ADD_WEB_CONTENT_TO_PLAYLIST_MODAL',
    CHANGE_PLAYER_MODAL = 'CHANGE_PLAYER_MODAL',
}

export type UploadVideoTab = 'whole' | 'parts';

export type UploadableFile = {
    id: string;
    file: File;
    progress: number;
    dimensions?: { width: number; height: number };
    duration?: number;
    warning?: CONTENT_UPLOAD_WARNINGS;
    done: boolean;
    error: boolean;
};

export type UploadProgress =
    | {
          id: string;
          progress: number;
          error: boolean;
          done: boolean;
      }
    | undefined;

export type ViewItemType = {
    name: string;
    src: string;
    type?: CONTENT_TYPE;
    dimensions?: { width: number; height: number };
};

export type ModalsState = {
    modal: MODALS | '';
    prevModal: MODALS | '';
    pageGoingTo: ActivePage;
    savedId: string | string[];

    modalData: unknown;

    isContentViewModalOpen: boolean;
    viewItems: ViewItemType[];
    viewIndex: number;

    uploadVideoTab: UploadVideoTab;
    uploadGroupId: string;
    uploadableFiles: UploadableFile[];
    uploadProgress: UploadProgress;
    closedSnackbars: string[];
};

export type ShowModalAction = ReturnType<typeof setShowModal>;
export type ShowModalNewAction = ReturnType<typeof setShowModalNew>;
export type HideModalAction = ReturnType<typeof setHideModal>;

export type OpenContentViewModalAction = ReturnType<typeof setOpenContentViewModal>;
export type CloseContentViewModalAction = ReturnType<typeof setCloseContentViewModal>;
export type SetContentViewIndexAction = ReturnType<typeof setContentViewIndex>;

export type SetActiveUploadVideoTabAction = ReturnType<typeof setActiveUploadVideoTab>;
export type SetUploadableFilesAction = ReturnType<typeof setUploadableFiles>;
export type SetUploadProgressAction = ReturnType<typeof setUploadProgress>;

export type SetClosedUploadingPlaylistSnackbars = ReturnType<typeof setClosedUploadingPlaylistSnackbars>;

export type ModalsAction =
    | ShowModalAction
    | ShowModalNewAction
    | HideModalAction
    | OpenContentViewModalAction
    | CloseContentViewModalAction
    | SetContentViewIndexAction
    | SetActiveUploadVideoTabAction
    | SetUploadableFilesAction
    | SetUploadProgressAction
    | SetClosedUploadingPlaylistSnackbars;
