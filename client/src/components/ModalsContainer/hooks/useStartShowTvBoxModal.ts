import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useMatch } from 'react-router-dom';

import { setShowModal } from '../actions';
import { MODALS } from '../types';
import { selectScreens } from '../../../@pages/ScreensPage/selectors';
import { TV_BOX_INFO_SHOWN } from '../../../constants';
import { selectRecoveryPasswordSuccessStatus } from '../../../@pages/RecoveryPassword/selectors';

export const useStartShowTvBoxModal = (): void => {
    const dispatch = useDispatch();

    const mainPageMatch = useMatch('/main');

    const screens = useSelector(selectScreens);
    const isChangePasswordSuccess = useSelector(selectRecoveryPasswordSuccessStatus);

    useEffect(() => {
        if (!mainPageMatch || isChangePasswordSuccess) return;

        const isTvBoxModalShown = localStorage.getItem(TV_BOX_INFO_SHOWN) === 'true';

        if (screens.length > 0 && !isTvBoxModalShown) {
            localStorage.setItem(TV_BOX_INFO_SHOWN, 'true');
        }

        let timeout: number;
        if (!isTvBoxModalShown) {
            timeout = window.setTimeout(() => {
                if (screens.length === 0) {
                    dispatch(setShowModal(MODALS.TV_BOX_INFO_MODAL));
                }
            }, 1000);
        }

        return () => {
            window.clearTimeout(timeout);
        };
    }, [dispatch, isChangePasswordSuccess, mainPageMatch, screens.length]);
};
