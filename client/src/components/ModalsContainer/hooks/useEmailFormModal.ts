import { ChangeEvent, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import { isPossiblePhoneNumber } from 'react-phone-number-input';
import isEmail from 'validator/es/lib/isEmail';

import { sendEmailMessageViaEmailForm } from '../../../resolvers/organizationsResolvers';
import { selectCurrentOrganization } from '../../../@pages/ProfilePage/selectors';
import { setHideModal } from '../actions';

type ReturnValue = {
    email: string;
    phone: string;
    messageText: string;
    onChangeEmail: (e: ChangeEvent<HTMLInputElement>) => void;
    onChangePhone: (value: string) => void;
    onChangeMessageText: (e: ChangeEvent<HTMLTextAreaElement>) => void;
    sendEmailMessage: () => void;
    isSubmitSendEmailButtonDisabled: boolean;
    emailInputError: string | false;
    phoneInputError: string | false;
    closeModal: () => void;
    t: TFunction;
};

export const useEmailFormModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'menu' });
    const { t: tProfileTab } = useTranslation('translation', { keyPrefix: 'profileTab' });

    const dispatch = useDispatch();

    const { title, email: organizationEmail, phone: organizationPhone } = useSelector(selectCurrentOrganization);

    const [email, setEmail] = useState(organizationEmail);
    const [phone, setPhone] = useState(organizationPhone);
    const [messageText, setMessageText] = useState('');

    const [emailInputError, setEmailInputError] = useState<string | false>(false);
    const [phoneInputError, setPhoneInputError] = useState<string | false>(false);

    const onChangeEmail = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setEmailInputError(false);
        setEmail(value);
    }, []);

    const onChangePhone = useCallback((value: string) => {
        setPhoneInputError(false);
        setPhone(value ?? '');
    }, []);

    const onChangeMessageText = useCallback(({ target: { value } }: ChangeEvent<HTMLTextAreaElement>) => {
        if (value.length <= 4096) {
            setMessageText(value);
        }
    }, []);

    const closeModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    const sendEmailMessage = useCallback(() => {
        if (!isEmail(email)) {
            return setEmailInputError(tProfileTab('incorrectEmail'));
        }

        if (phone && !isPossiblePhoneNumber(phone)) {
            return setPhoneInputError(tProfileTab('incorrectPhoneNumber'));
        }

        sendEmailMessageViaEmailForm({ title, email, phone, messageText }).then(() => {
            closeModal();
        });
    }, [closeModal, email, messageText, phone, tProfileTab, title]);

    const isSubmitSendEmailButtonDisabled = email.trim() === '' || phone.trim() === '' || messageText.trim() === '';

    return {
        email,
        phone,
        messageText,
        onChangeEmail,
        onChangePhone,
        onChangeMessageText,
        sendEmailMessage,
        isSubmitSendEmailButtonDisabled,
        emailInputError,
        phoneInputError,
        closeModal,
        t,
    };
};
