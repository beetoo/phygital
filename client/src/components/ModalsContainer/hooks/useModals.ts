import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { MODALS } from '../types';
import { selectCurrentModal } from '../selectors';
import { selectAddScreenModalOpen } from '../../../@pages/ScreensPage/selectors';
import { selectAuthorizationStatus } from '../../../selectors';

type ReturnValue = {
    currentModal: MODALS | '';
    authorizationStatus: 'active' | 'pending';
    isSomeNewModalsOpen: boolean;
    isAddScreenModalOpen: boolean;
    t: TFunction;
};

export const useModals = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'screensModal' });

    const currentModal = useSelector(selectCurrentModal);

    const isAddScreenModalOpen = useSelector(selectAddScreenModalOpen);

    const authorizationStatus = useSelector(selectAuthorizationStatus);

    const isSomeNewModalsOpen = Object.values(MODALS)
        .filter(
            (modal) =>
                ![
                    MODALS.DELETE_LOCATION,
                    MODALS.DELETE_MULTIPLY_PLAYLIST_ITEMS,
                    MODALS.CANCEL_CREATION_PLAYLIST,
                ].includes(modal),
        )
        .includes(currentModal as MODALS);

    return {
        currentModal,
        authorizationStatus,
        isSomeNewModalsOpen,
        isAddScreenModalOpen,
        t,
    };
};
