import { useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import { isPossiblePhoneNumber } from 'react-phone-number-input';

import { sendStorageAmountRequest } from '../../../resolvers/organizationsResolvers';
import { selectCurrentOrganization } from '../../../@pages/ProfilePage/selectors';
import { MODALS } from '../types';
import { setHideModal, setShowModal } from '../actions';
import { useCustomRouter } from '../../../hooks/useCustomRouter';

type ReturnValue = {
    phone: string;
    onChangePhone: (value: string) => void;
    phoneInputError: string | false;

    sendRequest: () => void;
    isSendRequestButtonDisabled: boolean;

    closeModal: () => void;
    t: TFunction;
};

export const useRequestStorageAmountModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'phygitalViewModal' });
    const { t: tProfileTab } = useTranslation('translation', { keyPrefix: 'profileTab' });

    const dispatch = useDispatch();

    const { currentPage } = useCustomRouter();

    const { title, email, phone: organizationPhone } = useSelector(selectCurrentOrganization);

    const [phone, setPhone] = useState(organizationPhone);
    const [phoneInputError, setPhoneInputError] = useState<string | false>(false);

    const onChangePhone = useCallback((value: string) => {
        setPhoneInputError(false);
        setPhone(value ?? '');
    }, []);

    const closeModal = useCallback(() => {
        dispatch(setShowModal(MODALS.UPLOAD_CONTENT_MODAL));
    }, [dispatch]);

    const sendRequest = useCallback(() => {
        if (phone && !isPossiblePhoneNumber(phone)) {
            return setPhoneInputError(tProfileTab('incorrectPhoneNumber'));
        }

        const phygitalMode = currentPage === 'uploaded_videos' ? 'View' : 'Signage';

        sendStorageAmountRequest({
            phone,
            title,
            email,
            phygitalMode,
        }).then(() => {
            dispatch(setHideModal());
        });
    }, [currentPage, dispatch, email, phone, tProfileTab, title]);

    const isSendRequestButtonDisabled = phone?.trim() === '';

    return {
        phone,
        onChangePhone,
        phoneInputError,

        sendRequest,
        isSendRequestButtonDisabled,

        closeModal,
        t,
    };
};
