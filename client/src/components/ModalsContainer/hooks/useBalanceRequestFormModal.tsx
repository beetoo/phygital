import React, { ChangeEvent, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import { isPossiblePhoneNumber } from 'react-phone-number-input';
import isEmail from 'validator/es/lib/isEmail';

import { sendBalanceRequestViaEmailForm } from '../../../resolvers/organizationsResolvers';
import { SuccessSnackbar } from '../../SuccessSnackbar';
import { selectCurrentOrganization } from '../../../@pages/ProfilePage/selectors';
import { setHideModal } from '../actions';
import { selectModalData } from '../selectors';
import { BalanceRequestType } from '../../../../../common/types';

type ReturnValue = {
    title: string;
    email: string;
    phone: string;
    messageText: string;
    onChangeTitle: (e: ChangeEvent<HTMLInputElement>) => void;
    onChangeEmail: (e: ChangeEvent<HTMLInputElement>) => void;
    onChangePhone: (value: string) => void;
    onChangeMessageText: (e: ChangeEvent<HTMLTextAreaElement>) => void;
    sendEmailMessage: () => void;
    isSubmitSendEmailButtonDisabled: boolean;
    emailInputError: string | false;
    phoneInputError: string | false;
    closeModal: () => void;
    t: TFunction;
};

export const useBalanceRequestFormModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'menu' });
    const { t: tProfileTab } = useTranslation('translation', { keyPrefix: 'profileTab' });

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const dispatch = useDispatch();

    const {
        title: organizationTitle,
        email: organizationEmail,
        phone: organizationPhone,
    } = useSelector(selectCurrentOrganization);

    const { balanceRequestType, screensNumber } = useSelector(selectModalData) as {
        balanceRequestType: BalanceRequestType;
        screensNumber: number;
    };

    const [title, setTitle] = useState(organizationTitle);
    const [email, setEmail] = useState(organizationEmail);
    const [phone, setPhone] = useState(organizationPhone);
    const [messageText, setMessageText] = useState('');

    const [emailInputError, setEmailInputError] = useState<string | false>(false);
    const [phoneInputError, setPhoneInputError] = useState<string | false>(false);

    const onChangeTitle = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setTitle(value);
    }, []);

    const onChangeEmail = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setEmailInputError(false);
        setEmail(value);
    }, []);

    const onChangePhone = useCallback((value: string) => {
        setPhoneInputError(false);
        setPhone(value ?? '');
    }, []);

    const onChangeMessageText = useCallback(({ target: { value } }: ChangeEvent<HTMLTextAreaElement>) => {
        if (value.length <= 4096) {
            setMessageText(value);
        }
    }, []);

    const closeModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    const sendEmailMessage = useCallback(() => {
        if (!isEmail(email)) {
            return setEmailInputError(tProfileTab('incorrectEmail'));
        }

        if (phone && !isPossiblePhoneNumber(phone)) {
            return setPhoneInputError(tProfileTab('incorrectPhoneNumber'));
        }

        sendBalanceRequestViaEmailForm({
            title,
            email,
            phone,
            messageText,
            balanceRequestType,
            screensNumber,
        })
            .then(() => {
                closeModal();
                const closeSnackbarKey = enqueueSnackbar(
                    <SuccessSnackbar
                        title={tProfileTab('thankRequestSent')}
                        onClose={() => closeSnackbar(closeSnackbarKey)}
                    />,
                    {
                        key: 'BalanceRequest',
                        autoHideDuration: 3000,
                    },
                );
            })
            .catch((err) => console.log(err));
    }, [
        balanceRequestType,
        closeModal,
        closeSnackbar,
        email,
        enqueueSnackbar,
        messageText,
        phone,
        screensNumber,
        tProfileTab,
        title,
    ]);

    const isSubmitSendEmailButtonDisabled = title.trim() === '' || email.trim() === '' || phone.trim() === '';

    return {
        title,
        email,
        phone,
        messageText,
        onChangeTitle,
        onChangeEmail,
        onChangePhone,
        onChangeMessageText,
        sendEmailMessage,
        isSubmitSendEmailButtonDisabled,
        emailInputError,
        phoneInputError,
        closeModal,
        t,
    };
};
