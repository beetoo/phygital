import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setShowModal } from '../actions';
import { MODALS } from '../types';

type ReturnValue = {
    openTvBoxRequestModal: () => void;
    openTvBoxSecondModal: () => void;
    t: TFunction;
};

export const useTvBoxInfoModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'onboarding' });

    const dispatch = useDispatch();

    const openTvBoxRequestModal = useCallback(() => {
        dispatch(setShowModal(MODALS.TV_BOX_REQUEST_MODAL, MODALS.TV_BOX_INFO_MODAL));
    }, [dispatch]);

    const openTvBoxSecondModal = useCallback(() => {
        dispatch(setShowModal(MODALS.TV_BOX_SECOND_INFO_MODAL));
    }, [dispatch]);

    return {
        openTvBoxRequestModal,
        openTvBoxSecondModal,
        t,
    };
};
