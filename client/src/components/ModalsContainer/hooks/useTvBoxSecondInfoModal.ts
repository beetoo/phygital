import { useCallback } from 'react';
import { useDispatch } from 'react-redux';

import { setHideModal, setShowModal } from '../actions';
import { setOnboardingEnabled } from '../../../actions';
import { MODALS } from '../types';
import { TV_BOX_INFO_SHOWN } from '../../../constants';

type ReturnValue = {
    openTvBoxRequestModal: () => void;
    closeModal: () => void;
};

export const useTvBoxSecondInfoModal = (): ReturnValue => {
    const dispatch = useDispatch();

    const openTvBoxRequestModal = useCallback(() => {
        dispatch(setShowModal(MODALS.TV_BOX_REQUEST_MODAL, MODALS.TV_BOX_SECOND_INFO_MODAL));
    }, [dispatch]);

    const closeModal = useCallback(() => {
        localStorage.setItem(TV_BOX_INFO_SHOWN, 'true');
        dispatch(setHideModal());
        dispatch(setOnboardingEnabled());
    }, [dispatch]);

    return {
        openTvBoxRequestModal,
        closeModal,
    };
};
