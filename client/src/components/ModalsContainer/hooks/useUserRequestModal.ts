import { ChangeEvent, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import { isPossiblePhoneNumber } from 'react-phone-number-input';
import isEmail from 'validator/es/lib/isEmail';

import { sendTvBoxRequestViaEmailForm } from '../../../resolvers/organizationsResolvers';
import { selectCurrentOrganization } from '../../../@pages/ProfilePage/selectors';
import { setHideModal, setShowModal } from '../actions';
import { setOnboardingEnabled } from '../../../actions';
import { TV_BOX_INFO_SHOWN } from '../../../constants';
import { selectPrevModal } from '../selectors';
import { MODALS } from '../types';
import { setOpenAddScreenModal } from '../../../@pages/ScreensPage/actions';

type ReturnValue = {
    userName: string;
    phone: string;
    organizationTitle: string;
    email: string;
    onChangeName: (e: ChangeEvent<HTMLInputElement>) => void;
    onChangePhone: (value: string) => void;
    onChangeTitle: (e: ChangeEvent<HTMLInputElement>) => void;
    onChangeEmail: (e: ChangeEvent<HTMLInputElement>) => void;

    phoneInputError: string | false;
    emailInputError: string | false;

    sendRequest: () => void;
    isSubmitSendRequestButtonDisabled: boolean;

    closeModal: () => void;

    t: TFunction;
};

export const useUserRequestModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'screensModal' });
    const { t: tProfileTab } = useTranslation('translation', { keyPrefix: 'profileTab' });

    const dispatch = useDispatch();

    const { title, email: organizationEmail, phone: organizationPhone } = useSelector(selectCurrentOrganization);
    const prevModal = useSelector(selectPrevModal);

    const [userName, setUserName] = useState('');
    const [phone, setPhone] = useState(organizationPhone);
    const [organizationTitle, setOrganizationTitle] = useState(title);
    const [email, setEmail] = useState(organizationEmail);

    const [phoneInputError, setPhoneInputError] = useState<string | false>(false);
    const [emailInputError, setEmailInputError] = useState<string | false>(false);

    const onChangeName = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setUserName(value);
    }, []);

    const onChangePhone = useCallback((value: string) => {
        setPhoneInputError(false);
        setPhone(value ?? '');
    }, []);

    const onChangeTitle = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setOrganizationTitle(value);
    }, []);

    const onChangeEmail = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setEmailInputError(false);
        setEmail(value);
    }, []);

    const isTvBoxInfoShown = localStorage.getItem(TV_BOX_INFO_SHOWN) === 'true';

    const closeModal = useCallback(() => {
        if (prevModal === MODALS.ADD_SCREEN_MODAL) {
            dispatch(setHideModal());
            return dispatch(setOpenAddScreenModal());
        }
        dispatch(prevModal ? setShowModal(prevModal) : setHideModal());
    }, [dispatch, prevModal]);

    const sendRequest = useCallback(() => {
        if (!isPossiblePhoneNumber(phone)) {
            return setPhoneInputError(tProfileTab('incorrectPhoneNumber'));
        }

        if (!isEmail(email)) {
            return setEmailInputError(tProfileTab('incorrectEmail'));
        }

        sendTvBoxRequestViaEmailForm({ userName, phone, title: organizationTitle, email }).then(() => {
            if (!isTvBoxInfoShown) {
                localStorage.setItem(TV_BOX_INFO_SHOWN, 'true');
                dispatch(setOnboardingEnabled());
            }
            dispatch(setHideModal());
        });
    }, [phone, email, userName, organizationTitle, tProfileTab, isTvBoxInfoShown, dispatch]);

    const isSubmitSendRequestButtonDisabled =
        userName.trim() === '' || phone?.trim() === '' || organizationTitle.trim() === '' || email.trim() === '';

    return {
        userName,
        phone,
        organizationTitle,
        email,

        onChangeName,
        onChangeTitle,
        onChangeEmail,
        onChangePhone,

        phoneInputError,
        emailInputError,

        sendRequest,
        isSubmitSendRequestButtonDisabled,

        closeModal,

        t,
    };
};
