import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setHideModal, setShowModalNew } from '../actions';
import { MODALS } from '../types';

type ReturnValue = {
    goToContentPage: () => void;
    openSupportModal: () => void;
    closeModal: () => void;
    t: TFunction;
};

export const useNoActiveStorageSubscriptionsModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'contentModal' });

    const dispatch = useDispatch();

    const goToContentPage = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    const openSupportModal = useCallback(() => {
        dispatch(
            setShowModalNew(MODALS.SUPPORT_MODAL, {
                onClose: () => {
                    dispatch(setHideModal());
                },
            }),
        );
    }, [dispatch]);

    const closeModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    return {
        goToContentPage,
        openSupportModal,
        closeModal,
        t,
    };
};
