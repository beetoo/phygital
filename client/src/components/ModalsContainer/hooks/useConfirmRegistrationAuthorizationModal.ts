import { useCallback, useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectAuthEmail, selectAuthUserId } from '../../../selectors';
import { httpService } from '../../../helpers/httpService';
import { setSignOutProcess } from '../../../actions';
import { formatSecondsToTimeString } from '../../../../../common/helpers';

type ReturnValue = {
    activeEmail: string;
    sendConfirmEmailButtonText: string;
    sendConfirmRegistrationEmail: () => void;
    isSendConfirmEmailButtonDisabled: boolean;
    handleLogout: () => void;
    t: TFunction;
};

export const useConfirmRegistrationAuthorizationModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'signUp' });

    const dispatch = useDispatch();

    const userId = useSelector(selectAuthUserId);
    const activeEmail = useSelector(selectAuthEmail);

    const secondsLeftStarted = Math.round(
        300 - (Date.now() - Number(localStorage.getItem('lastConfirm') ?? '0')) / 1000,
    );

    const [secondsLeft, setSecondsLeft] = useState(secondsLeftStarted);

    const sendConfirmEmailButtonText =
        secondsLeft > 0 ? formatSecondsToTimeString(secondsLeft, true) : t('resendEmail');

    const isSendConfirmEmailButtonDisabled = secondsLeft > 0;

    const intervalRef = useRef<number | null>(null);

    useEffect(() => {
        if (secondsLeft > 0 && !intervalRef.current) {
            intervalRef.current = window.setInterval(() => {
                setSecondsLeft((prev) => (prev > 0 ? prev - 1 : 0));
            }, 1000);
        } else if (secondsLeft <= 0 && intervalRef.current) {
            window.clearInterval(intervalRef.current);
            intervalRef.current = null;
        }
    }, [secondsLeft]);

    const sendConfirmRegistrationEmail = useCallback(async () => {
        if (userId) {
            await httpService.post(`user/${userId}/email/verification/resend`, {});

            setSecondsLeft(300);
            localStorage.setItem('lastConfirm', `${Date.now()}`);
        }
    }, [userId]);

    const handleLogout = useCallback(() => {
        localStorage.removeItem('lastPage');
        dispatch(setSignOutProcess());

        if (intervalRef.current) {
            window.clearInterval(intervalRef.current);
            intervalRef.current = null;
        }
    }, [dispatch]);

    return {
        activeEmail,
        sendConfirmEmailButtonText,
        sendConfirmRegistrationEmail,
        isSendConfirmEmailButtonDisabled,
        handleLogout,
        t,
    };
};
