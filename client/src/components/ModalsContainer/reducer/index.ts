import {
    HIDE_MODAL,
    SET_CLOSED_UPLOADING_PLAYLIST_SNACKBARS,
    SET_UPLOAD_PROGRESS,
    SET_UPLOAD_VIDEO_TAB,
    SET_UPLOADABLE_FILES,
    SHOW_MODAL,
    CLOSE_CONTENT_VIEW_MODAL,
    OPEN_CONTENT_VIEW_MODAL,
    SET_CONTENT_VIEW_INDEX,
    SHOW_MODAL_NEW,
} from '../actions';

import { ModalsAction, ModalsState } from '../types';

const modalsInitialState: ModalsState = {
    modal: '',
    pageGoingTo: '',
    savedId: '',
    prevModal: '',

    modalData: null,

    isContentViewModalOpen: false,
    viewItems: [],
    viewIndex: 0,

    uploadVideoTab: 'whole',
    uploadGroupId: '',
    uploadableFiles: [],
    uploadProgress: undefined,
    closedSnackbars: JSON.parse(window.localStorage.getItem('CLOSED_SNACKBARS') ?? '[]'),
};

export default function modalsReducer(state = modalsInitialState, action: ModalsAction): ModalsState {
    switch (action.type) {
        case SHOW_MODAL:
            return {
                ...state,
                modal: action.payload.modal,
                prevModal: action.payload.prevModal,
                pageGoingTo: action.payload.pageGoingTo,
                savedId: action.payload.savedId,
            };

        case SHOW_MODAL_NEW:
            return {
                ...state,
                modal: action.payload.modal,
                modalData: action.payload.modalData,
            };

        case HIDE_MODAL:
            return {
                ...state,
                modal: '',
                prevModal: '',
                pageGoingTo: '',
                savedId: '',
                modalData: null,
            };

        case OPEN_CONTENT_VIEW_MODAL:
            return {
                ...state,
                isContentViewModalOpen: true,
                viewItems: action.payload.viewItems,
                viewIndex: action.payload.viewIndex,
            };
        case CLOSE_CONTENT_VIEW_MODAL:
            return {
                ...state,
                isContentViewModalOpen: false,
            };

        case SET_CONTENT_VIEW_INDEX:
            return {
                ...state,
                viewIndex: action.payload.viewIndex,
            };

        case SET_UPLOAD_VIDEO_TAB:
            return {
                ...state,
                uploadVideoTab: action.payload.tab,
                uploadGroupId: action.payload.groupId,
            };

        case SET_UPLOADABLE_FILES:
            return {
                ...state,
                uploadableFiles: action.payload.files,
            };

        case SET_UPLOAD_PROGRESS:
            return {
                ...state,
                uploadProgress: action.payload.uploadProgress,
            };

        case SET_CLOSED_UPLOADING_PLAYLIST_SNACKBARS:
            return {
                ...state,
                closedSnackbars: action.payload.snackbarKeys,
            };

        default:
            return state;
    }
}
