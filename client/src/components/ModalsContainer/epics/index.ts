import { Observable } from 'rxjs';
import { combineEpics, ofType } from 'redux-observable';
import { map } from 'rxjs/operators';

import { setHideModal } from '../actions';
import { HideModalAction } from '../types';
import { UpdatePlaylistSuccessAction } from '../../../@pages/PlaylistsPage/types';
import { StoreType } from '../../../types';
import { UPDATE_PLAYLIST_SUCCESS } from '../../../@pages/PlaylistsPage/actions';

const hideModalOnSuccessEpic = (action$: Observable<UpdatePlaylistSuccessAction>): Observable<HideModalAction> =>
    action$.pipe(ofType(UPDATE_PLAYLIST_SUCCESS), map(setHideModal));

type ModalEpicsActions = UpdatePlaylistSuccessAction | HideModalAction;

export const modalsEpics = combineEpics<ModalEpicsActions, ModalEpicsActions, StoreType>(hideModalOnSuccessEpic);
