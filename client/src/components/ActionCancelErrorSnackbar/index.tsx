import ErrorRoundedIcon from '@mui/icons-material/ErrorRounded';
import React, { FC } from 'react';
import { Snackbar } from '../../ui-kit';

export const ActionCancelErrorSnackbar: FC<{ onClose: () => void }> = ({ onClose }) => (
    <Snackbar
        startAdornment={<ErrorRoundedIcon color="error" />}
        title="Действие не может быть отменено."
        onClose={onClose}
    />
);
