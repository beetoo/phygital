import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { YMInitializer } from 'react-yandex-metrika';
import { ThemeProvider } from '@mui/material';

import MainLayout from './components/MainLayout';
import store from './store';
import { muiTheme } from './theme/muiTheme';
import { SnackbarProvider } from './ui-kit';
import { isProductionClient } from './constants/environments';

const App: React.FC = () => (
    <>
        {isProductionClient && <YMInitializer accounts={[87177269]} options={{ webvisor: true }} version="2" />}
        <Provider store={store}>
            <BrowserRouter>
                <ThemeProvider theme={muiTheme}>
                    <SnackbarProvider>
                        <MainLayout />
                    </SnackbarProvider>
                </ThemeProvider>
            </BrowserRouter>
        </Provider>
    </>
);

export default App;
