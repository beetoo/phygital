import { ProviderContext } from 'notistack';
import React from 'react';
import { ErrorSnackbar } from '../components/ErrorSnackbar';

export const showErrorSnackbar = ({
    enqueueSnackbar,
    closeSnackbar,
    key,
    onClose: _onClose,
}: {
    closeSnackbar: ProviderContext['closeSnackbar'];
    enqueueSnackbar: ProviderContext['enqueueSnackbar'];
    key: string;
    onClose?: () => void;
}): (() => void) => {
    const snackbarKey = enqueueSnackbar(
        <ErrorSnackbar
            onClose={() => {
                closeSnackbar(snackbarKey);
                _onClose?.();
            }}
        />,
        {
            key,
        },
    );
    return () => closeSnackbar(snackbarKey);
};
