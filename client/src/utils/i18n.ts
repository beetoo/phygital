import * as i18n from 'i18next';
// import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';

import { isProductionEngClient } from '../constants/environments';

import en from './locales/en';
import ru from './locales/ru';
import es from './locales/es';

i18n.use(initReactI18next)
    .use(LanguageDetector)
    .init({
        lng: isProductionEngClient ? 'en' : undefined,
        fallbackLng: isProductionEngClient ? 'en' : 'ru',
        detection: { order: ['navigator'] },
        interpolation: { escapeValue: false },
        resources: {
            en: {
                translation: en,
            },
            ru: {
                translation: ru,
            },
            es: {
                translation: es,
            },
        },
    });

export default i18n;
