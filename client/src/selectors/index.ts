import { AuthState, ErrorResponse, StoreType } from '../types';
import { USER_ROLE } from '../constants';
import { ActivePage } from '../components/OrganizationContainer/types';

export const selectAuthOrganizationInfo = ({ auth }: StoreType): AuthState => auth;

export const selectAuthStatus = ({ auth }: StoreType): boolean => auth?.isAuth ?? false;

export const selectAuthOrganizationId = ({ auth }: StoreType): string => auth?.id ?? '';

export const selectAuthCheckingStatus = ({ auth }: StoreType): boolean => auth?.authChecking ?? false;

export const selectAuthorizationStatus = ({ auth }: StoreType): 'active' | 'pending' => auth?.status ?? 'pending';

export const selectTokenCheckingStatus = ({ auth }: StoreType): boolean => auth?.tokenChecking ?? false;

export const selectAuthProcessingStatus = ({ auth }: StoreType): boolean => auth?.authProcessing ?? false;

export const selectAuthErrorMessage = ({ auth }: StoreType): ErrorResponse['message'] => auth?.errors?.message ?? '';

export const selectAuthUserRole = ({ auth }: StoreType): USER_ROLE => (auth?.role as USER_ROLE) ?? USER_ROLE.USER;

export const selectAuthUserId = ({ auth: { userId } }: StoreType): string => userId ?? '';

export const selectAuthEmail = ({ auth: { email } }: StoreType): string => email ?? '';

export const selectFirstConfirmationStatus = ({ auth }: StoreType): boolean => auth?.firstConfirmation ?? false;

export const selectOnBoardingStatus = ({ onboarding }: StoreType): boolean => onboarding.isEnabled;

export const selectOnBoardingToggleRef = ({ onboarding: { onBoardingToggleRef } }: StoreType): HTMLElement | null =>
    onBoardingToggleRef;

export const selectHelpIconRef = ({ onboarding: { helpIconRef } }: StoreType): SVGSVGElement | null => helpIconRef;

export const selectScreensPageLinkRef = ({ onboarding: { screensPageLinkRef } }: StoreType): HTMLElement | null =>
    screensPageLinkRef;

export const selectPlaylistsPageLinkRef = ({ onboarding: { playlistsPageLinkRef } }: StoreType): HTMLElement | null =>
    playlistsPageLinkRef;

export const selectSkipOnBoardingStatus = ({ onboarding: { isOnBoardingSkipped } }: StoreType): boolean =>
    isOnBoardingSkipped;

// global state

export const selectPreviousPage = ({ global: { previousPage } }: StoreType): ActivePage => previousPage;
