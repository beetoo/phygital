import { OrientationRusType, OrientationTypeEng, OrientationTypeEs } from '../@pages/ScreensPage/types';

import { SUPPORTED_CONTENT_FORMATS } from '../../../common/constants';
import { OrientationEnum } from '../../../common/types';

export enum USER_ROLE {
    ADMIN = 'admin',
    MANAGER = 'manager',
    USER = 'user',
}

export const resolutionsList = ['1280×720 — HD', '1920×1080 — Full HD', '3840×2160 — 4K (UHD)'];

export const orientationsList: OrientationEnum[] = [
    OrientationEnum.LANDSCAPE,
    OrientationEnum.PORTRAIT,
    OrientationEnum['LANDSCAPE_FLIPPED'],
    OrientationEnum['PORTRAIT_FLIPPED'],
];

export const orientationsListRus: OrientationRusType[] = ['Не поворачивать', '90°', '180°', '270°'];
export const orientationsListEng: OrientationTypeEng[] = ["Don't turn", '90°', '180°', '270°'];
export const orientationsListEs: OrientationTypeEs[] = ['No girar', '90°', '180°', '270°'];

export const orientationsListShortRus: OrientationRusType[] = ['Не поворачивать', '90°'];
export const orientationsListShortEng: OrientationTypeEng[] = ["Don't turn", '90°'];
export const orientationsListShortEs: OrientationTypeEs[] = ['No girar', '90°'];

export enum RESOLUTIONS {
    HD = '1280×720 — HD',
    FULL_HD = '1920×1080 — Full HD',
    UHD = '3840×2160 — 4K (UHD)',
}

export const ACCEPTED_CONTENT_FORMATS = SUPPORTED_CONTENT_FORMATS.join(',');

export const ONBOARDING_LOCAL_STORAGE_ITEM = 'onboardingEnabled';

export const TV_BOX_INFO_SHOWN = 'TV_BOX_INFO_SHOWN';
