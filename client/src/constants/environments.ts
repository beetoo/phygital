declare const NODE_ENV: string;
declare const STAGE: string;

export const isDevelopmentClient = NODE_ENV === 'development' && STAGE === 'local';
export const isDevProductionClient = NODE_ENV === 'production' && STAGE === 'dev';
export const isProductionClient = NODE_ENV === 'production' && STAGE === 'prod';
export const isProductionEngClient = NODE_ENV === 'production' && STAGE === 'sl-eng';
export const isSLClient = NODE_ENV === 'production' && STAGE === 'sl';
export const isDevelopmentSLClient = NODE_ENV === 'development' && STAGE === 'sl-local';

export const isDevelopmentMode = isDevelopmentClient || isDevProductionClient;
export const isSLClientMode = isDevelopmentSLClient || isSLClient;
