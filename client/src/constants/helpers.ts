import { isProductionEngClient } from './environments';

export const locationCityAddressToString = (city: string, address: string): string =>
    (isProductionEngClient ? [address, city] : [city, address]).filter((value) => !!value).join(', ');

export const locationInfoToString = (
    location: string | undefined,
    city: string | undefined,
    address: string | undefined,
): string =>
    (isProductionEngClient ? [location, address, city] : [location, city, address])
        .filter((value) => !!value)
        .join(', ');
