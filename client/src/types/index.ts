import { FoldersPageState } from '../@pages/ContentPage/type';
import { MainPageState } from '../@pages/MainPage/types';
import { ScreensPageState } from '../@pages/ScreensPage/types';
import { AdminPageState } from '../@pages/AdminPage/types';
import { PlaylistsPageState } from '../@pages/PlaylistsPage/types';
import { ProfilePageState } from '../@pages/ProfilePage/types';
import { SchedulePageState } from '../@pages/DxSchedulePage/type';
import { ResetPasswordAction } from '../@pages/RecoveryPassword/types';
import { SignInAction } from '../@pages/SignIn/types';
import { SignUpAction } from '../@pages/SignUp/types';
import {
    setAuthCheckFail,
    setAuthCheckProcess,
    setAuthCheckSuccess,
    setClearAuthErrors,
    setDisableOnboarding,
    setEnableOnboarding,
    setHelpIconRef,
    setOnboardingDisabled,
    setOnboardingEnabled,
    setOnBoardingToggleRef,
    setPlaylistsPageLinkRef,
    setScreensPageLinkRef,
    setSignOutProcess,
    setSignOutSuccess,
    setSkipOnBoarding,
    setPreviousPage,
} from '../actions';
import { ModalsState } from '../components/ModalsContainer/types';
import { ActivePage } from '../components/OrganizationContainer/types';

export type ErrorResponse = {
    statusCode: number;
    message: string | { message: string; errors?: { [key: string]: string } };
};

export type SuccessResponse = {
    message?: 'OK';
    id?: string;
};

export type CancellableResponse = {
    cancelId: string;
    timeout: number;
    offlineScreenIds: false | string[];
};

export type AuthResponse = {
    id: string;
    role: string;
    status: 'active' | 'pending';
    email: string;
    name: string;
    surname: string;
    firstConfirmation?: boolean;
    token?: string;
};

export type StoreType = {
    global: GlobalState;
    auth: AuthState;
    onboarding: OnBoardingState;
    pages: {
        main: MainPageState;
        admin: AdminPageState;
        organization: {
            modals: ModalsState;
            screens: ScreensPageState;
            playlists: PlaylistsPageState;
            profile: ProfilePageState;
            folders: FoldersPageState;
            schedule: SchedulePageState;
        };
    };
};

export type GlobalState = {
    previousPage: ActivePage;
};

export type AuthState = {
    authProcessing: boolean;
    authChecking: boolean;
    tokenChecking: boolean;
    isAuth: boolean;

    isRegistrationSuccess: boolean;
    isConfirmRegistrationSuccess: boolean;
    firstConfirmation?: boolean;

    id: string;
    userId: string;
    role: string;
    status: 'active' | 'pending';
    email: string;
    name: string;
    surname: string;

    recoveryPasswordEmail: string;
    isCheckTokenSuccess: boolean;
    isSendEmailSuccess: boolean;
    isRecoveryPasswordSuccess: boolean;

    errors: ErrorResponse | null;
};

export type SetPreviousPageAction = ReturnType<typeof setPreviousPage>;

export type GlobalAction = SetPreviousPageAction;

export type AuthCheckProcessAction = ReturnType<typeof setAuthCheckProcess>;
export type AuthCheckSuccessAction = ReturnType<typeof setAuthCheckSuccess>;
export type AuthCheckFailAction = ReturnType<typeof setAuthCheckFail>;

export type SignOutProcessAction = ReturnType<typeof setSignOutProcess>;
export type SignOutSuccessAction = ReturnType<typeof setSignOutSuccess>;

export type ClearAuthErrorsAction = ReturnType<typeof setClearAuthErrors>;

export type AuthAction =
    | SignUpAction
    | SignInAction
    | ResetPasswordAction
    | AuthCheckProcessAction
    | AuthCheckSuccessAction
    | AuthCheckFailAction
    | SignOutProcessAction
    | SignOutSuccessAction
    | ClearAuthErrorsAction;

export type SetOnboardingEnabledAction = ReturnType<typeof setOnboardingEnabled>;
export type SetOnboardingDisabledAction = ReturnType<typeof setOnboardingDisabled>;
export type EnableOnboardingAction = ReturnType<typeof setEnableOnboarding>;
export type DisableOnboardingAction = ReturnType<typeof setDisableOnboarding>;

export type SkipOnBoardingAction = ReturnType<typeof setSkipOnBoarding>;

export type SetOnBoardingAction = SetOnboardingEnabledAction | SetOnboardingDisabledAction;

export type SetOnBoardingToggleRefAction = ReturnType<typeof setOnBoardingToggleRef>;
export type SetHelpIconRefAction = ReturnType<typeof setHelpIconRef>;
export type SetScreensPageLinkRefAction = ReturnType<typeof setScreensPageLinkRef>;
export type SetPlaylistsPageLinkRefAction = ReturnType<typeof setPlaylistsPageLinkRef>;

export type OnBoardingAction =
    | EnableOnboardingAction
    | DisableOnboardingAction
    | SetOnBoardingAction
    | SetOnBoardingToggleRefAction
    | SetHelpIconRefAction
    | SetScreensPageLinkRefAction
    | SetPlaylistsPageLinkRefAction
    | SkipOnBoardingAction;

export type OnBoardingState = {
    isEnabled: boolean;
    isOnBoardingSkipped: boolean;

    onBoardingToggleRef: HTMLElement | null;
    helpIconRef: SVGSVGElement | null;
    screensPageLinkRef: HTMLElement | null;
    playlistsPageLinkRef: HTMLElement | null;
};
