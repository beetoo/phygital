declare const NODE_ENV: string;
declare const STAGE: string;
declare module '*.css';
declare module '*.gif';
declare module '*.svg';
declare module '*.png';
declare module '*.mp3';
declare module '*.mp4';
