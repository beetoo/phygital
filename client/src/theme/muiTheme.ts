import { createTheme } from '@mui/material';

const spacing = 10;
const borderRadius = 6;

const fontSize = 14;

export const muiTheme = createTheme({
    typography: {
        fontSize,
        htmlFontSize: fontSize,
    },
    spacing,
    shape: { borderRadius },
    palette: {
        success: { main: '#2fd872' },
        primary: {
            main: '#5f71ff',
        },
        background: {
            default: '#3a3c4f',
        },
        warning: {
            main: '#fcb562',
        },
    },
    components: {
        MuiCardActions: {
            styleOverrides: {
                root: {
                    padding: spacing,
                    '& :not(:first-of-type)': {
                        marginLeft: spacing,
                    },
                },
            },
        },
        MuiList: {
            styleOverrides: {
                root: {
                    '&.MuiList-root': {
                        padding: spacing,
                    },
                },
            },
        },
        MuiMenuItem: {
            styleOverrides: {
                root: {
                    fontSize,
                    color: '#101431',
                    padding: spacing,
                    '&:hover': {
                        borderRadius: borderRadius,
                        background: '#e0e5ff',
                    },
                    '&:focus': {
                        color: '#3b51f9',
                    },
                },
            },
        },
    },
});

export type ThemeType = typeof muiTheme;
