import React from 'react';
import { clsx } from 'clsx';

import logo from '../../assets/logos/Logo_1.svg';
import { Tooltip } from '../../ui-kit';
import { useMainHeader } from '../../hooks/useMainHeader';

import css from './style.m.css';

const MainPageNew: React.FC = () => {
    const {
        handleLogout,
        organizationEmail,
        headerTitle,
        userRole,
        goToMainPage,
        isHeaderMenuShown,
        showHideHeaderMenu,
        goToProfileTab,
        t,
    } = useMainHeader();

    return (
        <div>
            <div className={css.headerOrganization}>
                <div className={css.containerLogoType} onClick={goToMainPage}>
                    <a>
                        <img src={logo} width="122" height="45" alt="" draggable="false" />
                    </a>
                </div>
                <div className={css.containerTabs}>
                    <div className={css.fieldTabsHeader}>
                        <a className={clsx(css.tabsHeaderLink, css.tabsHeaderLinkCurrent)}>Phygital Signage</a>
                    </div>
                </div>
                <div className={css.containerProfile}>
                    <div className={css.dropdown} onClick={showHideHeaderMenu}>
                        <ul>
                            <Tooltip title={`${headerTitle} ${organizationEmail}`} placement="bottom">
                                <li
                                    className={clsx(
                                        css.profileMain,
                                        isHeaderMenuShown ? css.profileMainUp : css.profileMainDown,
                                    )}
                                >
                                    {(headerTitle?.charAt(0) ?? '').toUpperCase()}
                                </li>
                            </Tooltip>
                        </ul>
                        <div
                            className={clsx(css.profileMenu, isHeaderMenuShown && css.shown)}
                            onClick={(e) => e.stopPropagation()}
                        >
                            <h3 className={css.profileUserName}>{headerTitle}</h3>
                            <span className={css.profileUserLogo}>{(headerTitle?.charAt(0) ?? '').toUpperCase()}</span>
                            <p className={css.profileUserEmail}>{organizationEmail}</p>
                            <p className={css.profileUser} onClick={goToProfileTab('profile')}>
                                {t('settings')}
                            </p>

                            <p className={css.tariffSection} onClick={goToProfileTab('tariff_plan')}>
                                {t('balance')}
                            </p>

                            <p className={css.logOut} onClick={handleLogout}>
                                {t('exit')}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className={css.bodyOrganization}>
                <div className={css.containerColumnOne}>
                    <div className={css.containerColumn}>
                        <div className={css.containerItem}>
                            <p>Техническая поддержка</p>
                            <button className={css.btnSupport}>Связаться с поддержкой</button>
                        </div>
                    </div>
                </div>
                <div className={css.containerColumnTwo}>
                    <div className={css.containerColumn}>
                        <div className={css.containerItem}>
                            <h2>Добро пожаловать в Фиджитал</h2>
                            <p>Сервис для удаленного управления контентом на экранах</p>
                        </div>
                        <div className={css.containerItem}>
                            <h2>Добро пожаловать в Фиджитал</h2>
                            <p>Сервис для удаленного управления контентом на экранах</p>
                        </div>
                    </div>
                </div>
                <div className={css.containerColumnThree}>
                    <div className={css.containerColumn}>
                        <div className={css.containerItem}>
                            <p>Техническая поддержка</p>
                            <button className={css.btnSupport}>Записаться на Демо</button>
                        </div>

                        <div className={css.containerItem}>
                            <p>Техническая поддержка</p>
                            <button className={css.btnSupport}>Оставить заявку</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default MainPageNew;
