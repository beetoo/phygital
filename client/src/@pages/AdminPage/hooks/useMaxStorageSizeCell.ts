import { useDispatch } from 'react-redux';
import { ChangeEvent, useCallback, useEffect, useState } from 'react';
import { setUpdateOrganizationByAdmin } from '../actions';

type Props = {
    organizationId: string;
    userId: string;
    maxStorageSize: number;
};

type ReturnValue = {
    maxStorageSizeValue: string;
    onMaxStorageSizeChange: (e: ChangeEvent<HTMLInputElement>) => void;
    updateMaxStorageSize: () => void;
    isUpdateMaxStorageSizeButtonDisabled: boolean;
};

export const useMaxStorageSizeCell = ({ organizationId, userId, maxStorageSize }: Props): ReturnValue => {
    const dispatch = useDispatch();

    const [maxStorageSizeValue, setMaxStorageSizeValue] = useState(maxStorageSize.toString());

    const onMaxStorageSizeChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        if (value.match(/^\d{0,5}$/)) {
            setMaxStorageSizeValue(value.length > 0 ? Number(value).toString() : value);
        }
    }, []);

    const updateMaxStorageSize = useCallback(() => {
        dispatch(
            setUpdateOrganizationByAdmin({ id: organizationId, userId, maxStorageSize: Number(maxStorageSizeValue) }),
        );
    }, [dispatch, maxStorageSizeValue, organizationId, userId]);

    // при клике на любом месте возвращаем значение maxScreens обратно, если инпут был пустым
    useEffect(() => {
        let listener: () => void;
        if (maxStorageSizeValue === '') {
            listener = () => setMaxStorageSizeValue(maxStorageSize.toString());
            document.addEventListener('click', listener);
        }

        return () => {
            if (listener) {
                document.removeEventListener('click', listener);
            }
        };
    }, [maxStorageSize, maxStorageSizeValue]);

    const isUpdateMaxStorageSizeButtonDisabled =
        maxStorageSizeValue === '' || Number(maxStorageSizeValue) === maxStorageSize;

    return {
        maxStorageSizeValue,
        onMaxStorageSizeChange,
        updateMaxStorageSize,
        isUpdateMaxStorageSizeButtonDisabled,
    };
};
