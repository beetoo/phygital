import { useState, useCallback } from 'react';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import { useDispatch } from 'react-redux';

import { setUpdateOrganizationByAdmin } from '../actions';
import { UserRoleType } from '../../../../../common/types';

type Props = { organizationId: string; userId: string; billingAvailable: boolean };

type ReturnValue = {
    billingAvailable: boolean;
    onBillingAvailableChange: ({ target: { value } }: SelectChangeEvent<UserRoleType>) => void;
    updateBillingAvailable: () => void;
    isSaveBillingAvailableButtonDisabled: boolean;
};

export const useBillingAvailableCell = ({
    organizationId,
    userId,
    billingAvailable: initialBillingAvailable,
}: Props): ReturnValue => {
    const dispatch = useDispatch();

    const [billingAvailable, setBillingAvailable] = useState<boolean>(initialBillingAvailable);

    const onBillingAvailableChange = useCallback(({ target: { value } }: SelectChangeEvent<UserRoleType>) => {
        setBillingAvailable(value === 'yes');
    }, []);

    const updateBillingAvailable = useCallback(() => {
        dispatch(setUpdateOrganizationByAdmin({ id: organizationId, userId, billingAvailable: billingAvailable }));
    }, [dispatch, organizationId, userId, billingAvailable]);

    const isSaveBillingAvailableButtonDisabled = billingAvailable === initialBillingAvailable;

    return {
        billingAvailable,
        onBillingAvailableChange,
        updateBillingAvailable,
        isSaveBillingAvailableButtonDisabled,
    };
};
