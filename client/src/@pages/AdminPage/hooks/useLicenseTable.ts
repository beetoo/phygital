import { useCallback, useMemo, useState } from 'react';

import { LicenceTypeEnum, LicenseType } from '../../../../../common/types';
import { httpService } from '../../../helpers/httpService';

type ReturnValue = {
    userLicenses: LicenseType[];
    isAddNewLicenseButtonDisabled: boolean;
    addInitialLicense: () => void;
    onSaveNewLicense: (license: LicenseType) => () => void;
    onDeleteLicense: (licenseId: string) => () => void;
};

export const useLicenseTable = (organizationId: string, licenses: LicenseType[]): ReturnValue => {
    const [userLicenses, setUserLicenses] = useState<LicenseType[]>(licenses);

    const isAddNewLicenseButtonDisabled = useMemo(() => userLicenses.some(({ id }) => id === 'new'), [userLicenses]);

    const addInitialLicense = useCallback(() => {
        setUserLicenses((prev) => [
            {
                id: 'new',
                screensNumber: 0,
                type: LicenceTypeEnum.basic_screens,
                capacity: 0,
                price: 0,
                startedAt: new Date(),
                endedAt: new Date(),
                description: '',
            },
            ...prev,
        ]);
    }, []);

    const onSaveNewLicense = useCallback(
        ({ id: licenseId, description, type, screensNumber, capacity, price, startedAt, endedAt }: LicenseType) =>
            async () => {
                let updatedLicense: LicenseType;
                if (licenseId === 'new') {
                    updatedLicense = (await httpService.post(`organization/${organizationId}/licenses`, {
                        description,
                        type,
                        screensNumber: Number(screensNumber),
                        capacity: Number(capacity),
                        price: Number(price),
                        startedAt: new Date(startedAt),
                        endedAt: new Date(endedAt),
                    })) as LicenseType;
                } else {
                    updatedLicense = (await httpService.patch(`license/${licenseId}`, {
                        description,
                        type,
                        screensNumber: Number(screensNumber),
                        capacity: Number(capacity),
                        price: Number(price),
                        startedAt: new Date(startedAt),
                        endedAt: new Date(endedAt),
                    })) as LicenseType;
                }

                setUserLicenses((prev) => prev.map((license) => (license.id === licenseId ? updatedLicense : license)));
            },
        [organizationId],
    );

    const onDeleteLicense = useCallback(
        (licenseId: string) => async () => {
            if (licenseId !== 'new') {
                await httpService.delete(`license/${licenseId}`);
            }
            setUserLicenses((prev) => prev.filter(({ id }) => id !== licenseId));
        },
        [],
    );

    return {
        userLicenses,
        isAddNewLicenseButtonDisabled,
        addInitialLicense,
        onSaveNewLicense,
        onDeleteLicense,
    };
};
