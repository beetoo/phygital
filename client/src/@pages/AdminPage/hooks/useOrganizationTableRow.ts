import { useCallback, useState, MouseEvent } from 'react';
import { useSelector } from 'react-redux';

import { selectAuthUserRole } from '../../../selectors';
import { USER_ROLE } from '../../../constants';

type ReturnValue = {
    isAdmin: boolean;
    isMainAdmin: (email: string) => boolean;
    isCollapseOpen: boolean;
    onCollapseChange: (event: MouseEvent) => void;
};

const ADMIN_LOGIN = (window as any)?.globalConfig?.ADMIN_LOGIN;

export const useOrganizationTableRow = (): ReturnValue => {
    const userRole = useSelector(selectAuthUserRole);
    const isAdmin = userRole === USER_ROLE.ADMIN;

    const isMainAdmin = useCallback((email: string) => email === (ADMIN_LOGIN || process.env.ADMIN_LOGIN), []);

    const [isCollapseOpen, setIsCollapseOpen] = useState(false);

    const onCollapseChange = useCallback(
        (event: MouseEvent) => {
            event.stopPropagation();
            setIsCollapseOpen(!isCollapseOpen);
        },
        [isCollapseOpen],
    );

    return { isAdmin, isMainAdmin, isCollapseOpen, onCollapseChange };
};
