import { useWindowDimensions } from '../../../hooks/useWindowDimensions';

type ReturnValue = {
    width: number;
    height: number;
};

export const useOrganizationsList = (): ReturnValue => {
    const { width, height } = useWindowDimensions();

    return {
        width,
        height,
    };
};
