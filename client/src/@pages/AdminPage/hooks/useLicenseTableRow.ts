import { ChangeEvent, useCallback, useMemo, useState } from 'react';
import { format, isBefore } from 'date-fns';

import { LicenceTypeEnum, LicenseType } from '../../../../../common/types';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';

type ReturnValue = {
    licenseDescription: string;
    licenseType: LicenceTypeEnum;
    licenseScreensNumber: number;
    licenseCapacity: number;
    licensePrice: number;
    startedAtTime: string;
    endedAtTime: string;
    onChangeDescription: (event: ChangeEvent<HTMLInputElement>) => void;
    onSelectLicenseType: (event: SelectChangeEvent<LicenceTypeEnum>) => void;
    onChangeScreensNumber: (event: ChangeEvent<HTMLInputElement>) => void;
    onChangeCapacityAmount: (event: ChangeEvent<HTMLInputElement>) => void;
    onChangeLicensePrice: (event: ChangeEvent<HTMLInputElement>) => void;
    onChangeStartedAtTime: (event: ChangeEvent<HTMLInputElement>) => void;
    onChangeEndedAtTime: (event: ChangeEvent<HTMLInputElement>) => void;
    isSaveButtonDisabled: boolean;
    isDeleteLicenseModalOpen: boolean;
    onOpenDeleteLicenseModal: () => void;
    onCloseDeleteLicenseModal: () => void;
};

export const useLicenseTableRow = ({
    description,
    screensNumber,
    capacity,
    price,
    type,
    startedAt,
    endedAt,
}: LicenseType): ReturnValue => {
    const initialStartedAtTime = format(new Date(startedAt), 'yyyy-MM-dd');
    const initialEndedAtTime = format(new Date(endedAt), 'yyyy-MM-dd');

    const [licenseDescription, setLicenseDescription] = useState<string>(description);
    const [licenseType, setLicenseType] = useState<LicenceTypeEnum>(type);
    const [licenseScreensNumber, setLicenseScreensNumber] = useState<number>(screensNumber);
    const [licenseCapacity, setLicenseCapacity] = useState<number>(capacity);
    const [licensePrice, setLicensePrice] = useState(price);
    const [startedAtTime, setStartedAtTime] = useState<string>(initialStartedAtTime);
    const [endedAtTime, setEndedAtTime] = useState<string>(initialEndedAtTime);

    const [isDeleteLicenseModalOpen, setIsDeleteLicenseModalOpen] = useState(false);

    const onChangeDescription = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setLicenseDescription(value);
    }, []);

    const onChangeScreensNumber = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        if (value.match(/^\d{0,4}$/)) {
            setLicenseScreensNumber(Number(value));
        }
    }, []);

    const onChangeCapacityAmount = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        if (value.match(/^\d{0,8}$/)) {
            setLicenseCapacity(Number(value));
        }
    }, []);

    const onChangeLicensePrice = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        if (value.match(/^\d{0,8}$/)) {
            setLicensePrice(Number(value));
        }
    }, []);

    const onSelectLicenseType = useCallback(({ target: { value } }: SelectChangeEvent<LicenceTypeEnum>) => {
        setLicenseType(value as LicenceTypeEnum);
    }, []);

    const onChangeStartedAtTime = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setStartedAtTime(value);
    }, []);

    const onChangeEndedAtTime = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setEndedAtTime(value);
    }, []);

    const isSaveButtonDisabled = useMemo(
        () =>
            // значения не поменялись
            (description.trim() === licenseDescription.trim() &&
                screensNumber === licenseScreensNumber &&
                capacity === licenseCapacity &&
                price === licensePrice &&
                type === licenseType &&
                startedAtTime === initialStartedAtTime &&
                endedAtTime === initialEndedAtTime) || // пустое описание
            licenseDescription.trim().length === 0 ||
            // все поля равные нулю
            (licenseScreensNumber === 0 && licenseCapacity === 0 && licensePrice === 0) ||
            // дата начала и конца совпадают
            isBefore(new Date(endedAtTime), new Date(startedAtTime)),
        [
            capacity,
            description,
            endedAtTime,
            initialEndedAtTime,
            initialStartedAtTime,
            licenseCapacity,
            licenseDescription,
            licensePrice,
            licenseScreensNumber,
            licenseType,
            price,
            screensNumber,
            startedAtTime,
            type,
        ],
    );

    const onOpenDeleteLicenseModal = useCallback(() => {
        setIsDeleteLicenseModalOpen(true);
    }, []);

    const onCloseDeleteLicenseModal = useCallback(() => {
        setIsDeleteLicenseModalOpen(false);
    }, []);

    return {
        licenseDescription,
        licenseType,
        licenseScreensNumber,
        licenseCapacity,
        licensePrice,
        startedAtTime,
        endedAtTime,
        onChangeDescription,
        onSelectLicenseType,
        onChangeScreensNumber,
        onChangeCapacityAmount,
        onChangeLicensePrice,
        onChangeStartedAtTime,
        onChangeEndedAtTime,
        isSaveButtonDisabled,
        isDeleteLicenseModalOpen,
        onOpenDeleteLicenseModal,
        onCloseDeleteLicenseModal,
    };
};
