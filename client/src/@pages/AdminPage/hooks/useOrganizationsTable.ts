import { useSelector } from 'react-redux';

import { selectAuthUserRole } from '../../../selectors';
import { USER_ROLE } from '../../../constants';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { ClientOrganizationType } from '../../../../../common/types';
import { useWindowDimensions } from '../../../hooks/useWindowDimensions';

type ReturnValue = {
    organizations: ClientOrganizationType[];
    isAdmin: boolean;
    height: number;
};
export const useOrganizationsTable = (): ReturnValue => {
    const { height } = useWindowDimensions();

    const organizations = [useSelector(selectCurrentOrganization)];

    const userRole = useSelector(selectAuthUserRole);
    const isAdmin = userRole === USER_ROLE.ADMIN;

    return {
        organizations,
        isAdmin,
        height,
    };
};
