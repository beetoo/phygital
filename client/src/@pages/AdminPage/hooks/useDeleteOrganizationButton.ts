import { useCallback, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { deleteOrganization } from '../../../resolvers/organizationsResolvers';

type ReturnValue = {
    isModalOpen: boolean;
    errorMessage: string;
    onOpenModal: () => void;
    onDeleteOrganization: () => void;
    onCloseModal: () => void;
};

type Props = {
    organizationId: string;
};

export const useDeleteOrganizationButton = ({ organizationId }: Props): ReturnValue => {
    const navigate = useNavigate();

    const [isModalOpen, setIsOpenModal] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const onOpenModal = useCallback(() => {
        setIsOpenModal(true);
    }, []);

    const onDeleteOrganization = useCallback(async () => {
        setErrorMessage('');

        try {
            await deleteOrganization(organizationId);
            setIsOpenModal(false);
            navigate('/organizations');
        } catch (err) {
            setErrorMessage((err.message?.errors?.reason || err.message?.message) ?? '');
        }
    }, [navigate, organizationId]);

    const onCloseModal = useCallback(() => {
        setIsOpenModal(false);
    }, []);

    return {
        errorMessage,
        isModalOpen,
        onOpenModal,
        onDeleteOrganization,
        onCloseModal,
    };
};
