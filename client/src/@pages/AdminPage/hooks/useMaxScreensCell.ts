import { useDispatch } from 'react-redux';
import { ChangeEvent, useCallback, useEffect, useState } from 'react';
import { setUpdateOrganizationByAdmin } from '../actions';

type Props = {
    organizationId: string;
    userId: string;
    maxScreens: number;
};

type ReturnValue = {
    maxScreensValue: string;
    onMaxScreensChange: (e: ChangeEvent<HTMLInputElement>) => void;
    updateMaxScreens: () => void;
    isUpdateMaxScreensButtonDisabled: boolean;
};

export const useMaxScreensCell = ({ organizationId, userId, maxScreens }: Props): ReturnValue => {
    const dispatch = useDispatch();

    const [maxScreensValue, setMaxScreensValue] = useState(maxScreens.toString());

    const onMaxScreensChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        if (value.match(/^\d{0,4}$/)) {
            setMaxScreensValue(value.length > 0 ? Number(value).toString() : value);
        }
    }, []);

    const updateMaxScreens = useCallback(() => {
        dispatch(setUpdateOrganizationByAdmin({ id: organizationId, userId, maxScreens: Number(maxScreensValue) }));
    }, [dispatch, maxScreensValue, organizationId, userId]);

    // при клике на любом месте возвращаем значение maxScreens обратно, если инпут был пустым
    useEffect(() => {
        let listener: () => void;
        if (maxScreensValue === '') {
            listener = () => setMaxScreensValue(maxScreens.toString());
            document.addEventListener('click', listener);
        }

        return () => {
            if (listener) {
                document.removeEventListener('click', listener);
            }
        };
    }, [maxScreens, maxScreensValue]);

    const isUpdateMaxScreensButtonDisabled = maxScreensValue === '' || Number(maxScreensValue) === maxScreens;

    return {
        maxScreensValue,
        onMaxScreensChange,
        updateMaxScreens,
        isUpdateMaxScreensButtonDisabled,
    };
};
