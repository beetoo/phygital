import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { setSignOutProcess } from '../../../actions';
import { selectAuthUserRole } from '../../../selectors';
import { USER_ROLE } from '../../../constants';

type ReturnValue = {
    handleLogout: () => void;
    isAdmin: boolean;
};

export const useAdminHeader = (): ReturnValue => {
    const dispatch = useDispatch();
    const userRole = useSelector(selectAuthUserRole);

    const handleLogout = useCallback(() => {
        localStorage.removeItem('lastPage');
        dispatch(setSignOutProcess());
    }, [dispatch]);

    const isAdmin = userRole === USER_ROLE.ADMIN;

    return {
        handleLogout,
        isAdmin,
    };
};
