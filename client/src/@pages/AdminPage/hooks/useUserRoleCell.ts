import { useState, useCallback } from 'react';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import { useDispatch } from 'react-redux';

import { setUpdateOrganizationByAdmin } from '../actions';
import { UserRoleType } from '../../../../../common/types';

type Props = { organizationId: string; userId: string; role: UserRoleType };

type ReturnValue = {
    userRoleValue: UserRoleType;
    onUserRoleChange: ({ target: { value } }: SelectChangeEvent<UserRoleType>) => void;
    updateUserRole: () => void;
    isSaveUserRoleButtonDisabled: boolean;
};

export const useUserRoleCell = ({ organizationId, userId, role }: Props): ReturnValue => {
    const dispatch = useDispatch();

    const [userRoleValue, setUserRoleValue] = useState<UserRoleType>(role);

    const onUserRoleChange = useCallback(({ target: { value } }: SelectChangeEvent<UserRoleType>) => {
        setUserRoleValue(value as UserRoleType);
    }, []);

    const updateUserRole = useCallback(() => {
        dispatch(setUpdateOrganizationByAdmin({ id: organizationId, userId, role: userRoleValue }));
    }, [dispatch, organizationId, userId, userRoleValue]);

    const isSaveUserRoleButtonDisabled = userRoleValue === role;

    return {
        userRoleValue,
        onUserRoleChange,
        updateUserRole,
        isSaveUserRoleButtonDisabled,
    };
};
