import { StoreType } from '../../../types';
import { AnalysisCameraType } from '../../../../../common/types';

// получение списка организаций

export const selectOrganizationsPage = ({
    pages: {
        admin: { organizationsPage },
    },
}: StoreType): number => organizationsPage;

export const selectOrganizationsSearchValue = ({
    pages: {
        admin: { organizationsSearchValue },
    },
}: StoreType): string => organizationsSearchValue;

export const selectOrganizationsCount = ({
    pages: {
        admin: { organizationsCount },
    },
}: StoreType): number => organizationsCount;

// cameras

export const selectCameras = ({
    pages: {
        admin: { cameras },
    },
}: StoreType): AnalysisCameraType[] => cameras;

export const selectAddCameraModalOpen = ({
    pages: {
        admin: { isAddCameraModalOpen },
    },
}: StoreType): boolean => isAddCameraModalOpen;
