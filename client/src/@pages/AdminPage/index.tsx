import React from 'react';

import OrganizationsTable from './components/OrganizationsTable';

const AdministrationPage: React.FC = () => <OrganizationsTable />;

export default AdministrationPage;
