import { of, Observable } from 'rxjs';
import { mergeMap, map, catchError, withLatestFrom } from 'rxjs/operators';
import { combineEpics, ofType, StateObservable } from 'redux-observable';

import {
    GET_ORGANIZATIONS,
    setGetOrganizationsSuccess,
    setGetOrganizationsFail,
    DELETE_ORGANIZATION_SUCCESS,
    DELETE_ORGANIZATION,
    setDeleteOrganizationSuccess,
    setDeleteOrganizationFail,
    UPDATE_ORGANIZATION_BY_ADMIN,
    setUpdateOrganizationByAdminSuccess,
    setUpdateOrganizationByAdminFail,
    UPDATE_ORGANIZATION_BY_ADMIN_SUCCESS,
} from '../actions';

import {
    fromDeleteOrganization,
    fromGetOrganizations,
    fromUpdateOrganization,
} from '../../../resolvers/organizationsResolvers';
import {
    DeleteOrganizationAction,
    DeleteOrganizationFailAction,
    DeleteOrganizationSuccessAction,
    GetOrganizationsAction,
    GetOrganizationsFailAction,
    GetOrganizationsSuccessAction,
    UpdateOrganizationByAdminAction,
    UpdateOrganizationByAdminFailAction,
    UpdateOrganizationByAdminSuccessAction,
} from '../types';
import { StoreType } from '../../../types';
import { GetOrganizationAction } from '../../ProfilePage/types';
import { setGetOrganization } from '../../ProfilePage/actions';

const getOrganizationsEpic = (
    action$: Observable<GetOrganizationsAction>,
): Observable<GetOrganizationsSuccessAction | GetOrganizationsFailAction> =>
    action$.pipe(
        ofType(GET_ORGANIZATIONS),
        mergeMap(({ payload: { skip, take, search } }) =>
            fromGetOrganizations({ skip, take, search }).pipe(
                map(setGetOrganizationsSuccess),
                catchError((error) => of(setGetOrganizationsFail(error))),
            ),
        ),
    );

const updateOrganizationByAdminEpic = (
    action$: Observable<UpdateOrganizationByAdminAction>,
): Observable<UpdateOrganizationByAdminSuccessAction | UpdateOrganizationByAdminFailAction> =>
    action$.pipe(
        ofType(UPDATE_ORGANIZATION_BY_ADMIN),
        mergeMap(({ payload: updateOrganizationDto }) =>
            fromUpdateOrganization(updateOrganizationDto).pipe(
                map(setUpdateOrganizationByAdminSuccess),
                catchError((error) => of(setUpdateOrganizationByAdminFail(error))),
            ),
        ),
    );

const deleteOrganizationsEpic = (
    action$: Observable<DeleteOrganizationAction>,
): Observable<DeleteOrganizationSuccessAction | DeleteOrganizationFailAction> =>
    action$.pipe(
        ofType(DELETE_ORGANIZATION),
        mergeMap(({ payload: { organizationId } }) =>
            fromDeleteOrganization(organizationId).pipe(
                map(setDeleteOrganizationSuccess),
                catchError((error) => of(setDeleteOrganizationFail(error))),
            ),
        ),
    );

const organizationsSuccessEpic = (
    action$: Observable<UpdateOrganizationByAdminSuccessAction | DeleteOrganizationSuccessAction>,
    state$: StateObservable<StoreType>,
): Observable<GetOrganizationAction> =>
    action$.pipe(
        ofType(UPDATE_ORGANIZATION_BY_ADMIN_SUCCESS, DELETE_ORGANIZATION_SUCCESS),
        withLatestFrom(state$),
        map(
            ([
                ,
                {
                    pages: {
                        organization: {
                            profile: {
                                organization: { id },
                            },
                        },
                    },
                },
            ]) => setGetOrganization(id),
        ),
    );

type EpicsActions =
    | GetOrganizationsAction
    | GetOrganizationsSuccessAction
    | GetOrganizationsFailAction
    | UpdateOrganizationByAdminAction
    | UpdateOrganizationByAdminSuccessAction
    | UpdateOrganizationByAdminFailAction
    | DeleteOrganizationAction
    | DeleteOrganizationSuccessAction
    | DeleteOrganizationFailAction
    | GetOrganizationAction;

export const organizationsEpics = combineEpics<EpicsActions, EpicsActions, StoreType>(
    getOrganizationsEpic,
    updateOrganizationByAdminEpic,
    deleteOrganizationsEpic,
    organizationsSuccessEpic,
);
