import { ScreenType } from '../../../../../common/types';

export const getArrayOfArrays = (arr: ScreenType[], splitNumber: number): ScreenType[][] => {
    if (arr.length <= splitNumber) {
        return [arr];
    }

    let temp = [] as ScreenType[];

    return arr.reduce((prev, curr, index, arr) => {
        temp.push(curr);
        if ((index + 1) % splitNumber === 0) {
            prev.push(temp);
            temp = [];
        }
        if (arr.length === index + 1) {
            prev.push(temp);
        }

        return prev;
    }, [] as ScreenType[][]);
};
