import React from 'react';
import { IconButton } from '@mui/material';
import { Save as SaveIcon } from '@mui/icons-material';

import { useUserRoleCell } from '../../hooks/useUserRoleCell';
import { MenuItem, Select } from '../../../../ui-kit';
import { UserRoleType } from '../../../../../../common/types';

const roleItems = ['admin', 'manager', 'user'];

type Props = { organizationId: string; userId: string; role: UserRoleType };

const UserRoleCell: React.FC<Props> = (props) => {
    const { userRoleValue, onUserRoleChange, updateUserRole, isSaveUserRoleButtonDisabled } = useUserRoleCell(props);

    return (
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            <Select
                size="small"
                displayEmpty
                value={userRoleValue}
                onChange={onUserRoleChange}
                MenuProps={{ disableScrollLock: true }}
            >
                {roleItems.map((role) => (
                    <MenuItem key={role} value={role}>
                        {role}
                    </MenuItem>
                ))}
            </Select>
            <IconButton color="primary" size="small" onClick={updateUserRole} disabled={isSaveUserRoleButtonDisabled}>
                <SaveIcon />
            </IconButton>
        </div>
    );
};

export default UserRoleCell;
