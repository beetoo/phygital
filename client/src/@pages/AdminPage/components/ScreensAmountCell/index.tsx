import React from 'react';

import pointGreenOn from '../../../../assets/pointGreenOn_12x12.svg';
import pointGreenOff from '../../../../assets/pointGreenOff_12x12.svg';
import pointGreenError from '../../../../assets/pointGreenError_12x12.svg';

import css from '../../style.m.css';

const ScreensAmountCell: React.FC = () => (
    <div>
        <p className={css.folderItemCard}>
            Screens Total: <span>{7}</span>
        </p>
        <div className={css.screensStatistics}>
            <div className={css.fieldStatisticsItem}>
                <img src={pointGreenOn} width="12px" height="12px" alt="" />
                <p className={css.screensNumberItem}>{0}</p>
            </div>

            <div className={css.fieldStatisticsItem}>
                <img src={pointGreenOff} width="12px" height="12px" alt="" />
                <p className={css.screensNumberItem}>{0}</p>
            </div>

            <div className={css.fieldStatisticsItem}>
                <img src={pointGreenError} width="12px" height="12px" alt="" />
                <p className={css.screensNumberItem}>{0}</p>
            </div>
        </div>
    </div>
);

export default ScreensAmountCell;
