import React from 'react';
import { IconButton, Input } from '@mui/material';
import { Save as SaveIcon } from '@mui/icons-material';

import { useMaxScreensCell } from '../../hooks/useMaxScreensCell';

type Props = { organizationId: string; userId: string; maxScreens: number };

const MaxScreensCell: React.FC<Props> = (props) => {
    const { maxScreensValue, onMaxScreensChange, updateMaxScreens, isUpdateMaxScreensButtonDisabled } =
        useMaxScreensCell(props);

    return (
        <>
            <Input
                sx={{ width: '45px' }}
                value={maxScreensValue}
                onChange={onMaxScreensChange}
                inputProps={{ maxLength: 125 }}
            />
            <IconButton
                color="primary"
                size="small"
                onClick={updateMaxScreens}
                disabled={isUpdateMaxScreensButtonDisabled}
            >
                <SaveIcon />
            </IconButton>
        </>
    );
};

export default MaxScreensCell;
