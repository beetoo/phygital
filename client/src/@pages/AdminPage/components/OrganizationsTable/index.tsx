import React, { memo } from 'react';
import { Paper, Table, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';

import OrganizationsTableRow from '../OrganizationsTableRow';
import { useOrganizationsTable } from '../../hooks/useOrganizationsTable';
import { isSLClientMode } from '../../../../constants/environments';

const OrganizationsTable: React.FC = () => {
    const { organizations, isAdmin, height } = useOrganizationsTable();

    return (
        <TableContainer
            component={Paper}
            sx={{
                marginLeft: 1,
                overflow: 'hidden',
                overflowY: 'auto',
                maxHeight: isSLClientMode ? 'auto' : height - 75 + 'px',
            }}
        >
            <Table size="small">
                <TableHead sx={{ paddingTop: 10 }}>
                    <TableRow>
                        <TableCell align="left">Title</TableCell>
                        <TableCell align="left">Email</TableCell>
                        {isAdmin && <TableCell align="left">Role</TableCell>}
                        {!isSLClientMode && <TableCell align="left">Billing available</TableCell>}
                        {isAdmin && <TableCell align="left">Actions</TableCell>}
                    </TableRow>
                </TableHead>

                {organizations.map((organization) => (
                    <OrganizationsTableRow key={organization.id} organization={organization} />
                ))}
            </Table>
        </TableContainer>
    );
};

export default memo(OrganizationsTable);
