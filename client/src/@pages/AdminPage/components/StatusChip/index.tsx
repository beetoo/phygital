import React from 'react';
import { clsx } from 'clsx';

import SendConfirmRegistrationEmailButton from '../SendConfirmRegistrationEmailButton';

import css from './style.m.css';

type Props = {
    userId: string;
    status: 'active' | 'pending';
};

const StatusChip: React.FC<Props> = ({ userId, status }) => (
    <div style={{ display: 'flex', alignItems: 'baseline', flexWrap: 'wrap' }}>
        <p
            className={clsx(css.chipCard, {
                [css.chipActive]: status === 'active',
                [css.chipPending]: status === 'pending',
            })}
        >
            {status}
        </p>
        {status === 'pending' && <SendConfirmRegistrationEmailButton userId={userId} />}
    </div>
);

export default StatusChip;
