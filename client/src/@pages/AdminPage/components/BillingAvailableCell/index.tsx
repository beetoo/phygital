import React from 'react';
import { IconButton } from '@mui/material';
import { Save as SaveIcon } from '@mui/icons-material';

import { MenuItem, Select } from '../../../../ui-kit';
import { useBillingAvailableCell } from '../../hooks/useBillingAvailableCell';

type Props = { organizationId: string; userId: string; billingAvailable: boolean };

const BillingAvailableCell: React.FC<Props> = (props) => {
    const { billingAvailable, onBillingAvailableChange, updateBillingAvailable, isSaveBillingAvailableButtonDisabled } =
        useBillingAvailableCell(props);

    return (
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            <Select
                size="small"
                displayEmpty
                value={billingAvailable ? 'yes' : 'no'}
                onChange={onBillingAvailableChange}
                MenuProps={{ disableScrollLock: true }}
            >
                {['yes', 'no'].map((value) => (
                    <MenuItem key={value} value={value}>
                        {value}
                    </MenuItem>
                ))}
            </Select>
            <IconButton
                color="primary"
                size="small"
                onClick={updateBillingAvailable}
                disabled={isSaveBillingAvailableButtonDisabled}
            >
                <SaveIcon />
            </IconButton>
        </div>
    );
};

export default BillingAvailableCell;
