import React from 'react';
import { IconButton, Input, TableCell, TableRow, TextField, TableBody, Table } from '@mui/material';
import { Delete as DeleteIcon, Save as SaveIcon } from '@mui/icons-material';

import DeleteLicenseModal from '../DeleteLicenseModal';
import { MenuItem, Select } from '../../../../ui-kit';
import { useLicenseTableRow } from '../../hooks/useLicenseTableRow';
import { LicenceTypeEnum, LicenseType } from '../../../../../../common/types';

type Props = {
    license: LicenseType;
    onSaveNewLicense: (license: LicenseType) => () => void;
    onDeleteLicense: (licenseId: string) => () => void;
};

const LicenseTableRow: React.FC<Props> = ({ license, onSaveNewLicense, onDeleteLicense }) => {
    const {
        licenseDescription,
        licenseType,
        licenseScreensNumber,
        licenseCapacity,
        licensePrice,
        startedAtTime,
        endedAtTime,
        onChangeDescription,
        onSelectLicenseType,
        onChangeScreensNumber,
        onChangeCapacityAmount,
        onChangeLicensePrice,
        onChangeStartedAtTime,
        onChangeEndedAtTime,
        isSaveButtonDisabled,
        isDeleteLicenseModalOpen,
        onOpenDeleteLicenseModal,
        onCloseDeleteLicenseModal,
    } = useLicenseTableRow(license);

    return (
        <>
            <Table size="small">
                <TableBody>
                    <TableRow>
                        <TableCell sx={{ borderBottom: 'none' }} align="left">
                            Description
                            <Input
                                sx={{ width: '50%', marginLeft: '10px' }}
                                value={licenseDescription}
                                onChange={onChangeDescription}
                                inputProps={{ maxLength: 125 }}
                            />
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
            <Table size="small">
                <TableBody>
                    <TableRow>
                        <TableCell align="left">
                            <div style={{ display: 'flex', alignItems: 'center' }}>
                                Type
                                <Select
                                    sx={{ marginLeft: '10px' }}
                                    size="small"
                                    value={licenseType}
                                    onChange={onSelectLicenseType}
                                >
                                    <MenuItem value={LicenceTypeEnum.basic_screens}>
                                        {LicenceTypeEnum.basic_screens}
                                    </MenuItem>
                                    <MenuItem value={LicenceTypeEnum.basic_storage}>
                                        {LicenceTypeEnum.basic_storage}
                                    </MenuItem>
                                </Select>
                            </div>
                        </TableCell>

                        <TableCell align="left">
                            Screens
                            <Input
                                sx={{ width: '45px', marginLeft: '10px' }}
                                value={licenseScreensNumber}
                                onChange={onChangeScreensNumber}
                                inputProps={{ maxLength: 125 }}
                            />
                        </TableCell>

                        <TableCell align="left">
                            Capacity
                            <Input
                                sx={{ width: '110px', marginLeft: '10px' }}
                                value={licenseCapacity}
                                onChange={onChangeCapacityAmount}
                                renderSuffix={() => 'mb'}
                                inputProps={{ maxLength: 125 }}
                            />
                        </TableCell>

                        <TableCell align="left">
                            Price
                            <Input
                                sx={{ width: '110px', marginLeft: '10px' }}
                                value={licensePrice}
                                onChange={onChangeLicensePrice}
                                renderSuffix={() => 'rub'}
                                inputProps={{ maxLength: 125 }}
                            />
                        </TableCell>

                        <TableCell align="left" sx={{ alignContent: 'center' }}>
                            <div style={{ display: 'flex', alignItems: 'center' }}>
                                Start date
                                <TextField
                                    sx={{ marginLeft: '10px' }}
                                    type="date"
                                    size="small"
                                    value={startedAtTime}
                                    onChange={onChangeStartedAtTime}
                                />
                            </div>
                        </TableCell>
                        <TableCell align="left">
                            <div style={{ display: 'flex', alignItems: 'center' }}>
                                End date
                                <TextField
                                    sx={{ marginLeft: '10px' }}
                                    type="date"
                                    size="small"
                                    value={endedAtTime}
                                    onChange={onChangeEndedAtTime}
                                />
                            </div>
                        </TableCell>

                        <TableCell align="left">
                            <IconButton
                                onClick={onSaveNewLicense({
                                    id: license.id,
                                    description: licenseDescription,
                                    screensNumber: licenseScreensNumber,
                                    capacity: licenseCapacity,
                                    price: licensePrice,
                                    type: licenseType,
                                    startedAt: new Date(startedAtTime),
                                    endedAt: new Date(endedAtTime),
                                })}
                                disabled={isSaveButtonDisabled}
                            >
                                <SaveIcon />
                            </IconButton>
                            <IconButton
                                onClick={license.id === 'new' ? onDeleteLicense(license.id) : onOpenDeleteLicenseModal}
                            >
                                <DeleteIcon />
                            </IconButton>
                            {isDeleteLicenseModalOpen && (
                                <DeleteLicenseModal
                                    onDeleteLicense={onDeleteLicense(license.id)}
                                    onCloseModal={onCloseDeleteLicenseModal}
                                />
                            )}
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </>
    );
};

export default LicenseTableRow;
