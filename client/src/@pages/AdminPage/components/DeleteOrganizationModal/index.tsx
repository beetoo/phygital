import React from 'react';
import { Dialog, DialogActions, DialogTitle, DialogContent, DialogContentText, Button } from '@mui/material';

type Props = {
    onDeleteOrganization: () => void;
    onCloseModal: () => void;
    errorMessage: string;
};

const DeleteOrganizationModal: React.FC<Props> = ({ onDeleteOrganization, onCloseModal, errorMessage }) => (
    <Dialog
        open={true}
        onClose={onCloseModal}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
    >
        <DialogTitle id="alert-dialog-title">Are you sure you want to delete the organization?</DialogTitle>
        <DialogContent>
            <DialogContentText id="alert-dialog-description">
                All screens, playlists, and the organization&apos;s content will be deleted.
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <div style={{ textAlign: 'right' }}>
                <Button onClick={onCloseModal}>Cancel</Button>
                <Button onClick={onDeleteOrganization}>Delete</Button>
                {errorMessage && <div style={{ color: 'red' }}>{errorMessage}</div>}
            </div>
        </DialogActions>
    </Dialog>
);

export default DeleteOrganizationModal;
