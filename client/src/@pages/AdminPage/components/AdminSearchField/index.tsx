import React, { ChangeEvent, memo } from 'react';
import { IconButton, InputBase, InputBaseProps, Paper } from '@mui/material';
import { Cancel as CancelIcon, Search as SearchIcon } from '@mui/icons-material';

interface Props extends InputBaseProps {
    onChange: (e: ChangeEvent) => void;
    clearSearchText?: () => void;
    value: string;
}

const AdminSearchField: React.FC<Props> = ({ clearSearchText, value, ...props }) => (
    <Paper
        component="form"
        sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 400, margin: '20px 10px 10px' }}
    >
        <SearchIcon sx={{ p: '10px' }} />
        <InputBase
            sx={{ ml: 1, flex: 1 }}
            placeholder="Search"
            inputProps={{ 'aria-label': 'search', maxLength: 125 }}
            value={value}
            {...props}
        />
        <IconButton sx={{ p: '10px' }} aria-label="cancel" onClick={clearSearchText} disabled={!value}>
            <CancelIcon fontSize="small" />
        </IconButton>
    </Paper>
);

export default memo(AdminSearchField);
