import React from 'react';
import { Dialog, DialogActions, DialogTitle, Button } from '@mui/material';

type Props = {
    onDeleteLicense: () => void;
    onCloseModal: () => void;
};

const DeleteLicenseModal: React.FC<Props> = ({ onDeleteLicense, onCloseModal }) => (
    <Dialog
        open={true}
        onClose={onCloseModal}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
    >
        <DialogTitle id="alert-dialog-title">Are you sure you want to delete the license?</DialogTitle>
        <DialogActions>
            <Button onClick={onCloseModal}>Cancel</Button>
            <Button onClick={onDeleteLicense}>Delete</Button>
        </DialogActions>
    </Dialog>
);

export default DeleteLicenseModal;
