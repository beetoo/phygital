import React from 'react';
import { LoadingButton } from '@mui/lab';
import { Delete as DeleteIcon } from '@mui/icons-material';

import DeleteOrganizationModal from '../DeleteOrganizationModal';

import { useDeleteOrganizationButton } from '../../hooks/useDeleteOrganizationButton';

const DeleteOrganizationButton: React.FC<{ organizationId: string }> = ({ organizationId }) => {
    const { isModalOpen, onOpenModal, onDeleteOrganization, onCloseModal, errorMessage } = useDeleteOrganizationButton({
        organizationId,
    });

    return (
        <>
            {isModalOpen && <DeleteOrganizationModal {...{ onDeleteOrganization, onCloseModal, errorMessage }} />}
            <LoadingButton
                sx={{ marginTop: '10px' }}
                variant="outlined"
                size="small"
                endIcon={<DeleteIcon />}
                loading={false}
                loadingIndicator="Deleting..."
                onClick={onOpenModal}
                disabled={false}
            >
                delete
            </LoadingButton>
        </>
    );
};

export default DeleteOrganizationButton;
