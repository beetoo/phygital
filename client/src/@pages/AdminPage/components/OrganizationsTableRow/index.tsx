import React from 'react';
import { TableBody, TableRow, TableCell, IconButton, Collapse } from '@mui/material';
import { KeyboardArrowUp, KeyboardArrowDown } from '@mui/icons-material';

import UserRoleCell from '../UserRoleCell';
import DeleteOrganizationButton from '../DeleteOrganizationButton';
import StatusChip from '../StatusChip';
import BillingAvailableCell from '../BillingAvailableCell';
import LicenseTable from '../LicenseTable';
import { USER_ROLE } from '../../../../constants';
import { useOrganizationTableRow } from '../../hooks/useOrganizationTableRow';
import { isSLClientMode } from '../../../../constants/environments';
import { ClientOrganizationType, UserRoleType } from '../../../../../../common/types';

type Props = {
    organization: ClientOrganizationType;
};

const OrganizationsTableRow: React.FC<Props> = ({
    organization: { id, userId, title, email, status, role, billingAvailable, licenses },
}) => {
    const { isAdmin, isMainAdmin, isCollapseOpen, onCollapseChange } = useOrganizationTableRow();

    return (
        <TableBody
            sx={{
                borderTop: '12px solid #F5F5FE',
                borderBottom: '12px solid #F5F5FE',
                borderRight: '8px solid #F5F5FE',
                borderLeft: '8px solid #F5F5FE',
            }}
        >
            <TableRow
                hover={!isMainAdmin(email)}
                sx={{
                    fontSize: '14px',
                    backgroundColor: isMainAdmin(email)
                        ? 'lavender'
                        : role === USER_ROLE.ADMIN
                          ? 'aliceblue'
                          : 'inherit',
                }}
            >
                <TableCell
                    align="left"
                    sx={{
                        cursor: isAdmin ? 'pointer' : 'auto',
                    }}
                >
                    <StatusChip userId={userId} status={status} />

                    {!isSLClientMode && (
                        <IconButton sx={{ marginLeft: -1, marginRight: 1 }} size="small" onClick={onCollapseChange}>
                            {isCollapseOpen ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
                        </IconButton>
                    )}

                    <p style={{ display: 'inline-block', marginTop: isSLClientMode ? '10px' : 'auto' }}>{title}</p>
                </TableCell>
                <TableCell
                    align="left"
                    sx={{
                        cursor: isAdmin ? 'pointer' : 'auto',
                        textOverflow: 'ellipsis',
                    }}
                >
                    {email.split('@').map((text, index) => (
                        <div style={{ display: 'inline-block' }} key={index}>
                            {index === 1 ? '@' + text : text}
                        </div>
                    ))}
                </TableCell>

                {isAdmin && (
                    <TableCell align="left">
                        {isMainAdmin(email) ? (
                            role
                        ) : (
                            <UserRoleCell organizationId={id} userId={userId} role={role as UserRoleType} />
                        )}
                    </TableCell>
                )}

                {isAdmin && !isSLClientMode && (
                    <TableCell align="left">
                        <BillingAvailableCell organizationId={id} userId={userId} billingAvailable={billingAvailable} />
                    </TableCell>
                )}
                {isAdmin && (
                    <TableCell align="left">
                        {role !== USER_ROLE.ADMIN && <DeleteOrganizationButton organizationId={id} />}
                    </TableCell>
                )}
            </TableRow>

            {!isSLClientMode && (
                <TableRow>
                    <TableCell sx={{ padding: 0 }} colSpan={8}>
                        <Collapse in={isCollapseOpen} timeout="auto" unmountOnExit={true}>
                            <LicenseTable organizationId={id} licenses={licenses} />
                        </Collapse>
                    </TableCell>
                </TableRow>
            )}
        </TableBody>
    );
};

export default OrganizationsTableRow;
