import React from 'react';
import { Input, IconButton } from '@mui/material';
import { Save as SaveIcon } from '@mui/icons-material';

import { useMaxStorageSizeCell } from '../../hooks/useMaxStorageSizeCell';

type Props = { organizationId: string; userId: string; maxStorageSize: number };

const MaxStorageSizeCell: React.FC<Props> = (props) => {
    const { maxStorageSizeValue, onMaxStorageSizeChange, updateMaxStorageSize, isUpdateMaxStorageSizeButtonDisabled } =
        useMaxStorageSizeCell(props);

    return (
        <>
            <Input
                sx={{ width: '90px' }}
                value={maxStorageSizeValue}
                onChange={onMaxStorageSizeChange}
                renderSuffix={() => 'mb'}
                inputProps={{ maxLength: 125 }}
            />
            <IconButton
                color="primary"
                size="small"
                onClick={updateMaxStorageSize}
                disabled={isUpdateMaxStorageSizeButtonDisabled}
            >
                <SaveIcon />
            </IconButton>
        </>
    );
};

export default MaxStorageSizeCell;
