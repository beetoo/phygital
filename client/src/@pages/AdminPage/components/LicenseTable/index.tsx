import React from 'react';
import { Table, TableBody, TableRow, TableCell, IconButton } from '@mui/material';
import { Add as AddIcon } from '@mui/icons-material';

import LicenseTableRow from '../LicenseTableRow';
import { useLicenseTable } from '../../hooks/useLicenseTable';
import { LicenseType } from '../../../../../../common/types';

type Props = {
    organizationId: string;
    licenses: LicenseType[];
};

const LicenseTable: React.FC<Props> = ({ organizationId, licenses }) => {
    const { userLicenses, isAddNewLicenseButtonDisabled, addInitialLicense, onSaveNewLicense, onDeleteLicense } =
        useLicenseTable(organizationId, licenses);

    return (
        <>
            <Table size="small">
                <TableBody>
                    <TableRow>
                        <TableCell>
                            <IconButton onClick={addInitialLicense} disabled={isAddNewLicenseButtonDisabled}>
                                <AddIcon />
                            </IconButton>
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
            {userLicenses.map((license) => (
                <LicenseTableRow
                    key={license.id}
                    license={license}
                    onDeleteLicense={onDeleteLicense}
                    onSaveNewLicense={onSaveNewLicense}
                />
            ))}
        </>
    );
};

export default LicenseTable;
