import React, { SyntheticEvent } from 'react';
import { AppBar, Toolbar, Typography, Button, Box, styled, Tabs, Tab } from '@mui/material';

import { useAdminHeader } from '../../hooks/useAdminHeader';

const Offset = styled('div')(({ theme }) => theme.mixins.toolbar);

type Props = {
    tabIndex: number;
    onChangeTab: (e: SyntheticEvent, value: number) => void;
};

const AdminHeader: React.FC<Props> = ({ tabIndex, onChangeTab }) => {
    const { handleLogout, isAdmin } = useAdminHeader();

    return (
        <Box>
            <AppBar color="default" position="absolute">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        {`Phygital ${isAdmin ? 'Admin' : 'Manager'}`}
                    </Typography>
                    <Tabs
                        sx={{ flexGrow: 1 }}
                        value={tabIndex}
                        onChange={onChangeTab}
                        textColor="primary"
                        indicatorColor="primary"
                    >
                        <Tab label="Organizations" tabIndex={0} />
                        <Tab label="Screen Logs" tabIndex={1} />
                        {/*<Tab label="Cameras" tabIndex={2} />*/}
                    </Tabs>
                    <Button color="inherit" onClick={handleLogout}>
                        Exit
                    </Button>
                </Toolbar>
            </AppBar>
            <Offset />
        </Box>
    );
};

export default AdminHeader;
