import React, { useCallback, useState, MouseEvent } from 'react';
import { LoadingButton } from '@mui/lab';
import { Send as SendIcon } from '@mui/icons-material';

import { sendConfirmRegistrationEmail } from '../../../../resolvers/authResolvers';

const SendConfirmRegistrationEmailButton: React.FC<{ userId: string }> = ({ userId }) => {
    const [isEmailSending, setIsEmailSending] = useState(false);

    const resendConfirmRegistrationEmail = useCallback(
        (event: MouseEvent) => {
            event.stopPropagation();
            setIsEmailSending(true);
            sendConfirmRegistrationEmail(userId).finally(() => {
                setIsEmailSending(false);
            });
        },
        [userId],
    );

    return (
        <LoadingButton
            variant="outlined"
            size="small"
            sx={{ fontSize: '10px', height: '18px' }}
            endIcon={<SendIcon />}
            loading={isEmailSending}
            loadingIndicator="Sending..."
            onClick={resendConfirmRegistrationEmail}
        >
            email
        </LoadingButton>
    );
};

export default SendConfirmRegistrationEmailButton;
