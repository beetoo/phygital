import { ErrorResponse } from '../../../types';
import { AnalysisCameraType, TakeSkipAndSearchType } from '../../../../../common/types';
import { UpdateUserAndOrganizationDto } from '../../../../../server/src/models/organization/dto/update-user-and-organization.dto';
import { GetOrganizationsResponseType } from '../types';

export const SET_ORGANIZATIONS_PAGE = 'SET_ORGANIZATIONS_PAGE';
export const SET_ORGANIZATIONS_SEARCH_VALUE = 'SET_ORGANIZATIONS_SEARCH_VALUE';

export const GET_ORGANIZATIONS = 'GET_ORGANIZATIONS';
export const GET_ORGANIZATIONS_SUCCESS = 'GET_ORGANIZATIONS_SUCCESS';
export const GET_ORGANIZATIONS_FAIL = 'GET_ORGANIZATIONS_FAIL';

export const UPDATE_ORGANIZATION_BY_ADMIN = 'UPDATE_ORGANIZATION_BY_ADMIN';
export const UPDATE_ORGANIZATION_BY_ADMIN_SUCCESS = 'UPDATE_ORGANIZATION_BY_ADMIN_SUCCESS';
export const UPDATE_ORGANIZATION_BY_ADMIN_FAIL = 'UPDATE_ORGANIZATION_BY_ADMIN_FAIL';

export const DELETE_ORGANIZATION = 'DELETE_ORGANIZATION';
export const DELETE_ORGANIZATION_SUCCESS = 'DELETE_ORGANIZATION_SUCCESS';
export const DELETE_ORGANIZATION_FAIL = 'DELETE_ORGANIZATION_FAIL';

export const SET_ACTIVE_ORGANIZATION_LOCATION_ID = 'SET_ACTIVE_ORGANIZATION_LOCATION_ID';
export const SET_ACTIVE_ORGANIZATION_SCREEN_ID = 'SET_ACTIVE_ORGANIZATION_SCREEN_ID';

export const GET_CAMERAS = 'GET_CAMERAS';
export const GET_CAMERAS_SUCCESS = 'GET_CAMERAS_SUCCESS';
export const GET_CAMERAS_FAIL = 'GET_CAMERAS_FAIL';

export const OPEN_ADD_CAMERA_MODAL = 'OPEN_ADD_CAMERA_MODAL';
export const CLOSE_ADD_CAMERA_MODAL = 'CLOSE_ADD_CAMERA_MODAL';

export const ADD_CAMERA = 'ADD_CAMERA';
export const ADD_CAMERA_SUCCESS = 'ADD_CAMERA_SUCCESS';
export const ADD_CAMERA_FAIL = 'ADD_CAMERA_FAIL';

export const DELETE_CAMERA = 'DELETE_CAMERA';
export const DELETE_CAMERA_SUCCESS = 'DELETE_CAMERA_SUCCESS';
export const DELETE_CAMERA_FAIL = 'DELETE_CAMERA_FAIL';

export const CLEAR_ADMIN_PAGE_STATE = 'CLEAR_ADMIN_PAGE_STATE';

// organizations

export const setOrganizationsPage = (page: number) => ({ type: SET_ORGANIZATIONS_PAGE, payload: { page } }) as const;
export const setOrganizationsSearchValue = (searchValue: string) =>
    ({ type: SET_ORGANIZATIONS_SEARCH_VALUE, payload: { searchValue } }) as const;

export const setGetOrganizations = ({ take, skip, search }: TakeSkipAndSearchType) =>
    ({ type: GET_ORGANIZATIONS, payload: { take, skip, search } }) as const;
export const setGetOrganizationsSuccess = ({ organizations, count }: GetOrganizationsResponseType) =>
    ({
        type: GET_ORGANIZATIONS_SUCCESS,
        payload: { organizations, organizationsCount: count },
    }) as const;
export const setGetOrganizationsFail = (errors: ErrorResponse) =>
    ({
        type: GET_ORGANIZATIONS_FAIL,
        payload: { errors },
    }) as const;

export const setUpdateOrganizationByAdmin = (updateOrganizationDto: UpdateUserAndOrganizationDto) =>
    ({ type: UPDATE_ORGANIZATION_BY_ADMIN, payload: updateOrganizationDto }) as const;
export const setUpdateOrganizationByAdminSuccess = () =>
    ({
        type: UPDATE_ORGANIZATION_BY_ADMIN_SUCCESS,
    }) as const;
export const setUpdateOrganizationByAdminFail = (errors: ErrorResponse) =>
    ({
        type: UPDATE_ORGANIZATION_BY_ADMIN_FAIL,
        payload: { errors },
    }) as const;

export const setDeleteOrganization = (organizationId: string) =>
    ({ type: DELETE_ORGANIZATION, payload: { organizationId } }) as const;
export const setDeleteOrganizationSuccess = () => ({ type: DELETE_ORGANIZATION_SUCCESS }) as const;
export const setDeleteOrganizationFail = (errors: ErrorResponse) =>
    ({ type: DELETE_ORGANIZATION_FAIL, payload: { errors } }) as const;

export const setActiveOrganizationLocationId = (locationId = '') =>
    ({ type: SET_ACTIVE_ORGANIZATION_LOCATION_ID, payload: { locationId } }) as const;

export const setActiveOrganizationScreenId = (screenId = '') =>
    ({ type: SET_ACTIVE_ORGANIZATION_SCREEN_ID, payload: { screenId } }) as const;

// cameras

export const setGetCameras = () => ({ type: GET_CAMERAS }) as const;
export const setGetCamerasSuccess = (cameras: AnalysisCameraType[]) =>
    ({
        type: GET_CAMERAS_SUCCESS,
        payload: { cameras },
    }) as const;
export const setGetCamerasFail = (errors: ErrorResponse) => ({ type: GET_CAMERAS_FAIL, payload: { errors } }) as const;

export const setOpenAddCameraModal = () => ({ type: OPEN_ADD_CAMERA_MODAL }) as const;
export const setCloseAddCameraModal = () => ({ type: CLOSE_ADD_CAMERA_MODAL }) as const;

export const setAddCamera = (createAnalysisCameraDto: unknown[]) =>
    ({ type: ADD_CAMERA, payload: { ...createAnalysisCameraDto } }) as const;
export const setAddCameraSuccess = () => ({ type: ADD_CAMERA_SUCCESS }) as const;
export const setAddCameraFail = (errors: ErrorResponse) => ({ type: ADD_CAMERA_FAIL, payload: { errors } }) as const;

export const setDeleteCamera = (cameraId: string) => ({ type: DELETE_CAMERA, payload: { cameraId } }) as const;
export const setDeleteCameraSuccess = () => ({ type: DELETE_CAMERA_SUCCESS }) as const;
export const setDeleteCameraFail = (errors: ErrorResponse) =>
    ({ type: DELETE_CAMERA_FAIL, payload: { errors } }) as const;

export const setClearAdminPageState = () => ({ type: CLEAR_ADMIN_PAGE_STATE }) as const;
