import {
    setGetOrganizations,
    setGetOrganizationsSuccess,
    setGetOrganizationsFail,
    setDeleteOrganization,
    setDeleteOrganizationSuccess,
    setDeleteOrganizationFail,
    setClearAdminPageState,
    setUpdateOrganizationByAdmin,
    setUpdateOrganizationByAdminSuccess,
    setUpdateOrganizationByAdminFail,
    setGetCameras,
    setGetCamerasSuccess,
    setGetCamerasFail,
    setAddCamera,
    setAddCameraSuccess,
    setAddCameraFail,
    setDeleteCamera,
    setDeleteCameraSuccess,
    setDeleteCameraFail,
    setOpenAddCameraModal,
    setCloseAddCameraModal,
    setOrganizationsPage,
    setOrganizationsSearchValue,
    setActiveOrganizationLocationId,
    setActiveOrganizationScreenId,
} from '../actions';
import { ErrorResponse } from '../../../types';
import { AdminTableOrganization } from '../../MainPage/types';
import { AnalysisCameraType, ScreenRequestLoggerType } from '../../../../../common/types';

export interface GetOrganizationsResponseType {
    organizations: AdminTableOrganization[];
    count: number;
}

interface ScreenRequestLoggerClientType extends ScreenRequestLoggerType {
    screen: string;
}

export type ScreenLogsClientType = {
    organization: string;
    logs: ScreenRequestLoggerClientType[];
};

export type AdminPageState = {
    organizationsPage: number;
    organizationsSearchValue: string;
    gettingOrganizations: boolean;
    organizations: AdminTableOrganization[];
    organizationsCount: number;
    updatingOrganizations: boolean;
    deletingOrganizations: boolean;
    organizationErrors: ErrorResponse | null;
    activeLocationId: string;
    activeScreenId: string;

    gettingCameras: boolean;
    cameras: AnalysisCameraType[];
    isAddCameraModalOpen: boolean;
    addingCameras: boolean;
    deletingCameras: boolean;
    camerasErrors: ErrorResponse | null;
};

export type SetOrganizationsListAction = ReturnType<typeof setOrganizationsPage>;
export type SetOrganizationsSearchValueAction = ReturnType<typeof setOrganizationsSearchValue>;

export type GetOrganizationsAction = ReturnType<typeof setGetOrganizations>;
export type GetOrganizationsSuccessAction = ReturnType<typeof setGetOrganizationsSuccess>;
export type GetOrganizationsFailAction = ReturnType<typeof setGetOrganizationsFail>;

export type SetActiveOrganizationLocationIdAction = ReturnType<typeof setActiveOrganizationLocationId>;
export type SetActiveOrganizationScreenIdAction = ReturnType<typeof setActiveOrganizationScreenId>;

export type UpdateOrganizationByAdminAction = ReturnType<typeof setUpdateOrganizationByAdmin>;
export type UpdateOrganizationByAdminSuccessAction = ReturnType<typeof setUpdateOrganizationByAdminSuccess>;
export type UpdateOrganizationByAdminFailAction = ReturnType<typeof setUpdateOrganizationByAdminFail>;

export type DeleteOrganizationAction = ReturnType<typeof setDeleteOrganization>;
export type DeleteOrganizationSuccessAction = ReturnType<typeof setDeleteOrganizationSuccess>;
export type DeleteOrganizationFailAction = ReturnType<typeof setDeleteOrganizationFail>;

export type GetCamerasAction = ReturnType<typeof setGetCameras>;
export type GetCamerasSuccessAction = ReturnType<typeof setGetCamerasSuccess>;
export type GetCamerasFailAction = ReturnType<typeof setGetCamerasFail>;

export type OpenAddCameraModalAction = ReturnType<typeof setOpenAddCameraModal>;
export type CloseAddCameraModalAction = ReturnType<typeof setCloseAddCameraModal>;

export type AddCameraAction = ReturnType<typeof setAddCamera>;
export type AddCameraSuccessAction = ReturnType<typeof setAddCameraSuccess>;
export type AddCameraFailAction = ReturnType<typeof setAddCameraFail>;

export type DeleteCameraAction = ReturnType<typeof setDeleteCamera>;
export type DeleteCameraSuccessAction = ReturnType<typeof setDeleteCameraSuccess>;
export type DeleteCameraFailAction = ReturnType<typeof setDeleteCameraFail>;

export type ClearAdminPageStateAction = ReturnType<typeof setClearAdminPageState>;

export type AdminPageAction =
    | SetOrganizationsListAction
    | SetOrganizationsSearchValueAction
    | GetOrganizationsAction
    | GetOrganizationsSuccessAction
    | GetOrganizationsFailAction
    | UpdateOrganizationByAdminAction
    | UpdateOrganizationByAdminSuccessAction
    | UpdateOrganizationByAdminFailAction
    | DeleteOrganizationAction
    | DeleteOrganizationSuccessAction
    | DeleteOrganizationFailAction
    | SetActiveOrganizationLocationIdAction
    | SetActiveOrganizationScreenIdAction
    | GetCamerasAction
    | GetCamerasSuccessAction
    | GetCamerasFailAction
    | OpenAddCameraModalAction
    | CloseAddCameraModalAction
    | AddCameraAction
    | AddCameraSuccessAction
    | AddCameraFailAction
    | DeleteCameraAction
    | DeleteCameraSuccessAction
    | DeleteCameraFailAction
    | ClearAdminPageStateAction;
