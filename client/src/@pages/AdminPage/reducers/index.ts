import {
    ADD_CAMERA,
    ADD_CAMERA_FAIL,
    ADD_CAMERA_SUCCESS,
    CLEAR_ADMIN_PAGE_STATE,
    CLOSE_ADD_CAMERA_MODAL,
    DELETE_CAMERA,
    DELETE_CAMERA_FAIL,
    DELETE_CAMERA_SUCCESS,
    DELETE_ORGANIZATION,
    DELETE_ORGANIZATION_FAIL,
    DELETE_ORGANIZATION_SUCCESS,
    GET_CAMERAS,
    GET_CAMERAS_FAIL,
    GET_CAMERAS_SUCCESS,
    GET_ORGANIZATIONS,
    GET_ORGANIZATIONS_FAIL,
    GET_ORGANIZATIONS_SUCCESS,
    OPEN_ADD_CAMERA_MODAL,
    SET_ACTIVE_ORGANIZATION_LOCATION_ID,
    SET_ACTIVE_ORGANIZATION_SCREEN_ID,
    SET_ORGANIZATIONS_PAGE,
    SET_ORGANIZATIONS_SEARCH_VALUE,
    UPDATE_ORGANIZATION_BY_ADMIN,
    UPDATE_ORGANIZATION_BY_ADMIN_FAIL,
    UPDATE_ORGANIZATION_BY_ADMIN_SUCCESS,
} from '../actions';
import { AdminPageAction, AdminPageState } from '../types';

export const adminPageInitialState: AdminPageState = {
    organizationsPage: 0,
    organizationsSearchValue: '',
    gettingOrganizations: false,
    organizations: [],
    organizationsCount: 0,
    updatingOrganizations: false,
    deletingOrganizations: false,
    organizationErrors: null,
    activeLocationId: '',
    activeScreenId: '',

    gettingCameras: false,
    cameras: [],
    isAddCameraModalOpen: false,
    addingCameras: false,
    deletingCameras: false,
    camerasErrors: null,
};

export default function adminPageReducer(state = adminPageInitialState, action: AdminPageAction): AdminPageState {
    switch (action.type) {
        case SET_ORGANIZATIONS_PAGE:
            return {
                ...state,
                organizationsPage: action.payload.page,
            };

        case SET_ORGANIZATIONS_SEARCH_VALUE:
            return {
                ...state,
                organizationsSearchValue: action.payload.searchValue,
            };

        case GET_ORGANIZATIONS:
            return {
                ...state,
                gettingOrganizations: true,
                organizationErrors: null,
            };
        case GET_ORGANIZATIONS_SUCCESS:
            return {
                ...state,
                gettingOrganizations: false,
                organizations: action.payload.organizations,
                organizationsCount: action.payload.organizationsCount,
            };
        case GET_ORGANIZATIONS_FAIL:
            return {
                ...state,
                gettingOrganizations: false,
                organizationErrors: action.payload.errors,
            };
        case UPDATE_ORGANIZATION_BY_ADMIN:
            return {
                ...state,
                updatingOrganizations: true,
                organizationErrors: null,
            };
        case UPDATE_ORGANIZATION_BY_ADMIN_SUCCESS:
            return {
                ...state,
                updatingOrganizations: false,
            };
        case UPDATE_ORGANIZATION_BY_ADMIN_FAIL:
            return {
                ...state,
                updatingOrganizations: false,
                organizationErrors: action.payload.errors,
            };
        case DELETE_ORGANIZATION:
            return {
                ...state,
                gettingOrganizations: true,
                organizationErrors: null,
            };
        case DELETE_ORGANIZATION_SUCCESS:
            return {
                ...state,
                gettingOrganizations: false,
            };
        case DELETE_ORGANIZATION_FAIL:
            return {
                ...state,
                gettingOrganizations: false,
                organizationErrors: action.payload.errors,
            };

        case SET_ACTIVE_ORGANIZATION_LOCATION_ID:
            return {
                ...state,
                activeLocationId: action.payload.locationId,
            };

        case SET_ACTIVE_ORGANIZATION_SCREEN_ID:
            return {
                ...state,
                activeScreenId: action.payload.screenId,
            };

        case GET_CAMERAS:
            return {
                ...state,
                gettingCameras: true,
                camerasErrors: null,
            };
        case GET_CAMERAS_SUCCESS:
            return {
                ...state,
                gettingCameras: false,
                cameras: action.payload.cameras,
            };
        case GET_CAMERAS_FAIL:
            return {
                ...state,
                gettingCameras: false,
                camerasErrors: action.payload.errors,
            };

        case OPEN_ADD_CAMERA_MODAL:
            return {
                ...state,
                isAddCameraModalOpen: true,
            };

        case CLOSE_ADD_CAMERA_MODAL:
            return {
                ...state,
                isAddCameraModalOpen: false,
            };

        case ADD_CAMERA:
            return {
                ...state,
                addingCameras: true,
                camerasErrors: null,
            };
        case ADD_CAMERA_SUCCESS:
            return {
                ...state,
                addingCameras: false,
                isAddCameraModalOpen: false,
            };
        case ADD_CAMERA_FAIL:
            return {
                ...state,
                addingCameras: false,
                camerasErrors: action.payload.errors,
            };

        case DELETE_CAMERA:
            return {
                ...state,
                deletingCameras: true,
                camerasErrors: null,
            };
        case DELETE_CAMERA_SUCCESS:
            return {
                ...state,
                deletingCameras: false,
            };
        case DELETE_CAMERA_FAIL:
            return {
                ...state,
                deletingCameras: false,
                camerasErrors: action.payload.errors,
            };

        case CLEAR_ADMIN_PAGE_STATE:
            return {
                ...state,
                organizationErrors: null,
                camerasErrors: null,
            };
        default:
            return state;
    }
}
