import {
    setClosedInfoBlocks,
    setCloseEasyOnboardingModal,
    setOpenEasyOnboardingModal,
    setUpdatesInfoBlockShown,
} from '../actions';
import { OrganizationProductEnum } from '../../../../../common/types';

export type AdminTableOrganization = {
    id: string;
    title: string;
    email: string;
    screensNumber: number;
    licenses: number;
    createdAt: string;
    href: string;
    product: OrganizationProductEnum;
};

export type MainPageState = {
    isEasyOnboardingModalOpen: boolean;
    closedInfoBlocks: string[];
    updateInfoBlockShown: boolean;
};

export type OpenEasyOnboardingAction = ReturnType<typeof setOpenEasyOnboardingModal>;
export type CloseEasyOnboardingAction = ReturnType<typeof setCloseEasyOnboardingModal>;

export type SetClosedInfoBlocksAction = ReturnType<typeof setClosedInfoBlocks>;
export type SetUpdatesInfoBlockShownAction = ReturnType<typeof setUpdatesInfoBlockShown>;

export type MainAction =
    | OpenEasyOnboardingAction
    | CloseEasyOnboardingAction
    | SetClosedInfoBlocksAction
    | SetUpdatesInfoBlockShownAction;
