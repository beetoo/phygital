export const OPEN_EASY_ONBOARDING_MODAL = 'OPEN_EASY_ONBOARDING_MODAL';
export const CLOSE_EASY_ONBOARDING_MODAL = 'CLOSE_EASY_ONBOARDING_MODAL';

export const SET_CLOSED_INFO_BLOCKS = 'SET_CLOSED_INFO_BLOCKS';

export const SET_UPDATES_INFO_BLOCK_SHOWN = 'SET_UPDATES_INFO_BLOCK_SHOWN';

export const setOpenEasyOnboardingModal = () => ({ type: OPEN_EASY_ONBOARDING_MODAL }) as const;
export const setCloseEasyOnboardingModal = () => ({ type: CLOSE_EASY_ONBOARDING_MODAL }) as const;

export const setClosedInfoBlocks = (blockId: string) =>
    ({ type: SET_CLOSED_INFO_BLOCKS, payload: { blockId } }) as const;

export const setUpdatesInfoBlockShown = (updateInfoBlockShown: boolean) =>
    ({ type: SET_UPDATES_INFO_BLOCK_SHOWN, payload: { updateInfoBlockShown } }) as const;
