import React from 'react';
import { useLocation } from 'react-router-dom';

import UserMainWindow from '../UserMainWindow';
import AdminOrganizationsList from '../AdminOrganizationsList';

const MainWindow: React.FC = () => {
    const isOrganizationsPage = useLocation().pathname.endsWith('organizations');

    return isOrganizationsPage ? <AdminOrganizationsList /> : <UserMainWindow />;
};

export default MainWindow;
