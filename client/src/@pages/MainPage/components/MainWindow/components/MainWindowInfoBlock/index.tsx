import React from 'react';
import ReactPlayer from 'react-player/youtube';
import { clsx } from 'clsx';

import imageSunCloud from '../../../../../../assets/imageSunCloud.png';

import { useMainWindowInfoBlocks } from '../../../../hooks/useMainWindowInfoBlocks';
import { isSLClientMode } from '../../../../../../constants/environments';

import css from './style.m.css';

const MainWindowInfoBlock: React.FC = () => {
    const { t } = useMainWindowInfoBlocks();

    return (
        <div className={css.wrapperRightBlockMainPage}>
            {isSLClientMode ? (
                <div id="infoBlock:004" className={css.wrapperOrderScreen}>
                    <div className={css.orderScreen}>
                        <div className={clsx(css.wrappImgOrderBlog, css.wrappImgOrderBlogFirst)}>
                            <img src={imageSunCloud} width="78px" height="78px" alt="" />
                        </div>
                        <p className={clsx(css.orderBlogItem, css.orderBlogItemFirst)}>
                            {t('textSix')}
                            {/*Мы добавили триггерные сценарии*/}
                        </p>
                        <p className={css.orderBlogItemSecond}>
                            {t('textSeven')}
                            {/*Теперь, создавая плейлист, можно настроить показ контента под любую погоду и время.*/}
                        </p>
                        {/*<span className={css.orderBlogClose} onClick={closeOneInfoBlock('infoBlock:004')} />*/}
                    </div>
                </div>
            ) : (
                <div id="infoBlock:006" className={css.wrapperOrderScreen}>
                    <div className={css.orderScreen}>
                        <div className={css.wrappImgOrderBlog}>
                            <ReactPlayer
                                url="https://www.youtube.com/watch?v=HUYuPENcbPo"
                                height="130px"
                                controls={true}
                            />
                        </div>
                        <div className={css.youtubeBlogItemWrapper}>
                            <p className={css.youtubeBlogItem}>{t('settingConditions')}</p>
                        </div>
                        {/*<span className={css.orderBlogClose} onClick={closeOneInfoBlock('infoBlock:006')} />*/}
                    </div>
                </div>
            )}
        </div>
    );
};

export default MainWindowInfoBlock;
