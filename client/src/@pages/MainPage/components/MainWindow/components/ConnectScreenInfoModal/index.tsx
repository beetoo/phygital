import React from 'react';
import { clsx } from 'clsx';

import { Tooltip } from '../../../../../../ui-kit';
import { useConnectScreenInfoModal } from '../../../../hooks/useConnectScreenInfoModal';
import tvBox_4x from '../../../../../../assets/tvBox_4x.png';
import imagePOS_3x from '../../../../../../assets/imagePOS_3x.png';
import headGirl_4x from '../../../../../../assets/headGirl_4x.png';

import css from './style.m.css';

const ConnectScreenInfoModal: React.FC = () => {
    const { openConsultationRequestModal, openAddScreenModal, closeInfoModal, isAddScreenModalDisabled, t } =
        useConnectScreenInfoModal();

    return (
        <div className={css.infoModal}>
            <h1 className={css.infoModalHeader}>{t('connectNewScreen')}</h1>
            <div className={css.infoModalBlockOne}>
                <div className={css.infoModalBlockTwo}>
                    <div id="infoBlock:001" className={css.wrapperOrderBox}>
                        <div className={css.orderBox}>
                            <div className={css.wrappImgOrderBlog}>
                                <img src={tvBox_4x} width="104px" height="104px" alt="" />
                            </div>
                            <p className={css.orderBlogItem}>{t('textEight')}</p>
                            <p className={css.orderBlogItemOne}>{t('textNine')}</p>
                            <button className={css.btnRecordConsultation} onClick={openConsultationRequestModal}>
                                {t('btnAskForAdvice')}
                            </button>
                        </div>
                    </div>
                    <div id="infoBlock:002" className={css.wrapperOrderBox}>
                        <div className={css.orderBox}>
                            <div className={css.wrappImgOrderBlog}>
                                <img className={css.imagePOS} src={imagePOS_3x} width="172px" height="100px" alt="" />
                            </div>
                            <p className={css.orderBlogItem}>{t('textTen')}</p>
                            <p className={css.orderBlogItemOne}>{t('textEleven')}</p>
                            <button className={css.btnRecordConsultation} onClick={openConsultationRequestModal}>
                                {t('btnAskForAdvice')}
                            </button>
                        </div>
                    </div>
                </div>
                <div className={css.wrapperConnectScreen}>
                    <div className={css.orderBox}>
                        <div className={css.wrappImgOrderBlog}>
                            <img src={headGirl_4x} width="148px" height="148px" alt="" />
                        </div>
                        <p className={css.orderBlogItem}>{t('textTwelve')}</p>
                        <p className={css.orderBlogItemOne}>{t('textThirteen')}</p>
                        {isAddScreenModalDisabled ? (
                            <Tooltip
                                title={t('maxScreensTooltip')
                                    .split(`\n`)
                                    .map((text, index) => (
                                        <div key={index}>{text}</div>
                                    ))}
                                placement="bottom"
                            >
                                <button className={clsx(css.btnConnectScreen, css.disabled)}>
                                    {t('btnConnectScreen')}
                                </button>
                            </Tooltip>
                        ) : (
                            <button className={css.btnConnectScreen} onClick={openAddScreenModal}>
                                {t('btnConnectScreen')}
                            </button>
                        )}
                    </div>
                </div>
            </div>
            <button className={css.orderBlogClose} onClick={closeInfoModal}></button>
        </div>
    );
};

export default ConnectScreenInfoModal;
