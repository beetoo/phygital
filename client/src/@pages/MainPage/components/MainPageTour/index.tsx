import React, { ComponentProps } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { Tour } from '../../../../components/Tour';
import { TourBody } from '../../../../components/Tour/components/TourBody';
import { TourHeader } from '../../../../components/Tour/components/TourHeader';
import { selectHelpIconRef, selectOnBoardingToggleRef } from '../../../../selectors';

import { selectCurrentModal } from '../../../../components/ModalsContainer/selectors';
import { MODALS } from '../../../../components/ModalsContainer/types';
import startImage from './images/start_image.png';
import learnerImg from './images/learner.png';
import learner2Img from './images/teacher.png';

type Steps = ComponentProps<typeof Tour>['steps'];

export const MainPageTour: React.FC<{ addScreen: HTMLElement | null; addPlaylist: HTMLElement | null }> = ({
    addScreen,
    addPlaylist,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'learningHints' });

    const onBoardingToggle = useSelector(selectOnBoardingToggleRef);
    const helpIconRef = useSelector(selectHelpIconRef);

    const steps = React.useMemo<Steps>(
        () => [
            helpIconRef
                ? {
                      name: 'helpIconRef',
                      target: helpIconRef,
                      title: (
                          <TourHeader>
                              <img src={learnerImg} width={32} height={32} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  Раздел «Помощь»
                                  {/* Пошаговые инструкции */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              Здесь вы найдете инструкции о том, с чего начать работу в сервисе, как подключить экраны к
                              ЛК и как запустить на экраны свой первый плейлист с контентом.
                              {/* Как подготовить экраны разных фирм к подключению и другие пошаговые инструкции смотрите в
                              разделе «Помощь». */}
                          </TourBody>
                      ),
                      offset: -4,
                  }
                : null,
            onBoardingToggle
                ? {
                      name: 'onBoardingStart',
                      target: onBoardingToggle,
                      title: (
                          <TourHeader>
                              <img src={startImage} width={32} height={32} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textOne')}
                                  {/* Быстрый доступ к обучению */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              {t('textTwo')}
                              {/* В любой момент подсказки можно выключить или пройти обучение сначала. */}
                          </TourBody>
                      ),
                  }
                : null,
            addScreen
                ? {
                      name: 'addScreen',
                      target: addScreen,
                      title: (
                          <TourHeader>
                              <img src={learnerImg} width={32} height={32} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textThree')}
                                  {/* Начните с подключения экрана */}
                                  {/* Начните с добавления экрана */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <div>
                                  {t('textFour')}
                                  {/* Первым делом подключите экраны, чтобы загружать на них плейлисты. */}
                              </div>
                              <div>
                                  {t('textFive')}
                                  {/* Также можно подключить экран в разделе «Экраны». */}
                              </div>
                              {/* Первым делом добавьте экраны, чтобы создавать и загружать на них плейлисты. Также можно
                              подключить экран в разделе «Экраны». */}
                          </TourBody>
                      ),
                  }
                : null,
            addPlaylist
                ? {
                      name: 'addPlaylist',
                      target: addPlaylist,
                      title: (
                          <TourHeader>
                              <img src={learner2Img} width={32} height={32} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textSix')}
                                  {/* Добавьте плейлист на экран */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <div>
                                  {t('textSeven')}
                                  {/* Чтобы запустить контент на экран, создайте плейлист. */}
                              </div>
                              <div>
                                  {t('textEight')}
                                  {/* Также можно перейти к созданию плейлиста в разделе «Плейлисты». */}
                              </div>
                          </TourBody>
                      ),
                  }
                : null,
        ],
        [addPlaylist, addScreen, helpIconRef, onBoardingToggle, t],
    );

    const currentModal = useSelector(selectCurrentModal);
    const isTvBoxInfoModalOpen = currentModal === MODALS.TV_BOX_INFO_MODAL;
    const isTvBoxSecondInfoModalOpen = currentModal === MODALS.TV_BOX_SECOND_INFO_MODAL;
    const isTvBoxRequestModalOpen = currentModal === MODALS.TV_BOX_REQUEST_MODAL;

    const isTvBoxModalsOpen = isTvBoxInfoModalOpen || isTvBoxSecondInfoModalOpen || isTvBoxRequestModalOpen;

    return (
        <Tour
            steps={isTvBoxModalsOpen ? [] : steps}
            name="mainPage"
            // run={isOnBoardingSkipped ? isOnBoardingSkipped : undefined}
        />
    );
};
