import React from 'react';
import { X } from '@phosphor-icons/react';

import { isProductionEngClient, isSLClientMode } from '../../../../constants/environments';
import { useMainWindowInfoBlocks } from '../../hooks/useMainWindowInfoBlocks';

import css from './style.m.css';

const UpdatesInfoBlock: React.FC = () => {
    const { isUpdatesInfoBlockShown, onUpdatesInfoBlockClick, closeUpdatesInfoBlock } = useMainWindowInfoBlocks(false);

    return !isProductionEngClient && !isSLClientMode && isUpdatesInfoBlockShown ? (
        <div className={css.containerUpdatedInfo}>
            <div className={css.containerUpdatedInfoInner}>
                <div className={css.updatedInfo}>
                    <div className={css.updatedInfoDescription}>
                        <p>Посмотреть последние обновления сервиса</p>
                        <button className={css.btnMoreDetailed} type="button" onClick={onUpdatesInfoBlockClick}>
                            Подробнее
                        </button>
                    </div>
                </div>
                <X className={css.closeButton} size={16} weight="bold" onClick={closeUpdatesInfoBlock} />
            </div>
        </div>
    ) : null;
};

export default UpdatesInfoBlock;
