import React from 'react';
import { clsx } from 'clsx';

import ModalTemplate from '../../../../components/ModalTemplate';

import { useRegisterWelcomePage } from '../../hooks/useRegisterWelcomePage';

import css from './style.m.css';

const RegisterWelcomePage: React.FC = () => {
    const { email, isFirstConfirmation, closeWelcomePage, t } = useRegisterWelcomePage();

    return (
        <ModalTemplate isOpen={true} overlayBackground="#f7f7f9">
            <div className={css.wrapConfirmEmail}>
                <div className={css.confirmEmail}>
                    <h2>
                        {isFirstConfirmation ? `${t('emailAddressConfirmed')}` : `${t('yourEmailAlreadyConfirmed')}`}
                    </h2>
                    <p className={css.confirmEmailText}>
                        {t('textEleven')} <b>{email}</b>{' '}
                        {isFirstConfirmation ? `${t('textTwelve')}` : `${t('textThirteen')}`}
                        <br />
                        {isFirstConfirmation ? '' : `${t('textFourteen')}`}
                    </p>
                    <div className={css.fieldBtnOk}>
                        <button
                            className={clsx(css.btnOk, !isFirstConfirmation && css.enter)}
                            onClick={closeWelcomePage}
                        >
                            {isFirstConfirmation ? `${t('btnOk')}` : `${t('btnLogIn')}`}
                        </button>
                    </div>
                </div>
            </div>
        </ModalTemplate>
    );
};

export default RegisterWelcomePage;
