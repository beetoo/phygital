import React from 'react';
import { clsx } from 'clsx';

import { useMobileDashboardPage } from '../../hooks/useMobileDashboardPage';

import css from './style.m.css';

const MobileDashboardPage: React.FC = () => {
    const {
        screensNumber,
        onlineScreensNumber,
        offlineScreensNumber,
        errorScreensNumber,
        isSupportInfoShown,
        setupSupportInfo,
        handleLogout,
        t,
    } = useMobileDashboardPage();

    return isSupportInfoShown ? (
        <div className={css.wraperDashboardPage}>
            <div className={css.mobileSignIn}>
                <a onClick={setupSupportInfo}>{t('back')}</a>
            </div>
            <div className={css.wrapperMobileDashboard}>
                <div className={css.mobilePage}>
                    <h2 className={css.title}>{t('support')}</h2>
                    <p className={css.titleItem}>{t('technicalSupport')}</p>
                    <p className={css.titleItem1}>
                        <a className={css.menuNavigationFeedBackLink} href="mailto:support@phygitalsignage.io">
                            support@phygitalsignage.io
                        </a>
                    </p>
                    <p className={css.titleItem1}>+7 499 705-04-52</p>
                    <p className={css.titleItem1}>
                        {t('telegramSupportBot')}:{' '}
                        <a
                            className={css.telegramBotLink}
                            target="_blank"
                            href="https://t.me/PhygitalSignage_support_bot"
                            rel="noreferrer"
                        >
                            @PhygitalSignage_support_bot
                        </a>
                    </p>
                    <p className={clsx(css.titleItem1, css.titleItem2)}>
                        {t('openingHours')}: {t('monFri')} 10:00 - 18:00 {t('moscowTime')}
                    </p>

                    <p className={css.titleItem}>{t('paymentAndSubscriptionChanges')}</p>
                    <p className={css.titleItem3}>
                        <a className={css.menuNavigationFeedBackLink} href="mailto:hello@phygitalsignage.io">
                            hello@phygitalsignage.io
                        </a>
                    </p>
                    <p className={css.titleItem1}>+7 499 705-04-52</p>
                    <p className={clsx(css.titleItem1, css.titleItem2)}>
                        {t('openingHours')}: {t('monFri')} 10:00 - 18:00 {t('moscowTime')}
                    </p>
                </div>
            </div>
        </div>
    ) : (
        <div className={css.wraperDashboardPage}>
            <div className={css.mobileSignIn}>
                <a onClick={handleLogout}>{t('logOut')}</a>
            </div>
            <div className={css.wrapperMobileDashboard}>
                <div className={css.blockScreenSetting}>
                    <h3>{t('screens')}</h3>
                    <ul className={css.ScreenStatistic}>
                        <li className={css.cardScreenStatistic}>
                            <a>
                                <p className={clsx(css.screenStatisticNumber, css.screenStatisticAll)}>
                                    {screensNumber}
                                </p>
                                <p className={css.screenStatisticName}>{t('total')}</p>
                            </a>
                        </li>
                        <li className={css.cardScreenStatistic}>
                            <a>
                                <p className={clsx(css.screenStatisticNumber, css.screenStatisticOn)}>
                                    {onlineScreensNumber}
                                </p>
                                <p className={css.screenStatisticName}>{t('on')}</p>
                            </a>
                        </li>
                        <li className={css.cardScreenStatistic}>
                            <a>
                                <p className={clsx(css.screenStatisticNumber, css.screenStatisticOff)}>
                                    {offlineScreensNumber}
                                </p>
                                <p className={css.screenStatisticName}>{t('off')}</p>
                            </a>
                        </li>
                        <li className={css.cardScreenStatistic}>
                            <a>
                                <p className={clsx(css.screenStatisticNumber, css.screenStatisticError)}>
                                    {errorScreensNumber}
                                </p>
                                <p className={css.screenStatisticName}>{t('error')}</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <div className={css.mobilePage}>
                    {/*<h2>{greeting}!</h2>*/}
                    {screensNumber === 0 ? <p>{t('textFive')}</p> : <p>{t('textSix')}</p>}
                    <p>
                        {screensNumber === 0 ? `${t('textSeven')}` : `${t('textEight')}`}
                        <span>{t('textNine')}</span> {t('textTen')}
                    </p>
                </div>
                <div className={css.support}>
                    <a onClick={setupSupportInfo}>{t('support')}</a>
                </div>
            </div>
        </div>
    );
};

export default MobileDashboardPage;
