import React from 'react';
import { clsx } from 'clsx';

import UpdatesInfoBlock from '../UpdatesInfoBlock';
import picture from '../../../../assets/img_girl1x.png';
import MainWindowInfoBlock from '../MainWindow/components/MainWindowInfoBlock';
import { useUserMainWindow } from '../../hooks/useUserMainWindow';

import css from './style.m.css';

const UserMainWindow: React.FC = () => {
    const {
        greeting,
        screensNumber,
        onlineScreensNumber,
        offlineScreensNumber,
        errorScreensNumber,
        // openAddPlaylistModal,
        // openAddScreenModal,
        // isNoScreens,
        t,
    } = useUserMainWindow();

    return (
        <div>
            <UpdatesInfoBlock />
            <div className={css.mainPage}>
                <div className={css.mainPageInfo}>
                    <div className={css.wrapblock1}>
                        <div className={css.wrapblock2}>
                            <div className={css.block1}>
                                <div className={css.block1Info}>
                                    <h3 className={css.block1Item}>{greeting}!</h3>
                                </div>
                                <div>
                                    <img className={css.block1ItemImage} src={picture} alt="" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className={css.wrapBlockScreenSetting}>
                        <div className={css.wrapperBlockScreenSetting}>
                            <div className={css.blockScreenSetting}>
                                <h3>{t('screens')}</h3>
                                <ul className={css.ScreenStatistic}>
                                    <li className={css.cardScreenStatistic}>
                                        <a>
                                            <p className={clsx(css.screenStatisticNumber, css.screenStatisticAll)}>
                                                {screensNumber}
                                            </p>
                                            <p className={css.screenStatisticName}>{t('total')}</p>
                                        </a>
                                    </li>
                                    <li className={css.cardScreenStatistic}>
                                        <a>
                                            <p className={clsx(css.screenStatisticNumber, css.screenStatisticOn)}>
                                                {onlineScreensNumber}
                                            </p>
                                            <p className={css.screenStatisticName}>{t('on')}</p>
                                        </a>
                                    </li>
                                    <li className={css.cardScreenStatistic}>
                                        <a>
                                            <p className={clsx(css.screenStatisticNumber, css.screenStatisticOff)}>
                                                {offlineScreensNumber}
                                            </p>
                                            <p className={css.screenStatisticName}>{t('off')}</p>
                                        </a>
                                    </li>
                                    <li className={css.cardScreenStatistic}>
                                        <a>
                                            <p className={clsx(css.screenStatisticNumber, css.screenStatisticError)}>
                                                {errorScreensNumber}
                                            </p>
                                            <p className={css.screenStatisticName}>{t('error')}</p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <MainWindowInfoBlock />
            </div>
        </div>
    );
};

export default UserMainWindow;
