import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setOnboardingDisabled, setOnboardingEnabled, setSkipOnBoarding } from '../../../../../actions';
import { selectOnBoardingStatus } from '../../../../../selectors';
import { selectEasyOnboardingModalStatus, selectScreens } from '../../../../ScreensPage/selectors';
import { setCloseEasyOnboardingModal, setOpenEasyOnboardingModal } from '../../../actions';
import { setShowModal } from '../../../../../components/ModalsContainer/actions';
import { MODALS } from '../../../../../components/ModalsContainer/types';
import { TV_BOX_INFO_SHOWN } from '../../../../../constants';

type ReturnValue = {
    isEasyOnboardingModalOpen: boolean;
    takeOnboarding: () => void;
    skipOnboarding: () => void;
};

export const useEasyOnboarding = (): ReturnValue => {
    const dispatch = useDispatch();

    const isEasyOnboardingModalOpen = useSelector(selectEasyOnboardingModalStatus);
    const isOnboardingEnabled = useSelector(selectOnBoardingStatus);

    const screens = useSelector(selectScreens);

    useEffect(() => {
        if (!isOnboardingEnabled && localStorage.getItem('EASY_ONBOARDING_SHOWN') !== 'true') {
            dispatch(setOpenEasyOnboardingModal());
        }
    }, [dispatch, isOnboardingEnabled]);

    const isTvBoxModalShown = localStorage.getItem(TV_BOX_INFO_SHOWN) === 'true';

    const takeOnboarding = useCallback(() => {
        dispatch(setCloseEasyOnboardingModal());
        localStorage.setItem('EASY_ONBOARDING_SHOWN', 'true');
        dispatch(setOnboardingEnabled());

        if (screens.length === 0 && !isTvBoxModalShown) {
            dispatch(setShowModal(MODALS.TV_BOX_INFO_MODAL));
        }
    }, [dispatch, isTvBoxModalShown, screens.length]);

    const skipOnboarding = useCallback(() => {
        localStorage.setItem('EASY_ONBOARDING_SHOWN', 'true');
        dispatch(setSkipOnBoarding(true));
        dispatch(setCloseEasyOnboardingModal());
        if (screens.length === 0 && !isTvBoxModalShown) {
            dispatch(setShowModal(MODALS.TV_BOX_INFO_MODAL));
            dispatch(setOnboardingDisabled());
        }
    }, [dispatch, isTvBoxModalShown, screens.length]);

    return {
        isEasyOnboardingModalOpen,
        takeOnboarding,
        skipOnboarding,
    };
};
