import React from 'react';

import ModalTemplate from '../../../../components/ModalTemplate';
import { useEasyOnboarding } from './hooks/useEasyOnboarding';

import css from './style.m.css';

export const EasyOnboarding: React.FC = () => {
    const { isEasyOnboardingModalOpen, takeOnboarding, skipOnboarding } = useEasyOnboarding();

    return (
        <ModalTemplate isOpen={isEasyOnboardingModalOpen}>
            <div className={css.outerContainer}>
                <div className={css.container}>
                    <div className={css.imgContainer} />
                    <h1 className={css.title}>Добро пожаловать в Phygital Signage!</h1>
                    <p>
                        Для лёгкого старта предлагаем вам пройти короткий обучающий тур по основным возможностям
                        приложения: быстрое начало работы; настройка и группировка экранов; создание и запуск плейлиста
                        на экран.
                    </p>
                    <p>Хотите пройти обучение сейчас?</p>
                    <div className={css.actionContainer}>
                        <button className={css.btnSkipTour} onClick={skipOnboarding}>
                            Попробовать позже
                        </button>
                        <button className={css.btnTakeTour} onClick={takeOnboarding}>
                            Пройти обучение
                        </button>
                    </div>
                </div>
            </div>
        </ModalTemplate>
    );
};
