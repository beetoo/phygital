import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import ModalTemplate from '../../../../components/ModalTemplate';

import { setClearRecoveryPasswordState } from '../../../RecoveryPassword/actions';
import { selectCurrentOrganization } from '../../../ProfilePage/selectors';

import css from './style.m.css';

const RecoveryPasswordSuccessModal: React.FC = () => {
    const { t } = useTranslation('translation', { keyPrefix: 'signIn' });

    const dispatch = useDispatch();

    const { email } = useSelector(selectCurrentOrganization);

    const resetRecoveryPasswordState = useCallback(() => {
        dispatch(setClearRecoveryPasswordState());
    }, [dispatch]);

    return (
        <ModalTemplate isOpen={true} overlayBackground="#f7f7f9">
            <div className={css.passwordChanged}>
                <h2>{t('passwordChanged')}</h2>
                <p className={css.passwordChangedParagraph}>
                    {t('textFour')} <span>{email}</span> {t('textFive')}
                </p>
                <div className={css.fieldBtnSignIn}>
                    <button className={css.btnSignIn} onClick={resetRecoveryPasswordState}>
                        {t('btnLogIn')}
                    </button>
                </div>
            </div>
        </ModalTemplate>
    );
};

export default RecoveryPasswordSuccessModal;
