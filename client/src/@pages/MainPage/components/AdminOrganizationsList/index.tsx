import React from 'react';

import { AdminOrganizationsTable } from './components/AdminOrganizationsTable';
import { useAdminOrganizationsList } from '../../hooks/useAdminOrganizationsList';
import { ROWS_PER_PAGE } from '../../../AdminPage/constants';

import './style.css';

const AdminOrganizationsList: React.FC = () => {
    const {
        inputValue,
        onChangeInputValue,
        onSearchClick,
        onSearchKeyDown,
        clearInputValue,

        count,
        page,
        onPageChange,
    } = useAdminOrganizationsList();

    return (
        <div className="my-alert">
            <div>
                <div>
                    <div>
                        <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                            <path
                                stroke="currentColor"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                            ></path>
                        </svg>
                    </div>
                    <input
                        type="text"
                        id="table-search-users"
                        placeholder="Поиск"
                        value={inputValue}
                        onChange={onChangeInputValue}
                        onKeyDown={onSearchKeyDown}
                        maxLength={125}
                    />
                </div>
                <button type="button" className="my-alert__unique1" onClick={onSearchClick}>
                    Искать
                </button>
                <button type="button" className="my-alert__unique2" onClick={clearInputValue}>
                    Очистить
                </button>
            </div>
            <AdminOrganizationsTable />
            <nav aria-label="Table navigation">
                <span>
                    Показаны{' '}
                    <span>{`${page * ROWS_PER_PAGE + 1} - ${count >= ROWS_PER_PAGE ? ROWS_PER_PAGE * (page + 1) : count}`}</span>{' '}
                    из <span>{count}</span>
                </span>
                <ul>
                    <li onClick={() => onPageChange(page > 0 ? page - 1 : 0)}>
                        <a className="my-alert__unique3">Назад</a>
                    </li>
                    <li onClick={ROWS_PER_PAGE * (page + 1) < count ? () => onPageChange(page + 1) : undefined}>
                        <a className="my-alert__unique4">Вперед</a>
                    </li>
                </ul>
            </nav>
        </div>
    );
};

export default AdminOrganizationsList;
