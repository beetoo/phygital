import React, { PropsWithChildren } from 'react';

import { useAdminOrganizationsTable } from '../../../../hooks/useAdminOrganizationsTable';
import { isSLClientMode } from '../../../../../../constants/environments';

const TableLink: React.FC<PropsWithChildren<{ href: string }>> = ({ href, children }) => (
    <a href={href} onClick={(event) => event.preventDefault()}>
        {children}
    </a>
);

export const AdminOrganizationsTable: React.FC = () => {
    const { organizations, goToOrganization } = useAdminOrganizationsTable();

    return (
        <table>
            <thead>
                <tr>
                    <th scope="col">Компания</th>

                    <th scope="col">Email</th>
                    <th scope="col">Экраны</th>
                    {!isSLClientMode && <th scope="col">Лицензии</th>}
                    <th scope="col">Регистрация</th>
                    {!isSLClientMode && <th scope="col">Продукт</th>}
                </tr>
            </thead>
            <tbody>
                {organizations.map(({ id, title, email, screensNumber, licenses, product, createdAt, href }) => (
                    <tr key={id} style={{ cursor: 'pointer' }} onClick={goToOrganization(id)}>
                        <td>
                            <TableLink href={href}>{title}</TableLink>
                        </td>

                        <td>
                            <TableLink href={href}>{email}</TableLink>
                        </td>
                        <td>
                            <TableLink href={href}>
                                <div>{screensNumber}</div>
                            </TableLink>
                        </td>
                        {!isSLClientMode && (
                            <td>
                                <TableLink href={href}>
                                    <div>{licenses}</div>
                                </TableLink>
                            </td>
                        )}

                        <td>
                            <TableLink href={href}>
                                <div>{createdAt}</div>
                            </TableLink>
                        </td>
                        {!isSLClientMode && (
                            <td>
                                <TableLink href={href}>
                                    <div>{product}</div>
                                </TableLink>
                            </td>
                        )}
                    </tr>
                ))}
            </tbody>
        </table>
    );
};
