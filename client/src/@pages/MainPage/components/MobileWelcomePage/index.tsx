import React from 'react';

import imageScreen from '../../../../assets/imageScreen.png';
import { useMobileWelcomePage } from '../../hooks/useMobileWelcomePage';

import css from './style.m.css';

const MobileWelcomePage: React.FC<{ closeWelcomeMobilePage: () => void }> = ({ closeWelcomeMobilePage }) => {
    const { greeting, isRegisterTextShown, handleLogout, t } = useMobileWelcomePage();

    return (
        <div className={css.wraperMobilePage}>
            <div className={css.mobileSignIn}>
                <a onClick={handleLogout}>{t('logOut')}</a>
            </div>
            <div className={css.mobilePage}>
                <img src={imageScreen} alt="" />
                <h2>{greeting}!</h2>
                {isRegisterTextShown && <p>{t('textOne')}</p>}
                <p>
                    <span>{t('textTwo')}</span> {t('textThree')}
                </p>
                <p>{t('textFour')}</p>
                <button className={css.mobileBtn} onClick={closeWelcomeMobilePage}>
                    {t('btnGoToScreens')}
                </button>
            </div>
        </div>
    );
};

export default MobileWelcomePage;
