import React from 'react';

import MainWindow from './components/MainWindow';
import RecoveryPasswordSuccessModal from './components/RecoveryPasswordSuccessModal';
import { useMainPage } from './hooks/useMainPage';

const MainPage: React.FC = () => {
    const { authProcessing, isChangePasswordSuccess } = useMainPage();

    return authProcessing ? null : isChangePasswordSuccess ? <RecoveryPasswordSuccessModal /> : <MainWindow />;
};

export default MainPage;
