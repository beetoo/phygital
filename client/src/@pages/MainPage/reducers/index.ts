import {
    CLOSE_EASY_ONBOARDING_MODAL,
    OPEN_EASY_ONBOARDING_MODAL,
    SET_CLOSED_INFO_BLOCKS,
    SET_UPDATES_INFO_BLOCK_SHOWN,
} from '../actions';
import { MainAction, MainPageState } from '../types';
import { CLOSED_INFO_BLOCKS } from '../constants';

const mainPageInitialState: MainPageState = {
    isEasyOnboardingModalOpen: false,
    closedInfoBlocks: JSON.parse(localStorage.getItem(CLOSED_INFO_BLOCKS) ?? '[]'),
    updateInfoBlockShown: true,
};

export default function mainPageReducer(state = mainPageInitialState, action: MainAction): MainPageState {
    switch (action.type) {
        case OPEN_EASY_ONBOARDING_MODAL:
            return {
                ...state,
                isEasyOnboardingModalOpen: true,
            };
        case CLOSE_EASY_ONBOARDING_MODAL:
            return {
                ...state,
                isEasyOnboardingModalOpen: false,
            };

        case SET_CLOSED_INFO_BLOCKS:
            return {
                ...state,
                closedInfoBlocks: [...state.closedInfoBlocks, action.payload.blockId],
            };

        case SET_UPDATES_INFO_BLOCK_SHOWN:
            return {
                ...state,
                updateInfoBlockShown: action.payload.updateInfoBlockShown,
            };
        default:
            return state;
    }
}
