import { StoreType } from '../../../types';
import { AdminTableOrganization } from '../types';

export const selectAdminTableOrganizations = ({
    pages: {
        admin: { organizations },
    },
}: StoreType): AdminTableOrganization[] => organizations;

export const selectClosedInfoBlocks = ({
    pages: {
        main: { closedInfoBlocks },
    },
}: StoreType): string[] => closedInfoBlocks;

export const selectUpdatesInfoBlockShown = ({
    pages: {
        main: { updateInfoBlockShown },
    },
}: StoreType): boolean => updateInfoBlockShown;
