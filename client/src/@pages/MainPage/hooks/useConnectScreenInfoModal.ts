import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setHideModal, setShowModal } from '../../../components/ModalsContainer/actions';
import { setOpenAddScreenModal } from '../../ScreensPage/actions';
import { MODALS } from '../../../components/ModalsContainer/types';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';

type ReturnValue = {
    openConsultationRequestModal: () => void;
    openAddScreenModal: () => void;
    closeInfoModal: () => void;
    isAddScreenModalDisabled: boolean;
    t: TFunction;
};

export const useConnectScreenInfoModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'main' });

    const dispatch = useDispatch();

    const { maxScreensCount, limitScreens } = useSelector(selectCurrentOrganization);

    const openConsultationRequestModal = useCallback(() => {
        dispatch(setShowModal(MODALS.TV_BOX_REQUEST_MODAL, MODALS.CONNECT_SCREEN_INFO_MODAL));
    }, [dispatch]);

    const isAddScreenModalDisabled = useMemo(() => maxScreensCount >= limitScreens, [limitScreens, maxScreensCount]);

    const openAddScreenModal = useCallback(() => {
        dispatch(setHideModal());
        dispatch(setShowModal('', MODALS.CONNECT_SCREEN_INFO_MODAL));
        dispatch(setOpenAddScreenModal());
    }, [dispatch]);

    const closeInfoModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    return {
        openConsultationRequestModal,
        openAddScreenModal,
        closeInfoModal,
        isAddScreenModalDisabled,
        t,
    };
};
