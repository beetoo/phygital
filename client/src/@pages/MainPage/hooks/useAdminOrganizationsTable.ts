import { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { selectAdminTableOrganizations } from '../selectors';
import { AdminTableOrganization } from '../types';

type ReturnValue = {
    organizations: AdminTableOrganization[];
    goToOrganization: (organizationId: string) => () => void;
};
export const useAdminOrganizationsTable = (): ReturnValue => {
    const navigate = useNavigate();

    const organizations = useSelector(selectAdminTableOrganizations);

    const goToOrganization = useCallback(
        (organizationId: string) => () => {
            navigate(`/organizations/${organizationId}/main`);
        },
        [navigate],
    );

    return {
        organizations,
        goToOrganization,
    };
};
