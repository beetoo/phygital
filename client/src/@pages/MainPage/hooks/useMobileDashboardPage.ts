import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setSignOutProcess } from '../../../actions';
import { selectScreens } from '../../ScreensPage/selectors';
import { getScreensInfo } from '../../ScreensPage/helpers';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { setGetScreens } from '../../ScreensPage/actions';
import { USER_ROLE } from '../../../constants';

type ReturnValue = {
    screensNumber: number;
    onlineScreensNumber: number;
    offlineScreensNumber: number;
    errorScreensNumber: number;
    isSupportInfoShown: boolean;
    setupSupportInfo: () => void;
    handleLogout: () => void;
    t: TFunction;
};

export const useMobileDashboardPage = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'mobile' });

    const dispatch = useDispatch();

    const { id: organizationId, role } = useSelector(selectCurrentOrganization);
    const screens = useSelector(selectScreens);

    const { screensNumber, onlineScreensNumber, offlineScreensNumber, errorScreensNumber } = getScreensInfo(screens);

    const [isSupportInfoShown, setIsSupportInfoShown] = useState(false);

    const setupSupportInfo = useCallback(() => {
        setIsSupportInfoShown((prev) => !prev);
    }, []);

    const handleLogout = useCallback(() => {
        localStorage.removeItem('lastPage');
        dispatch(setSignOutProcess());
    }, [dispatch]);

    useEffect(() => {
        if (organizationId) {
            dispatch(setGetScreens(organizationId, 'all', role === USER_ROLE.ADMIN));
        }
    }, [dispatch, organizationId, role]);

    return {
        screensNumber,
        onlineScreensNumber,
        offlineScreensNumber,
        errorScreensNumber,
        isSupportInfoShown,
        setupSupportInfo,
        handleLogout,
        t,
    };
};
