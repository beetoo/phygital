import { ChangeEvent, useCallback, useEffect, useState, useRef, KeyboardEvent } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
    selectOrganizationsCount,
    selectOrganizationsPage,
    selectOrganizationsSearchValue,
} from '../../AdminPage/selectors';
import { setGetOrganizations, setOrganizationsPage } from '../../AdminPage/actions';
import { useAdminSearchField } from './useAdminSearchField';
import { getTakeAndSkip } from '../../../helpers';
import { ROWS_PER_PAGE } from '../../AdminPage/constants';

type ReturnValue = {
    inputValue: string;
    onChangeInputValue: (e: ChangeEvent) => void;
    onSearchClick: () => void;
    onSearchKeyDown: (event: KeyboardEvent<HTMLInputElement>) => void;
    clearInputValue: () => void;
    count: number;
    page: number;
    onPageChange: (page: number) => void;
};

export const useAdminOrganizationsList = (): ReturnValue => {
    const dispatch = useDispatch();

    const count = useSelector(selectOrganizationsCount);
    const page = useSelector(selectOrganizationsPage);
    const searchValue = useSelector(selectOrganizationsSearchValue);

    const { inputValue, onChangeInputValue, onSearchClick, onSearchKeyDown, clearInputValue } = useAdminSearchField();

    const [isSearchMode, setIsSearchMode] = useState(false);

    const onPageChange = useCallback(
        (page: number) => {
            dispatch(setOrganizationsPage(page));
            dispatch(setGetOrganizations({ ...getTakeAndSkip(ROWS_PER_PAGE, page + 1), search: searchValue }));
        },
        [dispatch, searchValue],
    );

    // устанавливаем searchMode в зависимости от строки поиска
    useEffect(() => {
        setIsSearchMode(inputValue.length > 0);
    }, [dispatch, inputValue]);

    // если строка поиска меняется, то сбрасываем страницы на 1
    const prevSearchValue = useRef(inputValue);
    useEffect(() => {
        if (isSearchMode && prevSearchValue.current !== inputValue) {
            dispatch(setOrganizationsPage(0));
            prevSearchValue.current = inputValue;
        }
    }, [dispatch, isSearchMode, inputValue]);

    useEffect(() => {
        dispatch(setGetOrganizations({ ...getTakeAndSkip(ROWS_PER_PAGE, page + 1), search: searchValue }));
    }, [dispatch, page, searchValue]);

    return {
        inputValue,
        onChangeInputValue,
        onSearchClick,
        onSearchKeyDown,
        clearInputValue,

        count,
        page,
        onPageChange,
    };
};
