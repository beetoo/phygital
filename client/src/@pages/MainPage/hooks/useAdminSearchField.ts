import { ChangeEvent, KeyboardEvent, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { setGetOrganizations, setOrganizationsPage, setOrganizationsSearchValue } from '../../AdminPage/actions';
import { getTakeAndSkip } from '../../../helpers';
import { ROWS_PER_PAGE } from '../../AdminPage/constants';
import { selectOrganizationsPage, selectOrganizationsSearchValue } from '../../AdminPage/selectors';

type ReturnValue = {
    inputValue: string;
    onChangeInputValue: (e: ChangeEvent) => void;
    onSearchClick: () => void;
    onSearchKeyDown: (event: KeyboardEvent<HTMLInputElement>) => void;
    clearInputValue: () => void;
};

export const useAdminSearchField = (): ReturnValue => {
    const dispatch = useDispatch();

    const currentPage = useSelector(selectOrganizationsPage);
    const searchValue = useSelector(selectOrganizationsSearchValue);

    const [inputValue, setInputValue] = useState(searchValue);

    const onChangeInputValue = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>): void => {
        setInputValue(value);
    }, []);

    const onSearchClick = useCallback(() => {
        if (inputValue.length > 0) {
            dispatch(setOrganizationsSearchValue(inputValue));
            dispatch(setOrganizationsPage(0));
            dispatch(setGetOrganizations({ ...getTakeAndSkip(ROWS_PER_PAGE, currentPage + 1), search: inputValue }));
        }
    }, [currentPage, dispatch, inputValue]);

    const onSearchKeyDown = useCallback(
        (event: KeyboardEvent<HTMLInputElement>) => {
            if (event.key === 'Enter') {
                onSearchClick();
            }
        },
        [onSearchClick],
    );

    const clearInputValue = useCallback(() => {
        if (searchValue.length > 0) {
            setInputValue('');
            dispatch(setOrganizationsSearchValue(''));
            dispatch(setGetOrganizations({ ...getTakeAndSkip(ROWS_PER_PAGE, currentPage + 1), search: '' }));
        }
    }, [currentPage, dispatch, searchValue.length]);

    return {
        inputValue,
        onChangeInputValue,
        onSearchClick,
        onSearchKeyDown,
        clearInputValue,
    };
};
