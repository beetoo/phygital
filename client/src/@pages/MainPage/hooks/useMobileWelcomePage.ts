import { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setSignOutProcess } from '../../../actions';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { selectConfirmRegistrationSuccessStatus } from '../../SignUp/selectors';

type ReturnValue = {
    greeting: string;
    isRegisterTextShown: boolean;
    handleLogout: () => void;
    t: TFunction;
};

export const useMobileWelcomePage = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'mobile' });

    const dispatch = useDispatch();

    const { name: organizationName, surname: organizationSurname } = useSelector(selectCurrentOrganization);
    const isConfirmRegistrationSuccess = useSelector(selectConfirmRegistrationSuccessStatus);

    const [isRegisterTextShown, setIsRegisterTextShown] = useState(isConfirmRegistrationSuccess);

    const greeting = useMemo(
        () =>
            organizationName && organizationSurname
                ? `${t('welcome')}, ${organizationName} ${organizationSurname}`
                : t('welcome'),
        [organizationName, organizationSurname, t],
    );

    useEffect(() => {
        if (!isConfirmRegistrationSuccess && isRegisterTextShown) {
            setIsRegisterTextShown(false);
        }
    }, [isConfirmRegistrationSuccess, isRegisterTextShown]);

    const handleLogout = useCallback(() => {
        localStorage.removeItem('lastPage');
        dispatch(setSignOutProcess());
    }, [dispatch]);

    return {
        greeting,
        isRegisterTextShown,
        handleLogout,
        t,
    };
};
