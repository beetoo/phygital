import { useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectAuthEmail, selectAuthStatus, selectFirstConfirmationStatus } from '../../../selectors';

type ReturnValue = {
    email: string;
    isFirstConfirmation: boolean;
    closeWelcomePage: () => void;
    t: TFunction;
};

export const useRegisterWelcomePage = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'signUp' });

    const navigate = useNavigate();

    const isAuth = useSelector(selectAuthStatus);
    const email = useSelector(selectAuthEmail);
    const isFirstConfirmation = useSelector(selectFirstConfirmationStatus);

    const closeWelcomePage = useCallback(() => {
        navigate(isAuth ? '/main' : '/signin');
    }, [isAuth, navigate]);

    return {
        email,
        isFirstConfirmation,
        closeWelcomePage,
        t,
    };
};
