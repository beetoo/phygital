import { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { getScreensInfo } from '../../ScreensPage/helpers';
import { selectScreens } from '../../ScreensPage/selectors';
import { setGetScreens } from '../../ScreensPage/actions';
import { USER_ROLE } from '../../../constants';

type ReturnValue = {
    greeting: string;
    screensNumber: number;
    onlineScreensNumber: number;
    offlineScreensNumber: number;
    errorScreensNumber: number;
    t: TFunction;
};

export const useUserMainWindow = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'main' });

    const dispatch = useDispatch();

    const {
        id: organizationId,
        name: organizationName,
        surname: organizationSurname,
        role,
    } = useSelector(selectCurrentOrganization);

    const screens = useSelector(selectScreens);

    const { screensNumber, onlineScreensNumber, offlineScreensNumber, errorScreensNumber } = getScreensInfo(screens);

    const greeting = useMemo(
        () =>
            organizationName && organizationSurname
                ? `${t('welcome')}, ${organizationName} ${organizationSurname}`
                : t('welcome'),
        [organizationName, organizationSurname, t],
    );

    useEffect(() => {
        if (organizationId) {
            dispatch(setGetScreens(organizationId, 'all', role === USER_ROLE.ADMIN));
        }
    }, [dispatch, organizationId, role]);

    return {
        greeting,
        screensNumber,
        onlineScreensNumber,
        offlineScreensNumber,
        errorScreensNumber,
        t,
    };
};
