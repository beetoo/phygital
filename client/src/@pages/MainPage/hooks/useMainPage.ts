import { useSelector } from 'react-redux';

import { selectAuthProcessingStatus } from '../../../selectors';
import { selectRecoveryPasswordSuccessStatus } from '../../RecoveryPassword/selectors';

type ReturnValue = {
    authProcessing: boolean;
    isChangePasswordSuccess: boolean;
};

export const useMainPage = (): ReturnValue => {
    const authProcessing = useSelector(selectAuthProcessingStatus);
    const isChangePasswordSuccess = useSelector(selectRecoveryPasswordSuccessStatus);

    return {
        authProcessing,
        isChangePasswordSuccess,
    };
};
