import { MouseEvent, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { useCustomRouter } from '../../../hooks/useCustomRouter';
import { selectClosedInfoBlocks, selectUpdatesInfoBlockShown } from '../selectors';
import { setClosedInfoBlocks, setUpdatesInfoBlockShown } from '../actions';
import { CLOSED_INFO_BLOCKS } from '../constants';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { ActivePage } from '../../../components/OrganizationContainer/types';

type ReturnValue = {
    goToPage: (page: ActivePage) => () => void;
    closeOneInfoBlock: (blockId: string) => (event: MouseEvent) => void;
    onUpdatesInfoBlockClick: () => void;
    closeUpdatesInfoBlock: (event: MouseEvent) => void;
    isUpdatesInfoBlockShown: boolean;
    t: TFunction;
};

export const useMainWindowInfoBlocks = (effects = true): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'main' });

    const dispatch = useDispatch();

    const { pushToPage } = useCustomRouter();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const closedInfoDataArray = useSelector(selectClosedInfoBlocks);
    const isUpdatesInfoBlockShown = useSelector(selectUpdatesInfoBlockShown);

    const goToPage = useCallback(
        (page: ActivePage) => () => {
            pushToPage(page);
        },
        [pushToPage],
    );

    const onUpdatesInfoBlockClick = useCallback(() => {
        window.open('https://phygitalsignage.io/#upd', '_blank');
    }, []);

    const blockIdToClosedBlockInfo = useCallback(
        (blockId: string) => (organizationId ? organizationId.slice(0, 5) + ':' + blockId : ''),
        [organizationId],
    );

    const closeOneInfoBlock = useCallback(
        (blockId: string) => (event: MouseEvent) => {
            event.stopPropagation();

            const closedInfoData = blockIdToClosedBlockInfo(blockId);
            const updatedInfoBlocks = [...closedInfoDataArray, closedInfoData];
            dispatch(setClosedInfoBlocks(closedInfoData));
            localStorage.setItem(CLOSED_INFO_BLOCKS, JSON.stringify(updatedInfoBlocks));
        },
        [blockIdToClosedBlockInfo, closedInfoDataArray, dispatch],
    );

    const closeUpdatesInfoBlock = useCallback(
        (event: MouseEvent) => {
            event.stopPropagation();

            dispatch(setUpdatesInfoBlockShown(false));
        },
        [dispatch],
    );

    useEffect(() => {
        if (!effects) return;

        // находим все карточки
        const infoBlocks = document.querySelectorAll('[id^=infoBlock]');

        // получаем массив id карточек
        const existedInfoBlocksIds = Array.from(infoBlocks).map((block) => block.id);

        for (const infoBlockId of existedInfoBlocksIds) {
            // id карточки с частью id организации
            const closedInfoData = blockIdToClosedBlockInfo(infoBlockId);

            // была ли карточка закрыта ранее
            const isBlockInfoNotClosed = closedInfoData && !closedInfoDataArray.includes(closedInfoData);

            // если карточка не была закрыта, то показываем её
            if (isBlockInfoNotClosed) {
                const block = document.getElementById(infoBlockId);
                if (block) {
                    block.style.display = 'block';
                }
            }
        }
    }, [blockIdToClosedBlockInfo, closedInfoDataArray, effects]);

    useEffect(() => {
        if (!effects) return;

        // находим все id закрытых карточек, принадлежащих этой организации
        const closedInfoIds = closedInfoDataArray
            .filter((info) => organizationId && info.startsWith(organizationId.slice(0, 5)))
            .map((info) => info.slice(6));

        // и все скрываем их
        for (const blockInfoId of closedInfoIds) {
            const block = document.getElementById(blockInfoId);
            if (block) {
                block.style.display = 'none';
            }
        }
    }, [closedInfoDataArray, effects, organizationId]);

    return {
        goToPage,
        closeOneInfoBlock,
        isUpdatesInfoBlockShown,
        onUpdatesInfoBlockClick,
        closeUpdatesInfoBlock,
        t,
    };
};
