import { CreateLocationDto } from '../../../../../server/src/models/location/dto/create-location.dto';
import { UpdateLocationDto } from '../../../../../server/src/models/location/dto/update-location.dto';
import { CreateScreenDto } from '../../../../../server/src/models/screen/dto/create-screen.dto';
import { UpdateScreenDto } from '../../../../../server/src/models/screen/dto/update-screen.dto';
import { CancellableResponse, ErrorResponse } from '../../../types';
import { LocationType, ScreenType, DeleteScreenTagDto } from '../../../../../common/types';
import { AddLocationSuccess } from '../types';
import { UpdateScreensDto } from '../../../../../server/src/models/screen/dto/update-screens.dto';

export const SET_ACTIVE_LOCATION_ID = 'SET_ACTIVE_LOCATION_ID';
export const SET_ACTIVE_SCREEN_ID = 'SET_ACTIVE_SCREEN_ID';

export const GET_LOCATIONS = 'GET_LOCATIONS';
export const GET_LOCATIONS_SUCCESS = 'GET_LOCATIONS_SUCCESS';
export const GET_LOCATIONS_FAIL = 'GET_LOCATIONS_FAIL';

export const UPDATE_LOCATION = 'UPDATE_LOCATION';
export const UPDATE_LOCATION_SUCCESS = 'UPDATE_LOCATION_SUCCESS';
export const UPDATE_LOCATION_FAIL = 'UPDATE_LOCATION_FAIL';

export const ADD_LOCATION = 'ADD_LOCATION';
export const ADD_LOCATION_SUCCESS = 'ADD_LOCATION_SUCCESS';
export const ADD_LOCATION_FAIL = 'ADD_LOCATION_FAIL';

export const DELETE_LOCATION = 'DELETE_LOCATION';
export const DELETE_LOCATION_SUCCESS = 'DELETE_LOCATION_SUCCESS';
export const DELETE_LOCATION_FAIL = 'DELETE_LOCATION_FAIL';

export const CLEAR_DELETE_LOCATION = 'CLEAR_DELETE_LOCATION';

export const GET_SCREENS = 'GET_SCREENS';
export const GET_SCREENS_SUCCESS = 'GET_SCREENS_SUCCESS';
export const GET_SCREENS_FAIL = 'GET_SCREENS_FAIL';

export const UPDATE_SCREEN = 'UPDATE_SCREEN';
export const UPDATE_SCREEN_SUCCESS = 'UPDATE_SCREEN_SUCCESS';
export const UPDATE_SCREEN_FAIL = 'UPDATE_SCREEN_FAIL';

export const UPDATE_SCREENS = 'UPDATE_SCREENS';
export const UPDATE_SCREENS_SUCCESS = 'UPDATE_SCREENS_SUCCESS';
export const UPDATE_SCREENS_FAIL = 'UPDATE_SCREENS_FAIL';

export const OPEN_ADD_SCREEN_MODAL = 'OPEN_ADD_SCREEN_MODAL';
export const CLOSE_ADD_SCREEN_MODAL = 'CLOSE_ADD_SCREEN_MODAL';

export const ADD_SCREEN = 'ADD_SCREEN';
export const ADD_SCREEN_SUCCESS = 'ADD_SCREEN_SUCCESS';
export const ADD_SCREEN_FAIL = 'ADD_SCREEN_FAIL';

export const DELETE_SCREENS = 'DELETE_SCREENS';
export const DELETE_SCREENS_SUCCESS = 'DELETE_SCREENS_SUCCESS';
export const DELETE_SCREENS_FAIL = 'DELETE_SCREENS_FAIL';

export const CLEAR_DELETE_SCREENS = 'CLEAR_DELETE_SCREENS';

export const DELETE_SCREEN_TAGS = 'DELETE_SCREEN_TAGS';
export const DELETE_SCREEN_TAGS_SUCCESS = 'DELETE_SCREEN_TAGS_SUCCESS';
export const DELETE_SCREEN_TAGS_FAIL = 'DELETE_SCREEN_TAGS_FAIL';

export const SET_SELECTED_SCREEN_IDS = 'SET_SELECTED_SCREEN_IDS';

export const CLEAR_SCREENS_SUCCESS = 'CLEAR_SCREENS_SUCCESS';
export const CLEAR_SCREENS_ERROR = 'CLEAR_SCREENS_ERROR';

export const CLOSE_ADD_SCREEN_SNACKBAR = 'CLOSE_ADD_SCREEN_SNACKBAR';

export const setActiveLocationId = (activeLocationId: string | 'all') =>
    ({ type: SET_ACTIVE_LOCATION_ID, payload: { activeLocationId } }) as const;
export const setActiveScreenId = (activeScreenId = '') =>
    ({ type: SET_ACTIVE_SCREEN_ID, payload: { activeScreenId } }) as const;

// запрос локаций
export const setGetLocations = (organizationId: string) =>
    ({ type: GET_LOCATIONS, payload: { organizationId } }) as const;
export const setGetLocationsSuccess = (locations: LocationType[]) =>
    ({
        type: GET_LOCATIONS_SUCCESS,
        payload: { locations },
    }) as const;
export const setGetLocationsFail = (errors: ErrorResponse) =>
    ({
        type: GET_LOCATIONS_FAIL,
        payload: { errors },
    }) as const;

// обновление локации
export const setUpdateLocation = (updateLocationDto: UpdateLocationDto) =>
    ({ type: UPDATE_LOCATION, payload: updateLocationDto }) as const;
export const setUpdateLocationSuccess = (id: string, updateLocationDto: UpdateLocationDto) =>
    ({ type: UPDATE_LOCATION_SUCCESS, payload: { id, ...updateLocationDto } }) as const;
export const setUpdateLocationFail = (errors: ErrorResponse) =>
    ({
        type: UPDATE_LOCATION_FAIL,
        payload: { errors },
    }) as const;

export const setAddLocation = (createLocationDto: CreateLocationDto) =>
    ({
        type: ADD_LOCATION,
        payload: createLocationDto,
    }) as const;
export const setAddLocationSuccess = (addLocationSuccess: AddLocationSuccess) =>
    ({ type: ADD_LOCATION_SUCCESS, payload: addLocationSuccess }) as const;
export const setAddLocationFail = (errors: ErrorResponse) =>
    ({ type: ADD_LOCATION_FAIL, payload: { errors } }) as const;

export const setDeleteLocation = (location: LocationType) =>
    ({ type: DELETE_LOCATION, payload: { location } }) as const;
export const setDeleteLocationSuccess = (response: CancellableResponse) =>
    ({
        type: DELETE_LOCATION_SUCCESS,
        payload: response,
    }) as const;
export const setDeleteLocationFail = (location: LocationType, errors: ErrorResponse) =>
    ({ type: DELETE_LOCATION_FAIL, payload: { location, reason: errors } }) as const;
export const setClearDeleteLocation = () => ({ type: CLEAR_DELETE_LOCATION }) as const;

// запрос экранов
export const setGetScreens = (organizationId: string, locationId: string, isAdmin: boolean) =>
    ({ type: GET_SCREENS, payload: { organizationId, locationId, isAdmin } }) as const;
export const setGetScreensSuccess = (screens: ScreenType[]) =>
    ({
        type: GET_SCREENS_SUCCESS,
        payload: { screens },
    }) as const;
export const setGetScreensFail = (errors: ErrorResponse) =>
    ({
        type: GET_SCREENS_FAIL,
        payload: { errors },
    }) as const;

// обновление экрана
export const setUpdateScreen = (screenId: string, updateScreenDto: UpdateScreenDto) =>
    ({
        type: UPDATE_SCREEN,
        payload: { screenId, ...updateScreenDto },
    }) as const;
export const setUpdateScreenSuccess = (id: string | string[]) =>
    ({ type: UPDATE_SCREEN_SUCCESS, payload: { id } }) as const;
export const setUpdateScreenFail = (errors: ErrorResponse) =>
    ({
        type: UPDATE_SCREEN_FAIL,
        payload: { errors },
    }) as const;

// обновление экранов
export const setUpdateScreens = (updateScreensDto: UpdateScreensDto) =>
    ({
        type: UPDATE_SCREENS,
        payload: { ...updateScreensDto },
    }) as const;
export const setUpdateScreensSuccess = (id: string | string[]) =>
    ({ type: UPDATE_SCREENS_SUCCESS, payload: { id } }) as const;
export const setUpdateScreensFail = (errors: ErrorResponse) =>
    ({
        type: UPDATE_SCREENS_FAIL,
        payload: { errors },
    }) as const;

// добавление экрана
export const setOpenAddScreenModal = () => ({ type: OPEN_ADD_SCREEN_MODAL }) as const;
export const setCloseAddScreenModal = () => ({ type: CLOSE_ADD_SCREEN_MODAL }) as const;

export const setAddScreen = (payload: CreateScreenDto) =>
    ({
        type: ADD_SCREEN,
        payload,
    }) as const;
export const setAddScreenSuccess = (id: string, payload: CreateScreenDto) =>
    ({ type: ADD_SCREEN_SUCCESS, payload: { id, ...payload } }) as const;
export const setAddScreenFail = (errors: ErrorResponse) => ({ type: ADD_SCREEN_FAIL, payload: { errors } }) as const;

// удаление экрана

export const setDeleteScreens = (screens: ScreenType[]) => ({ type: DELETE_SCREENS, payload: { screens } }) as const;
export const setDeleteScreensSuccess = (response: CancellableResponse) =>
    ({
        type: DELETE_SCREENS_SUCCESS,
        payload: response,
    }) as const;
export const setDeleteScreensFail = (screens: ScreenType[], errors: ErrorResponse) =>
    ({ type: DELETE_SCREENS_FAIL, payload: { screens, reason: errors } }) as const;
export const setClearDeleteScreens = () => ({ type: CLEAR_DELETE_SCREENS }) as const;

// удаление тега
export const setDeleteScreenTags = (deleteScreenTagDto: DeleteScreenTagDto) =>
    ({ type: DELETE_SCREEN_TAGS, payload: { deleteScreenTagDto } }) as const;
export const setDeleteScreenTagsSuccess = () => ({ type: DELETE_SCREEN_TAGS_SUCCESS }) as const;
export const setDeleteScreenTagsFail = (errors: ErrorResponse) =>
    ({
        type: DELETE_SCREEN_TAGS_FAIL,
        payload: { errors },
    }) as const;

// выделение экранов
export const setSelectedScreenIds = (selectedScreensIds: string[] = []) =>
    ({ type: SET_SELECTED_SCREEN_IDS, payload: { selectedScreensIds } }) as const;

export const setClearScreensSuccess = () => ({ type: CLEAR_SCREENS_SUCCESS }) as const;
export const setClearScreensError = () => ({ type: CLEAR_SCREENS_ERROR }) as const;

export const setHideAddScreenSnackbar = () => ({ type: CLOSE_ADD_SCREEN_SNACKBAR }) as const;
