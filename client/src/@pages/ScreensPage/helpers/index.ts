import { orientationsList, resolutionsList } from '../../../constants';
import { DataSuggestion } from '../../../resolvers/screensResolvers';
import { OrientationRusType, OrientationTypeEng } from '../types';
import { isProductionEngClient } from '../../../constants/environments';
import { OrganizationLanguageType, ScreenType, OrientationEnum } from '../../../../../common/types';
import { TWENTY_FOUR_HOURS_TIME_REGEX } from '../../PlaylistsPage/helpers';

type ReturnValue = {
    screensNumber: number;
    onlineScreensNumber: number;
    offlineScreensNumber: number;
    errorScreensNumber: number;
};

export const getScreensInfo = (screens: ScreenType[]): ReturnValue => {
    const screensNumber = screens.length;
    const onlineScreensNumber = screens.filter(({ status }) => status === 'online').length;
    const offlineScreensNumber = screens.filter(({ status }) => ['offline', 'pending'].includes(status)).length;
    const errorScreensNumber = screens.filter(({ status }) => status === 'error').length;

    return {
        screensNumber,
        onlineScreensNumber,
        offlineScreensNumber,
        errorScreensNumber,
    };
};

export const formatScreenResolution = (screenResolution: {
    width: number;
    height: number;
}): { formattedScreenResolution: string; otherScreenResolutions: string[] } => {
    const formattedScreenResolution =
        resolutionsList.find(
            (resolution) =>
                resolution.includes(screenResolution?.width?.toString()) &&
                resolution.includes(screenResolution?.height?.toString()),
        ) ?? `${screenResolution?.width} × ${screenResolution?.height}`;
    const otherScreenResolutions = resolutionsList.filter((resolution) => resolution !== formattedScreenResolution);

    return {
        formattedScreenResolution,
        otherScreenResolutions,
    };
};

export const fromDbToSelectOrientationValue = (
    orientation: OrientationEnum,
    language: OrganizationLanguageType,
): OrientationRusType | OrientationTypeEng => {
    const horizontal = language === 'en' ? "Don't turn" : 'Не поворачивать';
    const portrait = '90°';
    const horizontalFlipped = '180°';
    const portraitFlipped = '270°';

    switch (orientation) {
        case OrientationEnum.LANDSCAPE:
            return horizontal;
        case OrientationEnum.PORTRAIT:
            return portrait;
        case OrientationEnum['LANDSCAPE_FLIPPED']:
            return horizontalFlipped;
        case OrientationEnum['PORTRAIT_FLIPPED']:
            return portraitFlipped;
        default:
            return horizontal;
    }
};

export const formatScreenOrientation = (
    screenOrientation: OrientationEnum,
    language: OrganizationLanguageType,
): {
    formattedScreenOrientation: OrientationRusType | OrientationTypeEng;
    otherScreenOrientations: (OrientationRusType | OrientationTypeEng)[];
} => {
    const formattedScreenOrientation = fromDbToSelectOrientationValue(screenOrientation, language);
    const otherScreenOrientations = orientationsList
        .filter((orientation) => orientation !== screenOrientation)
        .map((orientation) => fromDbToSelectOrientationValue(orientation, language));

    return {
        formattedScreenOrientation,
        otherScreenOrientations,
    };
};

export type ScreenTime = { hours: number; minutes: number };

export const screenTimeToString = (
    startTime: ScreenTime | undefined,
    endTime: ScreenTime | undefined,
): { startTimeValue: string; endTimeValue: string } => {
    let startTimeValue = '';
    let endTimeValue = '';

    if (startTime && endTime) {
        const toString = (time: number): string => (time < 10 ? `0${time}` : time.toString());

        startTimeValue = `${toString(startTime.hours)}:${toString(startTime.minutes)}`;
        endTimeValue = `${toString(endTime.hours)}:${toString(endTime.minutes)}`;
    }

    return {
        startTimeValue,
        endTimeValue,
    };
};

export const timeStringToTimeObject = (timeValue: string): ScreenTime | undefined => {
    const matched = timeValue.match(TWENTY_FOUR_HOURS_TIME_REGEX);
    if (matched) {
        const hours = Number(matched[1]);
        const minutes = Number(matched[2]);
        return { hours, minutes };
    }
};

type OptionNameType = {
    fullName: string;
    startNamePart: string;
    middleNamePart: string;
    lastNamePart: string;
};

export const reduceAddressQuery = (value: string, valueAddition: string): string => {
    if (!isProductionEngClient && !valueAddition) return value;

    const searchValue = valueAddition.split(', ')[0];
    const index = value.search(searchValue) - 2;
    return value.slice(0, index);
};

export const getSuggestionName = (
    suggestion: DataSuggestion,
    queryValue: string,
    queryAddition: string,
): OptionNameType => {
    let { value } = suggestion;

    value = reduceAddressQuery(value, queryAddition);

    const queryIndex = value.toLowerCase().search(queryValue.trim().toLowerCase());

    const isQueryValueFound = queryIndex !== -1;

    const startNamePart = isQueryValueFound ? value.slice(0, queryIndex) : '';
    const middleNamePart = isQueryValueFound ? value.slice(queryIndex, queryIndex + queryValue.length) : '';
    const lastNamePart = isQueryValueFound ? value.slice(queryIndex + queryValue.length) : value;

    return {
        fullName: value,
        startNamePart,
        middleNamePart,
        lastNamePart,
    };
};

export const openTextLink = (filename: string, text: string): void => {
    const downloadLink = document.createElement('a');
    downloadLink.style.display = 'none';
    downloadLink.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    downloadLink.setAttribute('download', filename);

    document.body.appendChild(downloadLink);

    downloadLink.click();

    document.body.removeChild(downloadLink);
};
