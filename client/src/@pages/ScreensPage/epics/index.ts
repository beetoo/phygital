import { combineEpics, ofType, StateObservable } from 'redux-observable';
import { Observable, of, from } from 'rxjs';
import { catchError, map, mergeMap, withLatestFrom } from 'rxjs/operators';

import { isCancellableResponse } from '../../../helpers/httpService';
import {
    fromAddLocation,
    fromAddScreens,
    fromDeleteLocation,
    fromDeleteScreens,
    fromDeleteScreenTags,
    fromGetLocations,
    fromGetScreens,
    fromUpdateLocation,
    fromUpdateScreen,
    fromUpdateScreens,
} from '../../../resolvers/screensResolvers';

import { StoreType } from '../../../types';
import {
    ADD_LOCATION,
    ADD_LOCATION_SUCCESS,
    ADD_SCREEN,
    ADD_SCREEN_SUCCESS,
    DELETE_LOCATION,
    DELETE_SCREEN_TAGS,
    DELETE_SCREEN_TAGS_SUCCESS,
    DELETE_SCREENS,
    GET_LOCATIONS,
    GET_SCREENS,
    setAddLocationFail,
    setAddLocationSuccess,
    setAddScreenFail,
    setAddScreenSuccess,
    setDeleteLocationFail,
    setDeleteLocationSuccess,
    setDeleteScreensFail,
    setDeleteScreensSuccess,
    setDeleteScreenTagsFail,
    setDeleteScreenTagsSuccess,
    setGetLocations,
    setGetLocationsFail,
    setGetLocationsSuccess,
    setGetScreens,
    setGetScreensFail,
    setGetScreensSuccess,
    setUpdateLocationFail,
    setUpdateLocationSuccess,
    setUpdateScreenFail,
    setUpdateScreensFail,
    setUpdateScreensSuccess,
    setUpdateScreenSuccess,
    UPDATE_LOCATION,
    UPDATE_LOCATION_SUCCESS,
    UPDATE_SCREEN,
    UPDATE_SCREEN_SUCCESS,
    UPDATE_SCREENS,
    UPDATE_SCREENS_SUCCESS,
} from '../actions';

import {
    AddLocationAction,
    AddLocationFailAction,
    AddLocationSuccessAction,
    AddScreenAction,
    AddScreenFailAction,
    AddScreenSuccessAction,
    DeleteLocationAction,
    DeleteLocationFailAction,
    DeleteLocationSuccessAction,
    DeleteScreensAction,
    DeleteScreensFailAction,
    DeleteScreensSuccessAction,
    DeleteScreenTagsAction,
    DeleteScreenTagsFailAction,
    DeleteScreenTagsSuccessAction,
    GetLocationsAction,
    GetLocationsFailAction,
    GetLocationsSuccessAction,
    GetScreensAction,
    GetScreensFailAction,
    GetScreensSuccessAction,
    UpdateLocationAction,
    UpdateLocationFailAction,
    UpdateLocationSuccessAction,
    UpdateScreenAction,
    UpdateScreenFailAction,
    UpdateScreensAction,
    UpdateScreensFailAction,
    UpdateScreensSuccessAction,
    UpdateScreenSuccessAction,
} from '../types';
import { AddPlaylistSuccessAction, UpdatePlaylistSuccessAction } from '../../PlaylistsPage/types';
import { ADD_PLAYLIST_SUCCESS, UPDATE_PLAYLIST_SUCCESS } from '../../PlaylistsPage/actions';
import { USER_ROLE } from '../../../constants';
import { setGetOrganization } from '../../ProfilePage/actions';
import { GetOrganizationAction } from '../../ProfilePage/types';

const getLocationsEpic = (
    action$: Observable<GetLocationsAction>,
): Observable<GetLocationsSuccessAction | GetLocationsFailAction> =>
    action$.pipe(
        ofType(GET_LOCATIONS),
        mergeMap(({ payload: { organizationId } }) =>
            fromGetLocations(organizationId).pipe(
                map(setGetLocationsSuccess),
                catchError((error) => of(setGetLocationsFail(error))),
            ),
        ),
    );

const addLocationEpic = (
    action$: Observable<AddLocationAction>,
): Observable<AddLocationSuccessAction | AddLocationFailAction> =>
    action$.pipe(
        ofType(ADD_LOCATION),
        mergeMap(({ payload }) =>
            fromAddLocation(payload).pipe(
                map(setAddLocationSuccess),
                catchError((error) => of(setAddLocationFail(error))),
            ),
        ),
    );

const updateLocationEpic = (
    action$: Observable<UpdateLocationAction>,
): Observable<UpdateLocationSuccessAction | UpdateLocationFailAction> =>
    action$.pipe(
        ofType(UPDATE_LOCATION),
        mergeMap(({ payload }) =>
            fromUpdateLocation(payload).pipe(
                map(({ id }: { id: string }) => setUpdateLocationSuccess(id, payload)),
                catchError((error) => of(setUpdateLocationFail(error))),
            ),
        ),
    );

const deleteLocationEpic = (
    action$: Observable<DeleteLocationAction>,
): Observable<DeleteLocationSuccessAction | DeleteLocationFailAction> =>
    action$.pipe(
        ofType(DELETE_LOCATION),
        mergeMap(({ payload: { location } }) =>
            fromDeleteLocation(location).pipe(
                map((response) => {
                    if (!isCancellableResponse(response)) {
                        throw new Error('Wrong cancellable response body');
                    }
                    return setDeleteLocationSuccess(response);
                }),
                catchError((error) => of(setDeleteLocationFail(location, error))),
            ),
        ),
    );

const getScreensEpic = (
    action$: Observable<GetScreensAction>,
): Observable<GetScreensSuccessAction | GetScreensFailAction> =>
    action$.pipe(
        ofType(GET_SCREENS),
        mergeMap(({ payload: { organizationId, locationId, isAdmin } }) =>
            fromGetScreens(organizationId, locationId, isAdmin).pipe(
                map(setGetScreensSuccess),
                catchError((error) => of(setGetScreensFail(error))),
            ),
        ),
    );

const updateScreenEpic = (
    action$: Observable<UpdateScreenAction>,
): Observable<UpdateScreenSuccessAction | UpdateScreenFailAction> =>
    action$.pipe(
        ofType(UPDATE_SCREEN),
        mergeMap(({ payload: { screenId, ...rest } }) =>
            fromUpdateScreen(screenId, { ...rest }).pipe(
                map(({ id }: { id: string }) => setUpdateScreenSuccess(id)),
                catchError((error) => of(setUpdateScreenFail(error))),
            ),
        ),
    );

const updateScreensEpic = (
    action$: Observable<UpdateScreensAction>,
): Observable<UpdateScreensSuccessAction | UpdateScreensFailAction> =>
    action$.pipe(
        ofType(UPDATE_SCREENS),
        mergeMap(({ payload: { screenIds, ...rest } }) =>
            fromUpdateScreens({ screenIds, ...rest }).pipe(
                map(({ id }: { id: string }) => setUpdateScreensSuccess(id)),
                catchError((error) => of(setUpdateScreensFail(error))),
            ),
        ),
    );

const addScreenEpic = (
    action$: Observable<AddScreenAction>,
): Observable<AddScreenSuccessAction | AddScreenFailAction> =>
    action$.pipe(
        ofType(ADD_SCREEN),
        mergeMap(({ payload }) =>
            fromAddScreens(payload).pipe(
                map(({ id }: { id: string }) => setAddScreenSuccess(id, payload)),
                catchError((error) => of(setAddScreenFail(error))),
            ),
        ),
    );

const deleteScreensEpic = (
    action$: Observable<DeleteScreensAction>,
): Observable<DeleteScreensSuccessAction | DeleteScreensFailAction> =>
    action$.pipe(
        ofType(DELETE_SCREENS),
        mergeMap(({ payload: { screens } }) =>
            fromDeleteScreens(screens).pipe(
                map((response) => {
                    if (!isCancellableResponse(response)) {
                        throw new Error('Wrong cancellable response body');
                    }
                    return setDeleteScreensSuccess(response);
                }),
                catchError((error) => of(setDeleteScreensFail(screens, error))),
            ),
        ),
    );

const deleteScreenTagsEpic = (
    action$: Observable<DeleteScreenTagsAction>,
): Observable<DeleteScreenTagsSuccessAction | DeleteScreenTagsFailAction> =>
    action$.pipe(
        ofType(DELETE_SCREEN_TAGS),
        mergeMap(({ payload: { deleteScreenTagDto } }) =>
            fromDeleteScreenTags(deleteScreenTagDto).pipe(
                map(setDeleteScreenTagsSuccess),
                catchError((error) => of(setDeleteScreenTagsFail(error))),
            ),
        ),
    );

const getLocationsOnSuccessEpic = (
    action$: Observable<
        | AddLocationSuccessAction
        | UpdateLocationSuccessAction
        | AddScreenSuccessAction
        | UpdateScreenSuccessAction
        | UpdateScreensSuccessAction
        | DeleteScreenTagsSuccessAction
    >,
    state$: StateObservable<StoreType>,
): Observable<GetLocationsAction> =>
    action$.pipe(
        ofType(
            ADD_LOCATION_SUCCESS,
            UPDATE_LOCATION_SUCCESS,
            ADD_SCREEN_SUCCESS,
            UPDATE_SCREEN_SUCCESS,
            UPDATE_SCREENS_SUCCESS,
            DELETE_SCREEN_TAGS_SUCCESS,
        ),
        withLatestFrom(state$),
        map(
            ([
                ,
                {
                    pages: {
                        organization: {
                            profile: {
                                organization: { id },
                            },
                        },
                    },
                },
            ]) => setGetLocations(id),
        ),
    );

const getScreensOnSuccessEpic = (
    action$: Observable<
        | AddScreenSuccessAction
        | UpdateScreenSuccessAction
        | UpdateScreensSuccessAction
        | DeleteScreenTagsSuccessAction
        | AddPlaylistSuccessAction
        | UpdatePlaylistSuccessAction
        | DeleteScreensSuccessAction
    >,
    state$: StateObservable<StoreType>,
): Observable<GetScreensAction | GetOrganizationAction> =>
    action$.pipe(
        ofType(
            ADD_SCREEN_SUCCESS,
            UPDATE_SCREEN_SUCCESS,
            UPDATE_SCREENS_SUCCESS,
            DELETE_SCREEN_TAGS_SUCCESS,
            ADD_PLAYLIST_SUCCESS,
            UPDATE_PLAYLIST_SUCCESS,
        ),
        withLatestFrom(state$),
        mergeMap(
            ([
                ,
                {
                    auth: { role },
                    pages: {
                        organization: {
                            profile: {
                                organization: { id },
                            },
                        },
                    },
                },
            ]) => from([setGetScreens(id, 'all', role === USER_ROLE.ADMIN), setGetOrganization(id)]),
        ),
    );

type EpicsActions =
    | GetLocationsAction
    | GetLocationsSuccessAction
    | GetLocationsFailAction
    | UpdateLocationAction
    | UpdateLocationSuccessAction
    | UpdateLocationFailAction
    | AddLocationAction
    | AddLocationSuccessAction
    | AddLocationFailAction
    | DeleteLocationAction
    | DeleteLocationSuccessAction
    | DeleteLocationFailAction
    | GetScreensAction
    | GetScreensSuccessAction
    | GetScreensFailAction
    | UpdateScreenAction
    | UpdateScreenSuccessAction
    | UpdateScreenFailAction
    | UpdateScreensAction
    | UpdateScreensSuccessAction
    | UpdateScreensFailAction
    | AddScreenAction
    | AddScreenSuccessAction
    | AddScreenFailAction
    | DeleteScreensAction
    | DeleteScreensSuccessAction
    | DeleteScreensFailAction
    | DeleteScreenTagsAction
    | DeleteScreenTagsSuccessAction
    | DeleteScreenTagsFailAction
    | AddPlaylistSuccessAction
    | UpdatePlaylistSuccessAction
    | GetOrganizationAction;

export const screensEpics = combineEpics<EpicsActions, EpicsActions, StoreType>(
    getLocationsEpic,
    updateLocationEpic,
    addLocationEpic,
    deleteLocationEpic,
    getScreensEpic,
    updateScreenEpic,
    updateScreensEpic,
    addScreenEpic,
    deleteScreensEpic,
    deleteScreenTagsEpic,
    getLocationsOnSuccessEpic,
    getScreensOnSuccessEpic,
);
