import { createSelector } from 'reselect';

import { ErrorResponse, StoreType } from '../../../types';
import { AddLocationSuccess, AddScreenSuccess, UpdateLocationSuccess } from '../types';
import { LocationType, ScreenType } from '../../../../../common/types';

export const selectActiveLocationId = ({
    pages: {
        organization: {
            screens: { activeLocationId },
        },
    },
}: StoreType): string => activeLocationId;

export const selectActiveLocation = createSelector(
    (state: StoreType) => state.pages.organization.screens,
    ({ activeLocationId, locations }) =>
        locations.find((location) => location?.id === activeLocationId) ?? {
            id: '',
            organizationId: '',
            name: '',
            city: '',
            address: '',
            geo_lat: '',
            geo_lon: '',
            tzLabel: '',
            screens: [],
            tzOffset: 0,
            createdAt: new Date(),
        },
);

export const selectActiveScreenId = ({
    pages: {
        organization: {
            screens: { activeScreenId },
        },
    },
}: StoreType): string => activeScreenId;

export const selectActiveScreen = createSelector(
    (state: StoreType) => state.pages.organization.screens,
    ({ activeScreenId, screensById }) =>
        screensById[activeScreenId] ?? {
            id: '',
            organizationId: '',
            name: '',
            status: '',
            resolution: { width: 0, height: 0 },
            orientation: '',
            startTime: { hours: 0, minutes: 0 },
            endTime: { hours: 0, minutes: 0 },
            availableBytes: 0,
            totalBytes: 0,
            tags: [],
            location: { name: '', city: '', address: '' },
            schedules: [],
            log: undefined,
            verificationHash: '',
            hasHistory: false,
            hasMessages: false,
        },
);

// получение локаций
export const selectLocations = ({
    pages: {
        organization: {
            screens: { locations },
        },
    },
}: StoreType): LocationType[] => locations;

// добавление локации

export const selectAddLocationSuccessStatus = ({
    pages: {
        organization: {
            screens: { addLocationSuccess },
        },
    },
}: StoreType): AddLocationSuccess | false => addLocationSuccess;

export const selectAddingLocationStatus = ({
    pages: {
        organization: {
            screens: { addingLocation },
        },
    },
}: StoreType): boolean => addingLocation;

export const selectAddedLocationId = ({
    pages: {
        organization: {
            screens: { addedLocationId },
        },
    },
}: StoreType): string => addedLocationId;

export const selectAddingLocationErrorMessage = ({
    pages: {
        organization: {
            screens: { addLocationError },
        },
    },
}: StoreType): string | any => (addLocationError ? addLocationError.message : '');

// обновление локации
export const selectUpdatingLocationStatus = ({
    pages: {
        organization: {
            screens: { updatingLocation },
        },
    },
}: StoreType): boolean => updatingLocation;

export const selectUpdateLocationSuccess = ({
    pages: {
        organization: {
            screens: { updateLocationSuccess },
        },
    },
}: StoreType): UpdateLocationSuccess | false => updateLocationSuccess;

// удаление локации
export const selectDeleteLocationStatus = ({
    pages: {
        organization: {
            screens: { deleteLocation },
        },
    },
}: StoreType): boolean => !!deleteLocation;

export const selectDeleteLocationSuccessStatus = ({
    pages: {
        organization: {
            screens: { deleteLocationSuccess },
        },
    },
}: StoreType): { location: LocationType; cancelId: string; timeout: number } | false => deleteLocationSuccess;

export const selectDeleteLocationErrorMessage = ({
    pages: {
        organization: {
            screens: { deleteLocationError },
        },
    },
}: StoreType):
    | {
          reason: ErrorResponse;
          location: LocationType;
      }
    | false => deleteLocationError;

// получение экранов
export const selectScreens = createSelector(
    (state: StoreType) => state.pages.organization.screens,
    ({ screensById }) => Object.values(screensById),
);

// добавление экрана
export const selectAddScreenModalOpen = ({
    pages: {
        organization: {
            screens: { isAddScreenModalOpen },
        },
    },
}: StoreType): boolean => isAddScreenModalOpen;

export const selectAddingScreenStatus = ({
    pages: {
        organization: {
            screens: { addingScreen },
        },
    },
}: StoreType): boolean => addingScreen;

export const selectAddScreenSuccessStatus = ({
    pages: {
        organization: {
            screens: { addScreenSuccess },
        },
    },
}: StoreType): AddScreenSuccess | false => addScreenSuccess;

export const selectAddingScreenErrorMessage = ({
    pages: {
        organization: {
            screens: { addScreenError },
        },
    },
}: StoreType): string | any => {
    const hashErrors = ['Resource not found - Device verification was not found by hash', 'Screen verification failed'];
    if (addScreenError && addScreenError.message) {
        if (hashErrors.includes(addScreenError.message as string)) {
            return 'Некорректный хэш';
        } else return addScreenError.message;
    }
    return '';
};

// обновление экрана
export const selectUpdateScreenStatus = ({
    pages: {
        organization: {
            screens: { updateScreen },
        },
    },
}: StoreType): boolean => updateScreen;

export const selectUpdateScreenIds = ({
    pages: {
        organization: {
            screens: { updateScreenIds },
        },
    },
}: StoreType): string | string[] | null => updateScreenIds;

export const selectUpdateScreenSuccessStatus = ({
    pages: {
        organization: {
            screens: { updateScreenSuccess },
        },
    },
}: StoreType): { id: string | string[] } | false => updateScreenSuccess;

export const selectUpdateScreenError = ({
    pages: {
        organization: {
            screens: { updateScreenError },
        },
    },
}: StoreType): ErrorResponse | null => updateScreenError;

// удаление экрана

export const selectDeleteScreenStatus = ({
    pages: {
        organization: {
            screens: { deleteScreen },
        },
    },
}: StoreType): ScreenType[] | false => deleteScreen;

export const selectDeleteScreenSuccessStatus = ({
    pages: {
        organization: {
            screens: { deleteScreenSuccess },
        },
    },
}: StoreType):
    | { screens: ScreenType[]; cancelId: string; timeout: number; offlineScreenIds: false | string[] }
    | false => deleteScreenSuccess;

export const selectDeleteScreenErrorMessage = ({
    pages: {
        organization: {
            screens: { deleteScreenError },
        },
    },
}: StoreType): { screens: ScreenType[]; reason: ErrorResponse } | false => deleteScreenError;

// выделение экранов
export const selectSelectedScreensIds = ({
    pages: {
        organization: {
            screens: { selectedScreensIds },
        },
    },
}: StoreType): string[] => selectedScreensIds;

export const selectSelectedScreens = createSelector(
    (store: StoreType) => store.pages.organization.screens,
    ({ selectedScreensIds, screensById }) =>
        selectedScreensIds
            .map((selectedScreenId) => screensById[selectedScreenId])
            .filter((screen: ScreenType | undefined): screen is ScreenType => Boolean(screen)),
);

// добавление тега
export const selectAddingScreenTagStatus = ({
    pages: {
        organization: {
            screens: { addingScreenTag },
        },
    },
}: StoreType): boolean => addingScreenTag;

export const selectDeleteScreenTagSuccessStatus = ({
    pages: {
        organization: {
            screens: { isDeleteScreenTagSuccess },
        },
    },
}: StoreType): boolean => isDeleteScreenTagSuccess;

export const selectEasyOnboardingModalStatus = ({
    pages: {
        main: { isEasyOnboardingModalOpen },
    },
}: StoreType): boolean => isEasyOnboardingModalOpen;
