import { LocationType, ScreenType } from '../../../../../common/types';
import { ErrorResponse } from '../../../types';
import {
    setActiveLocationId,
    setActiveScreenId,
    setAddLocation,
    setAddLocationFail,
    setAddLocationSuccess,
    setAddScreen,
    setAddScreenFail,
    setAddScreenSuccess,
    setClearDeleteLocation,
    setClearDeleteScreens,
    setClearScreensError,
    setClearScreensSuccess,
    setCloseAddScreenModal,
    setDeleteLocation,
    setDeleteLocationFail,
    setDeleteLocationSuccess,
    setDeleteScreens,
    setDeleteScreensFail,
    setDeleteScreensSuccess,
    setDeleteScreenTags,
    setDeleteScreenTagsFail,
    setDeleteScreenTagsSuccess,
    setGetLocations,
    setGetLocationsFail,
    setGetLocationsSuccess,
    setGetScreens,
    setGetScreensFail,
    setGetScreensSuccess,
    setHideAddScreenSnackbar,
    setOpenAddScreenModal,
    setSelectedScreenIds,
    setUpdateLocation,
    setUpdateLocationFail,
    setUpdateLocationSuccess,
    setUpdateScreen,
    setUpdateScreenFail,
    setUpdateScreens,
    setUpdateScreensFail,
    setUpdateScreensSuccess,
    setUpdateScreenSuccess,
} from '../actions';
import { CreateScreenDto } from '../../../../../server/src/models/screen/dto/create-screen.dto';
import { CreateLocationDto } from '../../../../../server/src/models/location/dto/create-location.dto';
import { UpdateLocationDto } from '../../../../../server/src/models/location/dto/update-location.dto';

export class AddLocationSuccess extends CreateLocationDto {
    id: string;
    name: string;
}

export class UpdateLocationSuccess extends UpdateLocationDto {
    id: string;
}

export class AddScreenSuccess extends CreateScreenDto {
    id: string;
}

export type OrientationRusType = 'Не поворачивать' | '90°' | '180°' | '270°';

export type OrientationTypeEng = "Don't turn" | '90°' | '180°' | '270°';
export type OrientationTypeEs = 'No girar' | '90°' | '180°' | '270°';

export type ScreensPageState = {
    activeLocationId: string;
    activeScreenId: string;

    loadingLocations: boolean;
    locations: LocationType[];
    loadingLocationsError: ErrorResponse | null;

    updatingLocation: boolean;
    updateLocationSuccess: UpdateLocationSuccess | false;
    updatingLocationError: ErrorResponse | null;

    addLocationSuccess: AddLocationSuccess | false;
    addingLocation: boolean;
    addedLocationId: string;
    addLocationError: ErrorResponse | null;

    deleteLocation: false | LocationType;
    deleteLocationSuccess: { location: LocationType; cancelId: string; timeout: number } | false;
    deleteLocationError:
        | false
        | {
              reason: ErrorResponse;
              location: LocationType;
          };

    loadingScreens: boolean;
    screensById: Record<string /*id*/, ScreenType>;
    loadingScreensError: ErrorResponse | null;

    updateScreen: boolean;
    updateScreenIds: string | string[] | null;
    updateScreenSuccess: { id: string | string[] } | false;
    updateScreenError: ErrorResponse | null;

    isAddScreenModalOpen: boolean;

    addingScreen: boolean;
    addScreenSuccess: AddScreenSuccess | false;
    addScreenError: ErrorResponse | null;

    deleteScreen: ScreenType[] | false;
    deleteScreenSuccess:
        | { screens: ScreenType[]; cancelId: string; timeout: number; offlineScreenIds: false | string[] }
        | false;
    deleteScreenError: { screens: ScreenType[]; reason: ErrorResponse } | false;

    addingScreenTag: boolean;
    deleteScreenTag: boolean;
    isDeleteScreenTagSuccess: boolean;
    deleteScreenTagError: ErrorResponse | null;

    selectedScreensIds: string[];
};

export type SetActiveLocationId = ReturnType<typeof setActiveLocationId>;
export type SetActiveScreenId = ReturnType<typeof setActiveScreenId>;

export type GetLocationsAction = ReturnType<typeof setGetLocations>;
export type GetLocationsSuccessAction = ReturnType<typeof setGetLocationsSuccess>;
export type GetLocationsFailAction = ReturnType<typeof setGetLocationsFail>;

export type UpdateLocationAction = ReturnType<typeof setUpdateLocation>;
export type UpdateLocationSuccessAction = ReturnType<typeof setUpdateLocationSuccess>;
export type UpdateLocationFailAction = ReturnType<typeof setUpdateLocationFail>;

export type AddLocationAction = ReturnType<typeof setAddLocation>;
export type AddLocationSuccessAction = ReturnType<typeof setAddLocationSuccess>;
export type AddLocationFailAction = ReturnType<typeof setAddLocationFail>;

export type DeleteLocationAction = ReturnType<typeof setDeleteLocation>;
export type DeleteLocationSuccessAction = ReturnType<typeof setDeleteLocationSuccess>;
export type DeleteLocationFailAction = ReturnType<typeof setDeleteLocationFail>;
export type ClearDeleteLocationAction = ReturnType<typeof setClearDeleteLocation>;

export type GetScreensAction = ReturnType<typeof setGetScreens>;
export type GetScreensSuccessAction = ReturnType<typeof setGetScreensSuccess>;
export type GetScreensFailAction = ReturnType<typeof setGetScreensFail>;

export type UpdateScreenAction = ReturnType<typeof setUpdateScreen>;
export type UpdateScreenSuccessAction = ReturnType<typeof setUpdateScreenSuccess>;
export type UpdateScreenFailAction = ReturnType<typeof setUpdateScreenFail>;

export type UpdateScreensAction = ReturnType<typeof setUpdateScreens>;
export type UpdateScreensSuccessAction = ReturnType<typeof setUpdateScreensSuccess>;
export type UpdateScreensFailAction = ReturnType<typeof setUpdateScreensFail>;

export type OpenAddScreenModalAction = ReturnType<typeof setOpenAddScreenModal>;
export type CloseAddScreenModalAction = ReturnType<typeof setCloseAddScreenModal>;

export type AddScreenAction = ReturnType<typeof setAddScreen>;
export type AddScreenSuccessAction = ReturnType<typeof setAddScreenSuccess>;
export type AddScreenFailAction = ReturnType<typeof setAddScreenFail>;

export type DeleteScreensAction = ReturnType<typeof setDeleteScreens>;
export type DeleteScreensSuccessAction = ReturnType<typeof setDeleteScreensSuccess>;
export type DeleteScreensFailAction = ReturnType<typeof setDeleteScreensFail>;
export type ClearDeleteScreensAction = ReturnType<typeof setClearDeleteScreens>;

export type DeleteScreenTagsAction = ReturnType<typeof setDeleteScreenTags>;
export type DeleteScreenTagsSuccessAction = ReturnType<typeof setDeleteScreenTagsSuccess>;
export type DeleteScreenTagsFailAction = ReturnType<typeof setDeleteScreenTagsFail>;

export type SetSelectedScreenIdsAction = ReturnType<typeof setSelectedScreenIds>;

export type ClearScreensSuccessAction = ReturnType<typeof setClearScreensSuccess>;
export type ClearScreensErrorAction = ReturnType<typeof setClearScreensError>;

export type HideAddScreenSnackbarAction = ReturnType<typeof setHideAddScreenSnackbar>;

export type ScreensAction =
    | SetActiveLocationId
    | SetActiveScreenId
    | GetLocationsAction
    | GetLocationsSuccessAction
    | GetLocationsFailAction
    | UpdateLocationAction
    | UpdateLocationSuccessAction
    | UpdateLocationFailAction
    | AddLocationAction
    | AddLocationSuccessAction
    | AddLocationFailAction
    | DeleteLocationAction
    | DeleteLocationSuccessAction
    | DeleteLocationFailAction
    | ClearDeleteLocationAction
    | GetScreensAction
    | GetScreensSuccessAction
    | GetScreensFailAction
    | UpdateScreenAction
    | UpdateScreenSuccessAction
    | UpdateScreenFailAction
    | UpdateScreensAction
    | UpdateScreensSuccessAction
    | UpdateScreensFailAction
    | OpenAddScreenModalAction
    | CloseAddScreenModalAction
    | AddScreenAction
    | AddScreenSuccessAction
    | AddScreenFailAction
    | DeleteScreensAction
    | DeleteScreensSuccessAction
    | DeleteScreensFailAction
    | ClearDeleteScreensAction
    | DeleteScreenTagsAction
    | DeleteScreenTagsSuccessAction
    | DeleteScreenTagsFailAction
    | SetSelectedScreenIdsAction
    | ClearScreensSuccessAction
    | ClearScreensErrorAction
    | HideAddScreenSnackbarAction;
