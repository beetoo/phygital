import { ScreenType } from '../../../../../common/types';
import {
    ADD_LOCATION,
    ADD_LOCATION_FAIL,
    ADD_LOCATION_SUCCESS,
    ADD_SCREEN,
    ADD_SCREEN_FAIL,
    ADD_SCREEN_SUCCESS,
    CLEAR_DELETE_LOCATION,
    CLEAR_DELETE_SCREENS,
    CLEAR_SCREENS_ERROR,
    CLEAR_SCREENS_SUCCESS,
    CLOSE_ADD_SCREEN_MODAL,
    CLOSE_ADD_SCREEN_SNACKBAR,
    DELETE_LOCATION,
    DELETE_LOCATION_FAIL,
    DELETE_LOCATION_SUCCESS,
    DELETE_SCREEN_TAGS,
    DELETE_SCREEN_TAGS_FAIL,
    DELETE_SCREEN_TAGS_SUCCESS,
    DELETE_SCREENS,
    DELETE_SCREENS_FAIL,
    DELETE_SCREENS_SUCCESS,
    GET_LOCATIONS,
    GET_LOCATIONS_FAIL,
    GET_LOCATIONS_SUCCESS,
    GET_SCREENS,
    GET_SCREENS_FAIL,
    GET_SCREENS_SUCCESS,
    OPEN_ADD_SCREEN_MODAL,
    SET_ACTIVE_LOCATION_ID,
    SET_ACTIVE_SCREEN_ID,
    SET_SELECTED_SCREEN_IDS,
    UPDATE_LOCATION,
    UPDATE_LOCATION_FAIL,
    UPDATE_LOCATION_SUCCESS,
    UPDATE_SCREEN,
    UPDATE_SCREEN_FAIL,
    UPDATE_SCREEN_SUCCESS,
    UPDATE_SCREENS,
    UPDATE_SCREENS_FAIL,
    UPDATE_SCREENS_SUCCESS,
} from '../actions';

import { ScreensAction, ScreensPageState } from '../types';

const screensPageInitialState: ScreensPageState = {
    activeLocationId: 'all',
    activeScreenId: '',

    loadingLocations: false,
    locations: [],
    loadingLocationsError: null,

    updatingLocation: false,
    updateLocationSuccess: false,
    updatingLocationError: null,

    deleteLocation: false,
    deleteLocationSuccess: false,
    deleteLocationError: false,

    addLocationSuccess: false,
    addingLocation: false,
    addedLocationId: '',
    addLocationError: null,

    loadingScreens: false,
    screensById: {},
    loadingScreensError: null,

    updateScreen: false,
    updateScreenIds: null,
    updateScreenError: null,
    updateScreenSuccess: false,

    isAddScreenModalOpen: false,
    addingScreen: false,
    addScreenSuccess: false,
    addScreenError: null,

    deleteScreen: false,
    deleteScreenSuccess: false,
    deleteScreenError: false,

    addingScreenTag: false,
    deleteScreenTag: false,
    isDeleteScreenTagSuccess: false,
    deleteScreenTagError: null,

    selectedScreensIds: [],
};

export default function screensPageReducer(state = screensPageInitialState, action: ScreensAction): ScreensPageState {
    const { deleteLocation, deleteScreen } = state;
    switch (action.type) {
        case SET_ACTIVE_LOCATION_ID:
            return {
                ...state,
                activeLocationId: action.payload.activeLocationId,
            };
        case SET_ACTIVE_SCREEN_ID:
            return {
                ...state,
                activeScreenId: action.payload.activeScreenId,
            };

        case GET_LOCATIONS:
            return {
                ...state,
                loadingLocations: true,
                loadingLocationsError: null,
            };
        case GET_LOCATIONS_SUCCESS:
            return {
                ...state,
                loadingLocations: false,
                locations: action.payload.locations,
            };
        case GET_LOCATIONS_FAIL:
            return {
                ...state,
                loadingLocations: false,
                loadingLocationsError: action.payload.errors,
            };

        case UPDATE_LOCATION:
            return {
                ...state,
                updatingLocation: true,
                updateLocationSuccess: false,
                updatingLocationError: null,
            };
        case UPDATE_LOCATION_SUCCESS:
            return {
                ...state,
                updateLocationSuccess: action.payload,
                updatingLocation: false,
            };
        case UPDATE_LOCATION_FAIL:
            return {
                ...state,
                updatingLocation: false,
                updateLocationSuccess: false,
                updatingLocationError: action.payload.errors,
            };

        case ADD_LOCATION:
            return {
                ...state,
                addingLocation: true,
                addLocationSuccess: false,
                addedLocationId: '',
            };
        case ADD_LOCATION_SUCCESS:
            return {
                ...state,
                addingLocation: false,
                addLocationSuccess: action.payload,
                addLocationError: null,
            };
        case ADD_LOCATION_FAIL:
            return {
                ...state,
                addLocationSuccess: false,
                addingLocation: false,
                addLocationError: action.payload.errors,
            };

        case DELETE_LOCATION:
            return {
                ...state,
                deleteLocation: action.payload.location,
                deleteLocationSuccess: false,
                deleteLocationError: false,
            };

        case DELETE_LOCATION_SUCCESS:
            if (deleteLocation) {
                return {
                    ...state,
                    deleteLocation: false,
                    //location is not yet removed, filter it out from locations array
                    locations: state.locations.filter((location) => location.id !== deleteLocation.id),
                    deleteLocationSuccess: {
                        location: deleteLocation,
                        ...action.payload,
                    },
                };
            } else {
                return {
                    ...state,
                    deleteLocation: false,
                };
            }

        case DELETE_LOCATION_FAIL:
            return {
                ...state,
                deleteLocation: false,
                deleteLocationSuccess: false,
                deleteLocationError: action.payload,
            };

        case CLEAR_DELETE_LOCATION:
            return {
                ...state,
                deleteLocationSuccess: false,
                deleteLocationError: false,
            };

        case GET_SCREENS:
            return {
                ...state,
                loadingScreens: true,
                loadingScreensError: null,
            };
        case GET_SCREENS_SUCCESS:
            return {
                ...state,
                loadingScreens: false,
                screensById: action.payload.screens.reduce(
                    (map: any, screen: any) => {
                        map[screen?.id] = screen;
                        return map;
                    },
                    {} as Record<string, ScreenType>,
                ),
                loadingScreensError: null,
            };
        case GET_SCREENS_FAIL:
            return {
                ...state,
                loadingScreens: false,
                loadingScreensError: action.payload.errors,
            };

        case UPDATE_SCREEN:
            return {
                ...state,
                updateScreen: true,
                updateScreenIds: action.payload.screenId,
                updateScreenError: null,
                updateScreenSuccess: false,
            };
        case UPDATE_SCREEN_SUCCESS:
            return {
                ...state,
                updateScreen: false,
                updateScreenSuccess: action.payload,
            };
        case UPDATE_SCREEN_FAIL:
            return {
                ...state,
                updateScreen: false,
                updateScreenError: action.payload.errors,
                updateScreenSuccess: false,
            };

        case UPDATE_SCREENS:
            return {
                ...state,
                updateScreen: true,
                updateScreenIds: action.payload.screenIds,
                updateScreenError: null,
                updateScreenSuccess: false,
            };
        case UPDATE_SCREENS_SUCCESS:
            return {
                ...state,
                updateScreen: false,
                updateScreenSuccess: action.payload,
            };
        case UPDATE_SCREENS_FAIL:
            return {
                ...state,
                updateScreen: false,
                updateScreenError: action.payload.errors,
                updateScreenSuccess: false,
            };

        case OPEN_ADD_SCREEN_MODAL:
            return {
                ...state,
                isAddScreenModalOpen: true,
                addedLocationId: '',
                addScreenError: null,
            };

        case CLOSE_ADD_SCREEN_MODAL:
            return {
                ...state,
                isAddScreenModalOpen: false,
                addScreenSuccess: false,
            };

        case ADD_SCREEN:
            return {
                ...state,
                addingScreen: true,
                addScreenSuccess: false,
            };

        case ADD_SCREEN_SUCCESS:
            return {
                ...state,
                addingScreen: false,
                addScreenSuccess: action.payload,
                isAddScreenModalOpen: false,
                addScreenError: null,
            };
        case ADD_SCREEN_FAIL:
            return {
                ...state,
                addingScreen: false,
                addScreenSuccess: false,
                addScreenError: action.payload.errors,
            };

        case DELETE_SCREENS:
            return {
                ...state,
                deleteScreen: action.payload.screens,
                deleteScreenSuccess: false,
                deleteScreenError: false,
            };
        case DELETE_SCREENS_SUCCESS:
            if (deleteScreen) {
                const screens = state.screensById;
                for (const { id } of deleteScreen) {
                    delete screens[id];
                }

                return {
                    ...state,
                    deleteScreen: false,
                    deleteScreenSuccess: { screens: deleteScreen, ...action.payload },
                    screensById: screens,
                };
            } else {
                return {
                    ...state,
                    deleteScreen: false,
                };
            }

        case DELETE_SCREENS_FAIL:
            return {
                ...state,
                deleteScreen: false,
                deleteScreenError: action.payload,
            };

        case CLEAR_DELETE_SCREENS:
            return {
                ...state,
                deleteScreenSuccess: false,
                deleteScreenError: false,
            };

        case DELETE_SCREEN_TAGS:
            return {
                ...state,
                isDeleteScreenTagSuccess: false,
                deleteScreenTag: true,
                deleteScreenTagError: null,
            };
        case DELETE_SCREEN_TAGS_SUCCESS:
            return {
                ...state,
                isDeleteScreenTagSuccess: true,
                deleteScreenTag: false,
            };
        case DELETE_SCREEN_TAGS_FAIL:
            return {
                ...state,
                isDeleteScreenTagSuccess: false,
                deleteScreenTag: false,
                deleteScreenTagError: action.payload.errors,
            };

        case SET_SELECTED_SCREEN_IDS:
            return {
                ...state,
                selectedScreensIds: action.payload.selectedScreensIds,
            };

        case CLEAR_SCREENS_SUCCESS:
            return {
                ...state,
                addLocationSuccess: false,
                updateLocationSuccess: false,
                deleteLocationSuccess: false,
                addScreenSuccess: false,
                updateScreenSuccess: false,
                deleteScreenSuccess: false,
                isDeleteScreenTagSuccess: false,

                updateScreenIds: null,
            };

        case CLEAR_SCREENS_ERROR:
            return {
                ...state,
                updateScreenError: null,
                addLocationError: null,
                addScreenError: null,

                updateScreenIds: null,
            };

        case CLOSE_ADD_SCREEN_SNACKBAR:
            return {
                ...state,
                addScreenSuccess: false,
            };

        default:
            return state;
    }
}
