import React from 'react';

import { AddIcon } from '../../ui-kit';
import LocationsList from './components/LocationsList';
import ScreensList from './components/ScreensList';
import RightScreensMenu from './components/RightScreensMenu';
import { useScreensPage } from './hooks/useScreensPage';

import css from './style.m.css';

const ScreensPage: React.FC = () => {
    const { userRole, openAddScreenModal, t } = useScreensPage();

    return (
        <div className={css.wrapperMainScreensAll}>
            <div className={css.wrapMainScreens}>
                <div className={css.mainScreens}>
                    <div className={css.centerBlockScreens}>
                        <div className={css.mainBlockScreen}>
                            <div className={css.fieldBtnAddScreen}>
                                <a>
                                    <button className={css.btnBlockScreen} onClick={openAddScreenModal}>
                                        <AddIcon />
                                        {t('button')}
                                    </button>
                                </a>
                            </div>
                        </div>
                        <div className={css.mainScreensAll}>
                            <div className={css.mainScreensPage}>
                                <LocationsList />
                                <ScreensList />
                            </div>
                        </div>
                    </div>

                    <RightScreensMenu />
                </div>
            </div>
        </div>
    );
};

export default ScreensPage;
