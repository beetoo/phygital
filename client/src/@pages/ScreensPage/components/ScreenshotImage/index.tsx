import React from 'react';

// import noPlaylistsPlaceholder from '../../../../assets/screenshots_not_available.jpg';
import { useGetScreenshots } from '../../../../hooks/useGetScreenshots';

type Props = {
    timeout: number;
    screenId?: string;
    width: string;
    height: string;
    className?: string;
};

const ScreenshotImage: React.FC<Props> = ({ className, width, height, screenId, timeout }) => {
    const { screenshotSrc } = useGetScreenshots(timeout, screenId);

    return <img className={className} src={screenshotSrc} width={width} height={height} loading="lazy" alt="" />;
};

export default ScreenshotImage;
