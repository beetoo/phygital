import React from 'react';
import { useTranslation } from 'react-i18next';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

import { getObjectTitleForSnackbar } from '../../../../hooks/getObjectTitleForSnackbar';
import { Snackbar } from '../../../../ui-kit';
import { AddLocationSuccess } from '../../types';

export const AddLocationSnackbar: React.FC<{ location: AddLocationSuccess; onClose: () => void }> = ({
    location,
    onClose,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const title = getObjectTitleForSnackbar(location.name);

    return (
        <Snackbar
            startAdornment={<CheckCircleIcon color="success" />}
            title={`${t('textFifteen')} «${title}» ${t('textThirtyOne')}`}
            // {`Локация «${title}» добавлена!`}
            onClose={onClose}
        />
    );
};
