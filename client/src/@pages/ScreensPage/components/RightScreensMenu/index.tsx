import React, { memo } from 'react';

import LocationMenu from '../LocationMenu';
import MultiplyScreensMenu from '../MultiplyScreensMenu';
import ScreenMenu from '../ScreenMenu';
import { useRightScreensMenu } from '../../hooks/useRightScreensMenu';

import css from '../../style.m.css';

const RightScreensMenu: React.FC = () => {
    const { isLocationsExist, isLocationSelected, isScreenSelected, isOneOrMoreScreensSelected } =
        useRightScreensMenu();

    return isLocationsExist ? (
        <>
            {isLocationSelected && <LocationMenu />}
            {isOneOrMoreScreensSelected && <MultiplyScreensMenu />}
            {isScreenSelected && <ScreenMenu />}
        </>
    ) : (
        <div className={css.mainRightMenu} />
    );
};

export default memo(RightScreensMenu);
