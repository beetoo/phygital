import React from 'react';
import { clsx } from 'clsx';

import AdminScreenLogInfo from '../AdminScreenLogInfo';
import ScreenshotImage from '../ScreenshotImage';
import { MenuItem, Select } from '../../../../ui-kit';
import { resolutionsList } from '../../../../constants';
import { OrientationRusType, OrientationTypeEng } from '../../types';
import { useScreenMenu } from '../../../../hooks/useScreenMenu';
import { ScreenHistoryItem } from '../../../PlaylistsPage/hooks/useHistoryItems';
import { useUpdateScreenSnackbar } from '../../hooks/useUpdateScreenSnackbar';
import { ScreenMessageItem } from '../../hooks/useScreenMessages';

import css from '../../style.m.css';

const ScreenMenu: React.FC = () => {
    const {
        userRole,
        activeScreenStatus,
        savedTags,

        hasCustomResolutionInput,
        isSubmitButtonDisabled,
        isSubmitButtonShown,

        screenName,
        customResolutionWidth,
        customResolutionHeight,
        tag,
        notSavedTags,
        log,
        availableBytesInfo,
        usedBytesInfo,
        totalBytesInfo,
        hasCapacityInfo,
        verificationHash,

        schedules,
        onScreenNameChange,
        isSchedulesListOpen,
        onSwitchSchedulesList,
        goToPlaylist,

        screenResolutionValue,
        onScreenResolutionSelect,

        screenOrientationValue,
        onScreenOrientationSelect,
        onCustomResolutionWidthChange,
        onCustomResolutionHeightChange,
        onTagChange,
        onTagEnter,
        onDeleteNotSavedTag,
        onDeleteSavedTag,
        handleSubmit,

        selectableLocations,
        locationSelectValue,
        setActiveLocationId,
        onLocationSelect,

        openDeleteScreenModal,

        openChangePlayerModal,

        isInfoShown,
        showHideInfo,

        screenMenuTab,
        setActiveScreenMenuTab,
        hasHistory,
        screenHistoryItems,

        hasMessages,
        screenMessages,

        tScreens,
        tScreensModal,
    } = useScreenMenu();

    useUpdateScreenSnackbar();

    return (
        <div className={css.mainRightMenuSettingsScreen}>
            <div className={css.wrapperFieldRightMenu}>
                <h2>{screenName}</h2>

                {(hasHistory || hasMessages) && (
                    <div className={css.containerTabs}>
                        <button
                            className={clsx(css.btnTab, screenMenuTab === 'settings' && css.btnTabActive)}
                            onClick={setActiveScreenMenuTab('settings')}
                        >
                            {tScreens('settings')}
                        </button>
                        {hasHistory && (
                            <button
                                className={clsx(css.btnTab, screenMenuTab === 'history' && css.btnTabActive)}
                                onClick={setActiveScreenMenuTab('history')}
                            >
                                {tScreens('history')}
                            </button>
                        )}
                        {hasMessages && (
                            <button
                                className={clsx(css.btnTab, screenMenuTab === 'messages' && css.btnTabActive)}
                                onClick={setActiveScreenMenuTab('messages')}
                            >
                                {tScreens('message')}
                            </button>
                        )}
                    </div>
                )}

                {screenMenuTab === 'settings' && (
                    <>
                        <p
                            className={
                                activeScreenStatus === 'online'
                                    ? css.statusOn
                                    : ['offline', 'pending'].includes(activeScreenStatus)
                                      ? css.statusOff
                                      : css.statusError
                            }
                        >
                            {activeScreenStatus === 'online'
                                ? tScreens('online')
                                : ['offline', 'pending'].includes(activeScreenStatus)
                                  ? tScreens('offline')
                                  : tScreens('error')}
                        </p>
                        <ScreenshotImage timeout={5} className={css.screenshotPreview} width="292" height="164" />
                        <div className={css.fieldBtnGoSchedule}>
                            <button type="button" className={css.btnGoSchedule} onClick={openChangePlayerModal}>
                                {tScreensModal('changePlayer')}
                            </button>
                        </div>
                        <div>
                            <label>{tScreens('screenName')}</label>
                            <input
                                className={css.rightMenuSetting}
                                placeholder={tScreens('screenName') as string}
                                value={screenName}
                                onChange={onScreenNameChange}
                                maxLength={125}
                            />
                            {/*<p className={css.mainRightMenuAddress}>{locationDescription}</p>*/}
                        </div>

                        {/*<div className={css.fieldBtnGoSchedule}>*/}
                        {/*    <button*/}
                        {/*        type="button"*/}
                        {/*        className={css.btnGoSchedule}*/}
                        {/*        onClick={goToScreenSchedule}*/}
                        {/*        disabled={isNoSchedules}*/}
                        {/*    >*/}
                        {/*        <img alt="" />*/}
                        {/*        {tScreens('btnGoSchedule')}*/}
                        {/*    </button>*/}
                        {/*</div>*/}
                        <div className={css.wrapperInfo}>
                            <div className={css.infoHeader} onClick={showHideInfo}>
                                <h3>{tScreens('info')}</h3>
                                <span className={isInfoShown ? css.iconArrowInfoUp : css.iconArrowInfoDown} />
                            </div>
                            {isInfoShown && (
                                <div>
                                    {hasCapacityInfo && (
                                        <div
                                            style={{
                                                display: 'flex',
                                                flexDirection: 'column',
                                                gap: '10px',
                                                marginBottom: '20px',
                                                padding: '0 24px',
                                            }}
                                        >
                                            {verificationHash && (
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        justifyContent: 'space-between',
                                                    }}
                                                >
                                                    <h3 style={{ width: '230px', margin: 0 }}>
                                                        {tScreensModal('connectionCode')}
                                                    </h3>
                                                    <p style={{ margin: 0 }}>{verificationHash}</p>
                                                </div>
                                            )}

                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'space-between',
                                                }}
                                            >
                                                <h3 style={{ width: '230px', margin: 0 }}>{tScreens('capacity')}</h3>
                                                <p style={{ margin: 0 }}>{totalBytesInfo}</p>
                                            </div>

                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'space-between',
                                                }}
                                            >
                                                <h3 style={{ width: '230px', margin: 0 }}>{tScreens('used')}</h3>
                                                <p style={{ margin: 0 }}>{usedBytesInfo}</p>
                                            </div>

                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent: 'space-between',
                                                }}
                                            >
                                                <h3 style={{ width: '230px', margin: 0 }}>{tScreens('free')}</h3>
                                                <p style={{ margin: 0 }}>{availableBytesInfo}</p>
                                            </div>
                                        </div>
                                    )}

                                    {/*<div*/}
                                    {/*    className={clsx(css.ledCheckbox, css.screenMenu)}*/}
                                    {/*    onClick={onLedScreenCheckboxClick}*/}
                                    {/*>*/}
                                    {/*    <p className={css.ledCheckboxParagraph}>{tScreensModal('ledScreen')}</p>*/}
                                    {/*    <span className={isLedScreen ? css.ledCheckboxOn : css.ledCheckboxOff} />*/}
                                    {/*</div>*/}

                                    <div style={{ marginBottom: '4px' }}>
                                        <Select
                                            label={tScreens('screenResolution') as string}
                                            onChange={onScreenResolutionSelect}
                                            value={screenResolutionValue}
                                            width={292}
                                            style={{ marginLeft: '24px' }}
                                        >
                                            {[...resolutionsList, tScreensModal('resolutionCustom')].map(
                                                (resolution) => (
                                                    <MenuItem key={resolution} value={resolution}>
                                                        {resolution}
                                                    </MenuItem>
                                                ),
                                            )}
                                        </Select>
                                    </div>

                                    {hasCustomResolutionInput && (
                                        <div className={css.customResolutionInputContainer}>
                                            <div className={css.fieldWidth}>
                                                <input
                                                    className={css.customResolutionInput}
                                                    placeholder={tScreens('width') as string}
                                                    value={customResolutionWidth}
                                                    onChange={onCustomResolutionWidthChange}
                                                    maxLength={125}
                                                />
                                                <div className={css.fieldWidthItem}>
                                                    <span className={css.customResolutionInputItem}>px</span>
                                                </div>
                                            </div>
                                            <div className={css.fieldWidth}>
                                                <input
                                                    className={css.customResolutionInput}
                                                    placeholder={tScreens('height') as string}
                                                    value={customResolutionHeight}
                                                    onChange={onCustomResolutionHeightChange}
                                                    maxLength={125}
                                                />
                                                <div className={css.fieldWidthItem}>
                                                    <span className={css.customResolutionInputItem}>px</span>
                                                </div>
                                            </div>
                                        </div>
                                    )}

                                    <div style={{ marginBottom: '4px' }}>
                                        <Select
                                            label={tScreens('imageRotation') as string}
                                            onChange={onScreenOrientationSelect}
                                            value={screenOrientationValue}
                                            width={292}
                                            style={{ marginLeft: '24px' }}
                                            MenuProps={{
                                                PaperProps: {
                                                    style: {
                                                        marginTop: '4px',
                                                        marginLeft: '0',
                                                    },
                                                },
                                            }}
                                        >
                                            {(
                                                JSON.parse(tScreensModal('orientationsList')) as (
                                                    | OrientationTypeEng
                                                    | OrientationRusType
                                                )[]
                                            ).map((orientation) => (
                                                <MenuItem key={orientation} value={orientation}>
                                                    {orientation}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </div>

                                    <AdminScreenLogInfo log={log} />
                                </div>
                            )}
                        </div>
                        <div className={clsx(css.playlists, css.wrapMainRightMenuInfoAll)}>
                            {schedules.length > 0 && (
                                <div className={css.infoHeader} onClick={onSwitchSchedulesList}>
                                    <h3>
                                        {tScreens('playlistsOnScreen')}:&ensp;
                                        <span>{schedules.length}</span>
                                    </h3>
                                    <span
                                        className={isSchedulesListOpen ? css.iconArrowInfoUp : css.iconArrowInfoDown}
                                    />
                                </div>
                            )}
                            <div className={css.mainRightMenuInfoAll}>
                                {schedules.length > 0 && (
                                    <>
                                        {isSchedulesListOpen && (
                                            <ul>
                                                {schedules.map(({ playlist }, index) => (
                                                    <li key={index}>
                                                        <p>{playlist?.title}</p>
                                                        <a onClick={goToPlaylist(playlist?.id ?? '')}>
                                                            {tScreens('goTo')}
                                                        </a>
                                                    </li>
                                                ))}
                                            </ul>
                                        )}
                                    </>
                                )}
                            </div>
                        </div>
                        <h3 className={css.nonUserSelect}>{tScreens('tags')}</h3>
                        <ul className={css.tagButton}>
                            {savedTags.map(({ id, name }) => (
                                <li key={id} className={css.tagButtonItem}>
                                    <button type="button">{name}</button>
                                    <span className={css.iconX} onClick={onDeleteSavedTag(id)} />
                                </li>
                            ))}
                            {notSavedTags.map((tag, index) => (
                                <li key={index} className={css.tagButtonItem}>
                                    <button type="button">{tag}</button>
                                    <span className={css.iconX} onClick={onDeleteNotSavedTag(tag)} />
                                </li>
                            ))}
                        </ul>
                        <div className={css.inputAddTag}>
                            <input
                                className={clsx(css.rightMenuSetting, css.addTag)}
                                placeholder={tScreens('tagsAdd') as string}
                                value={tag}
                                onChange={onTagChange}
                                onKeyPress={onTagEnter}
                                maxLength={125}
                            />
                            <label>
                                {tScreens('tagTextOne')}
                                <span>{tScreens('tagTextTwo')}</span>
                                {tScreens('tagTextThree')}
                            </label>
                        </div>
                        {selectableLocations.length > 0 && (
                            <>
                                <h3 className={css.nonUserSelect}>{tScreens('moveScreen')}</h3>
                                <div style={{ marginBottom: '22px', marginTop: '-10px' }}>
                                    <Select
                                        placeholder={tScreensModal('selectPointShort') as string}
                                        onChange={onLocationSelect}
                                        value={locationSelectValue}
                                        width={292}
                                        style={{ marginLeft: '24px' }}
                                    >
                                        {selectableLocations.map((location) => (
                                            <MenuItem
                                                onClick={setActiveLocationId(location.id)}
                                                key={location.id}
                                                value={location.name}
                                            >
                                                {location.name}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </div>
                            </>
                        )}
                        <button type="button" className={css.deleteScreen} onClick={openDeleteScreenModal}>
                            <img alt="" />
                            {tScreens('btnDelScreen')}
                        </button>
                    </>
                )}

                {screenMenuTab === 'history' && (
                    <div className={css.containerHistoryDescription}>
                        {(screenHistoryItems as ScreenHistoryItem[]).map(({ id, createdAt, status }) => (
                            <div key={id} className={css.historyDescription}>
                                <p>{createdAt}</p>
                                <p>
                                    {tScreens('status')}: {tScreens(status)}
                                </p>
                            </div>
                        ))}
                    </div>
                )}

                {screenMenuTab === 'messages' && (
                    <div className={css.containerHistoryDescription}>
                        {(screenMessages as ScreenMessageItem[]).map(({ id, createdAt, message }) => (
                            <div key={id} className={css.historyDescription}>
                                <p>{createdAt}</p>
                                <p>
                                    {tScreens('message')}: {message}
                                </p>
                            </div>
                        ))}
                    </div>
                )}
            </div>

            {isSubmitButtonShown && (
                <div className={css.wrapperFieldBtn}>
                    {screenMenuTab === 'settings' && (
                        <button
                            type="button"
                            className={css.saveSettings}
                            onClick={handleSubmit}
                            disabled={isSubmitButtonDisabled}
                        >
                            {tScreens('btnSaveSetting')}
                        </button>
                    )}
                </div>
            )}
        </div>
    );
};

export default ScreenMenu;
