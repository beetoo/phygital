import React, { CSSProperties } from 'react';
import { InputAdornment, CircularProgress } from '@mui/material';

import { SuggestionTooltip, SuggestListBox, CustomInput } from '../../../../ui-kit';
import { useAutocompleteInput } from '../../hooks/useAutocompleteInput';
import { DataSuggestion } from '../../../../resolvers/screensResolvers';

type Props = {
    locationCity: string;
    onLocationCityChange: (value: string) => void;
    onLocationCitySelect: (suggestion: DataSuggestion) => void;
    disabled?: boolean;

    style?: CSSProperties;
};

const AddLocationCityInput: React.FC<Props> = ({
    locationCity,
    onLocationCityChange,
    onLocationCitySelect,
    disabled,
    style,
}) => {
    const {
        inputValue,
        queryValue,
        queryAddition,
        suggestList,
        onInputChange,
        onSuggestSelect,
        isLoading,
        tScreensModal,
    } = useAutocompleteInput({
        inputValue: locationCity,
        onInputValueChange: onLocationCityChange,
        onSuggestValueSelect: onLocationCitySelect,
    });

    return (
        <div>
            <div>
                <label>{tScreensModal('city')}</label>
                <SuggestionTooltip title={inputValue} disableHoverListener={inputValue.length < 40 || disabled}>
                    <CustomInput
                        sx={{ margin: '9px 0 16px' }}
                        style={style}
                        placeholder={tScreensModal('enterCity')}
                        value={inputValue}
                        onChange={onInputChange}
                        disabled={disabled}
                        endAdornment={
                            <InputAdornment position="end">
                                <CircularProgress size={20} variant={isLoading ? 'indeterminate' : 'determinate'} />
                            </InputAdornment>
                        }
                    />
                </SuggestionTooltip>
            </div>
            <SuggestListBox
                {...{
                    queryValue,
                    queryAddition,
                    suggestList,
                    onSuggestSelect,
                }}
            />
        </div>
    );
};

export default AddLocationCityInput;
