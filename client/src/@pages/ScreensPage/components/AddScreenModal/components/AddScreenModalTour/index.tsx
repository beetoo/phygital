import React, { ComponentProps } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { Tour } from '../../../../../../components/Tour';
import { TourBody } from '../../../../../../components/Tour/components/TourBody';
import { TourHeader } from '../../../../../../components/Tour/components/TourHeader';

import addScreenModalOnBoardingTitle from '../../images/addScreenModalOnBoardingTitle.png';
import addScreenModalOnBoardingHash from '../../images/addScreenModalOnBoardingHash.png';
import addScreenModalOnBoardingScreenTime from '../../images/addScreenModalOnBoardingScreenTime.png';
import addScreenModalOnBoardingResolution from '../../images/addScreenModalOnBoardingResolution.png';
import addScreenModalOnBoardingOrientation from '../../images/addScreenModalOnBoardingOrientation.png';
import addScreenModalOnBoardingLocation from '../../images/addScreenModalOnBoardingLocation.png';

import { selectOnBoardingStatus } from '../../../../../../selectors';

type Steps = ComponentProps<typeof Tour>['steps'];

type Props = {
    addScreenTitleRef: HTMLElement | null;
    addScreenHashRef: HTMLElement | null;
    addScreenScreenTimeRef: HTMLElement | null;
    addScreenResolutionRef: HTMLElement | null;
    addScreenOrientationRef: HTMLElement | null;
    addScreenLocationRef: HTMLElement | null;
};

export const AddScreenModalTour: React.FC<Props> = ({
    addScreenTitleRef,
    addScreenHashRef,
    addScreenScreenTimeRef,
    addScreenResolutionRef,
    addScreenOrientationRef,
    addScreenLocationRef,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'learningHintsModal' });

    const steps = React.useMemo<Steps>(
        () => [
            addScreenTitleRef
                ? {
                      name: 'addScreenTitle',
                      target: addScreenTitleRef,
                      placement: 'left-start',
                      title: (
                          <TourHeader>
                              <img src={addScreenModalOnBoardingTitle} width={32} height={32} alt="" />
                              <div style={{ width: '168px', marginLeft: '15px' }}>
                                  {t('textOne')}
                                  {/* Зачем экрану имя */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  {t('textTwo')}
                                  {/* Новому экрану нужно задать имя, которое поможет его найти среди других экранов или
                                  сгруппировать. Например, &laquo;Над барной стойкой&raquo; или &laquo;У кассы&raquo;. */}
                              </p>
                          </TourBody>
                      ),
                  }
                : null,
            addScreenHashRef
                ? {
                      name: 'addScreenHash',
                      target: addScreenHashRef,
                      placement: 'left-start',
                      title: (
                          <TourHeader>
                              <img src={addScreenModalOnBoardingHash} width={32} height={32} alt="" />
                              <div style={{ width: '158px', marginLeft: '15px' }}>
                                  {t('textThree')}
                                  {/* Где и зачем брать хеш */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  {t('textFour')}
                                  {/* Хеш верификации нужен, чтобы подключить экран в сервис. */}
                              </p>
                              <p>
                                  {t('textFive')}
                                  {/* Включите экран устройства, который хотите подключить. На экране устройства вы увидите
                                  код подтверждения. Это и есть хеш верификации. Укажите его здесь. */}
                              </p>

                              {/* <p>
                                  На экране нет кода? Скорее всего на него не установлена операционная система Phygital
                                  Signage. Установите систему, используя{' '}
                                  <a
                                      onClick={() => window.open('/screen_management')}
                                      style={{ color: 'white', borderBottom: '1px solid white' }}
                                  >
                                      инструкции
                                  </a>
                                  , или обратитесь в службу поддержки за помощью.
                              </p> */}
                          </TourBody>
                      ),
                  }
                : null,
            addScreenScreenTimeRef
                ? {
                      name: 'addScreenScreenTime',
                      target: addScreenScreenTimeRef,
                      placement: 'right-start',
                      title: (
                          <TourHeader>
                              <img src={addScreenModalOnBoardingScreenTime} width={32} height={32} alt="" />
                              <div style={{ marginLeft: '15px' }}>
                                  {t('textSix')}
                                  {/* Время работы экрана */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  {t('textSeven')}
                                  {/* Время работы экрана устанавливается, чтобы было удобнее отслеживать пробелы в
                                  расписании. Например, если ваше кафе работает с 8:00 до 24:00, то установив время
                                  работы экрана, вам не нужно будет заполнять нерабочее время в его расписании. */}
                              </p>
                          </TourBody>
                      ),
                  }
                : null,
            addScreenResolutionRef
                ? {
                      name: 'addScreenResolution',
                      target: addScreenResolutionRef,
                      placement: 'right',
                      title: (
                          <TourHeader>
                              <img src={addScreenModalOnBoardingResolution} width={32} height={32} alt="" />
                              <div style={{ marginLeft: '15px' }}>
                                  {t('textEight')}
                                  {/* Разрешение экрана */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  {t('textNine')}
                                  {/* Важно указать разрешение экрана, чтобы контент корректно отображался на подключенных
                                  устройствах. */}
                              </p>
                              <p>
                                  {t('textTen')}
                                  {/* Его можно посмотреть в техпаспорте или настройках устройства, найти в интернете по
                                  названию или обратиться в службу поддержки. */}
                              </p>
                              <p>
                                  {t('textEleven')}
                                  {/* Размеры получаемого на экране изображения в пикселях: 800 × 600 px, 1280 × 1024 px и
                                  т.д. Этот параметр не соотносится с физическими размерами экрана, такими как длина,
                                  ширина или диагональ. У мониторов с разной диагональю может быть одинаковое разрешение
                                  экранов, например, 1920х1080. */}
                              </p>
                          </TourBody>
                      ),
                  }
                : null,
            addScreenOrientationRef
                ? {
                      name: 'addScreenOrientation',
                      target: addScreenOrientationRef,
                      placement: 'left',
                      title: (
                          <TourHeader>
                              <img src={addScreenModalOnBoardingOrientation} width={32} height={32} alt="" />
                              <div style={{ marginLeft: '15px' }}>
                                  {t('textTwelve')}
                                  {/* Ориентация экрана */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  {t('textThirteen')}
                                  {/* Выберите ориентацию экрана. */}
                              </p>
                              <p>
                                  {t('textFourteen')}
                                  {/* Горизонтальная ориентация ещё называется альбомной, когда ширина экрана больше высоты. */}
                              </p>
                              <p>
                                  {t('textFifteen')}
                                  {/* Вертикальная или портретная — высота экрана больше его ширины. */}
                              </p>
                              <p>
                                  {t('textSixteen')}
                                  {/* Перевернутая горизонтальная и вертикальная ориентации используются, если физические
                                  экраны повешены вверх ногами. */}
                              </p>
                          </TourBody>
                      ),
                  }
                : null,
            addScreenLocationRef
                ? {
                      name: 'addScreenLocation',
                      target: addScreenLocationRef,
                      placement: 'right-start',
                      title: (
                          <TourHeader>
                              <img src={addScreenModalOnBoardingLocation} width={32} height={32} alt="" />
                              <div style={{ marginLeft: '15px' }}>
                                  {t('textSeventeen')}
                                  {/* Локация */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  {t('textEighteen')}
                                  {/* Выберите или создайте новую локацию для всех экранов в одной локации.{' '} */}
                              </p>
                          </TourBody>
                      ),
                  }
                : null,
        ],
        [
            addScreenHashRef,
            addScreenLocationRef,
            addScreenOrientationRef,
            addScreenResolutionRef,
            addScreenScreenTimeRef,
            addScreenTitleRef,
            t,
        ],
    );

    const isOnboardingEnabled = useSelector(selectOnBoardingStatus);

    return <Tour steps={steps} name="addScreenModal" run={isOnboardingEnabled} />;
};
