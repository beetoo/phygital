import React from 'react';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';

import { setHideModal } from '../../../../../../components/ModalsContainer/actions';

import css from '../../../../../ScreensPage/components/AddScreenModal/components/MaxScreensAlertModal/style.m.css';

const MaxScreensAlertModal: React.FC = () => {
    const { t } = useTranslation('translation', { keyPrefix: 'screensModal' });

    const dispatch = useDispatch();

    return (
        <div className={css.popupAddScreen}>
            <h2>{t('addNewScreen')}</h2>

            <div className={css.description}>
                <p>{t('youHaveEnabledMaximum')}</p>
                <p>{t('ifYouHaveAnyQuestions')}</p>
            </div>

            <div className={css.fieldButtons}>
                <button className={css.btnClose} onClick={() => dispatch(setHideModal())}>
                    {t('close')}
                </button>
            </div>
        </div>
    );
};

export default MaxScreensAlertModal;
