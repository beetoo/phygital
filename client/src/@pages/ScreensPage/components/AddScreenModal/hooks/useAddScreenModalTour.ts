import { Dispatch, SetStateAction, useState } from 'react';

type ReturnValue = {
    addScreenTitleRef: HTMLInputElement | null;
    setAddScreenTitleRef: Dispatch<SetStateAction<HTMLInputElement | null>>;
    addScreenHashRef: HTMLInputElement | null;
    setAddScreenHashRef: Dispatch<SetStateAction<HTMLInputElement | null>>;
    addScreenScreenTimeRef: HTMLDivElement | null;
    setAddScreenScreenTimeRef: Dispatch<SetStateAction<HTMLDivElement | null>>;
    addScreenResolutionRef: HTMLElement | null;
    setAddScreenResolutionRef: Dispatch<SetStateAction<HTMLElement | null>>;
    addScreenOrientationRef: HTMLElement | null;
    setAddScreenOrientationRef: Dispatch<SetStateAction<HTMLElement | null>>;
    addScreenLocationRef: HTMLElement | null;
    setAddScreenLocationRef: Dispatch<SetStateAction<HTMLElement | null>>;
};

export const useAddScreenModalTour = (): ReturnValue => {
    const [addScreenTitleRef, setAddScreenTitleRef] = useState<HTMLInputElement | null>(null);
    const [addScreenHashRef, setAddScreenHashRef] = useState<HTMLInputElement | null>(null);
    const [addScreenScreenTimeRef, setAddScreenScreenTimeRef] = useState<HTMLDivElement | null>(null);
    const [addScreenResolutionRef, setAddScreenResolutionRef] = useState<HTMLElement | null>(null);
    const [addScreenOrientationRef, setAddScreenOrientationRef] = useState<HTMLElement | null>(null);
    const [addScreenLocationRef, setAddScreenLocationRef] = useState<HTMLElement | null>(null);

    return {
        addScreenTitleRef,
        setAddScreenTitleRef,
        addScreenHashRef,
        setAddScreenHashRef,
        addScreenScreenTimeRef,
        setAddScreenScreenTimeRef,
        addScreenResolutionRef,
        setAddScreenResolutionRef,
        addScreenOrientationRef,
        setAddScreenOrientationRef,
        addScreenLocationRef,
        setAddScreenLocationRef,
    };
};
