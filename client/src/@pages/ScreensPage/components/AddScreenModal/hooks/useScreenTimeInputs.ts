import { ChangeEvent, useCallback, useState, useMemo, useEffect } from 'react';

import { ScreenTime, screenTimeToString, timeStringToTimeObject } from '../../../helpers';
import { startTimeStringMoreThanEndTimeString } from '../../../../../helpers';
import { isTimeMatch } from '../../../../PlaylistsPage/helpers';

type ReturnValue = {
    startScreenTime: ScreenTime | undefined;
    endScreenTime: ScreenTime | undefined;
    startScreenTimeValue: string;
    endScreenTimeValue: string;
    onStartScreenTimeChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onEndScreenTimeChange: (e: ChangeEvent<HTMLInputElement>) => void;
    isScreenTimeNotCorrect: boolean;
    isScreenTimeNotChanged: boolean;
    isScreenTimeNextDayNoteShow: boolean;
};

type Props = {
    screenId: string;
    startTime: ScreenTime;
    endTime: ScreenTime;
};

export const useScreenTimeInputs = ({ screenId, startTime, endTime }: Props): ReturnValue => {
    const { startTimeValue: initialStartTime, endTimeValue: initialEndTime } = useMemo(
        () => screenTimeToString(startTime, endTime),
        [endTime, startTime],
    );

    const [startScreenTimeValue, setStartScreenTimeValue] = useState(initialStartTime);
    const [endScreenTimeValue, setEndScreenTimeValue] = useState(initialEndTime);

    const [startScreenTime, setStartScreenTime] = useState<ScreenTime | undefined>(startTime);
    const [endScreenTime, setEndScreenTime] = useState<ScreenTime | undefined>(endTime);

    useEffect(() => {
        if (screenId) {
            setStartScreenTimeValue(initialStartTime);
            setEndScreenTimeValue(initialEndTime);
            setStartScreenTime(startTime);
            setEndScreenTime(endTime);
        }
    }, [endTime, screenId, startTime, initialEndTime, initialStartTime]);

    const isScreenTimeNotCorrect = !(startScreenTime && endScreenTime);

    const isScreenTimeNotChanged = useMemo(() => {
        const initialStartHours = startTime?.hours;
        const initialStartMinutes = startTime?.minutes;

        const initialEndHours = endTime?.hours;
        const initialEndMinutes = endTime?.minutes;

        const startHours = startScreenTime?.hours;
        const startMinutes = startScreenTime?.minutes;

        const endHours = endScreenTime?.hours;
        const endMinutes = endScreenTime?.minutes;

        if (startScreenTime && endScreenTime) {
            return (
                startHours === initialStartHours &&
                startMinutes === initialStartMinutes &&
                endHours === initialEndHours &&
                endMinutes === initialEndMinutes
            );
        }
        return true;
    }, [endTime?.hours, endTime?.minutes, startTime?.hours, startTime?.minutes, endScreenTime, startScreenTime]);

    const onStartScreenTimeChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setStartScreenTimeValue(value);
        setStartScreenTime(timeStringToTimeObject(value));
    }, []);

    const onEndScreenTimeChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setEndScreenTimeValue(value);
        setEndScreenTime(timeStringToTimeObject(value));
    }, []);

    const isScreenTimeNextDayNoteShow = useMemo(() => {
        if (!isTimeMatch(startScreenTimeValue) || !isTimeMatch(endScreenTimeValue)) {
            return false;
        }

        return startTimeStringMoreThanEndTimeString(startScreenTimeValue, endScreenTimeValue);
    }, [endScreenTimeValue, startScreenTimeValue]);

    return {
        startScreenTime,
        endScreenTime,
        startScreenTimeValue,
        endScreenTimeValue,
        onStartScreenTimeChange,
        onEndScreenTimeChange,
        isScreenTimeNotCorrect,
        isScreenTimeNotChanged,
        isScreenTimeNextDayNoteShow,
    };
};
