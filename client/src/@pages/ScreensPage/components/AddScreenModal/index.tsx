import React from 'react';
import { CircularProgress } from '@mui/material';
import { clsx } from 'clsx';

import { Select, MenuItem, AddIcon, CancelButton } from '../../../../ui-kit';
import ModalTemplate from '../../../../components/ModalTemplate';
import { useAddScreenModal } from '../../hooks/useAddScreenModal';

import css from '../../style.m.css';

const AddScreenModal: React.FC = () => {
    const {
        isAddScreenModalOpen,
        closeAddScreenModal,
        handleSubmit,
        addingScreenErrorMessage,
        locations,
        verificationHash,
        onVerificationHashChange,
        screenName,
        onScreenNameChange,
        locationNameValue,
        setActiveLocationId,
        onLocationNameSelect,
        isSubmitButtonDisabled,
        addingScreen,

        t,
    } = useAddScreenModal();

    return (
        <ModalTemplate isOpen={isAddScreenModalOpen}>
            <form className={css.popupAddScreen} onSubmit={(e) => e.preventDefault()}>
                <div className={css.fieldHeader}>
                    <h2>{t('addNewScreen')}</h2>
                </div>

                <label>{t('screenName')}</label>
                <input
                    className={css.enterNameScreen}
                    placeholder={t('enterName') as string}
                    value={screenName}
                    onChange={onScreenNameChange}
                    maxLength={125}
                />
                <div className={css.wrapHash}>
                    <label>{t('connectionCode')}</label>

                    <div>
                        <input
                            className={clsx(
                                css.enterNumberScreen,
                                !!addingScreenErrorMessage && css.enterNumberScreenError,
                                !!addingScreenErrorMessage && css.iconWarning,
                            )}
                            placeholder={t('enterCode') as string}
                            value={verificationHash}
                            onChange={onVerificationHashChange}
                            maxLength={125}
                        />
                    </div>
                    {!!addingScreenErrorMessage && <div className={css.errorHash}>{t('hashVerificationError')}</div>}
                </div>

                <div className={css.wrapScreen}>
                    <Select
                        label={t('pointSales') as string}
                        placeholder={t('selectPoint') as string}
                        onChange={onLocationNameSelect}
                        value={locationNameValue}
                        width={360}
                    >
                        {locations.map((location) => (
                            <MenuItem
                                onClick={setActiveLocationId(location.id)}
                                id={location.id}
                                key={location.id}
                                value={location.name}
                            >
                                {location.name}
                            </MenuItem>
                        ))}
                    </Select>
                </div>
                <div className={css.fieldButtons}>
                    <CancelButton text={t('btnCancel')} onClick={closeAddScreenModal} disabled={addingScreen} />

                    <button
                        className={clsx(css.buttonAddScreen, addingScreen && css.progress)}
                        type="submit"
                        onClick={handleSubmit}
                        disabled={isSubmitButtonDisabled}
                    >
                        {addingScreen ? (
                            <CircularProgress color="inherit" size={30} />
                        ) : (
                            <>
                                <AddIcon />
                                {t('btnConnect')}
                            </>
                        )}
                    </button>
                </div>
            </form>
        </ModalTemplate>
    );
};

export default AddScreenModal;
