import React, { CSSProperties } from 'react';
import { InputAdornment, CircularProgress } from '@mui/material';

import { SuggestionTooltip, SuggestListBox, CustomInput } from '../../../../ui-kit';
import { DataSuggestion } from '../../../../resolvers/screensResolvers';
import { useAutocompleteInput } from '../../hooks/useAutocompleteInput';

type Props = {
    addressQueryAddition: string;
    locationAddress: string;
    onLocationAddressChange: (value: string) => void;
    onLocationAddressSelect: (suggestion: DataSuggestion) => void;
    disabled?: boolean;

    style?: CSSProperties;
};

const AddLocationAddressInput: React.FC<Props> = ({
    addressQueryAddition,
    locationAddress,
    onLocationAddressChange,
    onLocationAddressSelect,
    disabled,
    style,
}) => {
    const {
        inputValue,
        queryValue,
        queryAddition,
        suggestList,
        onInputChange,
        onSuggestSelect,
        isLoading,
        tScreensModal,
    } = useAutocompleteInput({
        inputValue: locationAddress,
        queryAddition: addressQueryAddition,
        onInputValueChange: onLocationAddressChange,
        onSuggestValueSelect: onLocationAddressSelect,
    });

    return (
        <div>
            <div>
                <label>{tScreensModal('address')}</label>
                <SuggestionTooltip
                    title={inputValue}
                    disableHoverListener={!addressQueryAddition || inputValue.length < 40 || disabled}
                >
                    <CustomInput
                        sx={{ margin: '9px 0 16px' }}
                        style={style}
                        placeholder={tScreensModal('enterAddress')}
                        onChange={onInputChange}
                        value={inputValue}
                        disabled={!addressQueryAddition || disabled}
                        endAdornment={
                            <InputAdornment position="end">
                                <CircularProgress size={20} variant={isLoading ? 'indeterminate' : 'determinate'} />
                            </InputAdornment>
                        }
                    />
                </SuggestionTooltip>
            </div>
            <SuggestListBox
                {...{
                    queryValue,
                    queryAddition,
                    suggestList,
                    onSuggestSelect,
                }}
            />
        </div>
    );
};

export default AddLocationAddressInput;
