import React from 'react';
import { useTranslation } from 'react-i18next';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

import { getObjectTitleForSnackbar } from '../../../../hooks/getObjectTitleForSnackbar';
import { Snackbar } from '../../../../ui-kit';
import { ScreenType } from '../../../../../../common/types';

export const UpdateScreenSnackbar: React.FC<{
    screens: ScreenType[];
    onClose: () => void;
}> = ({ screens, onClose }) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const screensNumber = screens.length;
    const title = screensNumber === 1 ? getObjectTitleForSnackbar(screens[screens.length - 1].name) : '';

    return (
        <Snackbar
            startAdornment={<CheckCircleIcon color="success" />}
            title={
                screensNumber === 1
                    ? `${t('textTwentyThree')} «${title}» ${t('textTwentyFour')}`
                    : `${t('textTwentyFive')} (${screensNumber}) ${t('textTwentyFour')}`
            }
            // {
            //     screensNumber === 1
            //         ? `Настройки экрана «${title}» сохранены!`
            //         : `Настройки нескольких экранов (${screensNumber}) сохранены!`
            // }
            onClose={onClose}
        />
    );
};
