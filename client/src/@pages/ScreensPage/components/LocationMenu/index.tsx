import React from 'react';
import { clsx } from 'clsx';

import TimezoneSelect from '../TimezoneSelect';
import { Tooltip } from '../../../../ui-kit';
import { useLocationMenu } from '../../../../hooks/useLocationMenu';
import { useUpdateLocationSnackbar } from '../../hooks/useUpdateLocationSnackbar';

import css from '../../style.m.css';

const LocationMenu: React.FC = () => {
    const {
        userRole,
        screensNumber,
        locationName,
        locationAddress,
        geo_lat,
        geo_lon,
        onLocationNameChange,
        onLocationAddressChange,
        onLocationLatitudeChange,
        onLocationLongitudeChange,
        latitudeError,
        longitudeError,
        tzLabel,
        onTimezoneChange,
        handleSubmit,
        isLocationSubmitDisabled,
        isSubmitButtonShown,
        openDeleteLocationAlert,
        t,
    } = useLocationMenu();

    useUpdateLocationSnackbar();

    return (
        <form className={css.mainRightMenuSettings} onSubmit={(e) => e.preventDefault()}>
            <div className={css.wrapperFiedRightMenu1}>
                <div className={css.wrappRightMenu1}>
                    <h2>{t('pointsSettings')}</h2>
                    <div style={{ marginBottom: '5px' }}>
                        <label>{t('pointsName')}</label>
                        <br />
                        <input
                            className={css.rightMenuSetting}
                            onChange={onLocationNameChange}
                            placeholder={t('pointsNamePlaceholder') as string}
                            value={locationName}
                            required
                            maxLength={125}
                        />
                    </div>

                    <div style={{ marginBottom: '5px' }}>
                        <label>{t('pointsAddress')}</label>
                        <br />
                        <input
                            className={css.rightMenuSetting}
                            onChange={onLocationAddressChange}
                            placeholder=""
                            value={locationAddress}
                            maxLength={125}
                        />
                    </div>

                    <div style={{ marginBottom: '5px' }}>
                        <label>{t('latitude')}</label>
                        <br />
                        <input
                            className={clsx(css.rightMenuSetting, latitudeError && css.error)}
                            onChange={onLocationLatitudeChange}
                            placeholder="55.752004"
                            value={geo_lat}
                            maxLength={9}
                        />
                        {latitudeError && <div className={css.errorCoordinates}>{latitudeError}</div>}
                    </div>

                    <div style={{ marginBottom: '5px' }}>
                        <label>{t('longitude')}</label>
                        <br />
                        <input
                            className={clsx(css.rightMenuSetting, longitudeError && css.error)}
                            onChange={onLocationLongitudeChange}
                            placeholder="37.617734"
                            value={geo_lon}
                            maxLength={9}
                        />
                        {longitudeError && <div className={css.errorCoordinates}>{longitudeError}</div>}
                    </div>

                    <TimezoneSelect
                        {...{ tzLabel, onTimezoneChange, disabled: false }}
                        style={{ width: '292px', marginLeft: '24px', marginBottom: '15px' }}
                    />
                </div>
                <div className={css.fieldDeleteLocation}>
                    <Tooltip
                        title={t('btnDelPointTooltip')}
                        // "Нельзя удалить локацию, если в ней есть экраны. Удалить или перенести экран в другую локацию можно в настройках экрана."
                        placement="top"
                        disableHoverListener={screensNumber === 0}
                    >
                        <button
                            type="button"
                            className={clsx(css.btnPlaylist, css.deleteLocation, screensNumber > 0 && css.disabled)}
                            onClick={() => {
                                if (screensNumber > 0) return;
                                openDeleteLocationAlert();
                            }}
                        >
                            <img alt="" />
                            {t('btnDelPoint')}
                        </button>
                    </Tooltip>
                </div>
            </div>

            {isSubmitButtonShown && (
                <div className={css.wrapperFieldBtn}>
                    <button
                        type="submit"
                        className={clsx(css.btnPlaylist, css.saveSettings)}
                        onClick={handleSubmit}
                        disabled={isLocationSubmitDisabled}
                    >
                        {t('btnSaveSetting')}
                    </button>
                </div>
            )}
        </form>
    );
};

export default LocationMenu;
