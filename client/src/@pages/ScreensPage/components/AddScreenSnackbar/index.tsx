import React from 'react';
import { useTranslation } from 'react-i18next';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

import { getObjectTitleForSnackbar } from '../../../../hooks/getObjectTitleForSnackbar';
import { Snackbar } from '../../../../ui-kit';
import { AddScreenSuccess } from '../../types';

export const AddScreenSnackbar: React.FC<{ screen: AddScreenSuccess; onClose: () => void }> = ({ screen, onClose }) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const title = getObjectTitleForSnackbar(screen.name);

    return (
        <Snackbar
            startAdornment={<CheckCircleIcon color="success" />}
            title={`${t('textSeventeen')} «${title}» ${t('textEighteen')}`}
            // {`Экран «${title}» добавлен!`}
            onClose={onClose}
        />
    );
};
