import React from 'react';
import { clsx } from 'clsx';

import { AddButton, Tooltip } from '../../../../ui-kit';
import { getScreensInfo } from '../../helpers';
import { useLocationsList } from '../../hooks/useLocationsList';
import { locationCityAddressToString } from '../../../../constants/helpers';

import Pin from '../../../../assets/Pin4k.png';
import pointGreenOn from '../../../../assets/pointGreenOn_12x12.svg';
import pointGreenOff from '../../../../assets/pointGreenOff_12x12.svg';
import pointGreenError from '../../../../assets/pointGreenError_12x12.svg';

import css from '../../style.m.css';

const LocationsList: React.FC = () => {
    const {
        userRole,
        activeLocationId,
        isActiveScreen,
        locations,
        addLocation,
        selectLocation,
        allScreensNumber,
        allOnlineScreensNumber,
        allOfflineScreensNumber,
        allErrorScreensNumber,
        t,
    } = useLocationsList();

    return (
        <div className={css.foldersFrame}>
            <div className={css.screensPointsSale}>
                <h2>{t('points')}</h2>
                <div>
                    <Tooltip title={t('addSalesPoint')} placement="top">
                        <AddButton onClick={addLocation} />
                    </Tooltip>
                </div>
            </div>
            <div className={css.wrapperFoldersFrameList}>
                <ul className={css.foldersFrameList}>
                    <li
                        className={clsx(
                            css.folder,
                            activeLocationId === 'all' && css.active,
                            isActiveScreen && css.activeScreen,
                        )}
                        onClick={() => selectLocation('all')}
                    >
                        <h3 className={css.folderName}>
                            {t('screensAll')} <span>({allScreensNumber})</span>
                        </h3>
                        <div className={css.folderStatistics}>
                            <Tooltip title={t('on')} placement="bottom">
                                <div className={css.fieldStatisticsItem}>
                                    <img src={pointGreenOn} width="12px" height="12px" alt="" />
                                    <p className={css.folderItem}>{allOnlineScreensNumber}</p>
                                </div>
                            </Tooltip>
                            <Tooltip title={t('off')} placement="bottom">
                                <div className={css.fieldStatisticsItem}>
                                    <img src={pointGreenOff} width="12px" height="12px" alt="" />
                                    <p className={css.folderItem}>{allOfflineScreensNumber}</p>
                                </div>
                            </Tooltip>
                            <Tooltip title={t('error')} placement="bottom">
                                <div className={css.fieldStatisticsItem}>
                                    <img src={pointGreenError} width="12px" height="12px" alt="" />
                                    <p className={css.folderItem}>{allErrorScreensNumber}</p>
                                </div>
                            </Tooltip>
                        </div>
                    </li>
                    {locations.map(({ id, name, city, address, screens }) => {
                        const { screensNumber, onlineScreensNumber, offlineScreensNumber, errorScreensNumber } =
                            getScreensInfo(screens);

                        return (
                            <li
                                className={clsx(
                                    css.folderCard,
                                    activeLocationId === id && css.active,
                                    isActiveScreen && css.activeScreen,
                                )}
                                key={id}
                                onClick={() => selectLocation(id)}
                            >
                                <div className={css.folderCardInfo}>
                                    <img src={Pin} className={css.imageMap} width="48px" height="37px" alt="" />
                                    {errorScreensNumber > 0 && <span className={css.iconCardInfo} />}
                                    <div className={css.folderCardInfoText}>
                                        <h3 className={css.folderNameCard}>{name}</h3>
                                        <p className={css.folderAddressCard}>
                                            {locationCityAddressToString(city, address)}
                                        </p>
                                    </div>

                                    {/*<CircleButton />*/}
                                </div>
                                <div>
                                    <p className={css.folderItemCard}>
                                        {t('screensTotal')}: <span>{screensNumber}</span>
                                    </p>
                                    <div className={css.folderStatistics}>
                                        <Tooltip title={t('on')} placement="bottom">
                                            <div className={css.fieldStatisticsItem}>
                                                <img src={pointGreenOn} width="12px" height="12px" alt="" />
                                                <p className={css.folderItem}>{onlineScreensNumber}</p>
                                            </div>
                                        </Tooltip>
                                        <Tooltip title={t('off')} placement="bottom">
                                            <div className={css.fieldStatisticsItem}>
                                                <img src={pointGreenOff} width="12px" height="12px" alt="" />
                                                <p className={css.folderItem}>{offlineScreensNumber}</p>
                                            </div>
                                        </Tooltip>
                                        <Tooltip title={t('error')} placement="bottom">
                                            <div className={css.fieldStatisticsItem}>
                                                <img src={pointGreenError} width="12px" height="12px" alt="" />
                                                <p className={css.folderItem}>{errorScreensNumber}</p>
                                            </div>
                                        </Tooltip>
                                    </div>
                                </div>
                            </li>
                        );
                    })}
                </ul>
            </div>
        </div>
    );
};

export default LocationsList;
