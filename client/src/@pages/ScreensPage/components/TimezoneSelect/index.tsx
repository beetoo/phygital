import React, { ChangeEvent, CSSProperties } from 'react';
import { useTranslation } from 'react-i18next';
import timezones from 'timezones-list';

import { MenuItem, Select } from '../../../../ui-kit';

type Props = {
    tzLabel: string;
    onTimezoneChange: (event: ChangeEvent<HTMLInputElement>) => void;
    disabled?: boolean;

    style?: CSSProperties;
};

const TimezoneSelect: React.FC<Props> = ({ tzLabel, onTimezoneChange, disabled, style }) => {
    const { t } = useTranslation('translation', { keyPrefix: 'screens' });

    return (
        <Select
            label={t('timezone')}
            style={style}
            value={tzLabel}
            onChange={onTimezoneChange}
            placeholder={t('selectTimezone')}
            disabled={disabled}
            MenuProps={{ PaperProps: { style: { height: 300 } } }}
        >
            {timezones.map(({ label }, index) => (
                <MenuItem key={index} value={label}>
                    {label}
                </MenuItem>
            ))}
        </Select>
    );
};

export default TimezoneSelect;
