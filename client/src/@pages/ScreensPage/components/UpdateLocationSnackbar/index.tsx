import React from 'react';
import { useTranslation } from 'react-i18next';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

import { getObjectTitleForSnackbar } from '../../../../hooks/getObjectTitleForSnackbar';
import { Snackbar } from '../../../../ui-kit';
import { LocationType } from '../../../../../../common/types';

export const UpdateLocationSnackbar: React.FC<{ location: LocationType | undefined; onClose: () => void }> = ({
    location,
    onClose,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const title = location ? getObjectTitleForSnackbar(location.name) : '';

    return (
        <Snackbar
            startAdornment={<CheckCircleIcon color="success" />}
            title={`«${title}» ${t('textThirtyTwo')}`}
            onClose={onClose}
        />
    );
};
