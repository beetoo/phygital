import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Button as MUiButton, styled } from '@mui/material';

import { CloseIcon, Snackbar } from '../../../../ui-kit';
import { getObjectTitleForSnackbar } from '../../../../hooks/getObjectTitleForSnackbar';
import { ScreenType } from '../../../../../../common/types';
import { ErrorResponse } from '../../../../types';

const MoreButton = styled(MUiButton)(`
    font-size: 14px;
    color: #c2c4d4;
    text-transform: none;
`);

const Extra = styled('div')(`max-width: 513px;`);
const Wrapper = styled('div')(
    `margin: 12px 47px 18px 62px; font-size: 12px; font-weight: 400; font-style: normal; line-height: 18xp; color: #101431; font-family: Inter, sans-serif;`,
);

export const UpdateScreenErrorSnackbar: React.FC<{
    screens: ScreenType[];
    error: ErrorResponse;
    onClose?: () => void;
}> = ({ screens, error, onClose }) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const [expanded, setExpanded] = useState(false);

    const screensNumber = screens.length;
    const title = screensNumber === 1 ? getObjectTitleForSnackbar(screens[screens.length - 1].name) : '';
    const errorMessage = error?.message ?? '';

    return (
        <Snackbar
            title={
                title
                    ? `${t('textTwentyThree')}  «${title}» ${t('textTwentySix')}`
                    : `${t('textTwentyFive')} (${screensNumber}) ${t('textTwentySix')}`
            }
            onClose={onClose}
            startAdornment={<CloseIcon />}
            endAdornment={
                <MoreButton variant="text" onClick={() => setExpanded((prevValue) => !prevValue)}>
                    {!expanded ? `${t('more')}` : `${t('ok')}`}
                </MoreButton>
            }
            content={
                expanded && (
                    <Extra>
                        <Wrapper>
                            <p>{`${t('textTwentySeven')} «${errorMessage}».`}</p>
                            <p>{t('textTwentyEight')}</p>
                        </Wrapper>
                    </Extra>
                )
            }
        />
    );
};
