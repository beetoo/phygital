import React from 'react';
import { clsx } from 'clsx';

import { Tooltip } from '../../../../ui-kit';

import { useAllLocationsMenu } from '../../hooks/useAllLocationsMenu';

import pointGreenOn from '../../../../assets/pointGreenOn_12x12.svg';
import pointGreenOff from '../../../../assets/pointGreenOff_12x12.svg';
import pointGreenError from '../../../../assets/pointGreenError_12x12.svg';

import css from '../../style.m.css';

const AllLocationsMenu: React.FC = () => {
    const { screensNumber, onlineScreensNumber, offlineScreensNumber, errorScreensNumber, t } = useAllLocationsMenu();

    return (
        <div className={css.mainRightMenu}>
            <div className={css.mainRightMenuInner}>
                <h2>{t('screensAll')}</h2>
                <ul>
                    <li>
                        <h3 className={css.mainRightMenuItem}>{t('info')}</h3>
                        <p className={css.mainRightMenuAddress}>
                            {t('screensTotal')}: <span>{screensNumber}</span>
                        </p>
                        <div className={css.rightMenuStatistics}>
                            <Tooltip title={t('on')} placement="bottom">
                                <div className={css.fieldStatisticsItem}>
                                    <img src={pointGreenOn} width="16px" height="16px" alt="" />
                                    <p className={clsx(css.folderItem)}>{onlineScreensNumber}</p>
                                </div>
                            </Tooltip>
                            <Tooltip title={t('off')} placement="bottom">
                                <div className={css.fieldStatisticsItem}>
                                    <img src={pointGreenOff} width="16px" height="16px" alt="" />
                                    <p className={clsx(css.folderItem)}>{offlineScreensNumber}</p>
                                </div>
                            </Tooltip>
                            <Tooltip title={t('error')} placement="bottom">
                                <div className={css.fieldStatisticsItem}>
                                    <img src={pointGreenError} width="16px" height="16px" alt="" />
                                    <p className={clsx(css.folderItem)}>{errorScreensNumber}</p>
                                </div>
                            </Tooltip>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default AllLocationsMenu;
