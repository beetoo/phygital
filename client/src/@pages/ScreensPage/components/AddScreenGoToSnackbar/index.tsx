import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { Button as MUiButton, styled } from '@mui/material';

import { getObjectTitleForSnackbar } from '../../../../hooks/getObjectTitleForSnackbar';
import { useCustomRouter } from '../../../../hooks/useCustomRouter';
import { Snackbar } from '../../../../ui-kit';
import { setActiveLocationId, setActiveScreenId } from '../../actions';
import { AddScreenSuccess } from '../../types';

const Button = styled(MUiButton)(`
    font-size: 14px;
    color: #c2c4d4;
    text-transform: none;
`);

export const AddScreenGoToSnackbar: React.FC<{ screen: AddScreenSuccess; onClose: () => void }> = ({
    screen,
    onClose,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const { pushToPage } = useCustomRouter();
    const dispatch = useDispatch();

    const goToScreen = React.useCallback(() => {
        onClose();
        pushToPage('screens');
        dispatch(setActiveLocationId(screen.locationId));
        dispatch(setActiveScreenId(screen.id));
    }, [dispatch, onClose, pushToPage, screen.id, screen.locationId]);

    const title = getObjectTitleForSnackbar(screen.name);

    return (
        <Snackbar
            startAdornment={<CheckCircleIcon color="success" />}
            title={`${t('textSeventeen')} «${title}» ${t('textEighteen')}`}
            // {`Экран «${title}» добавлен!`}
            endAdornment={
                <Button variant="text" onClick={goToScreen}>
                    {t('textNineteen')}
                    {/* Перейти к экрану */}
                </Button>
            }
            onClose={onClose}
        />
    );
};
