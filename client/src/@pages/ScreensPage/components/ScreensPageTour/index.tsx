import React, { ComponentProps } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { Tour } from '../../../../components/Tour';
import { TourBody } from '../../../../components/Tour/components/TourBody';
import { TourHeader } from '../../../../components/Tour/components/TourHeader';
import { selectScreens } from '../../selectors';

import learnerImg from './images/learner.png';
import tagsImg from './images/tags.png';

type Steps = ComponentProps<typeof Tour>['steps'];

export const ScreensPageTour: React.FC<{
    addScreen: HTMLElement | null;
    tagTitle: HTMLHeadingElement | null;
}> = ({ addScreen, tagTitle }) => {
    const { t } = useTranslation('translation', { keyPrefix: 'learningHints' });

    const screens = useSelector(selectScreens);

    const steps = React.useMemo<Steps>(
        () => [
            addScreen && screens.length === 0
                ? {
                      name: 'addScreen',
                      target: addScreen,
                      title: (
                          <TourHeader>
                              <img src={learnerImg} width={32} height={32} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textNine')}
                                  {/* Начните с добавления экрана */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              {t('textTen')}
                              {/* Первым делом добавьте экраны, чтобы создавать и загружать на них плейлисты. */}
                          </TourBody>
                      ),
                  }
                : null,
            tagTitle
                ? {
                      name: 'addTags',
                      target: tagTitle,
                      placement: 'left-end',
                      title: (
                          <TourHeader>
                              <img src={tagsImg} width={32} height={32} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textEleven')}
                                  {/* Добавьте теги экранам */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  {t('textTwelve')}
                                  {/* Задайте экранам теги, например, «у кассы», чтобы в дальнейшем управлять группой
                                  экранов. Это позволит быстрее выбирать экраны при создании плейлистов и изменении
                                  настроек. */}
                              </p>
                              <p>
                                  {t('textThirteen')}
                                  {/* После добавления тегов не забудьте нажать кнопку «Сохранить настройки». */}
                              </p>
                          </TourBody>
                      ),
                  }
                : null,
        ],
        [addScreen, screens.length, tagTitle, t],
    );
    return <Tour steps={steps} name="screensPage" />;
};
