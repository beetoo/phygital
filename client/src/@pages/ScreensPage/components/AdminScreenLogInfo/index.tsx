import React from 'react';
import { useSelector } from 'react-redux';
import { html_beautify, js_beautify } from 'js-beautify';
import { format } from 'date-fns';

import { ScreenRequestLoggerType } from '../../../../../../common/types';
import { openTextLink } from '../../helpers';

import { selectAuthUserRole } from '../../../../selectors';
import { USER_ROLE } from '../../../../constants';

type Props = {
    log: ScreenRequestLoggerType | undefined;
};

const AdminScreenLogInfo: React.FC<Props> = ({ log }) => {
    const userRole = useSelector(selectAuthUserRole);
    const isAdmin = userRole === USER_ROLE.ADMIN;

    if (!isAdmin) return null;

    if (!log || Object.keys(log).length < 7) return <h3>No Log Info</h3>;

    const { identification, updatedAt, requestJson, message = '', error } = log;

    const downloadReqJson = () => {
        const beautifiedJson = js_beautify(requestJson || 'null', { indent_size: 2, space_in_empty_paren: true });
        openTextLink(`${identification}_json`, beautifiedJson);
    };

    const downloadMessage = () => {
        const beautifiedMessage = html_beautify(message);
        openTextLink(`${identification}_message`, beautifiedMessage);
    };

    return (
        <div style={{ marginRight: '16px' }}>
            <h3>Identification</h3>
            <p>{identification}</p>
            <h3>Updated At</h3>
            <p>{format(new Date(updatedAt), 'yyyy-MM-dd HH:mm:ss')}</p>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <h3>Request Json</h3>
                <span style={{ cursor: 'pointer', color: '#3B51F9' }} onClick={downloadReqJson}>
                    download
                </span>
            </div>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <h3>Smil</h3>
                <span style={{ cursor: 'pointer', color: '#3B51F9' }} onClick={downloadMessage}>
                    download
                </span>
            </div>
            <div style={error ? undefined : { display: 'flex', justifyContent: 'space-between', fontWeight: 'bold' }}>
                <h3 style={{ marginBottom: 0 }}>Error</h3>
                {error ? <p>{error}</p> : <div>null</div>}
            </div>
        </div>
    );
};

export default AdminScreenLogInfo;
