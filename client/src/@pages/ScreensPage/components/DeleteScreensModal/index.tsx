import React from 'react';

import { useDeleteScreensModal } from '../../hooks/useDeleteScreensModal';

import css from '../../style.m.css';

const DeleteScreensModal: React.FC = () => {
    const { screensNumber, deletingScreens, onDeleteScreens, closeModal, t } = useDeleteScreensModal();

    return (
        <form className={css.popupDeleteScreen} onSubmit={(e) => e.preventDefault()}>
            <h2>{`${t('delScreens')} (${screensNumber})`}?</h2>
            <p>
                {t('textDelScreensOne')}
                <br />
                {t('textDelScreensThree')}
            </p>
            <div className={css.containerBtn}>
                <a className={css.btnCancel}>
                    <button type="button" className={css.buttonCancel} onClick={closeModal}>
                        {t('btnCancel')}
                    </button>
                </a>
                <a>
                    <button
                        type="submit"
                        className={css.buttonDelete}
                        onClick={onDeleteScreens}
                        disabled={!!deletingScreens}
                    >
                        {t('btnDelete')}
                    </button>
                </a>
            </div>
        </form>
    );
};

export default DeleteScreensModal;
