import React from 'react';
import { clsx } from 'clsx';

import { AddIcon, CancelButton } from '../../../../ui-kit';
import { useChangePlayerModal } from '../../hooks/useChangePlayerModal';

import css from '../../style.m.css';

const ChangePlayerModal: React.FC = () => {
    const {
        closeChangeScreenModal,
        changePlayer,
        verificationHashError,
        verificationHash,
        isHashChanging,
        onVerificationHashChange,
        isSubmitButtonDisabled,

        t,
    } = useChangePlayerModal();

    return (
        <form className={css.popupAddScreen} onSubmit={(e) => e.preventDefault()}>
            <div className={css.fieldHeader}>
                <h2>{t('changePlayer')}</h2>
            </div>

            <div className={css.wrapHash}>
                <label>{t('connectionCode')}</label>

                <div>
                    <input
                        className={clsx(
                            css.enterNumberScreen,
                            !!verificationHashError && css.enterNumberScreenError,
                            !!verificationHashError && css.iconWarning,
                        )}
                        placeholder={t('enterCode') as string}
                        value={verificationHash}
                        onChange={onVerificationHashChange}
                        maxLength={125}
                    />
                </div>
                {!!verificationHashError && <div className={css.errorHash}>{t('hashVerificationError')}</div>}
            </div>

            <div className={css.fieldButtons}>
                <CancelButton text={t('btnCancel')} onClick={closeChangeScreenModal} disabled={isHashChanging} />

                <button
                    className={css.buttonAddScreen}
                    type="submit"
                    onClick={changePlayer}
                    disabled={isSubmitButtonDisabled}
                >
                    <AddIcon />
                    {t('btnConnect')}
                </button>
            </div>
        </form>
    );
};

export default ChangePlayerModal;
