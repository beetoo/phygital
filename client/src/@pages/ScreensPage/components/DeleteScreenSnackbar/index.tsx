import React from 'react';
import { useTranslation } from 'react-i18next';
import WarningRoundedIcon from '@mui/icons-material/WarningRounded';

import { ScreenType } from '../../../../../../common/types';
import { getObjectTitleForSnackbar } from '../../../../hooks/getObjectTitleForSnackbar';
import { Button, Snackbar } from '../../../../ui-kit';

export const DeleteScreenSnackbar: React.FC<{ screens: ScreenType[]; onClose: () => void; onCancel: () => void }> = ({
    screens,
    onClose,
    onCancel,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const screensNumber = screens.length;
    const title = screensNumber === 1 ? getObjectTitleForSnackbar(screens[screensNumber - 1].name) : '';

    return (
        <Snackbar
            startAdornment={<WarningRoundedIcon color="warning" />}
            title={
                title
                    ? `${t('textSeventeen')} «${title}» ${t('textTwenty')}`
                    : `${t('textTwentyOne')} (${screensNumber}) ${t('textTwentyTwo')}`
            }
            // title={title ? `Экран «${title}» удалён.` : `Несколько экранов (${screensNumber}) удалено.`}
            endAdornment={
                <Button onClick={onCancel}>
                    {t('undo')}
                    {/* Отменить */}
                </Button>
            }
            onClose={onClose}
        />
    );
};
