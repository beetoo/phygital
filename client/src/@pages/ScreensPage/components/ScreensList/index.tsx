import React from 'react';
import { clsx } from 'clsx';

import { SelectButton, Tooltip } from '../../../../ui-kit';
import ScreenshotImage from '../ScreenshotImage';
import { useScreensList } from '../../hooks/useScreensList';
import { locationCityAddressToString } from '../../../../constants/helpers';

import css from '../../style.m.css';

const ScreensList: React.FC = () => {
    const {
        activeScreenId,
        screens,
        selectActiveScreen,
        selectScreen,
        unselectScreen,
        selectAllScreens,
        unselectAllScreens,
        isAllScreensSelected,
        isScreenSelected,
        openAddScreenModal,
        t,
    } = useScreensList();

    return (
        <div className={css.screensFrame}>
            <div className={css.screensFrameCheckbox}>
                <h2>{t('screens')}</h2>

                {screens.length > 0 && (
                    <Tooltip title={t('selectAll')} placement="top">
                        <SelectButton
                            active={isAllScreensSelected}
                            onClick={isAllScreensSelected ? unselectAllScreens() : selectAllScreens()}
                        />
                    </Tooltip>
                )}
            </div>
            <div className={css.wrapperScreensFrameList}>
                <ul className={css.screensFrameList}>
                    {screens.length > 0 ? (
                        screens.map(({ id, name, location, status }) => (
                            <li
                                className={clsx(
                                    css.frame,
                                    (activeScreenId === id || isScreenSelected(id)) && css.active,
                                )}
                                key={id}
                                onClick={selectActiveScreen(id)}
                            >
                                <ScreenshotImage
                                    timeout={30}
                                    className={css.frameImage}
                                    screenId={id}
                                    width="177"
                                    height="107"
                                />

                                <div className={css.frameInfo}>
                                    <p
                                        className={
                                            status === 'online'
                                                ? css.statusOn
                                                : ['offline', 'pending'].includes(status)
                                                  ? css.statusOff
                                                  : css.statusError
                                        }
                                    >
                                        {status === 'online'
                                            ? t('online')
                                            : ['offline', 'pending'].includes(status)
                                              ? t('offline')
                                              : t('error')}
                                    </p>
                                    <div className={css.fieldFrameName}>
                                        <p className={css.frameName}>
                                            <Tooltip title={name} placement="top">
                                                <span>{name}</span>
                                            </Tooltip>
                                        </p>
                                    </div>
                                    <div className={css.fieldFrameItem}>
                                        <p className={css.frameItem}>
                                            <Tooltip title={location?.name} placement="top">
                                                <span>{location?.name}</span>
                                            </Tooltip>
                                        </p>
                                    </div>
                                    <div className={css.fieldFrameAddress}>
                                        <p className={css.frameAddress}>
                                            <Tooltip
                                                title={locationCityAddressToString(location?.city, location?.address)}
                                                placement="top"
                                            >
                                                <span>
                                                    {locationCityAddressToString(location?.city, location?.address)}
                                                </span>
                                            </Tooltip>
                                        </p>
                                    </div>
                                </div>

                                <SelectButton
                                    active={isScreenSelected(id)}
                                    onClick={(event) => {
                                        event.stopPropagation();
                                        isScreenSelected(id) ? unselectScreen(id) : selectScreen(id);
                                    }}
                                />
                            </li>
                        ))
                    ) : (
                        <div className={css.screensEmpty}>
                            <p className={css.screensEmpty1}>
                                {t('textInfoOne')}
                                <br />
                                <a onClick={openAddScreenModal}>{t('textInfoTwo')}</a>
                                {t('textInfoThree')}
                            </p>
                        </div>
                    )}
                </ul>
            </div>
        </div>
    );
};

export default ScreensList;
