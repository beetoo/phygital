import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import WarningRoundedIcon from '@mui/icons-material/WarningRounded';

import { getObjectTitleForSnackbar } from '../../../../hooks/getObjectTitleForSnackbar';
import { Snackbar } from '../../../../ui-kit';
import { selectScreens } from '../../selectors';

export const DeleteOfflineScreensSnackbar: FC<{
    screenIds: string[];
    onClose: () => void;
}> = ({ screenIds, onClose }) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const screens = useSelector(selectScreens).filter(({ id }) => screenIds.includes(id));
    const title = screens.length === 1 ? getObjectTitleForSnackbar(screens[screens.length - 1].name) : '';

    return (
        <Snackbar
            startAdornment={<WarningRoundedIcon color="warning" />}
            title={
                title ? `${t('textThirtySix')} «${title}» ${t('textThirtySeven')}` : `${t('textThirtyEight')} `
                // ? `Экран «${title}» не был удалён, так как с ним потеряна связь.`
                // : 'Некоторые экраны не были удалены, так как с ними потеряна связь.'
            }
            onClose={onClose}
        />
    );
};
