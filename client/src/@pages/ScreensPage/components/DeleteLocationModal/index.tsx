import React from 'react';

import ModalTemplate from '../../../../components/ModalTemplate';

import { useDeleteLocationModal } from '../../hooks/useDeleteLocationModal';

import css from '../../style.m.css';

const DeleteLocationModal: React.FC = () => {
    const { isDeleteLocationModalOpen, isDeletingLocation, onDeleteLocation, closeModal, t } = useDeleteLocationModal();

    return (
        <ModalTemplate isOpen={isDeleteLocationModalOpen}>
            <form className={css.popupDeleteScreen} onSubmit={(e) => e.preventDefault()}>
                <h2>{t('delPoint')}?</h2>
                <p>
                    {t('textDelScreensFour1')}
                    <br />
                    {t('textDelScreensFour2')}
                </p>
                <div className={css.containerBtn}>
                    <a className={css.btnCancel}>
                        <button
                            type="button"
                            className={css.buttonCancel}
                            onClick={closeModal}
                            disabled={isDeletingLocation}
                        >
                            {t('btnCancel')}
                        </button>
                    </a>
                    <a>
                        <button
                            type="submit"
                            className={css.buttonDelete}
                            onClick={onDeleteLocation}
                            disabled={isDeletingLocation}
                        >
                            {t('btnDelete')}
                        </button>
                    </a>
                </div>
            </form>
        </ModalTemplate>
    );
};

export default DeleteLocationModal;
