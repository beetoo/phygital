import React from 'react';

import { useDeleteScreensModal } from '../../hooks/useDeleteScreensModal';

import css from '../../style.m.css';

const DeleteScreenModal: React.FC = () => {
    const { deletingScreens, onDeleteScreens, closeModal, t } = useDeleteScreensModal();

    return (
        <div className={css.popupDeleteScreen}>
            <h2>{t('delScreen')}?</h2>
            <p>
                {t('textDelScreensOne')}
                <br />
                {t('textDelScreensTwo')}
            </p>
            <div className={css.containerBtn}>
                <a className={css.btnCancel}>
                    <button className={css.buttonCancel} onClick={closeModal}>
                        {t('btnCancel')}
                    </button>
                </a>
                <a>
                    <button className={css.buttonDelete} onClick={onDeleteScreens} disabled={!!deletingScreens}>
                        {t('btnDelete')}
                    </button>
                </a>
            </div>
        </div>
    );
};

export default DeleteScreenModal;
