import React from 'react';
import WarningRoundedIcon from '@mui/icons-material/WarningRounded';
import { useTranslation } from 'react-i18next';

import { getObjectTitleForSnackbar } from '../../../../hooks/getObjectTitleForSnackbar';
import { Button, Snackbar } from '../../../../ui-kit';
import { LocationType } from '../../../../../../common/types';

export const LocationDeletedSnackbar: React.FC<{
    location: LocationType;
    onClose: () => void;
    onCancel: () => void;
}> = ({ location, onClose, onCancel }) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const title = getObjectTitleForSnackbar(location.name);

    return (
        <Snackbar
            startAdornment={<WarningRoundedIcon color="warning" />}
            title={`${t('textFifteen')} «${title}» ${t('textSixteen')}`}
            // {`Локация «${title}» удалена.`}
            endAdornment={
                <Button onClick={onCancel}>
                    {t('undo')}
                    {/* Отменить */}
                </Button>
            }
            onClose={onClose}
        />
    );
};
