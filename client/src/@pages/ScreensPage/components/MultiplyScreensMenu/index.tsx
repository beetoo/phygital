import React from 'react';
import { clsx } from 'clsx';

import { Tooltip, Select, MenuItem } from '../../../../ui-kit';
import { useMultiplyScreensMenu } from '../../hooks/useMultiplyScreensMenu';

import css from '../../style.m.css';

const MultiplyScreensMenu: React.FC = () => {
    const {
        selectedScreensNumber,
        savedTags,
        notSavedTags,
        isSubmitButtonDisabled,
        openDeleteScreensModal,
        tag,
        onTagNameChange,
        onTagEnter,
        onDeleteSavedTag,
        onDeleteNotSavedTag,
        onHandleSubmit,
        unselectAllScreens,
        selectableLocations,
        locationSelectValue,
        setActiveLocationId,
        onLocationSelect,
        tScreensModal,
        tScreens,
    } = useMultiplyScreensMenu();

    return (
        <div className={css.mainRightMenuSettingsScreenSelect}>
            <div className={clsx(css.wrapperFiedRightMenu, css.wrappRightMenu1, css.rightMenuScreen)}>
                <h2>{tScreens('screenSettings')}</h2>
                <h3 className={css.screenSelectItem1}>
                    {tScreens('screensSelected')}: {selectedScreensNumber}
                </h3>
                <button className={css.deleteScreen} onClick={openDeleteScreensModal}>
                    <img alt="" />
                    {selectedScreensNumber > 1 ? `${tScreens('delScreens')}` : `${tScreens('delScreen')}`}
                    {/* Удалить {selectedScreensNumber > 1 ? 'экраны' : 'экран'} */}
                </button>
                <div className={css.wrapScreenSelectItem2}>
                    <h3 className={css.screenSelectItem2}>{tScreens('tags')}</h3>
                    <Tooltip title={tScreens('tagTooltip')} placement="top-end">
                        <p className={css.infoBtn}></p>
                    </Tooltip>
                </div>
                <ul className={css.tagButton}>
                    {savedTags.map((name) => (
                        <li key={name} className={css.tagButtonItem}>
                            <button>{name}</button>
                            <span className={css.iconX} onClick={onDeleteSavedTag(name)} />
                        </li>
                    ))}
                    {notSavedTags.map((name) => (
                        <li key={name} className={css.tagButtonItem}>
                            <button>{name}</button>
                            <span className={css.iconX} onClick={onDeleteNotSavedTag(name)} />
                        </li>
                    ))}
                </ul>
                <div className={css.inputAddTag}>
                    <input
                        className={clsx(css.rightMenuSetting, css.addTag)}
                        placeholder={tScreens('tagsAdd') as string}
                        value={tag}
                        onChange={onTagNameChange}
                        onKeyPress={onTagEnter}
                        maxLength={125}
                    />
                    <label>
                        {tScreens('tagTextOne')} <span>{tScreens('tagTextTwo')}</span> {tScreens('tagTextThree')}
                    </label>
                </div>
                {selectableLocations.length > 0 && (
                    <>
                        <h3 className={css.screenSelectItem3}>{tScreens('moveScreen')}</h3>
                        <Select
                            placeholder={tScreensModal('selectPointShort') as string}
                            onChange={onLocationSelect}
                            value={locationSelectValue}
                            width={292}
                            style={{ marginLeft: '24px' }}
                        >
                            {selectableLocations.map((location) => (
                                <MenuItem
                                    onClick={setActiveLocationId(location.id)}
                                    key={location.id}
                                    value={location.name}
                                >
                                    {location.name}
                                </MenuItem>
                            ))}
                        </Select>
                    </>
                )}
            </div>

            <div className={clsx(css.wrapperFieldBtn, !isSubmitButtonDisabled && css.doubleButton)}>
                <button className={clsx(css.btnPlaylist, css.deselect)} onClick={unselectAllScreens}>
                    {tScreens('btnUnselect')}
                </button>

                {!isSubmitButtonDisabled && (
                    <button className={clsx(css.btnPlaylist, css.saveSettings)} onClick={onHandleSubmit}>
                        {tScreens('btnSaveSetting')}
                    </button>
                )}
            </div>
        </div>
    );
};

export default MultiplyScreensMenu;
