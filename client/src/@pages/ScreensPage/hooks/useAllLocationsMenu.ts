import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectScreens } from '../selectors';
import { getScreensInfo } from '../helpers';

type ReturnValue = {
    screensNumber: number;
    onlineScreensNumber: number;
    offlineScreensNumber: number;
    errorScreensNumber: number;
    t: TFunction;
};

export const useAllLocationsMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'screens' });

    const screens = useSelector(selectScreens);

    const { screensNumber, onlineScreensNumber, offlineScreensNumber, errorScreensNumber } = getScreensInfo(screens);

    return {
        screensNumber,
        onlineScreensNumber,
        offlineScreensNumber,
        errorScreensNumber,
        t,
    };
};
