import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setHideModal } from '../../../components/ModalsContainer/actions';
import { setDeleteScreens, setSelectedScreenIds } from '../actions';
import {
    selectActiveScreen,
    selectDeleteScreenStatus,
    selectDeleteScreenSuccessStatus,
    selectSelectedScreens,
} from '../selectors';
import { ScreenType } from '../../../../../common/types';

type ReturnValue = {
    screensNumber: number;
    deletingScreens: ScreenType[] | false;
    onDeleteScreens: () => void;
    closeModal: () => void;
    t: TFunction;
};

export const useDeleteScreensModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'screensModal' });

    const dispatch = useDispatch();

    const activeScreen = useSelector(selectActiveScreen);
    const selectedScreens = useSelector(selectSelectedScreens);
    const isDeleteScreensSuccessStatus = useSelector(selectDeleteScreenSuccessStatus);
    const deletingScreens = useSelector(selectDeleteScreenStatus);

    const onDeleteScreens = useCallback(() => {
        const deletedScreens = selectedScreens.length > 0 ? selectedScreens : activeScreen ? [activeScreen] : [];
        dispatch(setDeleteScreens(deletedScreens));
    }, [activeScreen, dispatch, selectedScreens]);

    useEffect(() => {
        if (isDeleteScreensSuccessStatus) {
            dispatch(setHideModal());
            dispatch(setSelectedScreenIds());
        }
    }, [dispatch, isDeleteScreensSuccessStatus]);

    const closeModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    const screensNumber = selectedScreens.length;

    return {
        screensNumber,
        deletingScreens,
        onDeleteScreens,
        closeModal,
        t,
    };
};
