import { useEffect, useState } from 'react';
import { format } from 'date-fns';

import { httpService } from '../../../helpers/httpService';
import { baseURL } from '../../../helpers/httpService/baseUrl';

export type ScreenMessageItem = {
    id: string;
    createdAt: string;
    message: string;
};

type Props = {
    fetchedId: string;
    fetch: boolean;
};

type ReturnValue = {
    screenMessages: ScreenMessageItem[];
};

export const useScreenMessages = ({ fetchedId, fetch }: Props): ReturnValue => {
    const [screenMessages, setScreenMessages] = useState<ScreenMessageItem[]>([]);

    useEffect(() => {
        if (fetch) {
            httpService.get(`${baseURL}/screen/messages/${fetchedId}`).then((data) => {
                const screenMessages = (data as ScreenMessageItem[]).map((item) => ({
                    ...item,
                    createdAt: format(new Date(item.createdAt), 'HH:mm, dd.MM.yyyy '),
                }));

                setScreenMessages(screenMessages);
            });
        }
    }, [fetch, fetchedId]);

    return { screenMessages };
};
