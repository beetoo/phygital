import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';

import { DEFAULT_CANCEL_TIMEOUT } from '../../../../../common/constants';
import { ActionCancelledSnackbar } from '../../../components/ActionCancelledSnackbar';
import { cancelDeleteLocation } from '../../../resolvers/screensResolvers';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { showErrorSnackbar } from '../../../utils';
import { setClearDeleteLocation, setGetLocations } from '../actions';
import { LocationDeletedSnackbar } from '../components/LocationDeletedSnackbar';
import { selectDeleteLocationErrorMessage, selectDeleteLocationSuccessStatus } from '../selectors';

export const useDeleteLocationSnackbar = (): void => {
    const dispatch = useDispatch();
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const deleteLocationSuccess = useSelector(selectDeleteLocationSuccessStatus);
    const deleteLocationError = useSelector(selectDeleteLocationErrorMessage);

    const onClose = useCallback(() => {
        dispatch(setClearDeleteLocation());
        setTimeout(() => {
            if (organizationId) {
                dispatch(setGetLocations(organizationId));
            }
        }, 500);
    }, [dispatch, organizationId]);

    const onCancel = useCallback(() => {
        if (deleteLocationSuccess) {
            cancelDeleteLocation(deleteLocationSuccess.cancelId).then(
                () => {
                    const close = () => {
                        closeSnackbar(key);
                        onClose();
                    };
                    const key = enqueueSnackbar(<ActionCancelledSnackbar onClose={close} />, {
                        key: `DeleteLocationSuccess:${deleteLocationSuccess.cancelId}`,
                    });
                },
                () =>
                    showErrorSnackbar({
                        enqueueSnackbar,
                        closeSnackbar,
                        key: `CancelDeleteLocationError:${deleteLocationSuccess.cancelId}`,
                    }),
            );
        }

        onClose();
    }, [closeSnackbar, deleteLocationSuccess, enqueueSnackbar, onClose]);

    useEffect(() => {
        if (deleteLocationSuccess) {
            const close = () => {
                closeSnackbar(key);
                onClose();
            };
            const key = enqueueSnackbar(
                <LocationDeletedSnackbar
                    location={deleteLocationSuccess.location}
                    onCancel={onCancel}
                    onClose={close}
                />,
                {
                    key: deleteLocationSuccess.cancelId,
                },
            );
            const autoHide = setTimeout(close, deleteLocationSuccess.timeout || DEFAULT_CANCEL_TIMEOUT);
            return () => {
                close();
                clearTimeout(autoHide);
            };
        }
    }, [closeSnackbar, deleteLocationSuccess, enqueueSnackbar, onCancel, onClose]);

    useEffect(() => {
        if (deleteLocationError) {
            showErrorSnackbar({
                enqueueSnackbar,
                closeSnackbar,
                key: `CancelDeleteLocationError:${deleteLocationError.location.id}`,
                onClose,
            });
        }
    }, [closeSnackbar, deleteLocationError, enqueueSnackbar, onClose]);
};
