import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setOpenAddScreenModal } from '../actions';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { selectScreens } from '../selectors';
import { useDeleteScreenSnackbar } from './useDeleteScreenSnackbar';
import { useAddLocationSnackbar } from './useAddLocationSnackbar';
import { useDeleteLocationSnackbar } from './useDeleteLocationSnackbar';
import { selectAuthUserRole } from '../../../selectors';
import { USER_ROLE } from '../../../constants';
import { setShowModalNew } from '../../../components/ModalsContainer/actions';
import { MODALS } from '../../../components/ModalsContainer/types';

type ReturnValue = {
    userRole: USER_ROLE;
    openAddScreenModal: () => void;
    t: TFunction;
};

export const useScreensPage = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'screens' });

    const dispatch = useDispatch();

    const userRole = useSelector(selectAuthUserRole);
    const { limitScreens } = useSelector(selectCurrentOrganization);
    const screens = useSelector(selectScreens);

    const isOpenAddModalButtonDisabled = screens.length >= limitScreens;

    const openAddScreenModal = useCallback(() => {
        if (isOpenAddModalButtonDisabled) {
            dispatch(setShowModalNew(MODALS.MAX_SCREENS_ALERT_MODAL, null));
        } else {
            dispatch(setOpenAddScreenModal());
        }
    }, [dispatch, isOpenAddModalButtonDisabled]);

    // уведомления
    useAddLocationSnackbar();
    useDeleteLocationSnackbar();
    useDeleteScreenSnackbar();

    return {
        userRole,
        openAddScreenModal,
        t,
    };
};
