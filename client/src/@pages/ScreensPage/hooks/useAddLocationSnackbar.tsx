import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';

import { setActiveLocationId } from '../actions';
import { AddLocationSnackbar } from '../components/AddLocationSnackbar';
import { selectAddLocationSuccessStatus } from '../selectors';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';

export const useAddLocationSnackbar = (): void => {
    const dispatch = useDispatch();

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const addLocationSuccess = useSelector(selectAddLocationSuccessStatus);

    // после успешного добавления экрана показываем Snackbar с возможностью перехода на экран
    useEffect(() => {
        if (addLocationSuccess) {
            const closeSnackbarKey = enqueueSnackbar(
                <AddLocationSnackbar location={addLocationSuccess} onClose={() => closeSnackbar(closeSnackbarKey)} />,
                {
                    key: `AddLocationSnackbar:${organizationId}:${addLocationSuccess.id}`,
                    autoHideDuration: 3000,
                },
            );
            dispatch(setActiveLocationId(addLocationSuccess.id));
        }
    }, [addLocationSuccess, closeSnackbar, dispatch, enqueueSnackbar, organizationId]);
};
