import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { ScreenType } from '../../../../../common/types';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import {
    setActiveScreenId,
    setClearScreensSuccess,
    setGetScreens,
    setOpenAddScreenModal,
    setSelectedScreenIds,
} from '../actions';

import {
    selectActiveLocationId,
    selectActiveScreenId,
    selectAddScreenSuccessStatus,
    selectScreens,
    selectSelectedScreensIds,
} from '../selectors';
import { setShowModalNew } from '../../../components/ModalsContainer/actions';
import { MODALS } from '../../../components/ModalsContainer/types';
import { selectAuthUserRole } from '../../../selectors';
import { USER_ROLE } from '../../../constants';

type ReturnValue = {
    activeScreenId: string;
    screens: ScreenType[];
    selectActiveScreen: (activeScreenId: string) => () => void;

    selectScreen: (screenId: string) => void;
    unselectScreen: (screenId: string) => void;
    selectAllScreens: () => () => void;
    unselectAllScreens: () => () => void;
    isAllScreensSelected: boolean;
    isScreenSelected: (screenName: string) => boolean;
    openAddScreenModal: () => void;
    t: TFunction;
};

export const useScreensList = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'screens' });

    const dispatch = useDispatch();

    const userRole = useSelector(selectAuthUserRole);
    const { id: organizationId, limitScreens } = useSelector(selectCurrentOrganization);
    const activeLocationId = useSelector(selectActiveLocationId);
    const activeScreenId = useSelector(selectActiveScreenId);
    const selectedScreens = useSelector(selectSelectedScreensIds);
    const allScreens = useSelector(selectScreens);
    const addScreenSuccess = useSelector(selectAddScreenSuccessStatus);

    const isOpenAddModalButtonDisabled = allScreens.length >= limitScreens;

    const openAddScreenModal = useCallback(() => {
        if (isOpenAddModalButtonDisabled) {
            dispatch(setShowModalNew(MODALS.MAX_SCREENS_ALERT_MODAL, null));
        } else {
            dispatch(setOpenAddScreenModal());
        }
    }, [dispatch, isOpenAddModalButtonDisabled]);

    useEffect(() => {
        if (organizationId) {
            dispatch(setGetScreens(organizationId, 'all', userRole === USER_ROLE.ADMIN));
        }
    }, [dispatch, organizationId, userRole]);

    // очищаем screenSuccess при заходе на страницу экранов
    useEffect(() => {
        if (addScreenSuccess) {
            dispatch(setClearScreensSuccess());
        }
    }, [addScreenSuccess, dispatch]);

    const screens = useMemo(
        () => allScreens.filter(({ location }) => activeLocationId === 'all' || location?.id === activeLocationId),
        [activeLocationId, allScreens],
    );

    const selectActiveScreen = useCallback(
        (activeScreenId: string) => () => {
            if (selectedScreens.length === 0) {
                dispatch(setActiveScreenId(activeScreenId));
            }
        },
        [dispatch, selectedScreens.length],
    );

    const selectScreen = useCallback(
        (screenId: string) => {
            dispatch(setActiveScreenId());
            dispatch(setSelectedScreenIds([...selectedScreens, screenId]));
        },
        [dispatch, selectedScreens],
    );

    const unselectScreen = useCallback(
        (screenId: string) => {
            dispatch(setSelectedScreenIds(selectedScreens.filter((id) => id !== screenId)));
        },
        [dispatch, selectedScreens],
    );

    const selectAllScreens = useCallback(
        () => () => {
            dispatch(setSelectedScreenIds(screens.map(({ id }) => id)));
        },
        [dispatch, screens],
    );

    const unselectAllScreens = useCallback(
        () => () => {
            dispatch(setSelectedScreenIds());
        },
        [dispatch],
    );

    const isAllScreensSelected = selectedScreens.length > 0 && selectedScreens.length === screens.length;

    const isScreenSelected = useCallback(
        (screenId: string) => selectedScreens.some((id) => id === screenId),
        [selectedScreens],
    );

    return {
        activeScreenId,
        screens,
        selectActiveScreen,
        selectScreen,
        unselectScreen,
        selectAllScreens,
        unselectAllScreens,
        isAllScreensSelected,
        isScreenSelected,
        openAddScreenModal,
        t,
    };
};
