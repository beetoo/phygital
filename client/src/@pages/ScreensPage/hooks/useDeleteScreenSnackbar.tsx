import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';

import { DEFAULT_CANCEL_TIMEOUT } from '../../../../../common/constants';
import { ActionCancelledSnackbar } from '../../../components/ActionCancelledSnackbar';
import { cancelDeleteScreen } from '../../../resolvers/screensResolvers';
import { showErrorSnackbar } from '../../../utils';
import { setClearDeleteScreens, setGetScreens } from '../actions';
import { selectDeleteScreenErrorMessage, selectDeleteScreenSuccessStatus } from '../selectors';
import { DeleteScreenSnackbar } from '../components/DeleteScreenSnackbar';
import { DeleteOfflineScreensSnackbar } from '../components/DeleteOfflineScreensSnackbar';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { selectAuthUserRole } from '../../../selectors';
import { USER_ROLE } from '../../../constants';
import { setGetOrganization } from '../../ProfilePage/actions';

export const useDeleteScreenSnackbar = (): void => {
    const dispatch = useDispatch();
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const deleteScreenSuccess = useSelector(selectDeleteScreenSuccessStatus);
    const deleteScreenError = useSelector(selectDeleteScreenErrorMessage);
    const userRole = useSelector(selectAuthUserRole);

    const showNotDeletedScreensSnackbar = useCallback(() => {
        if (deleteScreenSuccess && deleteScreenSuccess.offlineScreenIds) {
            const close = () => {
                closeSnackbar(key);
                clearTimeout(autoHide);
            };
            const autoHide = setTimeout(close, 3000);

            const key = enqueueSnackbar(
                <DeleteOfflineScreensSnackbar screenIds={deleteScreenSuccess.offlineScreenIds} onClose={close} />,
                {
                    key: `ScreenOfflineNotDeleted:${deleteScreenSuccess.offlineScreenIds}`,
                },
            );
        }
    }, [closeSnackbar, deleteScreenSuccess, enqueueSnackbar]);

    const onClose = useCallback(
        (fetchScreens = true, deleteCancel = false) => {
            dispatch(setClearDeleteScreens());
            if (fetchScreens) {
                dispatch(setGetScreens(organizationId, 'all', userRole === USER_ROLE.ADMIN));
            }

            if (!deleteCancel) {
                showNotDeletedScreensSnackbar();
            }
        },
        [dispatch, organizationId, showNotDeletedScreensSnackbar, userRole],
    );

    const onCancel = useCallback(() => {
        if (deleteScreenSuccess) {
            cancelDeleteScreen(deleteScreenSuccess.cancelId).then(
                () => {
                    const close = () => {
                        closeSnackbar(key);
                        onClose(true, true);
                    };
                    const key = enqueueSnackbar(<ActionCancelledSnackbar onClose={close} />, {
                        key: `DeleteScreenSuccess:${deleteScreenSuccess.cancelId}`,
                    });
                },
                () =>
                    showErrorSnackbar({
                        enqueueSnackbar,
                        closeSnackbar,
                        key: `CancelDeleteScreenError:${deleteScreenSuccess.cancelId}`,
                    }),
            );
        }

        onClose();
    }, [closeSnackbar, deleteScreenSuccess, enqueueSnackbar, onClose]);

    useEffect(() => {
        if (deleteScreenSuccess) {
            const close = () => {
                closeSnackbar(key);
                // обновляем информацию по организации после закрытия снекбара
                setTimeout(() => dispatch(setGetOrganization(organizationId)), 500);
                // при закрытии снекбара делаем запрос на получение экранов только если были неудаленные экраны
                onClose(!!deleteScreenSuccess.offlineScreenIds);
            };

            const key = enqueueSnackbar(
                <DeleteScreenSnackbar screens={deleteScreenSuccess.screens} onCancel={onCancel} onClose={close} />,
                {
                    key: deleteScreenSuccess.cancelId,
                },
            );
            const autoHide = setTimeout(close, deleteScreenSuccess.timeout || DEFAULT_CANCEL_TIMEOUT);

            return () => {
                clearTimeout(autoHide);
            };
        }
    }, [closeSnackbar, deleteScreenSuccess, dispatch, enqueueSnackbar, onCancel, onClose, organizationId]);

    useEffect(() => {
        if (deleteScreenError) {
            showErrorSnackbar({
                enqueueSnackbar,
                closeSnackbar,
                key:
                    deleteScreenError.screens.length === 1
                        ? `CancelDeleteScreenError:${deleteScreenError.screens[0].id}`
                        : `CancelDeleteScreensError:${deleteScreenError.screens.map(({ id }) => id)}`,
                onClose,
            });
        }
    }, [closeSnackbar, deleteScreenError, enqueueSnackbar, onClose]);
};
