import { ChangeEvent, useCallback, useEffect, useRef, useState } from 'react';
import { map } from 'rxjs';
import { AxiosResponse } from 'axios';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { useDebouncedState } from '../../../hooks/useDebouncedState';
import {
    DataSuggestion,
    fromGetDaDataLocationSuggestList,
    fromGetOSMLocationSuggestList,
} from '../../../resolvers/screensResolvers';
import { isProductionEngClient } from '../../../constants/environments';

type Props = {
    inputValue: string;
    queryAddition?: string;
    onInputValueChange: (value: string) => void;
    onSuggestValueSelect: (suggestion: DataSuggestion) => void;
};

type ReturnValue = {
    inputValue: string;
    queryValue: string;
    queryAddition: string;
    suggestList: DataSuggestion[];
    onInputChange: (event: ChangeEvent<HTMLInputElement>) => void;
    onSuggestSelect: (suggestion: DataSuggestion) => () => void;
    isLoading: boolean;
    tScreens: TFunction;
    tScreensModal: TFunction;
};

type OpenStreetMapResponse = {
    display_name: string;
    lat: string;
    lon: string;
};

export const useAutocompleteInput = ({
    inputValue,
    queryAddition = '',
    onInputValueChange,
    onSuggestValueSelect,
}: Props): ReturnValue => {
    const { t: tScreens } = useTranslation('translation', { keyPrefix: 'screens' });
    const { t: tScreensModal } = useTranslation('translation', { keyPrefix: 'screensModal' });

    const [suggestList, setSuggestList] = useState<DataSuggestion[]>([]);
    const [queryValue, setQueryValue] = useDebouncedState('', 1500);
    const [isSuggestListRequired, setIsSuggestListRequired] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const onInputChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            setIsLoading(!!value);
            setIsSuggestListRequired(true);
            onInputValueChange(value);
        },
        [onInputValueChange],
    );

    const onSuggestSelect = useCallback(
        (suggestion: DataSuggestion) => () => {
            setIsSuggestListRequired(false);
            onSuggestValueSelect(suggestion);
        },
        [onSuggestValueSelect],
    );

    useEffect(() => {
        if (inputValue) {
            setQueryValue(inputValue);
        }
    }, [inputValue, setQueryValue]);

    const prevQueryValue = useRef('');
    useEffect(() => {
        if (isSuggestListRequired) {
            if (queryValue && queryValue !== prevQueryValue.current) {
                prevQueryValue.current = queryValue;

                if (!(queryAddition + inputValue).trim()) return;

                if (isProductionEngClient || !inputValue.match(/[а-я]/i)) {
                    fromGetOSMLocationSuggestList({ query: inputValue, queryAddition })
                        .pipe(
                            map((response: AxiosResponse) =>
                                response.data.map(({ display_name, lat, lon }: OpenStreetMapResponse) => ({
                                    value: display_name,
                                    data: { geo_lat: lat, geo_lon: lon },
                                })),
                            ),
                        )
                        .subscribe((suggestions: DataSuggestion[]) => {
                            setIsLoading(false);
                            setSuggestList(suggestions);
                        });
                } else {
                    fromGetDaDataLocationSuggestList({ query: inputValue, queryAddition })
                        .pipe(
                            map((response: AxiosResponse) =>
                                response.data.suggestions
                                    .filter(
                                        ({
                                            data: {
                                                street_type_full,
                                                city_type_full,
                                                settlement_type_full,
                                                geo_lat,
                                                geo_lon,
                                            },
                                        }: DataSuggestion) =>
                                            queryAddition === ''
                                                ? (city_type_full || settlement_type_full) && street_type_full === null
                                                : geo_lat && geo_lon,
                                    )
                                    .map(({ value, data }: DataSuggestion) => ({
                                        value: queryAddition ? value.slice(queryAddition.length + 2) : value,
                                        data,
                                    })),
                            ),
                        )
                        .subscribe((suggestions: DataSuggestion[]) => {
                            setIsLoading(false);
                            setSuggestList(suggestions);
                        });
                }
            }
        }
    }, [inputValue, isSuggestListRequired, queryAddition, queryValue]);

    useEffect(() => {
        if (inputValue === '') {
            setSuggestList([]);
        }
    }, [inputValue]);

    useEffect(() => {
        const listener = () => {
            if (suggestList.length > 0) {
                setIsSuggestListRequired(false);
                setSuggestList([]);
            }
        };
        document.addEventListener('click', listener);

        return () => {
            document.removeEventListener('click', listener);
        };
    }, [suggestList.length]);

    return {
        inputValue,
        queryValue,
        queryAddition,
        suggestList,
        onInputChange,
        onSuggestSelect,
        isLoading,
        tScreens,
        tScreensModal,
    };
};
