import { ChangeEvent, useCallback, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import { sortBy } from 'lodash';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { setAddScreen, setClearScreensError, setCloseAddScreenModal } from '../actions';
import {
    selectAddingScreenErrorMessage,
    selectAddingScreenStatus,
    selectAddScreenModalOpen,
    selectLocations,
} from '../selectors';
import { useAddScreenSnackbar } from './useAddScreenSnackbar';
import { LocationType } from '../../../../../common/types';

type ReturnValue = {
    isAddScreenModalOpen: boolean;
    addingScreen: boolean;
    handleSubmit: () => void;
    addingScreenErrorMessage: string;
    closeAddScreenModal: () => void;
    locations: LocationType[];

    verificationHash: string;
    onVerificationHashChange: ({ target: { value } }: ChangeEvent<HTMLInputElement>) => void;

    screenName: string;
    onScreenNameChange: ({ target: { value } }: ChangeEvent<HTMLInputElement>) => void;

    setActiveLocationId: (locationId: string) => () => void;

    locationNameValue: '' | string;
    onLocationNameSelect: ({ target: { value } }: SelectChangeEvent) => void;

    isSubmitButtonDisabled: boolean;

    t: TFunction;
};

export const useAddScreenModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'screensModal' });

    const dispatch = useDispatch();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const isAddScreenModalOpen = useSelector(selectAddScreenModalOpen);
    const addingScreen = useSelector(selectAddingScreenStatus);
    let locations = useSelector(selectLocations);
    locations = sortBy(locations, ({ name }) => name.replace(/\s\(\d+\)$/, ''));
    const addingScreenErrorMessage = useSelector(selectAddingScreenErrorMessage);

    const [screenName, setScreenName] = useState('');
    const [verificationHash, setVerificationHash] = useState('');

    const { id, name } = useMemo(() => (locations.length === 1 ? locations[0] : { id: '', name: '' }), [locations]);

    const [locationId, setLocationId] = useState(id);
    const [locationNameValue, setLocationNameValue] = useState<'' | string>(name);

    const onScreenNameChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setScreenName(value);
    }, []);

    const onVerificationHashChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            // очищаем ошибку хеша
            if (addingScreenErrorMessage) {
                dispatch(setClearScreensError());
            }
            setVerificationHash(value);
        },
        [addingScreenErrorMessage, dispatch],
    );

    const setActiveLocationId = useCallback(
        (locationId: string) => () => {
            setLocationId(locationId);
        },
        [],
    );

    const onLocationNameSelect = useCallback(({ target: { value } }: SelectChangeEvent) => {
        setLocationNameValue(value);
    }, []);

    const handleSubmit = useCallback(() => {
        // очищаем ошибку хеша
        if (addingScreenErrorMessage) {
            dispatch(setClearScreensError());
        }

        dispatch(
            setAddScreen({
                organizationId,
                locationId,
                verificationHash,
                name: screenName,
            }),
        );
    }, [addingScreenErrorMessage, dispatch, locationId, organizationId, screenName, verificationHash]);

    const closeAddScreenModal = useCallback(() => {
        // очищаем ошибку хеша
        if (addingScreenErrorMessage) {
            dispatch(setClearScreensError());
        }

        dispatch(setCloseAddScreenModal());
    }, [addingScreenErrorMessage, dispatch]);

    const isSubmitButtonDisabled = useMemo(
        () => addingScreen || verificationHash.trim() === '' || screenName.trim() === '' || locationId === '',
        [addingScreen, locationId, screenName, verificationHash],
    );

    // уведомление
    useAddScreenSnackbar();

    return {
        isAddScreenModalOpen,
        addingScreen,
        closeAddScreenModal,
        addingScreenErrorMessage,
        handleSubmit,
        locations,

        verificationHash,
        onVerificationHashChange,
        screenName,
        onScreenNameChange,

        setActiveLocationId,
        onLocationNameSelect,

        locationNameValue,

        isSubmitButtonDisabled,

        t,
    };
};
