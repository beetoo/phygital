import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';

import { setActiveLocationId, setClearScreensSuccess } from '../actions';
import { UpdateLocationSnackbar } from '../components/UpdateLocationSnackbar';
import { selectLocations, selectUpdateLocationSuccess } from '../selectors';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';

export const useUpdateLocationSnackbar = (): void => {
    const dispatch = useDispatch();

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const updateLocationSuccess = useSelector(selectUpdateLocationSuccess);
    const locations = useSelector(selectLocations);

    // после успешного добавления экрана показываем Snackbar с возможностью перехода на экран
    useEffect(() => {
        if (updateLocationSuccess) {
            const location = locations.find(({ id }) => id === updateLocationSuccess.id);

            const closeSnackbarKey = enqueueSnackbar(
                <UpdateLocationSnackbar location={location} onClose={() => closeSnackbar(closeSnackbarKey)} />,
                {
                    key: `UpdateLocationSnackbar:${organizationId}:${updateLocationSuccess.id}`,
                    autoHideDuration: 3000,
                },
            );
            dispatch(setActiveLocationId(updateLocationSuccess.id));
            dispatch(setClearScreensSuccess());
        }
    }, [closeSnackbar, dispatch, enqueueSnackbar, locations, organizationId, updateLocationSuccess]);
};
