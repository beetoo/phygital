import { useMemo } from 'react';
import { useSelector } from 'react-redux';

import { selectActiveLocationId, selectActiveScreenId, selectLocations, selectSelectedScreensIds } from '../selectors';

type ReturnValue = {
    isLocationsExist: boolean;
    isLocationSelected: boolean;
    isScreenSelected: boolean;
    isOneOrMoreScreensSelected: boolean;
};

export const useRightScreensMenu = (): ReturnValue => {
    const activeLocationId = useSelector(selectActiveLocationId);
    const activeScreenId = useSelector(selectActiveScreenId);
    const selectedScreenIds = useSelector(selectSelectedScreensIds);
    const locations = useSelector(selectLocations);

    const selectedScreens = useMemo(
        () =>
            activeLocationId === 'all'
                ? locations.flatMap(({ screens }) => screens).filter(({ id }) => selectedScreenIds.includes(id))
                : (locations
                      .find(({ id }) => id === activeLocationId)
                      ?.screens.filter(({ id }) => selectedScreenIds.includes(id)) ?? []),
        [activeLocationId, locations, selectedScreenIds],
    );

    const isLocationsExist = locations.length > 0;
    const isOneOrMoreScreensSelected = selectedScreens.length > 0;
    const isLocationSelected = activeLocationId !== 'all' && activeScreenId === '' && !isOneOrMoreScreensSelected;
    const isScreenSelected = activeScreenId !== '' && !isOneOrMoreScreensSelected;

    return {
        isLocationsExist,
        isLocationSelected,
        isScreenSelected,
        isOneOrMoreScreensSelected,
    };
};
