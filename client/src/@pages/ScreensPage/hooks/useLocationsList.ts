import { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import {
    selectActiveLocationId,
    selectActiveScreenId,
    selectAddLocationSuccessStatus,
    selectLocations,
    selectSelectedScreensIds,
} from '../selectors';
import { getScreensInfo } from '../helpers';
import {
    setActiveLocationId,
    setActiveScreenId,
    setAddLocation,
    setClearScreensSuccess,
    setGetLocations,
    setSelectedScreenIds,
} from '../actions';
import { LocationType, ScreenType } from '../../../../../common/types';
import { selectAuthUserRole } from '../../../selectors';
import { USER_ROLE } from '../../../constants';

type ReturnValue = {
    userRole: USER_ROLE;
    activeLocationId: string;
    isActiveScreen: boolean;
    locations: LocationType[];
    addLocation: () => void;
    selectLocation: (locationId: string) => void;
    allScreensNumber: number;
    allOnlineScreensNumber: number;
    allOfflineScreensNumber: number;
    allErrorScreensNumber: number;
    t: TFunction;
};

export const useLocationsList = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'screens' });

    const dispatch = useDispatch();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const userRole = useSelector(selectAuthUserRole);
    const activeLocationId = useSelector(selectActiveLocationId);
    const activeScreenId = useSelector(selectActiveScreenId);
    const selectedScreenIds = useSelector(selectSelectedScreensIds);
    const locations = useSelector(selectLocations);
    const addLocationSuccess = useSelector(selectAddLocationSuccessStatus);

    const screens = useMemo(() => locations.flatMap(({ screens }) => screens), [locations]);

    const [allScreens, setAllScreens] = useState<ScreenType[]>([]);

    const {
        screensNumber: allScreensNumber,
        onlineScreensNumber: allOnlineScreensNumber,
        offlineScreensNumber: allOfflineScreensNumber,
        errorScreensNumber: allErrorScreensNumber,
    } = getScreensInfo(allScreens);

    useEffect(() => {
        if (activeLocationId === 'all') {
            setAllScreens(screens);
        }
    }, [activeLocationId, screens]);

    useEffect(() => {
        if (organizationId) {
            dispatch(setGetLocations(organizationId));
        }
    }, [dispatch, organizationId]);

    const selectLocation = useCallback(
        (locationId: string) => {
            if (locationId !== activeLocationId) {
                dispatch(setSelectedScreenIds());
            }
            dispatch(setActiveLocationId(locationId));
            if (activeScreenId) {
                dispatch(setActiveScreenId());
            }
        },
        [activeLocationId, activeScreenId, dispatch],
    );

    const addLocation = useCallback(() => {
        dispatch(setAddLocation({ organizationId }));
    }, [dispatch, organizationId]);

    const isActiveScreen = activeScreenId !== '' || selectedScreenIds.length > 0;

    // очищаем locationSuccess при заходе на страницу экранов
    useEffect(() => {
        if (addLocationSuccess) {
            dispatch(setClearScreensSuccess());
        }
    }, [addLocationSuccess, dispatch]);

    return {
        userRole,
        activeLocationId,
        isActiveScreen,
        locations,
        addLocation,
        selectLocation,
        allScreensNumber,
        allOnlineScreensNumber,
        allOfflineScreensNumber,
        allErrorScreensNumber,
        t,
    };
};
