import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';

import {
    selectScreens,
    selectUpdateScreenIds,
    selectUpdateScreenError,
    selectUpdateScreenSuccessStatus,
} from '../selectors';
import { DEFAULT_CANCEL_TIMEOUT } from '../../../../../common/constants';
import { UpdateScreenSnackbar } from '../components/UpdateScreenSnackbar';
import { UpdateScreenErrorSnackbar } from '../components/UpdateScreenErrorSnackbar';
import { setClearScreensError, setClearScreensSuccess } from '../actions';

export const useUpdateScreenSnackbar = (): void => {
    const dispatch = useDispatch();
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const allScreens = useSelector(selectScreens);
    const updateScreenIds = useSelector(selectUpdateScreenIds);
    const updateScreenSuccess = useSelector(selectUpdateScreenSuccessStatus);
    const updateScreenError = useSelector(selectUpdateScreenError);

    useEffect(() => {
        if (updateScreenSuccess) {
            const close = () => {
                dispatch(setClearScreensSuccess());
                closeSnackbar(key);
            };

            const screenIds = updateScreenSuccess.id;

            const screens = allScreens.filter(({ id }) =>
                Array.isArray(screenIds) ? screenIds.includes(id) : screenIds === id,
            );

            const key = enqueueSnackbar(<UpdateScreenSnackbar screens={screens} onClose={close} />, {
                key: `updateScreenSnackbar:${screenIds}`,
            });
            const autoHide = setTimeout(close, DEFAULT_CANCEL_TIMEOUT);

            return () => {
                clearTimeout(autoHide);
            };
        }
    }, [allScreens, closeSnackbar, dispatch, enqueueSnackbar, updateScreenSuccess]);

    useEffect(() => {
        if (updateScreenError) {
            const close = () => {
                dispatch(setClearScreensError());
                closeSnackbar(key);
            };

            const screens = allScreens.filter(({ id }) =>
                Array.isArray(updateScreenIds) ? updateScreenIds.includes(id) : updateScreenIds === id,
            );

            const key = enqueueSnackbar(
                <UpdateScreenErrorSnackbar screens={screens} error={updateScreenError} onClose={close} />,
                {
                    key: `updateScreenSnackbar:${updateScreenIds}`,
                },
            );
            const autoHide = setTimeout(close, 50000);

            return () => {
                clearTimeout(autoHide);
            };
        }
    }, [allScreens, closeSnackbar, dispatch, enqueueSnackbar, updateScreenError, updateScreenIds]);
};
