import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setHideModal } from '../../../components/ModalsContainer/actions';
import { selectCurrentModal } from '../../../components/ModalsContainer/selectors';
import { MODALS } from '../../../components/ModalsContainer/types';
import { setActiveLocationId, setActiveScreenId, setDeleteLocation } from '../actions';

import {
    selectActiveLocation,
    selectDeleteLocationErrorMessage,
    selectDeleteLocationStatus,
    selectDeleteLocationSuccessStatus,
} from '../selectors';

type ReturnValue = {
    isDeleteLocationModalOpen: boolean;
    isDeletingLocation: boolean;
    onDeleteLocation: () => void;
    closeModal: () => void;
    t: TFunction;
};

export const useDeleteLocationModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'screensModal' });

    const dispatch = useDispatch();

    const activeLocation = useSelector(selectActiveLocation);
    const currentModal = useSelector(selectCurrentModal);
    const isDeleteLocationSuccessStatus = useSelector(selectDeleteLocationSuccessStatus);
    const deleteLocationError = useSelector(selectDeleteLocationErrorMessage);
    const isDeletingLocation = useSelector(selectDeleteLocationStatus);

    const isDeleteLocationModalOpen = currentModal === MODALS.DELETE_LOCATION;

    useEffect(() => {
        if (isDeleteLocationSuccessStatus || deleteLocationError) {
            dispatch(setHideModal());
            dispatch(setActiveLocationId('all'));
            dispatch(setActiveScreenId());
        }
    }, [dispatch, isDeleteLocationSuccessStatus, deleteLocationError]);

    const closeModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    const onDeleteLocation = useCallback(() => {
        if (activeLocation) {
            dispatch(setDeleteLocation(activeLocation));
        }
    }, [activeLocation, dispatch]);

    return {
        isDeleteLocationModalOpen,
        isDeletingLocation,
        onDeleteLocation,
        closeModal,
        t,
    };
};
