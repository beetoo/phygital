import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';

import { AddScreenSnackbar } from '../components/AddScreenSnackbar';
import { setActiveScreenId } from '../actions';
import { AddScreenGoToSnackbar } from '../components/AddScreenGoToSnackbar';
import { useCustomRouter } from '../../../hooks/useCustomRouter';
import { selectAddScreenSuccessStatus } from '../selectors';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';

export const useAddScreenSnackbar = (): void => {
    const dispatch = useDispatch();

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const { currentPage } = useCustomRouter();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const addScreenSuccess = useSelector(selectAddScreenSuccessStatus);

    // после успешного добавления экрана показываем Snackbar с возможностью перехода на экран
    useEffect(() => {
        if (addScreenSuccess) {
            // on main page
            if (currentPage === 'screens') {
                const closeSnackbarKey = enqueueSnackbar(
                    <AddScreenSnackbar screen={addScreenSuccess} onClose={() => closeSnackbar(closeSnackbarKey)} />,
                    {
                        key: `ScreenCreatedSnackbar:${organizationId}:${addScreenSuccess.id}`,
                        autoHideDuration: 3000,
                    },
                );

                dispatch(setActiveScreenId(addScreenSuccess.id));
            } else {
                const closeSnackbarKey = enqueueSnackbar(
                    <AddScreenGoToSnackbar screen={addScreenSuccess} onClose={() => closeSnackbar(closeSnackbarKey)} />,
                    {
                        key: `ScreenCreatedSnackbar:${organizationId}:${addScreenSuccess.id}`,
                        autoHideDuration: 3000,
                    },
                );
            }
        }
    }, [addScreenSuccess, closeSnackbar, currentPage, dispatch, enqueueSnackbar, organizationId]);
};
