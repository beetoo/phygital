import { ChangeEvent, KeyboardEvent, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { orderBy, union, isEqual } from 'lodash';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';

import {
    selectAddingScreenTagStatus,
    selectDeleteScreenTagSuccessStatus,
    selectLocations,
    selectSelectedScreensIds,
    selectUpdateScreenSuccessStatus,
} from '../selectors';

import { setClearScreensSuccess, setDeleteScreenTags, setSelectedScreenIds, setUpdateScreens } from '../actions';
import { setShowModal } from '../../../components/ModalsContainer/actions';
import { MODALS } from '../../../components/ModalsContainer/types';
import { DeleteScreenTagDto, LocationType } from '../../../../../common/types';

type ReturnValue = {
    selectedScreensNumber: number;
    savedTags: string[];
    notSavedTags: string[];
    isSubmitButtonDisabled: boolean;
    tag: string;
    onTagNameChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onTagEnter: (e: KeyboardEvent<HTMLInputElement>) => void;
    onDeleteSavedTag: (tagName: string) => () => void;
    onDeleteNotSavedTag: (tagName: string) => () => void;
    onHandleSubmit: () => void;
    unselectAllScreens: () => void;
    selectableLocations: LocationType[];

    locationSelectValue: '' | string;
    setActiveLocationId: (locationId: string) => () => void;
    onLocationSelect: (e: SelectChangeEvent) => void;

    openDeleteScreensModal: () => void;
    tScreensModal: TFunction;
    tScreens: TFunction;
};

export const useMultiplyScreensMenu = (): ReturnValue => {
    const { t: tScreensModal } = useTranslation('translation', { keyPrefix: 'screensModal' });
    const { t: tScreens } = useTranslation('translation', { keyPrefix: 'screens' });

    const dispatch = useDispatch();

    const selectedScreenIds = useSelector(selectSelectedScreensIds);
    const addingScreenTag = useSelector(selectAddingScreenTagStatus);
    const locations = useSelector(selectLocations);
    const isDeleteScreenTagSuccess = useSelector(selectDeleteScreenTagSuccessStatus);
    const isUpdateScreenSuccess = useSelector(selectUpdateScreenSuccessStatus);

    const [tag, setTag] = useState('');
    const [notSavedTags, setNotSavedTags] = useState<string[]>([]);
    const [savedTags, setSavedTags] = useState<string[]>([]);
    const [deletableTags, setDeletableTags] = useState<{ screenId: string; tagId: string }[]>([]);

    const [locationId, setLocationId] = useState('');
    const [locationSelectValue, setLocationSelectValue] = useState<'' | string>('');

    const screens = useMemo(() => locations.flatMap(({ screens }) => screens), [locations]);

    const selectedScreens = useMemo(
        () => screens.filter(({ id }) => selectedScreenIds.some((screenId) => screenId === id)),
        [screens, selectedScreenIds],
    );

    const screensTags = useMemo(
        () => selectedScreens.flatMap(({ tags }) => tags.map(({ name }) => name)),
        [selectedScreens],
    );

    const selectableLocations = useMemo(() => {
        const isScreensHaveSingleLocation = selectedScreens.every(
            (screen, _, array) => screen.location.id === array[0].location.id,
        );
        return isScreensHaveSingleLocation
            ? locations.filter((location) => location.id !== selectedScreens[0].location.id)
            : locations;
    }, [locations, selectedScreens]);

    const commonTags = orderBy(union(screensTags));

    const numberOfSelectedScreensRef = useRef(0);
    const numberOfSelectedScreens = numberOfSelectedScreensRef.current;

    const selectedScreensNumber = selectedScreenIds.length;

    useEffect(() => {
        if (numberOfSelectedScreens !== selectedScreensNumber) {
            if (!isEqual(savedTags, commonTags)) {
                setSavedTags(commonTags);
            }

            if (deletableTags.length > 0) {
                setDeletableTags([]);
            }

            numberOfSelectedScreensRef.current = selectedScreensNumber;
        }
    }, [commonTags, deletableTags.length, numberOfSelectedScreens, savedTags, selectedScreensNumber]);

    useEffect(() => {
        if (isUpdateScreenSuccess || isDeleteScreenTagSuccess) {
            setNotSavedTags([]);
            setDeletableTags([]);
            setLocationId('');
            setLocationSelectValue('');
            if (isUpdateScreenSuccess) {
                const tagsAfterSave = orderBy(union([...savedTags, ...notSavedTags]));
                setSavedTags(tagsAfterSave);
            }
            dispatch(setClearScreensSuccess());
        }
    }, [dispatch, isDeleteScreenTagSuccess, isUpdateScreenSuccess, notSavedTags, savedTags]);

    const onTagNameChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setTag(value);
    }, []);

    const onTagEnter = useCallback(
        ({ key }: KeyboardEvent<HTMLInputElement>) => {
            const isAllScreensTagExist = selectedScreens.every(({ tags }) =>
                tags.some(({ name }) => name.toLowerCase() === tag.trim().toLowerCase()),
            );

            const isNotInSavedTags = !notSavedTags
                .map((tagName) => tagName.toLowerCase())
                .includes(tag.trim().toLowerCase());

            if (key === 'Enter' && tag.trim() !== '' && !isAllScreensTagExist && isNotInSavedTags) {
                setNotSavedTags([...notSavedTags, tag]);
                setTag('');
            }
        },
        [selectedScreens, tag, notSavedTags],
    );

    const onDeleteNotSavedTag = useCallback(
        (tagName: string) => () => {
            setNotSavedTags(notSavedTags.filter((tag) => tag !== tagName));
        },
        [notSavedTags],
    );

    const openDeleteScreensModal = useCallback(() => {
        dispatch(setShowModal(MODALS.DELETE_SCREENS));
    }, [dispatch]);

    const onDeleteSavedTag = useCallback(
        (tagName: string) => () => {
            const deletedTags: DeleteScreenTagDto = selectedScreens
                .filter(({ tags }) => tags.find(({ name }) => name.toLowerCase() === tagName.toLowerCase()))
                .map(({ id, tags }) => ({
                    screenId: id,
                    tagId: tags.find(({ name }) => name.toLowerCase() === tagName.toLowerCase())?.id ?? '',
                }));
            setSavedTags((prev) => prev.filter((name) => name.toLowerCase() !== tagName.toLowerCase()));
            setDeletableTags((prev) => [...prev, ...deletedTags]);
        },
        [selectedScreens],
    );

    const setActiveLocationId = useCallback(
        (locationId: string) => () => {
            setLocationId(locationId);
        },
        [],
    );

    const onLocationSelect = useCallback(({ target: { value } }: SelectChangeEvent) => {
        setLocationSelectValue(value);
    }, []);

    const filteredSelectedScreenId = useMemo(
        () =>
            screens
                .filter(({ id, location }) => selectedScreenIds.includes(id) && location.id !== locationId)
                .map(({ id }) => id),
        [locationId, screens, selectedScreenIds],
    );

    const onHandleSubmit = useCallback(() => {
        if (notSavedTags.length > 0) {
            dispatch(setUpdateScreens({ screenIds: selectedScreenIds, tags: notSavedTags }));
        }
        if (deletableTags.length > 0) {
            dispatch(setDeleteScreenTags(deletableTags));
        }
        if (locationId && filteredSelectedScreenId.length > 0) {
            dispatch(setUpdateScreens({ screenIds: filteredSelectedScreenId, locationId }));
        }
        // else if (locationId && filteredSelectedScreenId.length === 0) {
        //     dispatch(setUpdateScreenSuccess());
        // }
    }, [deletableTags, dispatch, filteredSelectedScreenId, locationId, notSavedTags, selectedScreenIds]);

    const unselectAllScreens = useCallback(() => {
        dispatch(setSelectedScreenIds());
    }, [dispatch]);

    const isSubmitButtonDisabled =
        addingScreenTag || (notSavedTags.length === 0 && deletableTags.length === 0 && locationId === '');

    return {
        selectedScreensNumber,
        savedTags,
        notSavedTags,
        isSubmitButtonDisabled,
        openDeleteScreensModal,
        tag,
        onTagNameChange,
        onTagEnter,
        onDeleteSavedTag,
        onDeleteNotSavedTag,
        onHandleSubmit,
        unselectAllScreens,

        selectableLocations,
        locationSelectValue,
        setActiveLocationId,
        onLocationSelect,
        tScreensModal,
        tScreens,
    };
};
