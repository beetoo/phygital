import { ChangeEvent, useCallback, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setHideModal } from '../../../components/ModalsContainer/actions';
import { httpService } from '../../../helpers/httpService';
import { selectActiveScreenId, selectScreens } from '../selectors';
import { setGetScreensSuccess } from '../actions';

type ReturnValue = {
    verificationHash: string;
    onVerificationHashChange: (e: ChangeEvent<HTMLInputElement>) => void;
    verificationHashError: string;
    isHashChanging: boolean;
    changePlayer: () => void;
    closeChangeScreenModal: () => void;
    isSubmitButtonDisabled: boolean;

    t: TFunction;
};

export const useChangePlayerModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'screensModal' });

    const dispatch = useDispatch();

    const activeScreenId = useSelector(selectActiveScreenId);
    const screens = useSelector(selectScreens);
    const [isHashChanging, setIsHashChanging] = useState(false);
    const [verificationHash, setVerificationHash] = useState('');
    const [verificationHashError, setVerificationHashError] = useState('');

    const onVerificationHashChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        // очищаем ошибку хеша
        setVerificationHashError('');
        setVerificationHash(value);
    }, []);

    const changePlayer = useCallback(async () => {
        if (!activeScreenId) {
            return;
        }

        setVerificationHashError('');

        try {
            setIsHashChanging(true);
            await httpService.patch(`screen/${activeScreenId}/hash`, { verificationHash });
            dispatch(
                setGetScreensSuccess(
                    screens.map((screen) => (screen.id === activeScreenId ? { ...screen, verificationHash } : screen)),
                ),
            );

            dispatch(setHideModal());
        } catch {
            setIsHashChanging(false);
            setVerificationHashError(t('hashVerificationError'));
        }
    }, [activeScreenId, dispatch, screens, t, verificationHash]);

    const closeChangeScreenModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    const isSubmitButtonDisabled = useMemo(() => verificationHash.trim() === '', [verificationHash]);

    return {
        verificationHash,
        onVerificationHashChange,
        verificationHashError,
        isHashChanging,
        changePlayer,
        closeChangeScreenModal,
        isSubmitButtonDisabled,

        t,
    };
};
