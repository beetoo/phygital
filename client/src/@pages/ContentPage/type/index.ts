import {
    setActiveContentItemId,
    setActiveFolderId,
    setAddFolder,
    setAddFolderFail,
    setAddFolderSuccess,
    setClearContentPageSuccess,
    setDeleteFolder,
    setDeleteFolderFail,
    setDeleteFolderSuccess,
    setGetFolders,
    setGetFoldersFail,
    setGetFoldersSuccess,
    setSelectedContentItemIds,
    setUpdateContentItem,
    setUpdateContentItemFail,
    setUpdateContentItemSuccess,
    setUpdateFolder,
    setUpdateFolderFail,
    setUpdateFolderSuccess,
    setDeleteContentItem,
    setDeleteContentItemFail,
    setDeleteContentItemSuccess,
    setGetContentItems,
    setGetContentItemsSuccess,
    setGetContentItemsFail,
    setUploadContentItem,
    setUploadContentItemSuccess,
    setUploadContentItemFail,
    setStartUploadContentItems,
    setAddContentToPlaylists,
    setAddContentToPlaylistsSuccess,
    setAddContentToPlaylistsFail,
    setUpdatedPlaylistsWithContent,
    setUpdateContentItems,
    setUpdateContentItemsSuccess,
    setUpdateContentItemsFail,
} from '../actions';
import { ContentType, FolderType } from '../../../../../common/types';
import { ErrorResponse } from '../../../types';
import { SelectTagType } from '../../../ui-kit';

export type AddContentToPlaylistDto = {
    playlistIds: string[];
    contentIds: string[];
};

export type AddFolderSuccessType = { id: string } | false;

export type FoldersPageState = {
    activeFolderId: string;
    activeContentItemId: string;
    selectedContentItemIds: string[];

    gettingFolders: boolean;
    isGetFoldersSuccess: boolean;
    folders: FolderType[];
    gettingFoldersError: ErrorResponse | null;

    addingFolder: boolean;
    addFolderSuccess: AddFolderSuccessType;
    addingFolderError: ErrorResponse | null;

    updatingFolder: boolean;
    isUpdateFolderSuccess: boolean;
    updatingFolderError: ErrorResponse | null;

    deleteFolder: boolean;
    isDeleteFolderSuccess: boolean;
    deleteFolderError: ErrorResponse | null;

    gettingContentItems: boolean;
    contentItems: ContentType[];
    gettingContentItemsError: ErrorResponse | null;

    uploadingContentItems: boolean;
    uploadingContentItemsError: ErrorResponse | null;

    updatingContentItem: boolean;
    isUpdateContentItemSuccess: boolean;
    updatingContentItemError: ErrorResponse | null;

    deleteContentItem: boolean;
    isDeleteContentItemSuccess: boolean;
    deleteContentItemError: ErrorResponse | null;

    updatedPlaylistsWithContent: SelectTagType[];
    addingContentToPlaylists: boolean;
    isAddContentToPlaylistsSuccess: boolean;
    addContentToPlaylistsError: ErrorResponse | null;
};

export type SetActiveFolderIdAction = ReturnType<typeof setActiveFolderId>;
export type SetActiveContentItemIdAction = ReturnType<typeof setActiveContentItemId>;
export type SetSelectedContentItemIdsAction = ReturnType<typeof setSelectedContentItemIds>;

export type GetFoldersAction = ReturnType<typeof setGetFolders>;
export type GetFoldersSuccessAction = ReturnType<typeof setGetFoldersSuccess>;
export type GetFoldersFailAction = ReturnType<typeof setGetFoldersFail>;

export type AddFolderAction = ReturnType<typeof setAddFolder>;
export type AddFolderSuccessAction = ReturnType<typeof setAddFolderSuccess>;
export type AddFolderFailAction = ReturnType<typeof setAddFolderFail>;

export type UpdateFolderAction = ReturnType<typeof setUpdateFolder>;
export type UpdateFolderSuccessAction = ReturnType<typeof setUpdateFolderSuccess>;
export type UpdateFolderFailAction = ReturnType<typeof setUpdateFolderFail>;

export type DeleteFolderAction = ReturnType<typeof setDeleteFolder>;
export type DeleteFolderSuccessAction = ReturnType<typeof setDeleteFolderSuccess>;
export type DeleteFolderFailAction = ReturnType<typeof setDeleteFolderFail>;

export type GetContentItemsAction = ReturnType<typeof setGetContentItems>;
export type GetContentItemsSuccessAction = ReturnType<typeof setGetContentItemsSuccess>;
export type GetContentItemsFailAction = ReturnType<typeof setGetContentItemsFail>;

export type SetStartUploadContentItemsAction = ReturnType<typeof setStartUploadContentItems>;

export type UploadContentItemAction = ReturnType<typeof setUploadContentItem>;
export type UploadContentItemSuccessAction = ReturnType<typeof setUploadContentItemSuccess>;
export type UploadContentItemFailAction = ReturnType<typeof setUploadContentItemFail>;

export type UpdateContentItemAction = ReturnType<typeof setUpdateContentItem>;
export type UpdateContentItemSuccessAction = ReturnType<typeof setUpdateContentItemSuccess>;
export type UpdateContentItemFailAction = ReturnType<typeof setUpdateContentItemFail>;

export type UpdateContentItemsAction = ReturnType<typeof setUpdateContentItems>;
export type UpdateContentItemsSuccessAction = ReturnType<typeof setUpdateContentItemsSuccess>;
export type UpdateContentItemsFailAction = ReturnType<typeof setUpdateContentItemsFail>;

export type DeleteContentItemAction = ReturnType<typeof setDeleteContentItem>;
export type DeleteContentItemSuccessAction = ReturnType<typeof setDeleteContentItemSuccess>;
export type DeleteContentItemFailAction = ReturnType<typeof setDeleteContentItemFail>;

export type SetUpdatedPlaylistsWithContentAction = ReturnType<typeof setUpdatedPlaylistsWithContent>;
export type AddContentToPlaylistsAction = ReturnType<typeof setAddContentToPlaylists>;
export type AddContentToPlaylistsSuccessAction = ReturnType<typeof setAddContentToPlaylistsSuccess>;
export type AddContentToPlaylistsFailAction = ReturnType<typeof setAddContentToPlaylistsFail>;

export type ClearContentPageSuccessAction = ReturnType<typeof setClearContentPageSuccess>;

export type ContentsAction =
    | SetActiveFolderIdAction
    | SetActiveContentItemIdAction
    | SetSelectedContentItemIdsAction
    | GetFoldersAction
    | GetFoldersSuccessAction
    | GetFoldersFailAction
    | AddFolderAction
    | AddFolderSuccessAction
    | AddFolderFailAction
    | UpdateFolderAction
    | UpdateFolderSuccessAction
    | UpdateFolderFailAction
    | DeleteFolderAction
    | DeleteFolderSuccessAction
    | DeleteFolderFailAction
    | GetContentItemsAction
    | GetContentItemsSuccessAction
    | GetContentItemsFailAction
    | SetStartUploadContentItemsAction
    | UploadContentItemAction
    | UploadContentItemSuccessAction
    | UploadContentItemFailAction
    | UpdateContentItemAction
    | UpdateContentItemSuccessAction
    | UpdateContentItemFailAction
    | UpdateContentItemsAction
    | UpdateContentItemsSuccessAction
    | UpdateContentItemsFailAction
    | DeleteContentItemAction
    | DeleteContentItemSuccessAction
    | DeleteContentItemFailAction
    | SetUpdatedPlaylistsWithContentAction
    | AddContentToPlaylistsAction
    | AddContentToPlaylistsSuccessAction
    | AddContentToPlaylistsFailAction
    | ClearContentPageSuccessAction;
