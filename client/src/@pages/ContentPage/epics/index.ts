import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, withLatestFrom } from 'rxjs/operators';
import { combineEpics, ofType, StateObservable } from 'redux-observable';

import {
    AddFolderAction,
    AddFolderFailAction,
    AddFolderSuccessAction,
    DeleteContentItemAction,
    DeleteContentItemFailAction,
    DeleteContentItemSuccessAction,
    DeleteFolderAction,
    DeleteFolderFailAction,
    DeleteFolderSuccessAction,
    GetFoldersAction,
    GetFoldersFailAction,
    GetFoldersSuccessAction,
    UpdateContentItemAction,
    UpdateContentItemFailAction,
    UpdateContentItemSuccessAction,
    UpdateFolderAction,
    UpdateFolderFailAction,
    UpdateFolderSuccessAction,
    GetContentItemsAction,
    UploadContentItemSuccessAction,
    GetContentItemsSuccessAction,
    GetContentItemsFailAction,
    UploadContentItemAction,
    UploadContentItemFailAction,
    AddContentToPlaylistsAction,
    AddContentToPlaylistsSuccessAction,
    AddContentToPlaylistsFailAction,
    UpdateContentItemsAction,
    UpdateContentItemsSuccessAction,
    UpdateContentItemsFailAction,
} from '../type';
import {
    ADD_FOLDER,
    ADD_FOLDER_SUCCESS,
    DELETE_CONTENT_ITEM,
    DELETE_CONTENT_ITEM_SUCCESS,
    DELETE_FOLDER,
    DELETE_FOLDER_SUCCESS,
    GET_FOLDERS,
    setAddFolderFail,
    setAddFolderSuccess,
    setDeleteContentItemFail,
    setDeleteContentItemSuccess,
    setDeleteFolderFail,
    setDeleteFolderSuccess,
    setGetFolders,
    setGetFoldersFail,
    setGetFoldersSuccess,
    setUpdateContentItemFail,
    setUpdateContentItemSuccess,
    setUpdateFolderFail,
    setUpdateFolderSuccess,
    UPDATE_CONTENT_ITEM,
    UPDATE_CONTENT_ITEM_SUCCESS,
    UPDATE_FOLDER,
    UPDATE_FOLDER_SUCCESS,
    GET_CONTENT_ITEMS,
    setGetContentItems,
    setGetContentItemsFail,
    setGetContentItemsSuccess,
    setUploadContentItemFail,
    setUploadContentItemSuccess,
    UPLOAD_CONTENT_ITEM,
    UPLOAD_CONTENT_ITEM_SUCCESS,
    ADD_CONTENT_TO_PLAYLISTS,
    setAddContentToPlaylistsSuccess,
    setAddContentToPlaylistsFail,
    UPDATE_CONTENT_ITEMS,
    setUpdateContentItemsSuccess,
    setUpdateContentItemsFail,
    UPDATE_CONTENT_ITEMS_SUCCESS,
} from '../actions';
import {
    fromAddFolder,
    fromDeleteContentItem,
    fromDeleteFolder,
    fromGetFolders,
    fromUpdateContentItem,
    fromUpdateFolder,
    fromGetContentItems,
    fromUploadContentItem,
    fromUpdateContentItems,
} from '../../../resolvers/contentResolvers';
import { StoreType } from '../../../types';

import { HideModalAction } from '../../../components/ModalsContainer/types';
import { setHideModal } from '../../../components/ModalsContainer/actions';
import { fromAddContentToPlaylists } from '../../../resolvers/playlistsResolvers';

const getFoldersEpic = (
    action$: Observable<GetFoldersAction>,
): Observable<GetFoldersSuccessAction | GetFoldersFailAction> =>
    action$.pipe(
        ofType(GET_FOLDERS),
        mergeMap(({ payload: { organizationId } }) =>
            fromGetFolders(organizationId).pipe(
                map(setGetFoldersSuccess),
                catchError((error) => of(setGetFoldersFail(error))),
            ),
        ),
    );

const addFolderEpic = (
    action$: Observable<AddFolderAction>,
): Observable<AddFolderSuccessAction | AddFolderFailAction> =>
    action$.pipe(
        ofType(ADD_FOLDER),
        mergeMap(({ payload: createFolderDto }) =>
            fromAddFolder(createFolderDto).pipe(
                map(({ id }) => setAddFolderSuccess(id)),
                catchError((error) => of(setAddFolderFail(error))),
            ),
        ),
    );

const updateFolderEpic = (
    action$: Observable<UpdateFolderAction>,
): Observable<UpdateFolderSuccessAction | UpdateFolderFailAction> =>
    action$.pipe(
        ofType(UPDATE_FOLDER),
        mergeMap(({ payload: updateFolderDto }) =>
            fromUpdateFolder(updateFolderDto).pipe(
                map(setUpdateFolderSuccess),
                catchError((error) => of(setUpdateFolderFail(error))),
            ),
        ),
    );

const deleteFolderEpic = (
    action$: Observable<DeleteFolderAction>,
): Observable<DeleteFolderSuccessAction | DeleteFolderFailAction> =>
    action$.pipe(
        ofType(DELETE_FOLDER),
        mergeMap(({ payload: { folderId } }) =>
            fromDeleteFolder(folderId).pipe(
                map(setDeleteFolderSuccess),
                catchError((error) => of(setDeleteFolderFail(error))),
            ),
        ),
    );

const getContentItemsEpic = (
    action$: Observable<GetContentItemsAction>,
): Observable<GetContentItemsSuccessAction | GetContentItemsFailAction> =>
    action$.pipe(
        ofType(GET_CONTENT_ITEMS),
        mergeMap(({ payload: { organizationId } }) =>
            fromGetContentItems(organizationId).pipe(
                map(setGetContentItemsSuccess),
                catchError((error) => of(setGetContentItemsFail(error))),
            ),
        ),
    );

const uploadContentItemEpic = (
    action$: Observable<UploadContentItemAction>,
): Observable<UploadContentItemSuccessAction | UploadContentItemFailAction> =>
    action$.pipe(
        ofType(UPLOAD_CONTENT_ITEM),
        mergeMap(({ payload: { organizationId, folderId, files, uploadedItemsInfo } }) =>
            fromUploadContentItem(organizationId, folderId, files, uploadedItemsInfo).pipe(
                map(setUploadContentItemSuccess),
                catchError((error) => of(setUploadContentItemFail(error))),
            ),
        ),
    );

const updateContentItemEpic = (
    action$: Observable<UpdateContentItemAction>,
): Observable<UpdateContentItemSuccessAction | UpdateContentItemFailAction> =>
    action$.pipe(
        ofType(UPDATE_CONTENT_ITEM),
        mergeMap(({ payload: { contentId, updateContentDto } }) =>
            fromUpdateContentItem(contentId, updateContentDto).pipe(
                map(setUpdateContentItemSuccess),
                catchError((error) => of(setUpdateContentItemFail(error))),
            ),
        ),
    );

const updateContentItemsEpic = (
    action$: Observable<UpdateContentItemsAction>,
): Observable<UpdateContentItemsSuccessAction | UpdateContentItemsFailAction> =>
    action$.pipe(
        ofType(UPDATE_CONTENT_ITEMS),
        mergeMap(({ payload: { contentIds, updateContentDto } }) =>
            fromUpdateContentItems(contentIds, updateContentDto).pipe(
                map(setUpdateContentItemsSuccess),
                catchError((error) => of(setUpdateContentItemsFail(error))),
            ),
        ),
    );

const addContentToPlaylistsEpic = (
    action$: Observable<AddContentToPlaylistsAction>,
): Observable<AddContentToPlaylistsSuccessAction | AddContentToPlaylistsFailAction> =>
    action$.pipe(
        ofType(ADD_CONTENT_TO_PLAYLISTS),
        mergeMap(({ payload: { contentIds, playlistIds } }) =>
            fromAddContentToPlaylists({ contentIds, playlistIds }).pipe(
                map(setAddContentToPlaylistsSuccess),
                catchError((error) => of(setAddContentToPlaylistsFail(error))),
            ),
        ),
    );

const deleteContentItemEpic = (
    action$: Observable<DeleteContentItemAction>,
): Observable<DeleteContentItemSuccessAction | DeleteContentItemFailAction> =>
    action$.pipe(
        ofType(DELETE_CONTENT_ITEM),
        mergeMap(({ payload: { contentIds } }) =>
            fromDeleteContentItem(contentIds).pipe(
                map(setDeleteContentItemSuccess),
                catchError((error) => of(setDeleteContentItemFail(error))),
            ),
        ),
    );

const contentSuccessEpicOne = (
    action$: Observable<
        AddFolderSuccessAction | UpdateFolderSuccessAction | DeleteFolderSuccessAction | UpdateContentItemSuccessAction
    >,
    state$: StateObservable<StoreType>,
): Observable<GetFoldersAction> =>
    action$.pipe(
        ofType(
            ADD_FOLDER_SUCCESS,
            UPDATE_FOLDER_SUCCESS,
            DELETE_FOLDER_SUCCESS,
            UPDATE_CONTENT_ITEM_SUCCESS,
            UPDATE_CONTENT_ITEMS_SUCCESS,
        ),
        withLatestFrom(state$),
        map(
            ([
                ,
                {
                    pages: {
                        organization: {
                            profile: {
                                organization: { id },
                            },
                        },
                    },
                },
            ]) => setGetFolders(id),
        ),
    );

const contentSuccessEpicTwo = (
    action$: Observable<UpdateContentItemSuccessAction | DeleteContentItemSuccessAction>,
    state$: StateObservable<StoreType>,
): Observable<GetContentItemsAction> =>
    action$.pipe(
        ofType(UPDATE_CONTENT_ITEM_SUCCESS, DELETE_CONTENT_ITEM_SUCCESS, UPDATE_CONTENT_ITEMS_SUCCESS),
        withLatestFrom(state$),
        map(
            ([
                ,
                {
                    pages: {
                        organization: {
                            profile: {
                                organization: { id },
                            },
                        },
                    },
                },
            ]) => setGetContentItems(id),
        ),
    );

const closeModalEpic = (
    action$: Observable<
        | AddFolderSuccessAction
        | DeleteFolderSuccessAction
        | UploadContentItemSuccessAction
        | DeleteContentItemSuccessAction
    >,
): Observable<HideModalAction> =>
    action$.pipe(
        ofType(ADD_FOLDER_SUCCESS, DELETE_FOLDER_SUCCESS, UPLOAD_CONTENT_ITEM_SUCCESS, DELETE_CONTENT_ITEM_SUCCESS),
        map(setHideModal),
    );

type EpicsActions =
    | GetFoldersAction
    | GetFoldersSuccessAction
    | GetFoldersFailAction
    | AddFolderAction
    | AddFolderSuccessAction
    | AddFolderFailAction
    | UpdateFolderAction
    | UpdateFolderSuccessAction
    | UpdateFolderFailAction
    | DeleteFolderAction
    | DeleteFolderSuccessAction
    | DeleteFolderFailAction
    | GetContentItemsAction
    | GetContentItemsSuccessAction
    | GetContentItemsFailAction
    | UploadContentItemAction
    | UploadContentItemSuccessAction
    | UploadContentItemFailAction
    | UpdateContentItemAction
    | UpdateContentItemSuccessAction
    | UpdateContentItemFailAction
    | UpdateContentItemsAction
    | UpdateContentItemsSuccessAction
    | UpdateContentItemsFailAction
    | AddContentToPlaylistsAction
    | AddContentToPlaylistsSuccessAction
    | AddContentToPlaylistsFailAction
    | DeleteContentItemAction
    | DeleteContentItemSuccessAction
    | DeleteContentItemFailAction
    | HideModalAction;

export const foldersEpics = combineEpics<EpicsActions, EpicsActions, StoreType>(
    getFoldersEpic,
    addFolderEpic,
    updateFolderEpic,
    deleteFolderEpic,
    getContentItemsEpic,
    uploadContentItemEpic,
    updateContentItemEpic,
    updateContentItemsEpic,
    addContentToPlaylistsEpic,
    deleteContentItemEpic,
    contentSuccessEpicOne,
    contentSuccessEpicTwo,
    closeModalEpic,
);
