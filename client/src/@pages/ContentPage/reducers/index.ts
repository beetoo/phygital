import {
    ADD_CONTENT_TO_PLAYLISTS,
    ADD_CONTENT_TO_PLAYLISTS_FAIL,
    ADD_CONTENT_TO_PLAYLISTS_SUCCESS,
    ADD_FOLDER,
    ADD_FOLDER_FAIL,
    ADD_FOLDER_SUCCESS,
    CLEAR_CONTENT_PAGE_SUCCESS,
    DELETE_CONTENT_ITEM,
    DELETE_CONTENT_ITEM_FAIL,
    DELETE_CONTENT_ITEM_SUCCESS,
    DELETE_FOLDER,
    DELETE_FOLDER_FAIL,
    DELETE_FOLDER_SUCCESS,
    GET_CONTENT_ITEMS,
    GET_CONTENT_ITEMS_FAIL,
    GET_CONTENT_ITEMS_SUCCESS,
    GET_FOLDERS,
    GET_FOLDERS_FAIL,
    GET_FOLDERS_SUCCESS,
    SET_ACTIVE_CONTENT_ITEM_ID,
    SET_ACTIVE_FOLDER_ID,
    SET_SELECTED_CONTENT_ITEM_IDS,
    SET_START_UPLOAD_CONTENT_ITEMS,
    SET_UPDATED_PLAYLISTS_WITH_CONTENT,
    UPDATE_CONTENT_ITEM,
    UPDATE_CONTENT_ITEM_FAIL,
    UPDATE_CONTENT_ITEM_SUCCESS,
    UPDATE_CONTENT_ITEMS,
    UPDATE_CONTENT_ITEMS_FAIL,
    UPDATE_CONTENT_ITEMS_SUCCESS,
    UPDATE_FOLDER,
    UPDATE_FOLDER_FAIL,
    UPDATE_FOLDER_SUCCESS,
    UPLOAD_CONTENT_ITEM,
    UPLOAD_CONTENT_ITEM_FAIL,
    UPLOAD_CONTENT_ITEM_SUCCESS,
} from '../actions';

import { ContentsAction, FoldersPageState } from '../type';

const foldersPageInitialState: FoldersPageState = {
    activeFolderId: '',
    activeContentItemId: '',
    selectedContentItemIds: [],

    gettingFolders: false,
    isGetFoldersSuccess: false,
    folders: [],
    gettingFoldersError: null,

    addingFolder: false,
    addFolderSuccess: false,
    addingFolderError: null,

    updatingFolder: false,
    isUpdateFolderSuccess: false,
    updatingFolderError: null,

    deleteFolder: false,
    isDeleteFolderSuccess: false,
    deleteFolderError: null,

    gettingContentItems: false,
    contentItems: [],
    gettingContentItemsError: null,

    uploadingContentItems: false,
    uploadingContentItemsError: null,

    updatingContentItem: false,
    isUpdateContentItemSuccess: false,
    updatingContentItemError: null,

    deleteContentItem: false,
    isDeleteContentItemSuccess: false,
    deleteContentItemError: null,

    updatedPlaylistsWithContent: [],
    addingContentToPlaylists: false,
    isAddContentToPlaylistsSuccess: false,
    addContentToPlaylistsError: null,
};

export default function foldersPageReducer(state = foldersPageInitialState, action: ContentsAction): FoldersPageState {
    switch (action.type) {
        case SET_ACTIVE_FOLDER_ID:
            return {
                ...state,
                activeFolderId: action.payload.folderId,
                activeContentItemId: '',
            };
        case SET_ACTIVE_CONTENT_ITEM_ID:
            return {
                ...state,
                activeContentItemId: action.payload.contentItemId,
            };
        case SET_SELECTED_CONTENT_ITEM_IDS:
            return {
                ...state,
                selectedContentItemIds: action.payload.contentItemIds,
            };

        case GET_FOLDERS:
            return {
                ...state,
                gettingFolders: true,
                isGetFoldersSuccess: false,
                gettingFoldersError: null,
            };
        case GET_FOLDERS_SUCCESS:
            return {
                ...state,
                gettingFolders: false,
                isGetFoldersSuccess: true,
                folders: action.payload.folders,
            };
        case GET_FOLDERS_FAIL:
            return {
                ...state,
                gettingFolders: false,
                gettingFoldersError: action.payload.errors,
            };

        case ADD_FOLDER:
            return {
                ...state,
                addingFolder: true,
                addFolderSuccess: false,
                addingFolderError: null,
            };
        case ADD_FOLDER_SUCCESS:
            return {
                ...state,
                addingFolder: false,
                addFolderSuccess: action.payload,
            };
        case ADD_FOLDER_FAIL:
            return {
                ...state,
                addingFolder: false,
                addingFolderError: action.payload.errors,
            };

        case UPDATE_FOLDER:
            return {
                ...state,
                updatingFolder: true,
                isUpdateFolderSuccess: false,
                updatingFolderError: null,
            };
        case UPDATE_FOLDER_SUCCESS:
            return {
                ...state,
                updatingFolder: false,
                isUpdateFolderSuccess: true,
            };
        case UPDATE_FOLDER_FAIL:
            return {
                ...state,
                updatingFolder: false,
                updatingFolderError: action.payload.errors,
            };

        case DELETE_FOLDER:
            return {
                ...state,
                deleteFolder: true,
                isDeleteFolderSuccess: false,
                deleteFolderError: null,
            };
        case DELETE_FOLDER_SUCCESS:
            return {
                ...state,
                deleteFolder: false,
                isDeleteFolderSuccess: true,
                activeFolderId: '',
            };
        case DELETE_FOLDER_FAIL:
            return {
                ...state,
                deleteFolder: false,
                deleteFolderError: action.payload.errors,
            };

        case GET_CONTENT_ITEMS:
            return {
                ...state,
                gettingContentItems: true,
                gettingContentItemsError: null,
            };
        case GET_CONTENT_ITEMS_SUCCESS:
            return {
                ...state,
                gettingContentItems: false,
                contentItems: action.payload.contentItems,
            };
        case GET_CONTENT_ITEMS_FAIL:
            return {
                ...state,
                gettingContentItems: false,
                gettingContentItemsError: action.payload.errors,
            };

        case SET_START_UPLOAD_CONTENT_ITEMS:
            return {
                ...state,
                uploadingContentItems: action.payload.status,
                uploadingContentItemsError: null,
            };

        case UPLOAD_CONTENT_ITEM:
            return {
                ...state,
                uploadingContentItems: true,
                uploadingContentItemsError: null,
            };
        case UPLOAD_CONTENT_ITEM_SUCCESS:
            return {
                ...state,
                uploadingContentItems: false,
            };
        case UPLOAD_CONTENT_ITEM_FAIL:
            return {
                ...state,
                uploadingContentItems: false,
                uploadingContentItemsError: action.payload.errors,
            };

        case UPDATE_CONTENT_ITEM:
        case UPDATE_CONTENT_ITEMS:
            return {
                ...state,
                updatingContentItem: true,
                isUpdateContentItemSuccess: false,
                updatingContentItemError: null,
            };
        case UPDATE_CONTENT_ITEM_SUCCESS:
        case UPDATE_CONTENT_ITEMS_SUCCESS:
            return {
                ...state,
                updatingContentItem: false,
                isUpdateContentItemSuccess: true,
            };
        case UPDATE_CONTENT_ITEM_FAIL:
        case UPDATE_CONTENT_ITEMS_FAIL:
            return {
                ...state,
                updatingContentItem: false,
                updatingContentItemError: action.payload.errors,
            };

        case SET_UPDATED_PLAYLISTS_WITH_CONTENT:
            return {
                ...state,
                updatedPlaylistsWithContent: action.payload.playlistIds,
            };

        case ADD_CONTENT_TO_PLAYLISTS:
            return {
                ...state,
                addingContentToPlaylists: true,
                isAddContentToPlaylistsSuccess: false,
            };
        case ADD_CONTENT_TO_PLAYLISTS_SUCCESS:
            return {
                ...state,
                addingContentToPlaylists: false,
                isAddContentToPlaylistsSuccess: true,
                updatedPlaylistsWithContent: [],
            };
        case ADD_CONTENT_TO_PLAYLISTS_FAIL:
            return {
                ...state,
                addingContentToPlaylists: false,
            };

        case DELETE_CONTENT_ITEM:
            return {
                ...state,
                deleteContentItem: true,
                isDeleteContentItemSuccess: false,
                deleteContentItemError: null,
            };
        case DELETE_CONTENT_ITEM_SUCCESS:
            return {
                ...state,
                deleteContentItem: false,
                isDeleteContentItemSuccess: true,
            };
        case DELETE_CONTENT_ITEM_FAIL:
            return {
                ...state,
                deleteContentItem: false,
                deleteContentItemError: action.payload.errors,
            };

        case CLEAR_CONTENT_PAGE_SUCCESS:
            return {
                ...state,
                isGetFoldersSuccess: false,
                addFolderSuccess: false,
                isUpdateFolderSuccess: false,
                isDeleteFolderSuccess: false,
                isUpdateContentItemSuccess: false,
                isAddContentToPlaylistsSuccess: false,
                isDeleteContentItemSuccess: false,
            };

        default:
            return state;
    }
}
