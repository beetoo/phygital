import { createSelector } from 'reselect';

import { StoreType } from '../../../types';
import { SelectTagType } from '../../../ui-kit';
import { AddFolderSuccessType } from '../type';
import { CONTENT_TYPE, FolderType } from '../../../../../common/types';

// select active ids

export const selectActiveFolderId = ({
    pages: {
        organization: {
            folders: { activeFolderId },
        },
    },
}: StoreType): string => activeFolderId;

export const selectActiveContentItemId = ({
    pages: {
        organization: {
            folders: { activeContentItemId },
        },
    },
}: StoreType): string => activeContentItemId;

export const selectSelectedContentItemIds = ({
    pages: {
        organization: {
            folders: { selectedContentItemIds },
        },
    },
}: StoreType): string[] => selectedContentItemIds;

// get folders

export const selectFolders = ({
    pages: {
        organization: {
            folders: { folders },
        },
    },
}: StoreType): FolderType[] => folders;

export const selectGetFoldersSuccessStatus = ({
    pages: {
        organization: {
            folders: { isGetFoldersSuccess },
        },
    },
}: StoreType): boolean => isGetFoldersSuccess;

// add folder

export const selectAddingFolderStatus = ({
    pages: {
        organization: {
            folders: { addingFolder },
        },
    },
}: StoreType): boolean => addingFolder;

export const selectAddFolderSuccess = ({
    pages: {
        organization: {
            folders: { addFolderSuccess },
        },
    },
}: StoreType): AddFolderSuccessType => addFolderSuccess;

// delete folder

export const selectDeleteFolderStatus = ({
    pages: {
        organization: {
            folders: { deleteFolder },
        },
    },
}: StoreType): boolean => deleteFolder;

// get content items

export const selectAllContentItems = createSelector(
    (state: StoreType) => state.pages.organization.folders,
    ({ contentItems }) => contentItems,
);

export const selectContentItems = createSelector(
    (state: StoreType) => state.pages.organization.folders,
    ({ contentItems }) => contentItems.filter((item) => item.type !== CONTENT_TYPE.WEB),
);

export const selectWebContentItems = createSelector(
    (state: StoreType) => state.pages.organization.folders,
    ({ contentItems }) => contentItems.filter((item) => item.type === CONTENT_TYPE.WEB),
);

// uploadContent

export const selectUploadingContentStatus = ({
    pages: {
        organization: {
            folders: { uploadingContentItems },
        },
    },
}: StoreType): boolean => uploadingContentItems;

// update content item

export const selectUpdatingContentItemStatus = ({
    pages: {
        organization: {
            folders: { updatingContentItem },
        },
    },
}: StoreType): boolean => updatingContentItem;

export const selectUpdateContentItemSuccessStatus = ({
    pages: {
        organization: {
            folders: { isUpdateContentItemSuccess },
        },
    },
}: StoreType): boolean => isUpdateContentItemSuccess;

// add content to playlists

export const selectUpdatedPlaylistWithContent = ({
    pages: {
        organization: {
            folders: { updatedPlaylistsWithContent },
        },
    },
}: StoreType): SelectTagType[] => updatedPlaylistsWithContent;

export const selectAddContentToPlaylistSuccess = ({
    pages: {
        organization: {
            folders: { isAddContentToPlaylistsSuccess },
        },
    },
}: StoreType): boolean => isAddContentToPlaylistsSuccess;

// delete content item

export const selectDeleteContentItemStatus = ({
    pages: {
        organization: {
            folders: { deleteContentItem },
        },
    },
}: StoreType): boolean => deleteContentItem;

export const selectDeleteContentItemSuccessStatus = ({
    pages: {
        organization: {
            folders: { isDeleteContentItemSuccess },
        },
    },
}: StoreType): boolean => isDeleteContentItemSuccess;
