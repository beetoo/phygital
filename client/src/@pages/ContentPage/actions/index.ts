import { ErrorResponse } from '../../../types';
import { ContentType, FolderType } from '../../../../../common/types';
import { UpdateFolderDto } from '../../../../../server/src/models/folder/dto/update-folder.dto';
import { AddContentToPlaylistDto } from '../type';
import { SelectTagType } from '../../../ui-kit';
import {
    AddFolderPayload,
    UpdateContentItemPayload,
    UpdateContentItemsPayload,
} from '../../../../../common/types/client';

export const SET_ACTIVE_FOLDER_ID = 'SET_ACTIVE_FOLDER_ID';
export const SET_ACTIVE_CONTENT_ITEM_ID = 'SET_ACTIVE_CONTENT_ITEM_ID';
export const SET_SELECTED_CONTENT_ITEM_IDS = 'SET_SELECTED_CONTENT_ITEM_IDS';

export const GET_FOLDERS = 'GET_FOLDERS';
export const GET_FOLDERS_SUCCESS = 'GET_FOLDERS_SUCCESS';
export const GET_FOLDERS_FAIL = 'GET_FOLDERS_FAIL';

export const ADD_FOLDER = 'ADD_FOLDER';
export const ADD_FOLDER_SUCCESS = 'ADD_FOLDER_SUCCESS';
export const ADD_FOLDER_FAIL = 'ADD_FOLDER_FAIL';

export const UPDATE_FOLDER = 'UPDATE_FOLDER';
export const UPDATE_FOLDER_SUCCESS = 'UPDATE_FOLDER_SUCCESS';
export const UPDATE_FOLDER_FAIL = 'UPDATE_FOLDER_FAIL';

export const DELETE_FOLDER = 'DELETE_FOLDER';
export const DELETE_FOLDER_SUCCESS = 'DELETE_FOLDER_SUCCESS';
export const DELETE_FOLDER_FAIL = 'DELETE_FOLDER_FAIL';

export const GET_CONTENT_ITEMS = 'GET_CONTENT_ITEMS';
export const GET_CONTENT_ITEMS_SUCCESS = 'GET_CONTENT_ITEMS_SUCCESS';
export const GET_CONTENT_ITEMS_FAIL = 'GET_CONTENT_ITEMS_FAIL';

export const SET_START_UPLOAD_CONTENT_ITEMS = 'SET_START_UPLOAD_CONTENT_ITEMS';

export const UPLOAD_CONTENT_ITEM = 'UPLOAD_CONTENT_ITEM';
export const UPLOAD_CONTENT_ITEM_SUCCESS = 'UPLOAD_CONTENT_ITEM_SUCCESS';
export const UPLOAD_CONTENT_ITEM_FAIL = 'UPLOAD_CONTENT_ITEM_FAIL';

export const UPDATE_CONTENT_ITEM = 'UPDATE_CONTENT_ITEM';
export const UPDATE_CONTENT_ITEM_SUCCESS = 'UPDATE_CONTENT_ITEM_SUCCESS';
export const UPDATE_CONTENT_ITEM_FAIL = 'UPDATE_CONTENT_ITEM_FAIL';

export const UPDATE_CONTENT_ITEMS = 'UPDATE_CONTENT_ITEMS';
export const UPDATE_CONTENT_ITEMS_SUCCESS = 'UPDATE_CONTENT_ITEMS_SUCCESS';
export const UPDATE_CONTENT_ITEMS_FAIL = 'UPDATE_CONTENT_ITEMS_FAIL';

export const DELETE_CONTENT_ITEM = 'DELETE_CONTENT_ITEM';
export const DELETE_CONTENT_ITEM_SUCCESS = 'DELETE_CONTENT_ITEM_SUCCESS';
export const DELETE_CONTENT_ITEM_FAIL = 'DELETE_CONTENT_ITEM_FAIL';

export const SET_UPDATED_PLAYLISTS_WITH_CONTENT = 'SET_UPDATED_PLAYLISTS_WITH_CONTENT';
export const ADD_CONTENT_TO_PLAYLISTS = 'ADD_CONTENT_TO_PLAYLISTS';
export const ADD_CONTENT_TO_PLAYLISTS_SUCCESS = 'ADD_CONTENT_TO_PLAYLISTS_SUCCESS';
export const ADD_CONTENT_TO_PLAYLISTS_FAIL = 'ADD_CONTENT_TO_PLAYLISTS_FAIL';

export const CLEAR_CONTENT_PAGE_SUCCESS = 'CLEAR_CONTENT_PAGE_SUCCESS';

export const setActiveFolderId = (folderId = '') => ({ type: SET_ACTIVE_FOLDER_ID, payload: { folderId } }) as const;
export const setActiveContentItemId = (contentItemId = '') =>
    ({ type: SET_ACTIVE_CONTENT_ITEM_ID, payload: { contentItemId } }) as const;
export const setSelectedContentItemIds = (contentItemIds: string[] = []) =>
    ({ type: SET_SELECTED_CONTENT_ITEM_IDS, payload: { contentItemIds } }) as const;

export const setGetFolders = (organizationId: string) => ({ type: GET_FOLDERS, payload: { organizationId } }) as const;
export const setGetFoldersSuccess = (folders: FolderType[]) =>
    ({ type: GET_FOLDERS_SUCCESS, payload: { folders } }) as const;
export const setGetFoldersFail = (errors: ErrorResponse) => ({ type: GET_FOLDERS_FAIL, payload: { errors } }) as const;

export const setAddFolder = (createFolderDto: AddFolderPayload) =>
    ({ type: ADD_FOLDER, payload: createFolderDto }) as const;
export const setAddFolderSuccess = (id: string) => ({ type: ADD_FOLDER_SUCCESS, payload: { id } }) as const;
export const setAddFolderFail = (errors: ErrorResponse) => ({ type: ADD_FOLDER_FAIL, payload: { errors } }) as const;

export const setUpdateFolder = (updateFolderDto: UpdateFolderDto) =>
    ({ type: UPDATE_FOLDER, payload: updateFolderDto }) as const;
export const setUpdateFolderSuccess = () => ({ type: UPDATE_FOLDER_SUCCESS }) as const;
export const setUpdateFolderFail = (errors: ErrorResponse) =>
    ({ type: UPDATE_FOLDER_FAIL, payload: { errors } }) as const;

export const setDeleteFolder = (folderId: string | string[]) =>
    ({ type: DELETE_FOLDER, payload: { folderId } }) as const;
export const setDeleteFolderSuccess = () => ({ type: DELETE_FOLDER_SUCCESS }) as const;
export const setDeleteFolderFail = (errors: ErrorResponse) =>
    ({ type: DELETE_FOLDER_FAIL, payload: { errors } }) as const;

export const setGetContentItems = (organizationId: string) =>
    ({ type: GET_CONTENT_ITEMS, payload: { organizationId } }) as const;
export const setGetContentItemsSuccess = (contentItems: ContentType[]) =>
    ({ type: GET_CONTENT_ITEMS_SUCCESS, payload: { contentItems } }) as const;
export const setGetContentItemsFail = (errors: ErrorResponse) =>
    ({ type: GET_CONTENT_ITEMS_FAIL, payload: { errors } }) as const;

export const setUpdatedPlaylistsWithContent = (playlistIds: SelectTagType[]) =>
    ({ type: SET_UPDATED_PLAYLISTS_WITH_CONTENT, payload: { playlistIds } }) as const;

export const setAddContentToPlaylists = (addContentToPlaylistDto: AddContentToPlaylistDto) =>
    ({ type: ADD_CONTENT_TO_PLAYLISTS, payload: addContentToPlaylistDto }) as const;
export const setAddContentToPlaylistsSuccess = () => ({ type: ADD_CONTENT_TO_PLAYLISTS_SUCCESS }) as const;
export const setAddContentToPlaylistsFail = (errors: ErrorResponse) =>
    ({ type: ADD_CONTENT_TO_PLAYLISTS_FAIL, payload: { errors } }) as const;

export const setStartUploadContentItems = (status: boolean) =>
    ({ type: SET_START_UPLOAD_CONTENT_ITEMS, payload: { status } }) as const;

export const setUploadContentItem = (
    organizationId: string,
    folderId: string,
    files: File[],
    uploadedItemsInfo: Pick<ContentType, 'originFilename' | 'dimensions' | 'duration'>[],
) =>
    ({
        type: UPLOAD_CONTENT_ITEM,
        payload: { organizationId, folderId, files, uploadedItemsInfo },
    }) as const;
export const setUploadContentItemSuccess = () => ({ type: UPLOAD_CONTENT_ITEM_SUCCESS }) as const;
export const setUploadContentItemFail = (errors: ErrorResponse) =>
    ({ type: UPLOAD_CONTENT_ITEM_FAIL, payload: { errors } }) as const;

export const setUpdateContentItem = (contentId: string, updateContentDto: UpdateContentItemPayload) =>
    ({ type: UPDATE_CONTENT_ITEM, payload: { contentId, updateContentDto } }) as const;
export const setUpdateContentItemSuccess = () => ({ type: UPDATE_CONTENT_ITEM_SUCCESS }) as const;
export const setUpdateContentItemFail = (errors: ErrorResponse) =>
    ({ type: UPDATE_CONTENT_ITEM_FAIL, payload: { errors } }) as const;

export const setUpdateContentItems = (contentIds: string[], updateContentDto: UpdateContentItemsPayload) =>
    ({ type: UPDATE_CONTENT_ITEMS, payload: { contentIds, updateContentDto } }) as const;
export const setUpdateContentItemsSuccess = () => ({ type: UPDATE_CONTENT_ITEMS_SUCCESS }) as const;
export const setUpdateContentItemsFail = (errors: ErrorResponse) =>
    ({ type: UPDATE_CONTENT_ITEMS_FAIL, payload: { errors } }) as const;

export const setDeleteContentItem = (contentIds: string[]) =>
    ({ type: DELETE_CONTENT_ITEM, payload: { contentIds } }) as const;
export const setDeleteContentItemSuccess = () => ({ type: DELETE_CONTENT_ITEM_SUCCESS }) as const;
export const setDeleteContentItemFail = (errors: ErrorResponse) =>
    ({ type: DELETE_CONTENT_ITEM_FAIL, payload: { errors } }) as const;

export const setClearContentPageSuccess = () => ({ type: CLEAR_CONTENT_PAGE_SUCCESS }) as const;
