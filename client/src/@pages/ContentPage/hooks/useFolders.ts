import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { selectActiveFolderId, selectFolders, selectContentItems, selectWebContentItems } from '../selectors';
import { setActiveFolderId } from '../actions';
import { useIsWebContentMode } from './useIsWebContentMode';
import { ContentType, FolderType } from '../../../../../common/types';

type ReturnValue = {
    activeFolderId: string;
    activeContentItems: ContentType[];
    folders: FolderType[];
    selectFolder: (folderId: string) => () => void;
    contentItemsWithoutFolders: ContentType[];
    numberOfContentItemsWithoutFolders: number;
};

export const useFolders = (): ReturnValue => {
    const dispatch = useDispatch();

    const { isWebContentMode } = useIsWebContentMode();

    const activeFolderId = useSelector(selectActiveFolderId);
    const folders = useSelector(selectFolders);
    const contentItems = useSelector(selectContentItems);
    const webContentItems = useSelector(selectWebContentItems);

    const contentItemsWithoutFolders = useMemo(() => contentItems.filter(({ folder }) => !folder), [contentItems]);
    const numberOfContentItemsWithoutFolders = useMemo(
        () => contentItemsWithoutFolders.length ?? 0,
        [contentItemsWithoutFolders.length],
    );

    const activeContentItems = useMemo(
        () =>
            isWebContentMode
                ? webContentItems
                : contentItems.filter(({ folder }) =>
                      activeFolderId === '' ? !folder : folder?.id === activeFolderId,
                  ),
        [activeFolderId, contentItems, isWebContentMode, webContentItems],
    );

    const selectFolder = useCallback(
        (folderId: string) => () => {
            dispatch(setActiveFolderId(folderId));
        },
        [dispatch],
    );

    return {
        activeFolderId,
        folders,
        activeContentItems,
        selectFolder,
        contentItemsWithoutFolders,
        numberOfContentItemsWithoutFolders,
    };
};
