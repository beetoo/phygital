import { useLocation } from 'react-router-dom';

type ReturnValue = {
    isWebContentMode: boolean;
};

export const useIsWebContentMode = (): ReturnValue => {
    const { pathname } = useLocation();

    return {
        isWebContentMode: pathname.endsWith('web-content'),
    };
};
