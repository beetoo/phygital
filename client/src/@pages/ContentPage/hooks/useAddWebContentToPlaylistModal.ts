import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setHideModal } from '../../../components/ModalsContainer/actions';
import { useAddContentToPlaylist } from '../../PlaylistsPage/components/PlaylistStepTwo/hooks/useAddContentToPlaylist';
import { useTimeLineRef } from '../../PlaylistsPage/components/PlaylistStepTwo/hooks/useTimeLIneRef';

type ReturnValue = {
    addWebContentToPlaylist: (contentItemId: string) => () => void;
    closeAddWebContentModal: () => void;

    t: TFunction;
};

export const useAddWebContentToPlaylistModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'content' });

    const dispatch = useDispatch();

    const { addWebContentToPlaylist: addWebContent } = useAddContentToPlaylist();

    const { scrollToRightOrBottom } = useTimeLineRef();

    const addWebContentToPlaylist = useCallback(
        (contentItemId: string) => () => {
            addWebContent(contentItemId);
            scrollToRightOrBottom();
            dispatch(setHideModal());
        },
        [addWebContent, dispatch, scrollToRightOrBottom],
    );

    const closeAddWebContentModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    return {
        addWebContentToPlaylist,
        closeAddWebContentModal,
        t,
    };
};
