import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { ContentType } from '../../../../../common/types';
import { selectActiveContentItemId, selectSelectedContentItemIds, selectWebContentItems } from '../selectors';
import { setActiveContentItemId, setGetContentItems, setSelectedContentItemIds } from '../actions';
import { MODALS } from '../../../components/ModalsContainer/types';
import { setShowModal } from '../../../components/ModalsContainer/actions';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';

type ReturnValue = {
    webContentItems: ContentType[];
    activeContentItemId: string;
    setActiveContentItem: (contentItemId: string) => () => void;
    isContentItemSelected: (contentItemId: string) => boolean;
    selectContentItem: (contentItemId: string) => void;
    unselectContentItem: (contentItemId: string) => void;
    isAllContentItemsSelected: boolean;
    isSomeContentItemsSelected: boolean;
    selectUnselectAllContentItems: () => void;
    openDeleteContentItemModal: (contentItemId: string) => void;

    t: TFunction;
};

export const useWebContentItemsList = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'content' });

    const dispatch = useDispatch();
    const { id: organizationId } = useSelector(selectCurrentOrganization);

    const webContentItems = useSelector(selectWebContentItems);

    useEffect(() => {
        if (organizationId) {
            dispatch(setGetContentItems(organizationId));
        }
    }, [dispatch, organizationId]);

    const activeContentItemId = useSelector(selectActiveContentItemId);
    const selectedContentItemIds = useSelector(selectSelectedContentItemIds);

    // при выделении контента обнуляем active contentItemId
    useEffect(() => {
        if (selectedContentItemIds.length > 0) {
            dispatch(setActiveContentItemId());
        }
    }, [dispatch, selectedContentItemIds.length]);

    const isAllContentItemsSelected = useMemo(
        () => webContentItems.length > 0 && webContentItems.length === selectedContentItemIds.length,
        [selectedContentItemIds.length, webContentItems.length],
    );

    const isSomeContentItemsSelected = useMemo(
        () => selectedContentItemIds.length > 0,
        [selectedContentItemIds.length],
    );

    const selectUnselectAllContentItems = useCallback(() => {
        dispatch(setSelectedContentItemIds(isAllContentItemsSelected ? [] : webContentItems.map(({ id }) => id)));
    }, [dispatch, isAllContentItemsSelected, webContentItems]);

    const isContentItemSelected = useCallback(
        (contentItemId: string) => selectedContentItemIds.some((id) => id === contentItemId),
        [selectedContentItemIds],
    );

    const selectContentItem = useCallback(
        (contentItemId: string) => {
            dispatch(setSelectedContentItemIds([...selectedContentItemIds, contentItemId]));
        },
        [dispatch, selectedContentItemIds],
    );

    const unselectContentItem = useCallback(
        (contentItemId: string) => {
            dispatch(setSelectedContentItemIds(selectedContentItemIds.filter((id) => id !== contentItemId)));
        },
        [dispatch, selectedContentItemIds],
    );

    const setActiveContentItem = useCallback(
        (contentItemId: string) => () => {
            if (contentItemId !== activeContentItemId) {
                dispatch(setActiveContentItemId(contentItemId));
            }
        },
        [activeContentItemId, dispatch],
    );

    const openDeleteContentItemModal = useCallback(
        (contentItemId: string) => {
            dispatch(setShowModal(MODALS.DELETE_CONTENT_ITEM, '', '', contentItemId));
        },
        [dispatch],
    );

    return {
        webContentItems,
        activeContentItemId,
        setActiveContentItem,
        isContentItemSelected,
        selectContentItem,
        unselectContentItem,
        isAllContentItemsSelected,
        isSomeContentItemsSelected,
        selectUnselectAllContentItems,
        openDeleteContentItemModal,

        t,
    };
};
