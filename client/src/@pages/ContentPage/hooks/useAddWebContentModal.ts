import { ChangeEvent, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setHideModal } from '../../../components/ModalsContainer/actions';
import { httpService } from '../../../helpers/httpService';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { setActiveContentItemId, setGetContentItems } from '../actions';

type ReturnValue = {
    webContentName: string;
    webContentLink: string;
    onWebContentNameChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onWebContentLinkChange: (e: ChangeEvent<HTMLInputElement>) => void;
    isSubmitButtonDisabled: boolean;
    addWebContent: () => void;
    closeAddWebContentModal: () => void;

    t: TFunction;
};

export const useAddWebContentModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'content' });

    const dispatch = useDispatch();

    const { id: organizationId } = useSelector(selectCurrentOrganization);

    const [webContentName, setWebContentName] = useState('');
    const [webContentLink, setWebContentLink] = useState('');

    const onWebContentNameChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setWebContentName(value);
    }, []);

    const onWebContentLinkChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setWebContentLink(value);
    }, []);

    const addWebContent = useCallback(() => {
        httpService
            .post(`organization/${organizationId}/content/web`, { name: webContentName, link: webContentLink })
            .then((data) => {
                dispatch(setGetContentItems(organizationId));
                dispatch(setHideModal());

                const contentItemId = (data as { id: string }).id;
                if (contentItemId) {
                    dispatch(setActiveContentItemId(contentItemId));
                }
            });
    }, [dispatch, organizationId, webContentLink, webContentName]);

    const closeAddWebContentModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    const isSubmitButtonDisabled = webContentName.trim() === '' || webContentLink.trim() === '';

    return {
        webContentName,
        webContentLink,
        onWebContentNameChange,
        onWebContentLinkChange,
        isSubmitButtonDisabled,
        addWebContent,
        closeAddWebContentModal,
        t,
    };
};
