import { ChangeEvent, DragEvent, RefObject, useCallback, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';

import { getImageOrVideoDimensionAndDuration } from '../../../helpers';
import { selectUploadingContentStatus } from '../selectors';
import { setShowModal, setUploadableFiles } from '../../../components/ModalsContainer/actions';
import { selectUploadableFiles } from '../../../components/ModalsContainer/selectors';
import { MODALS, UploadableFile } from '../../../components/ModalsContainer/types';
import { CONTENT_UPLOAD_WARNINGS, SIGNAGE_MAX_ONE_FILE_SIZE } from '../constants';
import { getContentFileInfo } from '../../../../../common/helpers';
import { useAlreadyUploadedFileSize } from '../../../hooks/useAlreadyUploadedFileSize';
import { useUploadFile } from '../../../hooks/useUploadFile';

type ReturnValue = {
    limitStorageSize: number;
    alreadyUploadedFilesSize: number;
    isStorageLimitExceeded: boolean;
    uploadInputRef: RefObject<HTMLInputElement>;
    openUploadContentModal: () => void;
    dropHandler: (event: DragEvent<HTMLDivElement>) => void;
    selectHandler: ({ target: { files } }: ChangeEvent<HTMLInputElement>) => void;
    uploadFile: (uploadableFileId: string) => () => Promise<void>;
    isUploadingContent: boolean;
};

export const useUploadContentFiles = (): ReturnValue => {
    const dispatch = useDispatch();

    const uploadableFiles = useSelector(selectUploadableFiles);
    const isUploadingContent = useSelector(selectUploadingContentStatus);

    const uploadInputRef = useRef<HTMLInputElement>(null);

    const { uploadFile } = useUploadFile();

    const { alreadyUploadedFilesSize, isStorageLimitExceeded, limitStorageSize } = useAlreadyUploadedFileSize();

    const openUploadContentModal = useCallback(() => {
        dispatch(setUploadableFiles());
        dispatch(setShowModal(MODALS.UPLOAD_CONTENT_MODAL));
    }, [dispatch]);

    const mapWithWarnings = useCallback(
        (files: UploadableFile[]) => {
            let fullSize = alreadyUploadedFilesSize;

            return Promise.all(
                files.map(async ({ file, ...rest }) => {
                    const isSupportedFileType = getContentFileInfo(file.name).isRequiredContentType;

                    const { width, height, duration } = await getImageOrVideoDimensionAndDuration(file);

                    const defaultFile = { file, dimensions: { width, height }, duration, ...rest };

                    if (!isSupportedFileType) {
                        return { ...defaultFile, warning: CONTENT_UPLOAD_WARNINGS.SIGNAGE_CONTENT_TYPE_WARNING };
                    }

                    if (file.size > SIGNAGE_MAX_ONE_FILE_SIZE * 1024 * 1024) {
                        return {
                            ...defaultFile,
                            warning: CONTENT_UPLOAD_WARNINGS.SIGNAGE_MAX_ONE_FILE_SIZE_WARNING,
                        };
                    }

                    fullSize += file.size;
                    if (fullSize > (limitStorageSize - alreadyUploadedFilesSize) * 1024 * 1024) {
                        return {
                            ...defaultFile,
                            warning: CONTENT_UPLOAD_WARNINGS.SIGNAGE_MAX_STORAGE_SIZE_WARNING,
                        };
                    }

                    return defaultFile;
                }),
            );
        },
        [alreadyUploadedFilesSize, limitStorageSize],
    );

    const saveFiles = useCallback(
        async (files: FileList | null) => {
            if (files && files.length > 0) {
                const mappedFiles = [...files].map((file) => ({
                    id: uuidv4(),
                    file,
                    progress: 0,
                    done: false,
                    error: false,
                }));
                const mappedWithWarnings = await mapWithWarnings([...uploadableFiles, ...mappedFiles]);

                dispatch(setUploadableFiles(mappedWithWarnings));
            }
        },
        [dispatch, mapWithWarnings, uploadableFiles],
    );

    const dropHandler = useCallback(
        async (event: DragEvent<HTMLDivElement>) => {
            event.preventDefault();

            await saveFiles(event.dataTransfer.files);
        },
        [saveFiles],
    );

    const selectHandler = useCallback(
        async ({ target: { files } }: ChangeEvent<HTMLInputElement>) => {
            await saveFiles(files);

            if (uploadInputRef.current) {
                uploadInputRef.current.value = '';
            }
        },
        [saveFiles],
    );

    return {
        limitStorageSize,
        alreadyUploadedFilesSize,
        isStorageLimitExceeded,
        uploadInputRef,
        openUploadContentModal,
        uploadFile,
        selectHandler,
        dropHandler,
        isUploadingContent,
    };
};
