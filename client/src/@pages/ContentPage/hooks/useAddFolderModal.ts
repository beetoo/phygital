import { ChangeEvent, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setAddFolder } from '../actions';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { selectAddingFolderStatus } from '../selectors';
import { setHideModal } from '../../../components/ModalsContainer/actions';

type ReturnValue = {
    folderName: string;
    folderDescription: string;
    onFolderNameChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onFolderDescriptionChange: (e: ChangeEvent<HTMLInputElement>) => void;
    isSubmitButtonDisabled: boolean;
    handleSubmit: () => void;
    closeModal: () => void;
    t: TFunction;
};

export const useAddFolderModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'contentModal' });

    const dispatch = useDispatch();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const addingFolder = useSelector(selectAddingFolderStatus);

    const [folderName, setFolderName] = useState('');
    const [folderDescription, setFolderDescription] = useState('');

    const onFolderNameChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setFolderName(value);
    }, []);

    const onFolderDescriptionChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setFolderDescription(value);
    }, []);

    const handleSubmit = useCallback(() => {
        dispatch(setAddFolder({ organizationId }));
    }, [dispatch, organizationId]);

    const closeModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    const isSubmitButtonDisabled = addingFolder || folderName.trim() === '';

    return {
        folderName,
        folderDescription,
        onFolderNameChange,
        onFolderDescriptionChange,
        isSubmitButtonDisabled,
        handleSubmit,
        closeModal,
        t,
    };
};
