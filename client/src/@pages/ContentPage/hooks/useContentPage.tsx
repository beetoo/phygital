import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import {
    selectActiveContentItemId,
    selectActiveFolderId,
    selectAddContentToPlaylistSuccess,
    selectDeleteContentItemSuccessStatus,
    selectSelectedContentItemIds,
    selectUploadingContentStatus,
} from '../selectors';
import { setActiveContentItemId, setClearContentPageSuccess, setSelectedContentItemIds } from '../actions';
import { AddContentToPlaylistsSnackbar } from '../components/AddContentToPlaylistSnackbar';
import { setShowModal, setUploadableFiles } from '../../../components/ModalsContainer/actions';
import { MODALS } from '../../../components/ModalsContainer/types';
import { useIsWebContentMode } from './useIsWebContentMode';

type ReturnValue = {
    isWebContentMode: boolean;
    isWithoutFoldersMenu: boolean;
    isSingleFolderMenu: boolean;
    isSingleContentItemMenu: boolean;
    isMultiplyContentItemsMenu: boolean;

    openAddContentModal: () => void;
    isUploadingContent: boolean;

    t: TFunction;
};

export const useContentPage = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'content' });

    const dispatch = useDispatch();

    const { isWebContentMode } = useIsWebContentMode();

    const activeFolderId = useSelector(selectActiveFolderId);
    const activeContentItemId = useSelector(selectActiveContentItemId);
    const selectedContentItemIds = useSelector(selectSelectedContentItemIds);
    const isDeleteContentItemSuccess = useSelector(selectDeleteContentItemSuccessStatus);
    const isUploadingContent = useSelector(selectUploadingContentStatus);

    const openAddContentModal = useCallback(() => {
        if (isWebContentMode) {
            dispatch(setShowModal(MODALS.ADD_WEB_CONTENT_MODAL));
        } else {
            dispatch(setUploadableFiles());
            dispatch(setShowModal(MODALS.UPLOAD_CONTENT_MODAL));
        }
    }, [dispatch, isWebContentMode]);

    const isMultiplyContentItemsMenu = selectedContentItemIds.length > 0;
    const isWithoutFoldersMenu = activeFolderId === '' && activeContentItemId === '' && !isMultiplyContentItemsMenu;
    const isSingleFolderMenu = activeFolderId !== '' && activeContentItemId === '' && !isMultiplyContentItemsMenu;
    const isSingleContentItemMenu = activeContentItemId !== '' && !isMultiplyContentItemsMenu;

    // обнуляем activeContentItemId после удаления единицы контента
    useEffect(() => {
        if (isDeleteContentItemSuccess) {
            dispatch(setActiveContentItemId());
            dispatch(setClearContentPageSuccess());
        }
    }, [dispatch, isDeleteContentItemSuccess]);

    // add content to playlist snackbar

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    const isAddContentToPlaylistsSuccess = useSelector(selectAddContentToPlaylistSuccess);

    useEffect(() => {
        if (isAddContentToPlaylistsSuccess) {
            const closeSnackbarKey = enqueueSnackbar(
                <AddContentToPlaylistsSnackbar onClose={() => closeSnackbar(closeSnackbarKey)} />,
                {
                    key: 'AddContentToPlaylists',
                    autoHideDuration: 3000,
                },
            );

            dispatch(setClearContentPageSuccess());
            dispatch(setSelectedContentItemIds());
        }
    }, [closeSnackbar, dispatch, enqueueSnackbar, isAddContentToPlaylistsSuccess]);

    return {
        isWebContentMode,
        isWithoutFoldersMenu,
        isSingleFolderMenu,
        isSingleContentItemMenu,
        isMultiplyContentItemsMenu,

        openAddContentModal,
        isUploadingContent,

        t,
    };
};
