import { ReactElement, useCallback, useMemo, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';

import { selectPlaylists } from '../../PlaylistsPage/selectors';
import { SelectTagType } from '../../../ui-kit';
import { selectUpdatedPlaylistWithContent } from '../selectors';
import { setUpdatedPlaylistsWithContent } from '../actions';

type ReturnValue = {
    isAllTagsSelected: boolean;
    playlistTitles: SelectTagType[];
    filteringPlaylistTags: SelectTagType[];
    onSelectPlaylistTag: (event: SelectChangeEvent) => void;
    onDeletePlaylistTag: (playlistName: string) => void;
    isTagsSelectDisabled: boolean;
};

export const usePlaylistTagsSelect = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'content' });

    const dispatch = useDispatch();

    const playlists = useSelector(selectPlaylists);
    const filteringPlaylistTags = useSelector(selectUpdatedPlaylistWithContent);

    const playlistTitles = useMemo(
        () => [{ id: 'all', name: t('selectAll') }, ...playlists.map(({ id, title: name }) => ({ id, name }))],
        [playlists, t],
    );

    const isAllTagsSelected = useMemo(
        () => playlistTitles.slice(1).length === filteringPlaylistTags.length,
        [filteringPlaylistTags.length, playlistTitles],
    );

    const onSelectPlaylistTag = useCallback(
        (_: SelectChangeEvent, element?: ReactElement) => {
            const playlistId = element?.props.id;
            if (playlistId === 'all') {
                dispatch(setUpdatedPlaylistsWithContent(isAllTagsSelected ? [] : playlistTitles.slice(1)));
            } else {
                const isTagSelected = filteringPlaylistTags.some(({ id }) => id === playlistId);
                const updatedPlaylistTags = isTagSelected
                    ? filteringPlaylistTags.filter(({ id }) => id !== playlistId)
                    : [...filteringPlaylistTags, { id: playlistId, name: element?.props.value }];
                dispatch(setUpdatedPlaylistsWithContent(updatedPlaylistTags));
            }
        },
        [dispatch, filteringPlaylistTags, isAllTagsSelected, playlistTitles],
    );

    const onDeletePlaylistTag = useCallback(
        (playlistId: string) => {
            const deletedPlaylistTags = filteringPlaylistTags.filter(({ id }) => id !== playlistId);
            dispatch(setUpdatedPlaylistsWithContent(deletedPlaylistTags));
        },
        [dispatch, filteringPlaylistTags],
    );

    // очищаем состояние при первом заходе
    useEffect(() => {
        dispatch(setUpdatedPlaylistsWithContent([]));
    }, [dispatch]);

    const isTagsSelectDisabled = playlists.length === 0;

    return {
        isAllTagsSelected,
        playlistTitles,
        filteringPlaylistTags,
        onSelectPlaylistTag,
        onDeletePlaylistTag,
        isTagsSelectDisabled,
    };
};
