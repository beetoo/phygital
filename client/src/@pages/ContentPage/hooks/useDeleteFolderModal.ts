import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setDeleteFolder } from '../actions';
import { selectActiveFolderId, selectDeleteFolderStatus } from '../selectors';
import { setHideModal } from '../../../components/ModalsContainer/actions';

type ReturnValue = {
    isDeletingFolder: boolean;
    closeModal: () => void;
    deleteFolder: () => void;
    t: TFunction;
};

export const useDeleteFolderModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'contentModal' });

    const dispatch = useDispatch();

    const activeFolderId = useSelector(selectActiveFolderId);
    const isDeletingFolder = useSelector(selectDeleteFolderStatus);

    const deleteFolder = useCallback(() => {
        dispatch(setDeleteFolder(activeFolderId));
    }, [activeFolderId, dispatch]);

    const closeModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    return {
        isDeletingFolder,
        closeModal,
        deleteFolder,
        t,
    };
};
