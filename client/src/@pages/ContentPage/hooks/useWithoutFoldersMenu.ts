import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { useIsWebContentMode } from './useIsWebContentMode';
import { selectContentItems } from '../selectors';
import { CONTENT_TYPE } from '../../../../../common/types';

type ReturnValue = {
    isWebContentMode: boolean;
    t: TFunction;
    imagesNumber: number;
    videosNumber: number;
};

export const useWithoutFoldersMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'content' });

    const { isWebContentMode } = useIsWebContentMode();

    const contentItems = useSelector(selectContentItems);

    const contentItemsWithoutFolders = useMemo(() => contentItems.filter(({ folder }) => !folder), [contentItems]);

    const imagesNumber = useMemo(
        () => contentItemsWithoutFolders.filter(({ type }) => type === CONTENT_TYPE.IMAGE).length,
        [contentItemsWithoutFolders],
    );

    const videosNumber = useMemo(
        () => contentItemsWithoutFolders.filter(({ type }) => type === CONTENT_TYPE.VIDEO).length,
        [contentItemsWithoutFolders],
    );

    return {
        isWebContentMode,
        imagesNumber,
        videosNumber,
        t,
    };
};
