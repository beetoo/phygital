import { ChangeEvent, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';

import {
    selectActiveContentItemId,
    selectAddFolderSuccess,
    selectFolders,
    selectGetFoldersSuccessStatus,
    selectUpdateContentItemSuccessStatus,
    selectUpdatingContentItemStatus,
} from '../selectors';
import { setActiveContentItemId, setClearContentPageSuccess, setUpdateContentItem } from '../actions';
import { selectPlaylists } from '../../PlaylistsPage/selectors';
import { setShowModal } from '../../../components/ModalsContainer/actions';
import { MODALS } from '../../../components/ModalsContainer/types';
import {
    getCreateOrUpdateTimeString,
    getFilesizeString,
    getFileTypeAndExtensionString,
    getOrientationString,
} from '../../PlaylistsPage/helpers';
import { formatSecondsToTimeString } from '../../../../../common/helpers';
import { CONTENT_TYPE, FolderType, PlaylistType } from '../../../../../common/types';
import { useCustomRouter } from '../../../hooks/useCustomRouter';
import { setActivePlaylistId } from '../../PlaylistsPage/actions';
import { useInfoShown } from '../../../hooks/useInfoShown';
import { useFolders } from './useFolders';
import { useIsWebContentMode } from './useIsWebContentMode';

type ReturnValue = {
    isWebContentMode: boolean;
    fullFilename: string;
    contentItemName: string;
    webContentItemLink: string;
    onContentItemNameChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onWebContentItemLinkChange: (e: ChangeEvent<HTMLInputElement>) => void;
    isImageType: boolean;
    isVideoType: boolean;
    isWebType: boolean;
    contentItemSrc: string;
    contentItemFileTypeAndExtension: string;
    contentItemSize: string;
    contentItemDimensions: string;
    contentItemOrientation: string;
    contentItemCreatedAt: string;
    contentItemUpdatedAt: string;
    contentItemDuration: string;
    playlistsUsedContentItem: PlaylistType[];
    selectableFolders: FolderType[];
    saveContentItemSettings: () => void;
    openDeleteContentItemAlert: () => void;
    isSaveContentItemSettingsButtonDisabled: boolean;
    isPlaylistsSelectShow: boolean;
    showPlaylistsSelect: () => void;
    goToPlaylist: (playlistId: string) => () => void;
    folderSelectValue: '' | string;
    setActiveFolderId: (folderId: string) => () => void;
    onFolderSelect: (e: SelectChangeEvent) => void;
    isSubmitButtonShown: boolean;

    isInfoShown: boolean;
    isDimensionsShown: boolean;
    showHideInfo: () => void;
    tContent: TFunction;
};

export const useSingleContentItemMenu = (): ReturnValue => {
    const { t: tContent } = useTranslation('translation', { keyPrefix: 'content' });
    const { t: tPlaylists } = useTranslation('translation', { keyPrefix: 'playlists' });

    const dispatch = useDispatch();

    const { isWebContentMode } = useIsWebContentMode();

    const { activeContentItems } = useFolders();

    const { pushToPage } = useCustomRouter();

    const activeContentItemId = useSelector(selectActiveContentItemId);
    const updatingContentItem = useSelector(selectUpdatingContentItemStatus);
    const playlists = useSelector(selectPlaylists);
    const folders = useSelector(selectFolders);
    const isGetFoldersSuccess = useSelector(selectGetFoldersSuccessStatus);
    const isUpdateContentItemSuccess = useSelector(selectUpdateContentItemSuccessStatus);
    const isAddFolderSuccess = useSelector(selectAddFolderSuccess);

    const { isInfoShown, showHideInfo } = useInfoShown(activeContentItemId);

    const [contentItemName, setContentItemName] = useState('');
    const [isPlaylistsSelectShow, setIsPlaylistsSelectShow] = useState(false);

    const [webContentItemLink, setWebContentItemLink] = useState('');

    const [folderId, setFolderId] = useState('');
    const [folderSelectValue, setFolderSelectValue] = useState<'' | string>('');

    const [isSubmitButtonShown, setIsSubmitButtonShown] = useState(false);

    const setActiveFolderId = useCallback(
        (folderId: string) => () => {
            setFolderId(folderId);
        },
        [],
    );

    const onFolderSelect = useCallback(({ target: { value } }: SelectChangeEvent) => {
        setFolderSelectValue(value);
    }, []);

    const selectableFolders = useMemo(
        () => folders.filter(({ contents }) => !contents.find(({ id }) => id === activeContentItemId)),
        [activeContentItemId, folders],
    );

    // после создания локации селект остается на ней
    useEffect(() => {
        if (isAddFolderSuccess && isGetFoldersSuccess) {
            const folder = folders[folders.length - 1];
            setFolderId(folder?.id ?? '');
            setFolderSelectValue(folder?.name ?? '');

            dispatch(setClearContentPageSuccess());
        }
    }, [dispatch, folders, isAddFolderSuccess, isGetFoldersSuccess]);

    // после переноса контента обнуляем contentItemId
    useEffect(() => {
        if (isUpdateContentItemSuccess && folderId) {
            dispatch(setActiveContentItemId());
            dispatch(setClearContentPageSuccess());
        }
    }, [dispatch, folderId, isUpdateContentItemSuccess]);

    const showPlaylistsSelect = useCallback(() => {
        setIsPlaylistsSelectShow((prev) => !prev);
    }, []);

    const activeContentItem = useMemo(
        () =>
            activeContentItems.find(({ id }) => id === activeContentItemId) ?? {
                id: '',
                originFilename: '',
                type: CONTENT_TYPE.UNKNOWN,
                src: '',
                dimensions: { width: 0, height: 0 },
                createdAt: new Date(),
                updatedAt: new Date(),
                duration: 0,
                size: 0,
            },
        [activeContentItemId, activeContentItems],
    );

    const onContentItemNameChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setContentItemName(value);
    }, []);

    const onWebContentItemLinkChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setWebContentItemLink(value);
    }, []);

    const fullFilename = activeContentItem?.originFilename ?? '';

    const {
        isImageType,
        isVideoType,
        isWebType,
        contentItemSrc,
        contentItemFileTypeAndExtension,
        contentItemSize,
        contentItemDimensions,
        contentItemOrientation,
        contentItemCreatedAt,
        contentItemUpdatedAt,
        contentItemDuration,
    } = useMemo(() => {
        const isImageType = activeContentItem?.type === CONTENT_TYPE.IMAGE;
        const isVideoType = activeContentItem?.type === CONTENT_TYPE.VIDEO;
        const isWebType = activeContentItem?.type === CONTENT_TYPE.WEB;

        const contentItemSrc = activeContentItem?.src;

        const contentItemFileTypeAndExtension = getFileTypeAndExtensionString(
            activeContentItem?.originFilename ?? '',
            activeContentItem?.type ?? '',
            tPlaylists,
        );
        const contentItemSize = getFilesizeString(activeContentItem?.size ?? 0);

        const contentItemWidth = activeContentItem?.dimensions?.width ?? 0;
        const contentItemHeight = activeContentItem?.dimensions?.height ?? 0;

        const contentItemDimensions = `${contentItemWidth} × ${contentItemHeight}`;

        const contentItemOrientation = getOrientationString(contentItemWidth, contentItemHeight, tPlaylists);

        const contentItemCreatedAt = getCreateOrUpdateTimeString(activeContentItem?.createdAt ?? new Date());
        const contentItemUpdatedAt = getCreateOrUpdateTimeString(activeContentItem?.updatedAt ?? new Date());
        const contentItemDuration = formatSecondsToTimeString(activeContentItem?.duration ?? 0);

        return {
            isImageType,
            isVideoType,
            isWebType,
            contentItemSrc,
            contentItemFileTypeAndExtension,
            contentItemSize,
            contentItemDimensions,
            contentItemOrientation,
            contentItemCreatedAt,
            contentItemUpdatedAt,
            contentItemDuration,
        };
    }, [
        activeContentItem?.createdAt,
        activeContentItem?.dimensions?.height,
        activeContentItem?.dimensions?.width,
        activeContentItem?.duration,
        activeContentItem?.originFilename,
        activeContentItem?.size,
        activeContentItem?.src,
        activeContentItem?.type,
        activeContentItem?.updatedAt,
        tPlaylists,
    ]);

    const playlistsUsedContentItem = useMemo(
        () =>
            playlists.filter(({ playlistItems }) =>
                playlistItems.find(({ content }) => content?.id === activeContentItemId),
            ),
        [activeContentItemId, playlists],
    );

    const selectedFolderId = useMemo(
        () => selectableFolders.map(({ id }) => ({ id })).find(({ id }) => id === folderId)?.id,
        [folderId, selectableFolders],
    );

    const saveContentItemSettings = useCallback(() => {
        dispatch(
            setUpdateContentItem(activeContentItemId, {
                name: contentItemName,
                folderId: selectedFolderId,
                webContentLink: isWebContentMode ? webContentItemLink : undefined,
            }),
        );
    }, [activeContentItemId, contentItemName, dispatch, isWebContentMode, selectedFolderId, webContentItemLink]);

    const openDeleteContentItemAlert = useCallback(() => {
        dispatch(setShowModal(MODALS.DELETE_CONTENT_ITEM));
    }, [dispatch]);

    const goToPlaylist = useCallback(
        (playlistId: string) => () => {
            dispatch(setActivePlaylistId(playlistId));
            pushToPage('playlists');
        },
        [dispatch, pushToPage],
    );

    useEffect(() => {
        let originFilename = activeContentItem?.originFilename;

        if (activeContentItemId && originFilename) {
            setFolderSelectValue('');
            // without extension
            originFilename = originFilename.replace(/\.[^/.]+$/, '');
            setContentItemName(originFilename);
            setWebContentItemLink(contentItemSrc);
        }
    }, [activeContentItem?.originFilename, activeContentItemId, contentItemSrc]);

    const isSaveContentItemSettingsButtonDisabled = useMemo(
        () =>
            updatingContentItem || isWebContentMode
                ? contentItemName.trim() === '' ||
                  webContentItemLink.trim() === '' ||
                  (contentItemName.trim() === activeContentItem?.originFilename &&
                      webContentItemLink.trim() === activeContentItem?.src)
                : contentItemName.trim() === '' ||
                  (contentItemName.trim() === activeContentItem?.originFilename.replace(/\.[^/.]+$/, '') &&
                      folderId === ''),
        [
            activeContentItem?.originFilename,
            activeContentItem?.src,
            contentItemName,
            folderId,
            isWebContentMode,
            updatingContentItem,
            webContentItemLink,
        ],
    );

    const isDimensionsShown = useMemo(
        () => contentItemDimensions.split('×').every((value) => Number(value) > 0),
        [contentItemDimensions],
    );

    const prevContentItemId = useRef(activeContentItemId);

    useEffect(() => {
        if (prevContentItemId.current !== activeContentItemId) {
            setIsSubmitButtonShown(false);

            prevContentItemId.current = activeContentItemId;
        } else {
            setIsSubmitButtonShown(!isSaveContentItemSettingsButtonDisabled);
        }
    }, [activeContentItemId, isSaveContentItemSettingsButtonDisabled]);

    return {
        isWebContentMode,
        isImageType,
        isVideoType,
        isWebType,
        contentItemName,
        webContentItemLink,
        fullFilename,
        onContentItemNameChange,
        onWebContentItemLinkChange,
        contentItemSrc,
        contentItemFileTypeAndExtension,
        contentItemSize,
        contentItemDimensions,
        contentItemOrientation,
        contentItemCreatedAt,
        contentItemUpdatedAt,
        contentItemDuration,
        playlistsUsedContentItem,
        selectableFolders,
        saveContentItemSettings,
        openDeleteContentItemAlert,
        isSaveContentItemSettingsButtonDisabled,
        isPlaylistsSelectShow,
        showPlaylistsSelect,
        goToPlaylist,
        folderSelectValue,
        setActiveFolderId,
        onFolderSelect,
        isSubmitButtonShown,

        isInfoShown,
        isDimensionsShown,
        showHideInfo,
        tContent,
    };
};
