import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setHideModal } from '../../../components/ModalsContainer/actions';
import { selectSavedWithModalId } from '../../../components/ModalsContainer/selectors';
import { setDeleteContentItem } from '../actions';
import { selectActiveContentItemId, selectDeleteContentItemStatus } from '../selectors';

type ReturnValue = {
    deleteContentItem: boolean;
    doDeleteContentItem: () => void;
    closeDeleteContentItemAlert: () => void;
    t: TFunction;
};

export const useDeleteContentItemAlert = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    const dispatch = useDispatch();

    const savedContentItemId = useSelector(selectSavedWithModalId);
    const deleteContentItem = useSelector(selectDeleteContentItemStatus);
    const activeContentItemId = useSelector(selectActiveContentItemId);

    const doDeleteContentItem = useCallback(() => {
        const multiplyContentItemIds = Array.isArray(savedContentItemId) ? savedContentItemId : [savedContentItemId];
        const contentIds = multiplyContentItemIds.some((id) => id) ? multiplyContentItemIds : [activeContentItemId];

        if (contentIds.length > 0) {
            dispatch(setDeleteContentItem(contentIds));
        }
    }, [activeContentItemId, dispatch, savedContentItemId]);

    const closeDeleteContentItemAlert = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    return {
        deleteContentItem,
        doDeleteContentItem,
        closeDeleteContentItemAlert,
        t,
    };
};
