import { useCallback, useState, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { SIGNAGE_MAX_ONE_FILE_SIZE } from '../constants';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';

type ReturnValue = {
    storageSize: string;
    storageSizeRemain: string;
    maxOneFileUploadSize: string;
    isTariffWithBigSizeStorage: boolean;
    isInfoShown: boolean;
    showHideInfo: () => void;
    t: TFunction;
};

type Props = {
    alreadyUploadedFilesSize: number;
};

export const useUploadContentModalInfoArea = ({ alreadyUploadedFilesSize }: Props): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'contentModal' });

    const { limitStorageSize } = useSelector(selectCurrentOrganization);

    const isTariffWithBigSizeStorage = limitStorageSize > 2500;

    const storageSizeRemain = useMemo(() => {
        const remain = limitStorageSize > alreadyUploadedFilesSize ? limitStorageSize - alreadyUploadedFilesSize : 0;

        return remain >= 1000 ? `${parseFloat((remain / 1000).toFixed(1))} ${t('gb')}` : `${remain} ${t('mb')}`;
    }, [alreadyUploadedFilesSize, limitStorageSize, t]);

    const storageSize = useMemo(
        () => (limitStorageSize >= 1000 ? `${limitStorageSize / 1000} ${t('gb')}` : `${limitStorageSize} ${t('mb')}`),
        [limitStorageSize, t],
    );

    const maxOneFileUploadSize = useMemo(
        () =>
            limitStorageSize > SIGNAGE_MAX_ONE_FILE_SIZE
                ? SIGNAGE_MAX_ONE_FILE_SIZE >= 1000
                    ? `${SIGNAGE_MAX_ONE_FILE_SIZE / 1000} ${t('gb')}`
                    : `${SIGNAGE_MAX_ONE_FILE_SIZE} ${t('mb')}`
                : storageSize,
        [limitStorageSize, storageSize, t],
    );

    const [isInfoShown, setIsInfoShown] = useState(false);

    const showHideInfo = useCallback(() => {
        setIsInfoShown((prev) => !prev);
    }, []);

    return {
        storageSize,
        storageSizeRemain,
        maxOneFileUploadSize,
        isTariffWithBigSizeStorage,
        isInfoShown,
        showHideInfo,
        t,
    };
};
