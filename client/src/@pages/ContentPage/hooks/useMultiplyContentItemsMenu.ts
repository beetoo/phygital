import { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';

import {
    selectActiveFolderId,
    selectDeleteContentItemSuccessStatus,
    selectFolders,
    selectSelectedContentItemIds,
    selectContentItems,
    selectUpdateContentItemSuccessStatus,
    selectUpdatedPlaylistWithContent,
} from '../selectors';
import {
    setAddContentToPlaylists,
    setClearContentPageSuccess,
    setSelectedContentItemIds,
    setUpdateContentItems,
} from '../actions';
import { getFilesizeString } from '../../PlaylistsPage/helpers';
import { setShowModal } from '../../../components/ModalsContainer/actions';
import { MODALS } from '../../../components/ModalsContainer/types';
import { FolderType } from '../../../../../common/types';
import { selectCurrentModal } from '../../../components/ModalsContainer/selectors';

type ReturnValue = {
    numberOfSelectedContentItemIds: number;
    overallFilesize: string;
    unselectAllContentItems: () => void;
    openDeleteContentItemModal: () => void;
    selectableFolders: FolderType[];
    folderSelectValue: '' | string;
    setActiveFolderId: (folderId: string) => () => void;
    onFolderSelect: (e: SelectChangeEvent) => void;
    saveSettings: () => void;
    isSaveSettingsButtonDisabled: boolean;
    t: TFunction;
};

export const useMultiplyContentItemsMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'content' });

    const dispatch = useDispatch();

    const activeFolderId = useSelector(selectActiveFolderId);
    const selectedContentItemIds = useSelector(selectSelectedContentItemIds);
    const contentItems = useSelector(selectContentItems);
    const updatedPlaylistIdsWithContent = useSelector(selectUpdatedPlaylistWithContent);
    const folders = useSelector(selectFolders);
    const isDeleteContentItemsSuccess = useSelector(selectDeleteContentItemSuccessStatus);
    const isUpdateContentItemSuccess = useSelector(selectUpdateContentItemSuccessStatus);
    const currentModal = useSelector(selectCurrentModal);

    const [folderId, setFolderId] = useState('');
    const [folderSelectValue, setFolderSelectValue] = useState<'' | string>('');

    const selectableFolders = useMemo(
        () => folders.filter(({ id }) => id !== activeFolderId),
        [activeFolderId, folders],
    );

    const setActiveFolderId = useCallback(
        (folderId: string) => () => {
            setFolderId(folderId);
        },
        [],
    );

    const onFolderSelect = useCallback(({ target: { value } }: SelectChangeEvent) => {
        setFolderSelectValue(value);
    }, []);

    useEffect(() => {
        // обнуляем селект при закрытии модального окна
        if (folderSelectValue === t('addFolder') && !currentModal) {
            setFolderId('');
            setFolderSelectValue('');
        }
    }, [currentModal, folderSelectValue, t]);

    // после переноса контента обнуляем contentItemId
    useEffect(() => {
        if (isUpdateContentItemSuccess && folderId) {
            setFolderId('');
            setFolderSelectValue('');
            dispatch(setSelectedContentItemIds());
            dispatch(setClearContentPageSuccess());
        }
    }, [dispatch, folderId, isUpdateContentItemSuccess]);

    useEffect(() => {
        if (isDeleteContentItemsSuccess) {
            dispatch(setSelectedContentItemIds([]));
            dispatch(setClearContentPageSuccess());
        }
    }, [dispatch, isDeleteContentItemsSuccess]);

    const selectedContentItems = useMemo(
        () => contentItems.filter(({ id }) => selectedContentItemIds.includes(id)),
        [contentItems, selectedContentItemIds],
    );

    const overallFilesize = useMemo(
        () => getFilesizeString(selectedContentItems.map(({ size }) => size).reduce((prev, curr) => prev + curr, 0)),
        [selectedContentItems],
    );

    const saveSettings = useCallback(() => {
        if (folderId) {
            dispatch(setUpdateContentItems(selectedContentItemIds, { folderId }));
        }

        if (updatedPlaylistIdsWithContent.length > 0) {
            dispatch(
                setAddContentToPlaylists({
                    playlistIds: updatedPlaylistIdsWithContent.map(({ id }) => id),
                    contentIds: selectedContentItemIds,
                }),
            );
        }
    }, [dispatch, folderId, selectedContentItemIds, updatedPlaylistIdsWithContent]);

    const openDeleteContentItemModal = useCallback(() => {
        dispatch(setShowModal(MODALS.DELETE_CONTENT_ITEM, '', '', selectedContentItemIds));
    }, [dispatch, selectedContentItemIds]);

    const unselectAllContentItems = useCallback(() => {
        dispatch(setSelectedContentItemIds([]));
    }, [dispatch]);

    const numberOfSelectedContentItemIds = selectedContentItemIds.length;

    const isSaveSettingsButtonDisabled = folderId === '' && updatedPlaylistIdsWithContent.length === 0;

    return {
        numberOfSelectedContentItemIds,
        overallFilesize,
        unselectAllContentItems,
        openDeleteContentItemModal,
        isSaveSettingsButtonDisabled,
        selectableFolders,
        saveSettings,
        folderSelectValue,
        setActiveFolderId,
        onFolderSelect,
        t,
    };
};
