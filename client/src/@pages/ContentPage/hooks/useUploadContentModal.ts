import { useCallback, DragEvent, RefObject, ChangeEvent } from 'react';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { promiseQueue } from '../../../helpers/PromiseQueue';
import { setHideModal, setUploadableFiles } from '../../../components/ModalsContainer/actions';
import { IterableUploadableFile, useUploadableFiles } from '../../../hooks/useUploadableFiles';
import { CONTENT_UPLOAD_WARNINGS, SIGNAGE_MAX_ONE_FILE_SIZE } from '../constants';
import { useUploadContentFiles } from './useUploadContentFiles';

type ReturnValue = {
    isStorageLimitExceeded: boolean;
    alreadyUploadedFilesSize: number;
    iterableUploadableFiles: IterableUploadableFile[];
    uploadableFilesWithoutWarnings: IterableUploadableFile[];
    deleteFile: (uploadableFileId: string) => () => void;
    closeUploadContentModal: () => void;

    uploadInputRef: RefObject<HTMLInputElement>;
    dropHandler: (e: DragEvent<HTMLDivElement>) => void;
    selectHandler: ({ target: { files } }: ChangeEvent<HTMLInputElement>) => void;
    startUploading: () => void;
    getUploadContentWarningMessage: (warning: CONTENT_UPLOAD_WARNINGS) => string;
    t: TFunction;
};

export const useUploadContentModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'contentModal' });

    const dispatch = useDispatch();

    const {
        uploadInputRef,
        uploadFile,
        dropHandler,
        selectHandler,
        limitStorageSize,
        isStorageLimitExceeded,
        alreadyUploadedFilesSize,
    } = useUploadContentFiles();

    const { iterableUploadableFiles, uploadableFilesWithoutWarnings, deleteFile } = useUploadableFiles();

    const startUploading = useCallback(() => {
        dispatch(setHideModal());
        const uploadableIds = uploadableFilesWithoutWarnings.map(({ id }) => id);
        promiseQueue.add(uploadableIds, uploadFile);
        promiseQueue.run();
    }, [dispatch, uploadableFilesWithoutWarnings, uploadFile]);

    const closeUploadContentModal = useCallback(() => {
        dispatch(setHideModal());
        dispatch(setUploadableFiles());
    }, [dispatch]);

    const getUploadContentWarningMessage = useCallback(
        (warning: CONTENT_UPLOAD_WARNINGS) => {
            switch (warning) {
                case CONTENT_UPLOAD_WARNINGS.SIGNAGE_CONTENT_TYPE_WARNING:
                    return t('formatNotSupported');
                case CONTENT_UPLOAD_WARNINGS.SIGNAGE_MAX_ONE_FILE_SIZE_WARNING:
                    return t('fileNotBeUploaded', {
                        size: (SIGNAGE_MAX_ONE_FILE_SIZE / 1000).toString().replace('.', ','),
                        unit: t('gb'),
                    });
                case CONTENT_UPLOAD_WARNINGS.SIGNAGE_MAX_STORAGE_SIZE_WARNING:
                    return t('fileNotBeUploaded', {
                        size: limitStorageSize - alreadyUploadedFilesSize,
                        unit: t('mb'),
                    });
                case CONTENT_UPLOAD_WARNINGS.SIGNAGE_DIMENSIONS_WARNING:
                    return t('resolutionNotSupported');
                default:
                    return '';
            }
        },
        [alreadyUploadedFilesSize, limitStorageSize, t],
    );

    return {
        isStorageLimitExceeded,
        alreadyUploadedFilesSize,
        iterableUploadableFiles,
        uploadableFilesWithoutWarnings,
        deleteFile,
        closeUploadContentModal,

        uploadInputRef,
        dropHandler,
        selectHandler,
        startUploading,
        getUploadContentWarningMessage,
        t,
    };
};
