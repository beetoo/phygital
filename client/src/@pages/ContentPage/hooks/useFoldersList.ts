import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { FolderType } from '../../../../../common/types';
import { setGetPlaylists } from '../../PlaylistsPage/actions';
import {
    setActiveFolderId,
    setGetFolders,
    setSelectedContentItemIds,
    setGetContentItems,
    setAddFolder,
} from '../actions';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { selectActiveContentItemId, selectSelectedContentItemIds } from '../selectors';
import { useFolders } from './useFolders';

type ReturnValue = {
    activeFolderId: string;
    isContentItemActive: boolean;
    numberOfContentItemsWithoutFolders: number;
    addNewFolder: () => void;
    folders: FolderType[];
    onFolderCardClick: (folderId?: string) => () => void;
    t: TFunction;
};

export const useFoldersList = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'content' });

    const dispatch = useDispatch();

    const { activeFolderId, folders, numberOfContentItemsWithoutFolders } = useFolders();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const activeContentItemId = useSelector(selectActiveContentItemId);
    const selectedContentItemIds = useSelector(selectSelectedContentItemIds);

    useEffect(() => {
        if (organizationId) {
            dispatch(setGetContentItems(organizationId));
            dispatch(setGetFolders(organizationId));
            dispatch(setGetPlaylists(organizationId));
        }
    }, [dispatch, organizationId]);

    const addNewFolder = useCallback(() => {
        dispatch(setAddFolder({ organizationId }));
    }, [dispatch, organizationId]);

    const onFolderCardClick = useCallback(
        (folderId = '') =>
            () => {
                dispatch(setActiveFolderId(folderId));
                dispatch(setSelectedContentItemIds([]));
            },
        [dispatch],
    );

    const isContentItemActive = activeContentItemId !== '' || selectedContentItemIds.length > 0;

    return {
        activeFolderId,
        isContentItemActive,
        addNewFolder,
        folders,
        onFolderCardClick,
        numberOfContentItemsWithoutFolders,
        t,
    };
};
