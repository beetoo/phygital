import { ChangeEvent, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectActiveFolderId, selectContentItems, selectFolders } from '../selectors';
import { getCreateOrUpdateTimeString, getFilesizeString } from '../../PlaylistsPage/helpers';
import { setUpdateFolder } from '../actions';
import { setShowModal } from '../../../components/ModalsContainer/actions';
import { MODALS } from '../../../components/ModalsContainer/types';
import { CONTENT_TYPE } from '../../../../../common/types';

type ReturnValue = {
    folderName: string;
    onFolderNameChange: (e: ChangeEvent<HTMLInputElement>) => void;
    folderCreatedAt: string;
    folderUpdatedAt: string;
    folderFilesize: string;
    onSaveFolderSettings: () => void;
    onDeleteFolder: () => void;
    isSaveFolderSettingsButtonDisabled: boolean;
    isSubmitButtonShown: boolean;
    numberOfPictures: number;
    numberOfVideos: number;
    t: TFunction;
};

export const useSingleFolderMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'content' });

    const dispatch = useDispatch();

    const activeFolderId = useSelector(selectActiveFolderId);
    const folders = useSelector(selectFolders);
    const contentItems = useSelector(selectContentItems);

    const [folderName, setFolderName] = useState('');

    const [isSubmitButtonShown, setIsSubmitButtonShown] = useState(false);

    const activeContentItems = useMemo(
        () => contentItems.filter(({ folder }) => folder?.id === activeFolderId),
        [activeFolderId, contentItems],
    );

    const { activeFolder, folderCreatedAt, folderUpdatedAt } = useMemo(() => {
        const activeFolder = folders.find(({ id }) => id === activeFolderId);

        return {
            activeFolder,
            folderCreatedAt: getCreateOrUpdateTimeString(activeFolder?.createdAt ?? ''),
            folderUpdatedAt: getCreateOrUpdateTimeString(activeFolder?.updatedAt ?? ''),
        };
    }, [activeFolderId, folders]);

    const { folderFilesize } = useMemo(() => {
        const filesize = activeContentItems.map(({ size }) => size).reduce((prev, curr) => prev + curr, 0);
        const folderFilesize = getFilesizeString(filesize);

        return {
            folderFilesize,
        };
    }, [activeContentItems]);

    useEffect(() => {
        if (activeFolder) {
            setFolderName(activeFolder.name);
        }
    }, [activeFolder]);

    const onFolderNameChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setFolderName(value);
    }, []);

    const onSaveFolderSettings = useCallback(() => {
        dispatch(setUpdateFolder({ id: activeFolderId, name: folderName }));
    }, [activeFolderId, dispatch, folderName]);

    const onDeleteFolder = useCallback(() => {
        dispatch(setShowModal(MODALS.DELETE_CONTENT_FOLDER));
    }, [dispatch]);

    const numberOfPictures = useMemo(
        () => activeContentItems.filter(({ type }) => type === CONTENT_TYPE.IMAGE).length,
        [activeContentItems],
    );

    const numberOfVideos = useMemo(
        () => activeContentItems.filter(({ type }) => type === CONTENT_TYPE.VIDEO).length,
        [activeContentItems],
    );

    const isSaveFolderSettingsButtonDisabled = useMemo(
        () => folderName.trim() === '' || folderName.trim() === activeFolder?.name,
        [activeFolder?.name, folderName],
    );

    const prevFolderId = useRef(activeFolderId);

    useEffect(() => {
        if (prevFolderId.current !== activeFolderId) {
            setIsSubmitButtonShown(false);

            prevFolderId.current = activeFolderId;
        } else {
            setIsSubmitButtonShown(!isSaveFolderSettingsButtonDisabled);
        }
    }, [activeFolderId, isSaveFolderSettingsButtonDisabled]);

    return {
        folderName,
        onFolderNameChange,
        folderCreatedAt,
        folderUpdatedAt,
        folderFilesize,
        onSaveFolderSettings,
        onDeleteFolder,
        isSaveFolderSettingsButtonDisabled,
        isSubmitButtonShown,
        numberOfPictures,
        numberOfVideos,
        t,
    };
};
