import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { ContentType } from '../../../../../common/types';
import { selectActiveContentItemId, selectSelectedContentItemIds, selectUploadingContentStatus } from '../selectors';
import { setActiveContentItemId, setSelectedContentItemIds } from '../actions';
import { MODALS } from '../../../components/ModalsContainer/types';
import { setOpenContentViewModal, setShowModal, setUploadableFiles } from '../../../components/ModalsContainer/actions';
import { useFolders } from './useFolders';

type ReturnValue = {
    activeContentItems: ContentType[];
    activeContentItemId: string;
    setActiveContentItem: (contentItemId: string) => () => void;
    openContentViewModal: (contentItemId: string) => () => void;
    isContentItemSelected: (contentItemId: string) => boolean;
    selectContentItem: (contentItemId: string) => void;
    unselectContentItem: (contentItemId: string) => void;
    isAllContentItemsSelected: boolean;
    isSomeContentItemsSelected: boolean;
    selectUnselectAllContentItems: () => void;
    openDeleteContentItemModal: (contentItemId: string) => void;
    isSquareScreenOrientation: (dimensions: { width: number; height: number }) => boolean;
    isHorizontalScreenOrientation: (dimensions: { width: number; height: number }) => boolean;
    openUploadContentModal: () => void;
    isUploadingContent: boolean;
    t: TFunction;
};

export const useContentItemsList = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'content' });

    const dispatch = useDispatch();

    const { activeContentItems } = useFolders();

    const activeContentItemId = useSelector(selectActiveContentItemId);
    const selectedContentItemIds = useSelector(selectSelectedContentItemIds);
    const isUploadingContent = useSelector(selectUploadingContentStatus);

    // при выделении контента обнуляем active contentItemId
    useEffect(() => {
        if (selectedContentItemIds.length > 0) {
            dispatch(setActiveContentItemId());
        }
    }, [dispatch, selectedContentItemIds.length]);

    const openUploadContentModal = useCallback(() => {
        dispatch(setUploadableFiles());
        dispatch(setShowModal(MODALS.UPLOAD_CONTENT_MODAL));
    }, [dispatch]);

    const isAllContentItemsSelected = useMemo(
        () => activeContentItems.length > 0 && activeContentItems.length === selectedContentItemIds.length,
        [activeContentItems.length, selectedContentItemIds.length],
    );

    const isSomeContentItemsSelected = useMemo(
        () => selectedContentItemIds.length > 0,
        [selectedContentItemIds.length],
    );

    const selectUnselectAllContentItems = useCallback(() => {
        dispatch(setSelectedContentItemIds(isAllContentItemsSelected ? [] : activeContentItems.map(({ id }) => id)));
    }, [activeContentItems, dispatch, isAllContentItemsSelected]);

    const isContentItemSelected = useCallback(
        (contentItemId: string) => selectedContentItemIds.some((id) => id === contentItemId),
        [selectedContentItemIds],
    );

    const selectContentItem = useCallback(
        (contentItemId: string) => {
            dispatch(setSelectedContentItemIds([...selectedContentItemIds, contentItemId]));
        },
        [dispatch, selectedContentItemIds],
    );

    const unselectContentItem = useCallback(
        (contentItemId: string) => {
            dispatch(setSelectedContentItemIds(selectedContentItemIds.filter((id) => id !== contentItemId)));
        },
        [dispatch, selectedContentItemIds],
    );

    const setActiveContentItem = useCallback(
        (contentItemId: string) => () => {
            if (contentItemId !== activeContentItemId) {
                dispatch(setActiveContentItemId(contentItemId));
            }
        },
        [activeContentItemId, dispatch],
    );

    const openContentViewModal = useCallback(
        (contentItemId: string) => () => {
            const currentIndex = activeContentItems.findIndex(({ id }) => id === contentItemId);
            const viewItems = activeContentItems.map(({ originFilename, type, src, dimensions }) => ({
                name: originFilename,
                type,
                src,
                dimensions,
            }));
            dispatch(setOpenContentViewModal(viewItems, currentIndex));
        },
        [activeContentItems, dispatch],
    );

    const openDeleteContentItemModal = useCallback(
        (contentItemId: string) => {
            dispatch(setShowModal(MODALS.DELETE_CONTENT_ITEM, '', '', contentItemId));
        },
        [dispatch],
    );

    const isHorizontalScreenOrientation = useCallback(
        (dimensions: { width: number; height: number }) => dimensions?.width > dimensions?.height,
        [],
    );

    const isSquareScreenOrientation = useCallback(
        (dimensions: { width: number; height: number }) => dimensions?.width === dimensions?.height,
        [],
    );

    return {
        activeContentItems,
        activeContentItemId,
        setActiveContentItem,
        openContentViewModal,
        isContentItemSelected,
        selectContentItem,
        unselectContentItem,
        isAllContentItemsSelected,
        isSomeContentItemsSelected,
        selectUnselectAllContentItems,
        openDeleteContentItemModal,
        isSquareScreenOrientation,
        isHorizontalScreenOrientation,
        openUploadContentModal,
        isUploadingContent,
        t,
    };
};
