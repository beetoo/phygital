import React from 'react';
import { clsx } from 'clsx';

import { AddButton, Tooltip } from '../../../../ui-kit';
import { useFoldersList } from '../../hooks/useFoldersList';

import iconFolder from '../../../../assets/iconFolder.png';

import css from '../../style.m.css';

const FoldersList: React.FC = () => {
    const {
        activeFolderId,
        isContentItemActive,
        addNewFolder,
        folders,
        onFolderCardClick,
        numberOfContentItemsWithoutFolders,

        t,
    } = useFoldersList();

    return (
        <div className={css.mainFolderBlock}>
            <div className={css.folderPointsSale}>
                <h2>{t('folders')}</h2>
                <div>
                    <Tooltip title={t('tooltipAddFolder')} placement="top">
                        <AddButton onClick={addNewFolder} />
                    </Tooltip>
                </div>
            </div>

            <div className={css.wrapperFolderBlock}>
                <div
                    className={clsx(
                        css.contentWithoutFolder,
                        activeFolderId === '' && css.active,
                        isContentItemActive && css.activeContentItem,
                    )}
                    onClick={onFolderCardClick()}
                >
                    <h2>
                        {t('contentWithoutFolders')} <span>({numberOfContentItemsWithoutFolders})</span>
                    </h2>
                </div>
                {folders.map(({ id, name, contents }, index) => (
                    <div
                        key={index}
                        className={clsx(
                            css.contentFolderItem,
                            id === activeFolderId && css.active,
                            isContentItemActive && css.activeContentItem,
                        )}
                        onClick={onFolderCardClick(id)}
                    >
                        <div className={css.contentFolderItemInfo}>
                            <img src={iconFolder} width="40" height="32" alt="" />
                            <div className={css.contentFolderInfoText}>
                                <div className={css.fieldFolderCardName}>
                                    <h3 className={css.folderCardName}>
                                        <Tooltip title={name} placement="top">
                                            <span>{name}</span>
                                        </Tooltip>
                                    </h3>
                                </div>
                                <div className={css.wrapFieldFolderCardAddress}>
                                    <div className={css.fieldFolderCardAddress}>
                                        <p className={css.folderCardAddress}>
                                            {t('content')}: <span>{contents.length}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default FoldersList;
