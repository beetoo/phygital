import React from 'react';
import { clsx } from 'clsx';

import { useAddWebContentModal } from '../../hooks/useAddWebContentModal';
import { CancelButton } from '../../../../ui-kit';

import css from './style.m.css';

const AddWebContentModal: React.FC = () => {
    const {
        webContentName,
        webContentLink,
        onWebContentNameChange,
        onWebContentLinkChange,
        addWebContent,
        isSubmitButtonDisabled,
        closeAddWebContentModal,
        t,
    } = useAddWebContentModal();

    return (
        <form className={css.popupAddPlaylist} onSubmit={(e) => e.preventDefault()}>
            <h2>{t('addWebPage')}</h2>

            <label>{t('pageName')} *</label>
            <input
                className={css.enterNamePlaylist}
                placeholder={t('addPageName')}
                value={webContentName}
                onChange={onWebContentNameChange}
                maxLength={125}
            />

            <label>Адрес *</label>
            <input
                className={clsx(css.enterNamePlaylist, css.last)}
                placeholder={t('addPageAddress')}
                value={webContentLink}
                onChange={onWebContentLinkChange}
                maxLength={125}
            />
            <div className={css.containerBtn}>
                <a className={css.btnCancel}>
                    <CancelButton text={t('cancel')} onClick={closeAddWebContentModal} />
                </a>
                <button
                    type="submit"
                    className={css.buttonCreate}
                    onClick={addWebContent}
                    disabled={isSubmitButtonDisabled}
                >
                    {t('add')}
                </button>
            </div>
        </form>
    );
};

export default AddWebContentModal;
