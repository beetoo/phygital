import React from 'react';
import { clsx } from 'clsx';

import StorageSizeWarning from '../../../../../components/StorageSizeWarning';
import { useUploadContentModalInfoArea } from '../../../hooks/useUploadContentModalInfoArea';

import css from '../style.m.css';

type Props = {
    isStorageLimitExceeded: boolean;
    alreadyUploadedFilesSize: number;
};

export const UploadContentModalInfoArea: React.FC<Props> = ({ isStorageLimitExceeded, alreadyUploadedFilesSize }) => {
    const {
        storageSize,
        storageSizeRemain,
        maxOneFileUploadSize,
        isTariffWithBigSizeStorage,
        isInfoShown,
        showHideInfo,

        t,
    } = useUploadContentModalInfoArea({ alreadyUploadedFilesSize });

    return (
        <div className={css.fieldUploadModalItemTwo}>
            <div className={css.wrapperUploadModalItemTwo}>
                <StorageSizeWarning alreadyUploadedFilesSize={alreadyUploadedFilesSize} />
                <p className={css.uploadModalItemTwo}>
                    {/* Доступно: */}
                    {t('available')}{' '}
                    <span className={clsx(isStorageLimitExceeded && css.uploadModalItemTwoError)}>
                        {storageSizeRemain}
                    </span>{' '}
                    {t('of')} <span>{storageSize}.</span>
                </p>
                <p className={css.uploadModalItemTwo}>
                    {/* Изображения: */}
                    {t('images')} <span>jpg, jpeg, png.</span> {/* Видео: */}
                    {t('video')} <span>mp4.</span>{' '}
                    {!isInfoShown && (
                        <a onClick={showHideInfo}>
                            {t('more')}
                            {/* Подробнее */}
                        </a>
                    )}
                </p>
            </div>

            {isInfoShown && (
                <>
                    {isTariffWithBigSizeStorage ? (
                        <>
                            <p className={css.uploadModalItemTwo}>
                                {/* Максимальный объём 1-го загружаемого файла */}
                                {t('textThree')} — <span>{maxOneFileUploadSize}.</span>
                            </p>
                            <p className={css.uploadModalItemTwo}>
                                {t('textSix')} <span>{maxOneFileUploadSize}</span> {t('textSeven')}
                                {/* Если контент не загружается, возможно выбранный формат файла не
                                поддерживается, превышен максимальный размер 1-го загружаемого файла в{' '}
                                <span>{maxOneFileUploadSize}</span> или не хватает памяти. */}
                            </p>
                        </>
                    ) : (
                        <p className={css.uploadModalItemTwo}>
                            {t('textFive')}
                            {/* Если контент не загружается, возможно не поддерживается формат файла или не хватает памяти. */}
                        </p>
                    )}
                    <p className={css.uploadModalItemTwo}>
                        {t('textFour')} <a onClick={showHideInfo}>{t('hide')}</a>
                        {/* Если контент не отображается на экране, возможно устройство не поддерживает выбранный формат
                        файла. Попробуйте другой формат, обратитесь к инструкции экрана или в техподдержку.
                        <a onClick={showHideInfo}>
                            {' '}
                            Скрыть
                        </a> */}
                    </p>
                </>
            )}
        </div>
    );
};
