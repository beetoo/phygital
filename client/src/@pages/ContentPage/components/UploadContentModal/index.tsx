import React from 'react';
import { clsx } from 'clsx';

import UploadModalsSizeWarningWrapper from '../../../../components/ModalsContainer/components/UploadModalsSizeWarningWrapper';
import { CancelButton, Tooltip } from '../../../../ui-kit';
import { useUploadContentModal } from '../../hooks/useUploadContentModal';
import { ACCEPTED_CONTENT_FORMATS } from '../../../../constants';
import { CONTENT_UPLOAD_WARNINGS } from '../../constants';
import { CONTENT_TYPE } from '../../../../../../common/types';
import { UploadContentModalInfoArea } from './components/UploadContentModalInfoArea';

import css from './style.m.css';

const UploadContentModal: React.FC = () => {
    const {
        isStorageLimitExceeded,
        alreadyUploadedFilesSize,
        iterableUploadableFiles,
        uploadableFilesWithoutWarnings,
        deleteFile,

        dropHandler,
        selectHandler,
        startUploading,
        closeUploadContentModal,
        uploadInputRef,
        getUploadContentWarningMessage,
        t,
    } = useUploadContentModal();

    return (
        <UploadModalsSizeWarningWrapper {...{ alreadyUploadedFilesSize }}>
            <div className={css.uploadModal}>
                <div className={css.fieldUploadModalHeader}>
                    <h2 className={css.uploadModalHeader}>{t('uploadContent')}</h2>
                </div>
                <div
                    className={css.fieldUploadModalItemOne}
                    onDrop={dropHandler}
                    onDragOver={(event) => event.preventDefault()}
                >
                    <p className={clsx(css.uploadModalItemOne, isStorageLimitExceeded && css.disabled)}>
                        <input
                            ref={uploadInputRef}
                            type="file"
                            onChange={selectHandler}
                            style={{ display: 'none' }}
                            accept={ACCEPTED_CONTENT_FORMATS}
                            multiple={true}
                            autoSave="false"
                        />
                        {t('dragAndDropFilesIntoThisAreaOr')}&nbsp;
                        <a onClick={isStorageLimitExceeded ? undefined : () => uploadInputRef.current?.click()}>
                            {t('selectOnYourComputer')}
                        </a>
                    </p>
                </div>
                <button
                    className={clsx(css.btnDownloadContent, css.select)}
                    onClick={() => uploadInputRef.current?.click()}
                    disabled={isStorageLimitExceeded}
                >
                    {t('selectFile')}
                </button>
                <UploadContentModalInfoArea {...{ isStorageLimitExceeded, alreadyUploadedFilesSize }} />
                <div className={css.fieldUploadModalItemItemThree}>
                    {iterableUploadableFiles.map(({ id, name, size, src, type, warning }) => (
                        <div key={id} className={css.fieldUploadModalDownloadFile}>
                            <div className={css.uploadModalItemItemThreeImg}>
                                {type === CONTENT_TYPE.IMAGE ? (
                                    <img src={src} alt="" />
                                ) : type === CONTENT_TYPE.VIDEO ? (
                                    <video src={src} />
                                ) : null}
                            </div>
                            <div className={css.fieldAboutInfo}>
                                <p className={css.uploadModalItemItemThree}>{name}</p>
                                <p className={clsx(css.uploadModalItemItemThree, css.uploadModalItemSizeError)}>
                                    {warning ? (
                                        <span className={css.uploadModalItemItemError}>
                                            {getUploadContentWarningMessage(warning as CONTENT_UPLOAD_WARNINGS)}
                                        </span>
                                    ) : (
                                        <span className={css.uploadModalItemItemSize}>{size}</span>
                                    )}
                                </p>
                            </div>
                            <Tooltip title={t('removeFromDownloads')} placement="top">
                                <button className={css.btnDownloadContentDelete} onClick={deleteFile(id)} />
                            </Tooltip>
                        </div>
                    ))}
                </div>
                <div className={css.fieldBtn}>
                    <CancelButton text={t('btnCancel')} onClick={closeUploadContentModal} />

                    <button
                        className={css.btnDownloadContent}
                        onClick={startUploading}
                        disabled={uploadableFilesWithoutWarnings.length === 0}
                    >
                        {t('btnUpload')}
                    </button>
                </div>
            </div>
        </UploadModalsSizeWarningWrapper>
    );
};

export default UploadContentModal;
