import React from 'react';
import { clsx } from 'clsx';

import { useAddWebContentToPlaylistModal } from '../../hooks/useAddWebContentToPlaylistModal';
import WebContentList from '../WebContentList';

import css from './style.m.css';

const AddWebContentToPlaylistModal: React.FC = () => {
    const { addWebContentToPlaylist, closeAddWebContentModal, t } = useAddWebContentToPlaylistModal();

    return (
        <div className={clsx(css.uploadModal, css.uploadModalView)}>
            <button className={css.orderBlogClose} onClick={closeAddWebContentModal}></button>
            <div className={css.containerTariff}>
                <h2>{t('webPages')}</h2>
                <WebContentList addWebContentToPlaylist={addWebContentToPlaylist} />
            </div>
        </div>
    );
};

export default AddWebContentToPlaylistModal;
