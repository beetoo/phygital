import React, { ComponentProps } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { Tour } from '../../../../components/Tour';
import { TourBody } from '../../../../components/Tour/components/TourBody';
import { TourHeader } from '../../../../components/Tour/components/TourHeader';

import { selectScreens } from '../../../ScreensPage/selectors';
import { selectPlaylists } from '../../../PlaylistsPage/selectors';
import { selectPlaylistsPageLinkRef, selectScreensPageLinkRef } from '../../../../selectors';

import foldersImg from './images/folders.png';
import uploadImg from './images/upload.png';
import screenImg from './images/screen.png';
import playImg from './images/play.png';

type Steps = ComponentProps<typeof Tour>['steps'];

type Props = {
    addFolderRef: HTMLButtonElement | null;
    uploadContentRef: HTMLButtonElement | null;
};

export const ContentPageTour: React.FC<Props> = ({ addFolderRef, uploadContentRef }: Props) => {
    const { t } = useTranslation('translation', { keyPrefix: 'learningHints' });

    const screensPageLinkRef = useSelector(selectScreensPageLinkRef);
    const playlistsPageLinkRef = useSelector(selectPlaylistsPageLinkRef);
    const screens = useSelector(selectScreens);
    const playlists = useSelector(selectPlaylists);

    const steps = React.useMemo<Steps>(
        () => [
            addFolderRef
                ? {
                      name: 'addFolder',
                      target: addFolderRef,
                      placement: 'bottom',
                      offset: -5,
                      title: (
                          <TourHeader>
                              <img src={foldersImg} width={32} height={32} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textEighteen')}
                                  {/* Папки для контента */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  {t('textNineTeen')}
                                  {/* Создайте папку и загрузите в неё изображения и видео. Группировка контента по папкам
                                  помогает быстрее находить нужный контент и поддерживать порядок. */}
                              </p>
                              <p>
                                  {t('textTwenty')}
                                  {/* Например, можно создать папку для отдельного экрана или локации. */}
                              </p>
                          </TourBody>
                      ),
                  }
                : null,
            uploadContentRef
                ? {
                      name: 'uploadContent',
                      target: uploadContentRef,
                      placement: 'bottom',
                      title: (
                          <TourHeader>
                              <img src={uploadImg} width={32} height={32} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textTwentyOne')}
                                  {/* Загрузите контент */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  {t('textTwentyTwo')}
                                  {/* Контент нужен, чтобы создать плейлист. */}
                              </p>
                              <p>
                                  {t('textTwentyThree')}
                                  {/* Нажмите, чтобы загрузить видео и изображения в библиотеку. */}
                              </p>
                          </TourBody>
                      ),
                  }
                : null,
            screensPageLinkRef && screens.length === 0
                ? {
                      name: 'screenPageLinkInContent',
                      target: screensPageLinkRef,
                      placement: 'right-start',
                      title: (
                          <TourHeader>
                              <img src={screenImg} width={32} height={32} alt="" />
                              <div style={{ marginLeft: '15px' }}>
                                  {t('textNine')}
                                  {/* Начните с добавления экрана */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              {t('textTen')}
                              {/* Первым делом добавьте экраны, чтобы создавать и загружать на них плейлисты. */}
                          </TourBody>
                      ),
                  }
                : null,
            playlistsPageLinkRef && screens.length > 0 && playlists.length === 0
                ? {
                      name: 'playlistsPageLinkInContent',
                      target: playlistsPageLinkRef,
                      placement: 'right-start',
                      title: (
                          <TourHeader>
                              <img src={playImg} width={32} height={32} alt="" />
                              <div style={{ marginLeft: '15px' }}>
                                  {t('textFourteen')}
                                  {/* Теперь создайте плейлист */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              {t('textFifteen')}
                              {/* Чтобы запустить контент на экран, создайте плейлист. Для этого перейдите в раздел
                              «Плейлисты». */}
                          </TourBody>
                      ),
                  }
                : null,
        ],
        [addFolderRef, playlists.length, playlistsPageLinkRef, screens.length, screensPageLinkRef, uploadContentRef, t],
    );

    return <Tour steps={steps} name="ContentPageTour" />;
};
