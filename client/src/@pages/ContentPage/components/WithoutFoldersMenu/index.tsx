import React from 'react';
import { clsx } from 'clsx';

import { Tooltip } from '../../../../ui-kit';
import { useWithoutFoldersMenu } from '../../hooks/useWithoutFoldersMenu';
import iconImage from '../../../../assets/icon22.svg';
import iconYoutubeDefault from '../../../../assets/iconYoutubeDefault.svg';

import css from '../../style.m.css';

const WithoutFoldersMenu: React.FC = () => {
    const { t, isWebContentMode, imagesNumber, videosNumber } = useWithoutFoldersMenu();

    return (
        <div className={clsx(css.rightBlockPageContent, css.pageContentRightBlockEmpty)}>
            {!isWebContentMode && (
                <>
                    <h2 className={clsx(css.contentRightBlockH2, css.contentRightBlockH2_1)}>
                        {t('contentWithoutFolders')}
                    </h2>
                    <h3 className={clsx(css.contentRightBlockH3, css.contentRightBlockH3_3)}>{t('info')}</h3>
                    <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItem5)}>
                        {t('contentTotal')} <span>{imagesNumber + videosNumber}</span>
                    </p>
                    <div className={css.folderStatistics}>
                        <Tooltip title={t('tooltipImages')} placement="bottom">
                            <div className={css.fieldStatisticsItem}>
                                <img src={iconImage} width="22px" height="22px" alt="" />
                                <p className={css.folderItem}>{imagesNumber}</p>
                            </div>
                        </Tooltip>
                        <Tooltip title={t('tooltipVideo')} placement="bottom">
                            <div className={css.fieldStatisticsItem}>
                                <img src={iconYoutubeDefault} width="22px" height="22px" alt="" />
                                <p className={css.folderItem}>{videosNumber}</p>
                            </div>
                        </Tooltip>
                    </div>
                </>
            )}
        </div>
    );
};

export default WithoutFoldersMenu;
