import React from 'react';
import { clsx } from 'clsx';

import { Tooltip } from '../../../../ui-kit';
import { useSingleFolderMenu } from '../../hooks/useSingleFolderMenu';

import iconImage from '../../../../assets/icon22.svg';
import iconYoutubeDefault from '../../../../assets/iconYoutubeDefault.svg';

import css from '../../style.m.css';

const SingleFolderMenu: React.FC = () => {
    const {
        folderName,
        onFolderNameChange,
        folderCreatedAt,
        folderUpdatedAt,
        folderFilesize,
        onSaveFolderSettings,
        onDeleteFolder,
        isSaveFolderSettingsButtonDisabled,
        isSubmitButtonShown,
        numberOfPictures,
        numberOfVideos,
        t,
    } = useSingleFolderMenu();

    return (
        <form className={css.wrapRightBlockPageContent} onSubmit={(e) => e.preventDefault()}>
            <div className={css.rightBlockPageContent}>
                <h2 className={clsx(css.contentRightBlockH2, css.contentRightBlockH2_1)}>
                    {t('settings')}
                    {/* Настройки */}
                </h2>
                <div className={css.wrapInputRightBlockFolder}>
                    <label>{t('folderName')}</label>
                    <input
                        className={css.inputRightBlockFolder}
                        placeholder={t('folderName') as string}
                        value={folderName}
                        onChange={onFolderNameChange}
                        maxLength={125}
                    />
                </div>

                <h3 className={clsx(css.contentRightBlockH3, css.contentRightBlockH3_3)}>{t('info')}</h3>
                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItem1)}>
                    {t('totalContent')} <span>{numberOfPictures + numberOfVideos}</span>
                </p>
                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItem2)}>
                    {t('size')} <span>{folderFilesize}</span>
                </p>
                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItem2)}>
                    {t('created')} <span>{folderCreatedAt}</span>
                </p>
                <p className={css.mainRightMenuInfoItem}>
                    {t('editedR')} <span>{folderUpdatedAt}</span>
                </p>
                <div className={css.folderStatistics}>
                    <Tooltip title={t('tooltipImages')} placement="bottom">
                        <div className={css.fieldStatisticsItem}>
                            <img src={iconImage} width="22px" height="22px" alt="" />
                            <p className={css.folderItem}>{numberOfPictures}</p>
                        </div>
                    </Tooltip>
                    <Tooltip title={t('tooltipVideo')} placement="bottom">
                        <div className={css.fieldStatisticsItem}>
                            <img src={iconYoutubeDefault} width="22px" height="22px" alt="" />
                            <p className={css.folderItem}>{numberOfVideos}</p>
                        </div>
                    </Tooltip>
                </div>

                <div className={css.fieldDeleteFolder}>
                    <Tooltip
                        sx={{
                            width: 239,
                        }}
                        title={t('tooltipBtnDel')}
                        placement="top"
                        disableHoverListener={true}
                    >
                        <button type="button" className={css.btnDeleteFolder} onClick={onDeleteFolder}>
                            <img alt="" />
                            {t('btnDeleteFolder')}
                        </button>
                    </Tooltip>
                </div>
            </div>
            {isSubmitButtonShown && (
                <div className={css.btnFieldRightPageContent}>
                    <button
                        type="submit"
                        className={css.btnSaveSettings}
                        onClick={onSaveFolderSettings}
                        disabled={isSaveFolderSettingsButtonDisabled}
                    >
                        {t('btnSaveSettings')}
                    </button>
                </div>
            )}
        </form>
    );
};

export default SingleFolderMenu;
