import React from 'react';
import { useTranslation } from 'react-i18next';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

import { Snackbar } from '../../../../ui-kit';

export const AddContentToPlaylistsSnackbar: React.FC<{ onClose: () => void }> = ({ onClose }) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    return (
        <Snackbar
            startAdornment={<CheckCircleIcon color="success" />}
            title={t('contentHasBeenAddedSuccessfully')}
            onClose={onClose}
        />
    );
};
