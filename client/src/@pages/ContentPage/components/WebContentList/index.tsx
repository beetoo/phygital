import React from 'react';

import { useWebContentItemsList } from '../../hooks/useWebContentItemsList';

import './index.css';
import './tailwind.css';

type Props = {
    addWebContentToPlaylist?: (contentItemId: string) => () => void;
};

const WebContentList: React.FC<Props> = ({ addWebContentToPlaylist }) => {
    const { webContentItems, activeContentItemId, setActiveContentItem, t } = useWebContentItemsList();

    const selected =
        'grid cursor-default select-none grid-cols-2 overflow-hidden rounded-lg border border-blue-600 bg-white ring-2 ring-blue-100';
    const notSelected =
        'grid cursor-default select-none grid-cols-2 overflow-hidden rounded-lg border-2 border-transparent bg-white';

    return (
        <div className={`h-screen ${addWebContentToPlaylist ? 'isModal' : ''} bg-slate-50 p-10`}>
            {webContentItems.length > 0 && (
                <div className="mb-5 grid grid-cols-2 text-sm">
                    <div className="px-5">{t('title')}</div>
                    <div className="px-5">{t('link')}</div>
                </div>
            )}
            <div className="flex w-full flex-col gap-3">
                {webContentItems.map(({ id, originFilename, src }) => (
                    <div
                        key={id}
                        className={activeContentItemId === id ? selected : notSelected}
                        onClick={addWebContentToPlaylist ? addWebContentToPlaylist(id) : setActiveContentItem(id)}
                    >
                        <div className="truncate p-5 text-sm">{originFilename}</div>
                        <div className="truncate p-5 text-sm text-blue-600">{src}</div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default WebContentList;
