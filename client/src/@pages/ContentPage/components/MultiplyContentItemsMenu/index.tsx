import React from 'react';
import { clsx } from 'clsx';

import PlaylistTagsSelect from '../PlaylistTagsSelect';
import { MenuItem, Select } from '../../../../ui-kit';
import { useMultiplyContentItemsMenu } from '../../hooks/useMultiplyContentItemsMenu';

import css from '../../style.m.css';

const MultiplyContentItemsMenu: React.FC = () => {
    const {
        numberOfSelectedContentItemIds,
        overallFilesize,
        unselectAllContentItems,
        openDeleteContentItemModal,
        isSaveSettingsButtonDisabled,
        selectableFolders,
        folderSelectValue,
        setActiveFolderId,
        onFolderSelect,
        saveSettings,
        t,
    } = useMultiplyContentItemsMenu();

    return (
        <form className={css.wrapRightBlockPageContent} onSubmit={(e) => e.preventDefault()}>
            <div className={css.rightBlockPageContent}>
                <h2 className={clsx(css.contentRightBlockH2, css.contentRightBlockH2_2)}>{t('settings')}</h2>
                <h3 className={clsx(css.contentRightBlockH3, css.contentRightBlockH3_1)}>
                    {t('contentUnitsSelected')} <span>{numberOfSelectedContentItemIds}</span>
                </h3>
                <div className={css.fieldDeleteContent}>
                    <button type="button" className={css.btnDeleteContent} onClick={openDeleteContentItemModal}>
                        <img alt="" />
                        {t('btnDeleteContent')}
                        {/* Удалить контент */}
                    </button>
                </div>
                <h3 className={clsx(css.contentRightBlockH3, css.contentRightBlockH3_2)}>{t('info')}</h3>
                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItem4)}>
                    {t('totalSize')} <span>{overallFilesize}</span>
                </p>
                {selectableFolders.length > 1 && (
                    <>
                        <h3 className={clsx(css.contentRightBlockH3, css.contentRightBlockH3_2)}>
                            {t('moveContentAnotherFolder')}
                        </h3>
                        <div style={{ marginLeft: '24px', marginTop: '-8px' }}>
                            <Select
                                placeholder={t('selectFolder') as string}
                                onChange={onFolderSelect}
                                value={folderSelectValue}
                                width={292}
                            >
                                {selectableFolders.map((folder) => (
                                    <MenuItem
                                        onClick={setActiveFolderId(folder.id)}
                                        key={folder.id}
                                        value={folder.name}
                                    >
                                        {folder.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </div>
                        <h3
                            style={{ marginTop: '14px' }}
                            className={clsx(css.contentRightBlockH3, css.contentRightBlockH3_2)}
                        >
                            {t('addContentToPlaylists')}
                        </h3>
                    </>
                )}

                <div style={{ marginLeft: '24px', marginTop: '2px' }}>
                    <PlaylistTagsSelect />
                </div>
            </div>

            <div
                className={clsx(
                    css.btnFieldRightPageContent,
                    !isSaveSettingsButtonDisabled && css.btnFieldRightPageContent2,
                )}
            >
                <button type="button" className={css.btnDeselect} onClick={unselectAllContentItems}>
                    {t('btnUnselect')}
                </button>
                {!isSaveSettingsButtonDisabled && (
                    <button type="submit" className={css.btnSaveSettings} onClick={saveSettings}>
                        {t('btnSaveSettings')}
                    </button>
                )}
            </div>
        </form>
    );
};

export default MultiplyContentItemsMenu;
