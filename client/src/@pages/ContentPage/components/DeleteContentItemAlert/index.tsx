import React from 'react';

import { useDeleteContentItemAlert } from '../../hooks/useDeleteContentItemAlert';

import css from '../../../PlaylistsPage/style.m.css';

const DeleteContentItemAlert: React.FC = () => {
    const { deleteContentItem, doDeleteContentItem, closeDeleteContentItemAlert, t } = useDeleteContentItemAlert();

    return (
        <form className={css.popupDeletePlaylist} onSubmit={(e) => e.preventDefault()}>
            <h2>{t('delContent')}?</h2>
            <p>
                {t('textDelContentOne')}
                <br />
                {t('textDelContentTwo')}
            </p>
            <div className={css.containerBtn}>
                <a className={css.btnCancel}>
                    <button type="button" className={css.buttonCancel} onClick={closeDeleteContentItemAlert}>
                        {t('btnCancel')}
                    </button>
                </a>
                <button
                    type="submit"
                    className={css.buttonDelete}
                    disabled={deleteContentItem}
                    onClick={doDeleteContentItem}
                >
                    {t('btnDelete')}
                </button>
            </div>
        </form>
    );
};

export default DeleteContentItemAlert;
