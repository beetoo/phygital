import React from 'react';

import { useAddFolderModal } from '../../hooks/useAddFolderModal';
import { AddIcon } from '../../../../ui-kit';

import css from './style.m.css';

const AddFolderModal: React.FC = () => {
    const {
        folderName,
        folderDescription,
        onFolderNameChange,
        onFolderDescriptionChange,
        isSubmitButtonDisabled,
        handleSubmit,
        closeModal,
        t,
    } = useAddFolderModal();

    return (
        <form className={css.popupAddPointSale} onSubmit={(e) => e.preventDefault()}>
            <h2>{t('addFolder')}</h2>
            <label>{t('folderName')}</label>
            <input
                className={css.enterNamePoint}
                placeholder={t('folderNameInput') as string}
                value={folderName}
                onChange={onFolderNameChange}
                maxLength={125}
            />
            <label>{t('description')}</label>
            <input
                className={css.enterAddress}
                placeholder={t('descriptionInput') as string}
                value={folderDescription}
                onChange={onFolderDescriptionChange}
                maxLength={125}
            />
            <div className={css.fieldButtons}>
                <a className={css.btnCancel}>
                    <button type="button" className={css.buttonCancel} onClick={closeModal}>
                        {t('btnCancel')}
                    </button>
                </a>
                <button
                    type="submit"
                    className={css.buttonAddScreen}
                    disabled={isSubmitButtonDisabled}
                    onClick={handleSubmit}
                >
                    <AddIcon />
                    {t('btnAdd')}
                </button>
            </div>
        </form>
    );
};

export default AddFolderModal;
