import React from 'react';
import { clsx } from 'clsx';

import ImgOrVideo from '../../../../components/ImgOrVideo';
import { Tooltip, Select, MenuItem } from '../../../../ui-kit';
import ScreenPreviewImage from '../../../../assets/ScreenPreviewImage.png';
import { useSingleContentItemMenu } from '../../hooks/useSingleContentItemMenu';

import css from '../../style.m.css';

const SingleContentItemMenu: React.FC = () => {
    const {
        isWebContentMode,
        isImageType,
        isVideoType,
        isWebType,
        contentItemName,
        fullFilename,
        webContentItemLink,
        onContentItemNameChange,
        onWebContentItemLinkChange,
        contentItemSrc,
        contentItemFileTypeAndExtension,
        contentItemSize,
        contentItemDimensions,
        contentItemOrientation,
        contentItemCreatedAt,
        contentItemUpdatedAt,
        contentItemDuration,
        playlistsUsedContentItem,
        selectableFolders,
        saveContentItemSettings,
        openDeleteContentItemAlert,
        isSaveContentItemSettingsButtonDisabled,
        isPlaylistsSelectShow,
        showPlaylistsSelect,
        goToPlaylist,
        folderSelectValue,
        setActiveFolderId,
        onFolderSelect,
        isInfoShown,
        isDimensionsShown,
        isSubmitButtonShown,
        showHideInfo,
        tContent,
    } = useSingleContentItemMenu();

    return (
        <form className={css.wrapRightBlockPageContent} onSubmit={(e) => e.preventDefault()}>
            <div className={css.rightBlockPageContent}>
                <h2 className={clsx(css.contentRightBlockH2, css.contentRightBlockH2_1)}>
                    {tContent('contentSettings')}
                </h2>
                {contentItemSrc ? (
                    isImageType ? (
                        <ImgOrVideo src={contentItemSrc} width="292" height="164" />
                    ) : isVideoType ? (
                        <div className={css.wrapperVideo}>
                            <ImgOrVideo src={contentItemSrc} width="292" height="164" />
                        </div>
                    ) : isWebType ? (
                        <iframe src={contentItemSrc} width="292" height="164" />
                    ) : (
                        <img src={ScreenPreviewImage} width="292" height="164" alt="" />
                    )
                ) : (
                    <img src={ScreenPreviewImage} width="292" height="164" alt="" />
                )}

                <div className={clsx(css.wrapInputRightBlockFolder, css.wrapInputRightBlockFolder1)}>
                    <label>{tContent(isWebContentMode ? 'title' : 'name')}</label>
                    <Tooltip title={fullFilename} placement="top">
                        <input
                            className={css.inputRightBlockFolder}
                            placeholder={tContent(isWebContentMode ? 'title' : 'name')}
                            value={contentItemName}
                            onChange={onContentItemNameChange}
                            maxLength={125}
                        />
                    </Tooltip>
                </div>
                {isWebContentMode && (
                    <div className={clsx(css.wrapInputRightBlockFolder, css.wrapInputRightBlockFolder1)}>
                        <label>{tContent('address')}</label>
                        <Tooltip title={webContentItemLink} placement="top">
                            <input
                                className={css.inputRightBlockFolder}
                                placeholder={tContent('address')}
                                value={webContentItemLink}
                                onChange={onWebContentItemLinkChange}
                                maxLength={1000}
                            />
                        </Tooltip>
                    </div>
                )}
                <div className={css.wrapperInfo} onClick={showHideInfo}>
                    <div className={css.infoHeader}>
                        <h3 className={clsx(css.contentRightBlockH3, css.contentRightBlockH3_2)}>{tContent('info')}</h3>
                        <span className={isInfoShown ? css.iconArrowInfoUp : css.iconArrowInfoDown} />
                    </div>
                    {isInfoShown && (
                        <div>
                            {!isWebContentMode && (
                                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItem2)}>
                                    {tContent('type')} <span>{contentItemFileTypeAndExtension}</span>
                                </p>
                            )}
                            {!isWebContentMode && (
                                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItem2)}>
                                    {tContent('size')} <span>{contentItemSize}</span>
                                </p>
                            )}
                            {isVideoType && (
                                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItem2)}>
                                    {tContent('duration')} <span>{contentItemDuration}</span>
                                </p>
                            )}
                            {isDimensionsShown && (
                                <>
                                    <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItem2)}>
                                        {tContent('resolution')} <span>{contentItemDimensions}</span>
                                    </p>
                                    <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItem2)}>
                                        {tContent('orientation')} <span>{contentItemOrientation}</span>
                                    </p>
                                </>
                            )}
                            <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItem2)}>
                                {tContent('createdRR')} <span>{contentItemCreatedAt}</span>
                            </p>
                            <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItem3)}>
                                {tContent('editedRR')} <span>{contentItemUpdatedAt}</span>
                            </p>
                        </div>
                    )}
                </div>

                <div className={css.wrapMainRightMenuInfoAll}>
                    {playlistsUsedContentItem.length > 0 && (
                        <div className={css.infoHeader} onClick={showPlaylistsSelect}>
                            <h3>
                                {tContent('usedInPlaylists')}&ensp;
                                <span>{playlistsUsedContentItem.length}</span>
                            </h3>
                            <span className={isPlaylistsSelectShow ? css.iconArrowInfoUp : css.iconArrowInfoDown} />
                        </div>
                    )}

                    <div className={css.mainRightMenuInfoAll}>
                        {isPlaylistsSelectShow && (
                            <ul>
                                {playlistsUsedContentItem.map(({ id, title }) => (
                                    <li key={id}>
                                        <p>{title}</p>
                                        <a onClick={goToPlaylist(id)}>{tContent('goTo')}</a>
                                    </li>
                                ))}
                            </ul>
                        )}
                    </div>
                </div>

                {!isWebContentMode && selectableFolders.length > 1 && (
                    <>
                        <h3 className={clsx(css.contentRightBlockH3, css.contentRightBlockH3_2)}>
                            {tContent('moveContentAnotherFolder')}
                        </h3>
                        <div style={{ marginLeft: '24px', marginTop: '-8px', marginBottom: '18px' }}>
                            <Select
                                placeholder={tContent('selectFolder') as string}
                                onChange={onFolderSelect}
                                value={folderSelectValue}
                                width={292}
                            >
                                {selectableFolders.map((folder) => (
                                    <MenuItem
                                        onClick={setActiveFolderId(folder.id)}
                                        key={folder.id}
                                        value={folder.name}
                                    >
                                        {folder.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </div>
                    </>
                )}

                <div className={css.fieldDeleteContent}>
                    <button type="button" className={css.btnDeleteContent} onClick={openDeleteContentItemAlert}>
                        <img alt="" />
                        {tContent('btnDeleteContent')}
                    </button>
                </div>
            </div>
            {isSubmitButtonShown && (
                <div className={clsx(css.btnFieldRightPageContent, css.btnFieldRightPageContent1)}>
                    <button
                        type="submit"
                        className={css.btnSaveSettings}
                        onClick={saveContentItemSettings}
                        disabled={isSaveContentItemSettingsButtonDisabled}
                    >
                        {tContent('btnSaveSettings')}
                    </button>
                </div>
            )}
        </form>
    );
};

export default SingleContentItemMenu;
