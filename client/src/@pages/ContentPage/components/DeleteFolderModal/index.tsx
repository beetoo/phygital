import React from 'react';

import { useDeleteFolderModal } from '../../hooks/useDeleteFolderModal';

import css from './style.m.css';

const DeleteFolderModal: React.FC = () => {
    const { isDeletingFolder, closeModal, deleteFolder, t } = useDeleteFolderModal();

    return (
        <div className={css.popupDeletePlaylist}>
            <h2>{t('deleteFolder')}?</h2>
            <p>
                {t('textOne')}
                <br />
                {t('textTwo')}
            </p>
            <div className={css.fieldButtons}>
                <a className={css.btnCancel}>
                    <button className={css.buttonCancel} onClick={closeModal}>
                        {t('btnCancel')}
                    </button>
                </a>
                <button className={css.buttonDelete} disabled={isDeletingFolder} onClick={deleteFolder}>
                    <img alt="" />
                    {t('btnDelete')}
                </button>
            </div>
        </div>
    );
};

export default DeleteFolderModal;
