import React from 'react';
import { clsx } from 'clsx';

import { SelectButton, Tooltip } from '../../../../ui-kit';
import ImgOrVideo from '../../../../components/ImgOrVideo';
import { useContentItemsList } from '../../hooks/useContentItemsList';
import { CONTENT_TYPE } from '../../../../../../common/types';
import { formatSecondsToTimeString } from '../../../../../../common/helpers';

import css from '../../style.m.css';

const ContentItemsList: React.FC = () => {
    const {
        activeContentItems,
        activeContentItemId,
        setActiveContentItem,
        openContentViewModal,
        isContentItemSelected,
        selectContentItem,
        unselectContentItem,
        isAllContentItemsSelected,
        isSomeContentItemsSelected,
        selectUnselectAllContentItems,
        openDeleteContentItemModal,
        isSquareScreenOrientation,
        isHorizontalScreenOrientation,
        openUploadContentModal,
        isUploadingContent,
        t,
    } = useContentItemsList();

    return (
        <div className={css.mainContentBlock}>
            <div className={css.contentSelectAll}>
                <h2>{t('content')}</h2>
                {activeContentItems.length > 0 && (
                    <Tooltip title={t('tooltipSelectAll')} placement="top">
                        <SelectButton active={isAllContentItemsSelected} onClick={selectUnselectAllContentItems} />
                    </Tooltip>
                )}
            </div>
            {activeContentItems.length === 0 ? (
                <div className={css.contentBlockEmpty}>
                    <p>
                        {t('theresNothingHereYet')} <br />
                        <a
                            className={clsx(isUploadingContent && css.disabled)}
                            onClick={isUploadingContent ? undefined : openUploadContentModal}
                        >
                            {t('uploadContent')}
                        </a>
                    </p>
                </div>
            ) : (
                <div className={css.wrapperContentBlockList}>
                    <div className={css.mainContentBlockList}>
                        {activeContentItems.map((content) => (
                            <div
                                className={css.wrapContentCardItem}
                                key={content?.id}
                                onClick={(e) => e.stopPropagation()}
                            >
                                <div className={css.wrapContentCardItem1} onClick={setActiveContentItem(content?.id)}>
                                    <div
                                        className={clsx(css.contentCardItem, {
                                            [css.selectedSome]: isSomeContentItemsSelected,
                                            [css.selectedItem]: isContentItemSelected(content?.id),
                                            [css.active]:
                                                content?.id === activeContentItemId ||
                                                isContentItemSelected(content?.id),
                                        })}
                                        onDoubleClick={openContentViewModal(content?.id)}
                                    >
                                        <a
                                            className={css.overlayTrash}
                                            onClick={(event) => {
                                                event.stopPropagation();
                                                openDeleteContentItemModal(content?.id);
                                            }}
                                        />
                                        <SelectButton
                                            active={isContentItemSelected(content?.id)}
                                            className={clsx(
                                                css.overlayCheckCircle,
                                                isContentItemSelected(content?.id) && css.overlayCheckCircleActive,
                                            )}
                                            onClick={(event) => {
                                                event.stopPropagation();
                                                if (isContentItemSelected(content?.id)) {
                                                    unselectContentItem(content?.id);
                                                } else {
                                                    selectContentItem(content?.id);
                                                }
                                            }}
                                        />
                                        {!!content?.dimensions && (
                                            <>
                                                <a
                                                    className={clsx(css.overlayScreen, {
                                                        [css.horizontal]: isHorizontalScreenOrientation(
                                                            content?.dimensions,
                                                        ),
                                                        [css.square]: isSquareScreenOrientation(content?.dimensions),
                                                    })}
                                                />

                                                <a
                                                    className={css.overlayScreenResolution}
                                                >{`${content?.dimensions?.width}×${content?.dimensions?.height}`}</a>
                                            </>
                                        )}
                                        <div className={css.wrapContentCardItemImg}>
                                            {content?.type === CONTENT_TYPE.IMAGE ? (
                                                <ImgOrVideo src={content?.src} native={false} />
                                            ) : (
                                                <div className={css.wrapperVideoCart}>
                                                    <ImgOrVideo src={content?.src} native={false} />
                                                </div>
                                            )}
                                        </div>

                                        <div className={css.contentCardItemName}>
                                            <p>{content?.originFilename}</p>
                                        </div>
                                        {content?.type === CONTENT_TYPE.VIDEO && (
                                            <div className={css.contentVideoTime}>
                                                <span>{formatSecondsToTimeString(content?.duration ?? 0)}</span>
                                            </div>
                                        )}
                                        <div className={css.blackoutOverlay} />
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            )}
        </div>
    );
};

export default ContentItemsList;
