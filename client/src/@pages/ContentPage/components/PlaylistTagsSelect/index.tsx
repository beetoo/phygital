import React from 'react';

import { SelectWithTags } from '../../../../ui-kit';
import { usePlaylistTagsSelect } from '../../hooks/usePlaylistTagsSelect';

const PlaylistTagsSelect: React.FC = () => {
    const {
        isAllTagsSelected,
        playlistTitles,
        filteringPlaylistTags,
        onSelectPlaylistTag,
        onDeletePlaylistTag,
        isTagsSelectDisabled,
    } = usePlaylistTagsSelect();

    return isTagsSelectDisabled ? null : (
        <div style={{ width: '292px' }}>
            <SelectWithTags
                isAllTagsSelected={isAllTagsSelected}
                tags={playlistTitles}
                filteringTags={filteringPlaylistTags}
                onSelectTags={onSelectPlaylistTag}
                onDeleteTag={onDeletePlaylistTag}
                isTagsSelectDisabled={isTagsSelectDisabled}
            />
        </div>
    );
};

export default PlaylistTagsSelect;
