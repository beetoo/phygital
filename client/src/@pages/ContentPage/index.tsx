import React from 'react';
import { clsx } from 'clsx';

import FoldersList from './components/FoldersList';
import WebContentList from './components/WebContentList';
import ContentItemsList from './components/ContentItemsList';
import WithoutFoldersMenu from './components/WithoutFoldersMenu';
import SingleFolderMenu from './components/SingleFolderMenu';
import SingleContentItemMenu from './components/SingleContentItemMenu';
import MultiplyContentItemsMenu from './components/MultiplyContentItemsMenu';
import { useContentPage } from './hooks/useContentPage';

import css from './style.m.css';

const ContentPage: React.FC = () => {
    const {
        isWebContentMode,
        isWithoutFoldersMenu,
        isSingleFolderMenu,
        isSingleContentItemMenu,
        isMultiplyContentItemsMenu,

        openAddContentModal,
        isUploadingContent,

        t,
    } = useContentPage();

    return (
        <div className={css.mainContent}>
            <div className={css.centerBlockContent}>
                <div className={css.mainBlockContent}>
                    <button
                        type="button"
                        onClick={openAddContentModal}
                        className={css.btnDownloadContent}
                        disabled={isUploadingContent}
                    >
                        <img className={clsx(isWebContentMode && css.webContent)} alt="" />
                        {t(isWebContentMode ? 'addWebContent' : 'btnUploadContent')}
                    </button>
                </div>

                {isWebContentMode ? (
                    <WebContentList />
                ) : (
                    <div className={css.wrapperContentAll}>
                        <FoldersList />
                        <ContentItemsList />
                    </div>
                )}
            </div>
            <div className={css.rightBlockPageContentContainer}>
                {isWithoutFoldersMenu && <WithoutFoldersMenu />}
                {isSingleFolderMenu && <SingleFolderMenu />}
                {isSingleContentItemMenu && <SingleContentItemMenu />}
                {isMultiplyContentItemsMenu && <MultiplyContentItemsMenu />}
            </div>
        </div>
    );
};

export default ContentPage;
