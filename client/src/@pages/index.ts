import { combineReducers } from 'redux';
import { combineEpics } from 'redux-observable';

import modalsReducer from '../components/ModalsContainer/reducer';

import { StoreType } from '../types';
import adminPageReducer from './AdminPage/reducers';
import mainPageReducer from './MainPage/reducers';
import screensPageReducer from './ScreensPage/reducers';
import playlistsPageReducer from './PlaylistsPage/reducers';
import profilePageReducer from './ProfilePage/reducers';
import foldersPageReducer from './ContentPage/reducers';
import schedulePageReducer from './DxSchedulePage/reducers';

import { signInEpic } from './SignIn/epics';
import { signUpEpics } from './SignUp/epics';
import { resetPasswordEpics } from './RecoveryPassword/epics';
import { organizationsEpics } from './AdminPage/epics';
import { screensEpics } from './ScreensPage/epics';
import { playlistsEpics } from './PlaylistsPage/epics';
import { profileEpics } from './ProfilePage/epics';
import { foldersEpics } from './ContentPage/epics';
import { schedulesEpics } from './DxSchedulePage/epics';

export const pagesEpic = combineEpics<any, any, StoreType | void>(
    signInEpic,
    signUpEpics,
    resetPasswordEpics,
    organizationsEpics,
    screensEpics,
    playlistsEpics,
    profileEpics,
    foldersEpics,
    schedulesEpics,
);

const singleOrganizationReducers = combineReducers({
    modals: modalsReducer,
    screens: screensPageReducer,
    playlists: playlistsPageReducer,
    profile: profilePageReducer,
    folders: foldersPageReducer,
    schedule: schedulePageReducer,
});

export const pagesReducers = {
    main: mainPageReducer,
    admin: adminPageReducer,
    organization: singleOrganizationReducers,
};
