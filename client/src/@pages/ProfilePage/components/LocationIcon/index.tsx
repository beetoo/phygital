import React from 'react';

export const LocationIcon: React.FC = () => (
    <svg width="48" height="37" viewBox="0 0 48 37" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clipPath="url(#clip0_15309_65481)">
            <rect width="48" height="37" rx="2" fill="#D0D1DA" fillOpacity="0.2" />
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M6.47775 40.8542L6.69069 40.4854L-5 37.3528V34.0437L8.34524 37.6196L12.7124 30.0555L-2.29771 26.0335L-5 30.714V27.5177L-3.89589 25.6053L-5 25.3095V23.6549L-3.06861 24.1724L0.978664 17.1623L-5 15.5604V12.2513L2.63321 14.2966L13.1125 -3.85416H14.958L11.7629 1.67989L26.773 5.70183L32.2901 -3.85416H35.0582L29.1702 6.34417L42.0916 9.80643L48.8191 -1.84594L41.3243 -3.85416H47.4992L49.6463 -3.27882L49.9785 -3.85412C50.8478 -3.85359 51.3563 -3.84505 51.7398 -3.70839L51.2445 -2.85059L52.9093 -2.40452C52.9974 -2.02579 52.9999 -1.52055 53 -0.725662L50.4172 -1.41771L43.6897 10.2347L53 12.7293V14.3839L42.8625 11.6675L36.1583 23.2796L53 27.7923V31.2904L49.0472 38.1368L52.9496 39.1825C52.9179 39.4119 52.8662 39.5969 52.782 39.7622C52.6069 40.1058 52.3365 40.3908 52.0043 40.5837L48.2199 39.5697L47.4784 40.8542H45.6329L46.6218 39.1415L29.6292 34.5883L26.0116 40.8542H24.1661L28.031 34.1601L15.1096 30.6978L10.7425 38.2619L20.4169 40.8542H6.47775ZM4.23139 14.7248L10.9356 3.11277L25.9457 7.13472L19.2415 18.7467L4.23139 14.7248ZM-1.47043 24.6007L2.57684 17.5906L17.5869 21.6125L13.5397 28.6226L-1.47043 24.6007ZM51.4963 30.6985L34.5037 26.1454L30.4564 33.1554L47.449 37.7086L51.4963 30.6985ZM32.9055 25.7171L28.8583 32.7272L15.9369 29.2649L19.9842 22.2549L32.9055 25.7171ZM41.2643 11.2393L34.5601 22.8514L21.6387 19.3891L28.343 7.77706L41.2643 11.2393Z"
                fill="url(#paint0_linear_15309_65481)"
                fillOpacity="0.5"
            />
            <g filter="url(#filter0_f_15309_65481)">
                <ellipse cx="24" cy="26" rx="8" ry="4" fill="#7D7DAB" fillOpacity="0.42" />
            </g>
            <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M32 14C32 15.5091 32 16.6977 30.6697 18.8695L24.1645 28.2738C24.085 28.3888 23.915 28.3888 23.8355 28.2738L17.3303 18.8695C16 16.6977 16 15.5091 16 14C16 9.58176 19.5818 6 24 6C28.4182 6 32 9.58176 32 14ZM24.0004 17.3487C25.8499 17.3487 27.3492 15.8494 27.3492 13.9999C27.3492 12.1504 25.8499 10.6511 24.0004 10.6511C22.1509 10.6511 20.6516 12.1504 20.6516 13.9999C20.6516 15.8494 22.1509 17.3487 24.0004 17.3487Z"
                fill="url(#paint1_linear_15309_65481)"
            />
        </g>
        <defs>
            <filter
                id="filter0_f_15309_65481"
                x="11"
                y="17"
                width="26"
                height="18"
                filterUnits="userSpaceOnUse"
                colorInterpolationFilters="sRGB"
            >
                <feFlood floodOpacity="0" result="BackgroundImageFix" />
                <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
                <feGaussianBlur stdDeviation="2.5" result="effect1_foregroundBlur_15309_65481" />
            </filter>
            <linearGradient
                id="paint0_linear_15309_65481"
                x1="24"
                y1="56.9987"
                x2="50"
                y2="-4.00125"
                gradientUnits="userSpaceOnUse"
            >
                <stop offset="0.00328437" stopColor="#9093AE" stopOpacity="0.42" />
                <stop offset="1" stopColor="#A0A2BA" stopOpacity="0" />
            </linearGradient>
            <linearGradient
                id="paint1_linear_15309_65481"
                x1="26.5"
                y1="28"
                x2="14.5"
                y2="-14"
                gradientUnits="userSpaceOnUse"
            >
                <stop stopColor="#3B51F9" />
                <stop offset="1" stopColor="#3B51F9" stopOpacity="0" />
            </linearGradient>
            <clipPath id="clip0_15309_65481">
                <rect width="48" height="37" rx="2" fill="white" />
            </clipPath>
        </defs>
    </svg>
);
