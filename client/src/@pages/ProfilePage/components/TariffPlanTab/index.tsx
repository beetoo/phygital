import React from 'react';
import { addDays, differenceInCalendarDays, format } from 'date-fns';
import { clsx } from 'clsx';

import { screensTariffsSelectList, storageTariffsSelectList, useTariffPlanTab } from '../../hooks/useTariffPlanTab';
import { MenuItem, Select } from '../../../../ui-kit';
import { LicenceTypeEnum } from '../../../../../../common/types';

import css from '../../style.m.css';

const TariffPlanTab: React.FC = () => {
    const {
        organizationInfo: { limitScreens, licenses, limitStorageSize, usedStorageSize },
        billingAvailable,
        screensNumber,
        screensTariffSelectValue,
        storageTariffSelectValue,
        onScreensTariffSelect,
        onStorageTariffSelect,
        openScreensTariffRobokassaWindow,
        openExtendTariffRobokassaWindow,
        openStorageTariffRobokassaWindow,
        onChangeScreensNumber,
        openBalanceRequestFormModal,
        toCapacityInfo,
        t,
    } = useTariffPlanTab();

    return (
        <div className={css.wrapBlockTariffPlan}>
            <div className={css.containerTariff}>
                <div className={css.containerTariffItem}>
                    <div className={css.tariffItemTitle}>
                        <h3>{t('screens')}</h3>
                    </div>
                    {limitScreens > 0 && (
                        <div className={css.wrapTariffPlanTotalInfo}>
                            <div className={css.fieldTariffPlanTotalScreens}>
                                <div className={css.wrapperColumn}>
                                    <p className={css.tariffPlanTotalInfoItem}>{t('addedScreens')}</p>
                                    <p className={css.tariffPlanTotalInfoItem}>{limitScreens}</p>
                                </div>
                            </div>
                        </div>
                    )}
                    {licenses
                        .filter((license) => license.type === null || license.type === LicenceTypeEnum.basic_screens)
                        .map(({ id, screensNumber, startedAt, endedAt, type }) => (
                            <div
                                key={id}
                                className={clsx(
                                    css.wrapTariffPlanTotalInfo,
                                    differenceInCalendarDays(addDays(new Date(endedAt), 1), new Date()) <= 10 &&
                                        css.licenseExpired,
                                )}
                            >
                                <div className={css.fieldTariffPlanTotalScreens}>
                                    <div className={css.wrapperColumn}>
                                        <p className={css.tariffPlanTotalInfoItem}>{t('addedScreens')}</p>
                                        <p className={css.tariffPlanTotalInfoItem}>{screensNumber}</p>
                                    </div>
                                    <div className={css.wrapperColumn} />
                                    <div className={css.wrapperColumn}>
                                        <p className={css.tariffPlanTotalInfoItem}>{t('period')}</p>
                                        <p className={css.tariffPlanTotalInfoItem}>
                                            {format(new Date(startedAt), 'dd.MM.yyyy')} -{' '}
                                            {format(new Date(endedAt), 'dd.MM.yyyy')}
                                        </p>
                                    </div>
                                    <div className={css.fieldAddBalance}>
                                        <button
                                            className={css.addBalance}
                                            onClick={
                                                billingAvailable
                                                    ? openExtendTariffRobokassaWindow(id, type)
                                                    : openBalanceRequestFormModal('license_renewal')
                                            }
                                        >
                                            {t('extend')}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        ))}

                    <div className={css.wrapTariffPlanTotalInfo}>
                        <div className={css.fieldTariffPlanTotalScreens}>
                            <div className={css.wrapperColumn}>
                                <p className={css.tariffPlanTotalInfoItem}>{t('addedScreens')}</p>
                                <p className={css.tariffPlanTotalInfoItem}>0</p>
                            </div>
                            <div className={css.wrapperColumn} />
                            <div className={css.wrapperColumn}>
                                {billingAvailable ? (
                                    <Select
                                        style={{
                                            width: '220px',
                                            borderRadius: '6px',
                                            background: '#fff',
                                        }}
                                        placeholder={t('selectTariff')}
                                        onChange={onScreensTariffSelect}
                                        value={screensTariffSelectValue}
                                    >
                                        {screensTariffsSelectList.map((info, idx) => (
                                            <MenuItem key={idx} value={info}>
                                                {info}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                ) : (
                                    <>
                                        <p className={css.tariffPlanTotalInfoItem}>{t('numberOfScreens')}</p>
                                        <div>
                                            <input
                                                className={css.inputTariffPlan}
                                                value={screensNumber}
                                                onChange={onChangeScreensNumber}
                                                maxLength={125}
                                            />
                                        </div>
                                    </>
                                )}
                            </div>
                            {(billingAvailable ? !!screensTariffSelectValue : true) && (
                                <div className={css.fieldAddBalance}>
                                    <button
                                        className={css.addBalance}
                                        onClick={
                                            billingAvailable
                                                ? openScreensTariffRobokassaWindow
                                                : openBalanceRequestFormModal('license_creation')
                                        }
                                    >
                                        {t(billingAvailable ? 'pay' : 'request')}
                                    </button>
                                </div>
                            )}
                        </div>
                    </div>

                    <div className={css.fieldNote}>
                        <p>{t('licencesEqual')}</p>
                    </div>
                </div>
            </div>

            <div className={css.containerTariff}>
                <div className={css.containerTariffItem}>
                    <div className={css.tariffItemTitle}>
                        <h3>{t('space')}</h3>
                    </div>
                    {licenses
                        .filter((license) => license.type === LicenceTypeEnum.basic_storage)
                        .map(({ id, capacity, startedAt, endedAt, type }) => (
                            <div key={id} className={css.wrapTariffPlanTotalInfo}>
                                <div className={css.fieldTariffPlanTotalVolume}>
                                    <div className={css.wrapperColumn}>
                                        <p className={css.tariffPlanTotalInfoItem}>{t('amountOfContent')}</p>
                                        <p className={css.tariffPlanTotalInfoItem}>{toCapacityInfo(capacity)}</p>
                                    </div>
                                    <div className={css.wrapperColumn}></div>

                                    <div className={css.wrapperColumn}>
                                        <p className={css.tariffPlanTotalInfoItem}>{t('period')}</p>
                                        <p className={css.tariffPlanTotalInfoItem}>
                                            {format(new Date(startedAt), 'dd.MM.yyyy')} -{' '}
                                            {format(new Date(endedAt), 'dd.MM.yyyy')}
                                        </p>
                                    </div>
                                    <div className={css.fieldAddBalance}>
                                        <button
                                            className={css.addBalance}
                                            onClick={
                                                billingAvailable
                                                    ? openExtendTariffRobokassaWindow(id, type)
                                                    : openBalanceRequestFormModal('license_renewal')
                                            }
                                        >
                                            {t('extend')}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        ))}
                    <div className={css.wrapTariffPlanTotalInfo}>
                        <div className={css.fieldTariffPlanTotalVolume}>
                            <div className={css.wrapperColumn}>
                                <p className={css.tariffPlanTotalInfoItem}>{t('amountOfContent')}</p>
                                <p className={css.tariffPlanTotalInfoItem}>
                                    {toCapacityInfo(usedStorageSize)}/ {toCapacityInfo(limitStorageSize)}
                                </p>
                            </div>
                            <div className={css.wrapperColumn}></div>
                            <div className={css.wrapperColumn}>
                                {billingAvailable && (
                                    <div className={css.wrapperColumn}>
                                        <Select
                                            placeholder={t('selectCapacity')}
                                            onChange={onStorageTariffSelect}
                                            value={storageTariffSelectValue}
                                            style={{
                                                width: '220px',
                                                borderRadius: '6px',
                                                background: '#fff',
                                            }}
                                        >
                                            {storageTariffsSelectList.map((info, idx) => (
                                                <MenuItem key={idx} value={info}>
                                                    {info}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </div>
                                )}
                            </div>
                            {(billingAvailable ? !!storageTariffSelectValue : true) && (
                                <div className={css.fieldAddBalance}>
                                    <button
                                        className={css.addBalance}
                                        onClick={
                                            billingAvailable
                                                ? openStorageTariffRobokassaWindow
                                                : openBalanceRequestFormModal('storage_increase')
                                        }
                                    >
                                        {t('increaseSpace')}
                                    </button>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default TariffPlanTab;
