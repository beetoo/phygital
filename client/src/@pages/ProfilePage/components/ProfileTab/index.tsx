import React from 'react';
import { clsx } from 'clsx';

import { Tooltip, Select, MenuItem } from '../../../../ui-kit';
import PhoneInputCustom from '../../../../ui-kit/PhoneInputCustom';
import { useProfileTab } from '../../hooks/useProfileTab';
import { isProductionEngClient } from '../../../../constants/environments';

import css from '../../style.m.css';

const ProfileTab: React.FC = () => {
    const {
        title,
        name,
        surname,
        email,
        phone,
        oldPassword,
        newPassword,
        confirmingPassword,
        onTitleChange,
        onNameChange,
        onSurnameChange,
        onPhoneChange,
        onEmailChange,
        onOldPasswordChange,
        onNewPasswordChange,
        onConfirmingPasswordChange,
        titleInputError,
        nameInputError,
        surnameInputError,
        phoneInputError,
        emailInputError,
        resetInputs,
        updateProfile,
        isSaveButtonDisabled,
        isCancelButtonDisabled,
        oldPasswordInputError,
        matchPasswordsError,
        isOldPasswordShowed,
        isNewPasswordShowed,
        isConfirmPasswordShowed,
        showHideOldPassword,
        showHideNewPassword,
        showHideConfirmPassword,

        language,
        languages,
        onLanguageSelect,
        t,
    } = useProfileTab();

    return (
        <div className={css.containerBlockFormProfile}>
            <form className={css.blockFormProfile} onSubmit={(e) => e.preventDefault()}>
                <div>
                    <h2>{t('userProfile')}</h2>
                    <div className={css.wrapProfileName}>
                        <label>{t('name')}</label>
                        <input
                            className={clsx(
                                css.profileInput,
                                nameInputError && [css.profileInputError, css.iconWarning],
                            )}
                            type="text"
                            placeholder={t('name') as string}
                            value={name}
                            onChange={onNameChange}
                            maxLength={125}
                        />
                        {nameInputError && <div className={css.errorField}>{nameInputError}</div>}
                    </div>
                    <div className={css.wrapProfileName}>
                        <label>{t('surname')}</label>
                        <input
                            className={clsx(
                                css.profileInput,
                                surnameInputError && [css.profileInputError, css.iconWarning],
                            )}
                            type="text"
                            placeholder={t('surname') as string}
                            value={surname}
                            onChange={onSurnameChange}
                            maxLength={125}
                        />
                        {surnameInputError && <div className={css.errorField}>{surnameInputError}</div>}
                    </div>
                    <div className={css.wrapProfileName}>
                        <label>{t('companyName')}</label>
                        <input
                            className={clsx(
                                css.profileInput,
                                titleInputError && [css.profileInputError, css.iconWarning],
                            )}
                            type="text"
                            placeholder={t('companyName') as string}
                            value={title}
                            onChange={onTitleChange}
                            maxLength={125}
                        />
                        {titleInputError && <div className={css.errorField}>{titleInputError}</div>}
                    </div>
                    <div className={css.wrapProfileNumber}>
                        <label>{t('phoneNumber')}</label>
                        <PhoneInputCustom
                            style={{ width: '360px' }}
                            {...{ phone, onPhoneInputChange: onPhoneChange, phoneInputError }}
                        />
                        {phoneInputError && <div className={css.errorField}>{phoneInputError}</div>}
                    </div>
                    <div className={css.wrapProfileEmail}>
                        <label>{t('email')}</label>
                        <input
                            className={clsx(
                                css.profileInput,
                                emailInputError && [css.profileInputError, css.iconWarning],
                            )}
                            type="text"
                            placeholder={t('email') as string}
                            value={email}
                            onChange={onEmailChange}
                            maxLength={125}
                            autoComplete="off"
                        />
                        {emailInputError && <div className={css.errorField}>{emailInputError}</div>}
                    </div>
                    <h3>{t('changePassword')}</h3>
                    <div className={css.wrapProfileOldPassword}>
                        <label>{t('oldPassword')}</label>
                        <div className={css.tooltip}>
                            <div className={css.fieldPassword}>
                                <input
                                    className={clsx(css.profileInput, oldPasswordInputError && css.profileInputError)}
                                    type={isOldPasswordShowed ? 'text' : 'password'}
                                    placeholder={t('enterPassword') as string}
                                    value={oldPassword}
                                    onChange={onOldPasswordChange}
                                    autoComplete="new-password"
                                    maxLength={125}
                                />
                                <Tooltip
                                    title={isOldPasswordShowed ? `${t('hidePassword')}` : `${t('viewPassword')}`}
                                    placement="right"
                                >
                                    <span
                                        onClick={showHideOldPassword}
                                        className={clsx(
                                            css.iconEye,
                                            isOldPasswordShowed ? css.iconEyeOpened : css.iconEyeClosed,
                                        )}
                                    />
                                </Tooltip>
                                {oldPasswordInputError && <div className={css.errorField}>{oldPasswordInputError}</div>}
                            </div>
                        </div>
                    </div>
                    <div className={css.wrapProfileNewPassword}>
                        <label>{t('newPassword')}</label>
                        <div className={css.tooltip}>
                            <input
                                className={css.profileInput}
                                type={isNewPasswordShowed ? 'text' : 'password'}
                                placeholder={t('enterNewPassword') as string}
                                value={newPassword}
                                onChange={onNewPasswordChange}
                                maxLength={125}
                                autoComplete="off"
                            />
                            <Tooltip
                                title={isNewPasswordShowed ? `${t('hidePassword')}` : `${t('viewPassword')}`}
                                placement="right"
                            >
                                <span
                                    onClick={showHideNewPassword}
                                    className={clsx(
                                        css.iconEye,
                                        isNewPasswordShowed ? css.iconEyeOpened : css.iconEyeClosed,
                                    )}
                                />
                            </Tooltip>
                        </div>
                    </div>
                    <div className={css.wrapProfileRepeatNewPassword}>
                        <label>{t('repeatNewPassword')}</label>
                        <div className={css.tooltip}>
                            <div className={css.fieldPassword}>
                                <input
                                    className={clsx(css.profileInput, matchPasswordsError && css.profileInputError)}
                                    type={isConfirmPasswordShowed ? 'text' : 'password'}
                                    placeholder={t('enterNewPasswordAgain') as string}
                                    value={confirmingPassword}
                                    onChange={onConfirmingPasswordChange}
                                    maxLength={125}
                                    autoComplete="off"
                                />
                                <Tooltip
                                    title={isConfirmPasswordShowed ? `${t('hidePassword')}` : `${t('viewPassword')}`}
                                    placement="right"
                                >
                                    <span
                                        onClick={showHideConfirmPassword}
                                        className={clsx(
                                            css.iconEye,
                                            isConfirmPasswordShowed ? css.iconEyeOpened : css.iconEyeClosed,
                                        )}
                                    />
                                </Tooltip>
                                {matchPasswordsError && <div className={css.errorPassword}>{matchPasswordsError}</div>}
                            </div>
                        </div>
                    </div>
                    {!isProductionEngClient && (
                        <>
                            <h3>{t('changeLanguage')}</h3>
                            <div>
                                <label>{t('serviceLanguage')}</label>
                                <Select
                                    style={{ marginTop: '0' }}
                                    sx={{
                                        width: '360px',
                                    }}
                                    value={language}
                                    onChange={onLanguageSelect}
                                >
                                    {languages.map((lng) => (
                                        <MenuItem key={lng} value={lng}>
                                            {lng}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </div>
                        </>
                    )}
                </div>
                <div>
                    <button className={css.buttonCancel} onClick={resetInputs} disabled={isCancelButtonDisabled}>
                        {t('btnCancel')}
                    </button>
                    <button
                        type="submit"
                        className={css.buttonSave}
                        onClick={updateProfile}
                        disabled={isSaveButtonDisabled}
                    >
                        {t('btnSave')}
                    </button>
                </div>
            </form>
        </div>
    );
};

export default ProfileTab;
