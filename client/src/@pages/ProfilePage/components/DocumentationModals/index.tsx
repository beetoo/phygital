import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { MODALS } from '../../../../components/ModalsContainer/types';
import { selectCurrentModal } from '../../../../components/ModalsContainer/selectors';
import { setHideModal } from '../../../../components/ModalsContainer/actions';
import {
    PublicOfferDocumentation,
    PrivacyPolicyDocumentation,
    DocumentationMitLicense,
    DocumentationCopyright,
} from './components';

const DocumentationModals: React.FC = () => {
    const dispatch = useDispatch();

    const currentModal = useSelector(selectCurrentModal);

    const closeModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    return (
        <>
            {currentModal === MODALS.PUBLIC_OFFER_DOCUMENTATION && <PublicOfferDocumentation closeModal={closeModal} />}
            {currentModal === MODALS.PRIVACY_POLICY_DOCUMENTATION && (
                <PrivacyPolicyDocumentation closeModal={closeModal} />
            )}
            {currentModal === MODALS.DOCUMENTATION_MIT_LICENSE && <DocumentationMitLicense closeModal={closeModal} />}
            {currentModal === MODALS.DOCUMENTATION_COPYRIGHT && <DocumentationCopyright closeModal={closeModal} />}
        </>
    );
};

export default DocumentationModals;
