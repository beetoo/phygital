import React from 'react';
import { clsx } from 'clsx';

import { useDocumentationTab } from '../../hooks/useDocumentationTab';
import { MODALS } from '../../../../components/ModalsContainer/types';

import css from '../../style.m.css';

const DocumentationTab: React.FC = () => {
    const { activeDocumentation, selectDocumentation, t } = useDocumentationTab();

    return (
        <div className={css.blockDocProfile}>
            <div className={css.wrapBlockDoc}>
                <h2>{t('documentation')}</h2>
                <ul className={css.blockDocList}>
                    <li
                        className={clsx(
                            activeDocumentation === MODALS.PUBLIC_OFFER_DOCUMENTATION && css.blockDocItemCurrent,
                        )}
                    >
                        <p>{t('publicOffer')}</p>
                        <a onClick={selectDocumentation(MODALS.PUBLIC_OFFER_DOCUMENTATION)}>{t('read')}</a>
                    </li>
                    <li
                        className={clsx(
                            activeDocumentation === MODALS.PRIVACY_POLICY_DOCUMENTATION && css.blockDocItemCurrent,
                        )}
                    >
                        <p>{t('personalDataProcessing')}</p>
                        <a onClick={selectDocumentation(MODALS.PRIVACY_POLICY_DOCUMENTATION)}>{t('read')}</a>
                    </li>
                    <li
                        className={clsx(
                            activeDocumentation === MODALS.DOCUMENTATION_MIT_LICENSE && css.blockDocItemCurrent,
                        )}
                    >
                        <p>MIT License. Copyright (c) 2020 Phosphor Icons</p>
                        <a onClick={selectDocumentation(MODALS.DOCUMENTATION_MIT_LICENSE)}>{t('read')}</a>
                    </li>
                    <li
                        className={clsx(
                            activeDocumentation === MODALS.DOCUMENTATION_COPYRIGHT && css.blockDocItemCurrent,
                        )}
                    >
                        <p>СС BY 4.0. Copyright (c) Free Emoji Pack by Paca</p>
                        <a onClick={selectDocumentation(MODALS.DOCUMENTATION_COPYRIGHT)}>{t('read')}</a>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default DocumentationTab;
