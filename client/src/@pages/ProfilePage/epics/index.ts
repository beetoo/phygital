import { of, Observable } from 'rxjs';
import { combineEpics, ofType, StateObservable } from 'redux-observable';
import { mergeMap, map, catchError, withLatestFrom } from 'rxjs/operators';

import {
    UPDATE_ORGANIZATION,
    setUpdateOrganizationSuccess,
    setUpdateOrganizationFail,
    UPDATE_ORGANIZATION_SUCCESS,
    GET_ORGANIZATION,
    setGetOrganizationSuccess,
    setGetOrganization,
    setGetOrganizationFail,
} from '../actions';

import {
    GetOrganizationAction,
    GetOrganizationFailAction,
    GetOrganizationSuccessAction,
    UpdateOrganizationAction,
    UpdateOrganizationFailAction,
    UpdateOrganizationSuccessAction,
} from '../types';
import { fromGetOrganization, fromUpdateOrganization } from '../../../resolvers/organizationsResolvers';

import { StoreType } from '../../../types';

const getOrganizationEpic = (
    action$: Observable<GetOrganizationAction>,
): Observable<GetOrganizationSuccessAction | GetOrganizationFailAction> =>
    action$.pipe(
        ofType(GET_ORGANIZATION),
        mergeMap(({ payload: { organizationId } }) =>
            fromGetOrganization(organizationId).pipe(
                map(setGetOrganizationSuccess),
                catchError((error) => of(setGetOrganizationFail(error))),
            ),
        ),
    );

const updateOrganizationEpic = (
    action$: Observable<UpdateOrganizationAction>,
): Observable<UpdateOrganizationSuccessAction | UpdateOrganizationFailAction> =>
    action$.pipe(
        ofType(UPDATE_ORGANIZATION),
        mergeMap(({ payload: updateProfileDto }) =>
            fromUpdateOrganization(updateProfileDto).pipe(
                map(setUpdateOrganizationSuccess),
                catchError((error) => of(setUpdateOrganizationFail(error))),
            ),
        ),
    );

const profileSuccessEpic = (
    action$: Observable<UpdateOrganizationSuccessAction>,
    state$: StateObservable<StoreType>,
): Observable<GetOrganizationAction> =>
    action$.pipe(
        ofType(UPDATE_ORGANIZATION_SUCCESS),
        withLatestFrom(state$),
        map(
            ([
                ,
                {
                    pages: {
                        organization: {
                            profile: {
                                organization: { id },
                            },
                        },
                    },
                },
            ]) => setGetOrganization(id),
        ),
    );

type EpicsActions =
    | GetOrganizationAction
    | GetOrganizationSuccessAction
    | GetOrganizationFailAction
    | UpdateOrganizationAction
    | UpdateOrganizationSuccessAction
    | UpdateOrganizationFailAction;

export const profileEpics = combineEpics<EpicsActions, EpicsActions, StoreType>(
    getOrganizationEpic,
    updateOrganizationEpic,
    profileSuccessEpic,
);
