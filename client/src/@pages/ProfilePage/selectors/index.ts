import { StoreType } from '../../../types';
import { ProfileTabType } from '../types';
import { ClientOrganizationType } from '../../../../../common/types';

// текущая организация
export const selectCurrentOrganization = ({
    pages: {
        organization: {
            profile: { organization },
        },
    },
}: StoreType): ClientOrganizationType => organization;

// update organization
export const selectUpdatingOrganizationStatus = ({
    pages: {
        organization: {
            profile: { updatingOrganization },
        },
    },
}: StoreType): boolean => updatingOrganization;

export const selectUpdateOrganizationSuccessStatus = ({
    pages: {
        organization: {
            profile: { isUpdateOrganizationSuccess },
        },
    },
}: StoreType): boolean => isUpdateOrganizationSuccess;

export const selectUpdateOrganizationError = ({
    pages: {
        organization: {
            profile: { updateOrganizationError },
        },
    },
}: StoreType): string | any => (updateOrganizationError ? updateOrganizationError.message : '');

export const selectProfileTab = ({
    pages: {
        organization: {
            profile: { profileTab },
        },
    },
}: StoreType): ProfileTabType => profileTab;

export const selectScreensNumberForTariffPlan = ({
    pages: {
        organization: {
            profile: { screensNumberForTariffPlan },
        },
    },
}: StoreType): number => screensNumberForTariffPlan;
