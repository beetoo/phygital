import {
    CLEAR_PROFILE_PAGE_ERRORS,
    GET_ORGANIZATION,
    GET_ORGANIZATION_FAIL,
    GET_ORGANIZATION_SUCCESS,
    SET_PROFILE_TAB,
    SET_SCREENS_NUMBER_FOR_TARIFF_PLAN,
    UPDATE_ORGANIZATION,
    UPDATE_ORGANIZATION_FAIL,
    UPDATE_ORGANIZATION_SUCCESS,
} from '../actions';

import { ProfileAction, ProfilePageState } from '../types';

const profilePageInitialState: ProfilePageState = {
    loadingOrganization: false,
    organization: {
        id: '',
        userId: '',
        title: '',
        name: '',
        surname: '',
        email: '',
        phone: '',
        createdAt: new Date(),
        role: 'user',
        status: 'active',
        licenses: [],
        maxScreens: 0,
        limitScreens: 0,
        limitStorageSize: 0,
        hasActiveSubscriptions: true,
        signageStorageSize: 500,
        licenseScreensNumber: 0,
        maxScreensCount: 0,
        billingAvailable: false,
        language: 'ru',
        licenseCapacityAmount: 0,
        locations: [],
    },
    loadOrganizationError: null,

    updatingOrganization: false,
    isUpdateOrganizationSuccess: false,
    updateOrganizationError: null,

    profileTab: 'profile',
    screensNumberForTariffPlan: 0,
};

export default function profilePageReducer(state = profilePageInitialState, action: ProfileAction): ProfilePageState {
    switch (action.type) {
        case GET_ORGANIZATION:
            return {
                ...state,
                loadingOrganization: true,
                loadOrganizationError: null,
            };
        case GET_ORGANIZATION_SUCCESS:
            return {
                ...state,
                loadingOrganization: false,
                organization: action.payload.organization,
            };
        case GET_ORGANIZATION_FAIL:
            return {
                ...state,
                loadingOrganization: false,
                loadOrganizationError: action.payload.errors,
            };

        case UPDATE_ORGANIZATION:
            return {
                ...state,
                updatingOrganization: true,
                isUpdateOrganizationSuccess: false,
                updateOrganizationError: null,
            };
        case UPDATE_ORGANIZATION_SUCCESS:
            return {
                ...state,
                isUpdateOrganizationSuccess: true,
                updatingOrganization: false,
            };
        case UPDATE_ORGANIZATION_FAIL:
            return {
                ...state,
                updatingOrganization: false,
                updateOrganizationError: action.payload.errors,
            };

        case SET_SCREENS_NUMBER_FOR_TARIFF_PLAN:
            return {
                ...state,
                screensNumberForTariffPlan: action.payload.screensNumber,
            };

        case SET_PROFILE_TAB:
            return {
                ...state,
                profileTab: action.payload.profileTab,
            };

        case CLEAR_PROFILE_PAGE_ERRORS:
            return {
                ...state,
                loadOrganizationError: null,
                updateOrganizationError: null,
            };

        default:
            return state;
    }
}
