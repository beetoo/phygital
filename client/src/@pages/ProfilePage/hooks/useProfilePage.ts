import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { ProfileTabType } from '../types';
import { setProfileTab } from '../actions';
import { selectProfileTab } from '../selectors';

type ReturnValue = {
    profileTab: ProfileTabType;
    chooseProfileTab: (profileTab: ProfileTabType) => () => void;
    t: TFunction;
};

export const useProfilePage = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'profileTab' });

    const dispatch = useDispatch();
    const profileTab = useSelector(selectProfileTab);

    const chooseProfileTab = useCallback(
        (profileTab: ProfileTabType) => () => {
            dispatch(setProfileTab(profileTab));
        },
        [dispatch],
    );

    useEffect(() => {
        const hash = window.location.hash.replace('#', '');

        if (['profile', 'tariff_plan', 'documentation'].includes(hash)) {
            dispatch(setProfileTab(hash as ProfileTabType));
        }
    }, [dispatch]);

    return {
        profileTab,
        chooseProfileTab,
        t,
    };
};
