import { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { MODALS } from '../../../components/ModalsContainer/types';
import { setShowModal } from '../../../components/ModalsContainer/actions';

type Documentation =
    | MODALS.PUBLIC_OFFER_DOCUMENTATION
    | MODALS.PRIVACY_POLICY_DOCUMENTATION
    | MODALS.DOCUMENTATION_MIT_LICENSE
    | MODALS.DOCUMENTATION_COPYRIGHT;

type ReturnValue = {
    activeDocumentation: Documentation;
    selectDocumentation: (documentation: Documentation) => () => void;
    t: TFunction;
};

export const useDocumentationTab = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'documentationTab' });

    const dispatch = useDispatch();

    const [activeDocumentation, setActiveDocumentation] = useState<Documentation>(MODALS.PRIVACY_POLICY_DOCUMENTATION);

    const selectDocumentation = useCallback(
        (documentation: Documentation) => () => {
            setActiveDocumentation(documentation);
            dispatch(setShowModal(documentation));
        },
        [dispatch],
    );

    return {
        activeDocumentation,
        selectDocumentation,
        t,
    };
};
