import { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { isPossiblePhoneNumber } from 'react-phone-number-input';
import isEmail from 'validator/es/lib/isEmail';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import {
    selectCurrentOrganization,
    selectUpdateOrganizationError,
    selectUpdateOrganizationSuccessStatus,
    selectUpdatingOrganizationStatus,
} from '../selectors';
import { setClearProfilePageErrors, setUpdateOrganization } from '../actions';
import { OrganizationLanguageType } from '../../../../../common/types';

type ProfileClientDto = {
    title: string;
    name: string;
    surname: string;
    email: string;
    phone: string;
};

type OldPasswordInputError = 'Неправильный пароль' | false;
type MatchPasswordsError = 'Пароли не совпадают' | false;

type LanguageSelectType = 'Russian' | 'Русский' | 'English' | 'Español';

const getLang = (lang: LanguageSelectType): OrganizationLanguageType => {
    switch (lang) {
        case 'Русский':
        case 'Russian':
            return 'ru';
        case 'English':
            return 'en';
        case 'Español':
            return 'es';
        default:
            return 'ru';
    }
};

type ReturnValue = {
    title: string;
    name: string;
    surname: string;
    phone: string;
    email: string;
    oldPassword: string;
    newPassword: string;
    confirmingPassword: string;
    onTitleChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onNameChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onSurnameChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onPhoneChange: (value: string) => void;
    onEmailChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onOldPasswordChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onNewPasswordChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onConfirmingPasswordChange: (e: ChangeEvent<HTMLInputElement>) => void;
    titleInputError: string | false;
    nameInputError: string | false;
    surnameInputError: string | false;
    phoneInputError: string | false;
    emailInputError: string | false;
    resetInputs: () => void;
    updateProfile: () => void;
    isSaveButtonDisabled: boolean;
    isCancelButtonDisabled: boolean;
    oldPasswordInputError: OldPasswordInputError;
    matchPasswordsError: MatchPasswordsError;
    isOldPasswordShowed: boolean;
    isNewPasswordShowed: boolean;
    isConfirmPasswordShowed: boolean;
    showHideOldPassword: () => void;
    showHideNewPassword: () => void;
    showHideConfirmPassword: () => void;

    language: LanguageSelectType;
    languages: LanguageSelectType[];
    onLanguageSelect: (event: SelectChangeEvent) => void;
    t: TFunction;
};

export const useProfileTab = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'profileTab' });

    const dispatch = useDispatch();

    const {
        id: organizationId,
        userId,
        title: organizationTitle,
        name: organizationName,
        surname: organizationSurname,
        phone: organizationPhone,
        email: organizationEmail,
        language: organizationLanguage,
        role: organizationRole,
    } = useSelector(selectCurrentOrganization);

    const updatingOrganization = useSelector(selectUpdatingOrganizationStatus);
    const isUpdateOrganizationSuccess = useSelector(selectUpdateOrganizationSuccessStatus);
    const updateOrganizationError = useSelector(selectUpdateOrganizationError);

    const [title, setTitle] = useState(organizationTitle);
    const [name, setName] = useState(organizationName);
    const [surname, setSurname] = useState(organizationSurname);
    const [phone, setPhone] = useState(organizationPhone);
    const [email, setEmail] = useState(organizationEmail);

    const [titleInputError, setTitleInputError] = useState<string | false>(false);
    const [nameInputError, setNameInputError] = useState<string | false>(false);
    const [surnameInputError, setSurnameInputError] = useState<string | false>(false);
    const [emailInputError, setEmailInputError] = useState<string | false>(false);
    const [phoneInputError, setPhoneInputError] = useState<string | false>(false);

    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmingPassword, setConfirmingPassword] = useState('');

    const [oldPasswordInputError, setOldPasswordInputError] = useState<OldPasswordInputError>(false);
    const [matchPasswordsError, setMatchPasswordsError] = useState<MatchPasswordsError>(false);

    const [isOldPasswordShowed, setIsOldPasswordShowed] = useState(false);
    const [isNewPasswordShowed, setIsNewPasswordShowed] = useState(false);
    const [isConfirmPasswordShowed, setIsConfirmPasswordShowed] = useState(false);

    const [language, setLanguage] = useState<LanguageSelectType>(
        organizationLanguage === 'ru' ? (t('russian') as LanguageSelectType) : 'English',
    );
    const [languages, setLanguages] = useState<LanguageSelectType[]>([t('russian'), 'English', 'Español']);

    const onLanguageSelect = useCallback(({ target: { value } }: SelectChangeEvent<LanguageSelectType>) => {
        setLanguage(value as LanguageSelectType);
    }, []);

    useEffect(() => {
        if (organizationId) {
            setTitle(organizationTitle);
            setName(organizationName);
            setSurname(organizationSurname);
            setPhone(organizationPhone);
            setEmail(organizationEmail);

            setLanguages([t('russian'), 'English', 'Español']);
            setLanguage(
                organizationLanguage === 'ru'
                    ? (t('russian') as LanguageSelectType)
                    : organizationLanguage === 'es'
                      ? 'Español'
                      : 'English',
            );
        }
    }, [
        organizationEmail,
        organizationId,
        organizationLanguage,
        organizationName,
        organizationPhone,
        organizationSurname,
        organizationTitle,
        t,
    ]);

    const onTitleChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setTitleInputError(false);
        setTitle(value);
    }, []);

    const onNameChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setNameInputError(false);
        setName(value);
    }, []);

    const onSurnameChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setSurnameInputError(false);
        setSurname(value);
    }, []);

    const onPhoneChange = useCallback((value: string) => {
        setPhoneInputError(false);
        setPhone(value ?? '');
    }, []);

    const onEmailChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setEmailInputError(false);
        setEmail(value);
    }, []);

    const onOldPasswordChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setOldPasswordInputError(false);
        setOldPassword(value);
    }, []);

    const onNewPasswordChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        value = value.split(' ').join('');

        setNewPassword(value);
    }, []);

    const onConfirmingPasswordChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setMatchPasswordsError(false);
        setConfirmingPassword(value);
    }, []);

    const resetInputs = useCallback(() => {
        setTitle(organizationTitle);
        setName(organizationName);
        setSurname(organizationSurname);
        setPhone(organizationPhone);
        setEmail(organizationEmail);

        setOldPassword('');
        setNewPassword('');
        setConfirmingPassword('');

        setTitleInputError(false);
        setNameInputError(false);
        setSurnameInputError(false);
        setPhoneInputError(false);
        setEmailInputError(false);
        setOldPasswordInputError(false);
        setMatchPasswordsError(false);
    }, [organizationEmail, organizationName, organizationPhone, organizationSurname, organizationTitle]);

    // если ошибка с сервера
    useEffect(() => {
        if (updateOrganizationError) {
            if (updateOrganizationError === 'Invalid password') {
                setOldPasswordInputError(t('incorrectPassword') as OldPasswordInputError);
                dispatch(setClearProfilePageErrors());
            }
        }
    }, [dispatch, t, updateOrganizationError]);

    // если успешно обновили данные, то очищаем инпуты паролей
    useEffect(() => {
        if (isUpdateOrganizationSuccess) {
            setOldPassword('');
            setNewPassword('');
            setConfirmingPassword('');
        }
    }, [isUpdateOrganizationSuccess]);

    const isPasswordsMatched = useCallback(
        ({ newPassword, confirmingPassword }: { newPassword: string; confirmingPassword: string }) => {
            let isValid = true;

            setMatchPasswordsError(false);

            if (newPassword !== confirmingPassword) {
                isValid = false;
                setMatchPasswordsError(t('passwordsDontMatch') as MatchPasswordsError);
            }

            return isValid;
        },
        [t],
    );

    const isInputsValid = useCallback(
        ({ title, name, surname, phone, email }: ProfileClientDto) => {
            let isValid = true;

            title = title.trim();
            name = name.trim();
            surname = surname.trim();
            phone = phone.trim();
            email = email.trim();

            setTitleInputError(false);
            setNameInputError(false);
            setSurnameInputError(false);
            setPhoneInputError(false);
            setEmailInputError(false);

            const emptyInputs = [{ title }, { name }, { surname }, { email }].filter(
                (item) => Object.values(item)[0] === '',
            );

            if (emptyInputs.length > 0) {
                emptyInputs.forEach((item) => {
                    const key = Object.keys(item)[0];
                    if (key === 'title') {
                        setTitleInputError(t('thereMustBeAtLeastOneLetterInTheName'));
                        isValid = false;
                    }

                    if (key === 'name') {
                        setNameInputError(t('thereMustBeAtLeastOneLetterInTheName'));
                        isValid = false;
                    }

                    if (key === 'surname') {
                        setSurnameInputError(t('thereMustBeAtLeastOneLetterInTheName'));
                        isValid = false;
                    }

                    if (key === 'email') {
                        setEmailInputError(t('theFieldCannotBeEmpty'));
                        isValid = false;
                    }
                });
            }

            if (email && !isEmail(email)) {
                setEmailInputError(t('incorrectEmail'));
                isValid = false;
            }

            if (phone && !isPossiblePhoneNumber(phone)) {
                setPhoneInputError(t('incorrectPhoneNumber'));
                isValid = false;
            }

            return isValid;
        },
        [t],
    );

    const updateProfile = useCallback(() => {
        if (
            isInputsValid({ title, name, surname, phone, email }) &&
            isPasswordsMatched({ newPassword, confirmingPassword })
        ) {
            const bodyParams = {
                title,
                phone,
                name,
                surname,
                email,
                password: oldPassword,
                newPassword,
                language: getLang(language),
            };

            dispatch(setUpdateOrganization({ id: organizationId, userId, ...bodyParams }));
        }
    }, [
        confirmingPassword,
        dispatch,
        email,
        isInputsValid,
        isPasswordsMatched,
        language,
        name,
        newPassword,
        oldPassword,
        organizationId,
        phone,
        surname,
        title,
        userId,
    ]);

    const showHideOldPassword = useCallback(() => {
        setIsOldPasswordShowed((prev) => !prev);
    }, []);

    const showHideNewPassword = useCallback(() => {
        setIsNewPasswordShowed((prev) => !prev);
    }, []);

    const showHideConfirmPassword = useCallback(() => {
        setIsConfirmPasswordShowed((prev) => !prev);
    }, []);

    const isAllPasswordsInputsEmpty = !oldPassword && !newPassword && !confirmingPassword;
    const isSomePasswordInputsEmpty = !oldPassword || !newPassword || !confirmingPassword;

    const isTitlePhoneEmailEqual = useMemo(
        () =>
            title?.toLowerCase().trim() === organizationTitle.toLowerCase() &&
            name?.toLowerCase().trim() === organizationName.toLowerCase() &&
            surname?.toLowerCase().trim() === organizationSurname.toLowerCase() &&
            phone?.toLowerCase().trim() === organizationPhone.toLowerCase().trim() &&
            email?.toLowerCase().trim() === organizationEmail.toLowerCase(),
        [
            email,
            name,
            organizationEmail,
            organizationName,
            organizationPhone,
            organizationSurname,
            organizationTitle,
            phone,
            surname,
            title,
        ],
    );

    const isSaveButtonDisabled = useMemo(
        () =>
            updatingOrganization || isAllPasswordsInputsEmpty
                ? isTitlePhoneEmailEqual && organizationLanguage === getLang(language)
                : isSomePasswordInputsEmpty,
        [
            isAllPasswordsInputsEmpty,
            isSomePasswordInputsEmpty,
            isTitlePhoneEmailEqual,
            language,
            organizationLanguage,
            updatingOrganization,
        ],
    );

    const isCancelButtonDisabled = useMemo(
        () => isTitlePhoneEmailEqual && oldPassword === '' && newPassword === '' && confirmingPassword === '',
        [confirmingPassword, isTitlePhoneEmailEqual, newPassword, oldPassword],
    );

    return {
        title,
        name,
        surname,
        email,
        phone,
        oldPassword,
        newPassword,
        confirmingPassword,
        onTitleChange,
        onNameChange,
        onSurnameChange,
        onPhoneChange,
        onEmailChange,
        onOldPasswordChange,
        onNewPasswordChange,
        onConfirmingPasswordChange,
        titleInputError,
        nameInputError,
        surnameInputError,
        phoneInputError,
        emailInputError,
        resetInputs,
        updateProfile,
        isSaveButtonDisabled,
        isCancelButtonDisabled,
        oldPasswordInputError,
        matchPasswordsError,
        isOldPasswordShowed,
        isNewPasswordShowed,
        isConfirmPasswordShowed,
        showHideOldPassword,
        showHideNewPassword,
        showHideConfirmPassword,

        language,
        languages,
        onLanguageSelect,
        t,
    };
};
