import { ChangeEvent, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectCurrentOrganization } from '../selectors';
import { setShowModalNew } from '../../../components/ModalsContainer/actions';
import { MODALS } from '../../../components/ModalsContainer/types';
import { httpService } from '../../../helpers/httpService';
import { useFetchOrganizationInfo } from '../../../hooks/useFetchOrganizationInfo';
import {
    type ScreensTariffInfo,
    screensTariffsInfoList,
    StorageTariffInfo,
    storageTariffsInfoList,
} from '../../../../../common/constants';
import {
    BalanceRequestType,
    BillingTypeEnum,
    ClientOrganizationInfo,
    LicenceTypeEnum,
} from '../../../../../common/types';

export const getScreensTariffSelectValue = (info: ScreensTariffInfo): string =>
    info ? `${info.screens} экр., ${info.capacity / 1000} гб - ${info.sum} руб. в год` : '';

export const getStorageTariffSelectValue = (info: StorageTariffInfo): string =>
    info ? `+ ${info.capacity / 1000} гб - ${info.sum} руб. в год` : '';

export const screensTariffsSelectList = screensTariffsInfoList.map((info) => getScreensTariffSelectValue(info));

export const storageTariffsSelectList = storageTariffsInfoList.map((info) => getStorageTariffSelectValue(info));

type ReturnValue = {
    organizationInfo: ClientOrganizationInfo;
    billingAvailable: boolean;
    screensNumber: number;
    screensTariffSelectValue: string;
    storageTariffSelectValue: string;
    onScreensTariffSelect: (event: SelectChangeEvent) => void;
    onStorageTariffSelect: (event: SelectChangeEvent) => void;
    openScreensTariffRobokassaWindow: () => void;
    openExtendTariffRobokassaWindow: (licenseId: string, type: LicenceTypeEnum) => () => void;
    openStorageTariffRobokassaWindow: () => void;
    onChangeScreensNumber: (event: ChangeEvent<HTMLInputElement>) => void;
    openBalanceRequestFormModal: (balanceRequestType: BalanceRequestType) => () => void;
    toCapacityInfo: (capacityAmount: number) => string;
    t: TFunction;
};

export const useTariffPlanTab = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'balanceTab' });

    const dispatch = useDispatch();

    const { id: organizationId, billingAvailable } = useSelector(selectCurrentOrganization);

    const { organizationInfo } = useFetchOrganizationInfo();

    const [screensNumber, setScreensNumber] = useState(0);

    const [screensTariffSelectValue, setScreensTariffSelectValue] = useState('');
    const [screensTariffInfo, setScreensTariffInfo] = useState<ScreensTariffInfo>();

    const [storageTariffSelectValue, setStorageTariffSelectValue] = useState('');
    const [storageTariffInfo, setStorageTariffInfo] = useState<StorageTariffInfo>();

    const onScreensTariffSelect = useCallback(({ target: { value } }: SelectChangeEvent) => {
        setScreensTariffSelectValue(value);
        const tariffInfoIndex = screensTariffsSelectList.findIndex((info) => info === value);
        const screenTariffInfo = screensTariffsInfoList[tariffInfoIndex];
        setScreensTariffInfo(screenTariffInfo);
    }, []);

    const onStorageTariffSelect = useCallback(({ target: { value } }: SelectChangeEvent) => {
        setStorageTariffSelectValue(value);
        const tariffInfoIndex = storageTariffsSelectList.findIndex((info) => info === value);
        const storageTariffInfo = storageTariffsInfoList[tariffInfoIndex];
        setStorageTariffInfo(storageTariffInfo);
    }, []);

    const openBalanceRequestFormModal = useCallback(
        (balanceRequestType: BalanceRequestType) => async () => {
            dispatch(
                setShowModalNew(MODALS.BALANCE_REQUEST_FORM_MODAL, {
                    balanceRequestType,
                    screensNumber,
                }),
            );
        },
        [dispatch, screensNumber],
    );

    const openScreensTariffRobokassaWindow = useCallback(async () => {
        try {
            const url = (await httpService.post('billing/refill', {
                organizationId,
                sum: screensTariffInfo?.sum ?? 0,
                type: BillingTypeEnum.screens,
                info: screensTariffInfo,
                description: `Приобретение подписки на экраны`,
            })) as string;

            window.open(url);
        } catch (err) {
            console.error(err);
            await openBalanceRequestFormModal('license_creation')();
        }
    }, [openBalanceRequestFormModal, organizationId, screensTariffInfo]);

    const openExtendTariffRobokassaWindow = useCallback(
        (licenseId: string, type: LicenceTypeEnum) => async () => {
            try {
                const url = (await httpService.post('billing/refill', {
                    organizationId,
                    licenseId,
                    type: type === LicenceTypeEnum.basic_screens ? BillingTypeEnum.screens : BillingTypeEnum.storage,
                    description:
                        type === LicenceTypeEnum.basic_screens
                            ? 'Продление подписки на экраны'
                            : 'Продление подписки на увеличение пространства хранения контента',
                })) as string;

                window.open(url);
            } catch (err) {
                console.error(err);
                await openBalanceRequestFormModal(
                    type === LicenceTypeEnum.basic_screens ? 'license_renewal' : 'storage_increase',
                )();
            }
        },
        [openBalanceRequestFormModal, organizationId],
    );

    const openStorageTariffRobokassaWindow = useCallback(async () => {
        try {
            const url = (await httpService.post('billing/refill', {
                organizationId,
                sum: storageTariffInfo?.sum ?? 0,
                type: BillingTypeEnum.storage,
                info: storageTariffInfo,
                description: 'Увеличение пространства хранения контента',
            })) as string;

            window.open(url);
        } catch (err) {
            console.error(err);
            await openBalanceRequestFormModal('storage_increase')();
        }
    }, [openBalanceRequestFormModal, organizationId, storageTariffInfo]);

    const onChangeScreensNumber = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        if (value.match(/^\d{0,5}$/)) {
            setScreensNumber(Number(value));
        }
    }, []);

    const toCapacityInfo = useCallback(
        (capacityAmount: number) =>
            capacityAmount >= 1000 ? `${capacityAmount / 1000} ${t('gb')}` : `${capacityAmount} ${t('mb')}`,
        [t],
    );

    return {
        organizationInfo,
        billingAvailable,
        screensNumber,
        screensTariffSelectValue,
        storageTariffSelectValue,
        onScreensTariffSelect,
        onStorageTariffSelect,
        openScreensTariffRobokassaWindow,
        openExtendTariffRobokassaWindow,
        openStorageTariffRobokassaWindow,
        onChangeScreensNumber,
        openBalanceRequestFormModal,
        toCapacityInfo,
        t,
    };
};
