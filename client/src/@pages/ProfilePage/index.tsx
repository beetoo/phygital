import React from 'react';
import { clsx } from 'clsx';

import ProfileTab from './components/ProfileTab';
import TariffPlanTab from './components/TariffPlanTab';
import DocumentationTab from './components/DocumentationTab';
import { useProfilePage } from './hooks/useProfilePage';
import { isProductionEngClient, isSLClientMode } from '../../constants/environments';

import css from './style.m.css';

const ProfilePage: React.FC = () => {
    const { profileTab, chooseProfileTab, t } = useProfilePage();

    return (
        <div className={css.menuPro}>
            <div className={css.wrapperMenuPro}>
                <div className={css.mainProfilePage}>
                    <div className={css.profilePageHeader}>
                        <ul className={css.profilePageHeaderList}>
                            <li
                                className={clsx(profileTab === 'profile' && css.profilePageHeaderItemCurrent)}
                                onClick={chooseProfileTab('profile')}
                            >
                                <a>{t('profile')}</a>
                            </li>

                            {!isSLClientMode && (
                                <li
                                    className={clsx(profileTab === 'tariff_plan' && css.profilePageHeaderItemCurrent)}
                                    onClick={chooseProfileTab('tariff_plan')}
                                >
                                    <a>{t('balance')}</a>
                                </li>
                            )}

                            {!isProductionEngClient && (
                                <li
                                    className={clsx(profileTab === 'documentation' && css.profilePageHeaderItemCurrent)}
                                    onClick={chooseProfileTab('documentation')}
                                >
                                    <a>{t('documentation')}</a>
                                </li>
                            )}
                        </ul>
                    </div>
                    {profileTab === 'profile' && <ProfileTab />}
                    {!isSLClientMode && profileTab === 'tariff_plan' && <TariffPlanTab />}
                    {profileTab === 'documentation' && <DocumentationTab />}
                </div>
            </div>
        </div>
    );
};

export default ProfilePage;
