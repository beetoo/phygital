import {
    setGetOrganization,
    setGetOrganizationFail,
    setGetOrganizationSuccess,
    setScreensNumberForTariffPlan,
    setProfileTab,
    setUpdateOrganization,
    setUpdateOrganizationFail,
    setUpdateOrganizationSuccess,
    setClearProfilePageErrors,
} from '../actions';
import { ErrorResponse } from '../../../types';
import { ClientOrganizationType } from '../../../../../common/types';

export type ProfileTabType = 'profile' | 'tariff_plan' | 'documentation';

export type ProfilePageState = {
    loadingOrganization: boolean;
    organization: ClientOrganizationType;
    loadOrganizationError: ErrorResponse | null;

    updatingOrganization: boolean;
    isUpdateOrganizationSuccess: boolean;
    updateOrganizationError: ErrorResponse | null;

    profileTab: ProfileTabType;
    screensNumberForTariffPlan: number;
};

export type GetOrganizationAction = ReturnType<typeof setGetOrganization>;
export type GetOrganizationSuccessAction = ReturnType<typeof setGetOrganizationSuccess>;
export type GetOrganizationFailAction = ReturnType<typeof setGetOrganizationFail>;

export type UpdateOrganizationAction = ReturnType<typeof setUpdateOrganization>;
export type UpdateOrganizationSuccessAction = ReturnType<typeof setUpdateOrganizationSuccess>;
export type UpdateOrganizationFailAction = ReturnType<typeof setUpdateOrganizationFail>;

export type SetProfileTabAction = ReturnType<typeof setProfileTab>;
export type SetScreensNumberForTariffPlanAction = ReturnType<typeof setScreensNumberForTariffPlan>;

export type ClearProfilePageErrorsAction = ReturnType<typeof setClearProfilePageErrors>;

export type ProfileAction =
    | GetOrganizationAction
    | GetOrganizationSuccessAction
    | GetOrganizationFailAction
    | UpdateOrganizationAction
    | UpdateOrganizationSuccessAction
    | UpdateOrganizationFailAction
    | SetProfileTabAction
    | SetScreensNumberForTariffPlanAction
    | ClearProfilePageErrorsAction;
