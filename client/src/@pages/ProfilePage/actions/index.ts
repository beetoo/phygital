import { ErrorResponse } from '../../../types';
import { UpdateUserAndOrganizationDto } from '../../../../../server/src/models/organization/dto/update-user-and-organization.dto';
import { ProfileTabType } from '../types';
import { ClientOrganizationType } from '../../../../../common/types';

export const GET_ORGANIZATION = 'GET_ORGANIZATION';
export const GET_ORGANIZATION_SUCCESS = 'GET_ORGANIZATION_SUCCESS';
export const GET_ORGANIZATION_FAIL = 'GET_ORGANIZATION_FAIL';

export const UPDATE_ORGANIZATION = 'UPDATE_ORGANIZATION';
export const UPDATE_ORGANIZATION_SUCCESS = 'UPDATE_ORGANIZATION_SUCCESS';
export const UPDATE_ORGANIZATION_FAIL = 'UPDATE_ORGANIZATION_FAIL';

export const SET_PROFILE_TAB = 'SET_PROFILE_TAB';
export const SET_SCREENS_NUMBER_FOR_TARIFF_PLAN = 'SET_SCREENS_NUMBER_FOR_TARIFF_PLAN';

export const CLEAR_PROFILE_PAGE_ERRORS = 'CLEAR_PROFILE_PAGE_ERRORS';

export const setGetOrganization = (organizationId: string) =>
    ({ type: GET_ORGANIZATION, payload: { organizationId } }) as const;
export const setGetOrganizationSuccess = (organization: ClientOrganizationType) =>
    ({ type: GET_ORGANIZATION_SUCCESS, payload: { organization } }) as const;
export const setGetOrganizationFail = (errors: ErrorResponse) =>
    ({ type: GET_ORGANIZATION_FAIL, payload: { errors } }) as const;

export const setUpdateOrganization = (updateProfileDto: UpdateUserAndOrganizationDto) =>
    ({ type: UPDATE_ORGANIZATION, payload: updateProfileDto }) as const;
export const setUpdateOrganizationSuccess = () => ({ type: UPDATE_ORGANIZATION_SUCCESS }) as const;
export const setUpdateOrganizationFail = (errors: ErrorResponse) =>
    ({ type: UPDATE_ORGANIZATION_FAIL, payload: { errors } }) as const;

export const setScreensNumberForTariffPlan = (screensNumber: number) =>
    ({ type: SET_SCREENS_NUMBER_FOR_TARIFF_PLAN, payload: { screensNumber } }) as const;

export const setProfileTab = (profileTab: ProfileTabType) =>
    ({ type: SET_PROFILE_TAB, payload: { profileTab } }) as const;

export const setClearProfilePageErrors = () => ({ type: CLEAR_PROFILE_PAGE_ERRORS }) as const;
