import React from 'react';
import { Link } from 'react-router-dom';
import { CircularProgress } from '@mui/material';
import isEmail from 'validator/es/lib/isEmail';
import { clsx } from 'clsx';

import { Tooltip } from '../../ui-kit';
import imageGirl1x from '../../assets/img_girl1x.png';
import { useSignUp } from './hooks/useSignUp';
import PrivacyPolicyModal from './components/PrivacyPolicyModal';

import css from './style.m.css';

const SignUpPage: React.FC = () => {
    const {
        name,
        surname,
        email,
        password,
        policyAgreement,
        nameInputError,
        surnameInputError,
        emailInputError,
        passwordInputError,
        onNameInputChange,
        onSurnameInputChange,
        onEmailInputChange,
        onPasswordInputChange,
        onPolicyAgreementChange,
        showPrivacyPolicyModal,
        setPolicyAgreement,
        onSubmit,
        authProcessing,
        clearRecoveryPasswordEmail,
        isPasswordShow,
        showHidePassword,
        isSubmitRegistrationButtonDisabled,
        t,
    } = useSignUp();

    return (
        <>
            <PrivacyPolicyModal {...{ setPolicyAgreement }} />
            <div className={css.mainRegister}>
                <div className={clsx(css.bgLeftContainer, css.signUp)}>
                    <div className={css.bgLeft}>
                        <h2>
                            {t('boostSalesWith')} <span>{t('digitalPOSMaterials')}</span>
                        </h2>
                        <img src={imageGirl1x} alt="" />
                    </div>
                </div>
                <div className={css.bgRightContainer}>
                    <div className={css.bgRight}>
                        <div className={css.bgRightSignUp}>
                            <p>{t('alreadyHaveAnAccount')}</p>
                            <Link onClick={clearRecoveryPasswordEmail} to="/signin">
                                {t('logIn')}
                            </Link>
                        </div>
                        <form
                            className={clsx(css.bgRightFormContainer, css.signUp)}
                            onSubmit={(e) => e.preventDefault()}
                        >
                            <div className={css.bgRightForm}>
                                <h2>{t('signUp')}</h2>
                                <div className={css.signUpRow}>
                                    <div className={css.fieldName}>
                                        <div className={css.fieldLabelWrapper}>
                                            <label className={css.fieldLabel} htmlFor="name">
                                                {t('name')} *
                                            </label>
                                        </div>
                                        <input
                                            id="name"
                                            className={clsx(nameInputError ? css.inputError : css.input, {
                                                [css.valid]: name.length > 0,
                                                [css.iconWarning]: nameInputError,
                                            })}
                                            type="text"
                                            placeholder={t('name') as string}
                                            value={name}
                                            onChange={onNameInputChange}
                                            maxLength={125}
                                        />
                                        {nameInputError && <div className={css.errorTitle}>{nameInputError}</div>}
                                    </div>
                                    <div className={css.fieldName}>
                                        <div className={css.fieldLabelWrapper}>
                                            <label className={css.fieldLabel} htmlFor="surname">
                                                {t('surname')} *
                                            </label>
                                        </div>

                                        <input
                                            id="surname"
                                            className={clsx(surnameInputError ? css.inputError : css.input, {
                                                [css.valid]: surname.length > 0,
                                                [css.iconWarning]: surnameInputError,
                                            })}
                                            type="text"
                                            placeholder={t('surname') as string}
                                            value={surname}
                                            onChange={onSurnameInputChange}
                                            maxLength={125}
                                        />

                                        {surnameInputError && <div className={css.errorTitle}>{surnameInputError}</div>}
                                    </div>
                                </div>

                                <div className={css.signUpRow}>
                                    <div className={css.fieldEmail}>
                                        <div className={css.fieldLabelWrapper}>
                                            <label className={css.fieldLabel} htmlFor="email">
                                                {t('email')} *
                                            </label>
                                        </div>

                                        <input
                                            id="email"
                                            className={clsx(emailInputError ? css.inputError : css.input, {
                                                [css.valid]: isEmail(email),
                                                [css.iconWarning]: emailInputError,
                                            })}
                                            type="text"
                                            placeholder="myemail@adress.ru"
                                            value={email}
                                            onChange={onEmailInputChange}
                                            maxLength={125}
                                        />

                                        {emailInputError && <div className={css.errorEmail}>{emailInputError}</div>}
                                    </div>
                                    <div className={css.fieldPassword}>
                                        <div className={css.fieldLabelWrapper}>
                                            <label className={css.fieldLabel} htmlFor="password">
                                                {t('password')} *
                                            </label>
                                        </div>

                                        <input
                                            id="password"
                                            className={clsx(
                                                passwordInputError ? css.inputError : css.input,
                                                password && css.passwordValid,
                                            )}
                                            type={isPasswordShow ? 'text' : 'password'}
                                            placeholder={t('enterPassword') as string}
                                            value={password}
                                            onChange={onPasswordInputChange}
                                            autoComplete="off"
                                            maxLength={125}
                                        />
                                        <Tooltip
                                            title={isPasswordShow ? `${t('hidePassword')}` : `${t('viewPassword')}`}
                                            placement="right"
                                        >
                                            <span
                                                className={clsx(css.iconEye, {
                                                    [css.iconEyeOpened]: isPasswordShow,
                                                    [css.iconEyeClosed]: !isPasswordShow,
                                                })}
                                                onClick={showHidePassword}
                                            />
                                        </Tooltip>
                                        {passwordInputError && (
                                            <div className={css.errorPassword}>{passwordInputError}</div>
                                        )}
                                    </div>
                                </div>

                                <div className={css.bgRightFormCheck}>
                                    <input
                                        className={css.bgFormCheck}
                                        id="checkbox"
                                        type="checkbox"
                                        onChange={onPolicyAgreementChange}
                                        checked={policyAgreement}
                                        maxLength={125}
                                    />
                                    <label htmlFor="checkbox">
                                        {t('iAccept')}{' '}
                                        <a
                                            onClick={(event) => {
                                                event.stopPropagation();
                                                showPrivacyPolicyModal();
                                            }}
                                        >
                                            {t('thePrivacyPolicy')}
                                        </a>
                                    </label>
                                </div>
                                <button
                                    className={css.registerBtn}
                                    onClick={onSubmit}
                                    disabled={isSubmitRegistrationButtonDisabled}
                                    style={{ paddingTop: authProcessing ? '6px' : undefined }}
                                >
                                    {authProcessing ? <CircularProgress color="inherit" size={30} /> : t('signUp')}
                                </button>
                                <p className={css.mobileBgRightFormCheck}>
                                    {t('ByClickingYouAccept')}&nbsp;
                                    <a onClick={showPrivacyPolicyModal}>{t('thePrivacyPolicy')}</a>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
};

export default SignUpPage;
