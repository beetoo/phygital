import React, { Dispatch, SetStateAction } from 'react';

import ModalTemplate from '../../../../components/ModalTemplate';
import { PrivacyPolicyText } from '../../../../components/PrivacyPolicyText';
import { usePrivacyPolicyModal } from '../../hooks/usePrivacyPolicyModal';

import css from './style.m.css';

type Props = {
    setPolicyAgreement: Dispatch<SetStateAction<boolean>>;
};

const PrivacyPolicyModal: React.FC<Props> = ({ setPolicyAgreement }: Props) => {
    const { isPrivacyPolicyModalOpen, onPolicyAgreement, onPolicyDisagreement, hidePrivacyPolicyModal } =
        usePrivacyPolicyModal(setPolicyAgreement);

    return (
        <ModalTemplate isOpen={isPrivacyPolicyModalOpen}>
            <div className={css.privacyPolicyContainer}>
                <div className={css.privacyPolicyContainerText}>
                    <PrivacyPolicyText />
                </div>
                <div className={css.privacyPolicyContainerFieldBtn}>
                    <button className={css.privacyPolicyContainerBtnAccept} onClick={onPolicyAgreement}>
                        Принимаю
                    </button>
                    <button className={css.privacyPolicyContainerBtnNotAccept} onClick={onPolicyDisagreement}>
                        Не принимаю
                    </button>
                </div>
                <span className={css.iconX} onClick={hidePrivacyPolicyModal} />
            </div>
        </ModalTemplate>
    );
};

export default PrivacyPolicyModal;
