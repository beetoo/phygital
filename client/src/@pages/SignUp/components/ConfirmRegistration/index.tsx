import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useMatch, Navigate } from 'react-router-dom';

import { setConfirmRegistration } from '../../actions';
import { ConfirmRegistrationStatus, useConfirmRegistration } from '../../hooks/useConfirmRegistration';
import RegisterWelcomePage from '../../../MainPage/components/RegisterWelcomePage';

const ConfirmRegistration: React.FC = () => {
    const dispatch = useDispatch();

    const { status } = useConfirmRegistration();

    const {
        params: { confirmationCode },
    } = useMatch('/confirm-registration/:confirmationCode') as {
        params: { confirmationCode: string };
    };

    useEffect(() => {
        if (confirmationCode) {
            dispatch(setConfirmRegistration(confirmationCode));
        }
    }, [confirmationCode, dispatch]);

    return status === ConfirmRegistrationStatus.FAIL ? <Navigate to="/signin" /> : <RegisterWelcomePage />;
};

export default ConfirmRegistration;
