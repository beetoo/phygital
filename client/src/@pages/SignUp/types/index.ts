import {
    setSignUpProcess,
    setSignUpSuccess,
    setSignUpFail,
    setConfirmRegistration,
    setConfirmRegistrationSuccess,
    setConfirmRegistrationFail,
    setCloseRegisterWelcomePage,
} from '../actions';

export type SignUpProcessAction = ReturnType<typeof setSignUpProcess>;
export type SignUpSuccessAction = ReturnType<typeof setSignUpSuccess>;
export type SignUpFailAction = ReturnType<typeof setSignUpFail>;

export type ConfirmRegistrationAction = ReturnType<typeof setConfirmRegistration>;
export type ConfirmRegistrationSuccessAction = ReturnType<typeof setConfirmRegistrationSuccess>;
export type ConfirmRegistrationFailAction = ReturnType<typeof setConfirmRegistrationFail>;

export type CloseRegisterWelcomePageAction = ReturnType<typeof setCloseRegisterWelcomePage>;

export type SignUpAction =
    | SignUpProcessAction
    | SignUpSuccessAction
    | SignUpFailAction
    | ConfirmRegistrationAction
    | ConfirmRegistrationSuccessAction
    | ConfirmRegistrationFailAction
    | CloseRegisterWelcomePageAction;
