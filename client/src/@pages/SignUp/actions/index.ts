import { AuthResponse, ErrorResponse } from '../../../types';
import { SignUpClientDto } from '../hooks/useSignUp';

export const SIGN_UP_PROCESS = 'SIGN_UP_PROCESS';
export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';
export const SIGN_UP_FAIL = 'SIGN_UP_FAIL';

export const CONFIRM_REGISTRATION = 'CONFIRM_REGISTRATION';
export const CONFIRM_REGISTRATION_SUCCESS = 'CONFIRM_REGISTRATION_SUCCESS';
export const CONFIRM_REGISTRATION_FAIL = 'CONFIRM_REGISTRATION_FAIL';

export const CLOSE_REGISTER_WELCOME_PAGE = 'CLOSE_REGISTER_WELCOME_PAGE';

export const setSignUpProcess = (payload: SignUpClientDto) =>
    ({
        type: SIGN_UP_PROCESS,
        payload,
    }) as const;
export const setSignUpSuccess = (payload: AuthResponse) => ({ type: SIGN_UP_SUCCESS, payload }) as const;
export const setSignUpFail = (errors: ErrorResponse) =>
    ({
        type: SIGN_UP_FAIL,
        payload: { errors },
    }) as const;

export const setConfirmRegistration = (confirmationCode: string) =>
    ({ type: CONFIRM_REGISTRATION, payload: { confirmationCode } }) as const;
export const setConfirmRegistrationSuccess = ({ id, role, email, name, surname, firstConfirmation }: AuthResponse) =>
    ({ type: CONFIRM_REGISTRATION_SUCCESS, payload: { id, role, email, name, surname, firstConfirmation } }) as const;
export const setConfirmRegistrationFail = (errors: ErrorResponse) =>
    ({ type: CONFIRM_REGISTRATION_FAIL, payload: { errors } }) as const;

export const setCloseRegisterWelcomePage = () => ({ type: CLOSE_REGISTER_WELCOME_PAGE }) as const;
