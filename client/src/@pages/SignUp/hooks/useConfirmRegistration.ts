import { useSelector } from 'react-redux';

import { selectAuthCheckingStatus, selectAuthErrorMessage, selectAuthProcessingStatus } from '../../../selectors';
import { selectConfirmRegistrationSuccessStatus } from '../selectors';

export enum ConfirmRegistrationStatus {
    IN_PROGRESS,
    WIN,
    FAIL,
    UNKNOWN,
}

type ReturnValue = {
    status: ConfirmRegistrationStatus;
};

export const useConfirmRegistration = (): ReturnValue => {
    const authProcessing = useSelector(selectAuthProcessingStatus);
    const authChecking = useSelector(selectAuthCheckingStatus);
    const confirmRegistrationSuccess = useSelector(selectConfirmRegistrationSuccessStatus);
    const authErrorMessage = useSelector(selectAuthErrorMessage);

    return {
        status:
            authProcessing || authChecking
                ? ConfirmRegistrationStatus.IN_PROGRESS
                : confirmRegistrationSuccess
                  ? ConfirmRegistrationStatus.WIN
                  : authErrorMessage
                    ? ConfirmRegistrationStatus.FAIL
                    : ConfirmRegistrationStatus.UNKNOWN,
    };
};
