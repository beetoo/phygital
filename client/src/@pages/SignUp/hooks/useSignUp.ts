import { ChangeEvent, Dispatch, SetStateAction, useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import isEmail from 'validator/es/lib/isEmail';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectAuthErrorMessage, selectAuthProcessingStatus } from '../../../selectors';
import { setSignUpProcess } from '../actions';
import { setShowModal } from '../../../components/ModalsContainer/actions';
import { MODALS } from '../../../components/ModalsContainer/types';
import { setRecoveryPasswordEmail } from '../../RecoveryPassword/actions';
import { selectRecoveryPasswordEmail } from '../../RecoveryPassword/selectors';
import { setClearAuthErrors } from '../../../actions';

export type SignUpClientDto = {
    name: string;
    surname: string;
    email: string;
    password: string;
};

type ReturnValue = {
    name: string;
    surname: string;
    email: string;
    password: string;
    policyAgreement: boolean;
    nameInputError: string | false;
    surnameInputError: string | false;
    emailInputError: string | false;
    passwordInputError: string | false;
    authProcessing: boolean;
    onNameInputChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onSurnameInputChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onEmailInputChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onPasswordInputChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onPolicyAgreementChange: () => void;
    showPrivacyPolicyModal: () => void;
    setPolicyAgreement: Dispatch<SetStateAction<boolean>>;
    onSubmit: () => void;
    isPasswordShow: boolean;
    showHidePassword: () => void;
    clearRecoveryPasswordEmail: () => void;
    isSubmitRegistrationButtonDisabled: boolean;
    t: TFunction;
};

export const useSignUp = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'signUp' });

    const dispatch = useDispatch();

    const [isPasswordShow, setIsPasswordShow] = useState(false);

    const authProcessing = useSelector(selectAuthProcessingStatus);
    const authErrorMessage = useSelector(selectAuthErrorMessage);
    const recoveryPasswordEmail = useSelector(selectRecoveryPasswordEmail);

    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [policyAgreement, setPolicyAgreement] = useState(true);

    const [nameInputError, setNameInputError] = useState<string | false>(false);
    const [surnameInputError, setSurnameInputError] = useState<string | false>(false);
    const [emailInputError, setEmailInputError] = useState<string | false>(false);
    const [passwordInputError, setPasswordInputError] = useState<string | false>(false);

    useEffect(() => {
        if (typeof authErrorMessage !== 'string' && authErrorMessage?.errors?.email === 'EMAIL_ALREADY_EXISTS') {
            setEmailInputError(t('emailAlreadyExists'));
        }
    }, [authErrorMessage, t]);

    const onNameInputChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            setNameInputError(!value ? t('theFieldCannotBeEmpty') : false);
            setName(value);
        },
        [t],
    );

    const onSurnameInputChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            setSurnameInputError(!value ? t('theFieldCannotBeEmpty') : false);
            setSurname(value);
        },
        [t],
    );

    const onEmailInputChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            if (authErrorMessage) {
                dispatch(setClearAuthErrors());
            }

            setEmailInputError(!value ? t('theFieldCannotBeEmpty') : !isEmail(value) ? t('incorrectEmail') : false);
            setEmail(value);
        },
        [authErrorMessage, dispatch, t],
    );

    const onPasswordInputChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            value = value.split(' ').join('');

            setPasswordInputError(value.length < 8 ? t('passwordMustContainCharacters') : false);

            setPassword(value);
        },
        [t],
    );

    const onPolicyAgreementChange = useCallback(() => {
        setPolicyAgreement(!policyAgreement);
    }, [policyAgreement]);

    const showPrivacyPolicyModal = useCallback(() => {
        dispatch(setShowModal(MODALS.PRIVACY_POLICY));
    }, [dispatch]);

    const clearRecoveryPasswordEmail = useCallback(() => {
        if (recoveryPasswordEmail) {
            dispatch(setRecoveryPasswordEmail());
        }
    }, [dispatch, recoveryPasswordEmail]);

    const showHidePassword = useCallback(() => {
        setIsPasswordShow((prevValue) => !prevValue);
    }, []);

    const onSubmit = useCallback(() => {
        dispatch(setSignUpProcess({ email, name, surname, password }));
    }, [dispatch, email, name, password, surname]);

    const isSubmitRegistrationButtonDisabled = useMemo(
        () =>
            authProcessing ||
            !policyAgreement ||
            !name.trim() ||
            !surname.trim() ||
            !email.trim() ||
            !isEmail(email) ||
            !password ||
            password.length < 8,
        [authProcessing, email, name, password, policyAgreement, surname],
    );

    return {
        name,
        surname,
        email,
        password,
        policyAgreement,
        nameInputError,
        surnameInputError,
        emailInputError,
        passwordInputError,
        onNameInputChange,
        onSurnameInputChange,
        onEmailInputChange,
        onPasswordInputChange,
        onPolicyAgreementChange,
        showPrivacyPolicyModal,
        setPolicyAgreement,
        authProcessing,
        onSubmit,
        clearRecoveryPasswordEmail,
        isPasswordShow,
        showHidePassword,
        isSubmitRegistrationButtonDisabled,
        t,
    };
};
