import { Dispatch, SetStateAction, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { setHideModal } from '../../../components/ModalsContainer/actions';
import { selectCurrentModal } from '../../../components/ModalsContainer/selectors';
import { MODALS } from '../../../components/ModalsContainer/types';

type ReturnValue = {
    isPrivacyPolicyModalOpen: boolean;
    hidePrivacyPolicyModal: () => void;
    onPolicyAgreement: () => void;
    onPolicyDisagreement: () => void;
};

export const usePrivacyPolicyModal = (setPolicyAgreement: Dispatch<SetStateAction<boolean>>): ReturnValue => {
    const dispatch = useDispatch();

    const currentModal = useSelector(selectCurrentModal);

    const isPrivacyPolicyModalOpen = currentModal === MODALS.PRIVACY_POLICY;

    const onPolicyAgreement = useCallback(() => {
        setPolicyAgreement(true);
        dispatch(setHideModal());
    }, [dispatch, setPolicyAgreement]);

    const onPolicyDisagreement = useCallback(() => {
        setPolicyAgreement(false);
        dispatch(setHideModal());
    }, [dispatch, setPolicyAgreement]);

    const hidePrivacyPolicyModal = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    return {
        isPrivacyPolicyModalOpen,
        onPolicyAgreement,
        onPolicyDisagreement,
        hidePrivacyPolicyModal,
    };
};
