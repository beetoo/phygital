import { StoreType } from '../../../types';

export const selectRegistrationSuccessStatus = ({ auth: { isRegistrationSuccess } }: StoreType): boolean =>
    isRegistrationSuccess;

export const selectConfirmRegistrationSuccessStatus = ({
    auth: { isConfirmRegistrationSuccess },
}: StoreType): boolean => isConfirmRegistrationSuccess;
