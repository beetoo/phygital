import { of, Observable } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { ofType, combineEpics } from 'redux-observable';

import {
    SIGN_UP_PROCESS,
    setSignUpSuccess,
    setSignUpFail,
    CONFIRM_REGISTRATION,
    setConfirmRegistrationSuccess,
    setConfirmRegistrationFail,
} from '../actions';
import { fromConfirmRegistration, fromSignUp } from '../../../resolvers/authResolvers';
import {
    ConfirmRegistrationAction,
    ConfirmRegistrationFailAction,
    ConfirmRegistrationSuccessAction,
    SignUpFailAction,
    SignUpProcessAction,
    SignUpSuccessAction,
} from '../types';
import { setAuthTokenToLocalStorage } from '../../../helpers/httpService/setAuthTokenToLocalStorage';

const signUpEpic = (action$: Observable<SignUpProcessAction>): Observable<SignUpSuccessAction | SignUpFailAction> =>
    action$.pipe(
        ofType(SIGN_UP_PROCESS),
        mergeMap(({ payload }) =>
            fromSignUp(payload).pipe(
                map((response) => {
                    setAuthTokenToLocalStorage(response);
                    return setSignUpSuccess(response);
                }),
                catchError((error) => of(setSignUpFail(error))),
            ),
        ),
    );

const confirmRegistrationEpic = (
    action$: Observable<ConfirmRegistrationAction>,
): Observable<ConfirmRegistrationSuccessAction | ConfirmRegistrationFailAction> =>
    action$.pipe(
        ofType(CONFIRM_REGISTRATION),
        mergeMap(({ payload: { confirmationCode } }) =>
            fromConfirmRegistration(confirmationCode).pipe(
                map((response) => {
                    setAuthTokenToLocalStorage(response);
                    return setConfirmRegistrationSuccess(response);
                }),
                catchError((error) => of(setConfirmRegistrationFail(error))),
            ),
        ),
    );

type EpicsActions =
    | SignUpProcessAction
    | SignUpSuccessAction
    | SignUpFailAction
    | ConfirmRegistrationAction
    | ConfirmRegistrationSuccessAction
    | ConfirmRegistrationFailAction;

export const signUpEpics = combineEpics<EpicsActions>(signUpEpic, confirmRegistrationEpic);
