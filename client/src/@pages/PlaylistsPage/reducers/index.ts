import {
    ADD_PLAYLIST,
    ADD_PLAYLIST_FAIL,
    ADD_PLAYLIST_SUCCESS,
    CLEAR_PLAYLISTS_SUCCESS,
    CLOSE_DELETE_PLAYLIST_ALERT,
    CLOSE_DELETE_PLAYLIST_ITEM_ALERT,
    CLOSE_START_PLAYLIST_MODAL,
    DELETE_PLAYLIST_ITEMS,
    DELETE_PLAYLIST_ITEMS_FAIL,
    DELETE_PLAYLIST_ITEMS_SUCCESS,
    DELETE_PLAYLISTS,
    DELETE_PLAYLISTS_FAIL,
    DELETE_PLAYLISTS_SUCCESS,
    GET_PLAYLIST_ITEMS,
    GET_PLAYLIST_ITEMS_FAIL,
    GET_PLAYLIST_ITEMS_SUCCESS,
    GET_PLAYLISTS,
    GET_PLAYLISTS_FAIL,
    GET_PLAYLISTS_SUCCESS,
    OPEN_DELETE_PLAYLIST_ALERT,
    OPEN_DELETE_PLAYLIST_ITEM_ALERT,
    OPEN_START_PLAYLIST_MODAL,
    SET_ACTIVE_PLAYLIST_ID,
    SET_ACTIVE_PLAYLIST_ITEM_ID,
    SET_CONTENT_DISPLAY_TYPE,
    SET_PLAYLIST_DTO_FILTERING_TAGS,
    SET_PLAYLIST_DTO_INFO,
    SET_PLAYLIST_DTO_LOG,
    SET_PLAYLIST_DTO_PLAYLIST_ITEMS,
    SET_PLAYLIST_DTO_SCHEDULES,
    SET_PLAYLIST_DTO_SCREENS,
    SET_PLAYLIST_DTO_SYNC,
    SET_SELECTED_PLAYLIST_IDS,
    SET_SELECTED_PLAYLIST_ITEM_IDS,
    SET_STEP_TWO_ACTIVE_PLAYLIST_ITEM_ID,
    SET_STEP_TWO_ACTIVE_SCENARIO_ID,
    SET_STEP_TWO_CURRENT_TIMELINE_SECONDS,
    SET_STEP_TWO_MENU_MODE,
    SET_STEP_TWO_PLAY_MODE,
    SET_STEP_TWO_PLAYABLE_PLAYLIST_ITEM_ID,
    SET_STEP_TWO_SCENARIOS_FILTER_OPEN,
    SET_STEP_TWO_SELECTED_CONTENT_ITEM_IDS,
    SET_STEP_TWO_SELECTED_PLAYLIST_ITEM_IDS,
    SET_STEP_TWO_SHOWED_SCENARIOS_IDS,
    SET_STEP_TWO_SLIDER_MOVING,
    SET_TIME_LINE_REF,
    UPDATE_PLAYLIST,
    UPDATE_PLAYLIST_FAIL,
    UPDATE_PLAYLIST_ITEM_NAME,
    UPDATE_PLAYLIST_ITEM_NAME_FAIL,
    UPDATE_PLAYLIST_ITEM_NAME_SUCCESS,
    UPDATE_PLAYLIST_SUCCESS,
    UPDATE_PLAYLIST_TITLE,
    UPDATE_PLAYLIST_TITLE_FAIL,
    UPDATE_PLAYLIST_TITLE_SUCCESS,
} from '../actions';

import { PlaylistsAction, PlaylistsPageState } from '../types';

const playlistsPageInitialState: PlaylistsPageState = {
    selectedPlaylistIds: [],
    selectedPlaylistItemIds: [],

    isDeletePlaylistAlertOpen: false,
    isDeletePlaylistItemAlertOpen: false,

    timeLineRef: null,
    contentDisplayType: 'horizontal',

    stepTwoMenuMode: 'content_menu',
    stepTwoActivePlaylistItemId: '',
    stepTwoPlayablePlaylistItemId: '',
    stepTwoActiveScenarioId: '',
    stepTwoSelectedPlaylistItemIds: [],
    stepTwoSelectedContentItemIds: [],
    stepTwoScenariosFilterOpen: false,
    stepTwoShowedScenariosIds: [],

    stepTwoPlayMode: 'pause',
    stepTwoSliderMoving: false,
    stepTwoCurrentTimelineSeconds: 0,

    activePlaylistId: '',
    activePlaylistItemId: '',
    deletablePlaylistItemId: '',

    loadingPlaylists: false,
    playlists: [],
    loadingPlaylistsError: null,
    playlistDtoScreens: [],
    playlistDto: {
        playlistDtoInfo: { title: '', draft: false },
        playlistDtoFilteringTags: [],
        playlistDtoItems: [],
        playlistDtoSchedules: [],
        playlistDtoSync: false,
        playlistDtoLog: true,
    },
    isStartPlaylistModalOpen: false,

    addingPlaylist: false,
    addPlaylistSuccess: false,
    addingPlaylistError: null,

    updatingPlaylist: false,
    updatePlaylistSuccess: false,
    updatingPlaylistError: null,

    updatingPlaylistTitle: false,
    isUpdatePlaylistTitleSuccess: false,
    updatingPlaylistTitleError: null,

    deletePlaylist: false,
    isDeletePlaylistSuccess: false,
    deletePlaylistError: null,

    loadingPlaylistItems: false,
    playlistItems: [],
    loadingPlaylistItemsError: null,

    updatingPlaylistItemName: false,
    updatingPlaylistItemNameError: null,

    deletePlaylistItem: false,
    isDeletePlaylistItemSuccess: false,
    deletePlaylistItemError: null,
};

export default function playlistsPageReducer(
    state = playlistsPageInitialState,
    action: PlaylistsAction,
): PlaylistsPageState {
    switch (action.type) {
        case SET_SELECTED_PLAYLIST_IDS:
            return {
                ...state,
                selectedPlaylistIds: action.payload.playlistIds,
            };
        case SET_SELECTED_PLAYLIST_ITEM_IDS:
            return {
                ...state,
                selectedPlaylistItemIds: action.payload.playlistItemIds,
            };

        case OPEN_DELETE_PLAYLIST_ALERT:
            return {
                ...state,
                isDeletePlaylistAlertOpen: true,
            };
        case CLOSE_DELETE_PLAYLIST_ALERT:
            return {
                ...state,
                isDeletePlaylistAlertOpen: false,
                isDeletePlaylistSuccess: false,
            };
        case OPEN_DELETE_PLAYLIST_ITEM_ALERT:
            return {
                ...state,
                isDeletePlaylistItemAlertOpen: true,
                deletablePlaylistItemId: action.payload.playlistItemId,
            };
        case CLOSE_DELETE_PLAYLIST_ITEM_ALERT:
            return {
                ...state,
                isDeletePlaylistItemAlertOpen: false,
                isDeletePlaylistItemSuccess: false,
                deletablePlaylistItemId: '',
            };
        case SET_ACTIVE_PLAYLIST_ID:
            return {
                ...state,
                activePlaylistId: action.payload.playlistId,
            };
        case SET_ACTIVE_PLAYLIST_ITEM_ID:
            return {
                ...state,
                activePlaylistItemId: action.payload.playlistItemId,
            };

        case SET_PLAYLIST_DTO_INFO:
            return {
                ...state,
                playlistDto: { ...state.playlistDto, playlistDtoInfo: action.payload.playlistInfo },
            };

        case SET_PLAYLIST_DTO_FILTERING_TAGS: {
            return {
                ...state,
                playlistDto: { ...state.playlistDto, playlistDtoFilteringTags: action.payload.filteringTags },
            };
        }

        case SET_PLAYLIST_DTO_SCREENS:
            return {
                ...state,
                playlistDtoScreens: action.payload.screens,
            };
        case SET_PLAYLIST_DTO_PLAYLIST_ITEMS:
            return {
                ...state,
                playlistDto: { ...state.playlistDto, playlistDtoItems: action.payload.playlistItems },
            };
        case SET_PLAYLIST_DTO_SCHEDULES:
            return {
                ...state,
                playlistDto: { ...state.playlistDto, playlistDtoSchedules: action.payload.playlistSchedules },
            };

        case SET_PLAYLIST_DTO_SYNC:
            return {
                ...state,
                playlistDto: { ...state.playlistDto, playlistDtoSync: action.payload.playlistSync },
            };
        case SET_PLAYLIST_DTO_LOG:
            return {
                ...state,
                playlistDto: { ...state.playlistDto, playlistDtoLog: action.payload.playlistLog },
            };

        case GET_PLAYLISTS:
            return {
                ...state,
                loadingPlaylists: true,
                loadingPlaylistsError: null,
            };
        case GET_PLAYLISTS_SUCCESS:
            return {
                ...state,
                loadingPlaylists: false,
                playlists: action.payload.playlists,
            };
        case GET_PLAYLISTS_FAIL:
            return {
                ...state,
                loadingPlaylists: false,
                loadingPlaylistsError: action.payload.errors,
            };

        case OPEN_START_PLAYLIST_MODAL:
            return {
                ...state,
                isStartPlaylistModalOpen: true,
            };

        case CLOSE_START_PLAYLIST_MODAL:
            return {
                ...state,
                isStartPlaylistModalOpen: false,
            };

        case ADD_PLAYLIST:
            return {
                ...state,
                addingPlaylist: true,
                addPlaylistSuccess: false,
                addingPlaylistError: null,
            };
        case ADD_PLAYLIST_SUCCESS:
            return {
                ...state,
                isStartPlaylistModalOpen: false,
                playlistDtoScreens: [],
                playlistDto: {
                    playlistDtoInfo: { title: '', draft: false },
                    playlistDtoFilteringTags: [],
                    playlistDtoItems: [],
                    playlistDtoSchedules: [],
                    playlistDtoSync: false,
                    playlistDtoLog: true,
                },
                addPlaylistSuccess: action.payload,
                addingPlaylist: false,
            };
        case ADD_PLAYLIST_FAIL:
            return {
                ...state,
                addingPlaylist: false,
                addPlaylistSuccess: false,
                addingPlaylistError: action.payload.errors,
            };

        case UPDATE_PLAYLIST:
            return {
                ...state,
                updatingPlaylist: true,
                updatePlaylistSuccess: false,
                updatingPlaylistError: null,
            };
        case UPDATE_PLAYLIST_SUCCESS:
            return {
                ...state,
                updatePlaylistSuccess: true,
                playlistDtoScreens: [],
                playlistDto: {
                    playlistDtoInfo: { title: '', draft: false },
                    playlistDtoFilteringTags: [],
                    playlistDtoItems: [],
                    playlistDtoSchedules: [],
                    playlistDtoSync: false,
                    playlistDtoLog: true,
                },
                updatingPlaylist: false,
            };
        case UPDATE_PLAYLIST_FAIL:
            return {
                ...state,
                updatingPlaylist: false,
                updatePlaylistSuccess: false,
                updatingPlaylistError: action.payload.errors,
            };

        case UPDATE_PLAYLIST_TITLE:
            return {
                ...state,
                updatingPlaylistTitle: true,
                isUpdatePlaylistTitleSuccess: false,
                updatingPlaylistTitleError: null,
            };
        case UPDATE_PLAYLIST_TITLE_SUCCESS:
            return {
                ...state,
                isUpdatePlaylistTitleSuccess: true,
                updatingPlaylistTitle: false,
            };
        case UPDATE_PLAYLIST_TITLE_FAIL:
            return {
                ...state,
                updatingPlaylistTitle: false,
                isUpdatePlaylistTitleSuccess: false,
                updatingPlaylistTitleError: action.payload.errors,
            };

        case DELETE_PLAYLISTS:
            return {
                ...state,
                deletePlaylist: true,
                deletePlaylistError: null,
            };
        case DELETE_PLAYLISTS_SUCCESS:
            return {
                ...state,
                selectedPlaylistIds: [],
                isDeletePlaylistSuccess: true,
                deletePlaylist: false,
            };
        case DELETE_PLAYLISTS_FAIL:
            return {
                ...state,
                deletePlaylist: false,
                deletePlaylistError: action.payload.errors,
            };

        case GET_PLAYLIST_ITEMS:
            return {
                ...state,
                loadingPlaylistItems: true,
                loadingPlaylistItemsError: null,
            };
        case GET_PLAYLIST_ITEMS_SUCCESS:
            return {
                ...state,
                loadingPlaylistItems: false,
                playlistItems: action.payload.playlistItems,
                loadingPlaylistItemsError: null,
            };
        case GET_PLAYLIST_ITEMS_FAIL:
            return {
                ...state,
                loadingPlaylistItems: false,
                loadingPlaylistItemsError: action.payload.errors,
            };
        case UPDATE_PLAYLIST_ITEM_NAME:
            return {
                ...state,
                updatingPlaylistItemName: true,
                updatingPlaylistItemNameError: null,
            };
        case UPDATE_PLAYLIST_ITEM_NAME_SUCCESS:
            return {
                ...state,
                updatingPlaylistItemName: false,
            };
        case UPDATE_PLAYLIST_ITEM_NAME_FAIL:
            return {
                ...state,
                updatingPlaylistItemName: false,
                updatingPlaylistItemNameError: action.payload.errors,
            };

        case DELETE_PLAYLIST_ITEMS:
            return {
                ...state,
                deletePlaylistItem: true,
                deletePlaylistItemError: null,
            };
        case DELETE_PLAYLIST_ITEMS_SUCCESS:
            return {
                ...state,
                selectedPlaylistItemIds: [],
                isDeletePlaylistItemSuccess: true,
                deletePlaylistItem: false,
            };
        case DELETE_PLAYLIST_ITEMS_FAIL:
            return {
                ...state,
                deletePlaylistItem: false,
                deletePlaylistItemError: action.payload.errors,
            };
        case CLEAR_PLAYLISTS_SUCCESS:
            return {
                ...state,
                addPlaylistSuccess: false,
                updatePlaylistSuccess: false,
                isDeletePlaylistSuccess: false,
                isDeletePlaylistItemSuccess: false,
            };

        case SET_STEP_TWO_MENU_MODE:
            return {
                ...state,
                stepTwoMenuMode: action.payload.menuMode,
                stepTwoScenariosFilterOpen: false,
            };

        case SET_STEP_TWO_ACTIVE_PLAYLIST_ITEM_ID:
            return {
                ...state,
                stepTwoActivePlaylistItemId: action.payload.playlistItemId,
                stepTwoActiveScenarioId: '',
            };

        case SET_STEP_TWO_PLAYABLE_PLAYLIST_ITEM_ID:
            return {
                ...state,
                stepTwoPlayablePlaylistItemId: action.payload.playlistItemId,
            };

        case SET_STEP_TWO_ACTIVE_SCENARIO_ID:
            return {
                ...state,
                stepTwoActiveScenarioId: action.payload.scenarioId,
                stepTwoActivePlaylistItemId: '',
                stepTwoMenuMode: 'scenario_setting_menu',
            };

        case SET_STEP_TWO_SELECTED_PLAYLIST_ITEM_IDS:
            return {
                ...state,
                stepTwoSelectedPlaylistItemIds: action.payload.playlistItemIds,
                stepTwoScenariosFilterOpen: false,
            };

        case SET_STEP_TWO_SELECTED_CONTENT_ITEM_IDS:
            return {
                ...state,
                stepTwoSelectedContentItemIds: action.payload.contentItemIds,
                stepTwoScenariosFilterOpen: false,
            };

        case SET_STEP_TWO_SCENARIOS_FILTER_OPEN:
            return {
                ...state,
                stepTwoScenariosFilterOpen: action.payload.status,
            };

        case SET_STEP_TWO_SHOWED_SCENARIOS_IDS:
            return {
                ...state,
                stepTwoShowedScenariosIds: action.payload.scenarioIds,
            };

        case SET_STEP_TWO_PLAY_MODE:
            return {
                ...state,
                stepTwoPlayMode: action.payload.mode,
            };

        case SET_STEP_TWO_SLIDER_MOVING:
            return {
                ...state,
                stepTwoSliderMoving: action.payload.moving,
            };

        case SET_STEP_TWO_CURRENT_TIMELINE_SECONDS:
            return {
                ...state,
                stepTwoCurrentTimelineSeconds: action.payload.seconds,
            };
        case SET_TIME_LINE_REF:
            return {
                ...state,
                timeLineRef: action.payload.timeLineRef,
            };
        case SET_CONTENT_DISPLAY_TYPE:
            return {
                ...state,
                contentDisplayType: action.payload.contentDisplayType,
            };

        default:
            return state;
    }
}
