import { Observable, of } from 'rxjs';
import { combineEpics, ofType, StateObservable } from 'redux-observable';
import { catchError, map, mergeMap, withLatestFrom } from 'rxjs/operators';

import {
    fromGetPlaylists,
    fromGetPlaylistItems,
    fromDeletePlaylists,
    fromUpdatePlaylistTitle,
    fromDeletePlaylistItems,
    fromUpdatePlaylistItemName,
    fromAddPlaylist,
    fromUpdatePlaylist,
} from '../../../resolvers/playlistsResolvers';
import {
    DELETE_PLAYLISTS,
    DELETE_PLAYLIST_ITEMS,
    DELETE_PLAYLIST_ITEMS_SUCCESS,
    DELETE_PLAYLISTS_SUCCESS,
    GET_PLAYLIST_ITEMS,
    GET_PLAYLISTS,
    setDeletePlaylistsFail,
    setDeletePlaylistItemsFail,
    setDeletePlaylistItemsSuccess,
    setDeletePlaylistsSuccess,
    setGetPlaylistItemsFail,
    setGetPlaylistItemsSuccess,
    setGetPlaylists,
    setGetPlaylistsFail,
    setGetPlaylistsSuccess,
    setUpdatePlaylistTitleFail,
    setUpdatePlaylistItemNameFail,
    setUpdatePlaylistItemNameSuccess,
    setUpdatePlaylistTitleSuccess,
    UPDATE_PLAYLIST_TITLE,
    UPDATE_PLAYLIST_ITEM_NAME,
    UPDATE_PLAYLIST_ITEM_NAME_SUCCESS,
    UPDATE_PLAYLIST_TITLE_SUCCESS,
    ADD_PLAYLIST,
    setAddPlaylistSuccess,
    setAddPlaylistFail,
    UPDATE_PLAYLIST,
    setUpdatePlaylistSuccess,
    setUpdatePlaylistFail,
} from '../actions';
import {
    GetPlaylistsAction,
    GetPlaylistsFailAction,
    GetPlaylistsSuccessAction,
    GetPlaylistItemsAction,
    GetPlaylistItemsSuccessAction,
    GetPlaylistItemsFailAction,
    DeletePlaylistAction,
    DeletePlaylistSuccessAction,
    DeletePlaylistFailAction,
    UpdatePlaylistTitleAction,
    UpdatePlaylistTitleSuccessAction,
    UpdatePlaylistTitleFailAction,
    DeletePlaylistItemsAction,
    DeletePlaylistItemsSuccessAction,
    DeletePlaylistItemsFailAction,
    UpdatePlaylistItemNameAction,
    UpdatePlaylistItemNameSuccessAction,
    UpdatePlaylistItemNameFailAction,
    AddPlaylistAction,
    AddPlaylistSuccessAction,
    AddPlaylistFailAction,
    UpdatePlaylistAction,
    UpdatePlaylistSuccessAction,
    UpdatePlaylistFailAction,
} from '../types';
import { StoreType } from '../../../types';

const getPlaylistsEpic = (
    action$: Observable<GetPlaylistsAction>,
): Observable<GetPlaylistsSuccessAction | GetPlaylistsFailAction> =>
    action$.pipe(
        ofType(GET_PLAYLISTS),
        mergeMap(({ payload: { organizationId } }) =>
            fromGetPlaylists(organizationId).pipe(
                map(setGetPlaylistsSuccess),
                catchError((error) => of(setGetPlaylistsFail(error))),
            ),
        ),
    );

const addPlaylistEpic = (
    action$: Observable<AddPlaylistAction>,
): Observable<AddPlaylistSuccessAction | AddPlaylistFailAction> =>
    action$.pipe(
        ofType(ADD_PLAYLIST),
        mergeMap(({ payload: { organizationId, playlistDto } }) =>
            fromAddPlaylist(organizationId, playlistDto).pipe(
                map(({ id }: { id: string }) => setAddPlaylistSuccess(id, playlistDto)),
                catchError((error) => of(setAddPlaylistFail(error))),
            ),
        ),
    );

const updatePlaylistEpic = (
    action$: Observable<UpdatePlaylistAction>,
): Observable<UpdatePlaylistSuccessAction | UpdatePlaylistFailAction> =>
    action$.pipe(
        ofType(UPDATE_PLAYLIST),
        mergeMap(({ payload: { playlistId, playlistDto } }) =>
            fromUpdatePlaylist(playlistId, playlistDto).pipe(
                map(setUpdatePlaylistSuccess),
                catchError((error) => of(setUpdatePlaylistFail(error))),
            ),
        ),
    );

const updatePlaylistTitleEpic = (
    action$: Observable<UpdatePlaylistTitleAction>,
): Observable<UpdatePlaylistTitleSuccessAction | UpdatePlaylistTitleFailAction> =>
    action$.pipe(
        ofType(UPDATE_PLAYLIST_TITLE),
        mergeMap(({ payload: { playlistId, ...rest } }) =>
            fromUpdatePlaylistTitle({ playlistId, ...rest }).pipe(
                map(setUpdatePlaylistTitleSuccess),
                catchError((error) => of(setUpdatePlaylistTitleFail(error))),
            ),
        ),
    );

const deletePlaylistsEpic = (
    action$: Observable<DeletePlaylistAction>,
): Observable<DeletePlaylistSuccessAction | DeletePlaylistFailAction> =>
    action$.pipe(
        ofType(DELETE_PLAYLISTS),
        mergeMap(({ payload: { playlistIds } }) =>
            fromDeletePlaylists(playlistIds).pipe(
                map(setDeletePlaylistsSuccess),
                catchError((error) => of(setDeletePlaylistsFail(error))),
            ),
        ),
    );

const playlistsSuccessEpic = (
    action$: Observable<
        | UpdatePlaylistTitleSuccessAction
        | DeletePlaylistSuccessAction
        | UpdatePlaylistItemNameSuccessAction
        | DeletePlaylistItemsSuccessAction
    >,
    state$: StateObservable<StoreType>,
): Observable<GetPlaylistsAction> =>
    action$.pipe(
        ofType(
            UPDATE_PLAYLIST_TITLE_SUCCESS,
            DELETE_PLAYLISTS_SUCCESS,
            UPDATE_PLAYLIST_ITEM_NAME_SUCCESS,
            DELETE_PLAYLIST_ITEMS_SUCCESS,
        ),
        withLatestFrom(state$),
        map(
            ([
                ,
                {
                    pages: {
                        organization: {
                            profile: {
                                organization: { id },
                            },
                        },
                    },
                },
            ]) => setGetPlaylists(id),
        ),
    );

const getPlaylistItemsEpic = (
    action$: Observable<GetPlaylistItemsAction>,
): Observable<GetPlaylistItemsSuccessAction | GetPlaylistItemsFailAction> =>
    action$.pipe(
        ofType(GET_PLAYLIST_ITEMS),
        mergeMap(({ payload: { playlistId } }) =>
            fromGetPlaylistItems(playlistId).pipe(
                map(setGetPlaylistItemsSuccess),
                catchError((error) => of(setGetPlaylistItemsFail(error))),
            ),
        ),
    );

const updatePlaylistItemNameEpic = (
    action$: Observable<UpdatePlaylistItemNameAction>,
): Observable<UpdatePlaylistItemNameSuccessAction | UpdatePlaylistItemNameFailAction> =>
    action$.pipe(
        ofType(UPDATE_PLAYLIST_ITEM_NAME),
        mergeMap(({ payload: { playlistItemId, name } }) =>
            fromUpdatePlaylistItemName({ playlistItemId, name }).pipe(
                map(setUpdatePlaylistItemNameSuccess),
                catchError((error) => of(setUpdatePlaylistItemNameFail(error))),
            ),
        ),
    );

const deletePlaylistItemsEpic = (
    action$: Observable<DeletePlaylistItemsAction>,
): Observable<DeletePlaylistItemsSuccessAction | DeletePlaylistItemsFailAction> =>
    action$.pipe(
        ofType(DELETE_PLAYLIST_ITEMS),
        mergeMap(({ payload: { playlistItemIds } }) =>
            fromDeletePlaylistItems(playlistItemIds).pipe(
                map(setDeletePlaylistItemsSuccess),
                catchError((error) => of(setDeletePlaylistItemsFail(error))),
            ),
        ),
    );

type EpicsActions =
    | GetPlaylistsAction
    | GetPlaylistsSuccessAction
    | GetPlaylistsFailAction
    | AddPlaylistAction
    | AddPlaylistSuccessAction
    | AddPlaylistFailAction
    | UpdatePlaylistAction
    | UpdatePlaylistSuccessAction
    | UpdatePlaylistFailAction
    | UpdatePlaylistTitleAction
    | UpdatePlaylistTitleSuccessAction
    | UpdatePlaylistTitleFailAction
    | DeletePlaylistAction
    | DeletePlaylistSuccessAction
    | DeletePlaylistFailAction
    | UpdatePlaylistItemNameSuccessAction
    | DeletePlaylistItemsSuccessAction
    | GetPlaylistItemsAction
    | GetPlaylistItemsSuccessAction
    | GetPlaylistItemsFailAction
    | UpdatePlaylistItemNameAction
    | UpdatePlaylistItemNameFailAction
    | DeletePlaylistItemsAction
    | DeletePlaylistItemsFailAction;

export const playlistsEpics = combineEpics<EpicsActions, EpicsActions, StoreType>(
    getPlaylistsEpic,
    addPlaylistEpic,
    updatePlaylistEpic,
    updatePlaylistTitleEpic,
    deletePlaylistsEpic,
    playlistsSuccessEpic,

    getPlaylistItemsEpic,
    updatePlaylistItemNameEpic,
    deletePlaylistItemsEpic,
);
