import React from 'react';

import { AddIcon } from '../../ui-kit';
import PlaylistsList from './components/PlaylistsList';
import RightPlaylistsMenu from './components/RightPlaylistsMenu';
import PlaylistStepsContainer from './components/PlaylistStepsContainer';
import { usePlaylistsPage } from './hooks/usePlaylistsPage';
import { useUploadingPlaylistSnackbar } from './hooks/useUploadingPlaylistSnackbar';
import { PriorityButton } from './components/PriorityButton';

import css from './style.m.css';

const PlaylistsPage: React.FC = () => {
    const { isMoreThanOnePlaylist, startCreatingPlaylist, isPlaylistStepMode, tPlaylists } = usePlaylistsPage();

    useUploadingPlaylistSnackbar();

    return (
        <div className={css.mainPlaylists}>
            <div className={css.centerBlockPlaylist}>
                {!isPlaylistStepMode && (
                    <div style={{ display: 'flex', justifyContent: 'right' }}>
                        {isMoreThanOnePlaylist && (
                            <div className={css.mainBlockScreen}>
                                <PriorityButton />
                            </div>
                        )}
                        <div className={css.mainBlockScreen}>
                            <div className={css.fieldBtnBlockScreen}>
                                <button className={css.btnBlockScreen} onClick={startCreatingPlaylist}>
                                    <AddIcon />
                                    {tPlaylists('btnAddPlaylist')}
                                </button>
                            </div>
                        </div>
                    </div>
                )}
                <div className={css.mainPlaylistsPage}>
                    <div className={css.mainPlaylistBlocks}>
                        {isPlaylistStepMode ? <PlaylistStepsContainer /> : <PlaylistsList />}
                    </div>
                </div>
            </div>
            {!isPlaylistStepMode && <RightPlaylistsMenu />}
        </div>
    );
};
export default PlaylistsPage;
