import {
    UpdatePlaylistItemNameDto,
    UpdatePlaylistTitleDto,
    UpdatePlaylistDtoClient,
    CreatePlaylistDtoClient,
    StepTwoMenuMode,
    StepTwoPlayMode,
    PlaylistDtoScreen,
    ContentDisplayType,
} from '../types';
import { ErrorResponse } from '../../../types';
import { PlaylistDtoInfo, PlaylistItemType, PlaylistType } from '../../../../../common/types';
import { CreatePlaylistScheduleDto } from '../../../../../server/src/models/playlist-schedule/dto/create-playlist-schedule.dto';

export const SET_SELECTED_PLAYLIST_IDS = 'SET_SELECTED_PLAYLIST_IDS';
export const SET_SELECTED_PLAYLIST_ITEM_IDS = 'SET_SELECTED_PLAYLIST_ITEM_IDS';

export const OPEN_DELETE_PLAYLIST_ALERT = 'OPEN_DELETE_PLAYLIST_ALERT';
export const CLOSE_DELETE_PLAYLIST_ALERT = 'CLOSE_DELETE_PLAYLIST_ALERT';

export const OPEN_DELETE_PLAYLIST_ITEM_ALERT = 'OPEN_DELETE_PLAYLIST_ITEM_ALERT';
export const CLOSE_DELETE_PLAYLIST_ITEM_ALERT = 'CLOSE_DELETE_PLAYLIST_ITEM_ALERT';

export const OPEN_START_PLAYLIST_MODAL = 'OPEN_START_PLAYLIST_MODAL';
export const CLOSE_START_PLAYLIST_MODAL = 'CLOSE_START_PLAYLIST_MODAL';

export const SET_ACTIVE_PLAYLIST_ID = 'SET_ACTIVE_PLAYLIST_ID';
export const SET_ACTIVE_PLAYLIST_ITEM_ID = 'SET_ACTIVE_PLAYLIST_ITEM_ID';

export const GET_PLAYLISTS = 'GET_PLAYLISTS';
export const GET_PLAYLISTS_SUCCESS = 'GET_PLAYLISTS_SUCCESS';
export const GET_PLAYLISTS_FAIL = 'GET_PLAYLISTS_FAIL';

export const SET_PLAYLIST_DTO_INFO = 'SET_PLAYLIST_DTO_INFO';
export const SET_PLAYLIST_DTO_FILTERING_TAGS = 'SET_PLAYLIST_DTO_FILTERING_TAGS';
export const SET_PLAYLIST_DTO_SCREENS = 'SET_PLAYLIST_DTO_SCREENS';
export const SET_PLAYLIST_DTO_PLAYLIST_ITEMS = 'SET_PLAYLIST_DTO_PLAYLIST_ITEMS';
export const SET_PLAYLIST_DTO_SCHEDULES = 'SET_PLAYLIST_DTO_SCHEDULES';
export const SET_PLAYLIST_DTO_SYNC = 'SET_PLAYLIST_DTO_SYNC';
export const SET_PLAYLIST_DTO_LOG = 'SET_PLAYLIST_DTO_LOG';

export const SET_STEP_TWO_MENU_MODE = 'SET_STEP_TWO_MENU_MODE';
export const SET_STEP_TWO_ACTIVE_PLAYLIST_ITEM_ID = 'SET_STEP_TWO_ACTIVE_PLAYLIST_ITEM_ID';
export const SET_STEP_TWO_PLAYABLE_PLAYLIST_ITEM_ID = 'SET_STEP_TWO_PLAYABLE_PLAYLIST_ITEM_ID';
export const SET_STEP_TWO_ACTIVE_SCENARIO_ID = 'SET_STEP_TWO_ACTIVE_SCENARIO_ID';
export const SET_STEP_TWO_SELECTED_PLAYLIST_ITEM_IDS = 'SET_STEP_TWO_SELECTED_PLAYLIST_ITEM_IDS';
export const SET_STEP_TWO_SELECTED_CONTENT_ITEM_IDS = 'SET_STEP_TWO_SELECTED_CONTENT_ITEM_IDS';
export const SET_STEP_TWO_SCENARIOS_FILTER_OPEN = 'SET_STEP_TWO_SCENARIOS_FILTER_OPEN';
export const SET_STEP_TWO_SHOWED_SCENARIOS_IDS = 'SET_STEP_TWO_SHOWED_SCENARIOS_IDS';

export const SET_STEP_TWO_PLAY_MODE = 'SET_STEP_TWO_PLAY_MODE';
export const SET_STEP_TWO_SLIDER_MOVING = 'SET_STEP_TWO_SLIDER_MOVING';
export const SET_STEP_TWO_CURRENT_TIMELINE_SECONDS = 'SET_STEP_TWO_CURRENT_TIMELINE_SECONDS';

export const ADD_PLAYLIST = 'ADD_PLAYLIST';
export const ADD_PLAYLIST_SUCCESS = 'ADD_PLAYLIST_SUCCESS';
export const ADD_PLAYLIST_FAIL = 'ADD_PLAYLIST_FAIL';

export const UPDATE_PLAYLIST = 'UPDATE_PLAYLIST';
export const UPDATE_PLAYLIST_SUCCESS = 'UPDATE_PLAYLIST_SUCCESS';
export const UPDATE_PLAYLIST_FAIL = 'UPDATE_PLAYLIST_FAIL';

export const UPDATE_PLAYLIST_TITLE = 'UPDATE_PLAYLIST_TITLE';
export const UPDATE_PLAYLIST_TITLE_SUCCESS = 'UPDATE_PLAYLIST_TITLE_SUCCESS';
export const UPDATE_PLAYLIST_TITLE_FAIL = 'UPDATE_PLAYLIST_TITLE_FAIL';

export const DELETE_PLAYLISTS = 'DELETE_PLAYLISTS';
export const DELETE_PLAYLISTS_SUCCESS = 'DELETE_PLAYLISTS_SUCCESS';
export const DELETE_PLAYLISTS_FAIL = 'DELETE_PLAYLISTS_FAIL';

export const GET_PLAYLIST_ITEMS = 'GET_PLAYLIST_ITEMS';
export const GET_PLAYLIST_ITEMS_SUCCESS = 'GET_PLAYLIST_ITEMS_SUCCESS';
export const GET_PLAYLIST_ITEMS_FAIL = 'GET_PLAYLIST_ITEMS_FAIL';

export const UPDATE_PLAYLIST_ITEM_NAME = 'UPDATE_PLAYLIST_ITEM_NAME';
export const UPDATE_PLAYLIST_ITEM_NAME_SUCCESS = 'UPDATE_PLAYLIST_ITEM_NAME_SUCCESS';
export const UPDATE_PLAYLIST_ITEM_NAME_FAIL = 'UPDATE_PLAYLIST_ITEM_NAME_FAIL';

export const DELETE_PLAYLIST_ITEMS = 'DELETE_PLAYLIST_ITEMS';
export const DELETE_PLAYLIST_ITEMS_SUCCESS = 'DELETE_PLAYLIST_ITEMS_SUCCESS';
export const DELETE_PLAYLIST_ITEMS_FAIL = 'DELETE_PLAYLIST_ITEMS_FAIL';

export const CLEAR_PLAYLISTS_SUCCESS = 'CLEAR_PLAYLISTS_SUCCESS';

export const SET_TIME_LINE_REF = 'SET_TIME_LINE_REF';
export const SET_CONTENT_DISPLAY_TYPE = 'SET_CONTENT_DISPLAY_TYPE';

export const setSelectedPlaylistIds = (playlistIds: string[] = []) =>
    ({ type: SET_SELECTED_PLAYLIST_IDS, payload: { playlistIds } }) as const;
export const setSelectedPlaylistItemIds = (playlistItemIds: string[] = []) =>
    ({ type: SET_SELECTED_PLAYLIST_ITEM_IDS, payload: { playlistItemIds } }) as const;

export const setOpenDeletePlaylistAlert = () => ({ type: OPEN_DELETE_PLAYLIST_ALERT }) as const;
export const setCloseDeletePlaylistAlert = () => ({ type: CLOSE_DELETE_PLAYLIST_ALERT }) as const;

export const setOpenDeletePlaylistItemAlert = (playlistItemId = '') =>
    ({ type: OPEN_DELETE_PLAYLIST_ITEM_ALERT, payload: { playlistItemId } }) as const;
export const setCloseDeletePlaylistItemAlert = () => ({ type: CLOSE_DELETE_PLAYLIST_ITEM_ALERT }) as const;

export const setActivePlaylistId = (playlistId = '') =>
    ({ type: SET_ACTIVE_PLAYLIST_ID, payload: { playlistId } }) as const;
export const setActivePlaylistItemId = (playlistItemId = '') =>
    ({ type: SET_ACTIVE_PLAYLIST_ITEM_ID, payload: { playlistItemId } }) as const;

// playlists

export const setGetPlaylists = (organizationId: string) =>
    ({ type: GET_PLAYLISTS, payload: { organizationId } }) as const;
export const setGetPlaylistsSuccess = (playlists: PlaylistType[]) =>
    ({ type: GET_PLAYLISTS_SUCCESS, payload: { playlists } }) as const;
export const setGetPlaylistsFail = (errors: ErrorResponse) =>
    ({ type: GET_PLAYLISTS_FAIL, payload: { errors } }) as const;

export const setPlaylistDtoInfo = (playlistInfo: PlaylistDtoInfo = { title: '', draft: false }) =>
    ({ type: SET_PLAYLIST_DTO_INFO, payload: { playlistInfo } }) as const;
export const setPlaylistDtoFilteringTags = (filteringTags: string[] = []) =>
    ({ type: SET_PLAYLIST_DTO_FILTERING_TAGS, payload: { filteringTags } }) as const;
export const setPlaylistDtoScreens = (screens: PlaylistDtoScreen[] = []) =>
    ({ type: SET_PLAYLIST_DTO_SCREENS, payload: { screens } }) as const;
export const setPlaylistDtoPlaylistItems = (playlistItems: PlaylistItemType[] = []) =>
    ({ type: SET_PLAYLIST_DTO_PLAYLIST_ITEMS, payload: { playlistItems } }) as const;
export const setPlaylistDtoSchedules = (playlistSchedules: CreatePlaylistScheduleDto[] = []) =>
    ({ type: SET_PLAYLIST_DTO_SCHEDULES, payload: { playlistSchedules } }) as const;
export const setPlaylistDtoSync = (playlistSync: boolean) =>
    ({ type: SET_PLAYLIST_DTO_SYNC, payload: { playlistSync } }) as const;
export const setPlaylistDtoLog = (playlistLog: boolean) =>
    ({ type: SET_PLAYLIST_DTO_LOG, payload: { playlistLog } }) as const;

export const setOpenStartPlaylistModal = () => ({ type: OPEN_START_PLAYLIST_MODAL }) as const;
export const setCloseStartPlaylistModal = () => ({ type: CLOSE_START_PLAYLIST_MODAL }) as const;

export const setAddPlaylist = (organizationId: string, playlistDto: CreatePlaylistDtoClient) =>
    ({ type: ADD_PLAYLIST, payload: { organizationId, playlistDto } }) as const;
export const setAddPlaylistSuccess = (id: string, playlistDto: CreatePlaylistDtoClient) =>
    ({ type: ADD_PLAYLIST_SUCCESS, payload: { id, ...playlistDto } }) as const;
export const setAddPlaylistFail = (errors: ErrorResponse) =>
    ({ type: ADD_PLAYLIST_FAIL, payload: { errors } }) as const;

export const setUpdatePlaylist = (playlistId: string, playlistDto: UpdatePlaylistDtoClient) =>
    ({ type: UPDATE_PLAYLIST, payload: { playlistId, playlistDto } }) as const;
export const setUpdatePlaylistSuccess = () => ({ type: UPDATE_PLAYLIST_SUCCESS }) as const;
export const setUpdatePlaylistFail = (errors: ErrorResponse) =>
    ({ type: UPDATE_PLAYLIST_FAIL, payload: { errors } }) as const;

export const setUpdatePlaylistTitle = ({ playlistId, ...rest }: UpdatePlaylistTitleDto) =>
    ({ type: UPDATE_PLAYLIST_TITLE, payload: { playlistId, ...rest } }) as const;
export const setUpdatePlaylistTitleSuccess = () => ({ type: UPDATE_PLAYLIST_TITLE_SUCCESS }) as const;
export const setUpdatePlaylistTitleFail = (errors: ErrorResponse) =>
    ({ type: UPDATE_PLAYLIST_TITLE_FAIL, payload: { errors } }) as const;

export const setDeletePlaylists = (playlistIds: string | string[]) =>
    ({ type: DELETE_PLAYLISTS, payload: { playlistIds } }) as const;
export const setDeletePlaylistsSuccess = () => ({ type: DELETE_PLAYLISTS_SUCCESS }) as const;
export const setDeletePlaylistsFail = (errors: ErrorResponse) =>
    ({ type: DELETE_PLAYLISTS_FAIL, payload: { errors } }) as const;

// playlistItems

export const setGetPlaylistItems = (playlistId: string) =>
    ({ type: GET_PLAYLIST_ITEMS, payload: { playlistId } }) as const;
export const setGetPlaylistItemsSuccess = (playlistItems: PlaylistItemType[]) =>
    ({ type: GET_PLAYLIST_ITEMS_SUCCESS, payload: { playlistItems } }) as const;
export const setGetPlaylistItemsFail = (errors: ErrorResponse) =>
    ({ type: GET_PLAYLIST_ITEMS_FAIL, payload: { errors } }) as const;

export const setUpdatePlaylistItemName = ({ playlistItemId, name }: UpdatePlaylistItemNameDto) =>
    ({ type: UPDATE_PLAYLIST_ITEM_NAME, payload: { playlistItemId, name } }) as const;
export const setUpdatePlaylistItemNameSuccess = () => ({ type: UPDATE_PLAYLIST_ITEM_NAME_SUCCESS }) as const;
export const setUpdatePlaylistItemNameFail = (errors: ErrorResponse) =>
    ({ type: UPDATE_PLAYLIST_ITEM_NAME_FAIL, payload: { errors } }) as const;

export const setDeletePlaylistItems = (playlistItemIds: string | string[]) =>
    ({ type: DELETE_PLAYLIST_ITEMS, payload: { playlistItemIds } }) as const;
export const setDeletePlaylistItemsSuccess = () => ({ type: DELETE_PLAYLIST_ITEMS_SUCCESS }) as const;
export const setDeletePlaylistItemsFail = (errors: ErrorResponse) =>
    ({ type: DELETE_PLAYLIST_ITEMS_FAIL, payload: { errors } }) as const;

export const setClearPlaylistsSuccess = () => ({ type: CLEAR_PLAYLISTS_SUCCESS }) as const;

export const setStepTwoMenuMode = (menuMode: StepTwoMenuMode) =>
    ({ type: SET_STEP_TWO_MENU_MODE, payload: { menuMode } }) as const;
export const setStepTwoActivePlaylistItemId = (playlistItemId = '') =>
    ({ type: SET_STEP_TWO_ACTIVE_PLAYLIST_ITEM_ID, payload: { playlistItemId } }) as const;
export const setStepTwoPlayablePlaylistItemId = (playlistItemId = '') =>
    ({ type: SET_STEP_TWO_PLAYABLE_PLAYLIST_ITEM_ID, payload: { playlistItemId } }) as const;
export const setStepTwoActiveScenarioId = (scenarioId = '') =>
    ({ type: SET_STEP_TWO_ACTIVE_SCENARIO_ID, payload: { scenarioId } }) as const;
export const setStepTwoSelectedPlaylistItemIds = (playlistItemIds: string[] = []) =>
    ({ type: SET_STEP_TWO_SELECTED_PLAYLIST_ITEM_IDS, payload: { playlistItemIds } }) as const;
export const setStepTwoSelectedContentItemIds = (contentItemIds: string[] = []) =>
    ({ type: SET_STEP_TWO_SELECTED_CONTENT_ITEM_IDS, payload: { contentItemIds } }) as const;
export const setStepTwoScenarioFilterOpen = (status: boolean) =>
    ({ type: SET_STEP_TWO_SCENARIOS_FILTER_OPEN, payload: { status } }) as const;
export const setStepTwoShowedScenariosIds = (scenarioIds: string[]) =>
    ({ type: SET_STEP_TWO_SHOWED_SCENARIOS_IDS, payload: { scenarioIds } }) as const;

export const setStepTwoPlayMode = (mode: StepTwoPlayMode) =>
    ({ type: SET_STEP_TWO_PLAY_MODE, payload: { mode } }) as const;
export const setStepTwoSliderMoving = (moving: boolean) =>
    ({ type: SET_STEP_TWO_SLIDER_MOVING, payload: { moving } }) as const;
export const setStepTwoCurrentTimelineSeconds = (seconds: number) =>
    ({ type: SET_STEP_TWO_CURRENT_TIMELINE_SECONDS, payload: { seconds } }) as const;

export const setTimeLineRef = (timeLineRef: HTMLDivElement | null) =>
    ({ type: SET_TIME_LINE_REF, payload: { timeLineRef } }) as const;
export const setContentDisplayType = (contentDisplayType: ContentDisplayType) =>
    ({ type: SET_CONTENT_DISPLAY_TYPE, payload: { contentDisplayType } }) as const;
