import { Dispatch, SetStateAction, useEffect, useMemo, useState } from 'react';
import {
    differenceInDays,
    differenceInWeeks,
    differenceInMonths,
    differenceInYears,
    isSameDay,
    differenceInCalendarDays,
} from 'date-fns';
import { useTranslation } from 'react-i18next';

export type TimeFrequency =
    | 'Без повтора'
    | 'Каждый день'
    | 'Каждую неделю'
    | 'Каждый месяц'
    | 'Каждый год'
    | 'No repeat'
    | 'Every day'
    | 'Every week'
    | 'Every month'
    | 'Every year'
    | '';

type Props = {
    startDay: string;
    endDay: string;
    broadcastAlways?: boolean;
    initialFrequencyValue?: TimeFrequency | '';
};

type ReturnValue = {
    frequencyValue: TimeFrequency | '';
    frequencyValues: TimeFrequency[];
    setFrequencyValue: Dispatch<SetStateAction<TimeFrequency>>;
    isEveryDayFrequency: boolean;
    isEveryMonthYearFrequency: boolean;
    isEveryMonthFrequencyError: boolean;
    isEveryYearFrequencyError: boolean;
    isStartAndEndDaysEqual: boolean;
    isLessThanWeekPeriod: boolean;
};

export const useFrequencySelectValues = ({
    startDay,
    endDay,
    broadcastAlways = false,
    initialFrequencyValue,
}: Props): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const isEndDateLessThanStartDate = useMemo(
        () => differenceInCalendarDays(new Date(startDay), new Date(endDay)) > 0,
        [endDay, startDay],
    );

    const isStartAndEndDaysEqual = useMemo(
        () => !broadcastAlways && isSameDay(new Date(startDay), new Date(endDay)),
        [broadcastAlways, endDay, startDay],
    );

    const isLessThanWeekPeriod = useMemo(
        () =>
            differenceInDays(new Date(endDay), new Date(startDay)) > 0 &&
            differenceInWeeks(new Date(endDay), new Date(startDay)) < 1,
        [endDay, startDay],
    );

    const isLessThanMonthPeriod = useMemo(
        () =>
            differenceInWeeks(new Date(endDay), new Date(startDay)) > 0 &&
            differenceInMonths(new Date(endDay), new Date(startDay)) < 1,
        [endDay, startDay],
    );

    const isLessThanYearPeriod = useMemo(
        () =>
            differenceInMonths(new Date(endDay), new Date(startDay)) > 0 &&
            differenceInYears(new Date(endDay), new Date(startDay)) < 1,
        [endDay, startDay],
    );

    const isMoreThanYearPeriod = useMemo(
        () => differenceInYears(new Date(endDay), new Date(startDay)) > 0,
        [endDay, startDay],
    );

    const [frequencyValue, setFrequencyValue] = useState<TimeFrequency | ''>('');
    const [frequencyValues, setFrequencyValues] = useState<TimeFrequency[]>([]);

    // меняем список в селекте в зависимости от условий
    useEffect(() => {
        const allFrequencies = [t('everyDay'), t('everyWeek'), t('everyMonth'), t('everyYear')];
        const noRepeatFrequency = [t('noRepeat')];

        switch (true) {
            case broadcastAlways:
            case isMoreThanYearPeriod:
                setFrequencyValues(allFrequencies as TimeFrequency[]);
                setFrequencyValue(
                    initialFrequencyValue && allFrequencies.includes(initialFrequencyValue)
                        ? initialFrequencyValue
                        : (t('everyDay') as TimeFrequency),
                );
                break;
            case isEndDateLessThanStartDate:
            case isStartAndEndDaysEqual:
                setFrequencyValues(noRepeatFrequency as TimeFrequency[]);
                setFrequencyValue(t('noRepeat') as TimeFrequency);
                break;
            case isLessThanWeekPeriod:
                setFrequencyValues(allFrequencies.slice(0, 1) as TimeFrequency[]);
                setFrequencyValue(t('everyDay') as TimeFrequency);
                break;
            case isLessThanMonthPeriod:
                setFrequencyValues(allFrequencies.slice(0, 2) as TimeFrequency[]);
                setFrequencyValue(
                    initialFrequencyValue && allFrequencies.slice(0, 2).includes(initialFrequencyValue)
                        ? initialFrequencyValue
                        : (t('everyDay') as TimeFrequency),
                );
                break;
            case isLessThanYearPeriod:
                setFrequencyValues(allFrequencies.slice(0, 3) as TimeFrequency[]);
                setFrequencyValue(
                    initialFrequencyValue && allFrequencies.slice(0, 3).includes(initialFrequencyValue)
                        ? initialFrequencyValue
                        : (t('everyDay') as TimeFrequency),
                );
                break;

            default:
                setFrequencyValues([]);
                setFrequencyValue('');
        }
    }, [
        broadcastAlways,
        initialFrequencyValue,
        isEndDateLessThanStartDate,
        isLessThanMonthPeriod,
        isLessThanWeekPeriod,
        isLessThanYearPeriod,
        isMoreThanYearPeriod,
        isStartAndEndDaysEqual,
        t,
    ]);

    const isEveryDayFrequency = useMemo(() => frequencyValue.includes(t('everyDay')), [frequencyValue, t]);

    const isEveryMonthYearFrequency = useMemo(
        () => frequencyValue.includes(t('everyMonth')) || frequencyValue.includes(t('everyYear')),
        [frequencyValue, t],
    );

    const { startDateDay, startDayMonth } = useMemo(
        () => ({
            startDateDay: startDay.slice(startDay.length - 2),
            startDayMonth: startDay.slice(5, startDay.length - 3),
        }),
        [startDay],
    );

    const isEveryMonthFrequencyError = useMemo(
        () => frequencyValue.includes(t('everyMonth')) && startDateDay === '31',
        [frequencyValue, startDateDay, t],
    );

    const isEveryYearFrequencyError = useMemo(
        () => frequencyValue.includes(t('everyYear')) && startDateDay === '29' && startDayMonth === '02',
        [frequencyValue, startDateDay, startDayMonth, t],
    );

    return {
        frequencyValue,
        frequencyValues,
        setFrequencyValue,
        isEveryDayFrequency,
        isEveryMonthYearFrequency,
        isEveryMonthFrequencyError,
        isEveryYearFrequencyError,
        isStartAndEndDaysEqual,
        isLessThanWeekPeriod,
    };
};
