import { useCallback, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setPlaylistDtoScreens } from '../actions';
import { selectScreens } from '../../ScreensPage/selectors';
import { selectPlaylistDtoScreensOrdered, selectPlaylistDtoScreens } from '../selectors';
import { ScreenType } from '../../../../../common/types';
import { usePlaylistStep } from './usePlaylistStep';
import { useCustomRouter } from '../../../hooks/useCustomRouter';

type ReturnValue = {
    selectedScreens: ScreenType[];
    unselectAllScreens: () => void;
    unselectScreen: (screenId: string) => () => void;
    moveToPlaylistStepTwo: () => void;

    isBroadcastBreakAlertShown: boolean;
    onSubmitBroadcastBreakAlert: () => void;
    onCancelBroadcastBreakAlert: () => void;
    unselectedScreensNumber: number;

    t: TFunction;
};

export const useStepOneMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const { playlistStep, isEditPlaylistStep } = usePlaylistStep();

    const { pushToPage } = useCustomRouter();

    const screens = useSelector(selectScreens);
    const playlistDtoScreens = useSelector(selectPlaylistDtoScreens);
    const playlistDtoScreensOrdered = useSelector(selectPlaylistDtoScreensOrdered);

    const [isBroadcastBreakAlertShown, setIsBroadcastBreakAlertShown] = useState(false);
    const [unselectableScreenIds, setUnselectableScreenIds] = useState<string[]>([]);

    const unselectedScreensNumber = unselectableScreenIds.length;

    const onSubmitBroadcastBreakAlert = useCallback(() => {
        if (unselectableScreenIds.length > 1) {
            dispatch(setPlaylistDtoScreens());
        } else {
            dispatch(
                setPlaylistDtoScreens(playlistDtoScreensOrdered.filter(({ id }) => id !== unselectableScreenIds[0])),
            );
        }

        setIsBroadcastBreakAlertShown(false);
    }, [dispatch, playlistDtoScreensOrdered, unselectableScreenIds]);

    const onCancelBroadcastBreakAlert = useCallback(() => {
        setUnselectableScreenIds([]);
        setIsBroadcastBreakAlertShown(false);
    }, []);

    const selectedScreens = useMemo(
        () => playlistDtoScreens.map((screenId) => screens.find(({ id }) => id === screenId)) as ScreenType[],
        [playlistDtoScreens, screens],
    );

    const unselectAllScreens = useCallback(() => {
        if (isEditPlaylistStep) {
            setUnselectableScreenIds(playlistDtoScreens);
            return setIsBroadcastBreakAlertShown(true);
        }

        dispatch(setPlaylistDtoScreens());
    }, [dispatch, isEditPlaylistStep, playlistDtoScreens]);

    const unselectScreen = useCallback(
        (screenId: string) => () => {
            if (isEditPlaylistStep) {
                setUnselectableScreenIds([screenId]);
                return setIsBroadcastBreakAlertShown(true);
            }

            dispatch(setPlaylistDtoScreens(playlistDtoScreensOrdered.filter(({ id }) => id !== screenId)));
        },
        [dispatch, isEditPlaylistStep, playlistDtoScreensOrdered],
    );

    const moveToPlaylistStepTwo = useCallback(() => {
        pushToPage(playlistStep === 'creating_one' ? 'playlists/creating_two' : 'playlists/editing_two');
    }, [playlistStep, pushToPage]);

    return {
        selectedScreens,
        unselectAllScreens,
        unselectScreen,
        moveToPlaylistStepTwo,

        isBroadcastBreakAlertShown,
        onSubmitBroadcastBreakAlert,
        onCancelBroadcastBreakAlert,
        unselectedScreensNumber,
        t,
    };
};
