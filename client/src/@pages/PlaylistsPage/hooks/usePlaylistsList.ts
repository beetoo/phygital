import { useCallback, useEffect, useMemo, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { PlaylistType } from '../../../../../common/types';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { setGetScreens } from '../../ScreensPage/actions';
import { selectScreens } from '../../ScreensPage/selectors';
import {
    setActivePlaylistId,
    setActivePlaylistItemId,
    setClearPlaylistsSuccess,
    setGetPlaylists,
    setPlaylistDtoInfo,
    setPlaylistDtoPlaylistItems,
    setPlaylistDtoScreens,
    setSelectedPlaylistIds,
} from '../actions';
import {
    selectActivePlaylistId,
    selectAddPlaylistSuccessStatus,
    selectPlaylists,
    selectSelectedPlaylistIds,
    selectSelectedPlaylistItemIds,
} from '../selectors';
import { selectAuthUserRole } from '../../../selectors';
import { USER_ROLE } from '../../../constants';
import { useCustomRouter } from '../../../hooks/useCustomRouter';

type ReturnValue = {
    playlists: PlaylistType[];
    startCreatingPlaylist: () => void;
    isAllPlaylistsSelected: boolean;
    selectAllPlaylists: () => void;
    unselectAllPlaylists: () => void;
    isPlaylistSelectDisabled: boolean;
    t: TFunction;
};

export const usePlaylistsList = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlists' });

    const dispatch = useDispatch();

    const { pushToPage } = useCustomRouter();

    const { id: organizationId, language } = useSelector(selectCurrentOrganization);
    const userRole = useSelector(selectAuthUserRole);
    const playlists = useSelector(selectPlaylists);

    const activePlaylistId = useSelector(selectActivePlaylistId);
    const screens = useSelector(selectScreens);
    const isAddPlaylistSuccess = useSelector(selectAddPlaylistSuccessStatus);

    const selectedPlaylistIds = useSelector(selectSelectedPlaylistIds);
    const selectedPlaylistItemIds = useSelector(selectSelectedPlaylistItemIds);

    const selectedPlaylists = useMemo(
        () => playlists.filter(({ id }) => selectedPlaylistIds.includes(id)),
        [playlists, selectedPlaylistIds],
    );

    useEffect(() => {
        if (organizationId) {
            dispatch(setGetPlaylists(organizationId));
        }
    }, [dispatch, organizationId]);

    useEffect(() => {
        if (organizationId && screens.length === 0) {
            dispatch(setGetScreens(organizationId, 'all', userRole === USER_ROLE.ADMIN));
        }
    }, [dispatch, organizationId, screens.length, userRole]);

    useEffect(() => {
        if (playlists.length > 0 && !activePlaylistId) {
            dispatch(setActivePlaylistId(playlists[0].id));
        }
    }, [activePlaylistId, dispatch, playlists]);

    const playlistLengthRef = useRef(0);

    useEffect(() => {
        if (playlists.length > 0 && playlistLengthRef.current === 0) {
            playlistLengthRef.current = playlists.length;
        }
    }, [playlists.length]);

    useEffect(() => {
        if (isAddPlaylistSuccess && playlists.length > 0 && playlists.length !== playlistLengthRef.current) {
            playlistLengthRef.current = playlists.length;
            dispatch(setActivePlaylistId(playlists[playlists.length - 1]?.id));
            dispatch(setClearPlaylistsSuccess());
        }
    }, [dispatch, isAddPlaylistSuccess, playlists]);

    const isAllPlaylistsSelected = playlists.length > 0 && playlists.length === selectedPlaylists.length;

    const selectAllPlaylists = useCallback(() => {
        if (selectedPlaylistItemIds.length === 0) {
            const allPlaylistIds = playlists.map(({ id }) => id);
            dispatch(setSelectedPlaylistIds(allPlaylistIds));
            dispatch(setActivePlaylistItemId());
        }
    }, [dispatch, playlists, selectedPlaylistItemIds.length]);

    const unselectAllPlaylists = useCallback(() => {
        dispatch(setSelectedPlaylistIds([]));
    }, [dispatch]);

    const startCreatingPlaylist = useCallback(() => {
        dispatch(setPlaylistDtoInfo({ title: language === 'ru' ? 'Новый плейлист' : 'New playlist', draft: false }));
        dispatch(setPlaylistDtoPlaylistItems());
        dispatch(setPlaylistDtoScreens());
        pushToPage('playlists/creating_one');
    }, [dispatch, language, pushToPage]);

    const isPlaylistSelectDisabled = playlists.length === 0 || selectedPlaylistItemIds.length > 0;

    return {
        playlists,
        startCreatingPlaylist,
        isAllPlaylistsSelected,
        selectAllPlaylists,
        unselectAllPlaylists,
        isPlaylistSelectDisabled,
        t,
    };
};
