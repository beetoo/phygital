import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setHideModal } from '../../../components/ModalsContainer/actions';
import { setActivePlaylistId, setDeletePlaylists } from '../actions';

import {
    selectDeletePlaylistsSuccessStatus,
    selectDeletePlaylistStatus,
    selectPlaylists,
    selectSelectedPlaylistIds,
} from '../selectors';

type ReturnValue = {
    selectedPlaylistsNumber: number;
    doDeletePlaylist: () => void;
    deletePlaylist: boolean;
    closeDeletePlaylistAlert: () => void;
    t: TFunction;
};

export const useDeleteMultiplyPlaylistsAlert = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    const dispatch = useDispatch();

    const playlists = useSelector(selectPlaylists);
    const selectedPlaylistIds = useSelector(selectSelectedPlaylistIds);

    const isDeletePlaylistSuccessStatus = useSelector(selectDeletePlaylistsSuccessStatus);
    const deletePlaylist = useSelector(selectDeletePlaylistStatus);

    useEffect(() => {
        if (isDeletePlaylistSuccessStatus) {
            dispatch(setHideModal());
            if (playlists.length > 0) {
                dispatch(setActivePlaylistId(playlists[0].id));
            }
        }
    }, [dispatch, isDeletePlaylistSuccessStatus, playlists]);

    const closeDeletePlaylistAlert = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    const doDeletePlaylist = useCallback(() => {
        dispatch(setDeletePlaylists(selectedPlaylistIds));
    }, [dispatch, selectedPlaylistIds]);

    const selectedPlaylistsNumber = selectedPlaylistIds.length;

    return {
        selectedPlaylistsNumber,
        deletePlaylist,
        doDeletePlaylist,
        closeDeletePlaylistAlert,
        t,
    };
};
