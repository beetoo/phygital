import { ChangeEvent, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import orderBy from 'lodash/orderBy';
import uniq from 'lodash/uniq';

import { setActivePlaylistId, setOpenDeletePlaylistAlert, setUpdatePlaylistTitle } from '../actions';
import { getCreateOrUpdateTimeString, getResolutionType } from '../helpers';
import {
    selectActivePlaylistId,
    selectDeletePlaylistsSuccessStatus,
    selectPlaylists,
    selectUpdatePlaylistTitleStatus,
    selectUpdatePlaylistTitleSuccessStatus,
} from '../selectors';
import { setActiveScreenId } from '../../ScreensPage/actions';
import { useCustomRouter } from '../../../hooks/useCustomRouter';
import { fromDbToSelectOrientationValue } from '../../ScreensPage/helpers';
import { usePlaylistItemsInfo } from '../../../hooks/usePlaylistItemsInfo';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { useInfoShown } from '../../../hooks/useInfoShown';
import { HistoryItem, useHistoryItems } from './useHistoryItems';
import { CONTENT_TYPE, PlaylistStatus, PlaylistType, ScreenType, OrientationEnum } from '../../../../../common/types';
import { useOpenEditPlaylistStepButton } from './useOpenEditPlaylistStepButton';

type PlaylistMenuTab = 'settings' | 'history';

type ReturnValue = {
    playlistTitle: string;
    playlistStatus: PlaylistType['status'];
    updatingPlaylist: boolean;
    onChangePlaylistTitle: (e: ChangeEvent<HTMLInputElement>) => void;
    updatePlaylist: () => void;
    isUpdatePlaylistButtonDisabled: boolean;
    isSubmitButtonShown: boolean;
    isUploading: boolean;
    openEditPlaylistStep: () => void;
    openDeletePlaylistAlert: () => void;
    firstImage: string;
    firstItemType: CONTENT_TYPE;
    playlistDuration: string;
    playlistFilesSize: string;
    playlistResolution: string;
    playlistOrientation: string;
    playlistCreatedAt: string;
    playlistUpdatedAt: string;
    playlistScreens: ScreenType[];
    isPlaylistScreensOpen: boolean;
    showHidePlaylistScreens: () => void;
    goToScreen: (screenId: string) => () => void;
    playlistMenuTab: PlaylistMenuTab;
    setActivePlaylistMenuTab: () => void;
    playlistHistoryItems: HistoryItem[];
    hasHistory: boolean;

    isInfoShown: boolean;
    showHideInfo: () => void;
    t: TFunction;
};

export const useSinglePlaylistMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlists' });

    const dispatch = useDispatch();

    const { pushToPage } = useCustomRouter();

    const { language } = useSelector(selectCurrentOrganization);
    const activePlaylistId = useSelector(selectActivePlaylistId);
    const playlists = useSelector(selectPlaylists);
    const updatingPlaylist = useSelector(selectUpdatePlaylistTitleStatus);
    const isUpdatePlaylistTitleSuccess = useSelector(selectUpdatePlaylistTitleSuccessStatus);
    const isDeletePlaylistSuccess = useSelector(selectDeletePlaylistsSuccessStatus);

    const { isInfoShown, showHideInfo } = useInfoShown(activePlaylistId);

    const [playlistTitle, setPlaylistTitle] = useState('');
    const [isPlaylistScreensOpen, setIsPlaylistScreensOpen] = useState(false);
    const [playlistStatus, setPlaylistStatus] = useState<PlaylistType['status']>(PlaylistStatus.initial);
    const [playlistMenuTab, setPlaylistMenuTab] = useState<PlaylistMenuTab>('settings');

    const [isSubmitButtonShown, setIsSubmitButtonShown] = useState(false);

    const setActivePlaylistMenuTab = useCallback(() => {
        setPlaylistMenuTab((prevTab) => (prevTab === 'settings' ? 'history' : 'settings'));
    }, []);

    useEffect(() => {
        if (activePlaylistId) {
            setPlaylistMenuTab('settings');
        }
    }, [activePlaylistId]);

    const activePlaylist = useMemo(
        () => playlists.find(({ id }) => id === activePlaylistId),
        [activePlaylistId, playlists],
    );

    const { historyItems: playlistHistoryItems } = useHistoryItems({
        type: 'playlist',
        fetchedId: activePlaylistId,
        fetch: playlistMenuTab === 'history',
    });

    const hasHistory = activePlaylist?.hasHistory ?? false;

    const isUploading = useMemo(() => activePlaylist?.status === PlaylistStatus.uploading, [activePlaylist]);

    const { openEditPlaylistStep, playlistScreens } = useOpenEditPlaylistStepButton();

    const { playlistItemsDuration: playlistDuration, playlistItemsFilesSize: playlistFilesSize } = usePlaylistItemsInfo(
        activePlaylist?.playlistItems ?? [],
    );

    const playlistResolution = useMemo(() => {
        const resolutions =
            playlistScreens.filter(({ resolution }) => resolution).map(({ resolution }) => resolution) ?? [];
        const uniqueResolutions = uniq(resolutions);
        const { width, height } = uniqueResolutions.length > 0 ? uniqueResolutions[0] : { width: 0, height: 0 };
        const type = getResolutionType({ width, height });
        return `${type} (${width} × ${height})`;
    }, [playlistScreens]);

    const playlistOrientation = useMemo(() => {
        const orientations = playlistScreens.map(({ orientation }) => orientation) ?? [];
        const orientation = orientations.length > 0 ? uniq(orientations)[0] : '';
        return fromDbToSelectOrientationValue(orientation as OrientationEnum, language);
    }, [language, playlistScreens]);

    const playlistCreatedAt = useMemo(
        () => getCreateOrUpdateTimeString(activePlaylist?.createdAt ?? ''),
        [activePlaylist?.createdAt],
    );

    const playlistUpdatedAt = useMemo(
        () => getCreateOrUpdateTimeString(activePlaylist?.updatedAt ?? ''),
        [activePlaylist?.updatedAt],
    );

    const isUpdatePlaylistButtonDisabled = useMemo(
        () =>
            updatingPlaylist || playlistTitle.trim() === '' || activePlaylist?.title?.trim() === playlistTitle?.trim(),
        [activePlaylist?.title, playlistTitle, updatingPlaylist],
    );

    useEffect(() => {
        if (isDeletePlaylistSuccess && playlists.length > 0) {
            const id = playlists.length > 0 ? playlists[0].id : '';
            dispatch(setActivePlaylistId(id));
        }
    }, [dispatch, isDeletePlaylistSuccess, playlists]);

    useEffect(() => {
        if (isUpdatePlaylistTitleSuccess) {
            setPlaylistTitle(activePlaylist?.title ?? '');
        }
        setPlaylistTitle(activePlaylist?.title ?? '');
    }, [activePlaylist?.title, isUpdatePlaylistTitleSuccess]);

    useEffect(() => {
        setPlaylistStatus(activePlaylist?.status ?? PlaylistStatus.initial);
    }, [activePlaylist?.status]);

    const onChangePlaylistTitle = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setPlaylistTitle(value);
    }, []);

    const updatePlaylist = useCallback(() => {
        dispatch(setUpdatePlaylistTitle({ playlistId: activePlaylistId, title: playlistTitle }));
    }, [activePlaylistId, dispatch, playlistTitle]);

    const openDeletePlaylistAlert = useCallback(() => {
        dispatch(setOpenDeletePlaylistAlert());
    }, [dispatch]);

    const firstImage = useMemo(() => {
        const orderedPlaylistItems = orderBy(activePlaylist?.playlistItems ?? [], ['orderIndex'], ['asc']);
        const content = orderedPlaylistItems[0]?.content;
        return content?.src;
    }, [activePlaylist?.playlistItems]);

    const firstItemType = useMemo(() => {
        const orderedPlaylistItems = orderBy(activePlaylist?.playlistItems ?? [], ['orderIndex'], ['asc']);
        const content = orderedPlaylistItems[0]?.content;
        return content?.type ?? CONTENT_TYPE.IMAGE;
    }, [activePlaylist?.playlistItems]);

    const showHidePlaylistScreens = useCallback(() => {
        setIsPlaylistScreensOpen((prev) => !prev);
    }, []);

    const goToScreen = useCallback(
        (screenId: string) => () => {
            pushToPage('screens');
            setTimeout(() => dispatch(setActiveScreenId(screenId)), 0);
        },
        [dispatch, pushToPage],
    );

    const prevPlaylistId = useRef(activePlaylistId);

    useEffect(() => {
        if (prevPlaylistId.current !== activePlaylistId) {
            setIsSubmitButtonShown(false);

            prevPlaylistId.current = activePlaylistId;
        } else {
            setIsSubmitButtonShown(!isUpdatePlaylistButtonDisabled);
        }
    }, [activePlaylistId, isUpdatePlaylistButtonDisabled]);

    return {
        playlistTitle,
        updatingPlaylist,
        playlistStatus,
        onChangePlaylistTitle,
        isUpdatePlaylistButtonDisabled,
        isSubmitButtonShown,
        updatePlaylist,
        openEditPlaylistStep,
        openDeletePlaylistAlert,
        firstImage,
        firstItemType,
        isUploading,
        playlistDuration,
        playlistFilesSize,
        playlistResolution,
        playlistOrientation,
        playlistCreatedAt,
        playlistUpdatedAt,
        playlistScreens,
        isPlaylistScreensOpen,
        showHidePlaylistScreens,
        goToScreen,
        playlistMenuTab,
        setActivePlaylistMenuTab,
        playlistHistoryItems,
        hasHistory,

        isInfoShown,
        showHideInfo,
        t,
    };
};
