import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import orderBy from 'lodash/orderBy';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import {
    selectActivePlaylistId,
    selectPlaylists,
    selectSelectedPlaylistIds,
    selectSelectedPlaylistItemIds,
} from '../selectors';

import { setSelectedPlaylistItemIds } from '../actions';
import { PlaylistItemType } from '../../../../../common/types';

type ReturnValue = {
    playlistItems: PlaylistItemType[];
    isAllPlaylistItemsSelected: boolean;
    selectAllPlaylistItems: () => void;
    unselectAllPlaylistItems: () => void;
    isPlaylistItemDisabled: boolean;
    t: TFunction;
};

export const usePlaylistItemsList = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlists' });

    const dispatch = useDispatch();

    const activePlaylistId = useSelector(selectActivePlaylistId);
    const playlists = useSelector(selectPlaylists);

    const selectedPlaylistIds = useSelector(selectSelectedPlaylistIds);
    const selectedPlaylistItemIds = useSelector(selectSelectedPlaylistItemIds);

    const playlistItems = useMemo(
        () =>
            orderBy(playlists.find(({ id }) => id === activePlaylistId)?.playlistItems ?? [], ['orderIndex'], ['asc']),
        [activePlaylistId, playlists],
    );

    const activePlaylistSelectedItemIds = useMemo(
        () => playlistItems.filter(({ id }) => selectedPlaylistItemIds.includes(id)),
        [playlistItems, selectedPlaylistItemIds],
    );

    const isAllPlaylistItemsSelected =
        playlistItems.length > 0 && playlistItems.length === activePlaylistSelectedItemIds.length;

    const selectAllPlaylistItems = useCallback(() => {
        if (selectedPlaylistIds.length === 0) {
            const allPlaylistItemsIds = playlistItems.map(({ id }) => id);
            dispatch(setSelectedPlaylistItemIds(allPlaylistItemsIds));
        }
    }, [dispatch, playlistItems, selectedPlaylistIds.length]);

    const unselectAllPlaylistItems = useCallback(() => {
        dispatch(setSelectedPlaylistItemIds([]));
    }, [dispatch]);

    const isPlaylistItemDisabled = selectedPlaylistIds.length > 0;

    return {
        playlistItems,
        isAllPlaylistItemsSelected,
        selectAllPlaylistItems,
        unselectAllPlaylistItems,
        isPlaylistItemDisabled,
        t,
    };
};
