import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setActivePlaylistId, setCloseDeletePlaylistAlert, setDeletePlaylists } from '../actions';

import {
    selectActivePlaylistId,
    selectDeletePlaylistAlertStatus,
    selectDeletePlaylistsSuccessStatus,
    selectDeletePlaylistStatus,
    selectPlaylists,
} from '../selectors';

type ReturnValue = {
    isDeletePlaylistAlertOpen: boolean;
    doDeletePlaylist: () => void;
    deletePlaylist: boolean;
    closeDeletePlaylistAlert: () => void;
    t: TFunction;
};

export const useDeletePlaylistAlert = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    const dispatch = useDispatch();

    const activePlaylistId = useSelector(selectActivePlaylistId);
    const isDeletePlaylistAlertOpen = useSelector(selectDeletePlaylistAlertStatus);
    const playlists = useSelector(selectPlaylists);

    const isDeletePlaylistSuccessStatus = useSelector(selectDeletePlaylistsSuccessStatus);
    const deletePlaylist = useSelector(selectDeletePlaylistStatus);

    useEffect(() => {
        if (isDeletePlaylistSuccessStatus) {
            dispatch(setCloseDeletePlaylistAlert());
            if (playlists.length > 0) {
                dispatch(setActivePlaylistId(playlists[0].id));
            }
        }
    }, [dispatch, isDeletePlaylistSuccessStatus, playlists]);

    const closeDeletePlaylistAlert = useCallback(() => {
        dispatch(setCloseDeletePlaylistAlert());
    }, [dispatch]);

    const doDeletePlaylist = useCallback(() => {
        dispatch(setDeletePlaylists(activePlaylistId));
    }, [activePlaylistId, dispatch]);

    return {
        isDeletePlaylistAlertOpen,
        deletePlaylist,
        doDeletePlaylist,
        closeDeletePlaylistAlert,
        t,
    };
};
