import { useState, SetStateAction, useEffect, Dispatch } from 'react';

import { usePlaylistStep } from './usePlaylistStep';

type ReturnValue = {
    selectScreenRef: HTMLParagraphElement | null;
    setSelectScreenRef: Dispatch<SetStateAction<HTMLParagraphElement | null>>;
    unsuitableScreenRef: HTMLLIElement | null;
    setUnsuitableScreenRef: Dispatch<SetStateAction<HTMLLIElement | null>>;

    contentWithoutFolderRef: HTMLLIElement | null;
    setContentWithoutFolderRef: Dispatch<SetStateAction<HTMLLIElement | null>>;

    uploadContentBtnRef: HTMLButtonElement | null;
    setUploadContentBtnRef: Dispatch<SetStateAction<HTMLButtonElement | null>>;

    createPlaylistItemRef: HTMLLIElement | null;
    setCreatePlaylistItemRef: Dispatch<SetStateAction<HTMLLIElement | null>>;

    durationRef: HTMLParagraphElement | null;
    setDurationRef: Dispatch<SetStateAction<HTMLParagraphElement | null>>;
};

export const useCreatePlaylistOnBoardingRefs = (): ReturnValue => {
    // первый шаг
    const [selectScreenRef, setSelectScreenRef] = useState<HTMLParagraphElement | null>(null);
    const [unsuitableScreenRef, setUnsuitableScreenRef] = useState<HTMLLIElement | null>(null);

    // второй шаг
    const [contentWithoutFolderRef, setContentWithoutFolderRef] = useState<HTMLLIElement | null>(null);
    const [uploadContentBtnRef, setUploadContentBtnRef] = useState<HTMLButtonElement | null>(null);
    const [createPlaylistItemRef, setCreatePlaylistItemRef] = useState<HTMLLIElement | null>(null);
    const [durationRef, setDurationRef] = useState<HTMLParagraphElement | null>(null);

    const { playlistStep } = usePlaylistStep();

    const isStepOne = ['creating_one', 'editing_one'].includes(playlistStep);
    const isStepTwo = ['creating_two', 'editing_two'].includes(playlistStep);

    // обнуляем рефы, чтобы при повторном заходе в шаг, карточка никуда не сдвинулась
    useEffect(() => {
        if (playlistStep === '' || isStepTwo) {
            setUnsuitableScreenRef(null);
        }
        if (playlistStep === '' || isStepOne) {
            setDurationRef(null);
        }
    }, [isStepOne, isStepTwo, playlistStep]);

    return {
        selectScreenRef,
        setSelectScreenRef,
        unsuitableScreenRef,
        setUnsuitableScreenRef,

        contentWithoutFolderRef,
        setContentWithoutFolderRef,
        uploadContentBtnRef,
        setUploadContentBtnRef,
        createPlaylistItemRef,
        setCreatePlaylistItemRef,
        durationRef,
        setDurationRef,
    };
};
