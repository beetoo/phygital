import { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { selectActiveLocationId, selectActiveScreenId, selectLocations } from '../../ScreensPage/selectors';
import { getScreensInfo } from '../../ScreensPage/helpers';
import { setActiveLocationId, setActiveScreenId, setGetLocations } from '../../ScreensPage/actions';
import { LocationType } from '../../../../../common/types';
import { selectPlaylistDtoScreens } from '../selectors';

type ReturnValue = {
    activeLocationId: string;
    isScreensActive: boolean;
    locations: LocationType[];
    selectLocation: (locationId: string) => void;
    allScreensNumber: number;
    allOnlineScreensNumber: number;
    allOfflineScreensNumber: number;
    allErrorScreensNumber: number;
    t: TFunction;
};

export const useStepOneLocationsList = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const { id: organizationId } = useSelector(selectCurrentOrganization);

    const playlistDtoScreens = useSelector(selectPlaylistDtoScreens);
    const activeLocationId = useSelector(selectActiveLocationId);
    const activeScreenId = useSelector(selectActiveScreenId);
    const locations = useSelector(selectLocations);

    const allScreens = useMemo(() => locations.flatMap(({ screens }) => screens), [locations]);

    const {
        screensNumber: allScreensNumber,
        onlineScreensNumber: allOnlineScreensNumber,
        offlineScreensNumber: allOfflineScreensNumber,
        errorScreensNumber: allErrorScreensNumber,
    } = getScreensInfo(allScreens);

    useEffect(() => {
        if (organizationId) {
            dispatch(setGetLocations(organizationId));
        }
    }, [dispatch, organizationId]);

    const selectLocation = (locationId: string) => {
        dispatch(setActiveLocationId(locationId));
        if (activeScreenId) {
            dispatch(setActiveScreenId());
        }
    };

    const isScreensActive = activeScreenId !== '' || playlistDtoScreens.length > 0;

    return {
        activeLocationId,
        isScreensActive,
        locations,
        selectLocation,
        allScreensNumber,
        allOnlineScreensNumber,
        allOfflineScreensNumber,
        allErrorScreensNumber,
        t,
    };
};
