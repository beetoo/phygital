import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import sortBy from 'lodash/sortBy';

import { formatPlaylistSchedules } from '../helpers';
import {
    setPlaylistDtoInfo,
    setPlaylistDtoLog,
    setPlaylistDtoPlaylistItems,
    setPlaylistDtoSchedules,
    setPlaylistDtoScreens,
    setPlaylistDtoSync,
} from '../actions';
import { selectActivePlaylistId, selectPlaylists } from '../selectors';
import { selectScreens } from '../../ScreensPage/selectors';
import { ScreenType } from '../../../../../common/types';
import { useCustomRouter } from '../../../hooks/useCustomRouter';

type ReturnValue = {
    playlistScreens: ScreenType[];
    openEditPlaylistStep: () => void;
};

export const useOpenEditPlaylistStepButton = (): ReturnValue => {
    const dispatch = useDispatch();

    const { pushToPage } = useCustomRouter();

    const activePlaylistId = useSelector(selectActivePlaylistId);
    const playlists = useSelector(selectPlaylists);
    const screens = useSelector(selectScreens);

    const activePlaylist = useMemo(
        () => playlists.find(({ id }) => id === activePlaylistId),
        [activePlaylistId, playlists],
    );

    // находим playlistItems, которые использует плейлист
    const currentPlaylistItems = useMemo(
        () => sortBy(activePlaylist?.playlistItems ?? [], (o) => o.orderIndex),
        [activePlaylist?.playlistItems],
    );

    // находим расписания, которые используются этим плейлистом
    const currentPlaylistSchedules = useMemo(
        () => formatPlaylistSchedules(activePlaylist?.schedules ?? []),
        [activePlaylist?.schedules],
    );

    const playlistScreens = useMemo(
        () =>
            screens.filter(({ schedules }) => schedules.some(({ playlist }) => playlist?.id === activePlaylistId)) ??
            [],
        [activePlaylistId, screens],
    );

    // находим экраны, использующие этот плейлист
    const playlistScreensIds = useMemo(() => playlistScreens.map(({ id }) => id) || [], [playlistScreens]);

    const openEditPlaylistStep = useCallback(() => {
        dispatch(setPlaylistDtoInfo({ title: activePlaylist?.title ?? '', draft: false }));
        dispatch(setPlaylistDtoScreens(playlistScreensIds.map((id) => ({ id }))));
        dispatch(setPlaylistDtoPlaylistItems(currentPlaylistItems));
        dispatch(setPlaylistDtoSchedules(currentPlaylistSchedules));
        pushToPage(playlistScreensIds.length > 0 ? 'playlists/editing_two' : 'playlists/editing_one');
        dispatch(setPlaylistDtoSync(activePlaylist?.sync ?? false));
        dispatch(setPlaylistDtoLog(activePlaylist?.log ?? false));
    }, [
        activePlaylist?.log,
        activePlaylist?.sync,
        activePlaylist?.title,
        currentPlaylistItems,
        currentPlaylistSchedules,
        dispatch,
        playlistScreensIds,
        pushToPage,
    ]);

    return {
        playlistScreens,
        openEditPlaylistStep,
    };
};
