import { ChangeEvent, Dispatch, SetStateAction, useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setPlaylistDtoInfo, setPlaylistDtoPlaylistItems, setPlaylistDtoScreens } from '../actions';
import { useCustomRouter } from '../../../hooks/useCustomRouter';

type ReturnValue = {
    playlistName: string;
    onPlaylistNameChange: (e: ChangeEvent<HTMLInputElement>) => void;
    isSubmitButtonDisabled: boolean;

    startCreatingPlaylist: () => void;

    addPlaylistNameInput: HTMLInputElement | null;
    setAddPlaylistNameInput: Dispatch<SetStateAction<HTMLInputElement | null>>;
    t: TFunction;
};

export const useAddPlaylistModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    const dispatch = useDispatch();

    const { pushToPage } = useCustomRouter();

    const [playlistName, setPlaylistName] = useState('');

    const onPlaylistNameChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setPlaylistName(value);
    }, []);

    const startCreatingPlaylist = useCallback(() => {
        dispatch(setPlaylistDtoInfo({ title: playlistName, draft: false }));
        dispatch(setPlaylistDtoPlaylistItems());
        dispatch(setPlaylistDtoScreens());
        pushToPage('playlists/creating_one');
    }, [dispatch, playlistName, pushToPage]);

    const isSubmitButtonDisabled = playlistName.trim() === '';

    // обучение
    const [addPlaylistNameInput, setAddPlaylistNameInput] = useState<HTMLInputElement | null>(null);

    return {
        playlistName,
        onPlaylistNameChange,
        isSubmitButtonDisabled,
        startCreatingPlaylist,
        addPlaylistNameInput,
        setAddPlaylistNameInput,
        t,
    };
};
