import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { PlaylistStatus } from '../../../../../common/types';
import { setShowModal } from '../../../components/ModalsContainer/actions';
import { MODALS } from '../../../components/ModalsContainer/types';
import { setSelectedPlaylistIds } from '../actions';
import { getFilesizeString } from '../helpers';
import { selectPlaylists, selectSelectedPlaylistIds } from '../selectors';

type ReturnValue = {
    selectedPlaylistsNumber: number;
    playlistsFilesSize: string;
    openDeleteMultiplyPlaylistsAlert: () => void;
    unselectAllPlaylists: () => void;
    isPlaylistsCouldBeDeleted: boolean;
    t: TFunction;
};

export const useMultiplyPlaylistsMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlists' });

    const dispatch = useDispatch();

    const playlists = useSelector(selectPlaylists);
    const selectedPlaylistIds = useSelector(selectSelectedPlaylistIds);

    const openDeleteMultiplyPlaylistsAlert = useCallback(() => {
        dispatch(setShowModal(MODALS.DELETE_MULTIPLY_PLAYLISTS));
    }, [dispatch]);

    const unselectAllPlaylists = useCallback(() => {
        dispatch(setSelectedPlaylistIds([]));
    }, [dispatch]);

    const selectedPlayLists = useMemo(
        () => playlists.filter(({ id }) => selectedPlaylistIds.includes(id)),
        [playlists, selectedPlaylistIds],
    );

    const playlistsFilesSize = useMemo(() => {
        const size =
            selectedPlayLists
                .flatMap(({ playlistItems }) => playlistItems)
                .map(({ content }) => content?.size ?? 0)
                .reduce((prev, current) => prev + current, 0) ?? 0;
        return getFilesizeString(size);
    }, [selectedPlayLists]);

    const selectedPlaylistsNumber = selectedPlaylistIds.length;

    const isPlaylistsCouldBeDeleted = useMemo(
        () => !selectedPlayLists.some((p) => p.status === PlaylistStatus.uploading),
        [selectedPlayLists],
    );

    return {
        selectedPlaylistsNumber,
        playlistsFilesSize,
        openDeleteMultiplyPlaylistsAlert,
        unselectAllPlaylists,
        isPlaylistsCouldBeDeleted,
        t,
    };
};
