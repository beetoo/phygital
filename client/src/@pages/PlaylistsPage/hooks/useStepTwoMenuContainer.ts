import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import {
    selectPlaylistDto,
    selectStepTwoMenuMode,
    selectStepTwoSelectedContentItemIds,
    selectStepTwoSelectedPlaylistItemIds,
} from '../selectors';
import { StepTwoMenuMode } from '../types';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { setActiveFolderId, setGetContentItems, setGetFolders } from '../../ContentPage/actions';
import { setStepTwoMenuMode, setStepTwoSelectedContentItemIds } from '../actions';
import { useAddContentToPlaylist } from '../components/PlaylistStepTwo/hooks/useAddContentToPlaylist';
import { useTimeLineRef } from '../components/PlaylistStepTwo/hooks/useTimeLIneRef';
import { usePlaylistStep } from './usePlaylistStep';
import { useCustomRouter } from '../../../hooks/useCustomRouter';

type ReturnValue = {
    menuMode: StepTwoMenuMode;
    isMultiplyContentSettingMenuEnabled: boolean;
    isNextStepButtonDisabled: boolean;
    moveToStepThree: () => void;
    addContentItemsToPlaylist: () => void;
    cancelAddContentItemToScenario: () => void;
    isAddContentButtonsShown: boolean;
    isAddContentButtonDisabled: boolean;
    isAddToScenarioMode: boolean;
    t: TFunction;
};

export const useStepTwoMenuContainer = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const { playlistStep } = usePlaylistStep();

    const { pushToPage } = useCustomRouter();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const selectedPlaylistItemIds = useSelector(selectStepTwoSelectedPlaylistItemIds);
    const menuMode = useSelector(selectStepTwoMenuMode);
    const selectedContentItemIds = useSelector(selectStepTwoSelectedContentItemIds);
    const { playlistDtoItems } = useSelector(selectPlaylistDto);

    const isMultiplyContentSettingMenuEnabled = selectedPlaylistItemIds.length > 0;

    const isAddToScenarioMode = menuMode === 'content_menu_to_scenario';

    useEffect(() => {
        if (organizationId) {
            dispatch(setGetContentItems(organizationId));
            dispatch(setGetFolders(organizationId));
            dispatch(setActiveFolderId());
        }
    }, [dispatch, organizationId]);

    const moveToStepThree = useCallback(() => {
        pushToPage(playlistStep === 'creating_two' ? 'playlists/creating_three' : 'playlists/editing_three');
    }, [playlistStep, pushToPage]);

    const { addContentToPlaylist } = useAddContentToPlaylist();

    const { scrollToRightOrBottom } = useTimeLineRef();

    const addContentItemsToPlaylist = useCallback(() => {
        addContentToPlaylist();
        dispatch(setStepTwoSelectedContentItemIds());
        scrollToRightOrBottom();
    }, [addContentToPlaylist, dispatch, scrollToRightOrBottom]);

    const cancelAddContentItemToScenario = useCallback(() => {
        if (selectedContentItemIds.length > 0) {
            dispatch(setStepTwoSelectedContentItemIds());
        }

        if (isAddToScenarioMode) {
            dispatch(setStepTwoMenuMode('scenario_setting_menu'));
        }
    }, [dispatch, isAddToScenarioMode, selectedContentItemIds.length]);

    const isNextStepButtonDisabled = playlistDtoItems.length === 0;

    const isAddContentButtonsShown = selectedContentItemIds.length > 0 || isAddToScenarioMode;

    const isAddContentButtonDisabled = isAddToScenarioMode && selectedContentItemIds.length === 0;

    return {
        menuMode,
        isMultiplyContentSettingMenuEnabled,
        isNextStepButtonDisabled,
        moveToStepThree,
        addContentItemsToPlaylist,
        cancelAddContentItemToScenario,
        isAddContentButtonsShown,
        isAddContentButtonDisabled,
        isAddToScenarioMode,
        t,
    };
};
