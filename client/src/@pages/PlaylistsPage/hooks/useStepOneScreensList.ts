import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectActiveLocationId, selectActiveScreenId, selectScreens } from '../../ScreensPage/selectors';
import { setActiveScreenId } from '../../ScreensPage/actions';
import { setPlaylistDtoScreens } from '../actions';
import { selectPlaylistDto, selectPlaylistDtoScreens, selectPlaylistDtoScreensOrdered } from '../selectors';
import { ScreenType } from '../../../../../common/types';
import { getAspectRatioString } from '../../../helpers';

type ReturnValue = {
    activeScreenId: string;
    filteredUnselectedScreens: ScreenType[];
    selectActiveScreen: (activeScreenId: string) => () => void;
    selectScreen: (screenId: string) => () => void;
    selectAllScreens: () => () => void;
    unselectAllScreens: () => () => void;
    isAllScreensSelected: boolean;
    isAllScreensSelectButtonDisabled: boolean;
    isScreenWithAnotherResolutionOrOrientation: (screenId: string) => boolean;
    t: TFunction;
};

export const useStepOneScreensList = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const activeLocationId = useSelector(selectActiveLocationId);
    const activeScreenId = useSelector(selectActiveScreenId);
    const screens = useSelector(selectScreens);
    const { playlistDtoFilteringTags } = useSelector(selectPlaylistDto);
    const playlistDtoScreens = useSelector(selectPlaylistDtoScreens);
    const playlistDtoScreensOrdered = useSelector(selectPlaylistDtoScreensOrdered);

    const activeResolutionAndOrientation = useMemo(() => {
        const selectedScreens = screens
            .filter(({ location }) => (activeLocationId !== 'all' ? location?.id === activeLocationId : location?.id))
            .filter(({ id }) => playlistDtoScreens.includes(id));

        return selectedScreens.length > 0
            ? {
                  resolution: selectedScreens[0].resolution,
                  orientation: selectedScreens[0].orientation,
              }
            : {};
    }, [activeLocationId, screens, playlistDtoScreens]);

    const unselectedScreens = useMemo(
        () =>
            screens
                .filter(({ location }) =>
                    activeLocationId !== 'all' ? location?.id === activeLocationId : location?.id,
                )
                .filter(({ id }) => !playlistDtoScreens.includes(id)),
        [activeLocationId, playlistDtoScreens, screens],
    );

    const filteredUnselectedScreens = useMemo(
        () =>
            unselectedScreens.filter(({ tags }) =>
                playlistDtoFilteringTags.length > 0
                    ? tags.some(({ name }) => playlistDtoFilteringTags.includes(name))
                    : true,
            ),
        [playlistDtoFilteringTags, unselectedScreens],
    );

    const isScreenWithAnotherResolutionOrOrientation = useCallback(
        (screenId: string) =>
            playlistDtoScreens.length > 0 &&
            unselectedScreens
                .filter(
                    ({ resolution, orientation }) =>
                        orientation !== activeResolutionAndOrientation?.orientation ||
                        getAspectRatioString(
                            activeResolutionAndOrientation?.resolution?.width,
                            activeResolutionAndOrientation?.resolution?.height,
                        ) !== getAspectRatioString(resolution?.width, resolution?.height),
                )
                .some(({ id }) => id === screenId),
        [
            activeResolutionAndOrientation?.orientation,
            activeResolutionAndOrientation?.resolution?.height,
            activeResolutionAndOrientation?.resolution?.width,
            playlistDtoScreens.length,
            unselectedScreens,
        ],
    );

    // если активный плейлист становится disabled, то убираем
    useEffect(() => {
        if (isScreenWithAnotherResolutionOrOrientation(activeScreenId)) {
            dispatch(setActiveScreenId());
        }
    }, [activeScreenId, dispatch, isScreenWithAnotherResolutionOrOrientation]);

    const selectActiveScreen = useCallback(
        (activeScreenId: string) => () => {
            if (!isScreenWithAnotherResolutionOrOrientation(activeScreenId)) {
                dispatch(setActiveScreenId(activeScreenId));
            }
        },
        [dispatch, isScreenWithAnotherResolutionOrOrientation],
    );

    const selectScreen = useCallback(
        (screenId: string) => () => {
            dispatch(setPlaylistDtoScreens([{ id: screenId }, ...playlistDtoScreensOrdered]));
        },
        [dispatch, playlistDtoScreensOrdered],
    );

    const selectAllScreens = useCallback(
        () => () => {
            dispatch(
                setPlaylistDtoScreens([
                    ...unselectedScreens
                        .map(({ id }) => ({ id }))
                        .filter(({ id }) => !isScreenWithAnotherResolutionOrOrientation(id)),
                    ...playlistDtoScreensOrdered,
                ]),
            );
        },
        [dispatch, isScreenWithAnotherResolutionOrOrientation, playlistDtoScreensOrdered, unselectedScreens],
    );

    const unselectAllScreens = useCallback(
        () => () => {
            dispatch(setPlaylistDtoScreens());
        },
        [dispatch],
    );

    const isAllScreensSelectButtonDisabled = useMemo(
        () =>
            screens.some(
                ({ resolution, orientation }, _, array) =>
                    orientation !== array[0].orientation ||
                    getAspectRatioString(resolution.width, resolution.height) !==
                        getAspectRatioString(array[0].resolution.width, array[0].resolution.height),
            ),
        [screens],
    );

    const isAllScreensSelected = useMemo(
        () =>
            screens.length > 0 &&
            unselectedScreens.filter(({ id }) => !isScreenWithAnotherResolutionOrOrientation(id)).length === 0,
        [isScreenWithAnotherResolutionOrOrientation, screens.length, unselectedScreens],
    );

    return {
        activeScreenId,
        filteredUnselectedScreens,
        selectActiveScreen,
        selectScreen,
        selectAllScreens,
        unselectAllScreens,
        isAllScreensSelectButtonDisabled,
        isAllScreensSelected,
        isScreenWithAnotherResolutionOrOrientation,
        t,
    };
};
