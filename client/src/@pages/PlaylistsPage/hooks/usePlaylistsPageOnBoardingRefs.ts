import { useState, SetStateAction, Dispatch } from 'react';

type ReturnValue = {
    addPlaylistRef: HTMLButtonElement | null;
    setAddPlaylistRef: Dispatch<SetStateAction<HTMLButtonElement | null>>;
    contentRef: HTMLLIElement | null;
    setContentRef: Dispatch<SetStateAction<HTMLLIElement | null>>;
    editPlaylistButtonRef: HTMLButtonElement | null;
    setEditPlaylistButtonRef: Dispatch<SetStateAction<HTMLButtonElement | null>>;
};

export const usePlaylistsPageOnBoardingRefs = (): ReturnValue => {
    // страница плейлистов
    const [addPlaylistRef, setAddPlaylistRef] = useState<HTMLButtonElement | null>(null);
    const [contentRef, setContentRef] = useState<HTMLLIElement | null>(null);
    const [editPlaylistButtonRef, setEditPlaylistButtonRef] = useState<HTMLButtonElement | null>(null);

    return {
        addPlaylistRef,
        setAddPlaylistRef,
        contentRef,
        setContentRef,
        editPlaylistButtonRef,
        setEditPlaylistButtonRef,
    };
};
