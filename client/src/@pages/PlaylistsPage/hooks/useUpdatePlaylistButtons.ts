import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { differenceInCalendarDays } from 'date-fns';
import isEqual from 'lodash/isEqual';

import {
    selectActivePlaylistId,
    selectPlaylistDto,
    selectPlaylistDtoScreens,
    selectPlaylists,
    selectUpdatingPlaylistStatus,
} from '../selectors';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { setAddPlaylist, setOpenStartPlaylistModal, setUpdatePlaylist } from '../actions';
import { formatPlaylistSchedules, formatPlaylistItems, sortArrayOfIds } from '../helpers';
import { selectScreens } from '../../ScreensPage/selectors';
import { UpdatePlaylistDtoClient } from '../types';
import {
    setClosedUploadingPlaylistSnackbars,
    setHideModal,
    setShowModalNew,
} from '../../../components/ModalsContainer/actions';
import { selectClosedUploadingPlaylistSnackbars } from '../../../components/ModalsContainer/selectors';
import { toDate } from '../../../helpers';
import { MODALS } from '../../../components/ModalsContainer/types';
import { usePlaylistStep } from './usePlaylistStep';
import { useCustomRouter } from '../../../hooks/useCustomRouter';

type ReturnValue = {
    openStartPlaylistModal: () => void;
    updatePlaylist: () => void;
    isCreatePlaylistStep: boolean;
    isSubmitButtonDisabled: boolean;
};

export const useUpdatePlaylistButtons = (): ReturnValue => {
    const dispatch = useDispatch();

    const { pushToPage } = useCustomRouter();

    const { isCreatePlaylistStep, isEditPlaylistStep, playlistStep } = usePlaylistStep();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const activePlaylistId = useSelector(selectActivePlaylistId);
    const playlists = useSelector(selectPlaylists);
    const screens = useSelector(selectScreens);
    const closedSnackbarKeys = useSelector(selectClosedUploadingPlaylistSnackbars);

    const {
        playlistDtoInfo,
        playlistDtoFilteringTags,
        playlistDtoItems,
        playlistDtoSchedules,
        playlistDtoSync,
        playlistDtoLog,
    } = useSelector(selectPlaylistDto);
    const playlistDtoScreens = useSelector(selectPlaylistDtoScreens);

    const updatingPlaylist = useSelector(selectUpdatingPlaylistStatus);

    const openStartPlaylistModal = useCallback(() => {
        dispatch(setOpenStartPlaylistModal());
    }, [dispatch]);

    const activePlaylist = useMemo(
        () => playlists.find(({ id }) => id === activePlaylistId),
        [activePlaylistId, playlists],
    );

    const isVirtualScreenDtoChanged = useMemo(
        () => !isEqual(activePlaylist?.virtualScreen, playlistDtoInfo.virtualScreen),
        [activePlaylist?.virtualScreen, playlistDtoInfo.virtualScreen],
    );

    const isFilteringTagsDtoChanged = useMemo(
        () => !isEqual(sortArrayOfIds(activePlaylist?.filteringTags), sortArrayOfIds(playlistDtoFilteringTags)),
        [activePlaylist?.filteringTags, playlistDtoFilteringTags],
    );

    const isScreensDtoChanged = useMemo(
        () =>
            !isEqual(
                sortArrayOfIds(
                    screens
                        .filter(({ schedules }) => schedules.some(({ playlist }) => playlist.id === activePlaylistId))
                        .map(({ id }) => id) ?? [],
                ),
                sortArrayOfIds(playlistDtoScreens),
            ),
        [activePlaylistId, playlistDtoScreens, screens],
    );

    const isPlaylistItemsDtoChanged = useMemo(
        () => !isEqual(formatPlaylistItems(activePlaylist?.playlistItems), formatPlaylistItems(playlistDtoItems)),
        [activePlaylist?.playlistItems, playlistDtoItems],
    );

    const isPlaylistSchedulesDtoChanged = useMemo(
        () => !isEqual(formatPlaylistSchedules(activePlaylist?.schedules ?? []), playlistDtoSchedules),
        [activePlaylist?.schedules, playlistDtoSchedules],
    );

    const playlistDtoItemsMinimized = useMemo(
        () =>
            playlistDtoItems.map(({ content, ...rest }) => ({
                content: { id: content?.id },
                ...rest,
            })),
        [playlistDtoItems],
    );

    const savePlaylistOnEditStep = useCallback(
        (isCloseModalRequired = false) => {
            if (
                isVirtualScreenDtoChanged ||
                isFilteringTagsDtoChanged ||
                isScreensDtoChanged ||
                isPlaylistItemsDtoChanged ||
                isPlaylistSchedulesDtoChanged
            ) {
                // очистка сохраненных закрытых снекбаров этого плейлиста
                dispatch(
                    setClosedUploadingPlaylistSnackbars(
                        closedSnackbarKeys.filter((snackbarKey) => !snackbarKey.includes(activePlaylistId)),
                    ),
                );

                dispatch(
                    setUpdatePlaylist(activePlaylistId, {
                        playlistDtoInfo: { ...playlistDtoInfo, organizationId },
                        playlistDtoFilteringTags: isFilteringTagsDtoChanged ? playlistDtoFilteringTags : undefined,
                        playlistDtoScreens: playlistDtoScreens,
                        playlistDtoItems: playlistDtoItemsMinimized,
                        playlistDtoSchedules: isPlaylistSchedulesDtoChanged ? playlistDtoSchedules : undefined,
                        playlistDtoSync,
                        playlistDtoLog,
                    } as UpdatePlaylistDtoClient),
                );
            } else {
                pushToPage('playlists');
                if (isCloseModalRequired) {
                    dispatch(setHideModal());
                }
            }
        },
        [
            activePlaylistId,
            closedSnackbarKeys,
            dispatch,
            isFilteringTagsDtoChanged,
            isPlaylistItemsDtoChanged,
            isPlaylistSchedulesDtoChanged,
            isScreensDtoChanged,
            isVirtualScreenDtoChanged,
            organizationId,
            playlistDtoFilteringTags,
            playlistDtoInfo,
            playlistDtoItemsMinimized,
            playlistDtoLog,
            playlistDtoSchedules,
            playlistDtoScreens,
            playlistDtoSync,
            pushToPage,
        ],
    );

    const updatePlaylist = useCallback(() => {
        if (isCreatePlaylistStep) {
            dispatch(
                setAddPlaylist(organizationId, {
                    playlistDtoInfo: { ...playlistDtoInfo, draft: true },
                    playlistDtoFilteringTags,
                    playlistDtoScreens,
                    playlistDtoItems: playlistDtoItemsMinimized,
                    playlistDtoSchedules,
                    playlistDtoSync,
                    playlistDtoLog,
                }),
            );
        }

        if (isEditPlaylistStep) {
            if (playlistDtoScreens.length === 0 && playlistStep === 'editing_three') {
                return dispatch(
                    setShowModalNew(MODALS.DRAFT_PLAYLIST_SAVE_MODAL, {
                        savePlaylistOnEditStep: () => savePlaylistOnEditStep(true),
                    }),
                );
            }

            savePlaylistOnEditStep();
        }
    }, [
        dispatch,
        isCreatePlaylistStep,
        isEditPlaylistStep,
        organizationId,
        playlistDtoFilteringTags,
        playlistDtoInfo,
        playlistDtoItemsMinimized,
        playlistDtoLog,
        playlistDtoSchedules,
        playlistDtoScreens,
        playlistDtoSync,
        playlistStep,
        savePlaylistOnEditStep,
    ]);

    const isEndDateLessThanStartDate = useMemo(
        () =>
            playlistDtoSchedules.some(
                ({ startDay, endDay, startTime, endTime, broadcastAlways }) =>
                    !broadcastAlways &&
                    differenceInCalendarDays(toDate(endDay, endTime), toDate(startDay, startTime)) < 0,
            ),
        [playlistDtoSchedules],
    );

    const isSubmitButtonDisabled = useMemo(
        () => playlistDtoInfo.title?.trim().length === 0 || isEndDateLessThanStartDate || updatingPlaylist,
        [isEndDateLessThanStartDate, playlistDtoInfo.title, updatingPlaylist],
    );

    return {
        openStartPlaylistModal,
        updatePlaylist,
        isCreatePlaylistStep,

        isSubmitButtonDisabled,
    };
};
