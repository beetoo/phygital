import { useEffect, useState } from 'react';
import { format } from 'date-fns';

import { httpService } from '../../../helpers/httpService';

export type PlaylistHistoryItem = {
    id: string;
    createdAt: string;
    type: 'status' | 'trigger';
    description: string;
};

export type ScreenHistoryItem = {
    id: string;
    createdAt: string;
    status: string;
};

type Props = {
    type: 'playlist' | 'screen';
    fetchedId: string;
    fetch: boolean;
};

export type HistoryItem = PlaylistHistoryItem | ScreenHistoryItem;

type ReturnValue = {
    historyItems: HistoryItem[];
};

export const useHistoryItems = ({ type, fetchedId, fetch }: Props): ReturnValue => {
    const [historyItems, setHistoryItems] = useState<HistoryItem[]>([]);

    useEffect(() => {
        if (fetch) {
            httpService.get(`${type}/${fetchedId}/history`).then((data) => {
                const historyItems = (data as HistoryItem[]).map((item) => ({
                    ...item,
                    createdAt: format(new Date(item.createdAt), 'HH:mm, dd.MM.yyyy '),
                }));
                setHistoryItems(historyItems);
            });
        }
    }, [fetch, fetchedId, type]);

    return { historyItems };
};
