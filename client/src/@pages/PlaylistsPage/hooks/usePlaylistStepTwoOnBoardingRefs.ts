import { useState, SetStateAction, useEffect, Dispatch } from 'react';

import { usePlaylistStep } from './usePlaylistStep';

type ReturnValue = {
    contentWithoutFolderRef: HTMLLIElement | null;
    setContentWithoutFolderRef: Dispatch<SetStateAction<HTMLLIElement | null>>;

    uploadContentBtnRef: HTMLButtonElement | null;
    setUploadContentBtnRef: Dispatch<SetStateAction<HTMLButtonElement | null>>;

    emptyPlaylistItemRef: HTMLLIElement | null;
    setEmptyPlaylistItemRef: Dispatch<SetStateAction<HTMLLIElement | null>>;

    firstPlaylistItemRef: HTMLLIElement | null;
    setFirstPlaylistItemRef: Dispatch<SetStateAction<HTMLLIElement | null>>;

    durationRef: HTMLParagraphElement | null;
    setDurationRef: Dispatch<SetStateAction<HTMLParagraphElement | null>>;
};

export const usePlaylistStepTwoOnBoardingRefs = (): ReturnValue => {
    // второй шаг
    const [contentWithoutFolderRef, setContentWithoutFolderRef] = useState<HTMLLIElement | null>(null);
    const [uploadContentBtnRef, setUploadContentBtnRef] = useState<HTMLButtonElement | null>(null);
    const [emptyPlaylistItemRef, setEmptyPlaylistItemRef] = useState<HTMLLIElement | null>(null);
    const [firstPlaylistItemRef, setFirstPlaylistItemRef] = useState<HTMLLIElement | null>(null);
    const [durationRef, setDurationRef] = useState<HTMLParagraphElement | null>(null);

    const { playlistStep } = usePlaylistStep();

    const isStepOne = playlistStep.includes('one');

    // обнуляем рефы, чтобы при повторном заходе в шаг, карточка никуда не сдвинулась
    useEffect(() => {
        if (playlistStep === '' || isStepOne) {
            setDurationRef(null);
        }
    }, [isStepOne, playlistStep]);

    return {
        contentWithoutFolderRef,
        setContentWithoutFolderRef,
        uploadContentBtnRef,
        setUploadContentBtnRef,
        emptyPlaylistItemRef,
        setEmptyPlaylistItemRef,
        firstPlaylistItemRef,
        setFirstPlaylistItemRef,
        durationRef,
        setDurationRef,
    };
};
