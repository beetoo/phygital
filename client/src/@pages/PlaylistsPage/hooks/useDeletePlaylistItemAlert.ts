import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setActivePlaylistItemId, setCloseDeletePlaylistItemAlert, setDeletePlaylistItems } from '../actions';
import {
    selectActivePlaylistItemId,
    selectDeletablePlaylistItemId,
    selectDeletePlaylistItemAlertStatus,
    selectDeletePlaylistItemStatus,
    selectDeletePlaylistItemSuccessStatus,
} from '../selectors';

type ReturnValue = {
    isDeleteContentAlertOpen: boolean;
    deletePlaylistItem: boolean;
    doDeletePlaylistItem: () => void;
    closeDeleteContentAlert: () => void;
    t: TFunction;
};

export const useDeletePlaylistItemAlert = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    const dispatch = useDispatch();

    const isDeleteContentAlertOpen = useSelector(selectDeletePlaylistItemAlertStatus);
    const activePlaylistItemId = useSelector(selectActivePlaylistItemId);
    const deletablePlaylistItemId = useSelector(selectDeletablePlaylistItemId);

    const deletePlaylistItem = useSelector(selectDeletePlaylistItemStatus);
    const isDeletePlaylistItemSuccess = useSelector(selectDeletePlaylistItemSuccessStatus);

    useEffect(() => {
        if (isDeletePlaylistItemSuccess) {
            dispatch(setCloseDeletePlaylistItemAlert());
            dispatch(setActivePlaylistItemId());
        }
    }, [dispatch, isDeletePlaylistItemSuccess]);

    const doDeletePlaylistItem = useCallback(() => {
        dispatch(setDeletePlaylistItems(deletablePlaylistItemId || activePlaylistItemId));
    }, [activePlaylistItemId, deletablePlaylistItemId, dispatch]);

    const closeDeleteContentAlert = useCallback(() => {
        dispatch(setCloseDeletePlaylistItemAlert());
    }, [dispatch]);

    return {
        isDeleteContentAlertOpen,
        deletePlaylistItem,
        doDeletePlaylistItem,
        closeDeleteContentAlert,
        t,
    };
};
