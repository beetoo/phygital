import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import orderBy from 'lodash/orderBy';

import { useOpenEditPlaylistStepButton } from './useOpenEditPlaylistStepButton';
import { setActivePlaylistId, setSelectedPlaylistIds } from '../actions';
import {
    selectActivePlaylistId,
    selectActivePlaylistItemId,
    selectPlaylists,
    selectSelectedPlaylistIds,
} from '../selectors';
import { formatSecondsToTimeString } from '../../../../../common/helpers';
import { PlaylistItemType, CONTENT_TYPE } from '../../../../../common/types';

type ReturnValue = {
    activePlaylistId: string;
    isActivePlaylistItem: boolean;
    onPlaylistClick: (playlistId: string) => () => void;
    onPlaylistDoubleClick: (playlistId: string) => () => void;
    openEditPlaylistStep: () => void;
    firstImage: (playlistItems: PlaylistItemType[]) => string;
    firstItemType: (playlistItems: PlaylistItemType[]) => CONTENT_TYPE;
    getFullPlaylistTime: (playlistId: string) => string;
    isPlaylistSelected: (playlistId: string) => boolean;
    selectPlaylist: (playlistId: string) => void;
    unselectPlaylist: (playlistId: string) => void;
    getPlaylistPicturesNumber: (playlistItems: PlaylistItemType[]) => number;
    getPlaylistVideosNumber: (playlistItems: PlaylistItemType[]) => number;
    t: TFunction;
};

export const usePlaylistCard = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlists' });

    const dispatch = useDispatch();

    const activePlaylistId = useSelector(selectActivePlaylistId);
    const activePlaylistItemId = useSelector(selectActivePlaylistItemId);
    const playlists = useSelector(selectPlaylists);

    const selectedPlaylistIds = useSelector(selectSelectedPlaylistIds);

    const { openEditPlaylistStep } = useOpenEditPlaylistStepButton();

    const onPlaylistClick = useCallback(
        (playlistId: string) => () => {
            if (activePlaylistId !== playlistId) {
                dispatch(setActivePlaylistId(playlistId));
            }
        },
        [activePlaylistId, dispatch],
    );

    const onPlaylistDoubleClick = useCallback(
        (playlistId: string) => () => {
            if (selectedPlaylistIds.length === 0) {
                onPlaylistClick(playlistId)();
                openEditPlaylistStep();
            }
        },
        [onPlaylistClick, openEditPlaylistStep, selectedPlaylistIds.length],
    );

    const isPlaylistSelected = useCallback(
        (playlistId: string) => selectedPlaylistIds.includes(playlistId),
        [selectedPlaylistIds],
    );

    const selectPlaylist = useCallback(
        (playlistId: string) => {
            dispatch(setSelectedPlaylistIds([...selectedPlaylistIds, playlistId]));
        },
        [dispatch, selectedPlaylistIds],
    );

    const unselectPlaylist = useCallback(
        (playlistId: string) => {
            const filteredItems = selectedPlaylistIds.filter((id) => id !== playlistId);
            dispatch(setSelectedPlaylistIds(filteredItems));
        },
        [dispatch, selectedPlaylistIds],
    );

    const firstImage = useCallback((playlistItems: PlaylistItemType[]) => {
        const orderedPlaylistItems = orderBy(playlistItems, ['orderIndex'], ['asc']);
        if (orderedPlaylistItems.length === 0) {
            return '';
        }
        const content = orderedPlaylistItems[0].content;
        return content?.src;
    }, []);

    const firstItemType = useCallback((playlistItems: PlaylistItemType[]) => {
        const orderedPlaylistItems = orderBy(playlistItems, ['orderIndex'], ['asc']);
        if (orderedPlaylistItems.length === 0) {
            return CONTENT_TYPE.IMAGE;
        }
        const content = orderedPlaylistItems[0].content;
        return content?.type ?? CONTENT_TYPE.IMAGE;
    }, []);

    const getFullPlaylistTime = useCallback(
        (playlistId: string) => {
            const playlistItems = playlists.find(({ id }) => id === playlistId)?.playlistItems ?? [];

            const duration =
                playlistItems
                    .map(({ delay, content }) => ({
                        delay,
                        duration: content?.duration ?? 0,
                        type: content?.type ?? 'unknown',
                    }))
                    .reduce(
                        (prev, { delay, duration, type }) =>
                            type === CONTENT_TYPE.IMAGE || type === CONTENT_TYPE.WEB ? prev + delay : prev + duration,
                        0,
                    ) ?? 0;
            return formatSecondsToTimeString(duration ?? 0);
        },
        [playlists],
    );

    const getPlaylistPicturesNumber = useCallback(
        (playlistItems: PlaylistItemType[]) =>
            playlistItems.filter(({ content }) => content?.type === CONTENT_TYPE.IMAGE).length ?? 0,
        [],
    );

    const getPlaylistVideosNumber = useCallback(
        (playlistItems: PlaylistItemType[]) =>
            playlistItems.filter(({ content }) => content?.type === CONTENT_TYPE.VIDEO).length ?? 0,
        [],
    );

    const isActivePlaylistItem = activePlaylistItemId !== '';

    return {
        activePlaylistId,
        isActivePlaylistItem,
        onPlaylistClick,
        onPlaylistDoubleClick,
        openEditPlaylistStep,
        firstImage,
        firstItemType,
        getFullPlaylistTime,
        isPlaylistSelected,
        selectPlaylist,
        unselectPlaylist,
        getPlaylistPicturesNumber,
        getPlaylistVideosNumber,
        t,
    };
};
