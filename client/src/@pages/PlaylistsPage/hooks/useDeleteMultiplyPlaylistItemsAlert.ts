import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setHideModal } from '../../../components/ModalsContainer/actions';
import { selectCurrentModal } from '../../../components/ModalsContainer/selectors';
import { MODALS } from '../../../components/ModalsContainer/types';
import { setActivePlaylistItemId, setDeletePlaylistItems, setSelectedPlaylistItemIds } from '../actions';

import {
    selectDeletePlaylistItemStatus,
    selectDeletePlaylistItemSuccessStatus,
    selectSelectedPlaylistItemIds,
} from '../selectors';

type ReturnValue = {
    isDeletePlaylistItemsAlertOpen: boolean;
    selectedPlaylistItemsNumber: number;
    deletePlaylistItems: boolean;
    doDeletePlaylistItems: () => void;
    closeDeletePlaylistItemsAlert: () => void;
    t: TFunction;
};

export const useDeleteMultiplyPlaylistItemsAlert = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    const dispatch = useDispatch();

    const currentModal = useSelector(selectCurrentModal);
    const selectedPlaylistItemIds = useSelector(selectSelectedPlaylistItemIds);

    const deletePlaylistItems = useSelector(selectDeletePlaylistItemStatus);
    const isDeletePlaylistItemsSuccess = useSelector(selectDeletePlaylistItemSuccessStatus);

    useEffect(() => {
        if (isDeletePlaylistItemsSuccess) {
            dispatch(setHideModal());
            dispatch(setActivePlaylistItemId());
            dispatch(setSelectedPlaylistItemIds([]));
        }
    }, [dispatch, isDeletePlaylistItemsSuccess]);

    const doDeletePlaylistItems = useCallback(() => {
        dispatch(setDeletePlaylistItems(selectedPlaylistItemIds));
    }, [dispatch, selectedPlaylistItemIds]);

    const closeDeletePlaylistItemsAlert = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    const selectedPlaylistItemsNumber = selectedPlaylistItemIds.length;
    const isDeletePlaylistItemsAlertOpen = currentModal === MODALS.DELETE_MULTIPLY_PLAYLIST_ITEMS;

    return {
        isDeletePlaylistItemsAlertOpen,
        selectedPlaylistItemsNumber,
        deletePlaylistItems,
        doDeletePlaylistItems,
        closeDeletePlaylistItemsAlert,
        t,
    };
};
