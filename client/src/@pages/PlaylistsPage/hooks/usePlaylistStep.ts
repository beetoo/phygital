import { useLocation } from 'react-router-dom';

import { PlaylistStep } from '../types';

export const playlistSteps = [
    'creating_one',
    'creating_two',
    'creating_three',
    'editing_one',
    'editing_two',
    'editing_three',
] as PlaylistStep[];

const getPlaylistStep = (pathname: string) => {
    const splitPath = pathname.split('/');

    const playlistStep = splitPath[splitPath.length - 1] as PlaylistStep;

    return playlistSteps.includes(playlistStep) ? playlistStep : '';
};

type ReturnValue = {
    playlistStep: PlaylistStep;
    isPlaylistStepOne: boolean;
    isPlaylistStepTwo: boolean;
    isPlaylistStepThree: boolean;
    isCreatePlaylistStep: boolean;
    isEditPlaylistStep: boolean;
    isPlaylistStepModeEnabled: boolean;
};

export const usePlaylistStep = (): ReturnValue => {
    const { pathname } = useLocation();

    const isPlaylistStepOne = pathname.endsWith('one');
    const isPlaylistStepTwo = pathname.endsWith('two');
    const isPlaylistStepThree = pathname.endsWith('three');

    const playlistStep = getPlaylistStep(pathname);

    const isCreatePlaylistStep = playlistStep.includes('creating');
    const isEditPlaylistStep = playlistStep.includes('editing');

    const isPlaylistStepModeEnabled = Boolean(playlistStep);

    return {
        playlistStep,
        isPlaylistStepOne,
        isPlaylistStepTwo,
        isPlaylistStepThree,
        isCreatePlaylistStep,
        isEditPlaylistStep,
        isPlaylistStepModeEnabled,
    };
};
