import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DropResult } from '@hello-pangea/dnd';

import { selectPlaylistDto, selectPlaylistDtoScreens, selectPlaylistDtoScreensOrdered } from '../selectors';
import { selectActiveLocationId, selectLocations, selectScreens } from '../../ScreensPage/selectors';
import { setPlaylistDtoSchedules, setPlaylistDtoScreens } from '../actions';
import { getAspectRatioString } from '../../../helpers';

type ReturnValue = {
    onDragEnd: (result: DropResult) => void;
};

export const usePlaylistStepOne = (): ReturnValue => {
    const dispatch = useDispatch();

    const activeLocationId = useSelector(selectActiveLocationId);
    const screens = useSelector(selectScreens);
    const locations = useSelector(selectLocations);
    const { playlistDtoSchedules } = useSelector(selectPlaylistDto);
    const playlistDtoScreens = useSelector(selectPlaylistDtoScreens);
    const playlistDtoScreensOrdered = useSelector(selectPlaylistDtoScreensOrdered);

    const unselectedScreens = useMemo(
        () =>
            screens
                .filter(({ location }) =>
                    activeLocationId !== 'all' ? location?.id === activeLocationId : location?.id,
                )
                .filter(({ id }) => !playlistDtoScreens.includes(id)),
        [activeLocationId, screens, playlistDtoScreens],
    );

    const activeResolutionAndOrientation = useMemo(() => {
        const selectedScreens = locations
            .filter(({ id }) => (activeLocationId !== 'all' ? id === activeLocationId : id))
            .flatMap(({ screens }) => screens)
            .filter(({ id }) => playlistDtoScreens.includes(id));

        return selectedScreens.length > 0
            ? {
                  resolution: selectedScreens[0].resolution,
                  orientation: selectedScreens[0].orientation,
              }
            : {};
    }, [activeLocationId, locations, playlistDtoScreens]);

    const isScreenWithAnotherResolutionOrOrientation = useCallback(
        (screenId: string) =>
            playlistDtoScreens.length > 0 &&
            unselectedScreens
                .filter(
                    ({ resolution, orientation }) =>
                        orientation !== activeResolutionAndOrientation?.orientation ||
                        getAspectRatioString(
                            activeResolutionAndOrientation?.resolution?.width,
                            activeResolutionAndOrientation?.resolution?.height,
                        ) !== getAspectRatioString(resolution?.width, resolution?.height),
                )
                .some(({ id }) => id === screenId),
        [
            activeResolutionAndOrientation?.orientation,
            activeResolutionAndOrientation?.resolution?.height,
            activeResolutionAndOrientation?.resolution?.width,
            playlistDtoScreens.length,
            unselectedScreens,
        ],
    );

    const onDragEnd = useCallback(
        (result: DropResult) => {
            const { source, destination } = result;

            // если дроп из невыбранных скринов в выбранные скрины
            if (source?.droppableId === 'unselectedScreens' && destination?.droppableId === 'selectedScreens') {
                const { id } = unselectedScreens[source.index];
                if (!isScreenWithAnotherResolutionOrOrientation(id)) {
                    dispatch(setPlaylistDtoScreens([{ id }, ...playlistDtoScreensOrdered]));
                }
            }
        },
        [dispatch, isScreenWithAnotherResolutionOrOrientation, playlistDtoScreensOrdered, unselectedScreens],
    );

    // если кол-во экранов меняется, то playlistScheduleDto тоже меняется
    useEffect(() => {
        if (playlistDtoScreens.length < playlistDtoSchedules.length) {
            dispatch(
                setPlaylistDtoSchedules(
                    playlistDtoSchedules.filter(({ screenId }) => playlistDtoScreens.includes(screenId)),
                ),
            );
        }
    }, [dispatch, playlistDtoSchedules, playlistDtoScreens]);

    return {
        onDragEnd,
    };
};
