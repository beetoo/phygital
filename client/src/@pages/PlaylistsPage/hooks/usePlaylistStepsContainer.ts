import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
    selectActivePlaylistId,
    selectAddPlaylistSuccessStatus,
    selectStepTwoActivePlaylistItemId,
    selectStepTwoMenuMode,
    selectStepTwoSelectedContentItemIds,
    selectStepTwoSelectedPlaylistItemIds,
    selectUpdatePlaylistSuccessStatus,
} from '../selectors';
import {
    setStepTwoActivePlaylistItemId,
    setStepTwoMenuMode,
    setStepTwoSelectedContentItemIds,
    setStepTwoSelectedPlaylistItemIds,
} from '../actions';
import { usePlaylistStep } from './usePlaylistStep';
import { useCustomRouter } from '../../../hooks/useCustomRouter';

type ReturnValue = {
    isPlaylistStepOne: boolean;
    isPlaylistStepTwo: boolean;
    isPlaylistStepThree: boolean;
};

export const usePlaylistStepsContainer = (): ReturnValue => {
    const dispatch = useDispatch();

    const activePlaylistId = useSelector(selectActivePlaylistId);
    const activePlaylistItemId = useSelector(selectStepTwoActivePlaylistItemId);
    const selectedPlaylistItemIds = useSelector(selectStepTwoSelectedPlaylistItemIds);
    const selectedContentItemIds = useSelector(selectStepTwoSelectedContentItemIds);
    const menuMode = useSelector(selectStepTwoMenuMode);
    const addPlaylistSuccess = useSelector(selectAddPlaylistSuccessStatus);
    const updatePlaylistSuccess = useSelector(selectUpdatePlaylistSuccessStatus);

    const { isPlaylistStepOne, isPlaylistStepTwo, isPlaylistStepThree, isPlaylistStepModeEnabled, isEditPlaylistStep } =
        usePlaylistStep();

    const { pushToPage } = useCustomRouter();

    // обнуляем некоторые данные второго шага
    useEffect(() => {
        if (!isPlaylistStepTwo) {
            if (activePlaylistItemId) {
                dispatch(setStepTwoActivePlaylistItemId());
            }
            if (selectedContentItemIds.length > 0) {
                dispatch(setStepTwoSelectedContentItemIds());
            }

            if (selectedPlaylistItemIds.length > 0) {
                dispatch(setStepTwoSelectedPlaylistItemIds());
            }
            if (menuMode !== 'content_menu') {
                dispatch(setStepTwoMenuMode('content_menu'));
            }
        }
    }, [
        activePlaylistItemId,
        dispatch,
        isPlaylistStepTwo,
        menuMode,
        selectedContentItemIds.length,
        selectedPlaylistItemIds.length,
    ]);

    useEffect(() => {
        if (
            isPlaylistStepModeEnabled &&
            (addPlaylistSuccess || updatePlaylistSuccess || (isEditPlaylistStep && !activePlaylistId))
        ) {
            pushToPage('playlists');
        }
    }, [
        activePlaylistId,
        addPlaylistSuccess,
        isEditPlaylistStep,
        isPlaylistStepModeEnabled,
        pushToPage,
        updatePlaylistSuccess,
    ]);

    return {
        isPlaylistStepOne,
        isPlaylistStepTwo,
        isPlaylistStepThree,
    };
};
