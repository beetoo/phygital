import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import {
    selectAddingPlaylistStatus,
    selectPlaylistDto,
    selectPlaylistDtoScreens,
    selectStartPlaylistModalStatus,
} from '../selectors';
import { setAddPlaylist, setCloseStartPlaylistModal } from '../actions';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';

type ReturnValue = {
    isStartPlaylistModalOpen: boolean;
    addingPlaylist: boolean;
    savePlaylist: () => void;
    closeStartPlaylistModal: () => void;
    t: TFunction;
};

export const useStartPlaylistModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    const dispatch = useDispatch();

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const playlistDto = useSelector(selectPlaylistDto);
    const playlistDtoScreens = useSelector(selectPlaylistDtoScreens);

    const isStartPlaylistModalOpen = useSelector(selectStartPlaylistModalStatus);
    const addingPlaylist = useSelector(selectAddingPlaylistStatus);

    const savePlaylist = useCallback(() => {
        const playlistDtoItemsMinimized = playlistDto.playlistDtoItems.map(({ content, ...rest }) => ({
            content: { id: content?.id },
            ...rest,
        }));
        const playlistDtoMinimized = {
            ...playlistDto,
            playlistDtoItems: playlistDtoItemsMinimized,
            playlistDtoScreens,
        };
        dispatch(setAddPlaylist(organizationId, playlistDtoMinimized));
    }, [dispatch, organizationId, playlistDto, playlistDtoScreens]);

    const closeStartPlaylistModal = useCallback(() => {
        dispatch(setCloseStartPlaylistModal());
    }, [dispatch]);

    return {
        isStartPlaylistModalOpen,
        addingPlaylist,
        savePlaylist,
        closeStartPlaylistModal,
        t,
    };
};
