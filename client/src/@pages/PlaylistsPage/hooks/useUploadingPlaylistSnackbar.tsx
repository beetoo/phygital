import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ProviderContext, useSnackbar } from 'notistack';
import { isEqual } from 'lodash';

import { PlaylistStatus } from '../../../../../common/types';
import { setClosedUploadingPlaylistSnackbars } from '../../../components/ModalsContainer/actions';
import { PlaylistUploadingSnackbar } from '../components/PlaylistUploadingSnackbar';
import { selectClosedUploadingPlaylistSnackbars } from '../../../components/ModalsContainer/selectors';
import { selectPlaylists } from '../selectors';

export const useUploadingPlaylistSnackbar = (): void => {
    const dispatch = useDispatch();

    const playlists = useSelector(selectPlaylists);
    const closedSnackbarKeys = useSelector(selectClosedUploadingPlaylistSnackbars);

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const [playlistUploadingSnackbars] = useState(
        new Map<string, { close: ProviderContext['closeSnackbar']; status: PlaylistStatus }>(),
    );

    // show snackbars for "uploading" and "waiting for uploading" playlists and start recheck statuses interval
    useEffect(() => {
        playlists
            .filter(({ schedules }) => schedules.length > 0)
            .forEach((playlist) => {
                const close = () => {
                    playlistUploadingSnackbars.get(playlist.id)?.close();
                    playlistUploadingSnackbars.delete(playlist.id);
                };

                const uploadingSnackbarKey = `${playlist.id}:${PlaylistStatus.uploading}`;

                const isUploadingClosed = closedSnackbarKeys.includes(uploadingSnackbarKey);

                if (playlist.status === PlaylistStatus.uploading && !isUploadingClosed) {
                    if (playlistUploadingSnackbars.get(playlist.id)?.status !== PlaylistStatus.uploading) {
                        //close previous status
                        close();
                    }
                    if (!playlistUploadingSnackbars.has(playlist.id)) {
                        const closeSnackbarKey = enqueueSnackbar(
                            <PlaylistUploadingSnackbar title={playlist.title} onClose={close} />,
                            {
                                key: uploadingSnackbarKey,
                                persist: true,
                            },
                        );
                        playlistUploadingSnackbars.set(playlist.id, {
                            close: () => {
                                dispatch(
                                    setClosedUploadingPlaylistSnackbars([...closedSnackbarKeys, uploadingSnackbarKey]),
                                );
                                closeSnackbar(closeSnackbarKey);
                            },
                            status: PlaylistStatus.uploading,
                        });
                    }
                } else {
                    //close SnackBar (if exists) in all other cases
                    close();
                }
            });
        // if there are snackbars for missed playlist (playlist was deleted), close theme
        for (const playlistId of playlistUploadingSnackbars.keys()) {
            if (!playlists.find((p) => p.id === playlistId)) {
                playlistUploadingSnackbars.get(playlistId)?.close();
            }
        }
    }, [closeSnackbar, closedSnackbarKeys, dispatch, enqueueSnackbar, playlistUploadingSnackbars, playlists]);

    // обновляем данные о снекбарах в localStorage
    const prevClosedSnackbars = useRef<string[]>(closedSnackbarKeys);

    useEffect(() => {
        if (!isEqual(closedSnackbarKeys.sort(), prevClosedSnackbars.current.sort())) {
            prevClosedSnackbars.current = closedSnackbarKeys;
            window.localStorage.setItem('CLOSED_SNACKBARS', JSON.stringify(closedSnackbarKeys));
        }
    }, [closedSnackbarKeys]);

    // удаляем несуществующие плейлисты из списка снекбаров
    const playlistIds = playlists.map(({ id }) => id);
    const prevPlaylistIds = useRef<string[]>([]);

    useEffect(() => {
        if (!isEqual(playlistIds.sort(), prevPlaylistIds.current.sort())) {
            prevPlaylistIds.current = playlistIds;
            dispatch(
                setClosedUploadingPlaylistSnackbars(
                    closedSnackbarKeys.filter((snackbarKey) =>
                        playlistIds.includes(
                            snackbarKey.slice(
                                0,
                                snackbarKey.split('').findIndex((val) => val === ':'),
                            ),
                        ),
                    ),
                ),
            );
        }
    }, [closedSnackbarKeys, dispatch, playlistIds]);
};
