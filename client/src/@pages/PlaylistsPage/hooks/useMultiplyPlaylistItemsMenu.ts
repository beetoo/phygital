import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { getFilesizeString } from '../helpers';
import { selectActivePlaylistId, selectPlaylists, selectSelectedPlaylistItemIds } from '../selectors';
import { setSelectedPlaylistItemIds } from '../actions';
import { setShowModal } from '../../../components/ModalsContainer/actions';
import { MODALS } from '../../../components/ModalsContainer/types';

type ReturnValue = {
    selectedPlaylistItemsNumber: number;
    selectedItemsFilesize: string;
    unselectAllPlaylistItems: () => void;
    showDeleteMultiplyPlaylistItemsModal: () => void;
    t: TFunction;
};

export const useMultiplyPlaylistItemsMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlists' });

    const dispatch = useDispatch();

    const selectedPlaylistItemsIds = useSelector(selectSelectedPlaylistItemIds);
    const activePlaylistId = useSelector(selectActivePlaylistId);
    const playlists = useSelector(selectPlaylists);

    const playlistItems = useMemo(
        () => playlists.find(({ id }) => id === activePlaylistId)?.playlistItems ?? [],
        [activePlaylistId, playlists],
    );

    const selectedItemsFilesize = useMemo(() => {
        const filesize = playlistItems
            .filter(({ id }) => selectedPlaylistItemsIds.includes(id))
            .map(({ content }) => content?.size)
            .reduce((prev, current) => prev + current, 0);

        return getFilesizeString(filesize);
    }, [playlistItems, selectedPlaylistItemsIds]);

    const unselectAllPlaylistItems = useCallback(() => {
        dispatch(setSelectedPlaylistItemIds([]));
    }, [dispatch]);

    const showDeleteMultiplyPlaylistItemsModal = useCallback(() => {
        dispatch(setShowModal(MODALS.DELETE_MULTIPLY_PLAYLIST_ITEMS));
    }, [dispatch]);

    const selectedPlaylistItemsNumber = selectedPlaylistItemsIds.length;

    return {
        selectedPlaylistItemsNumber,
        selectedItemsFilesize,
        unselectAllPlaylistItems,
        showDeleteMultiplyPlaylistItemsModal,
        t,
    };
};
