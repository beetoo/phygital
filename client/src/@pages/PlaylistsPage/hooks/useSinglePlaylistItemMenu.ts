import { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import {
    setActivePlaylistId,
    setActivePlaylistItemId,
    setOpenDeletePlaylistItemAlert,
    setUpdatePlaylistItemName,
} from '../actions';
import { selectActivePlaylistItemId, selectPlaylists, selectUpdatingPlaylistItemNameStatus } from '../selectors';
import { PlaylistType } from '../../../../../common/types';
import { usePlaylistItemInfo } from '../../../hooks/usePlaylistItemInfo';
import { useInfoShown } from '../../../hooks/useInfoShown';

type ReturnValue = {
    playlistItemSrc: string;
    isImageType: boolean;
    playlistItemName: string;
    onPlaylistItemNameChange: (e: ChangeEvent<HTMLInputElement>) => void;
    updatePlaylistItemName: () => void;
    isUpdatePlaylistItemButtonDisabled: boolean;
    openDeleteContentAlert: () => void;
    playlistItemFileSize: string;
    playlistItemFileTypeAndExtension: string;
    playlistItemDimensions: string;
    playlistItemOrientation: string;
    playlistItemDuration: string;
    playlistItemCreatedAt: string;
    playlistItemUpdatedAt: string;
    playlistsInUsage: PlaylistType[];
    isPlaylistsListOpen: boolean;
    showHidePlaylistsList: () => void;
    goToPlaylist: (playlistId: string) => () => void;

    isInfoShown: boolean;
    isDimensionsShown: boolean;
    showHideInfo: () => void;
    t: TFunction;
};

export const useSinglePlaylistItemMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlists' });

    const dispatch = useDispatch();

    const activePlaylistItemId = useSelector(selectActivePlaylistItemId);
    const updatingPlaylistItemName = useSelector(selectUpdatingPlaylistItemNameStatus);

    const playlists = useSelector(selectPlaylists);

    const [playlistItemName, setPlaylistItemName] = useState('');
    const [isPlaylistsListOpen, setIsPlaylistsListOpen] = useState(false);

    const playlistItems = playlists.flatMap(({ playlistItems }) => playlistItems);
    const activePlaylistItem = playlistItems.find(({ id }) => id === activePlaylistItemId);

    const { isInfoShown, showHideInfo } = useInfoShown(activePlaylistItemId);

    const {
        playlistItemName: activePlaylistItemName,
        isImageType,
        playlistItemSrc,
        playlistItemFileTypeAndExtension,
        playlistItemFileSize,
        playlistItemDimensions,
        playlistItemOrientation,
        playlistItemCreatedAt,
        playlistItemUpdatedAt,
        playlistItemDuration,
    } = usePlaylistItemInfo(activePlaylistItem);

    const { playlistsInUsage } = useMemo(() => {
        const contentId = activePlaylistItem?.content?.id;
        const playlistsInUsage = playlists.filter(({ playlistItems }) =>
            playlistItems.find(({ content }) => content?.id === contentId),
        );

        return {
            playlistsInUsage,
        };
    }, [activePlaylistItem?.content?.id, playlists]);

    useEffect(() => {
        if (activePlaylistItemName) {
            setPlaylistItemName(activePlaylistItemName);
        }
    }, [activePlaylistItemName]);

    const onPlaylistItemNameChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setPlaylistItemName(value);
    }, []);

    const isUpdatePlaylistItemButtonDisabled =
        updatingPlaylistItemName ||
        activePlaylistItemName.trim().toLowerCase() === playlistItemName.trim().toLowerCase();

    const updatePlaylistItemName = useCallback(() => {
        dispatch(setUpdatePlaylistItemName({ playlistItemId: activePlaylistItemId, name: playlistItemName }));
    }, [activePlaylistItemId, dispatch, playlistItemName]);

    const openDeleteContentAlert = useCallback(() => {
        dispatch(setOpenDeletePlaylistItemAlert());
    }, [dispatch]);

    const showHidePlaylistsList = useCallback(() => {
        setIsPlaylistsListOpen((prev) => !prev);
    }, []);

    const goToPlaylist = useCallback(
        (playlistId: string) => () => {
            dispatch(setActivePlaylistItemId());
            dispatch(setActivePlaylistId(playlistId));
        },
        [dispatch],
    );

    const isDimensionsShown = useMemo(
        () => playlistItemDimensions.split('×').every((value) => Number(value) > 0),
        [playlistItemDimensions],
    );

    return {
        playlistItemSrc,
        isImageType,
        playlistItemName,
        onPlaylistItemNameChange,
        isUpdatePlaylistItemButtonDisabled,
        updatePlaylistItemName,
        openDeleteContentAlert,
        playlistItemFileSize,
        playlistItemFileTypeAndExtension,
        playlistItemDimensions,
        playlistItemOrientation,
        playlistItemDuration,
        playlistItemCreatedAt,
        playlistItemUpdatedAt,
        playlistsInUsage,
        isPlaylistsListOpen,
        showHidePlaylistsList,
        goToPlaylist,

        isInfoShown,
        isDimensionsShown,
        showHideInfo,
        t,
    };
};
