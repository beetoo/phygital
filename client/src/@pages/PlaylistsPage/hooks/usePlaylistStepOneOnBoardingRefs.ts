import { useState, SetStateAction, useEffect, Dispatch } from 'react';

import { usePlaylistStep } from './usePlaylistStep';

type ReturnValue = {
    selectScreenRef: HTMLParagraphElement | null;
    setSelectScreenRef: Dispatch<SetStateAction<HTMLParagraphElement | null>>;

    unsuitableScreenRef: HTMLLIElement | null;
    setUnsuitableScreenRef: Dispatch<SetStateAction<HTMLLIElement | null>>;
};

export const usePlaylistStepOneOnBoardingRef = (): ReturnValue => {
    // первый шаг
    const [selectScreenRef, setSelectScreenRef] = useState<HTMLParagraphElement | null>(null);
    const [unsuitableScreenRef, setUnsuitableScreenRef] = useState<HTMLLIElement | null>(null);

    const { playlistStep } = usePlaylistStep();

    const isStepTwo = playlistStep.includes('two');

    // обнуляем рефы, чтобы при повторном заходе в шаг, карточка никуда не сдвинулась
    useEffect(() => {
        if (playlistStep === '' || isStepTwo) {
            setUnsuitableScreenRef(null);
        }
    }, [isStepTwo, playlistStep]);

    return {
        selectScreenRef,
        setSelectScreenRef,
        unsuitableScreenRef,
        setUnsuitableScreenRef,
    };
};
