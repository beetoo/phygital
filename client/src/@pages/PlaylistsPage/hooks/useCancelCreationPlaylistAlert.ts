import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectCurrentModal, selectPageGoingTo } from '../../../components/ModalsContainer/selectors';
import { MODALS } from '../../../components/ModalsContainer/types';
import { setHideModal } from '../../../components/ModalsContainer/actions';
import { setStepTwoCurrentTimelineSeconds } from '../actions';
import { setSignOutProcess } from '../../../actions';
import { selectStepTwoCurrentTimelineSeconds } from '../selectors';
import { useCustomRouter } from '../../../hooks/useCustomRouter';
import { usePlaylistStep } from './usePlaylistStep';

type ReturnValue = {
    isModalOpen: boolean;
    playlistModeWords: string[];
    onCancel: () => void;
    onExit: () => void;
    t: TFunction;
};

export const useCancelCreationPlaylistAlert = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    const dispatch = useDispatch();

    const { pushToPage } = useCustomRouter();

    const { playlistStep } = usePlaylistStep();

    const stepTwoTimelineSeconds = useSelector(selectStepTwoCurrentTimelineSeconds);
    const currentModal = useSelector(selectCurrentModal);
    const pageGoingTo = useSelector(selectPageGoingTo);

    const onCancel = useCallback(() => {
        dispatch(setHideModal());
    }, [dispatch]);

    const onExit = useCallback(() => {
        if (pageGoingTo === 'signin') {
            localStorage.removeItem('lastPage');
            dispatch(setSignOutProcess());
            pushToPage('signin');
        } else if (pageGoingTo !== '') {
            // очищаем некоторые данные второго шага
            if (stepTwoTimelineSeconds > 0) {
                dispatch(setStepTwoCurrentTimelineSeconds(0));
            }

            pushToPage('playlists');
            dispatch(setHideModal());
            pushToPage(pageGoingTo);
        }
    }, [dispatch, pageGoingTo, pushToPage, stepTwoTimelineSeconds]);

    const isModalOpen = currentModal === MODALS.CANCEL_CREATION_PLAYLIST;

    const playlistModeWords = ['creating_one', 'creating_two', 'creating_three'].includes(playlistStep)
        ? [t('creation'), t('data')]
        : ['editing_one', 'editing_two', 'editing_three'].includes(playlistStep)
          ? [t('edit'), t('changes')]
          : ['', ''];

    return {
        isModalOpen,
        playlistModeWords,
        onCancel,
        onExit,
        t,
    };
};
