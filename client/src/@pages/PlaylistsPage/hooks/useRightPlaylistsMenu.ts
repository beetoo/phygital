import { useSelector } from 'react-redux';

import { useMemo } from 'react';
import {
    selectActivePlaylistId,
    selectActivePlaylistItemId,
    selectPlaylists,
    selectSelectedPlaylistIds,
    selectSelectedPlaylistItemIds,
} from '../selectors';

type ReturnValue = {
    hasNoPlaylists: boolean;
    hasSelectedPlaylist: boolean;
    hasMultiplyPlaylistsMenu: boolean;
    hasMultiplyPlaylistItemsMenu: boolean;
};

export const useRightPlaylistsMenu = (): ReturnValue => {
    const playlists = useSelector(selectPlaylists);
    const activePlaylistId = useSelector(selectActivePlaylistId);
    const activePlaylistItemId = useSelector(selectActivePlaylistItemId);
    const selectedPlaylistsIds = useSelector(selectSelectedPlaylistIds);
    const selectedPlaylistsItemIds = useSelector(selectSelectedPlaylistItemIds);

    const selectedPlaylistItems = useMemo(
        () =>
            playlists
                .find(({ id }) => id === activePlaylistId)
                ?.playlistItems.filter(({ id }) => selectedPlaylistsItemIds.includes(id)) ?? [],
        [activePlaylistId, playlists, selectedPlaylistsItemIds],
    );

    const hasNoPlaylists = playlists.length === 0;
    const hasMultiplyPlaylistsMenu = selectedPlaylistsIds.length > 0;
    const hasMultiplyPlaylistItemsMenu = selectedPlaylistItems.length > 0 && !hasMultiplyPlaylistsMenu;
    const hasSelectedPlaylist =
        activePlaylistId !== '' &&
        activePlaylistItemId === '' &&
        !hasMultiplyPlaylistsMenu &&
        !hasMultiplyPlaylistItemsMenu;

    return {
        hasNoPlaylists,
        hasSelectedPlaylist,
        hasMultiplyPlaylistsMenu,
        hasMultiplyPlaylistItemsMenu,
    };
};
