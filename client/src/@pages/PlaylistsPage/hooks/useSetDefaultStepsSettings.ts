import { useCallback, useEffect, useMemo, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { format } from 'date-fns';

import { CreatePlaylistScheduleDto } from '../../../../../server/src/models/playlist-schedule/dto/create-playlist-schedule.dto';
import {
    setPlaylistDtoInfo,
    setPlaylistDtoSchedules,
    setStepTwoActiveScenarioId,
    setStepTwoMenuMode,
    setStepTwoShowedScenariosIds,
} from '../actions';
import { selectActivePlaylistId, selectPlaylistDto, selectPlaylists, selectPlaylistDtoScreens } from '../selectors';
import { selectScreens } from '../../ScreensPage/selectors';
import {
    addTimeFromAnotherDate,
    getPlaylistDtoItemsWithScenarios,
    screenTimeValueToString,
    toDate,
} from '../../../helpers';
import { ScenarioType } from '../../../../../common/types';
import { usePlaylistStep } from './usePlaylistStep';

export const useSetDefaultStepsSettings = (): void => {
    const initialDay = format(new Date(), 'yyyy-MM-dd');

    const { isCreatePlaylistStep, isEditPlaylistStep } = usePlaylistStep();

    const dispatch = useDispatch();
    const screens = useSelector(selectScreens);
    const activePlaylistId = useSelector(selectActivePlaylistId);
    const playlists = useSelector(selectPlaylists);
    const playlistDtoScreens = useSelector(selectPlaylistDtoScreens);
    const { playlistDtoSchedules, playlistDtoItems } = useSelector(selectPlaylistDto);

    const currentPlaylist = useMemo(
        () => (isEditPlaylistStep ? playlists.find(({ id }) => activePlaylistId === id) : undefined),
        [activePlaylistId, isEditPlaylistStep, playlists],
    );

    const defaultDtoSchedule: Omit<CreatePlaylistScheduleDto, 'startDate' | 'endDate' | 'screenId'> = useMemo(
        () =>
            currentPlaylist?.schedule ?? {
                byDay: 'MO,TU,WE,TH,FR,SA,SU',
                broadcastAlways: true,
                startDay: format(new Date(), 'yyyy-MM-dd'),
                endDay: format(new Date(), 'yyyy-MM-dd'),
                startTime: '00:00',
                endTime: '00:00',
            },
        [currentPlaylist?.schedule],
    );

    const screenIdsWithoutSchedules = useMemo(
        () =>
            playlistDtoScreens.filter(
                (screenId) => !playlistDtoSchedules.some((schedule) => schedule.screenId === screenId),
            ),
        [playlistDtoSchedules, playlistDtoScreens],
    );

    const defaultSchedules = useMemo(
        () =>
            screens
                .filter(({ id }) => screenIdsWithoutSchedules.includes(id))
                .map((screen) => {
                    if (playlistDtoSchedules.length > 0) {
                        return { ...playlistDtoSchedules[0], screenId: screen.id, id: undefined };
                    }

                    const { screenStartTime, screenEndTime } = screenTimeValueToString(screen);

                    return {
                        ...defaultDtoSchedule,
                        startDate: toDate(initialDay, screenStartTime),
                        endDate: addTimeFromAnotherDate(initialDay, screenEndTime),
                        screenId: screen.id,
                    };
                }),
        [defaultDtoSchedule, initialDay, playlistDtoSchedules, screenIdsWithoutSchedules, screens],
    );

    const setCreatePlaylistDefaultSchedules = useCallback(() => {
        if (playlistDtoScreens.length > playlistDtoSchedules.length) {
            dispatch(setPlaylistDtoSchedules([...playlistDtoSchedules, ...defaultSchedules]));
        }
    }, [defaultSchedules, dispatch, playlistDtoSchedules, playlistDtoScreens.length]);

    const setEditPlaylistDefaultSchedules = useCallback(() => {
        if (playlistDtoScreens.length > playlistDtoSchedules.length) {
            dispatch(setPlaylistDtoSchedules([...playlistDtoSchedules, ...defaultSchedules]));
        }
    }, [defaultSchedules, dispatch, playlistDtoSchedules, playlistDtoScreens.length]);

    // устанавливаем начальное playlistDtoInfo

    const isDtoInfoSet = useRef(false);

    useEffect(() => {
        if (currentPlaylist && !isDtoInfoSet.current) {
            isDtoInfoSet.current = true;

            const title = currentPlaylist.title;
            const draft = currentPlaylist.draft;
            const virtualScreen = currentPlaylist?.virtualScreen;

            dispatch(setPlaylistDtoInfo({ title, draft, virtualScreen }));
        }
    }, [currentPlaylist, dispatch]);

    useEffect(() => {
        if (isCreatePlaylistStep) {
            setCreatePlaylistDefaultSchedules();
        }
        if (isEditPlaylistStep) {
            setEditPlaylistDefaultSchedules();
        }
    }, [isCreatePlaylistStep, isEditPlaylistStep, setCreatePlaylistDefaultSchedules, setEditPlaylistDefaultSchedules]);

    // если кол-во экранов меняется, то playlistScheduleDto тоже меняется
    useEffect(() => {
        if (playlistDtoScreens.length < playlistDtoSchedules.length) {
            dispatch(
                setPlaylistDtoSchedules(
                    playlistDtoSchedules.filter(({ screenId }) => playlistDtoScreens.includes(screenId)),
                ),
            );
        }
    }, [dispatch, playlistDtoSchedules, playlistDtoScreens]);

    // на шаге два по умолчанию показываются все сценарии
    const scenariosIds = useMemo(
        () =>
            (
                getPlaylistDtoItemsWithScenarios(playlistDtoItems)
                    .filter((item) => item.scenario)
                    .map((item) => item.scenario) as ScenarioType[]
            ).map(({ id }) => id),
        [playlistDtoItems],
    );

    useEffect(() => {
        if (scenariosIds.length > 0) {
            dispatch(setStepTwoShowedScenariosIds(scenariosIds));
        }
    }, [dispatch, scenariosIds]);

    // для шага два обнуляем activeScenarioId если он есть
    useEffect(() => {
        dispatch(setStepTwoActiveScenarioId());
        dispatch(setStepTwoMenuMode('content_menu'));
    }, [dispatch]);
};
