import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import {
    setActivePlaylistId,
    setClearPlaylistsSuccess,
    setPlaylistDtoInfo,
    setPlaylistDtoPlaylistItems,
    setPlaylistDtoScreens,
} from '../actions';
import { selectAddPlaylistSuccessStatus, selectPlaylists } from '../selectors';
import { AddPlaylistSnackbar } from '../components/AddPlaylistSnackbar';
import { useCustomRouter } from '../../../hooks/useCustomRouter';
import { usePlaylistStep } from './usePlaylistStep';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';

type ReturnValue = {
    isMoreThanOnePlaylist: boolean;
    startCreatingPlaylist: () => void;
    isPlaylistStepMode: boolean;
    tPlaylists: TFunction;
    tSchedule: TFunction;
};

export const usePlaylistsPage = (): ReturnValue => {
    const { t: tPlaylists } = useTranslation('translation', { keyPrefix: 'playlists' });
    const { t: tSchedule } = useTranslation('translation', { keyPrefix: 'schedule' });

    const dispatch = useDispatch();

    const { currentPage, pushToPage } = useCustomRouter();

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const { playlistStep } = usePlaylistStep();

    const { language } = useSelector(selectCurrentOrganization);
    const addPlaylistSuccess = useSelector(selectAddPlaylistSuccessStatus);
    const isMoreThanOnePlaylist = useSelector(selectPlaylists).length > 1;

    const startCreatingPlaylist = useCallback(() => {
        dispatch(setPlaylistDtoInfo({ title: language === 'ru' ? 'Новый плейлист' : 'New playlist', draft: false }));
        dispatch(setPlaylistDtoPlaylistItems());
        dispatch(setPlaylistDtoScreens());
        pushToPage('playlists/creating_one');
    }, [dispatch, language, pushToPage]);

    const isPlaylistStepMode = playlistStep !== '';

    // после успешного добавления экрана показываем Snackbar с возможностью перехода на экран
    useEffect(() => {
        if (addPlaylistSuccess) {
            // on playlists page

            const closeSnackbarKey = enqueueSnackbar(
                <AddPlaylistSnackbar playlist={addPlaylistSuccess} onClose={() => closeSnackbar(closeSnackbarKey)} />,
                {
                    key: `AddPlaylistSnackbar:${addPlaylistSuccess.id}`,
                    autoHideDuration: 3000,
                },
            );
            dispatch(setActivePlaylistId(addPlaylistSuccess.id));
        }

        dispatch(setClearPlaylistsSuccess());
    }, [addPlaylistSuccess, closeSnackbar, currentPage, dispatch, enqueueSnackbar]);

    // обнуляем addPlaylistSuccess после перехода с MainWindow
    useEffect(() => {
        if (addPlaylistSuccess) {
            dispatch(setClearPlaylistsSuccess());
        }
    }, [addPlaylistSuccess, dispatch]);

    return {
        isMoreThanOnePlaylist,
        startCreatingPlaylist,
        isPlaylistStepMode,
        tPlaylists,
        tSchedule,
    };
};
