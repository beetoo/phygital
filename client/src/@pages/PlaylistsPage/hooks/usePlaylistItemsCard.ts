import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
    selectActivePlaylistId,
    selectActivePlaylistItemId,
    selectPlaylists,
    selectSelectedPlaylistIds,
    selectSelectedPlaylistItemIds,
} from '../selectors';

import { setActivePlaylistItemId, setOpenDeletePlaylistItemAlert, setSelectedPlaylistItemIds } from '../actions';
import { setOpenContentViewModal } from '../../../components/ModalsContainer/actions';

type ReturnValue = {
    activePlaylistItemId: string;
    setActivePlaylistItem: (playlistItemId: string) => () => void;
    openContentViewModal: (playlistItemId: string) => () => void;
    isSomePlaylistItemsSelected: boolean;
    selectPlaylistItem: (playlistItemId: string) => void;
    unselectPlaylistItem: (playlistItemId: string) => void;
    isPlaylistItemSelected: (playlistItemId: string) => boolean;
    openDeletePlaylistItemAlert: (playlistItemId: string) => void;
    isSquareScreenOrientation: (dimensions: { width: number; height: number }) => boolean;
    isHorizontalScreenOrientation: (dimensions: { width: number; height: number }) => boolean;
    isPlaylistItemsDisabled: boolean;
    playlistsNumber: number;
};

export const usePlaylistItemCard = (): ReturnValue => {
    const dispatch = useDispatch();

    const activePlaylistId = useSelector(selectActivePlaylistId);
    const activePlaylistItemId = useSelector(selectActivePlaylistItemId);
    const playlists = useSelector(selectPlaylists);

    const selectedPlaylistIds = useSelector(selectSelectedPlaylistIds);
    const selectedPlaylistItemIds = useSelector(selectSelectedPlaylistItemIds);

    const playlistItems = useMemo(
        () => playlists.find(({ id }) => id === activePlaylistId)?.playlistItems ?? [],
        [activePlaylistId, playlists],
    );

    const activePlaylistSelectedItemIds = useMemo(
        () => playlistItems.filter(({ id }) => selectedPlaylistItemIds.includes(id)),
        [playlistItems, selectedPlaylistItemIds],
    );

    const isPlaylistItemsDisabled = selectedPlaylistIds.length > 0;

    const setActivePlaylistItem = useCallback(
        (playlistItemId: string) => () => {
            if (isPlaylistItemsDisabled) return;

            if (selectedPlaylistItemIds.length === 0) {
                if (activePlaylistItemId !== playlistItemId) {
                    dispatch(setActivePlaylistItemId(playlistItemId));
                }
            }
        },
        [activePlaylistItemId, dispatch, isPlaylistItemsDisabled, selectedPlaylistItemIds.length],
    );

    const openContentViewModal = useCallback(
        (playlistItemId: string) => () => {
            const currentIndex = playlistItems.findIndex(({ id }) => id === playlistItemId);
            const viewItems = playlistItems.map(({ content }) => ({
                name: content?.originFilename,
                type: content?.type,
                src: content?.src,
                dimensions: content?.dimensions,
            }));
            dispatch(setOpenContentViewModal(viewItems, currentIndex));
        },
        [dispatch, playlistItems],
    );

    const selectPlaylistItem = useCallback(
        (playlistItemId: string) => {
            dispatch(setActivePlaylistItemId());
            dispatch(setSelectedPlaylistItemIds([...selectedPlaylistItemIds, playlistItemId]));
        },
        [dispatch, selectedPlaylistItemIds],
    );

    const unselectPlaylistItem = useCallback(
        (playlistItemId: string) => {
            const filteredItems = selectedPlaylistItemIds.filter((id) => id !== playlistItemId);
            dispatch(setSelectedPlaylistItemIds(filteredItems));
        },
        [dispatch, selectedPlaylistItemIds],
    );

    const isPlaylistItemSelected = useCallback(
        (playlistItemId: string) => selectedPlaylistItemIds.includes(playlistItemId),
        [selectedPlaylistItemIds],
    );

    const isSomePlaylistItemsSelected = useMemo(
        () => activePlaylistSelectedItemIds.length > 0,
        [activePlaylistSelectedItemIds.length],
    );

    const isHorizontalScreenOrientation = useCallback(
        (dimensions: { width: number; height: number }) => dimensions?.width > dimensions?.height,
        [],
    );

    const isSquareScreenOrientation = useCallback(
        (dimensions: { width: number; height: number }) => dimensions?.width === dimensions?.height,
        [],
    );

    const openDeletePlaylistItemAlert = useCallback(
        (playlistItemId: string) => {
            dispatch(setOpenDeletePlaylistItemAlert(playlistItemId));
        },
        [dispatch],
    );

    const playlistsNumber = playlists.length;

    return {
        activePlaylistItemId,
        setActivePlaylistItem,
        openContentViewModal,
        isSomePlaylistItemsSelected,
        selectPlaylistItem,
        unselectPlaylistItem,
        isPlaylistItemSelected,
        openDeletePlaylistItemAlert,
        isSquareScreenOrientation,
        isHorizontalScreenOrientation,
        isPlaylistItemsDisabled,
        playlistsNumber,
    };
};
