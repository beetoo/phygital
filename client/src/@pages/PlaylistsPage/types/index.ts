import { PlaylistDtoInfo, PlaylistItemType, PlaylistType } from '../../../../../common/types';
import {
    setCloseDeletePlaylistItemAlert,
    setCloseDeletePlaylistAlert,
    setOpenDeletePlaylistItemAlert,
    setOpenDeletePlaylistAlert,
    setGetPlaylists,
    setGetPlaylistsSuccess,
    setGetPlaylistsFail,
    setActivePlaylistId,
    setGetPlaylistItems,
    setGetPlaylistItemsFail,
    setGetPlaylistItemsSuccess,
    setActivePlaylistItemId,
    setDeletePlaylists,
    setDeletePlaylistsSuccess,
    setDeletePlaylistsFail,
    setUpdatePlaylistTitle,
    setUpdatePlaylistTitleSuccess,
    setUpdatePlaylistTitleFail,
    setDeletePlaylistItems,
    setDeletePlaylistItemsSuccess,
    setDeletePlaylistItemsFail,
    setUpdatePlaylistItemName,
    setUpdatePlaylistItemNameSuccess,
    setUpdatePlaylistItemNameFail,
    setPlaylistDtoInfo,
    setPlaylistDtoScreens,
    setPlaylistDtoPlaylistItems,
    setAddPlaylist,
    setAddPlaylistSuccess,
    setAddPlaylistFail,
    setOpenStartPlaylistModal,
    setCloseStartPlaylistModal,
    setUpdatePlaylist,
    setUpdatePlaylistSuccess,
    setUpdatePlaylistFail,
    setSelectedPlaylistItemIds,
    setSelectedPlaylistIds,
    setClearPlaylistsSuccess,
    setPlaylistDtoFilteringTags,
    setPlaylistDtoSchedules,
    setStepTwoSelectedPlaylistItemIds,
    setStepTwoActivePlaylistItemId,
    setStepTwoMenuMode,
    setStepTwoActiveScenarioId,
    setStepTwoScenarioFilterOpen,
    setStepTwoShowedScenariosIds,
    setStepTwoSelectedContentItemIds,
    setStepTwoPlayablePlaylistItemId,
    setStepTwoCurrentTimelineSeconds,
    setStepTwoPlayMode,
    setStepTwoSliderMoving,
    setPlaylistDtoSync,
    setPlaylistDtoLog,
    setTimeLineRef,
    setContentDisplayType,
} from '../actions';
import { ErrorResponse } from '../../../types';
import { CreatePlaylistScheduleDto } from '../../../../../server/src/models/playlist/dto/create-playlist-schedule.dto';
import { UpdatePlaylistScheduleDto } from '../../../../../server/src/models/playlist/dto/update-playlist-schedule.dto';

export const initialByDaysList: ByDaysList = [
    { MO: false },
    { TU: false },
    { WE: false },
    { TH: false },
    { FR: false },
    { SA: false },
    { SU: false },
];

export const allDaysSelectedByDaysList: ByDaysList = [
    { MO: true },
    { TU: true },
    { WE: true },
    { TH: true },
    { FR: true },
    { SA: true },
    { SU: true },
];

export type ByDay = 'MO' | 'TU' | 'WE' | 'TH' | 'FR' | 'SA' | 'SU';

export type ByDaysList = [
    { MO: boolean },
    { TU: boolean },
    { WE: boolean },
    { TH: boolean },
    { FR: boolean },
    { SA: boolean },
    { SU: boolean },
];

export type PlaylistStep =
    | 'creating_one'
    | 'creating_two'
    | 'creating_three'
    | 'editing_one'
    | 'editing_two'
    | 'editing_three'
    | '';

export type PlaylistStepDto = {
    playlistDtoInfo: PlaylistDtoInfo;
    playlistDtoFilteringTags: string[];
    playlistDtoItems: PlaylistItemType[];
    playlistDtoSchedules: CreatePlaylistScheduleDto[];
    playlistDtoSync: boolean;
    playlistDtoLog: boolean;
};

export type CreatePlaylistDtoClient = {
    playlistDtoInfo: PlaylistDtoInfo;
    playlistDtoFilteringTags: string[];
    playlistDtoScreens: string[];
    playlistDtoItems: Pick<PlaylistItemType, 'id'>[];
    playlistDtoSchedules: CreatePlaylistScheduleDto[];
    playlistDtoSync: boolean;
    playlistDtoLog: boolean;
};

export interface AddPlaylistSuccess extends CreatePlaylistDtoClient {
    id: string;
}

export type UpdatePlaylistDtoClient = {
    playlistDtoInfo: PlaylistDtoInfo;
    playlistDtoFilteringTags: string[];
    playlistDtoScreens: string[];
    playlistDtoItems?: Pick<PlaylistItemType, 'id'>[];
    playlistDtoSchedules?: UpdatePlaylistScheduleDto[];
    playlistDtoSync: boolean;
    playlistDtoLog: boolean;
};

export type UpdatePlaylistTitleDto = {
    playlistId: string;
    title?: string;
    draft?: boolean;
    resolution?: { width: number; height: number };
};
export type UpdatePlaylistItemNameDto = { playlistItemId: string; name: string };

export type StepTwoMenuMode =
    | 'content_menu'
    | 'content_menu_to_scenario'
    | 'content_setting_menu'
    | 'scenario_content_setting_menu'
    | 'scenario_setting_menu'
    | 'resolution_setting_menu';

export type StepTwoPlayMode = 'play' | 'pause';

export type TriggerResolutionSelectType =
    | 'Full HD (1920 × 1080 / 16:9)'
    | 'HD (1280 × 720 / 16:9)'
    | '4K (UHD) (3840 × 2160 / 16:9)';

export type TriggerOrientationSelectType = 'Горизонтальная' | 'Вертикальная' | 'Horizontal' | 'Portrait' | '-';

export type PlaylistDtoScreen = { id: string };

export type ContentDisplayType = 'horizontal' | 'vertical';

export type PlaylistsPageState = {
    selectedPlaylistIds: string[];
    selectedPlaylistItemIds: string[];

    isDeletePlaylistAlertOpen: boolean;
    isDeletePlaylistItemAlertOpen: boolean;

    activePlaylistId: string;
    activePlaylistItemId: string;
    deletablePlaylistItemId: string;

    loadingPlaylists: boolean;
    playlists: PlaylistType[];
    loadingPlaylistsError: ErrorResponse | null;

    playlistDto: PlaylistStepDto;
    playlistDtoScreens: PlaylistDtoScreen[];

    timeLineRef: HTMLDivElement | null;
    contentDisplayType: ContentDisplayType;

    stepTwoMenuMode: StepTwoMenuMode;
    stepTwoActivePlaylistItemId: string;
    stepTwoPlayablePlaylistItemId: string;
    stepTwoActiveScenarioId: string;
    stepTwoSelectedPlaylistItemIds: string[];
    stepTwoSelectedContentItemIds: string[];
    stepTwoScenariosFilterOpen: boolean;
    stepTwoShowedScenariosIds: string[];

    stepTwoCurrentTimelineSeconds: number;
    stepTwoSliderMoving: boolean;
    stepTwoPlayMode: StepTwoPlayMode;

    isStartPlaylistModalOpen: boolean;

    addingPlaylist: boolean;
    addPlaylistSuccess: AddPlaylistSuccess | false;
    addingPlaylistError: ErrorResponse | null;

    updatingPlaylist: boolean;
    updatePlaylistSuccess: boolean;
    updatingPlaylistError: ErrorResponse | null;

    updatingPlaylistTitle: boolean;
    isUpdatePlaylistTitleSuccess: boolean;
    updatingPlaylistTitleError: ErrorResponse | null;

    deletePlaylist: boolean;
    isDeletePlaylistSuccess: boolean;
    deletePlaylistError: ErrorResponse | null;

    loadingPlaylistItems: boolean;
    playlistItems: PlaylistItemType[];
    loadingPlaylistItemsError: ErrorResponse | null;

    updatingPlaylistItemName: boolean;
    updatingPlaylistItemNameError: ErrorResponse | null;

    deletePlaylistItem: boolean;
    isDeletePlaylistItemSuccess: boolean;
    deletePlaylistItemError: ErrorResponse | null;
};

export type SetSelectedPlaylistIds = ReturnType<typeof setSelectedPlaylistIds>;
export type SetSelectedPlaylistItemIds = ReturnType<typeof setSelectedPlaylistItemIds>;

export type OpenDeletePlaylistAlertAction = ReturnType<typeof setOpenDeletePlaylistAlert>;
export type CloseDeletePlaylistAlertAction = ReturnType<typeof setCloseDeletePlaylistAlert>;

export type OpenDeletePlaylistItemAlertAction = ReturnType<typeof setOpenDeletePlaylistItemAlert>;
export type CloseDeletePlaylistItemAlertAction = ReturnType<typeof setCloseDeletePlaylistItemAlert>;

export type SetActivePlaylistId = ReturnType<typeof setActivePlaylistId>;
export type SetActivePlaylistItemId = ReturnType<typeof setActivePlaylistItemId>;

export type GetPlaylistsAction = ReturnType<typeof setGetPlaylists>;
export type GetPlaylistsSuccessAction = ReturnType<typeof setGetPlaylistsSuccess>;
export type GetPlaylistsFailAction = ReturnType<typeof setGetPlaylistsFail>;

export type SetPlaylistDtoInfoAction = ReturnType<typeof setPlaylistDtoInfo>;
export type SetPlaylistDtoFilteringTagsAction = ReturnType<typeof setPlaylistDtoFilteringTags>;
export type SetPlaylistDtoScreensAction = ReturnType<typeof setPlaylistDtoScreens>;
export type SetPlaylistDtoPlaylistItemsAction = ReturnType<typeof setPlaylistDtoPlaylistItems>;
export type SetPlaylistDtoSchedulesAction = ReturnType<typeof setPlaylistDtoSchedules>;
export type SetPlaylistDtoSyncAction = ReturnType<typeof setPlaylistDtoSync>;
export type SetPlaylistDtoLogAction = ReturnType<typeof setPlaylistDtoLog>;

export type OpenStartPlaylistModalAction = ReturnType<typeof setOpenStartPlaylistModal>;
export type CloseStartPlaylistModalAction = ReturnType<typeof setCloseStartPlaylistModal>;

export type AddPlaylistAction = ReturnType<typeof setAddPlaylist>;
export type AddPlaylistSuccessAction = ReturnType<typeof setAddPlaylistSuccess>;
export type AddPlaylistFailAction = ReturnType<typeof setAddPlaylistFail>;

export type UpdatePlaylistAction = ReturnType<typeof setUpdatePlaylist>;
export type UpdatePlaylistSuccessAction = ReturnType<typeof setUpdatePlaylistSuccess>;
export type UpdatePlaylistFailAction = ReturnType<typeof setUpdatePlaylistFail>;

export type UpdatePlaylistTitleAction = ReturnType<typeof setUpdatePlaylistTitle>;
export type UpdatePlaylistTitleSuccessAction = ReturnType<typeof setUpdatePlaylistTitleSuccess>;
export type UpdatePlaylistTitleFailAction = ReturnType<typeof setUpdatePlaylistTitleFail>;

export type DeletePlaylistAction = ReturnType<typeof setDeletePlaylists>;
export type DeletePlaylistSuccessAction = ReturnType<typeof setDeletePlaylistsSuccess>;
export type DeletePlaylistFailAction = ReturnType<typeof setDeletePlaylistsFail>;

export type GetPlaylistItemsAction = ReturnType<typeof setGetPlaylistItems>;
export type GetPlaylistItemsSuccessAction = ReturnType<typeof setGetPlaylistItemsSuccess>;
export type GetPlaylistItemsFailAction = ReturnType<typeof setGetPlaylistItemsFail>;

export type UpdatePlaylistItemNameAction = ReturnType<typeof setUpdatePlaylistItemName>;
export type UpdatePlaylistItemNameSuccessAction = ReturnType<typeof setUpdatePlaylistItemNameSuccess>;
export type UpdatePlaylistItemNameFailAction = ReturnType<typeof setUpdatePlaylistItemNameFail>;

export type DeletePlaylistItemsAction = ReturnType<typeof setDeletePlaylistItems>;
export type DeletePlaylistItemsSuccessAction = ReturnType<typeof setDeletePlaylistItemsSuccess>;
export type DeletePlaylistItemsFailAction = ReturnType<typeof setDeletePlaylistItemsFail>;

export type ClearPlaylistsSuccessAction = ReturnType<typeof setClearPlaylistsSuccess>;

export type SetStepTwoMenuModeAction = ReturnType<typeof setStepTwoMenuMode>;
export type SetStepTwoActivePlaylistItemIdAction = ReturnType<typeof setStepTwoActivePlaylistItemId>;
export type SetStepTwoActivePlayableItemIdAction = ReturnType<typeof setStepTwoPlayablePlaylistItemId>;
export type SetStepTwoActiveScenarioIdAction = ReturnType<typeof setStepTwoActiveScenarioId>;
export type SetStepTwoSelectedPlaylistItemIdsAction = ReturnType<typeof setStepTwoSelectedPlaylistItemIds>;
export type SetStepTwoSelectedContentItemIdsAction = ReturnType<typeof setStepTwoSelectedContentItemIds>;
export type SetStepTwoScenariosFilterOpen = ReturnType<typeof setStepTwoScenarioFilterOpen>;
export type SetStepTwoShowedScenariosIds = ReturnType<typeof setStepTwoShowedScenariosIds>;

export type SetStepTwoPlayMode = ReturnType<typeof setStepTwoPlayMode>;
export type SetStepTwoSliderMoving = ReturnType<typeof setStepTwoSliderMoving>;
export type SetStepTwoCurrentTimelineSeconds = ReturnType<typeof setStepTwoCurrentTimelineSeconds>;

export type SetTimeLineRef = ReturnType<typeof setTimeLineRef>;
export type SetContentDisplayType = ReturnType<typeof setContentDisplayType>;

export type PlaylistsAction =
    | SetSelectedPlaylistIds
    | SetSelectedPlaylistItemIds
    | OpenDeletePlaylistAlertAction
    | CloseDeletePlaylistAlertAction
    | OpenDeletePlaylistItemAlertAction
    | CloseDeletePlaylistItemAlertAction
    | SetActivePlaylistId
    | SetActivePlaylistItemId
    | GetPlaylistsAction
    | GetPlaylistsSuccessAction
    | GetPlaylistsFailAction
    | SetPlaylistDtoInfoAction
    | SetPlaylistDtoFilteringTagsAction
    | SetPlaylistDtoScreensAction
    | SetPlaylistDtoPlaylistItemsAction
    | SetPlaylistDtoSchedulesAction
    | SetPlaylistDtoSyncAction
    | SetPlaylistDtoLogAction
    | OpenStartPlaylistModalAction
    | CloseStartPlaylistModalAction
    | AddPlaylistAction
    | AddPlaylistSuccessAction
    | AddPlaylistFailAction
    | UpdatePlaylistAction
    | UpdatePlaylistSuccessAction
    | UpdatePlaylistFailAction
    | UpdatePlaylistTitleAction
    | UpdatePlaylistTitleSuccessAction
    | UpdatePlaylistTitleFailAction
    | DeletePlaylistAction
    | DeletePlaylistSuccessAction
    | DeletePlaylistFailAction
    | GetPlaylistItemsAction
    | GetPlaylistItemsSuccessAction
    | GetPlaylistItemsFailAction
    | UpdatePlaylistItemNameAction
    | UpdatePlaylistItemNameSuccessAction
    | UpdatePlaylistItemNameFailAction
    | DeletePlaylistItemsAction
    | DeletePlaylistItemsSuccessAction
    | DeletePlaylistItemsFailAction
    | ClearPlaylistsSuccessAction
    | SetStepTwoMenuModeAction
    | SetStepTwoActivePlaylistItemIdAction
    | SetStepTwoActivePlayableItemIdAction
    | SetStepTwoActiveScenarioIdAction
    | SetStepTwoSelectedPlaylistItemIdsAction
    | SetStepTwoSelectedContentItemIdsAction
    | SetStepTwoScenariosFilterOpen
    | SetStepTwoShowedScenariosIds
    | SetStepTwoPlayMode
    | SetStepTwoSliderMoving
    | SetStepTwoCurrentTimelineSeconds
    | SetTimeLineRef
    | SetContentDisplayType;
