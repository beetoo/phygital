import { TFunction } from 'i18next';
import { isMatch } from 'date-fns';

import { CreatePlaylistScheduleDto } from '../../../../../server/src/models/playlist-schedule/dto/create-playlist-schedule.dto';
import { ByDaysList, initialByDaysList } from '../types';
import { ContentType, PlaylistItemType, PlaylistScheduleType } from '../../../../../common/types';

export const getResolutionType = ({ width, height }: { width: number; height: number }): string => {
    if (width === 1920 && height === 1080) {
        return 'Full HD';
    } else if (width === 1280 && height === 720) {
        return 'HD Ready';
    } else if (width === 3840 && height === 2160) {
        return 'UHD';
    } else if (width === 4096 && height === 3072) {
        return '4K';
    } else {
        return 'Custom Resolution';
    }
};

export const timeStringToSeconds = (timeString: string): number => {
    const hours = Number(timeString.slice(0, 2));
    const minutes = Number(timeString.slice(3, 5));
    const seconds = Number(timeString.slice(6));

    return hours * 3600 + minutes * 60 + seconds;
};

export const getFilesizeString = (size: number): string => `${Math.round((size / 1024 / 1024) * 100) / 100} MB`;

export const getCreateOrUpdateTimeString = (time: Date | string): string => {
    const date = new Date(time || new Date());

    const dateString = date.toLocaleDateString();
    const timeString = date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });

    return `${dateString} в ${timeString}`;
};

export const getOrientationString = (width: number, height: number, t: TFunction): string =>
    width === height ? t('square') : width > height ? t('horizontal') : t('portrait');

export const getFileTypeAndExtensionString = (name: string, type: string, t: TFunction): string => {
    const extension = (name.match(/[a-zA-Z\d]+$/) ?? [])[0] ?? '';
    const filetype = type === 'image' ? t('image') : t('video');

    return `${filetype} ${extension.toUpperCase()}`;
};

export const getPlaylistStatus = (status: string, t: TFunction): string => {
    switch (status) {
        case 'initial':
            return t('loadingIsExpected');
        case 'uploading':
            return t('loading');
        case 'upload_error':
            return t('error');
        case 'upload_success':
            return t('active');
        case 'draft':
            return t('draft');
        default:
            return t('unknown');
    }
};

export const sortArrayOfIds = (arr: string[] | undefined): string[] => arr?.sort((a, b) => (a > b ? 1 : -1)) ?? [];

export const formatPlaylistItems = (
    playlistItems: PlaylistItemType[] | undefined,
): Pick<PlaylistItemType, ('name' | 'delay' | 'orderIndex') & { content: Pick<ContentType, 'id'> }>[] =>
    playlistItems
        ?.sort((a, b) => a.orderIndex - b.orderIndex)
        ?.map(({ name, delay, content, scenario }, index) => ({
            name,
            delay,
            orderIndex: index,
            content: { id: content?.id },
            scenario,
        }))
        .sort((a, b) => (a.name > b.name ? 1 : -1)) ?? [];

export const formatPlaylistSchedules = (schedules: PlaylistScheduleType[]): CreatePlaylistScheduleDto[] =>
    schedules.map(({ screen, ...rest }) => ({
        screenId: screen?.id ?? '',
        ...rest,
    })) ?? [];

export const byDaysValueToByDaysString = (byDaysList: ByDaysList): string =>
    byDaysList
        .filter((dayValue) => Object.values(dayValue)[0])
        .map((dayValue) => Object.keys(dayValue)[0])
        .join(',');

export const byDaysStringToByDaysValue = (byDaysString: string): ByDaysList => {
    const daysList = byDaysString ? byDaysString.split(',') : [];

    return initialByDaysList.map((dayValue) =>
        daysList.includes(Object.keys(dayValue)[0]) ? { [Object.keys(dayValue)[0]]: true } : dayValue,
    ) as ByDaysList;
};

export const TWENTY_FOUR_HOURS_TIME_REGEX = /([01][0-9]|2[0-3]):([0-5][0-9])/;
export const TWENTY_FOUR_HOURS_WITH_SECONDS_TIME_REGEX = /([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])/;

export const isDateMatch = (value: string): boolean => isMatch(value, 'yyyy-MM-dd');
export const isTimeMatch = (value: string): boolean => value.match(TWENTY_FOUR_HOURS_TIME_REGEX) !== null;
