import { Dispatch, SetStateAction, useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
    selectContentDisplayType,
    selectPlaylistDto,
    selectPlaylistDtoScreens,
    selectStepTwoPlayablePlaylistItemId,
    selectStepTwoScenariosFilterOpen,
    selectStepTwoSelectedPlaylistItemIds,
    selectStepTwoShowedScenariosIds,
} from '../../../selectors';
import { PlaylistItemType, ScenarioType, CONTENT_TYPE, OrientationEnum } from '../../../../../../../common/types';
import { StepTwoMenuMode, TriggerResolutionSelectType } from '../../../types';
import {
    setContentDisplayType,
    setPlaylistDtoInfo,
    setStepTwoActivePlaylistItemId,
    setStepTwoMenuMode,
    setStepTwoScenarioFilterOpen,
    setStepTwoSelectedContentItemIds,
    setStepTwoSelectedPlaylistItemIds,
    setStepTwoShowedScenariosIds,
} from '../../../actions';
import { getAspectRatioWidthAndHeight, getPlaylistDtoItemsWithScenarios } from '../../../../../helpers';
import { resolutionValueToTriggerResolutionType } from '../helpers';
import { selectScreens } from '../../../../ScreensPage/selectors';
import { useElementDimensions } from '../../../../../hooks/useElementDimensions';

export type ScenarioWithPlaylistItems = {
    orderIndex: number;
    scenario: ScenarioType;
    playlistItems: PlaylistItemType[];
};

export type PlaylistItemWithScenario = PlaylistItemType | ScenarioWithPlaylistItems;

type ReturnValue = {
    isImageType: boolean;
    isVideoType: boolean;
    isWebType: boolean;
    playlistItemSrc: string;
    setMenuMode: (menuMode: StepTwoMenuMode) => void;
    isPlaylistItemsExist: boolean;
    isSelectedPlaylistItemsExist: boolean;
    selectUnselectAllPlaylistItems: () => void;
    clickOnViewArea: () => void;

    isVerticalTimeLine: boolean;
    setHorizontalTimeLineType: () => void;
    setVerticalTimeLineType: () => void;

    scenariosList: ScenarioType[];
    isScenariosFilterOpen: boolean;
    openCloseScenariosFilter: () => void;
    isScenarioShown: (scenarioId: string) => boolean;
    selectUnselectScenario: (scenarioId: string) => () => void;
    isAllScenariosShown: boolean;
    selectUnselectAllScenarios: () => void;
    isContentShown: boolean;
    shownPlaylistItemsNumber: number;

    setVirtualScreenArea: Dispatch<SetStateAction<HTMLDivElement | null>>;
    virtualResolution: TriggerResolutionSelectType | string;
    isScreensSelected: boolean;
    virtualScreenWidth: number | undefined;
    virtualScreenHeight: number | undefined;
};

export const useStepTwoPlaylistItems = (): ReturnValue => {
    const dispatch = useDispatch();

    const { playlistDtoInfo, playlistDtoItems } = useSelector(selectPlaylistDto);
    const playlistDtoScreens = useSelector(selectPlaylistDtoScreens);
    const screens = useSelector(selectScreens);
    const selectedPlaylistItemIds = useSelector(selectStepTwoSelectedPlaylistItemIds);
    const playablePlaylistItemId = useSelector(selectStepTwoPlayablePlaylistItemId);
    const showedScenariosIds = useSelector(selectStepTwoShowedScenariosIds);

    const isVerticalTimeLine = useSelector(selectContentDisplayType) === 'vertical';

    const setHorizontalTimeLineType = useCallback(() => {
        dispatch(setContentDisplayType('horizontal'));
    }, [dispatch]);

    const setVerticalTimeLineType = useCallback(() => {
        dispatch(setContentDisplayType('vertical'));
    }, [dispatch]);

    const showedPlaylistItems = useMemo(
        () => playlistDtoItems.filter((item) => (item.scenario ? showedScenariosIds.includes(item.scenario.id) : true)),
        [playlistDtoItems, showedScenariosIds],
    );

    const shownPlaylistItemsNumber = useMemo(
        () => getPlaylistDtoItemsWithScenarios(showedPlaylistItems).length,
        [showedPlaylistItems],
    );

    const isPlaylistItemsExist = playlistDtoItems.length > 0;
    const isSelectedPlaylistItemsExist = selectedPlaylistItemIds.length > 0;

    const { isImageType, isVideoType, isWebType, playlistItemSrc } = useMemo(() => {
        const {
            content: { type, src },
        } = showedPlaylistItems.find(({ id }) => id === playablePlaylistItemId) ?? {
            content: { type: '', src: '' },
        };

        return {
            isImageType: type === CONTENT_TYPE.IMAGE,
            isVideoType: type === CONTENT_TYPE.VIDEO,
            isWebType: type === CONTENT_TYPE.WEB,
            playlistItemSrc: src,
        };
    }, [playablePlaylistItemId, showedPlaylistItems]);

    const isContentShown = useMemo(
        () => Boolean(playablePlaylistItemId) || selectedPlaylistItemIds.length > 0,
        [playablePlaylistItemId, selectedPlaylistItemIds.length],
    );

    const setMenuMode = useCallback(
        (menuMode: StepTwoMenuMode) => {
            if (isSelectedPlaylistItemsExist) return;
            dispatch(setStepTwoMenuMode(menuMode));
        },
        [dispatch, isSelectedPlaylistItemsExist],
    );

    const selectUnselectAllPlaylistItems = useCallback(() => {
        if (isSelectedPlaylistItemsExist) {
            dispatch(setStepTwoSelectedPlaylistItemIds());
        } else {
            dispatch(setStepTwoSelectedPlaylistItemIds(playlistDtoItems.map(({ id }) => id)));
        }
    }, [dispatch, isSelectedPlaylistItemsExist, playlistDtoItems]);

    const isScenariosFilterOpen = useSelector(selectStepTwoScenariosFilterOpen);

    const scenariosList = useMemo(
        () =>
            getPlaylistDtoItemsWithScenarios(playlistDtoItems)
                .filter((item) => item.scenario)
                .map((item) => item.scenario) as ScenarioType[],
        [playlistDtoItems],
    );

    const openCloseScenariosFilter = useCallback(() => {
        dispatch(setStepTwoScenarioFilterOpen(!isScenariosFilterOpen));
    }, [dispatch, isScenariosFilterOpen]);

    const closeScenariosFilter = useCallback(() => {
        if (isScenariosFilterOpen) {
            dispatch(setStepTwoScenarioFilterOpen(false));
        }
    }, [dispatch, isScenariosFilterOpen]);

    const isScenarioShown = useCallback(
        (scenarioId: string) => showedScenariosIds.includes(scenarioId),
        [showedScenariosIds],
    );

    const selectUnselectScenario = useCallback(
        (scenarioId: string) => () => {
            dispatch(
                setStepTwoShowedScenariosIds(
                    isScenarioShown(scenarioId)
                        ? showedScenariosIds.filter((id) => id !== scenarioId)
                        : [...showedScenariosIds, scenarioId],
                ),
            );
        },
        [dispatch, isScenarioShown, showedScenariosIds],
    );

    const isAllScenariosShown = useMemo(
        () => scenariosList.length > 0 && scenariosList.length === showedScenariosIds.length,
        [scenariosList.length, showedScenariosIds.length],
    );

    const selectUnselectAllScenarios = useCallback(() => {
        dispatch(setStepTwoShowedScenariosIds(isAllScenariosShown ? [] : scenariosList.map(({ id }) => id)));
    }, [dispatch, isAllScenariosShown, scenariosList]);

    const clickOnViewArea = useCallback(() => {
        if (isSelectedPlaylistItemsExist) return;

        dispatch(setStepTwoActivePlaylistItemId());
        dispatch(setStepTwoSelectedContentItemIds());
        dispatch(setStepTwoMenuMode('content_menu'));
    }, [dispatch, isSelectedPlaylistItemsExist]);

    // закрытие списка сценариев при клике в любом месте
    useEffect(() => {
        if (isScenariosFilterOpen) {
            document.addEventListener('click', closeScenariosFilter);

            return () => {
                document.removeEventListener('click', closeScenariosFilter);
            };
        }
    }, [closeScenariosFilter, dispatch, isScenariosFilterOpen]);

    const { dtoResolution, dtoResolutionWidth, dtoResolutionHeight, dtoOrientation } = useMemo(() => {
        const { virtualScreen } = playlistDtoInfo;
        const dtoResolution = virtualScreen?.resolution ?? { width: 1920, height: 1080 };
        const dtoResolutionWidth = dtoResolution?.width;
        const dtoResolutionHeight = dtoResolution?.height;
        const dtoOrientation = virtualScreen?.orientation ?? OrientationEnum.LANDSCAPE;

        return { dtoResolution, dtoResolutionWidth, dtoResolutionHeight, dtoOrientation };
    }, [playlistDtoInfo]);

    const virtualResolution = useMemo(
        () =>
            resolutionValueToTriggerResolutionType(
                dtoResolution ?? { width: 1920, height: 1080 },
                dtoOrientation === OrientationEnum.LANDSCAPE,
            ),
        [dtoOrientation, dtoResolution],
    );

    const isScreensSelected = playlistDtoScreens.length > 0;

    // если на первом шаге выбраны экраны с определенными разрешением и ориентацией,
    // то на втором шаге автоматически выбираем эти разрешение и ориентацию

    const { currentScreenResolution, currentScreenOrientation } = useMemo(() => {
        const screenId = playlistDtoScreens[0] ?? '';
        const screen = screens.find(({ id }) => id === screenId);

        return {
            currentScreenResolution: screen?.resolution,
            currentScreenOrientation: screen?.orientation,
        };
    }, [playlistDtoScreens, screens]);

    const isSelectDisabled = playlistDtoScreens.length > 0;

    useEffect(() => {
        const width = currentScreenResolution?.width;
        const height = currentScreenResolution?.height;

        const isChangeRequired =
            currentScreenResolution &&
            currentScreenOrientation &&
            (dtoResolutionHeight !== height ||
                dtoResolutionWidth !== width ||
                dtoOrientation !== currentScreenOrientation);

        if (isSelectDisabled && isChangeRequired) {
            dispatch(
                setPlaylistDtoInfo({
                    ...playlistDtoInfo,
                    virtualScreen: { resolution: currentScreenResolution, orientation: currentScreenOrientation },
                }),
            );
        }
    }, [
        currentScreenOrientation,
        currentScreenResolution,
        dispatch,
        dtoOrientation,
        dtoResolutionHeight,
        dtoResolutionWidth,
        isSelectDisabled,
        playlistDtoInfo,
    ]);

    const [virtualScreenArea, setVirtualScreenArea] = useState<HTMLDivElement | null>(null);

    const { width: maxWidth, height: maxHeight, isResizing } = useElementDimensions(virtualScreenArea);

    const { width: virtualScreenWidth, height: virtualScreenHeight } = useMemo(() => {
        const isHorizontal = dtoOrientation.includes(OrientationEnum.LANDSCAPE);

        const width = isHorizontal ? dtoResolutionWidth : dtoResolutionHeight;
        const height = isHorizontal ? dtoResolutionHeight : dtoResolutionWidth;

        return getAspectRatioWidthAndHeight(width, height, { maxWidth, maxHeight, isResizing });
    }, [dtoOrientation, dtoResolutionHeight, dtoResolutionWidth, isResizing, maxHeight, maxWidth]);

    return {
        isImageType,
        isVideoType,
        isWebType,
        playlistItemSrc,
        setMenuMode,
        isPlaylistItemsExist,
        isSelectedPlaylistItemsExist,
        selectUnselectAllPlaylistItems,
        scenariosList,
        isVerticalTimeLine,
        setHorizontalTimeLineType,
        setVerticalTimeLineType,
        isScenariosFilterOpen,
        openCloseScenariosFilter,
        isScenarioShown,
        selectUnselectScenario,
        isAllScenariosShown,
        selectUnselectAllScenarios,
        isContentShown,
        shownPlaylistItemsNumber,
        clickOnViewArea,
        setVirtualScreenArea,
        virtualResolution,
        isScreensSelected,
        virtualScreenWidth,
        virtualScreenHeight,
    };
};
