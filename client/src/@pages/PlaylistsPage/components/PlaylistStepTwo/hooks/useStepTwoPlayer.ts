import { RefObject, useCallback, useEffect, useMemo, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import accurateInterval, { AccurateInterval } from 'accurate-interval';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { CONTENT_TYPE } from '../../../../../../../common/types';
import {
    selectPlaylistDto,
    selectStepTwoCurrentTimelineSeconds,
    selectStepTwoPlayablePlaylistItemId,
    selectStepTwoPlayMode,
    selectStepTwoShowedScenariosIds,
    selectStepTwoSliderMoving,
} from '../../../selectors';
import {
    setStepTwoCurrentTimelineSeconds,
    setStepTwoPlayablePlaylistItemId,
    setStepTwoPlayMode,
} from '../../../actions';
import { StepTwoPlayMode } from '../../../types';
import { formatSecondsToTimeString } from '../../../../../../../common/helpers';
import { SLIDER_STEP_VALUE } from '../constants';

type ReturnValue = {
    videoRef: RefObject<HTMLVideoElement>;
    playMode: StepTwoPlayMode;
    onPlayClick: () => void;
    onPauseClick: () => void;
    onBackClick: () => void;
    currentPlaylistTime: string;
    fullPlaylistTime: string;
    isPlayerDisabled: boolean;
    isBackButtonDisabled: boolean;
    t: TFunction;
};

export const useStepTwoPlayer = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const videoRef = useRef<HTMLVideoElement>(null);

    const { playlistDtoItems } = useSelector(selectPlaylistDto);
    const showedScenariosIds = useSelector(selectStepTwoShowedScenariosIds);
    const playablePlaylistItemId = useSelector(selectStepTwoPlayablePlaylistItemId);
    const currentTimelineSeconds = useSelector(selectStepTwoCurrentTimelineSeconds);
    const playMode = useSelector(selectStepTwoPlayMode);
    const sliderMoving = useSelector(selectStepTwoSliderMoving);

    const showedPlaylistItems = useMemo(
        () => playlistDtoItems.filter((item) => (item.scenario ? showedScenariosIds.includes(item.scenario.id) : true)),
        [playlistDtoItems, showedScenariosIds],
    );

    const currentPlaylistTime = useMemo(
        () => formatSecondsToTimeString(currentTimelineSeconds),
        [currentTimelineSeconds],
    );

    const totalTimelineSeconds = useMemo(
        () =>
            showedPlaylistItems
                .map(({ content, delay }) =>
                    content?.type === CONTENT_TYPE.IMAGE || content?.type === CONTENT_TYPE.WEB
                        ? delay
                        : (content?.duration ?? 0),
                )
                .reduce((acc, curr) => acc + curr, 0),
        [showedPlaylistItems],
    );

    const fullPlaylistTime = useMemo(() => formatSecondsToTimeString(totalTimelineSeconds), [totalTimelineSeconds]);

    const playablePlaylistItem = useMemo(() => {
        let fromSeconds = 0;
        let duration = 0;
        const playablePlaylistItem = showedPlaylistItems.find(({ content, delay }) => {
            duration =
                content?.type === CONTENT_TYPE.IMAGE || content?.type === CONTENT_TYPE.WEB ? delay : content?.duration;

            const from = fromSeconds;
            const toSeconds = fromSeconds + duration;
            fromSeconds += duration;
            return from <= currentTimelineSeconds && currentTimelineSeconds < toSeconds;
        });
        return playablePlaylistItem ? { ...playablePlaylistItem, fromSeconds: fromSeconds - duration } : undefined;
    }, [currentTimelineSeconds, showedPlaylistItems]);

    useEffect(() => {
        if (playablePlaylistItem) {
            if (playablePlaylistItem.id !== playablePlaylistItemId) {
                dispatch(setStepTwoPlayablePlaylistItemId(playablePlaylistItem.id));
            }
        }
    }, [dispatch, playablePlaylistItem, playablePlaylistItemId]);

    useEffect(() => {
        if (showedPlaylistItems.length === 0) {
            dispatch(setStepTwoPlayablePlaylistItemId());
        }
    }, [dispatch, showedPlaylistItems.length]);

    useEffect(() => {
        const isVideoType = playablePlaylistItem?.content?.type === CONTENT_TYPE.VIDEO;
        const fromSeconds = playablePlaylistItem?.fromSeconds ?? 0;

        const currentContentTime = currentTimelineSeconds - fromSeconds;

        if (videoRef.current !== null) {
            if (sliderMoving) {
                videoRef.current.currentTime = currentContentTime;
            }

            if (isVideoType) {
                if (playMode === 'play') {
                    if (videoRef.current.paused) {
                        videoRef.current.play();
                    }
                }

                if (playMode === 'pause') {
                    if (videoRef.current.currentTime !== currentContentTime) {
                        videoRef.current.currentTime = currentContentTime;
                    }

                    if (!videoRef.current.paused) {
                        videoRef.current.pause();
                    }
                }
            }
        }
    }, [
        currentTimelineSeconds,
        playMode,
        playablePlaylistItem?.content?.type,
        playablePlaylistItem?.fromSeconds,
        sliderMoving,
    ]);

    useEffect(() => {
        let interval: AccurateInterval;
        if (playMode === 'play') {
            interval = accurateInterval(
                () => {
                    dispatch(setStepTwoCurrentTimelineSeconds(currentTimelineSeconds + SLIDER_STEP_VALUE));
                },
                1000 * SLIDER_STEP_VALUE,
                { aligned: true, immediate: true },
            );
        }

        return () => {
            if (interval) {
                interval.clear();
            }
        };
    }, [currentTimelineSeconds, dispatch, playMode]);

    const isEndOfTimeline = currentTimelineSeconds >= totalTimelineSeconds;

    useEffect(() => {
        if (playMode === 'play' && isEndOfTimeline) {
            dispatch(setStepTwoPlayMode('pause'));
            dispatch(setStepTwoCurrentTimelineSeconds(0));
        }
    }, [dispatch, isEndOfTimeline, playMode]);

    const onPlayClick = useCallback(() => {
        dispatch(setStepTwoPlayMode('play'));
        if (isEndOfTimeline) {
            dispatch(setStepTwoCurrentTimelineSeconds(0));
        }
    }, [dispatch, isEndOfTimeline]);

    const onPauseClick = useCallback(() => {
        dispatch(setStepTwoPlayMode('pause'));
    }, [dispatch]);

    const onBackClick = useCallback(() => {
        dispatch(setStepTwoCurrentTimelineSeconds(0));
    }, [dispatch]);

    const isPlayerDisabled = showedPlaylistItems.length === 0;
    const isBackButtonDisabled = isPlayerDisabled || currentTimelineSeconds === 0;

    return {
        videoRef,
        currentPlaylistTime,
        fullPlaylistTime,
        playMode,
        onPlayClick,
        onPauseClick,
        onBackClick,
        isPlayerDisabled,
        isBackButtonDisabled,
        t,
    };
};
