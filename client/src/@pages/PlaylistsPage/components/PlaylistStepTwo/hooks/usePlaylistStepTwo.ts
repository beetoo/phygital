import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DropResult } from '@hello-pangea/dnd';

import {
    selectPlaylistDto,
    selectStepTwoSelectedPlaylistItemIds,
    selectStepTwoMenuMode,
    selectStepTwoSelectedContentItemIds,
} from '../../../selectors';
import {
    setPlaylistDtoPlaylistItems,
    setStepTwoActivePlaylistItemId,
    setStepTwoMenuMode,
    setStepTwoSelectedContentItemIds,
} from '../../../actions';
import { getReorderedItemsWithScenarios } from '../../../../../helpers';
import { useAddContentToPlaylist } from './useAddContentToPlaylist';
import { selectActiveFolderId } from '../../../../ContentPage/selectors';
import { useTimeLineRef } from './useTimeLIneRef';

type ReturnType = {
    onDragEnd: (result: DropResult) => void;
    onStepTwoAreaClick: () => void;
};

export const usePlaylistStepTwo = (): ReturnType => {
    const dispatch = useDispatch();

    const menuMode = useSelector(selectStepTwoMenuMode);
    const selectedContentItemIds = useSelector(selectStepTwoSelectedContentItemIds);
    const { playlistDtoItems } = useSelector(selectPlaylistDto);
    const activeFolderId = useSelector(selectActiveFolderId);

    const selectedPlaylistItemIds = useSelector(selectStepTwoSelectedPlaylistItemIds);

    const { addContentToPlaylist } = useAddContentToPlaylist(activeFolderId);

    const { scrollToRightOrBottom } = useTimeLineRef();

    const onDragEnd = useCallback(
        (result: DropResult) => {
            const { source, destination } = result;

            // если дроп из контента на виртуальный экран
            if (source?.droppableId === 'content' && destination?.droppableId === 'virtualScreen') {
                addContentToPlaylist({ sourceIndex: source.index, destinationIndex: playlistDtoItems.length });
                scrollToRightOrBottom();
            }

            // если дроп из контента в items
            if (source?.droppableId === 'content' && destination?.droppableId === 'playlistItems') {
                addContentToPlaylist({ sourceIndex: source.index, destinationIndex: destination.index });
            }

            // если дроп из items в items - reorder
            if (source?.droppableId === 'playlistItems' && destination?.droppableId === 'playlistItems') {
                const reorderedItems = getReorderedItemsWithScenarios({
                    playlistDtoItems,
                    from: source.index,
                    to: destination.index,
                });

                dispatch(setPlaylistDtoPlaylistItems(reorderedItems));
            }
        },
        [addContentToPlaylist, dispatch, playlistDtoItems, scrollToRightOrBottom],
    );

    const onStepTwoAreaClick = useCallback(() => {
        if (selectedPlaylistItemIds.length > 0) return;
        if (menuMode === 'content_menu' && selectedContentItemIds.length > 0) return;

        dispatch(setStepTwoActivePlaylistItemId());
        dispatch(setStepTwoSelectedContentItemIds());
        dispatch(setStepTwoMenuMode('content_menu'));
    }, [dispatch, menuMode, selectedContentItemIds.length, selectedPlaylistItemIds.length]);

    return {
        onDragEnd,
        onStepTwoAreaClick,
    };
};
