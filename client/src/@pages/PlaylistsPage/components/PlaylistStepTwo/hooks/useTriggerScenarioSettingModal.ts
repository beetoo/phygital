import { Dispatch, SetStateAction, useCallback, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { ScenarioRuleType } from '../../../../../../../common/types';
import { selectPlaylistDto, selectStepTwoActiveScenarioId } from '../../../selectors';

type TriggerContentSettingTab = 'new_scenario' | 'favourites';

type ReturnValue = {
    rulesList: ScenarioRuleType[];
    setRulesList: Dispatch<SetStateAction<ScenarioRuleType[]>>;
    triggerTab: TriggerContentSettingTab;
    chooseTriggerTab: (triggerTab: TriggerContentSettingTab) => () => void;
    t: TFunction;
};

export const useTriggerScenarioSettingModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    const activeScenarioId = useSelector(selectStepTwoActiveScenarioId);

    const { playlistDtoItems } = useSelector(selectPlaylistDto);

    const activeScenario = useMemo(
        () => playlistDtoItems.find((item) => item.scenario?.id === activeScenarioId)?.scenario,
        [activeScenarioId, playlistDtoItems],
    );

    const [rulesList, setRulesList] = useState<ScenarioRuleType[]>(activeScenario?.rules ?? []);
    const [triggerTab, setTriggerTab] = useState<TriggerContentSettingTab>('new_scenario');

    const chooseTriggerTab = useCallback(
        (triggerTab: TriggerContentSettingTab) => () => {
            setTriggerTab(triggerTab);
        },
        [],
    );

    return {
        rulesList,
        setRulesList,
        triggerTab,
        chooseTriggerTab,
        t,
    };
};
