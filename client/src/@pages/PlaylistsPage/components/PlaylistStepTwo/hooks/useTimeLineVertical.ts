import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectPlaylistDto, selectStepTwoShowedScenariosIds } from '../../../selectors';
import { getPlaylistDtoItemsWithScenarios } from '../../../../../helpers';
import { PlaylistItemWithScenario } from './useStepTwoPlaylistItems';
import { PlaylistItemType } from '../../../../../../../common/types';

type ReturnValue = {
    playlistDtoItems: PlaylistItemType[];
    playlistDtoItemsWithScenario: PlaylistItemWithScenario[];

    t: TFunction;
};

export const useTimeLineVertical = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const { playlistDtoItems } = useSelector(selectPlaylistDto);
    const showedScenariosIds = useSelector(selectStepTwoShowedScenariosIds);

    const showedPlaylistItems = useMemo(
        () => playlistDtoItems.filter((item) => (item.scenario ? showedScenariosIds.includes(item.scenario.id) : true)),
        [playlistDtoItems, showedScenariosIds],
    );

    const playlistDtoItemsWithScenario = useMemo(
        () => getPlaylistDtoItemsWithScenarios(showedPlaylistItems),
        [showedPlaylistItems],
    );

    return {
        playlistDtoItems,
        playlistDtoItemsWithScenario,
        t,
    };
};
