import { RefObject, useCallback, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { useElementDimensions } from '../../../../../hooks/useElementDimensions';
import {
    selectPlaylistDto,
    selectStepTwoCurrentTimelineSeconds,
    selectStepTwoShowedScenariosIds,
    selectStepTwoSliderMoving,
    selectTimeLineRef,
} from '../../../selectors';
import { getPlaylistDtoItemsWithScenarios } from '../../../../../helpers';
import { setStepTwoCurrentTimelineSeconds, setStepTwoSliderMoving } from '../../../actions';
import { PlaylistItemWithScenario } from './useStepTwoPlaylistItems';
import { PlaylistItemType, CONTENT_TYPE } from '../../../../../../../common/types';

type ReturnValue = {
    timeLineWrapperRef: RefObject<HTMLDivElement>;
    playlistDtoItems: PlaylistItemType[];
    playlistDtoItemsWithScenario: PlaylistItemWithScenario[];
    timeLineArray: any[];
    lastElementWidth: (index: number) => number | undefined;
    lastElementDisplay: (index: number) => 'none' | undefined;
    is20SecDividerRequired: (index: number) => boolean;
    is10SecDividerRequired: (index: number) => boolean;
    totalPlaylistDuration: number;
    onSliderChange: (_: Event, seconds: number | number[]) => void;
    currentTimelineSeconds: number;
    isTimelineSliderShown: boolean;
    onSliderTouchStart: () => void;
    onSliderTouchEnd: () => void;
    onSliderMove: () => void;
    t: TFunction;
};

export const useTimeLineHorizontal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const timeLineWrapperRef = useRef<HTMLDivElement>(null);
    const timeLineRef = useSelector(selectTimeLineRef);

    const { playlistDtoItems } = useSelector(selectPlaylistDto);
    const showedScenariosIds = useSelector(selectStepTwoShowedScenariosIds);
    const currentTimelineSeconds = useSelector(selectStepTwoCurrentTimelineSeconds);

    const showedPlaylistItems = useMemo(
        () => playlistDtoItems.filter((item) => (item.scenario ? showedScenariosIds.includes(item.scenario.id) : true)),
        [playlistDtoItems, showedScenariosIds],
    );

    const playlistDtoItemsWithScenario = useMemo(
        () => getPlaylistDtoItemsWithScenarios(showedPlaylistItems),
        [showedPlaylistItems],
    );

    const totalPlaylistDuration = useMemo(
        () =>
            playlistDtoItems
                .map(({ delay, content }) =>
                    content?.type === CONTENT_TYPE.IMAGE || content?.type === CONTENT_TYPE.WEB
                        ? delay
                        : content?.duration,
                )
                .reduce((acc, curr) => acc + curr, 0),
        [playlistDtoItems],
    );

    const { width: currentTimeLineWidth } = useElementDimensions(timeLineWrapperRef.current);

    const maxTimeLineWidth = useMemo(() => {
        const totalPlaylistWidth = totalPlaylistDuration * 10 + 200;
        return (totalPlaylistWidth < currentTimeLineWidth ? currentTimeLineWidth : totalPlaylistWidth) || 0;
    }, [currentTimeLineWidth, totalPlaylistDuration]);

    const timeLineArray = new Array(Math.ceil(maxTimeLineWidth / 200)).fill(undefined);

    const lastElementWidth = useCallback(
        (index: number) => {
            const isLastIndex = timeLineArray.length - 1 === index;
            return isLastIndex ? maxTimeLineWidth % 200 : undefined;
        },
        [maxTimeLineWidth, timeLineArray.length],
    );

    const lastElementDisplay = useCallback(
        (index: number) => {
            const isLastIndex = timeLineArray.length - 1 === index;
            const lastElementWidth = maxTimeLineWidth % 200;
            return isLastIndex && lastElementWidth > 0 && lastElementWidth < 26 ? 'none' : undefined;
        },
        [maxTimeLineWidth, timeLineArray.length],
    );

    const is20SecDividerRequired = useCallback(
        (index: number) => {
            const isFirstIndex = index === 0;
            const isLastIndex = timeLineArray.length - 1 === index;

            return !isFirstIndex && !isLastIndex;
        },
        [timeLineArray.length],
    );

    const is10SecDividerRequired = useCallback(
        (index: number) => {
            const isLastIndex = timeLineArray.length - 1 === index;

            return isLastIndex && (maxTimeLineWidth % 200 > 100 || maxTimeLineWidth % 200 === 0);
        },
        [maxTimeLineWidth, timeLineArray.length],
    );

    const { width: timelineWidth } = useElementDimensions(timeLineWrapperRef.current);

    const onSliderChange = useCallback(
        (_: Event, seconds: number | number[]) => {
            seconds = typeof seconds === 'number' ? seconds : 0;
            dispatch(setStepTwoCurrentTimelineSeconds(seconds));

            if (timeLineRef !== null) {
                if (seconds + 5 >= timelineWidth / 10) {
                    timeLineRef.scrollLeft += 20;
                }
                if (seconds - 5 + timelineWidth / 10 < totalPlaylistDuration) {
                    timeLineRef.scrollLeft -= 20;
                }
            }
        },
        [dispatch, timeLineRef, timelineWidth, totalPlaylistDuration],
    );

    const sliderMoving = useSelector(selectStepTwoSliderMoving);
    const [isSliderTouched, setIsSliderTouched] = useState(false);

    const onSliderTouchStart = useCallback(() => {
        setIsSliderTouched(true);
    }, []);

    const onSliderMove = useCallback(() => {
        if (sliderMoving !== isSliderTouched) {
            dispatch(setStepTwoSliderMoving(isSliderTouched));
        }
    }, [dispatch, isSliderTouched, sliderMoving]);

    const onSliderTouchEnd = useCallback(() => {
        setIsSliderTouched(false);
        dispatch(setStepTwoSliderMoving(false));
    }, [dispatch]);

    const isTimelineSliderShown = showedPlaylistItems.length > 0;

    return {
        timeLineWrapperRef,
        playlistDtoItems,
        playlistDtoItemsWithScenario,
        timeLineArray,
        lastElementWidth,
        lastElementDisplay,
        is20SecDividerRequired,
        is10SecDividerRequired,
        totalPlaylistDuration,
        onSliderChange,
        currentTimelineSeconds,
        isTimelineSliderShown,
        onSliderTouchStart,
        onSliderTouchEnd,
        onSliderMove,
        t,
    };
};
