import { useCallback, useState, ChangeEvent, useEffect, useRef, useMemo } from 'react';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import { useTranslation } from 'react-i18next';

import { ParameterValue, ParameterOperator } from '../components/TriggerScenarioSettingModal/types';
import { ScenarioRuleType, WeatherAndTrafficData } from '../../../../../../../common/types';
import { tSelectOperator, tOperator, tParameterValue } from '../components/TriggerScenarioSettingModal/helpers';

type Props = {
    data?: WeatherAndTrafficData;
    updateRulesList: (data: Omit<ScenarioRuleType, 'id'>) => void;
};

type ReturnValue = {
    parameterOperator: ParameterOperator | '';
    parameterOperators: ParameterOperator[];
    onParameterOperatorSelect: (e: SelectChangeEvent) => void;
    parameterValue: ParameterValue | '';
    onParameterValueChange: (e: ChangeEvent) => void;
};

export const useTrafficRow = ({ data, updateRulesList }: Props): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    const parameterOperators = useMemo(() => JSON.parse(t('parameterOperators')) as ParameterOperator[], [t]);

    const initialParameterOperator =
        data && 'operator' in data
            ? (tOperator(data.operator ?? 'less', t) as ParameterOperator)
            : (t('less') as ParameterOperator);

    const [parameterOperator, setParameterOperator] = useState<ParameterOperator>(initialParameterOperator);
    const [parameterValue, setParameterValue] = useState<ParameterValue | ''>('');

    useEffect(() => {
        if (data) {
            if ('value' in data) {
                const initialParameterValue = tParameterValue(data.value ?? '', t);
                setParameterValue((prevValue) => (!prevValue ? initialParameterValue : prevValue));
            }
        }
    }, [data, t]);

    const onParameterOperatorSelect = useCallback(({ target: { value } }: SelectChangeEvent<ParameterOperator>) => {
        setParameterOperator(value as ParameterOperator);
    }, []);

    const onParameterValueChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            const numValue = Number(value);
            if (numValue > 12 || numValue < 0) return;

            setParameterValue(value);
            updateRulesList({ data: { value } });
        },
        [updateRulesList],
    );

    const prevParameterOperator = useRef<ParameterOperator>();
    useEffect(() => {
        if (prevParameterOperator.current !== parameterOperator) {
            prevParameterOperator.current = parameterOperator;
            updateRulesList({
                data: { operator: tSelectOperator(parameterOperator as ParameterOperator, t) },
            });
        }
    }, [parameterOperator, t, updateRulesList]);

    return {
        parameterOperator,
        parameterOperators,
        onParameterOperatorSelect,
        parameterValue,
        onParameterValueChange,
    };
};
