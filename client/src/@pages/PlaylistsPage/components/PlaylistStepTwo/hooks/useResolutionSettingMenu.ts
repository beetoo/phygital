import { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectPlaylistDto, selectPlaylistDtoScreens } from '../../../selectors';
import { setPlaylistDtoInfo } from '../../../actions';
import { OrientationEnum, VirtualScreenType } from '../../../../../../../common/types';
import { TriggerOrientationSelectType, TriggerResolutionSelectType } from '../../../types';
import {
    orientationValueToTriggerOrientationType,
    resolutionValueToTriggerResolutionType,
    triggerOrientationTypeToOrientationValue,
    triggerResolutionTypeToResolutionValue,
} from '../helpers';
import { TRIGGER_SELECT_RESOLUTIONS } from '../constants';
import { selectScreens } from '../../../../ScreensPage/selectors';
import { getAspectRatioString } from '../../../../../helpers';

const resolutionsList = (isHorizontal = true) =>
    [
        `Full HD (${isHorizontal ? '1920 × 1080' : '1080 × 1920'} / 16:9)`,
        `HD (${isHorizontal ? '1280 × 720' : '720 × 1280'} / 16:9)`,
        `UHD (${isHorizontal ? '3840 × 2160' : '2160 × 3840'} / 16:9)`,
    ] as TriggerResolutionSelectType[];

type ReturnValue = {
    resolutions: (TriggerResolutionSelectType | string)[];
    screenResolutionValue: TriggerResolutionSelectType | string;
    onScreenResolutionSelect: (e: SelectChangeEvent) => void;
    hasCustomResolutionInput: boolean;
    customResolutionWidth: string;
    customResolutionHeight: string;
    onCustomResolutionWidthChange: (e: SelectChangeEvent) => void;
    onCustomResolutionHeightChange: (e: SelectChangeEvent) => void;

    orientations: TriggerOrientationSelectType[];
    screenOrientationValue: TriggerOrientationSelectType;
    onScreenOrientationSelect: (e: SelectChangeEvent) => void;
    isSelectDisabled: boolean;
    isOrientationSelectDisabled: boolean;
    t: TFunction;
};

export const useResolutionSettingMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const screens = useSelector(selectScreens);
    const { playlistDtoInfo } = useSelector(selectPlaylistDto);
    const playlistDtoScreens = useSelector(selectPlaylistDtoScreens);

    const orientationsList = useMemo(
        () => JSON.parse(t('orientationsListShort')) as TriggerOrientationSelectType[],
        [t],
    );

    const [screenResolutionValue, setScreenResolutionValue] = useState<TriggerResolutionSelectType | string>(
        TRIGGER_SELECT_RESOLUTIONS.FULL_HD,
    );
    const [resolutions, setResolutions] = useState<(TriggerResolutionSelectType | string)[]>(resolutionsList);

    const [screenOrientationValue, setScreenOrientationValue] = useState<TriggerOrientationSelectType>(
        t('horizontal') as TriggerOrientationSelectType,
    );
    const [orientations, setOrientations] = useState<TriggerOrientationSelectType[]>(orientationsList);

    const [hasCustomResolutionInput, setHasCustomResolutionInput] = useState(false);
    const [customResolutionWidth, setCustomResolutionWidth] = useState('');
    const [customResolutionHeight, setCustomResolutionHeight] = useState('');

    const onScreenResolutionSelect = useCallback(
        ({ target: { value } }: SelectChangeEvent<TriggerResolutionSelectType>) => {
            setHasCustomResolutionInput(value === t('resolutionCustom'));
            setScreenResolutionValue(value as TriggerResolutionSelectType);

            if ((value as TriggerResolutionSelectType) !== t('resolutionCustom')) {
                setCustomResolutionWidth('');
                setCustomResolutionHeight('');

                dispatch(
                    setPlaylistDtoInfo({
                        ...playlistDtoInfo,
                        virtualScreen: {
                            ...(playlistDtoInfo.virtualScreen as VirtualScreenType),
                            resolution: triggerResolutionTypeToResolutionValue(value as TriggerResolutionSelectType),
                        },
                    }),
                );
            }
        },
        [dispatch, playlistDtoInfo, t],
    );

    const onCustomResolutionWidthChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        if (value === '' || value.match(/^\d{1,5}$/)) {
            setCustomResolutionWidth(value);
        }
    }, []);

    const onCustomResolutionHeightChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        if (value === '' || value.match(/^\d{1,5}$/)) {
            setCustomResolutionHeight(value);
        }
    }, []);

    const onScreenOrientationSelect = useCallback(
        ({ target: { value } }: SelectChangeEvent<TriggerOrientationSelectType>) => {
            setScreenOrientationValue(value as TriggerOrientationSelectType);

            dispatch(
                setPlaylistDtoInfo({
                    ...playlistDtoInfo,
                    virtualScreen: {
                        ...(playlistDtoInfo.virtualScreen as VirtualScreenType),
                        orientation: triggerOrientationTypeToOrientationValue(value as TriggerOrientationSelectType),
                    },
                }),
            );
        },
        [dispatch, playlistDtoInfo],
    );

    const isDifferentScreensResolution = useMemo(
        () =>
            screens
                .filter(({ id }) => playlistDtoScreens.includes(id))
                .some(
                    ({ resolution }, _, array) =>
                        resolution.width !== array[0].resolution.width &&
                        resolution.height !== array[0].resolution.height,
                ),
        [playlistDtoScreens, screens],
    );

    const { dtoResolution, dtoResolutionWidth, dtoResolutionHeight, dtoOrientation } = useMemo(() => {
        const { virtualScreen } = playlistDtoInfo;
        const dtoResolution = virtualScreen?.resolution;
        const dtoResolutionWidth = dtoResolution?.width;
        const dtoResolutionHeight = dtoResolution?.height;
        const dtoOrientation = virtualScreen?.orientation;

        return { dtoResolution, dtoResolutionWidth, dtoResolutionHeight, dtoOrientation };
    }, [playlistDtoInfo]);

    useEffect(() => {
        if (!playlistDtoInfo.virtualScreen) {
            dispatch(
                setPlaylistDtoInfo({
                    ...playlistDtoInfo,
                    virtualScreen: {
                        resolution: { width: 1920, height: 1080 },
                        orientation: OrientationEnum.LANDSCAPE,
                    },
                }),
            );
        }
    }, [dispatch, playlistDtoInfo]);

    // эффект на выбор разрешения через селект
    useEffect(() => {
        if (hasCustomResolutionInput) return;

        const isDtoExist = dtoResolution && dtoResolutionWidth && dtoResolutionHeight && dtoOrientation;

        if (isDtoExist) {
            const resolutionValue = isDifferentScreensResolution
                ? getAspectRatioString(dtoResolutionWidth, dtoResolutionHeight)
                : resolutionValueToTriggerResolutionType(dtoResolution, dtoOrientation === OrientationEnum.LANDSCAPE);

            const orientationValue = orientationValueToTriggerOrientationType(dtoOrientation, t);

            const isResolutionChangeRequired = screenResolutionValue !== resolutionValue;

            if (isResolutionChangeRequired) {
                setScreenResolutionValue(resolutionValue);
            }

            const isOrientationChangeRequired = screenOrientationValue !== orientationValue;

            if (isOrientationChangeRequired) {
                setScreenOrientationValue(orientationValue);
            }

            const resolutions = resolutionsList(dtoOrientation === OrientationEnum.LANDSCAPE);

            if (resolutionValue.includes('Custom')) {
                setResolutions([...resolutions, resolutionValue]);
            } else if (isDifferentScreensResolution) {
                setResolutions([resolutionValue]);
            } else {
                setResolutions(resolutions);
            }
        }
    }, [
        dtoOrientation,
        dtoResolution,
        dtoResolutionHeight,
        dtoResolutionWidth,
        hasCustomResolutionInput,
        isDifferentScreensResolution,
        screenOrientationValue,
        screenResolutionValue,
        t,
    ]);

    // эффект на ввод custom разрешения
    useEffect(() => {
        if (hasCustomResolutionInput) {
            if (customResolutionWidth && customResolutionHeight) {
                const resolutionWidth = Number(customResolutionWidth);
                const resolutionHeight = Number(customResolutionHeight);

                const isResolutionChanged =
                    resolutionWidth !== dtoResolutionWidth || resolutionHeight !== dtoResolutionHeight;

                if (isResolutionChanged) {
                    const isHorizontal = resolutionWidth >= resolutionHeight;

                    setOrientations(orientationsList);
                    setScreenOrientationValue(
                        isHorizontal
                            ? (t('horizontal') as TriggerOrientationSelectType)
                            : (t('portrait') as TriggerOrientationSelectType),
                    );

                    dispatch(
                        setPlaylistDtoInfo({
                            ...playlistDtoInfo,
                            virtualScreen: {
                                resolution: {
                                    width: resolutionWidth,
                                    height: resolutionHeight,
                                },
                                orientation: isHorizontal ? OrientationEnum.LANDSCAPE : OrientationEnum.PORTRAIT,
                            },
                        }),
                    );
                }
            } else {
                setOrientations(['-']);
                setScreenOrientationValue('-');
            }
        } else {
            setOrientations(orientationsList);
        }
    }, [
        customResolutionHeight,
        customResolutionWidth,
        dispatch,
        dtoResolutionHeight,
        dtoResolutionWidth,
        hasCustomResolutionInput,
        orientationsList,
        playlistDtoInfo,
        t,
    ]);

    const isSelectDisabled = playlistDtoScreens.length > 0;
    const isOrientationSelectDisabled =
        isSelectDisabled || (hasCustomResolutionInput && (!customResolutionWidth || !customResolutionHeight));

    return {
        resolutions,
        screenResolutionValue,
        onScreenResolutionSelect,
        hasCustomResolutionInput,
        customResolutionWidth,
        customResolutionHeight,
        onCustomResolutionHeightChange,
        onCustomResolutionWidthChange,

        orientations,
        screenOrientationValue,
        onScreenOrientationSelect,
        isSelectDisabled,
        isOrientationSelectDisabled,
        t,
    };
};
