import { useCallback, Dispatch, SetStateAction, useMemo, useState, ChangeEvent, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import { v4 as uuidv4 } from 'uuid';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import { isEqual } from 'lodash';

import { RuleCondition } from '../components/TriggerScenarioSettingModal/types';
import { ScenarioRuleType } from '../../../../../../../common/types';
import {
    getEnabledRules,
    getRuleListFilterByParameter,
    tSelectCondition,
} from '../components/TriggerScenarioSettingModal/helpers';
import { setHideModal } from '../../../../../components/ModalsContainer/actions';

import {
    setPlaylistDtoPlaylistItems,
    setStepTwoActiveScenarioId,
    setStepTwoSelectedPlaylistItemIds,
    setStepTwoShowedScenariosIds,
} from '../../../actions';
import {
    selectPlaylistDto,
    selectStepTwoActivePlaylistItemId,
    selectStepTwoActiveScenarioId,
    selectStepTwoSelectedPlaylistItemIds,
    selectStepTwoShowedScenariosIds,
} from '../../../selectors';
import { selectCurrentOrganization } from '../../../../ProfilePage/selectors';

type Props = {
    rulesList: ScenarioRuleType[];
    setRulesList: Dispatch<SetStateAction<ScenarioRuleType[]>>;
};

type ReturnValue = {
    scenarioName: string;
    scenarioDescription: string;
    onScenarioNameChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onScenarioDescriptionChange: (e: ChangeEvent<HTMLInputElement>) => void;

    updateRulesList: (ruleId: string) => (data: Omit<ScenarioRuleType, 'id'>) => void;
    addNewRule: () => void;
    isAddNewRuleDisabled: boolean;
    deleteRule: (id: string) => () => void;

    ruleConditions: RuleCondition[];

    onRuleConditionSelect: (id: string) => (e: SelectChangeEvent) => void;
    isRuleConditionDisabled: (condition: RuleCondition) => boolean;
    rowIdsWithError: string[];
    setRowIdsWithError: Dispatch<SetStateAction<string[]>>;

    saveScenario: () => void;
    isSaveButtonDisabled: boolean;

    isDeleteRulesAlertOpen: boolean;
    openDeleteRulesAlert: () => void;
    onDeleteRulesSubmit: () => void;
    onDeleteRulesCancel: () => void;

    isExitScenarioAlertOpen: boolean;
    openExitScenarioSettingAlert: () => void;
    onExitScenarioSettingSubmit: () => void;
    onExitScenarioSettingCancel: () => void;

    t: TFunction;
};

export const useNewScenarioTab = ({ rulesList, setRulesList }: Props): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    const dispatch = useDispatch();

    const ruleConditions = useMemo(() => JSON.parse(t('ruleConditions')) as RuleCondition[], [t]);

    const { id: organizationId } = useSelector(selectCurrentOrganization);
    const { playlistDtoItems } = useSelector(selectPlaylistDto);
    const activePlaylistItemId = useSelector(selectStepTwoActivePlaylistItemId);
    const activeScenarioId = useSelector(selectStepTwoActiveScenarioId);
    const selectedPlaylistItemIds = useSelector(selectStepTwoSelectedPlaylistItemIds);
    const showedScenariosIds = useSelector(selectStepTwoShowedScenariosIds);

    const activeScenario = useMemo(
        () => playlistDtoItems.find((item) => item.scenario?.id === activeScenarioId)?.scenario,
        [activeScenarioId, playlistDtoItems],
    );

    const [scenarioName, setScenarioName] = useState<string>(activeScenario?.name ?? (t('newScript') as string));
    const [scenarioDescription, setScenarioDescription] = useState<string>(activeScenario?.description ?? '');
    const [rowIdsWithError, setRowIdsWithError] = useState<string[]>([]);

    const onScenarioNameChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setScenarioName(value);
    }, []);

    const onScenarioDescriptionChange = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setScenarioDescription(value);
    }, []);

    const isRuleConditionDisabled = useCallback(
        (condition: RuleCondition) => {
            if (condition === t('time')) {
                return rulesList.some(({ condition }) => condition === 'time');
            } else if (condition === t('traffic')) {
                return rulesList.filter(({ condition }) => condition === 'traffic').length === 2;
            } else {
                const filteredRuleListByParameter = getRuleListFilterByParameter(rulesList);

                const isWindOrHumidityDisabled = (rules: ScenarioRuleType[]) => {
                    const withOperator = rules.some(({ data }) => data && 'operator' in data && data?.operator);

                    return rules.length === (withOperator ? 2 : 1);
                };

                return (
                    filteredRuleListByParameter('temperature').length === 2 &&
                    filteredRuleListByParameter('water_temperature').length === 2 &&
                    filteredRuleListByParameter('cloud_cover').length === 1 &&
                    filteredRuleListByParameter('rain_fall').length === 1 &&
                    isWindOrHumidityDisabled(filteredRuleListByParameter('wind')) &&
                    filteredRuleListByParameter('atmospheric_pressure').length === 2 &&
                    isWindOrHumidityDisabled(filteredRuleListByParameter('humidity'))
                );
            }
        },
        [rulesList, t],
    );

    const isAddNewRuleDisabled = useMemo(
        () => rulesList.length === 5 || (rulesList.length > 0 && getEnabledRules(rulesList).length < rulesList.length),
        [rulesList],
    );

    const addNewRule = useCallback(() => {
        setRulesList((prevList) => [...prevList, { id: uuidv4() }]);
    }, [setRulesList]);

    const deleteRule = useCallback(
        (id: string) => () => {
            setRulesList((prevList) => prevList.filter((rule) => rule.id !== id));
            setRowIdsWithError((prevList) => prevList.filter((ruleId) => ruleId !== id));
        },
        [setRulesList],
    );

    const updateRulesList = useCallback(
        (ruleId: string) => (data: Omit<ScenarioRuleType, 'id'>) => {
            setRulesList((prevList) =>
                prevList.map((rule) =>
                    ruleId === rule.id
                        ? {
                              ...rule,
                              condition: data.condition ? data.condition : rule.condition,
                              data: data.data ? { ...rule.data, ...data.data } : data.data,
                          }
                        : rule,
                ),
            );
        },
        [setRulesList],
    );

    const onRuleConditionSelect = useCallback(
        (id: string) =>
            ({ target: { value } }: SelectChangeEvent<RuleCondition>) => {
                updateRulesList(id)({ condition: tSelectCondition(value as RuleCondition, t), data: undefined });
            },
        [t, updateRulesList],
    );

    const withPostfix = useCallback(
        (scenarioName: string) => {
            const repeatedNamesNumber = playlistDtoItems
                .filter((item) => item.scenario?.id !== activeScenarioId)
                .filter(
                    (item) =>
                        item.scenario?.name.startsWith(scenarioName) && !!item.scenario?.name.match(/.+(\W\(\d+\))?$/),
                )
                .map((item) => item.scenario?.name ?? '')
                .reduce((acc, curr) => (acc.includes(curr) ? acc : [...acc, curr]), [] as string[])?.length;
            return repeatedNamesNumber > 0 ? `${scenarioName} (${repeatedNamesNumber + 1})` : scenarioName;
        },
        [activeScenarioId, playlistDtoItems],
    );

    const activePlaylistItemIds = useMemo(
        () => (selectedPlaylistItemIds.length > 0 ? selectedPlaylistItemIds : [activePlaylistItemId]),
        [activePlaylistItemId, selectedPlaylistItemIds],
    );

    const isScenarioNameOrDescriptionChanged = useMemo(
        () =>
            activeScenario
                ? scenarioName.trim() !== activeScenario?.name.trim() ||
                  scenarioDescription.trim() !== activeScenario?.description.trim()
                : false,
        [activeScenario, scenarioDescription, scenarioName],
    );

    const saveScenario = useCallback(() => {
        let playlistDtoItemsWithScenario;

        if (selectedPlaylistItemIds.length > 0) {
            dispatch(setStepTwoSelectedPlaylistItemIds());
        }

        if (activeScenarioId === '') {
            const scenarioId = uuidv4();

            playlistDtoItemsWithScenario = playlistDtoItems.map(({ id, ...rest }) =>
                activePlaylistItemIds.includes(id)
                    ? {
                          id,
                          ...rest,
                          scenarioId,
                          scenario: {
                              id: scenarioId,
                              organizationId,
                              name: withPostfix(scenarioName),
                              description: scenarioDescription,
                              rules: rulesList,
                          },
                      }
                    : { id, ...rest },
            );

            dispatch(setStepTwoShowedScenariosIds([...showedScenariosIds, scenarioId]));
            dispatch(setStepTwoActiveScenarioId(scenarioId));
        } else {
            playlistDtoItemsWithScenario = playlistDtoItems.map(({ scenario, ...rest }) =>
                scenario && scenario?.id === activeScenarioId
                    ? {
                          ...rest,
                          scenarioId: activeScenarioId,
                          scenario: {
                              ...scenario,
                              name: isScenarioNameOrDescriptionChanged ? withPostfix(scenarioName) : scenarioName,
                              description: scenarioDescription,
                              rules: rulesList,
                          },
                      }
                    : { scenario, ...rest },
            );
        }

        dispatch(setPlaylistDtoPlaylistItems(playlistDtoItemsWithScenario));

        dispatch(setHideModal());
    }, [
        selectedPlaylistItemIds.length,
        activeScenarioId,
        dispatch,
        playlistDtoItems,
        showedScenariosIds,
        activePlaylistItemIds,
        organizationId,
        withPostfix,
        scenarioName,
        scenarioDescription,
        rulesList,
        isScenarioNameOrDescriptionChanged,
    ]);

    const prevRulesList = useRef<ScenarioRuleType[]>(rulesList);

    const isRulesEqual = useMemo(() => isEqual(rulesList, prevRulesList.current), [rulesList]);

    const isSaveButtonDisabled = useMemo(() => {
        if (rowIdsWithError.length > 0) return true;
        if (rulesList.length === 0 || (!isScenarioNameOrDescriptionChanged && isRulesEqual)) return true;
        return rulesList.length > 0 && getEnabledRules(rulesList).length < rulesList.length;
    }, [isRulesEqual, isScenarioNameOrDescriptionChanged, rowIdsWithError.length, rulesList]);

    // delete rules alert

    const [isDeleteRulesAlertOpen, setIsDeleteRulesAlertOpen] = useState(false);

    const openDeleteRulesAlert = useCallback(() => {
        setIsDeleteRulesAlertOpen(true);
    }, []);

    const onDeleteRulesSubmit = useCallback(() => {
        setRulesList([]);
        setIsDeleteRulesAlertOpen(false);
    }, [setRulesList]);

    const onDeleteRulesCancel = useCallback(() => {
        setIsDeleteRulesAlertOpen(false);
    }, []);

    // exit scenario alert

    const [isExitScenarioAlertOpen, setIsExitScenarioAlertOpen] = useState(false);

    const openExitScenarioSettingAlert = useCallback(() => {
        if (rulesList.length === 0 || isRulesEqual) {
            setIsExitScenarioAlertOpen(false);
            dispatch(setHideModal());
        } else {
            setIsExitScenarioAlertOpen(true);
        }
    }, [dispatch, isRulesEqual, rulesList.length]);

    const onExitScenarioSettingSubmit = useCallback(() => {
        setIsExitScenarioAlertOpen(false);
        dispatch(setHideModal());
    }, [dispatch]);

    const onExitScenarioSettingCancel = useCallback(() => {
        setIsExitScenarioAlertOpen(false);
    }, []);

    return {
        scenarioName,
        scenarioDescription,
        onScenarioNameChange,
        onScenarioDescriptionChange,

        updateRulesList,
        addNewRule,
        isAddNewRuleDisabled,
        deleteRule,

        ruleConditions,

        onRuleConditionSelect,
        isRuleConditionDisabled,
        rowIdsWithError,
        setRowIdsWithError,

        isSaveButtonDisabled,
        saveScenario,

        isDeleteRulesAlertOpen,
        openDeleteRulesAlert,
        onDeleteRulesSubmit,
        onDeleteRulesCancel,

        isExitScenarioAlertOpen,
        onExitScenarioSettingSubmit,
        onExitScenarioSettingCancel,
        openExitScenarioSettingAlert,

        t,
    };
};
