import { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setOpenContentViewModal, setShowModal } from '../../../../../components/ModalsContainer/actions';
import { MODALS } from '../../../../../components/ModalsContainer/types';
import { selectPlaylistDto, selectStepTwoActivePlaylistItemId } from '../../../selectors';
import { usePlaylistItemInfo } from '../../../../../hooks/usePlaylistItemInfo';
import { setPlaylistDtoPlaylistItems, setStepTwoMenuMode } from '../../../actions';
import { getReorderedItems } from '../../../../../helpers';
import { timeStringToSeconds, TWENTY_FOUR_HOURS_WITH_SECONDS_TIME_REGEX } from '../../../helpers';

type ReturnValue = {
    playlistItemSrc: string;
    playlistItemName: string;
    playlistItemFileSize: string;
    playlistItemFileTypeAndExtension: string;
    playlistItemDimensions: string;
    playlistItemOrientation: string;
    isVideoType: boolean;
    isImageType: boolean;
    isWebType: boolean;

    playlistItemDuration: string;
    onPlaylistItemDurationChange: (e: ChangeEvent<HTMLInputElement>) => void;
    placeIndex: string;
    onPlaylistItemPlaceIndexChange: (e: ChangeEvent<HTMLInputElement>) => void;
    showTriggerContentSettingModal: () => void;
    deletePlaylistItem: () => void;

    isContentInfoShown: boolean;
    showContentInfo: () => void;
    showContentViewModal: () => void;
    t: TFunction;
};

export const useContentSettingMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const { playlistDtoItems } = useSelector(selectPlaylistDto);
    const activePlaylistItemId = useSelector(selectStepTwoActivePlaylistItemId);

    const [isContentInfoShown, setIsContentInfoShown] = useState(false);

    const showContentInfo = useCallback(() => {
        setIsContentInfoShown((prev) => !prev);
    }, []);

    const activePlaylistItem = useMemo(
        () => playlistDtoItems.find(({ id }) => id === activePlaylistItemId),
        [activePlaylistItemId, playlistDtoItems],
    );

    const {
        playlistItemName,
        playlistItemFileTypeAndExtension,
        playlistItemFileSize,
        playlistItemSrc,
        playlistItemDimensions,
        playlistItemOrientation,
        playlistItemDuration: defaultPlaylistItemDuration,
        playlistItemPlaceIndex,
        isVideoType,
        isImageType,
        isWebType,
    } = usePlaylistItemInfo(activePlaylistItem);

    const [playlistItemDuration, setPlaylistItemDuration] = useState(defaultPlaylistItemDuration);

    const onPlaylistItemDurationChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            if (value.match(TWENTY_FOUR_HOURS_WITH_SECONDS_TIME_REGEX)) {
                const newSecondsValue = timeStringToSeconds(value);
                const currentSecondsValue = timeStringToSeconds(defaultPlaylistItemDuration);

                if (currentSecondsValue !== newSecondsValue) {
                    const updatedPlaylistDtoItems = playlistDtoItems.map(({ id, delay, ...rest }) =>
                        id === activePlaylistItemId ? { id, delay: newSecondsValue, ...rest } : { id, delay, ...rest },
                    );
                    dispatch(setPlaylistDtoPlaylistItems(updatedPlaylistDtoItems));
                }
            }
            setPlaylistItemDuration(value);
        },
        [activePlaylistItemId, defaultPlaylistItemDuration, dispatch, playlistDtoItems],
    );

    // обновляем значение в инпуте при каждом изменении активного контента
    useEffect(() => {
        if (defaultPlaylistItemDuration) {
            setPlaylistItemDuration(defaultPlaylistItemDuration);
        }
    }, [defaultPlaylistItemDuration]);

    const [placeIndex, setPlaceIndex] = useState<string>(playlistItemPlaceIndex.toString());

    const onPlaylistItemPlaceIndexChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            if (value) {
                if (!value.match(/\d{1,4}/)) return;
                if (Number(value) === 0 || Number(value) > playlistDtoItems.length) return;

                if (playlistItemPlaceIndex !== Number(value)) {
                    const reorderedItems = getReorderedItems({
                        playlistDtoItems,
                        from: playlistItemPlaceIndex - 1,
                        to: Number(value) - 1,
                    });

                    dispatch(setPlaylistDtoPlaylistItems(reorderedItems));
                }
            }
            setPlaceIndex(value);
        },
        [dispatch, playlistDtoItems, playlistItemPlaceIndex],
    );

    // обновляем значение в инпуте для индекса при каждом изменении активного контента
    useEffect(() => {
        if (playlistItemPlaceIndex.toString()) {
            setPlaceIndex(playlistItemPlaceIndex.toString());
        }
    }, [playlistItemPlaceIndex]);

    const deletePlaylistItem = useCallback(() => {
        dispatch(setPlaylistDtoPlaylistItems(playlistDtoItems.filter(({ id }) => id !== activePlaylistItemId)));
        dispatch(setStepTwoMenuMode('content_menu'));
    }, [activePlaylistItemId, dispatch, playlistDtoItems]);

    const showTriggerContentSettingModal = useCallback(() => {
        dispatch(setShowModal(MODALS.TRIGGER_SCENARIO_SETTING));
    }, [dispatch]);

    const showContentViewModal = useCallback(() => {
        if (activePlaylistItem?.content) {
            const { originFilename, type, src, dimensions } = activePlaylistItem.content;
            const viewItem = { name: originFilename, type, src, dimensions };
            dispatch(setOpenContentViewModal([viewItem], 0));
        }
    }, [activePlaylistItem, dispatch]);

    return {
        playlistItemSrc,
        playlistItemName,
        playlistItemFileTypeAndExtension,
        playlistItemFileSize,
        playlistItemDimensions,
        playlistItemOrientation,
        isVideoType,
        isImageType,
        isWebType,

        playlistItemDuration,
        onPlaylistItemDurationChange,
        placeIndex,
        onPlaylistItemPlaceIndexChange,
        deletePlaylistItem,
        showTriggerContentSettingModal,

        isContentInfoShown,
        showContentInfo,
        showContentViewModal,
        t,
    };
};
