import { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { selectContentDisplayType, selectTimeLineRef } from '../../../selectors';
import { setTimeLineRef } from '../../../actions';

type ReturnValue = {
    addTimeLineRef: (ref: HTMLDivElement | null) => void;
    scrollToTop: () => void;
    scrollToBottom: () => void;
    scrollToLeft: () => void;
    scrollToRight: () => void;
    scrollToRightOrBottom: () => void;
};

export const useTimeLineRef = (): ReturnValue => {
    const dispatch = useDispatch();

    const timeLineRef = useSelector(selectTimeLineRef);
    const contentDisplayType = useSelector(selectContentDisplayType);

    const addTimeLineRef = useCallback(
        (ref: HTMLDivElement | null) => {
            dispatch(setTimeLineRef(ref));
        },
        [dispatch],
    );

    const scrollToTop = useCallback(() => {
        timeLineRef?.scrollTo({ top: 0, behavior: 'smooth' });
    }, [timeLineRef]);

    const scrollToBottom = useCallback(() => {
        timeLineRef?.scrollTo({ top: timeLineRef?.scrollHeight, behavior: 'smooth' });
    }, [timeLineRef]);

    const scrollToLeft = useCallback(() => {
        timeLineRef?.scrollTo({ left: 0, behavior: 'smooth' });
    }, [timeLineRef]);

    const scrollToRight = useCallback(() => {
        timeLineRef?.scrollTo({ left: timeLineRef?.scrollWidth, behavior: 'smooth' });
    }, [timeLineRef]);

    const scrollToRightOrBottom = useCallback(() => {
        window.setTimeout(
            () =>
                timeLineRef?.scrollTo({
                    left: contentDisplayType === 'horizontal' ? timeLineRef?.scrollWidth : undefined,
                    top: contentDisplayType === 'vertical' ? timeLineRef?.scrollHeight : undefined,
                    behavior: 'smooth',
                }),
            0,
        );
    }, [contentDisplayType, timeLineRef]);

    return {
        addTimeLineRef,
        scrollToTop,
        scrollToBottom,
        scrollToLeft,
        scrollToRight,
        scrollToRightOrBottom,
    };
};
