import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import sortBy from 'lodash/sortBy';
import { v4 as uuidv4 } from 'uuid';

import { selectContentItems, selectWebContentItems } from '../../../../ContentPage/selectors';
import { ContentType, PlaylistItemType, CONTENT_TYPE } from '../../../../../../../common/types';
import {
    selectActivePlaylistId,
    selectPlaylistDto,
    selectStepTwoActiveScenarioId,
    selectStepTwoMenuMode,
    selectStepTwoSelectedContentItemIds,
} from '../../../selectors';
import {
    flatPlaylistItemsWithScenarios,
    getPlaylistDtoItemsWithScenarios,
    sortAndIndexItems,
} from '../../../../../helpers';
import { setPlaylistDtoPlaylistItems, setStepTwoSelectedContentItemIds } from '../../../actions';
import { PlaylistItemWithScenario } from './useStepTwoPlaylistItems';

type ReturnValue = {
    addContentToPlaylist: (data?: { sourceIndex?: number; destinationIndex?: number }) => void;
    addWebContentToPlaylist: (contentItemId: string) => void;
    getUpdatedPlaylistDtoItems: (contentItem: ContentType, destinationIndex: number) => PlaylistItemType[];
};

export const useAddContentToPlaylist = (activeFolderId = ''): ReturnValue => {
    const dispatch = useDispatch();

    const activePlaylistId = useSelector(selectActivePlaylistId);
    const activeScenarioId = useSelector(selectStepTwoActiveScenarioId);
    const contentItems = useSelector(selectContentItems);
    const webContentItems = useSelector(selectWebContentItems);
    const menuMode = useSelector(selectStepTwoMenuMode);
    const selectedContentItemIds = useSelector(selectStepTwoSelectedContentItemIds);
    const { playlistDtoItems } = useSelector(selectPlaylistDto);

    const activeContentItems = useMemo(
        () => contentItems.filter(({ folder }) => (activeFolderId === '' ? !folder : folder?.id === activeFolderId)),
        [activeFolderId, contentItems],
    );

    const activeScenario = useMemo(
        () => playlistDtoItems.find((item) => item.scenario?.id === activeScenarioId)?.scenario,
        [activeScenarioId, playlistDtoItems],
    );

    // обновляем все orderIndex от 0 до последнего индекса
    const formattedPlaylistDtoItems = useMemo(
        () =>
            sortBy(playlistDtoItems, (o) => o.orderIndex).map(({ ...rest }, index) => ({
                ...rest,
                orderIndex: index,
            })),
        [playlistDtoItems],
    );

    const formattedPlaylistDtoItemsWithScenarios = useMemo(
        () => sortAndIndexItems(getPlaylistDtoItemsWithScenarios(playlistDtoItems)) as PlaylistItemWithScenario[],
        [playlistDtoItems],
    );

    const orderIndexOffset = useMemo(() => {
        const numbers = playlistDtoItems
            .filter((item) => item.scenario?.id === activeScenarioId)
            .map(({ orderIndex }) => orderIndex);
        return numbers.length > 0 ? numbers[numbers.length - 1] : playlistDtoItems.length - 1;
    }, [activeScenarioId, playlistDtoItems]);

    const getUpdatedPlaylistDtoItems = useCallback(
        (
            contentItem: ContentType,
            destinationIndex: number,
            initialPlaylistDtoItemsWithScenarios = formattedPlaylistDtoItemsWithScenarios,
        ) => {
            const item = {
                id: uuidv4(),
                playlistId: activePlaylistId,
                name: contentItem.originFilename,
                content: contentItem,
                delay: contentItem?.type === CONTENT_TYPE.IMAGE || contentItem?.type === CONTENT_TYPE.WEB ? 20 : 0,
                orderIndex: destinationIndex,
                scenarioId: menuMode === 'content_menu_to_scenario' ? (activeScenario?.id ?? null) : null,
                scenario: menuMode === 'content_menu_to_scenario' ? activeScenario : undefined,
            };

            const updatedPlaylistItems = [...initialPlaylistDtoItemsWithScenarios, item];

            const isNeedReorder = formattedPlaylistDtoItems.some(({ orderIndex }) => orderIndex === destinationIndex);

            if (isNeedReorder) {
                const reorderablePart = formattedPlaylistDtoItemsWithScenarios
                    .filter(({ orderIndex }) => orderIndex >= destinationIndex)
                    .map(({ orderIndex, ...rest }) => ({ ...rest, orderIndex: orderIndex + 1 }));

                const reorderedPlaylistItems = sortAndIndexItems(
                    updatedPlaylistItems.map((item) => {
                        const id = 'playlistItems' in item ? item.scenario.id : item.id;
                        const itemInPart = reorderablePart.find((reorderableItem) =>
                            'playlistItems' in reorderableItem
                                ? id === reorderableItem.scenario.id
                                : id === reorderableItem.id,
                        );
                        if (itemInPart) {
                            return {
                                ...item,
                                orderIndex: itemInPart.orderIndex,
                            };
                        } else return item;
                    }),
                );

                return sortBy(flatPlaylistItemsWithScenarios(reorderedPlaylistItems), (o) => o.orderIndex);
            } else {
                return flatPlaylistItemsWithScenarios(updatedPlaylistItems);
            }
        },
        [activePlaylistId, activeScenario, formattedPlaylistDtoItems, formattedPlaylistDtoItemsWithScenarios, menuMode],
    );

    const addContentToPlaylist = useCallback(
        (data: { sourceIndex?: number; destinationIndex?: number }) => {
            let updatedPlaylistItems = [] as PlaylistItemType[];

            if (selectedContentItemIds.length > 0) {
                const selectedContentItems = selectedContentItemIds.map((contentItemId) =>
                    contentItems.find(({ id }) => id === contentItemId),
                );

                let updatedPlaylistDtoItemsWithScenario: PlaylistItemWithScenario[];

                selectedContentItems.forEach((contentItem, index) => {
                    if (contentItem) {
                        const destinationIndex = orderIndexOffset + 1 + index;
                        updatedPlaylistItems = getUpdatedPlaylistDtoItems(
                            contentItem,
                            destinationIndex,
                            updatedPlaylistDtoItemsWithScenario,
                        );
                        updatedPlaylistDtoItemsWithScenario = sortAndIndexItems(
                            getPlaylistDtoItemsWithScenarios(updatedPlaylistItems),
                        );
                    }
                });

                dispatch(setPlaylistDtoPlaylistItems(updatedPlaylistItems));

                // снимаем галочки с чекбоксов после добавления
                dispatch(setStepTwoSelectedContentItemIds());
            } else {
                if (data) {
                    const sourceIndex = data.sourceIndex ?? 0;
                    const destinationIndex = data.destinationIndex ?? 0;
                    const contentItem = activeContentItems[sourceIndex];

                    updatedPlaylistItems = getUpdatedPlaylistDtoItems(contentItem, destinationIndex);

                    dispatch(setPlaylistDtoPlaylistItems(updatedPlaylistItems));
                }
            }
        },
        [
            activeContentItems,
            contentItems,
            dispatch,
            getUpdatedPlaylistDtoItems,
            orderIndexOffset,
            selectedContentItemIds,
        ],
    );

    const addWebContentToPlaylist = useCallback(
        (contentItemId: string) => {
            let updatedPlaylistItems = [] as PlaylistItemType[];

            const selectedContentItems = webContentItems.filter(({ id }) => id === contentItemId);

            let updatedPlaylistDtoItemsWithScenario: PlaylistItemWithScenario[];

            selectedContentItems.forEach((contentItem, index) => {
                if (contentItem) {
                    const destinationIndex = orderIndexOffset + 1 + index;
                    updatedPlaylistItems = getUpdatedPlaylistDtoItems(
                        contentItem,
                        destinationIndex,
                        updatedPlaylistDtoItemsWithScenario,
                    );
                    updatedPlaylistDtoItemsWithScenario = sortAndIndexItems(
                        getPlaylistDtoItemsWithScenarios(updatedPlaylistItems),
                    );
                }
            });

            dispatch(setPlaylistDtoPlaylistItems(updatedPlaylistItems));

            // снимаем галочки с чекбоксов после добавления
            dispatch(setStepTwoSelectedContentItemIds());
        },
        [dispatch, getUpdatedPlaylistDtoItems, orderIndexOffset, webContentItems],
    );

    return {
        addContentToPlaylist,
        addWebContentToPlaylist,
        getUpdatedPlaylistDtoItems,
    };
};
