import React, { RefObject, useCallback, useRef } from 'react';

type ReturnValue = {
    anchorElRef: RefObject<HTMLSpanElement>;
    isOpen: boolean;
    onPopoverOpen: () => void;
    onPopoverClose: () => void;
};

export const usePopoverCustom = (): ReturnValue => {
    const anchorElRef = useRef<HTMLSpanElement>(null);
    const [isOpen, setIsOpen] = React.useState(false);

    const onPopoverOpen = useCallback(() => {
        setIsOpen(true);
    }, []);

    const onPopoverClose = useCallback(() => {
        setIsOpen(false);
    }, []);

    return {
        anchorElRef,
        isOpen,
        onPopoverOpen,
        onPopoverClose,
    };
};
