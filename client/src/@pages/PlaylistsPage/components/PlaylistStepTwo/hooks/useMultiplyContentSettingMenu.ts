import { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setShowModal } from '../../../../../components/ModalsContainer/actions';
import { MODALS } from '../../../../../components/ModalsContainer/types';
import { selectPlaylistDto, selectStepTwoSelectedPlaylistItemIds } from '../../../selectors';
import { setPlaylistDtoPlaylistItems, setStepTwoMenuMode, setStepTwoSelectedPlaylistItemIds } from '../../../actions';
import { usePlaylistItemsInfo } from '../../../../../hooks/usePlaylistItemsInfo';
import { CONTENT_TYPE } from '../../../../../../../common/types';
import { timeStringToSeconds, TWENTY_FOUR_HOURS_WITH_SECONDS_TIME_REGEX } from '../../../helpers';
import { formatSecondsToTimeString } from '../../../../../../../common/helpers';

type ReturnValue = {
    selectedPlaylistItemsNumber: number;
    playlistItemsDuration: string;
    playlistItemsFilesSize: string;
    isOnlyImageItems: boolean;
    isOnlyVideoItems: boolean;
    isOnlyWebItems: boolean;
    isDifferentDurations: boolean;
    onPlaylistItemsDurationChange: (e: ChangeEvent<HTMLInputElement>) => void;
    showTriggerContentSettingModal: () => void;
    deleteSelectedPlaylistItems: () => void;
    isWithScenariosOnly: boolean;
    isWithCreateScenarioButton: boolean;
    isItemsWithinOneScenarioSelected: boolean;
    isSelectableItemsOnly: boolean;
    t: TFunction;
};

export const useMultiplyContentSettingMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const { playlistDtoItems } = useSelector(selectPlaylistDto);
    const selectedPlaylistItemIds = useSelector(selectStepTwoSelectedPlaylistItemIds);

    const selectedPlaylistItems = useMemo(
        () => playlistDtoItems.filter(({ id }) => selectedPlaylistItemIds.includes(id)),
        [playlistDtoItems, selectedPlaylistItemIds],
    );

    const withSelectedItemsScenariosNumber = useMemo(
        () =>
            selectedPlaylistItems
                .filter((item) => item.scenario)
                .map((item) => item.scenario?.id ?? '')
                .reduce((prev: string[], curr) => (!prev.includes(curr) ? [...prev, curr] : prev), []).length,
        [selectedPlaylistItems],
    );

    const isWithScenariosOnly = useMemo(
        () => selectedPlaylistItems.filter((item) => item.scenario).length === selectedPlaylistItemIds.length,
        [selectedPlaylistItemIds.length, selectedPlaylistItems],
    );

    const isItemsWithinOneScenarioSelected = useMemo(
        () => withSelectedItemsScenariosNumber === 1 && isWithScenariosOnly,
        [isWithScenariosOnly, withSelectedItemsScenariosNumber],
    );

    const isSelectableItemsOnly = useMemo(() => {
        const itemsWithoutScenarios = playlistDtoItems.filter(
            (item) => selectedPlaylistItemIds.includes(item.id) && !item.scenario,
        );
        return (
            itemsWithoutScenarios.length === 1 ||
            itemsWithoutScenarios.every(
                ({ orderIndex }, index, arr) =>
                    arr[index + 1]?.orderIndex - orderIndex === 1 || orderIndex - arr[index - 1]?.orderIndex === 1,
            )
        );
    }, [playlistDtoItems, selectedPlaylistItemIds]);

    const isWithCreateScenarioButton = useMemo(
        () => selectedPlaylistItems.some((item) => item.scenario),
        [selectedPlaylistItems],
    );

    const { playlistItemsFilesSize } = usePlaylistItemsInfo(selectedPlaylistItems);

    const selectedPlaylistItemsNumber = selectedPlaylistItemIds.length;

    const isOnlyImageItems = useMemo(
        () => selectedPlaylistItems.every(({ content }) => content?.type === CONTENT_TYPE.IMAGE),
        [selectedPlaylistItems],
    );

    const isOnlyVideoItems = useMemo(
        () => selectedPlaylistItems.every(({ content }) => content?.type === CONTENT_TYPE.VIDEO),
        [selectedPlaylistItems],
    );

    const isOnlyWebItems = useMemo(
        () => selectedPlaylistItems.every(({ content }) => content?.type === CONTENT_TYPE.WEB),
        [selectedPlaylistItems],
    );

    const reducedDurations = useMemo(() => {
        if (isOnlyImageItems || isOnlyWebItems) {
            return selectedPlaylistItems
                .filter(({ content }) => content?.type === CONTENT_TYPE.IMAGE || content?.type === CONTENT_TYPE.WEB)
                .map((content) => content?.delay ?? 0)
                .reduce((prev: number[], curr) => (prev.includes(curr) ? prev : [...prev, curr]), []);
        }
        if (isOnlyVideoItems) {
            return selectedPlaylistItems
                .filter(({ content }) => content?.type === CONTENT_TYPE.VIDEO)
                .map(({ content }) => content?.duration ?? 0)
                .reduce((prev: number[], curr) => (prev.includes(curr) ? prev : [...prev, curr]), []);
        }
        return [];
    }, [isOnlyImageItems, isOnlyVideoItems, isOnlyWebItems, selectedPlaylistItems]);

    const isDifferentDurations = reducedDurations.length > 1;

    const currentSecondsValue = isDifferentDurations ? false : reducedDurations[0];

    const defaultPlaylistItemsDuration = currentSecondsValue
        ? formatSecondsToTimeString(reducedDurations[0])
        : 'Разная';

    const [playlistItemsDuration, setPlaylistItemsDuration] = useState(defaultPlaylistItemsDuration);

    const onPlaylistItemsDurationChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            if (value.match(TWENTY_FOUR_HOURS_WITH_SECONDS_TIME_REGEX)) {
                const newSecondsValue = timeStringToSeconds(value);

                if (isDifferentDurations || (currentSecondsValue && currentSecondsValue !== newSecondsValue)) {
                    const updatedPlaylistDtoItems = playlistDtoItems.map(({ id, delay, ...rest }) =>
                        selectedPlaylistItemIds.includes(id)
                            ? { id, delay: newSecondsValue, ...rest }
                            : { id, delay, ...rest },
                    );
                    dispatch(setPlaylistDtoPlaylistItems(updatedPlaylistDtoItems));
                }
            }

            setPlaylistItemsDuration(value);
        },
        [currentSecondsValue, dispatch, isDifferentDurations, playlistDtoItems, selectedPlaylistItemIds],
    );

    useEffect(() => {
        if (selectedPlaylistItemIds) {
            setPlaylistItemsDuration(defaultPlaylistItemsDuration);
        }
    }, [defaultPlaylistItemsDuration, selectedPlaylistItemIds]);

    const showTriggerContentSettingModal = useCallback(() => {
        dispatch(setShowModal(MODALS.TRIGGER_SCENARIO_SETTING));
    }, [dispatch]);

    const deleteSelectedPlaylistItems = useCallback(() => {
        dispatch(
            setPlaylistDtoPlaylistItems(playlistDtoItems.filter(({ id }) => !selectedPlaylistItemIds.includes(id))),
        );
        dispatch(setStepTwoSelectedPlaylistItemIds());
        dispatch(setStepTwoMenuMode('content_menu'));
    }, [dispatch, playlistDtoItems, selectedPlaylistItemIds]);

    return {
        selectedPlaylistItemsNumber,
        playlistItemsDuration,
        playlistItemsFilesSize,
        isOnlyImageItems,
        isOnlyVideoItems,
        isOnlyWebItems,
        isDifferentDurations,
        onPlaylistItemsDurationChange,
        showTriggerContentSettingModal,
        deleteSelectedPlaylistItems,
        isWithScenariosOnly,
        isWithCreateScenarioButton,
        isItemsWithinOneScenarioSelected,
        isSelectableItemsOnly,
        t,
    };
};
