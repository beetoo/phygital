import { ChangeEvent, useCallback, useEffect, useState } from 'react';
import { format } from 'date-fns';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { byDaysStringToByDaysValue, byDaysValueToByDaysString, isDateMatch, isTimeMatch } from '../../../helpers';
import { allDaysSelectedByDaysList, ByDay, ByDaysList } from '../../../types';
import { ScenarioRuleType, TimeData } from '../../../../../../../common/types';
import { frequencyFromByDay } from '../../../../../../../common/helpers';

type ReturnValue = {
    initialDay: string;
    startDay: string;
    endDay: string;
    onStartDateChange: (e: ChangeEvent<HTMLTextAreaElement>) => void;
    onEndDateChange: (e: ChangeEvent<HTMLTextAreaElement>) => void;

    dayValue: (day: ByDay) => boolean;
    onByDayChange: (day: ByDay) => () => void;

    startTimeValue: string | undefined;
    endTimeValue: string | undefined;
    onStartTimeChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onEndTimeChange: (e: ChangeEvent<HTMLInputElement>) => void;

    t: TFunction;
};

type Props = {
    data?: TimeData;
    updateRulesList: (data: Omit<ScenarioRuleType, 'id'>) => void;
};

export const useTimeRow = ({ data, updateRulesList }: Props): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const initialDay = format(new Date(), 'yyyy-MM-dd');

    const initialStartDay = data?.startDay || initialDay;
    const initialEndDay = data?.endDay || initialDay;

    const initialStartTimeValue = data?.startTime || '00:00';
    const initialEndTimeValue = data?.endTime || '00:00';

    const [startDay, setStartDay] = useState(initialStartDay);
    const [endDay, setEndDay] = useState(initialEndDay);
    const [startTimeValue, setStartTimeValue] = useState<string>(initialStartTimeValue);
    const [endTimeValue, setEndTimeValue] = useState<string>(initialEndTimeValue);

    const onStartDateChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLTextAreaElement>) => {
            const isDateValid = isDateMatch(value);
            const isTimeValid = isTimeMatch(startTimeValue);

            if (isDateValid) {
                setStartDay(value);

                if (isTimeValid) {
                    updateRulesList({
                        data: { startDay: value },
                    });
                }
            }
        },
        [startTimeValue, updateRulesList],
    );

    const onEndDateChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLTextAreaElement>) => {
            const isDateValid = isDateMatch(value);
            const isTimeValid = isTimeMatch(endTimeValue);

            if (isDateValid) {
                setEndDay(value);

                if (isTimeValid) {
                    updateRulesList({
                        data: { endDay: value },
                    });
                }
            }
        },
        [endTimeValue, updateRulesList],
    );

    const initialByDays =
        data && 'byDay' in data ? byDaysStringToByDaysValue(data.byDay as string) : allDaysSelectedByDaysList;

    const [byDays, setByDays] = useState<ByDaysList>(initialByDays);

    const onStartTimeChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            const isTimeValid = isTimeMatch(value);

            if (isTimeValid) {
                setStartTimeValue(value);
                updateRulesList({ data: { startTime: value } });
            } else {
                updateRulesList({ data: { startTime: undefined } });
            }
        },
        [updateRulesList],
    );

    const onEndTimeChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            const isTimeValid = isTimeMatch(value);

            if (isTimeValid) {
                setEndTimeValue(value);
                updateRulesList({ data: { endTime: value } });
            } else {
                updateRulesList({ data: { endTime: undefined } });
            }
        },
        [updateRulesList],
    );

    const onByDayChange = useCallback(
        (day: ByDay) => () => {
            const changedByDays = byDays.map((dayValue) =>
                day in dayValue ? { [day]: !Object.values(dayValue)[0] } : dayValue,
            ) as ByDaysList;

            setByDays(changedByDays);

            const byDay = byDaysValueToByDaysString(changedByDays);
            const frequency = frequencyFromByDay(byDay);

            updateRulesList({ data: { byDay, frequency } });
        },
        [byDays, updateRulesList],
    );

    const dayValue = useCallback(
        (day: ByDay) => {
            const requiredDay = byDays.find((dayValue) => day in dayValue);
            if (requiredDay) {
                return Object.values(requiredDay)[0];
            } else {
                return false;
            }
        },
        [byDays],
    );

    useEffect(() => {
        if (!data) {
            updateRulesList({
                data: {
                    startDay: initialStartDay,
                    endDay: initialEndDay,
                    startTime: '00:00',
                    endTime: '00:00',
                    frequency: 'DAILY',
                    byDay: 'MO,TU,WE,TH,FR,SA,SU',
                },
            });
        }
    }, [data, initialEndDay, initialStartDay, updateRulesList]);

    return {
        initialDay,
        startDay,
        endDay,
        onStartDateChange,
        onEndDateChange,

        dayValue,
        onByDayChange,

        startTimeValue,
        endTimeValue,
        onStartTimeChange,
        onEndTimeChange,

        t,
    };
};
