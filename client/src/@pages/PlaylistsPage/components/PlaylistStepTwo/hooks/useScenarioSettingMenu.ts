import { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setShowModal } from '../../../../../components/ModalsContainer/actions';
import { MODALS } from '../../../../../components/ModalsContainer/types';
import { selectPlaylistDto, selectStepTwoActiveScenarioId, selectStepTwoShowedScenariosIds } from '../../../selectors';
import {
    setPlaylistDtoPlaylistItems,
    setStepTwoActivePlaylistItemId,
    setStepTwoMenuMode,
    setStepTwoShowedScenariosIds,
} from '../../../actions';
import { getPlaylistDtoItemsWithScenarios, getReorderedItemsWithScenarios } from '../../../../../helpers';
import { PlaylistItemType, ScenarioRuleType, TimeData, WeatherAndTrafficData } from '../../../../../../../common/types';
import { ScenarioWithPlaylistItems } from './useStepTwoPlaylistItems';

export interface WeatherAndTrafficRuleType extends ScenarioRuleType {
    data: WeatherAndTrafficData;
}

export interface TimeRuleType extends ScenarioRuleType {
    data: TimeData;
}

type ReturnValue = {
    scenarioName: string;
    scenarioDescription: string;
    scenarioIndex: string;

    onScenarioPlaceIndexChange: (e: ChangeEvent<HTMLInputElement>) => void;
    addContentToScenario: () => void;
    showTriggerContentSettingModal: () => void;
    deleteScenario: () => void;

    isRulesShown: boolean;
    showRules: () => void;
    isUsablePlaylistItemsShown: boolean;
    showUsablePlaylistItems: () => void;

    weatherRules: WeatherAndTrafficRuleType[];
    timeRules: TimeRuleType[];
    usablePlaylistItems: Pick<PlaylistItemType, 'id' | 'name'>[];
    goToUsablePlaylistItem: (id: string) => () => void;
    t: TFunction;
};

export const useScenarioSettingMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const { playlistDtoItems } = useSelector(selectPlaylistDto);
    const activeScenarioId = useSelector(selectStepTwoActiveScenarioId);
    const showedScenariosIds = useSelector(selectStepTwoShowedScenariosIds);

    const [isRulesShown, setIsRulesShown] = useState(false);
    const [isUsablePlaylistItemsShown, setIsUsablePlaylistItemsShown] = useState(false);

    const showRules = useCallback(() => {
        setIsRulesShown((prev) => !prev);
    }, []);

    const showUsablePlaylistItems = useCallback(() => {
        setIsUsablePlaylistItemsShown((prev) => !prev);
    }, []);

    const playlistDtoItemsWithScenarios = useMemo(
        () => getPlaylistDtoItemsWithScenarios(playlistDtoItems),
        [playlistDtoItems],
    );

    const {
        scenario: { name: scenarioName, description: scenarioDescription, rules: scenarioRules },
        playlistItems: usablePlaylistItems,
    } = useMemo(() => {
        const activeItem = (playlistDtoItemsWithScenarios.find(
            (item) => item.scenario?.id === activeScenarioId,
        ) as ScenarioWithPlaylistItems) ?? {
            scenario: {
                name: '',
                description: '',
                rules: [],
            },
            playlistItems: [],
        };

        return { ...activeItem, playlistItems: activeItem.playlistItems.map(({ id, name }) => ({ id, name })) };
    }, [activeScenarioId, playlistDtoItemsWithScenarios]);

    const weatherRules = scenarioRules.filter(
        (rule: ScenarioRuleType) => rule.condition === 'weather',
    ) as WeatherAndTrafficRuleType[];
    const timeRules = scenarioRules.filter((rule: ScenarioRuleType) => rule.condition === 'time') as TimeRuleType[];

    const goToUsablePlaylistItem = useCallback(
        (id: string) => () => {
            dispatch(setStepTwoActivePlaylistItemId(id));
            dispatch(setStepTwoMenuMode('scenario_content_setting_menu'));
        },
        [dispatch],
    );

    const initialScenarioIndex = useMemo(
        () => playlistDtoItemsWithScenarios.findIndex((item) => item?.scenario?.id === activeScenarioId) + 1,
        [activeScenarioId, playlistDtoItemsWithScenarios],
    );

    const [scenarioIndex, setScenarioIndex] = useState<string>(initialScenarioIndex.toString());

    const onScenarioPlaceIndexChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            if (value) {
                if (!value.match(/\d{1,4}/)) return;

                const numValue = Number(value);
                if (numValue === 0 || numValue > playlistDtoItemsWithScenarios.length) return;

                const scenarioIndexValue = Number(scenarioIndex);
                if (scenarioIndexValue !== numValue) {
                    const reorderedItems = getReorderedItemsWithScenarios({
                        playlistDtoItems,
                        from: scenarioIndexValue - 1,
                        to: numValue - 1,
                    });

                    dispatch(setPlaylistDtoPlaylistItems(reorderedItems));
                }
            }
            setScenarioIndex(value);
        },
        [dispatch, playlistDtoItems, playlistDtoItemsWithScenarios.length, scenarioIndex],
    );

    // обновляем значение в инпуте для индекса при каждом изменении активного контента
    useEffect(() => {
        if (initialScenarioIndex.toString()) {
            setScenarioIndex(initialScenarioIndex.toString());
        }
    }, [initialScenarioIndex]);

    const showTriggerContentSettingModal = useCallback(() => {
        dispatch(setShowModal(MODALS.TRIGGER_SCENARIO_SETTING));
    }, [dispatch]);

    const deleteScenario = useCallback(() => {
        const updatedPlaylistDtoItems = playlistDtoItems.map(({ scenario, ...rest }) =>
            scenario?.id === activeScenarioId
                ? { ...rest, scenario: undefined, scenarioId: null }
                : { scenario, ...rest },
        );

        dispatch(setPlaylistDtoPlaylistItems(updatedPlaylistDtoItems));
        dispatch(setStepTwoShowedScenariosIds(showedScenariosIds.filter((id) => id !== activeScenarioId)));
        dispatch(setStepTwoMenuMode('content_menu'));
    }, [activeScenarioId, dispatch, playlistDtoItems, showedScenariosIds]);

    const addContentToScenario = useCallback(() => {
        dispatch(setStepTwoMenuMode('content_menu_to_scenario'));
    }, [dispatch]);

    return {
        scenarioName,
        scenarioDescription,
        scenarioIndex,

        onScenarioPlaceIndexChange,
        addContentToScenario,
        showTriggerContentSettingModal,
        deleteScenario,

        isRulesShown,
        showRules,
        isUsablePlaylistItemsShown,
        showUsablePlaylistItems,

        weatherRules,
        timeRules,
        usablePlaylistItems,
        goToUsablePlaylistItem,

        t,
    };
};
