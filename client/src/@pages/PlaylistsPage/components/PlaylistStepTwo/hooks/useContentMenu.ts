import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { setStepTwoSelectedContentItemIds } from '../../../actions';
import { setOpenContentViewModal, setShowModal } from '../../../../../components/ModalsContainer/actions';
import { MODALS } from '../../../../../components/ModalsContainer/types';
import { useFolders } from '../../../../ContentPage/hooks/useFolders';
import { setActiveFolderId } from '../../../../ContentPage/actions';
import { selectActiveFolderId, selectContentItems } from '../../../../ContentPage/selectors';
import { selectPlaylistDto, selectStepTwoSelectedContentItemIds } from '../../../selectors';
import { ContentType, FolderType } from '../../../../../../../common/types';

type ReturnValue = {
    activeFolderId: string;
    activeContentItems: ContentType[];
    openDeleteContentItemAlert: (contentItemId: string) => () => void;
    showContentViewModal: (contentId: string) => () => void;
    folders: FolderType[];
    onFolderClick: (folderId: string) => () => void;
    openAddWebContentToPlaylistModal: () => void;

    isContentItemSelected: (contentItemId: string) => boolean;
    selectContentItem: (contentItemId: string) => () => void;
    unselectContentItem: (contentItemId: string) => () => void;
    isDeleteCheckboxShown: (contentItemId: string) => boolean;
    t: TFunction;
};

export const useContentMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const { folders } = useFolders();

    const selectedContentItemIds = useSelector(selectStepTwoSelectedContentItemIds);
    const { playlistDtoItems } = useSelector(selectPlaylistDto);
    const contentItems = useSelector(selectContentItems);
    const activeFolderId = useSelector(selectActiveFolderId);

    const openAddWebContentToPlaylistModal = useCallback(() => {
        dispatch(setShowModal(MODALS.ADD_WEB_CONTENT_TO_PLAYLIST_MODAL));
    }, [dispatch]);

    const activeContentItems = useMemo(
        () => contentItems.filter(({ folder }) => (activeFolderId === '' ? !folder : folder?.id === activeFolderId)),
        [activeFolderId, contentItems],
    );

    const onFolderClick = useCallback(
        (folderId: string) => () => {
            dispatch(setActiveFolderId(folderId));
        },
        [dispatch],
    );

    const openDeleteContentItemAlert = useCallback(
        (contentItemId: string) => () => {
            dispatch(setShowModal(MODALS.DELETE_CONTENT_ITEM, '', '', contentItemId));
        },
        [dispatch],
    );

    const showContentViewModal = useCallback(
        (contentId: string) => () => {
            const contentLinks = activeContentItems.map(({ originFilename, type, src, dimensions }) => ({
                name: originFilename,
                type,
                src,
                dimensions,
            }));
            const currentIndex = activeContentItems.findIndex(({ id }) => id === contentId);
            dispatch(setOpenContentViewModal(contentLinks, currentIndex));
        },
        [activeContentItems, dispatch],
    );

    const isContentItemSelected = useCallback(
        (contentItemId: string) => selectedContentItemIds.includes(contentItemId),
        [selectedContentItemIds],
    );

    const selectContentItem = useCallback(
        (contentItemId: string) => () => {
            dispatch(setStepTwoSelectedContentItemIds([...selectedContentItemIds, contentItemId]));
        },
        [dispatch, selectedContentItemIds],
    );

    const unselectContentItem = useCallback(
        (contentItemId: string) => () => {
            dispatch(setStepTwoSelectedContentItemIds(selectedContentItemIds.filter((id) => id !== contentItemId)));
        },
        [dispatch, selectedContentItemIds],
    );

    const isDeleteCheckboxShown = useCallback(
        (contentItemId: string) =>
            !(
                selectedContentItemIds.includes(contentItemId) ||
                playlistDtoItems.some((item) => item.content?.id === contentItemId)
            ),
        [playlistDtoItems, selectedContentItemIds],
    );

    return {
        activeFolderId,
        activeContentItems,
        openAddWebContentToPlaylistModal,
        openDeleteContentItemAlert,
        showContentViewModal,
        folders,
        onFolderClick,
        isContentItemSelected,
        selectContentItem,
        unselectContentItem,
        isDeleteCheckboxShown,
        t,
    };
};
