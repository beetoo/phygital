import { ChangeEvent, useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectPlaylistDto, selectStepTwoActivePlaylistItemId } from '../../../selectors';
import { usePlaylistItemInfo } from '../../../../../hooks/usePlaylistItemInfo';
import { setPlaylistDtoPlaylistItems, setStepTwoMenuMode } from '../../../actions';
import { getPlaylistDtoItemsWithScenarios, getReorderedItems } from '../../../../../helpers';
import { timeStringToSeconds, TWENTY_FOUR_HOURS_WITH_SECONDS_TIME_REGEX } from '../../../helpers';
import { PlaylistItemType } from '../../../../../../../common/types';
import { setOpenContentViewModal } from '../../../../../components/ModalsContainer/actions';
import { ScenarioWithPlaylistItems } from './useStepTwoPlaylistItems';

type ReturnValue = {
    playlistItemSrc: string;
    playlistItemName: string;
    playlistItemFileSize: string;
    playlistItemFileTypeAndExtension: string;
    playlistItemDimensions: string;
    playlistItemOrientation: string;
    isVideoType: boolean;
    isImageType: boolean;
    isWebType: boolean;

    playlistItemDuration: string;
    onPlaylistItemDurationChange: (e: ChangeEvent<HTMLInputElement>) => void;
    placeIndex: string;
    onPlaylistItemPlaceIndexChange: (e: ChangeEvent<HTMLInputElement>) => void;
    deletePlaylistItem: () => void;

    isContentInfoShown: boolean;
    showContentInfo: () => void;
    showContentViewModal: () => void;
    t: TFunction;
};

export const useScenarioContentSettingMenu = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const { playlistDtoItems } = useSelector(selectPlaylistDto);
    const activePlaylistItemId = useSelector(selectStepTwoActivePlaylistItemId);

    const playlistDtoItemsWithScenarios = useMemo(
        () => getPlaylistDtoItemsWithScenarios(playlistDtoItems),
        [playlistDtoItems],
    );

    const activePlaylistItem = useMemo(
        () => playlistDtoItems.find(({ id }) => id === activePlaylistItemId),
        [activePlaylistItemId, playlistDtoItems],
    );

    const [isContentInfoShown, setIsContentInfoShown] = useState(false);

    const showContentInfo = useCallback(() => {
        setIsContentInfoShown((prev) => !prev);
    }, []);

    const {
        playlistItemSrc,
        playlistItemName,
        playlistItemFileTypeAndExtension,
        playlistItemFileSize,
        playlistItemDimensions,
        playlistItemOrientation,
        playlistItemDuration: defaultPlaylistItemDuration,
        isVideoType,
        isImageType,
        isWebType,
    } = usePlaylistItemInfo(activePlaylistItem);

    const [playlistItemDuration, setPlaylistItemDuration] = useState(defaultPlaylistItemDuration);

    const onPlaylistItemDurationChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            if (value.match(TWENTY_FOUR_HOURS_WITH_SECONDS_TIME_REGEX)) {
                const newSecondsValue = timeStringToSeconds(value);
                const currentSecondsValue = timeStringToSeconds(defaultPlaylistItemDuration);

                if (currentSecondsValue !== newSecondsValue) {
                    const updatedPlaylistDtoItems = playlistDtoItems.map(({ id, delay, ...rest }) =>
                        id === activePlaylistItemId ? { id, delay: newSecondsValue, ...rest } : { id, delay, ...rest },
                    );
                    dispatch(setPlaylistDtoPlaylistItems(updatedPlaylistDtoItems));
                }
            }
            setPlaylistItemDuration(value);
        },
        [activePlaylistItemId, defaultPlaylistItemDuration, dispatch, playlistDtoItems],
    );

    // обновляем значение в инпуте при каждом изменении активного контента
    useEffect(() => {
        if (defaultPlaylistItemDuration) {
            setPlaylistItemDuration(defaultPlaylistItemDuration);
        }
    }, [defaultPlaylistItemDuration]);

    const activeScenarioIndex = useMemo(
        () =>
            playlistDtoItemsWithScenarios.findIndex((item) =>
                'playlistItems' in item
                    ? item.playlistItems.map(({ id }: PlaylistItemType) => id).includes(activePlaylistItemId)
                    : false,
            ),
        [activePlaylistItemId, playlistDtoItemsWithScenarios],
    );

    const activeScenarioPlaylistItems = useMemo(
        () =>
            (
                playlistDtoItemsWithScenarios.find((item) =>
                    'playlistItems' in item
                        ? item.playlistItems.map(({ id }: PlaylistItemType) => id).includes(activePlaylistItemId)
                        : false,
                ) as ScenarioWithPlaylistItems
            )?.playlistItems ?? [],
        [activePlaylistItemId, playlistDtoItemsWithScenarios],
    );

    const activePlaylistItemIndex = useMemo(
        () => activeScenarioPlaylistItems.findIndex(({ id }) => id === activePlaylistItemId) + 1,
        [activePlaylistItemId, activeScenarioPlaylistItems],
    );

    const [placeIndex, setPlaceIndex] = useState<string>(activePlaylistItemIndex.toString());

    const onPlaylistItemPlaceIndexChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            if (value) {
                if (!value.match(/\d{1,4}/)) return;

                const numValue = Number(value);
                if (numValue === 0 || numValue > activeScenarioPlaylistItems.length) return;

                if (activePlaylistItemIndex !== numValue) {
                    const reorderedItems = getReorderedItems({
                        playlistDtoItems,
                        from: activeScenarioIndex + activePlaylistItemIndex - 1,
                        to: activeScenarioIndex + numValue - 1,
                    });

                    dispatch(setPlaylistDtoPlaylistItems(reorderedItems));
                }
            }
            setPlaceIndex(value);
        },
        [activePlaylistItemIndex, activeScenarioIndex, activeScenarioPlaylistItems.length, dispatch, playlistDtoItems],
    );

    // обновляем значение в инпуте для индекса при каждом изменении активного контента
    useEffect(() => {
        if (activePlaylistItemIndex.toString()) {
            setPlaceIndex(activePlaylistItemIndex.toString());
        }
    }, [activePlaylistItemIndex]);

    const deletePlaylistItem = useCallback(() => {
        dispatch(setPlaylistDtoPlaylistItems(playlistDtoItems.filter(({ id }) => id !== activePlaylistItemId)));
        dispatch(setStepTwoMenuMode('content_menu'));
    }, [activePlaylistItemId, dispatch, playlistDtoItems]);

    const showContentViewModal = useCallback(() => {
        if (activePlaylistItem?.content) {
            const { originFilename, type, src, dimensions } = activePlaylistItem.content;
            const viewItem = { name: originFilename, type, src, dimensions };
            dispatch(setOpenContentViewModal([viewItem], 0));
        }
    }, [activePlaylistItem, dispatch]);

    return {
        playlistItemSrc,
        playlistItemName,
        playlistItemFileTypeAndExtension,
        playlistItemFileSize,
        playlistItemDimensions,
        playlistItemOrientation,
        isVideoType,
        isImageType,
        isWebType,

        playlistItemDuration,
        onPlaylistItemDurationChange,
        placeIndex,
        onPlaylistItemPlaceIndexChange,
        deletePlaylistItem,

        isContentInfoShown,
        showContentInfo,
        showContentViewModal,

        t,
    };
};
