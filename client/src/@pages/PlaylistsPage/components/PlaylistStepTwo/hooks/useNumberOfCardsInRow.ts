import { useMemo } from 'react';
import { useWindowDimensions } from '../../../../../hooks/useWindowDimensions';

type ReturnValue = {
    numberOfCardsInRow: number;
};

export const useNumberOfCardsInRow = (): ReturnValue => {
    const { width } = useWindowDimensions();

    const numberOfCardsInRow = useMemo(() => {
        const menuWidth = 80;
        const contentMenuWidth = 340;
        const leftPaddingWidth = 24;
        const otherWidth = 8;
        const oneCardWidth = 182;

        const playlistItemsRowWidth = width - menuWidth - contentMenuWidth - leftPaddingWidth - otherWidth;
        return Math.floor(playlistItemsRowWidth / oneCardWidth);
    }, [width]);

    return {
        numberOfCardsInRow,
    };
};
