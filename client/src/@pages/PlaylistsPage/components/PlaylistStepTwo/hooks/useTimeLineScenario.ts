import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import {
    selectStepTwoActivePlaylistItemId,
    selectStepTwoActiveScenarioId,
    selectStepTwoSelectedContentItemIds,
    selectStepTwoSelectedPlaylistItemIds,
} from '../../../selectors';
import {
    setStepTwoActivePlaylistItemId,
    setStepTwoActiveScenarioId,
    setStepTwoMenuMode,
    setStepTwoSelectedContentItemIds,
    setStepTwoSelectedPlaylistItemIds,
} from '../../../actions';
import { PlaylistItemType, CONTENT_TYPE } from '../../../../../../../common/types';

type ReturnValue = {
    selectedPlaylistItemIds: string[];
    setActiveScenario: (scenarioId: string) => void;
    setActivePlaylistItem: (playlistItemId: string) => void;
    selectPlaylistItem: (playlistItemId: string) => void;
    unselectPlaylistItem: (playlistItemId: string) => void;
    isPlaylistItemActive: (playlistItemId: string) => boolean;
    isPlaylistItemSelected: (playlistItemId: string) => boolean;
    isScenarioActive: (scenarioId: string) => boolean;
    scenarioSeconds: number;
    t: TFunction;
};

type Props = {
    playlistItems: PlaylistItemType[];
};

export const useTimeLineScenario = ({ playlistItems }: Props): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'contentModal' });

    const dispatch = useDispatch();

    const activePlaylistItemId = useSelector(selectStepTwoActivePlaylistItemId);
    const activeScenarioId = useSelector(selectStepTwoActiveScenarioId);
    const selectedPlaylistItemIds = useSelector(selectStepTwoSelectedPlaylistItemIds);
    const selectedContentItemIds = useSelector(selectStepTwoSelectedContentItemIds);

    const scenarioSeconds = useMemo(
        () =>
            playlistItems
                .map(({ delay, content }) =>
                    content?.type === CONTENT_TYPE.IMAGE || content?.type === CONTENT_TYPE.WEB
                        ? delay
                        : (content?.duration ?? 0),
                )
                .reduce((acc, curr) => acc + curr, 0),
        [playlistItems],
    );

    const setActiveScenario = useCallback(
        (scenarioId: string) => {
            if (selectedPlaylistItemIds.length > 0) return;
            if (selectedContentItemIds.length > 0) {
                dispatch(setStepTwoSelectedContentItemIds());
            }
            dispatch(setStepTwoActiveScenarioId(scenarioId));
        },
        [dispatch, selectedContentItemIds.length, selectedPlaylistItemIds.length],
    );

    const setActivePlaylistItem = useCallback(
        (playlistItemId: string) => {
            if (selectedPlaylistItemIds.length > 0) return;
            dispatch(setStepTwoMenuMode('scenario_content_setting_menu'));
            dispatch(setStepTwoActivePlaylistItemId(playlistItemId));
        },
        [dispatch, selectedPlaylistItemIds.length],
    );

    const isPlaylistItemSelected = useCallback(
        (playlistItemId: string) => selectedPlaylistItemIds.includes(playlistItemId),
        [selectedPlaylistItemIds],
    );

    const selectPlaylistItem = useCallback(
        (playlistItemId: string) => {
            dispatch(setStepTwoSelectedPlaylistItemIds([...selectedPlaylistItemIds, playlistItemId]));
        },
        [dispatch, selectedPlaylistItemIds],
    );

    const unselectPlaylistItem = useCallback(
        (playlistItemId: string) => {
            dispatch(setStepTwoSelectedPlaylistItemIds(selectedPlaylistItemIds.filter((id) => id !== playlistItemId)));
        },
        [dispatch, selectedPlaylistItemIds],
    );

    const isScenarioActive = useCallback((scenarioId: string) => scenarioId === activeScenarioId, [activeScenarioId]);

    const isPlaylistItemActive = useCallback(
        (playlistItemId: string) => playlistItemId === activePlaylistItemId,
        [activePlaylistItemId],
    );

    return {
        selectedPlaylistItemIds,
        setActiveScenario,
        setActivePlaylistItem,
        selectPlaylistItem,
        unselectPlaylistItem,
        isPlaylistItemActive,
        isPlaylistItemSelected,
        isScenarioActive,
        scenarioSeconds,
        t,
    };
};
