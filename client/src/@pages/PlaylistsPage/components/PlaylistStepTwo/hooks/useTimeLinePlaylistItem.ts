import { useCallback, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import {
    selectStepTwoActivePlaylistItemId,
    selectStepTwoScenariosFilterOpen,
    selectStepTwoSelectedPlaylistItemIds,
} from '../../../selectors';
import {
    setStepTwoActivePlaylistItemId,
    setStepTwoMenuMode,
    setStepTwoScenarioFilterOpen,
    setStepTwoSelectedPlaylistItemIds,
} from '../../../actions';
import { StepTwoMenuMode } from '../../../types';
import { CONTENT_TYPE } from '../../../../../../../common/types';

type Props = { delay: number; duration: number; type: CONTENT_TYPE };

type ReturnValue = {
    selectedPlaylistItemIds: string[];
    setMenuMode: (menuMode: StepTwoMenuMode) => void;
    setActivePlaylistItem: (playlistItemId: string) => void;
    selectPlaylistItem: (playlistItemId: string) => void;
    unselectPlaylistItem: (playlistItemId: string) => void;
    isPlaylistItemActive: (playlistItemId: string) => boolean;
    isPlaylistItemSelected: (playlistItemId: string) => boolean;
    playlistItemSeconds: number;
    closeScenariosList: () => void;

    isTooltipShown: boolean;
    showTooltip: () => void;
    hideTooltip: () => void;

    t: TFunction;
};

export const useTimeLinePlaylistItem = ({ delay, duration, type }: Props): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'contentModal' });

    const dispatch = useDispatch();

    const activePlaylistItemId = useSelector(selectStepTwoActivePlaylistItemId);
    const selectedPlaylistItemIds = useSelector(selectStepTwoSelectedPlaylistItemIds);

    const setMenuMode = useCallback(
        (menuMode: StepTwoMenuMode) => {
            if (selectedPlaylistItemIds.length > 0) return;
            dispatch(setStepTwoMenuMode(menuMode));
        },
        [dispatch, selectedPlaylistItemIds.length],
    );

    const setActivePlaylistItem = useCallback(
        (playlistItemId: string) => {
            if (selectedPlaylistItemIds.length > 0) return;
            dispatch(setStepTwoActivePlaylistItemId(playlistItemId));
        },
        [dispatch, selectedPlaylistItemIds.length],
    );

    const selectPlaylistItem = useCallback(
        (playlistItemId: string) => {
            dispatch(setStepTwoSelectedPlaylistItemIds([...selectedPlaylistItemIds, playlistItemId]));
        },
        [dispatch, selectedPlaylistItemIds],
    );

    const unselectPlaylistItem = useCallback(
        (playlistItemId: string) => {
            dispatch(setStepTwoSelectedPlaylistItemIds(selectedPlaylistItemIds.filter((id) => id !== playlistItemId)));
        },
        [dispatch, selectedPlaylistItemIds],
    );

    const isPlaylistItemActive = useCallback(
        (playlistItemId: string) => playlistItemId === activePlaylistItemId,
        [activePlaylistItemId],
    );

    const isPlaylistItemSelected = useCallback(
        (playlistItemId: string) => selectedPlaylistItemIds.includes(playlistItemId),
        [selectedPlaylistItemIds],
    );

    const playlistItemSeconds = useMemo(
        () => (type === CONTENT_TYPE.IMAGE || type === CONTENT_TYPE.WEB ? delay : duration),
        [delay, duration, type],
    );

    const isScenariosListOpen = useSelector(selectStepTwoScenariosFilterOpen);

    const closeScenariosList = useCallback(() => {
        if (isScenariosListOpen) {
            dispatch(setStepTwoScenarioFilterOpen(false));
        }
    }, [dispatch, isScenariosListOpen]);

    const [isTooltipShown, setIsTooltipShown] = useState(false);

    const showTooltip = useCallback(() => {
        setIsTooltipShown(true);
    }, []);

    const hideTooltip = useCallback(() => {
        setIsTooltipShown(false);
    }, []);

    return {
        selectedPlaylistItemIds,
        setMenuMode,
        setActivePlaylistItem,
        selectPlaylistItem,
        unselectPlaylistItem,
        isPlaylistItemActive,
        isPlaylistItemSelected,
        playlistItemSeconds,
        closeScenariosList,

        isTooltipShown,
        showTooltip,
        hideTooltip,

        t,
    };
};
