import { ChangeEvent, Dispatch, SetStateAction, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import {
    CloudCoverParameterValue,
    ConditionParameter,
    HumidityParameterValue,
    ParameterOperator,
    ParameterValue,
    RainFallParameterValue,
    WeatherParameter,
    WindParameterValue,
} from '../components/TriggerScenarioSettingModal/types';
import { ScenarioRuleType, WeatherAndTrafficData } from '../../../../../../../common/types';
import {
    getRuleListFilterByParameter,
    isOperatorAndValueMixError,
    tSelectOperator,
    tOperator,
    tSelectParameter,
    tParameter,
    tSelectParameterValue,
    tParameterValue,
} from '../components/TriggerScenarioSettingModal/helpers';

type ReturnValue = {
    conditionParameter: ConditionParameter | '';
    onConditionParameterSelect: (e: SelectChangeEvent) => void;

    parameterOperator: ParameterOperator | '';
    onParameterOperatorSelect: (e: SelectChangeEvent) => void;

    parameterValue: ParameterValue | '';
    parameterValues: ParameterValue[];
    onParameterValueSelect: (e: SelectChangeEvent) => void;
    onParameterValueChange: (e: ChangeEvent) => void;

    isOperatorRequired: boolean;
    isConditionParameterDisabled: (parameter: ConditionParameter) => boolean;
    isParameterValueDisabled: (value: ParameterValue) => boolean;
    isRowValueError: boolean;

    t: TFunction;
    weatherConditionParameters: ConditionParameter[];
    parameterOperators: ParameterOperator[];
};

type Props = {
    weatherRowId: string;
    rulesList: ScenarioRuleType[];
    data?: WeatherAndTrafficData;
    updateRulesList: (data: Omit<ScenarioRuleType, 'id'>) => void;
    setRowIdsWithError: Dispatch<SetStateAction<string[]>>;
};

export const useWeatherRow = ({
    weatherRowId,
    rulesList,
    data,
    updateRulesList,
    setRowIdsWithError,
}: Props): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    const weatherConditionParameters = useMemo(
        () => JSON.parse(t('weatherConditionParameters')) as ConditionParameter[],
        [t],
    );
    const parameterOperators = useMemo(() => JSON.parse(t('parameterOperators')) as ParameterOperator[], [t]);
    const cloudCoverConditions = useMemo(
        () => JSON.parse(t('cloudCoverConditions')) as CloudCoverParameterValue[],
        [t],
    );
    const rainFallConditions = useMemo(() => JSON.parse(t('rainFallConditions')) as RainFallParameterValue[], [t]);
    const windConditions = useMemo(() => JSON.parse(t('windConditions')) as WindParameterValue[], [t]);
    const humidityConditions = useMemo(() => JSON.parse(t('humidityConditions')) as HumidityParameterValue[], [t]);

    const initialConditionParameter = data && 'parameter' in data ? tParameter(data.parameter ?? '', t) : '';
    const initialParameterOperator =
        data && 'operator' in data ? tOperator(data.operator ?? '', t) : (t('less') as ParameterOperator);
    const defaultValue =
        initialConditionParameter === t('atmosphericPressure')
            ? '760'
            : initialConditionParameter.includes(t('temperature'))
              ? '0'
              : '';

    const initialParameterValue = data && 'value' in data ? tParameterValue(data.value ?? '', t) : defaultValue;

    const [conditionParameter, setConditionParameter] = useState<ConditionParameter | ''>(initialConditionParameter);
    const [parameterOperator, setParameterOperator] = useState<ParameterOperator | ''>('');
    const [parameterValue, setParameterValue] = useState<ParameterValue | ''>('');
    const [parameterValues, setParameterValues] = useState<ParameterValue[]>([]);

    const conditionsWithRequiredOperatorOne = useMemo(
        () => [t('temperature'), t('waterTemperature'), t('atmosphericPressure')] as ConditionParameter[],
        [t],
    );

    const conditionsWithRequiredOperatorTwo = useMemo(() => [t('wind'), t('airHumidity')] as ConditionParameter[], [t]);

    const conditionsWithRequiredOperatorThree = useMemo(
        () => [t('cloudiness'), t('precipitation')] as ConditionParameter[],
        [t],
    );

    const initialIsOperatorRequired = useMemo(
        () =>
            conditionsWithRequiredOperatorOne.includes(conditionParameter as ConditionParameter) ||
            (conditionsWithRequiredOperatorTwo.includes(conditionParameter as ConditionParameter) &&
                Boolean(data?.operator)),
        [conditionParameter, conditionsWithRequiredOperatorOne, conditionsWithRequiredOperatorTwo, data?.operator],
    );

    const [isOperatorRequired, setIsOperatorRequired] = useState(initialIsOperatorRequired);

    const onConditionParameterSelect = useCallback(
        ({ target: { value } }: SelectChangeEvent<ConditionParameter>) => {
            if (conditionsWithRequiredOperatorTwo.includes(value as ConditionParameter)) {
                setIsOperatorRequired(false);
                setParameterValue('');
                updateRulesList({ data: { value: undefined } });
            }

            setConditionParameter(value as ConditionParameter);

            updateRulesList({ data: { parameter: tSelectParameter(value as ConditionParameter, t) } });
        },
        [conditionsWithRequiredOperatorTwo, t, updateRulesList],
    );

    const setOperator = useCallback(
        (operator: ParameterOperator | '' = '') => {
            if (parameterOperator !== operator) {
                setParameterOperator(operator);

                updateRulesList({ data: { operator: tSelectOperator(operator, t) } });
            }
        },
        [parameterOperator, t, updateRulesList],
    );

    const onParameterOperatorSelect = useCallback(
        ({ target: { value } }: SelectChangeEvent<ParameterOperator>) => {
            setOperator(value as ParameterOperator);
        },
        [setOperator],
    );

    const setValue = useCallback(
        (value: ParameterValue) => {
            if (parameterValue !== value) {
                setParameterValue(value as ParameterValue);

                updateRulesList({ data: { value: tSelectParameterValue(value, t) } });
            }
        },
        [parameterValue, t, updateRulesList],
    );

    const onParameterValueSelect = useCallback(
        ({ target: { value } }: SelectChangeEvent<ParameterValue>) => {
            if (value === t('custom')) {
                setIsOperatorRequired(true);
                setOperator(t('less') as ParameterOperator);

                if (conditionParameter === t('wind')) {
                    value = '5';
                }
                if (conditionParameter === t('airHumidity')) {
                    value = '60';
                }
            }

            setValue(value);
        },
        [conditionParameter, setOperator, setValue, t],
    );

    const onParameterValueChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            const numValue = Number(value);

            if (conditionParameter === t('temperature') || conditionParameter === t('waterTemperature')) {
                if (numValue > 100 || numValue < -100) return;
            }
            if (conditionParameter === t('airHumidity')) {
                if (numValue > 100 || numValue < 0) return;
            }
            if (conditionParameter === t('wind')) {
                if (numValue > 150 || numValue < 0) return;
            }
            if (conditionParameter === t('atmosphericPressure')) {
                if (numValue > 1000 || numValue < 0) return;
            }

            setValue(value);
        },
        [conditionParameter, setValue, t],
    );

    // меняем значения в зависимости от параметров
    const prevConditionParameter = useRef<ConditionParameter | ''>('');

    useEffect(() => {
        if (prevConditionParameter.current !== conditionParameter) {
            const isFirst = prevConditionParameter.current === '';
            prevConditionParameter.current = conditionParameter;

            switch (conditionParameter) {
                case t('temperature'):
                    setOperator(isFirst ? initialParameterOperator : (t('less') as ParameterOperator));
                    setValue(isFirst ? initialParameterValue : '0');
                    break;
                case t('waterTemperature'):
                    setOperator(isFirst ? initialParameterOperator : (t('less') as ParameterOperator));
                    setValue(isFirst ? initialParameterValue : '0');
                    break;
                case t('cloudiness'):
                    setOperator();
                    setValue(isFirst ? initialParameterValue : '');
                    setParameterValues(cloudCoverConditions);
                    break;
                case t('precipitation'):
                    setOperator();
                    setValue(isFirst ? initialParameterValue : '');
                    setParameterValues(rainFallConditions);
                    break;
                case t('wind'):
                    setOperator(isFirst ? initialParameterOperator : '');
                    setValue(isFirst ? initialParameterValue : '');
                    setParameterValues(windConditions);
                    break;
                case t('atmosphericPressure'):
                    setOperator(isFirst ? initialParameterOperator : (t('less') as ParameterOperator));
                    setValue(isFirst ? initialParameterValue : '760');
                    break;
                case t('airHumidity'):
                    setOperator(isFirst ? initialParameterOperator : '');
                    setValue(isFirst ? initialParameterValue : '');
                    setParameterValues(humidityConditions);
                    break;
            }
        }
    }, [
        cloudCoverConditions,
        conditionParameter,
        humidityConditions,
        initialParameterOperator,
        initialParameterValue,
        rainFallConditions,
        setOperator,
        setValue,
        t,
        windConditions,
    ]);

    useEffect(() => {
        if (data?.value?.startsWith('[')) {
            setOperator();
        }
    }, [data?.value, setOperator]);

    useEffect(() => {
        const withinConditions = (conditions: ConditionParameter[]) =>
            conditions.includes(conditionParameter as ConditionParameter);

        if (withinConditions(conditionsWithRequiredOperatorOne)) {
            setIsOperatorRequired(true);
        } else if (withinConditions(conditionsWithRequiredOperatorThree)) {
            setIsOperatorRequired(false);
        }
    }, [conditionParameter, conditionsWithRequiredOperatorOne, conditionsWithRequiredOperatorThree, setOperator, t]);

    const isConditionParameterDisabled = useCallback(
        (parameter: ConditionParameter) => {
            if (rulesList.length > 1) {
                const weatherParameter = tSelectParameter(parameter, t);

                const filteredRuleList = rulesList.filter(
                    ({ data }) =>
                        data && 'parameter' in data && (data?.parameter as WeatherParameter) === weatherParameter,
                );

                if (conditionsWithRequiredOperatorOne.includes(parameter)) {
                    return filteredRuleList.length === 2;
                }
                if (conditionsWithRequiredOperatorThree.includes(parameter)) {
                    return filteredRuleList.length === 1;
                }
                if (conditionsWithRequiredOperatorTwo.includes(parameter)) {
                    const withOperator = filteredRuleList.some(
                        ({ data }) => data && 'operator' in data && data?.operator,
                    );
                    if (withOperator) {
                        return filteredRuleList.length === 2;
                    }
                    return filteredRuleList.length === 1;
                }
            }

            return false;
        },
        [
            conditionsWithRequiredOperatorOne,
            conditionsWithRequiredOperatorThree,
            conditionsWithRequiredOperatorTwo,
            rulesList,
            t,
        ],
    );

    const filteredRuleListByParameter = getRuleListFilterByParameter(rulesList);

    const isParameterValueDisabled = useCallback(
        (value: ParameterValue) => {
            if (conditionParameter === t('wind')) {
                const withHandleInput = filteredRuleListByParameter('wind').some(
                    ({ data }, _, array) => array.length > 1 && data && 'operator' in data && data.operator,
                );
                if (withHandleInput) {
                    return windConditions.includes(value as WindParameterValue);
                }
            }

            if (conditionParameter === t('airHumidity')) {
                const withHandleInput = filteredRuleListByParameter('humidity').some(
                    ({ data }, _, array) => array.length > 1 && data && 'operator' in data && data.operator,
                );
                if (withHandleInput) {
                    return humidityConditions.includes(value as HumidityParameterValue);
                }
            }

            return false;
        },
        [conditionParameter, filteredRuleListByParameter, humidityConditions, t, windConditions],
    );

    const isParameterOperatorDisabled = useMemo(() => {
        const isDisabled = (maxValue: string, minValue: string) =>
            (parameterOperator === t('more') && parameterValue === maxValue) ||
            (parameterOperator === t('less') && parameterValue === minValue);

        if (conditionParameter === t('temperature') || conditionParameter === t('waterTemperature')) {
            return isDisabled('100', '-100');
        }

        if (conditionParameter === t('wind')) {
            return isDisabled('150', '0');
        }

        if (conditionParameter === t('atmosphericPressure')) {
            return isDisabled('1000', '0');
        }

        if (conditionParameter === t('airHumidity')) {
            return isDisabled('100', '0');
        }

        return false;
    }, [conditionParameter, parameterOperator, parameterValue, t]);

    const getPrevRowData = useCallback(
        (weatherParameter: ConditionParameter) => {
            const parameter = tSelectParameter(weatherParameter, t);

            const prevRule = filteredRuleListByParameter(parameter as WeatherParameter).filter(
                ({ id }, index, array) => array.length === 2 && index === 0 && id !== weatherRowId,
            )[0];
            const prevOperator =
                prevRule && prevRule.data && 'operator' in prevRule.data ? (prevRule.data?.operator ?? '') : '';
            const prevValue = prevRule && prevRule.data && 'value' in prevRule.data ? (prevRule.data?.value ?? '') : '';

            return { prevOperator: tOperator(prevOperator, t), prevValue: tParameterValue(prevValue, t) };
        },
        [filteredRuleListByParameter, t, weatherRowId],
    );

    const isRowValueError = useMemo(() => {
        if (isParameterOperatorDisabled) {
            return true;
        }

        if (conditionParameter && weatherConditionParameters.includes(conditionParameter)) {
            const { prevOperator, prevValue } = getPrevRowData(conditionParameter);
            const values = {
                prevOperator,
                prevValue,
                operator: parameterOperator,
                value: parameterValue,
            };
            return isOperatorAndValueMixError(values, t);
        }

        return false;
    }, [
        conditionParameter,
        getPrevRowData,
        isParameterOperatorDisabled,
        parameterOperator,
        parameterValue,
        t,
        weatherConditionParameters,
    ]);

    useEffect(() => {
        if (isRowValueError) {
            setRowIdsWithError((prevIds) => [...prevIds, weatherRowId]);
        } else {
            setRowIdsWithError((prevIds) => prevIds.filter((rowId) => rowId !== weatherRowId));
        }
    }, [isRowValueError, setRowIdsWithError, weatherRowId]);

    return {
        conditionParameter,
        onConditionParameterSelect,
        parameterOperator,
        onParameterOperatorSelect,
        parameterValue,
        parameterValues,
        onParameterValueSelect,
        onParameterValueChange,
        isOperatorRequired,
        isConditionParameterDisabled,
        isParameterValueDisabled,
        isRowValueError,

        t,
        weatherConditionParameters,
        parameterOperators,
    };
};
