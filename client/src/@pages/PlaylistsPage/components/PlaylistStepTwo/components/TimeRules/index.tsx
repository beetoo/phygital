import React from 'react';
import { format } from 'date-fns';
import { useTranslation } from 'react-i18next';

import { TimeRuleType } from '../../hooks/useScenarioSettingMenu';
import { tSelectFrequency } from '../../../../../DxSchedulePage/helpers';
import { toDate } from '../../../../../../helpers';

import css from '../../style.m.css';

type Props = {
    timeRules: TimeRuleType[];
};

const TimeRules: React.FC<Props> = ({ timeRules }: Props) => {
    const { t: tPlaylistsModal } = useTranslation('translation', { keyPrefix: 'playlistsModal' });
    const { t: tCreatePlaylist } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    return timeRules.length > 0 ? (
        <li className={css.mainRightMenuInfoRulesHeader}>
            {tPlaylistsModal('time')}:
            {timeRules.map(({ id, data }: TimeRuleType) => {
                const startDate = toDate(data.startDay, data.startTime);
                const endDate = toDate(data.endDay, data.endTime);

                const startTime = data.startTime;
                const endTime = data.endTime;
                const startDay = format(startDate, 'dd.MM.yyyy');
                const endDay = format(endDate, 'dd.MM.yyyy');
                const frequency = tSelectFrequency(data.frequency ?? '', tCreatePlaylist);

                return (
                    <ul key={id} className={css.mainRightMenuInfoRulesList}>
                        <li>
                            <span>{tPlaylistsModal('date')}:</span> {startDay}–{endDay}
                        </li>
                        <li>
                            <span>{tPlaylistsModal('repeat')}:</span> {frequency}
                        </li>
                        <li>
                            <span>{tPlaylistsModal('time').toLowerCase()}:</span> {startTime}–{endTime}
                        </li>
                    </ul>
                );
            })}
        </li>
    ) : null;
};

export default TimeRules;
