import React, { memo, useEffect, useRef, useState } from 'react';
import { Droppable } from '@hello-pangea/dnd';
import { List } from '@phosphor-icons/react';
import { clsx } from 'clsx';

import TimeLineHorizontal from '../TimeLineHorizontal';
import TimeLineVertical from '../TimeLineVertical';
import { Tooltip } from '../../../../../../ui-kit';
import { useStepTwoPlaylistItems } from '../../hooks/useStepTwoPlaylistItems';
import { useStepTwoPlayer } from '../../hooks/useStepTwoPlayer';

import css from '../../style.m.css';

const StepTwoPlaylistItemsList: React.FC = () => {
    const {
        isImageType,
        isVideoType,
        isWebType,
        playlistItemSrc,
        isPlaylistItemsExist,
        isSelectedPlaylistItemsExist,
        selectUnselectAllPlaylistItems,
        scenariosList,
        isScenariosFilterOpen,
        openCloseScenariosFilter,
        isScenarioShown,
        isVerticalTimeLine,
        setVerticalTimeLineType,
        setHorizontalTimeLineType,
        selectUnselectScenario,
        isAllScenariosShown,
        selectUnselectAllScenarios,
        isContentShown,
        shownPlaylistItemsNumber,
        setVirtualScreenArea,
        clickOnViewArea,
        virtualResolution,
        isScreensSelected,
        virtualScreenWidth,
        virtualScreenHeight,
    } = useStepTwoPlaylistItems();

    const {
        videoRef,
        playMode,
        onPlayClick,
        onPauseClick,
        onBackClick,
        currentPlaylistTime,
        fullPlaylistTime,
        isPlayerDisabled,
        isBackButtonDisabled,
        t,
    } = useStepTwoPlayer();

    const resizeElementRef = useRef<HTMLDivElement | null>(null);
    const resizeElementButtonRef = useRef<SVGSVGElement | null>(null);

    const [height, setHeight] = useState(100);

    useEffect(() => {
        let mPos: number;
        const resize = (e: any) => {
            const dx = mPos - e.y;
            mPos = e.y;

            resizeElementRef?.current?.style.setProperty(
                'height',
                parseInt(getComputedStyle(resizeElementRef.current, '').height) + dx + 'px',
            );

            if (resizeElementRef?.current?.clientHeight) {
                setHeight(resizeElementRef.current.clientHeight - 76);
            }
        };

        if (!isVerticalTimeLine) {
            document.removeEventListener('mousemove', resize, false);
            return resizeElementRef?.current?.style.setProperty('height', '176px');
        }

        resizeElementButtonRef?.current?.addEventListener(
            'mousedown',
            (e) => {
                mPos = e.y;
                document.addEventListener('mousemove', resize, false);
            },
            false,
        );

        document.addEventListener(
            'mouseup',
            function () {
                document.removeEventListener('mousemove', resize, false);
            },
            false,
        );
    }, [isVerticalTimeLine]);

    return (
        <div className={css.wrapperSettingContent}>
            <div className={clsx(css.wrapperCanvas, css.vertical)}>
                <div
                    className={css.fieldBtnScreenResolution}
                    style={{
                        width: virtualScreenWidth,
                        margin: '0 auto 6px',
                    }}
                >
                    <Tooltip title={t('clickChange')} placement="top">
                        <button
                            className={css.btnScreenResolution}
                            onClick={(e) => {
                                e.stopPropagation();
                                clickOnViewArea();
                            }}
                        >
                            {t('screenResolution')}

                            {virtualResolution}
                        </button>
                    </Tooltip>
                </div>

                <Droppable droppableId="virtualScreen">
                    {(provided) => (
                        <div ref={provided.innerRef} {...provided.droppableProps}>
                            <div ref={setVirtualScreenArea}>
                                <div
                                    className={css.wrapperCanvas2}
                                    style={{
                                        width: virtualScreenWidth,
                                        height: virtualScreenHeight,
                                        visibility: virtualScreenWidth && virtualScreenHeight ? undefined : 'hidden',
                                    }}
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        clickOnViewArea();
                                    }}
                                >
                                    {isContentShown ? (
                                        <>
                                            {isImageType ? (
                                                <img src={playlistItemSrc} height={virtualScreenHeight} alt="" />
                                            ) : isVideoType ? (
                                                <video
                                                    ref={videoRef}
                                                    src={playlistItemSrc}
                                                    height={virtualScreenHeight}
                                                />
                                            ) : isWebType ? (
                                                <iframe src={playlistItemSrc} height={virtualScreenHeight} />
                                            ) : null}
                                        </>
                                    ) : (
                                        <div className={css.wrapperCanvas3Text}>
                                            {isScreensSelected ? (
                                                <p className={clsx(css.canvasItem, css.canvasItem1)}>
                                                    {t('textNineteen')}
                                                    <br />
                                                    {t('textTwenty')}
                                                </p>
                                            ) : (
                                                <p className={clsx(css.canvasItem, css.canvasItem1)}>
                                                    {t('screenResolution')}
                                                    {virtualResolution}
                                                </p>
                                            )}
                                            {isScreensSelected ? (
                                                <p className={css.canvasItem}>
                                                    {t('screenResolution')}
                                                    {virtualResolution}
                                                </p>
                                            ) : (
                                                <p className={css.canvasItem}>
                                                    {t('textFour')}
                                                    {t('textFive')}
                                                </p>
                                            )}
                                        </div>
                                    )}

                                    <div className={css.wrapperCanvasLayers}>
                                        <span className={css.wrapperCanvasLayersItem}>{shownPlaylistItemsNumber}</span>
                                    </div>
                                </div>
                            </div>
                            <div style={{ display: 'none' }}>{provided.placeholder}</div>
                        </div>
                    )}
                </Droppable>

                <div className={css.wrapperPlayPauseTime}>
                    <div className={css.wrapperBtnPlayPause}>
                        <Tooltip title={t('backToBeginning')} placement="top">
                            <button
                                className={clsx(css.btnPlayBack, isBackButtonDisabled && css.disabled)}
                                onClick={isBackButtonDisabled ? undefined : onBackClick}
                            />
                        </Tooltip>
                        {playMode === 'pause' && (
                            <Tooltip title={t('play')} placement="top">
                                <button
                                    className={clsx(css.btnPlay, isPlayerDisabled && css.disabled)}
                                    onClick={isPlayerDisabled ? undefined : onPlayClick}
                                />
                            </Tooltip>
                        )}
                        {playMode === 'play' && (
                            <Tooltip title={t('pause')} placement="top">
                                <button
                                    className={css.btnPause}
                                    onClick={isPlayerDisabled ? undefined : onPauseClick}
                                />
                            </Tooltip>
                        )}
                    </div>
                    <div className={css.wrapperTime}>
                        <div className={clsx(css.wrapperTimeItem, isPlayerDisabled && css.wrapperTimeItemDisabled)}>
                            <p className={css.startTimeItem}>{currentPlaylistTime}</p>&ensp;/&ensp;
                            <p className={css.endTimeItem}>{fullPlaylistTime}</p>
                        </div>
                    </div>
                </div>
            </div>

            <div ref={resizeElementRef} className={isVerticalTimeLine ? css.verticalWrapper : css.horizontalWrapper}>
                {isVerticalTimeLine && (
                    <div
                        style={{
                            position: 'absolute',
                            left: (resizeElementRef?.current?.clientWidth ?? 0) / 2,
                        }}
                    >
                        <List ref={resizeElementButtonRef} size={24} style={{ cursor: 'pointer' }} />
                    </div>
                )}
                <div className={css.wrapperBtn}>
                    <div
                        style={{ display: 'flex', flexDirection: 'column', gap: '4px' }}
                        onMouseOver={(event) => event.stopPropagation()}
                    >
                        <div>
                            <button
                                className={clsx(css.btnSelectAll, isPlaylistItemsExist && css.enabled)}
                                onClick={(e) => {
                                    e.stopPropagation();
                                    selectUnselectAllPlaylistItems();
                                }}
                            >
                                {isSelectedPlaylistItemsExist ? `${t('deselect')}` : `${t('selectAll')}`}
                            </button>
                        </div>
                        <div className={css.wrapperTrigger}>
                            <Tooltip
                                title={
                                    scenariosList.length === 0
                                        ? `${t('triggerScriptsNoCreated')}`
                                        : `${t('btnShowTriggerScripts')}`
                                }
                                placement="top"
                            >
                                <button
                                    className={clsx(css.btnShowTrigger, scenariosList.length > 0 && css.enabled)}
                                    onClick={(e) => {
                                        if (scenariosList.length > 0) {
                                            e.stopPropagation();
                                            openCloseScenariosFilter();
                                        }
                                    }}
                                >
                                    {isAllScenariosShown
                                        ? `${t('btnHideTriggerScripts')}`
                                        : `${t('btnShowTriggerScripts')}`}
                                </button>
                            </Tooltip>

                            {isScenariosFilterOpen && (
                                <div className={css.wrapperTriggerSelect}>
                                    <div>
                                        {scenariosList.map(({ id, name }) => (
                                            <div key={id} className={css.wrapperTriggerSelectList}>
                                                <p className={css.wrapperTriggerSelectItem}>{name}</p>
                                                <span
                                                    className={
                                                        isScenarioShown(id)
                                                            ? css.checkboxFreeSlotOn
                                                            : css.checkboxFreeSlotOff
                                                    }
                                                    onClick={selectUnselectScenario(id)}
                                                />
                                            </div>
                                        ))}
                                    </div>
                                    <div>
                                        <div
                                            className={clsx(
                                                css.wrapperTriggerSelectList,
                                                css.wrapperTriggerSelectListShowAll,
                                            )}
                                        >
                                            <p className={css.wrapperTriggerSelectItem}>{t('showAllScripts')}</p>
                                            <span
                                                className={
                                                    isAllScenariosShown
                                                        ? css.checkboxFreeSlotOn
                                                        : css.checkboxFreeSlotOff
                                                }
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                    selectUnselectAllScenarios();
                                                }}
                                            />
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                    <div className={css.contentViewTypeWrapper}>
                        <h3>{t('contentDisplayType')}</h3>

                        <div className={css.contentViewTypeButtonsWrapper}>
                            <div className={css.contentViewTypeButton} onClick={setHorizontalTimeLineType}>
                                <input type="radio" checked={!isVerticalTimeLine} readOnly />
                                <button className={css.btnSelectAll}>{t('horizontally')}</button>
                            </div>
                            <div className={css.contentViewTypeButton} onClick={setVerticalTimeLineType}>
                                <input type="radio" checked={isVerticalTimeLine} readOnly />
                                <button className={css.btnSelectAll}>{t('vertical')}</button>
                            </div>
                        </div>
                    </div>
                </div>

                {isVerticalTimeLine ? <TimeLineVertical height={height} /> : <TimeLineHorizontal />}
            </div>
        </div>
    );
};

export default memo(StepTwoPlaylistItemsList);
