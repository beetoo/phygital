import React, { ComponentProps } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { Tour } from '../../../../../../components/Tour';
import { TourBody } from '../../../../../../components/Tour/components/TourBody';
import { TourHeader } from '../../../../../../components/Tour/components/TourHeader';
import { selectContentItems, selectFolders } from '../../../../../ContentPage/selectors';

import { selectPlaylistDto } from '../../../../selectors';
import foldersImg from './images/folders.png';
import upload from './images/upload.png';
import play from './images/play.png';
import clock from './images/clock.png';
import trash from './images/trash.png';

type Steps = ComponentProps<typeof Tour>['steps'];

type Props = {
    contentWithoutFolder: HTMLElement | null;
    uploadContentBtn: HTMLElement | null;
    emptyPlaylistItem: HTMLElement | null;
    firstPlaylistItem: HTMLElement | null;
    duration: HTMLElement | null;
};

export const PlaylistStepTwoTour: React.FC<Props> = ({
    contentWithoutFolder,
    uploadContentBtn,
    firstPlaylistItem,
    emptyPlaylistItem,
    duration,
}: Props) => {
    const { t } = useTranslation('translation', { keyPrefix: 'learningHints' });

    const content = useSelector(selectContentItems);
    const folders = useSelector(selectFolders);
    const { playlistDtoItems } = useSelector(selectPlaylistDto);

    const steps = React.useMemo<Steps>(
        () => [
            contentWithoutFolder && folders.length === 0
                ? {
                      name: 'firstFolder',
                      target: contentWithoutFolder,
                      placement: 'left-start',
                      title: (
                          <TourHeader>
                              <img src={foldersImg} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textEighteen')}
                                  {/* Папки для контента */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  {t('textNineTeen')}
                                  {/* Создайте папку и загрузите в неё изображения и видео. Группировка контента по папкам
                                  помогает быстрее находить нужный контент и поддерживать порядок. */}
                              </p>
                              <p>
                                  {t('textTwenty')}
                                  {/* Например, можно создать папку для отдельного экрана или локации. */}
                              </p>
                          </TourBody>
                      ),
                  }
                : null,
            uploadContentBtn && content.length === 0
                ? {
                      name: 'uploadContentBtn',
                      target: uploadContentBtn,
                      placement: 'left',
                      title: (
                          <TourHeader>
                              <img src={upload} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textTwentyOne')}
                                  {/* Загрузите контент */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  {t('textTwentyFour')}
                                  {/* Чтобы создать плейлист необходим контент. */}
                              </p>
                              <p>
                                  {t('textTwentyThree')}
                                  {/* Нажмите, чтобы загрузить видео и изображения. */}
                              </p>
                              <p>
                                  {t('textTwentyFive')}
                                  {/* Контент можно добавлять и редактировать в разделе «Контент» */}
                              </p>
                          </TourBody>
                      ),
                  }
                : null,

            emptyPlaylistItem && playlistDtoItems.length === 0
                ? {
                      name: 'addPlaylistItem',
                      target: emptyPlaylistItem,
                      placement: 'right',
                      title: (
                          <TourHeader>
                              <img src={play} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textTwentyNine')}
                                  {/* Добавьте контент */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  {t('textThirty')}
                                  {/* Перетащите контент из правой области в слоты в нужной последовательности.{' '} */}
                              </p>
                              <p>
                                  {t('textThirtyOne')}
                                  {/* Плейлист будет проигрываться циклично. Это значит, что когда плейлист закончится, его
                                  проигрывание начнётся заново. И так по кругу. */}
                              </p>
                              <p>
                                  {t('textThirtyTwo')}
                                  {/* Если добавить одно изображение — оно будет показываться на экране беспрерывно. */}
                              </p>
                          </TourBody>
                      ),
                  }
                : null,
            duration && playlistDtoItems.length === 1
                ? {
                      name: 'duration',
                      target: duration,
                      placement: 'right-start',
                      title: (
                          <TourHeader>
                              <img src={clock} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textThirtyThree')}
                                  {/* Установите длительность */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              {t('textThirtyFour')}
                              {/* По умолчанию длительность показа изображения в плейлисте установлена на 20 секунд. Укажите
                              другую длительность, если необходимо. */}
                          </TourBody>
                      ),
                  }
                : null,

            firstPlaylistItem && playlistDtoItems.length === 1
                ? {
                      name: 'deletePlaylistItem',
                      target: firstPlaylistItem,
                      placement: 'right',
                      title: (
                          <TourHeader>
                              <img src={trash} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textThirtyFive')}
                                  {/* Удаление контента */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              {t('textThirtySix')}
                              {/* Для удаления наведите мышкой на нужный контент и нажмите на иконку «Удалить». */}
                          </TourBody>
                      ),
                  }
                : null,
        ],
        [
            content.length,
            contentWithoutFolder,
            duration,
            emptyPlaylistItem,
            firstPlaylistItem,
            folders.length,
            playlistDtoItems.length,
            uploadContentBtn,
            t,
        ],
    );

    return <Tour steps={steps} name="PlaylistStepTwo" />;
};
