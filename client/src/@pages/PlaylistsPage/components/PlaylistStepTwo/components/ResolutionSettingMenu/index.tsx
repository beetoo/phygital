import React, { memo } from 'react';

import { MenuItem, Select } from '../../../../../../ui-kit';
import { useResolutionSettingMenu } from '../../hooks/useResolutionSettingMenu';

import css from '../../style.m.css';

const ResolutionSettingMenu: React.FC = () => {
    const {
        resolutions,
        screenResolutionValue,
        onScreenResolutionSelect,
        hasCustomResolutionInput,
        customResolutionWidth,
        customResolutionHeight,
        onCustomResolutionHeightChange,
        onCustomResolutionWidthChange,

        orientations,
        screenOrientationValue,
        onScreenOrientationSelect,
        isSelectDisabled,
        isOrientationSelectDisabled,
        t,
    } = useResolutionSettingMenu();

    return (
        <div className={css.wrapMainRightMenuSetup123}>
            <h2>{t('screenSettings')}</h2>

            <div style={{ marginBottom: '5px' }}>
                <Select
                    label={t('resolution') as string}
                    style={{ height: '42px', marginLeft: '12px' }}
                    width={304}
                    value={screenResolutionValue}
                    onChange={onScreenResolutionSelect}
                    disabled={isSelectDisabled}
                >
                    {[...resolutions, t('resolutionCustom')].map((resolution) => (
                        <MenuItem key={resolution} value={resolution}>
                            {resolution}
                        </MenuItem>
                    ))}
                </Select>
            </div>

            {hasCustomResolutionInput && (
                <div className={css.customResolutionInputContainer}>
                    <div className={css.fieldWidth}>
                        <input
                            className={css.customResolutionInput}
                            placeholder={t('width') as string}
                            value={customResolutionWidth}
                            onChange={onCustomResolutionWidthChange}
                            maxLength={125}
                        />
                        <div className={css.fieldWidthItem}>
                            <span className={css.customResolutionInputItem}>px</span>
                        </div>
                    </div>
                    <div className={css.fieldWidth}>
                        <input
                            className={css.customResolutionInput}
                            placeholder={t('height') as string}
                            value={customResolutionHeight}
                            onChange={onCustomResolutionHeightChange}
                            maxLength={125}
                        />
                        <div className={css.fieldWidthItem}>
                            <span className={css.customResolutionInputItem}>px</span>
                        </div>
                    </div>
                </div>
            )}

            <div style={{ marginBottom: '4px' }}>
                <Select
                    label={t('orientation') as string}
                    style={{ marginLeft: '12px' }}
                    width={304}
                    value={screenOrientationValue}
                    onChange={onScreenOrientationSelect}
                    disabled={isOrientationSelectDisabled}
                    MenuProps={{
                        PaperProps: {
                            style: {
                                marginLeft: '-2px',
                            },
                        },
                    }}
                >
                    {orientations.map((orientation) => (
                        <MenuItem key={orientation} value={orientation}>
                            {orientation}
                        </MenuItem>
                    ))}
                </Select>
            </div>
        </div>
    );
};

export default memo(ResolutionSettingMenu);
