import React from 'react';
import { Draggable } from '@hello-pangea/dnd';
import { Globe } from '@phosphor-icons/react';
import { clsx } from 'clsx';

import ImgOrVideo from '../../../../../../components/ImgOrVideo';
import { useTimeLinePlaylistItem } from '../../hooks/useTimeLinePlaylistItem';
import { SelectButton, Tooltip } from '../../../../../../ui-kit';
import { formatSecondsToTimeString } from '../../../../../../../../common/helpers';
import { CONTENT_TYPE, PlaylistItemType } from '../../../../../../../../common/types';

import css from './style.m.css';

type Props = {
    index: number;
    playlistItem: PlaylistItemType;
};

const TimeLinePlaylistItemVertical: React.FC<Props> = ({ playlistItem: { id, content, delay, scenario }, index }) => {
    const {
        setActivePlaylistItem,
        setMenuMode,
        selectPlaylistItem,
        unselectPlaylistItem,
        isPlaylistItemActive,
        isPlaylistItemSelected,
        playlistItemSeconds,
        closeScenariosList,
        t,
    } = useTimeLinePlaylistItem({ delay, duration: content?.duration ?? 0, type: content?.type });

    return (
        <Draggable draggableId={id} index={index}>
            {(provided) => (
                <div
                    className={css.containerVerticalTrigger}
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                >
                    <div
                        className={clsx(css.wrapperTriggerScriptBlock, scenario && css.withTrigger)}
                        onClick={(e) => {
                            e.stopPropagation();
                            closeScenariosList();
                            setMenuMode('content_setting_menu');
                            setActivePlaylistItem(id);
                        }}
                    >
                        <span className={css.timeLineSliderEnd} />

                        <div className={css.wrapperTriggerScript}>
                            <Tooltip
                                title={`${content?.originFilename} (${formatSecondsToTimeString(playlistItemSeconds)})`}
                                placement="top"
                            >
                                <div
                                    className={clsx(
                                        css.wrapperTriggerComponentItem,
                                        (isPlaylistItemActive(id) || isPlaylistItemSelected(id)) && css.active,
                                    )}
                                >
                                    <div className={css.wrapperTriggerComponentImg}>
                                        {content?.type === CONTENT_TYPE.IMAGE ? (
                                            <ImgOrVideo
                                                classNameForImg={clsx(playlistItemSeconds === 5 && css.fiveSecondsSize)}
                                                src={content?.src}
                                            />
                                        ) : content?.type === CONTENT_TYPE.VIDEO ? (
                                            <ImgOrVideo
                                                classNameForVideo={clsx(
                                                    playlistItemSeconds === 5 && css.fiveSecondsSize,
                                                )}
                                                src={content?.src}
                                            />
                                        ) : content?.type === CONTENT_TYPE.WEB ? (
                                            <Globe size={30} weight="thin" />
                                        ) : null}
                                    </div>

                                    <div className={css.wrapperTriggerComponentText}>
                                        <p className={css.triggerComponentItem}>{content?.originFilename}</p>
                                    </div>

                                    <SelectButton
                                        className={css.iconContentSelect}
                                        active={isPlaylistItemSelected(id)}
                                        onClick={(e) => {
                                            e.stopPropagation();
                                            closeScenariosList();
                                            isPlaylistItemSelected(id)
                                                ? unselectPlaylistItem(id)
                                                : selectPlaylistItem(id);
                                        }}
                                    />
                                </div>
                            </Tooltip>
                        </div>
                    </div>
                    <div>
                        <h3>{t('contentTime')}</h3>
                        <p className={css.triggerScriptTimeItem}>{formatSecondsToTimeString(playlistItemSeconds)}</p>
                    </div>
                </div>
            )}
        </Draggable>
    );
};

export default TimeLinePlaylistItemVertical;
