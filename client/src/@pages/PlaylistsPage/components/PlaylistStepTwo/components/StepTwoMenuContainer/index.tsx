import React, { memo } from 'react';
import { clsx } from 'clsx';

import ContentMenu from '../ContentMenu';
import ContentSettingMenu from '../ContentSettingMenu';
import MultiplyContentSettingMenu from '../MultiplyContentSettingMenu';
import ScenarioSettingMenu from '../ScenarioSettingMenu';
import ScenarioContentSettingMenu from '../ScenarioContentSettingMenu';

import { useStepTwoMenuContainer } from '../../../../hooks/useStepTwoMenuContainer';
import { useUpdatePlaylistButtons } from '../../../../hooks/useUpdatePlaylistButtons';
import { StepTwoMenuMode } from '../../../../types';

import css from '../../style.m.css';

const StepTwoMenuContainer: React.FC = () => {
    const {
        menuMode,
        isMultiplyContentSettingMenuEnabled,
        isNextStepButtonDisabled,
        moveToStepThree,
        addContentItemsToPlaylist,
        cancelAddContentItemToScenario,
        isAddContentButtonsShown,
        isAddContentButtonDisabled,
        isAddToScenarioMode,
        t,
    } = useStepTwoMenuContainer();

    const { updatePlaylist, isCreatePlaylistStep, isSubmitButtonDisabled } = useUpdatePlaylistButtons();

    return (
        <div className={css.mainRightMenuSetup}>
            {isMultiplyContentSettingMenuEnabled ? (
                <MultiplyContentSettingMenu />
            ) : (
                <>
                    {(['content_menu', 'content_menu_to_scenario'] as StepTwoMenuMode[]).includes(menuMode) && (
                        <ContentMenu />
                    )}
                    {menuMode === 'content_setting_menu' && <ContentSettingMenu />}
                    {menuMode === 'scenario_content_setting_menu' && <ScenarioContentSettingMenu />}
                    {menuMode === 'scenario_setting_menu' && <ScenarioSettingMenu />}
                </>
            )}
            <div
                className={clsx(
                    css.btnStartScreensField,
                    (isAddContentButtonsShown
                        ? isAddContentButtonDisabled
                        : isSubmitButtonDisabled || isNextStepButtonDisabled) && css.btnStartScreensField2,
                )}
            >
                {isAddContentButtonsShown ? (
                    <>
                        {!isAddContentButtonDisabled && (
                            <button className={css.btnAdd} onClick={addContentItemsToPlaylist}>
                                {isAddToScenarioMode ? `${t('btnAddToScrip')}` : `${t('btnAddToPlaylist')}`}
                            </button>
                        )}
                        <button className={css.btnCancel} onClick={cancelAddContentItemToScenario}>
                            {t('btnCancel')}
                        </button>
                    </>
                ) : (
                    <>
                        {!isSubmitButtonDisabled && (
                            <button className={css.btnSaveChangeExit} onClick={updatePlaylist}>
                                {isCreatePlaylistStep ? `${t('btnSaveDraft')}` : `${t('btnSaveChangesExit')}`}
                            </button>
                        )}
                        {!isNextStepButtonDisabled && (
                            <button className={css.btnNextStep} onClick={moveToStepThree}>
                                <img alt="" />
                                {t('btnSetUpSchedule')}
                            </button>
                        )}
                    </>
                )}
            </div>
        </div>
    );
};

export default memo(StepTwoMenuContainer);
