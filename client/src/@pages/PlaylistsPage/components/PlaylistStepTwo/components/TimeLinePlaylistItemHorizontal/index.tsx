import React from 'react';
import { Draggable } from '@hello-pangea/dnd';
import { Globe } from '@phosphor-icons/react';
import { clsx } from 'clsx';

import ThreeDots from '../ThreeDots';
import ImgOrVideo from '../../../../../../components/ImgOrVideo';
import { CONTENT_TYPE, PlaylistItemType } from '../../../../../../../../common/types';
import { useTimeLinePlaylistItem } from '../../hooks/useTimeLinePlaylistItem';
import { formatSecondsToTimeString } from '../../../../../../../../common/helpers';
import { SelectButton, Tooltip } from '../../../../../../ui-kit';

import css from '../../style.m.css';

type Props = {
    index: number;
    playlistItem: PlaylistItemType;
};

const TimeLinePlaylistItem: React.FC<Props> = ({ playlistItem: { id, content, delay, scenario }, index }) => {
    const {
        setActivePlaylistItem,
        setMenuMode,
        selectPlaylistItem,
        unselectPlaylistItem,
        isPlaylistItemActive,
        isPlaylistItemSelected,
        playlistItemSeconds,
        closeScenariosList,
    } = useTimeLinePlaylistItem({ delay, duration: content?.duration ?? 0, type: content?.type });

    return (
        <Draggable draggableId={id} index={index}>
            {(provided) => (
                <div
                    className={clsx(css.wrapperTriggerScriptBlock, scenario && css.withTrigger)}
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    onClick={(e) => {
                        e.stopPropagation();
                        closeScenariosList();
                        setMenuMode('content_setting_menu');
                        setActivePlaylistItem(id);
                    }}
                >
                    <span className={css.timeLineSliderEnd} />

                    <div className={css.wrapperTriggerScript}>
                        <Tooltip
                            title={`${content?.originFilename} (${formatSecondsToTimeString(playlistItemSeconds)})`}
                            placement="top"
                        >
                            <div
                                className={clsx(
                                    css.wrapperTriggerComponentItem,
                                    (isPlaylistItemActive(id) || isPlaylistItemSelected(id)) && css.active,
                                )}
                                style={{ width: playlistItemSeconds * 10 || 0 }}
                            >
                                {playlistItemSeconds > 2 && (
                                    <div className={css.wrapperTriggerComponentImg}>
                                        {content?.type === CONTENT_TYPE.IMAGE ? (
                                            <ImgOrVideo
                                                classNameForImg={clsx(playlistItemSeconds === 5 && css.fiveSecondsSize)}
                                                src={content?.src}
                                            />
                                        ) : content?.type === CONTENT_TYPE.VIDEO ? (
                                            <ImgOrVideo
                                                classNameForVideo={clsx(
                                                    playlistItemSeconds === 5 && css.fiveSecondsSize,
                                                )}
                                                src={content?.src}
                                            />
                                        ) : content?.type === CONTENT_TYPE.WEB ? (
                                            <Globe size={30} weight="thin" />
                                        ) : null}
                                    </div>
                                )}

                                {playlistItemSeconds > 7 && (
                                    <div
                                        className={css.wrapperTriggerComponentText}
                                        style={{ width: playlistItemSeconds * 10 - 74 || 0 }}
                                    >
                                        <p className={css.triggerComponentItem}>{content?.originFilename}</p>
                                    </div>
                                )}

                                {playlistItemSeconds < 7 ? (
                                    <ThreeDots
                                        playlistItemSeconds={playlistItemSeconds}
                                        isPlaylistItemSelected={isPlaylistItemSelected(id)}
                                        selectPlaylistItem={() => selectPlaylistItem(id)}
                                        unselectPlaylistItem={() => unselectPlaylistItem(id)}
                                    />
                                ) : (
                                    <SelectButton
                                        className={css.iconContentSelect}
                                        active={isPlaylistItemSelected(id)}
                                        onClick={(e) => {
                                            e.stopPropagation();
                                            closeScenariosList();
                                            isPlaylistItemSelected(id)
                                                ? unselectPlaylistItem(id)
                                                : selectPlaylistItem(id);
                                        }}
                                    />
                                )}
                            </div>
                        </Tooltip>
                    </div>
                </div>
            )}
        </Draggable>
    );
};

export default TimeLinePlaylistItem;
