import React from 'react';
import { Droppable } from '@hello-pangea/dnd';

import TimeLineScenarioVertical from '../TimeLineScenarioVertical';
import TimeLinePlaylistItemVertical from '../TimeLinePlaylistItemVertical';
import { useTimeLineVertical } from '../../hooks/useTimeLineVertical';
import { useTimeLineRef } from '../../hooks/useTimeLIneRef';

import css from './style.m.css';

type Props = {
    height?: number;
};

const TimeLineVertical: React.FC<Props> = ({ height }) => {
    const { playlistDtoItems, playlistDtoItemsWithScenario, t } = useTimeLineVertical();

    const { addTimeLineRef, scrollToTop, scrollToBottom } = useTimeLineRef();

    return (
        <Droppable droppableId="playlistItems" direction="vertical">
            {(provided) => (
                <div ref={provided.innerRef} {...provided.droppableProps}>
                    <div style={{ height: '2px' }} onMouseOver={scrollToTop} />
                    <div>
                        {playlistDtoItems.length > 0 ? (
                            <div ref={addTimeLineRef} className={css.vertical} style={{ height }}>
                                {playlistDtoItemsWithScenario.map((item, index) =>
                                    'playlistItems' in item ? (
                                        <TimeLineScenarioVertical
                                            key={item.scenario.id}
                                            index={index}
                                            scenario={item.scenario}
                                            playlistItems={item.playlistItems}
                                        />
                                    ) : (
                                        <TimeLinePlaylistItemVertical key={item.id} index={index} playlistItem={item} />
                                    ),
                                )}
                            </div>
                        ) : (
                            <div className={css.wrapperTimeLineEmpty}>
                                <div className={css.wrapperTimeLineItem}>
                                    <p className={css.timeLineItem}>{t('textSix')}</p>
                                    <p className={css.timeLineItem}>{t('textSeven')}</p>
                                </div>
                            </div>
                        )}
                        <div style={{ display: 'none' }}>{provided.placeholder}</div>
                    </div>
                    <div style={{ height: '2px' }} onMouseOver={scrollToBottom} />
                </div>
            )}
        </Droppable>
    );
};

export default TimeLineVertical;
