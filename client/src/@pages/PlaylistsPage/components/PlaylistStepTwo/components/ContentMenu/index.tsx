import React, { memo } from 'react';
import { Draggable, Droppable } from '@hello-pangea/dnd';
import { clsx } from 'clsx';

import ImgOrVideo from '../../../../../../components/ImgOrVideo';
import iconFolder from '../../../../../../assets/iconFolder.png';
import { useContentMenu } from '../../hooks/useContentMenu';
import { Tooltip } from '../../../../../../ui-kit';
import { CONTENT_TYPE } from '../../../../../../../../common/types';

import css from '../../style.m.css';

const ContentMenu: React.FC = () => {
    const {
        activeFolderId,
        activeContentItems,
        openAddWebContentToPlaylistModal,
        openDeleteContentItemAlert,
        showContentViewModal,
        folders,
        onFolderClick,
        isContentItemSelected,
        selectContentItem,
        unselectContentItem,
        isDeleteCheckboxShown,
        t,
    } = useContentMenu();

    return (
        <div className={css.wrapMainRightMenuSetup}>
            <div className={css.wrapHeaderButton}>
                <h2>{t('content')}</h2>
                <button onClick={openAddWebContentToPlaylistModal} className={css.btnDownloadContent}>
                    {t('addWebContent')}
                </button>
            </div>
            <div className={css.wrapFieldRightMenuSetup}>
                <ul className={css.fieldRightMenuSetup}>
                    <Tooltip title={t('contentWithoutFolder')} placement="top">
                        <li className={clsx(activeFolderId === '' && css.active)} onClick={onFolderClick('')}>
                            <img src={iconFolder} width="26" height="20.8" alt="" />
                            <a>{t('contentWithoutFolder')}</a>
                        </li>
                    </Tooltip>

                    {folders.length > 0 &&
                        folders.map(({ id, name }) => (
                            <Tooltip key={id} title={name} placement="top">
                                <li className={clsx(activeFolderId === id && css.active)} onClick={onFolderClick(id)}>
                                    <img src={iconFolder} width="26" height="20.8" alt="" />
                                    <a>{name}</a>
                                </li>
                            </Tooltip>
                        ))}
                </ul>
            </div>
            <div className={css.mainRightMenuSetupAllList}>
                <Droppable droppableId="content" isDropDisabled={true}>
                    {(provided) => (
                        <ul ref={provided.innerRef} className={css.mainRightMenuSetupList}>
                            {activeContentItems.map(({ id, originFilename, type, src, dimensions }, index) => (
                                <Draggable key={id} draggableId={id} index={index}>
                                    {(provided) => (
                                        <div
                                            className={clsx(css.hoverEffectBtn, css.tooltip)}
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            onClick={(e) => e.stopPropagation()}
                                        >
                                            {isDeleteCheckboxShown(id) && (
                                                <a
                                                    className={css.overlayTrash}
                                                    onClick={openDeleteContentItemAlert(id)}
                                                />
                                            )}
                                            {dimensions && (
                                                <>
                                                    <a className={css.overlayScreen} />
                                                    <a className={css.overlayScreenResolution}>
                                                        {`${dimensions?.width}×${dimensions?.height}`}
                                                    </a>
                                                </>
                                            )}

                                            <a
                                                onClick={
                                                    isContentItemSelected(id)
                                                        ? unselectContentItem(id)
                                                        : selectContentItem(id)
                                                }
                                                className={
                                                    isContentItemSelected(id)
                                                        ? css.overlayCheckCircleActive
                                                        : css.overlayCheckCircle
                                                }
                                            />
                                            <li
                                                className={css.mainRightMenuSetupItem}
                                                onDoubleClick={showContentViewModal(id)}
                                            >
                                                {type === CONTENT_TYPE.VIDEO ? (
                                                    <div className={css.wrapperVideoCart}>
                                                        <ImgOrVideo
                                                            src={src}
                                                            width="138"
                                                            height="138"
                                                            title={originFilename}
                                                        />
                                                    </div>
                                                ) : (
                                                    <ImgOrVideo
                                                        classNameForImg={css.contentImage}
                                                        src={src}
                                                        width="138"
                                                        height="138"
                                                        title={originFilename}
                                                    />
                                                )}
                                                <div className={css.contentPlaylistImagesName}>
                                                    <p>{originFilename}</p>
                                                </div>
                                                <div className={css.blackoutOverlay} />
                                            </li>
                                        </div>
                                    )}
                                </Draggable>
                            ))}
                            {provided.placeholder}
                        </ul>
                    )}
                </Droppable>
            </div>
        </div>
    );
};

export default memo(ContentMenu);
