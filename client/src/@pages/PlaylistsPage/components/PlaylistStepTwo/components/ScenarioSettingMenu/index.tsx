import React, { memo } from 'react';
import { clsx } from 'clsx';

import WeatherRules from '../WeatherRules';
import TimeRules from '../TimeRules';
import { useScenarioSettingMenu } from '../../hooks/useScenarioSettingMenu';

import css from '../../style.m.css';

const ScenarioSettingMenu: React.FC = () => {
    const {
        scenarioName,
        scenarioDescription,
        scenarioIndex,

        onScenarioPlaceIndexChange,
        addContentToScenario,
        showTriggerContentSettingModal,
        deleteScenario,

        isRulesShown,
        showRules,
        isUsablePlaylistItemsShown,
        showUsablePlaylistItems,

        weatherRules,
        timeRules,
        usablePlaylistItems,
        goToUsablePlaylistItem,
        t,
    } = useScenarioSettingMenu();

    return (
        <div className={css.wrapMainRightMenuSetup124}>
            <h2>{t('triggerScript')}</h2>
            <div className={clsx(css.wrapperTotalInfo, css.wrapperTotalInfoOne)}>
                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItemOne)}>
                    {t('name')}
                    <span>{scenarioName}</span>
                </p>
                {!!scenarioDescription && (
                    <p className={css.mainRightMenuInfoItem}>
                        {t('description')}
                        <span>{scenarioDescription}</span>
                    </p>
                )}
            </div>

            <div className={css.wrapMainRightMenuInfoRules}>
                <h3>{t('rules')}</h3>
                <span className={isRulesShown ? css.iconArrowInfoUp : css.iconArrowInfoDown} onClick={showRules} />
                {isRulesShown && (
                    <div className={css.mainRightMenuInfoRules}>
                        <ul>
                            <WeatherRules weatherRules={weatherRules} />
                            <TimeRules timeRules={timeRules} />
                        </ul>
                    </div>
                )}
            </div>

            <div className={clsx(css.wrapperPlacePlaylist, css.wrapperPlacePlaylistOne)}>
                <label>{t('placeScriptPlaylist')}</label>
                <br />
                <input
                    className={css.rightMenuNameContent}
                    placeholder={t('numberPlacePlaylist') as string}
                    value={scenarioIndex}
                    onChange={onScenarioPlaceIndexChange}
                    maxLength={125}
                />
            </div>

            <button className={css.btnEditedTrigger} onClick={showTriggerContentSettingModal}>
                <img alt="" />
                {t('btnEditScript')}
            </button>

            <div className={css.wrapMainRightMenuInfoAll}>
                <h3>
                    {t('usedContent')}
                    <span>{usablePlaylistItems.length}</span>
                </h3>
                <span
                    className={isUsablePlaylistItemsShown ? css.iconArrowInfoUp : css.iconArrowInfoDown}
                    onClick={showUsablePlaylistItems}
                />

                {isUsablePlaylistItemsShown && (
                    <div className={clsx(css.mainRightMenuInfoAll, css.mainRightMenuInfoAllOne)}>
                        <ul>
                            {usablePlaylistItems.map(({ id, name }) => (
                                <li key={id}>
                                    <p>{name}</p>
                                    <a onClick={goToUsablePlaylistItem(id)}>{t('goTo')}</a>
                                </li>
                            ))}
                        </ul>
                    </div>
                )}
            </div>

            <button className={css.btnAddedContentTrigger} onClick={addContentToScenario}>
                <img alt="" />
                {t('btnAddContentToScript')}
            </button>
            <button className={css.btnDeleteTrigger} onClick={deleteScenario}>
                <img alt="" />
                {t('btnDeleteScript')}
            </button>
        </div>
    );
};

export default memo(ScenarioSettingMenu);
