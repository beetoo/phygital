import React, { memo } from 'react';
import { PatternFormat } from 'react-number-format';
import { clsx } from 'clsx';

import { useMultiplyContentSettingMenu } from '../../hooks/useMultiplyContentSettingMenu';
import { isTimeAllowed } from '../../../../../../helpers';

import css from '../../style.m.css';

const MultiplyContentSettingMenu: React.FC = () => {
    const {
        selectedPlaylistItemsNumber,
        playlistItemsDuration,
        playlistItemsFilesSize,
        isOnlyImageItems,
        isOnlyVideoItems,
        isOnlyWebItems,
        onPlaylistItemsDurationChange,
        isDifferentDurations,
        showTriggerContentSettingModal,
        deleteSelectedPlaylistItems,
        isWithScenariosOnly,
        isWithCreateScenarioButton,
        isSelectableItemsOnly,
        isItemsWithinOneScenarioSelected,
        t,
    } = useMultiplyContentSettingMenu();

    return (
        <div className={css.wrapMainRightMenuSetup124}>
            <h2>{isItemsWithinOneScenarioSelected ? t('scriptContentSettings') : t('contentSettings')}</h2>
            <h2>
                {t('contentSelected')}
                {selectedPlaylistItemsNumber}
            </h2>
            <div className={css.wrapperTotalInfo}>
                <p className={css.mainRightMenuInfoItem}>
                    {t('totalSize')}
                    <span>{playlistItemsFilesSize}</span>
                </p>
            </div>

            {(isOnlyImageItems || isOnlyVideoItems || isOnlyWebItems) && (
                <div className={css.wrapperDisplayDuration}>
                    <label>{t('showDuration')}</label>
                    <br />
                    <PatternFormat
                        className={css.rightMenuNameContent}
                        format="##:##:##"
                        placeholder={playlistItemsDuration === 'Разная' ? t('different') : '00:00:00'}
                        isAllowed={isTimeAllowed}
                        value={playlistItemsDuration}
                        onChange={onPlaylistItemsDurationChange}
                        disabled={isOnlyVideoItems}
                    />
                    {isOnlyImageItems && isDifferentDurations && (
                        <div className={css.wrapNoChange}>{t('clickInputChange')}</div>
                    )}
                    <div className={css.wrapNoChange}>
                        {isOnlyImageItems ? `${t('textEight')}` : `${t('textNine')}`}
                    </div>
                </div>
            )}

            {!isWithCreateScenarioButton && (
                <div>
                    <button
                        className={css.btnCreateTrigger}
                        onClick={showTriggerContentSettingModal}
                        disabled={!isSelectableItemsOnly}
                    >
                        <img alt="" />
                        {t('btnCreateTriggerScript')}
                    </button>
                    {!isSelectableItemsOnly && (
                        <div className={clsx(css.wrapNoChange, css.wrapNoChangeItem)}>{t('textTen')}</div>
                    )}
                </div>
            )}

            <button className={css.btnDeleteContent} onClick={deleteSelectedPlaylistItems}>
                <img alt="" />
                {`${isWithScenariosOnly ? `${t('deleteContentScript')}` : `${t('deleteContentPlaylist')}`}`}
            </button>
        </div>
    );
};

export default memo(MultiplyContentSettingMenu);
