import React from 'react';
import { useTranslation } from 'react-i18next';

import { tOperator, tParameter, tParameterValue } from '../TriggerScenarioSettingModal/helpers';
import { WeatherAndTrafficRuleType } from '../../hooks/useScenarioSettingMenu';

import css from '../../style.m.css';

type Props = {
    weatherRules: WeatherAndTrafficRuleType[];
};

const WeatherRules: React.FC<Props> = ({ weatherRules }: Props) => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    return weatherRules.length > 0 ? (
        <li className={css.mainRightMenuInfoRulesHeader}>
            {t('weather')}:
            <ul className={css.mainRightMenuInfoRulesList}>
                {weatherRules.map((rule: WeatherAndTrafficRuleType) => {
                    const parameter = rule.data.parameter ?? 'temperature';
                    const operator = rule.data.operator ?? '';
                    const value = rule.data.value ?? '';

                    const sign = isNaN(Number(value))
                        ? ''
                        : parameter === 'temperature' || parameter === 'water_temperature'
                          ? '°C'
                          : parameter === 'wind'
                            ? t('ms')
                            : parameter === 'atmospheric_pressure'
                              ? t('mmHg')
                              : parameter === 'humidity'
                                ? '%'
                                : '';

                    const parameterRus = tParameter(parameter, t).toLowerCase();
                    const operatorRus = tOperator(operator, t).toLowerCase();
                    const valueRus = tParameterValue(value, t).toLowerCase();

                    return (
                        <li key={rule.id}>
                            <span>{parameterRus}:</span> {operatorRus} {valueRus} {sign}
                        </li>
                    );
                })}
            </ul>
        </li>
    ) : null;
};

export default WeatherRules;
