import React from 'react';

import { SelectButton, PopoverCustom } from '../../../../../../ui-kit';
import { usePopoverCustom } from '../../hooks/usePopoverCustom';

import css from '../../style.m.css';

type Props = {
    playlistItemSeconds: number;
    isPlaylistItemSelected: boolean;
    selectPlaylistItem: () => void;
    unselectPlaylistItem: () => void;
};

const ThreeDots: React.FC<Props> = ({
    playlistItemSeconds,
    isPlaylistItemSelected,
    selectPlaylistItem,
    unselectPlaylistItem,
}) => {
    const { anchorElRef, isOpen, onPopoverOpen, onPopoverClose } = usePopoverCustom();

    return (
        <div className={css.wrapIconSelectThreeDots} onClick={(e) => e.stopPropagation()}>
            <span ref={anchorElRef} className={css.iconSelectThreeDots} onClick={onPopoverOpen} />
            <PopoverCustom
                sx={{ left: [1, 3].includes(playlistItemSeconds) ? 0 : 2 }}
                open={isOpen}
                anchorEl={anchorElRef.current}
                onClose={onPopoverClose}
            >
                <div className={css.wrapTooltipSelect} onClick={(e) => e.stopPropagation()}>
                    <div className={css.wrapIconContentSelect}>
                        <SelectButton
                            active={isPlaylistItemSelected}
                            onClick={(e) => {
                                e.stopPropagation();
                                isPlaylistItemSelected ? unselectPlaylistItem() : selectPlaylistItem();
                            }}
                        />
                    </div>
                    <p className={isPlaylistItemSelected ? css.wrapTooltipSelectItemTwo : css.wrapTooltipSelectItemOne}>
                        {isPlaylistItemSelected ? 'Отменить выбор' : 'Выбрать'}
                    </p>
                </div>
            </PopoverCustom>
        </div>
    );
};

export default ThreeDots;
