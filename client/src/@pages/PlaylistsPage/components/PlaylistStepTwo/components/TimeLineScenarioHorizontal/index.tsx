import React from 'react';
import { Draggable } from '@hello-pangea/dnd';
import { Globe } from '@phosphor-icons/react';
import { clsx } from 'clsx';

import ThreeDots from '../ThreeDots';
import { PlaylistItemType, ScenarioType, CONTENT_TYPE } from '../../../../../../../../common/types';
import { useTimeLineScenario } from '../../hooks/useTimeLineScenario';
import { formatSecondsToTimeString } from '../../../../../../../../common/helpers';
import { SelectButton, Tooltip } from '../../../../../../ui-kit';

import iconTrigger from '../../../../../../assets/iconGitFork_22x22.svg';

import css from '../../style.m.css';

type Props = {
    index: number;
    scenario: ScenarioType;
    playlistItems: PlaylistItemType[];
};

const TimeLineScenarioHorizontal: React.FC<Props> = ({ scenario, playlistItems, index }) => {
    const {
        setActivePlaylistItem,
        setActiveScenario,
        selectPlaylistItem,
        unselectPlaylistItem,
        isPlaylistItemActive,
        isScenarioActive,
        isPlaylistItemSelected,
        scenarioSeconds,
    } = useTimeLineScenario({ playlistItems });

    return (
        <Draggable draggableId={scenario.id} index={index}>
            {(provided) => (
                <div
                    className={clsx(
                        css.wrapperTriggerScriptBlock,
                        scenario && css.withTrigger,
                        isScenarioActive(scenario.id) && css.active,
                    )}
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    onClick={(e) => {
                        e.stopPropagation();
                        setActiveScenario(scenario.id);
                    }}
                >
                    <span className={css.timeLineSliderEnd} />
                    <div style={{ display: 'flex' }}>
                        {playlistItems.map(({ id, content, delay }, index, array) => {
                            const playlistItemSeconds =
                                content?.type === CONTENT_TYPE.IMAGE || content?.type === CONTENT_TYPE.WEB
                                    ? delay
                                    : (content?.duration ?? 0);
                            const widthOffset =
                                array.length === 1 ? 6 : index === 0 || index === array.length - 1 ? 3 : 0;

                            return (
                                <div
                                    key={id}
                                    className={css.wrapperTriggerScript}
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        setActivePlaylistItem(id);
                                    }}
                                >
                                    <div
                                        className={clsx(
                                            css.wrapperTriggerComponentItem,
                                            (isPlaylistItemActive(id) || isPlaylistItemSelected(id)) && css.active,
                                        )}
                                        style={{ width: playlistItemSeconds * 10 - widthOffset }}
                                    >
                                        {playlistItemSeconds > 2 && (
                                            <div className={css.wrapperTriggerComponentImg}>
                                                {content?.type === CONTENT_TYPE.IMAGE ? (
                                                    <img
                                                        className={clsx(
                                                            playlistItemSeconds === 5 && css.fiveSecondsSize,
                                                        )}
                                                        src={content?.src}
                                                        alt=""
                                                    />
                                                ) : content?.type === CONTENT_TYPE.VIDEO ? (
                                                    <video
                                                        className={clsx(
                                                            playlistItemSeconds === 5 && css.fiveSecondsSize,
                                                        )}
                                                        src={content?.src}
                                                    />
                                                ) : content?.type === CONTENT_TYPE.WEB ? (
                                                    <Globe size={30} weight="thin" />
                                                ) : null}
                                            </div>
                                        )}
                                        {playlistItemSeconds > 7 && (
                                            <Tooltip
                                                title={`${content?.originFilename} (${formatSecondsToTimeString(
                                                    playlistItemSeconds,
                                                )})`}
                                                placement="top"
                                            >
                                                <div
                                                    className={css.wrapperTriggerComponentText}
                                                    style={{ width: playlistItemSeconds * 10 - 74 }}
                                                >
                                                    <p className={css.triggerComponentItem}>
                                                        {content?.originFilename}
                                                    </p>
                                                </div>
                                            </Tooltip>
                                        )}
                                        {playlistItemSeconds < 7 ? (
                                            <ThreeDots
                                                playlistItemSeconds={playlistItemSeconds}
                                                isPlaylistItemSelected={isPlaylistItemSelected(id)}
                                                selectPlaylistItem={() => selectPlaylistItem(id)}
                                                unselectPlaylistItem={() => unselectPlaylistItem(id)}
                                            />
                                        ) : (
                                            <SelectButton
                                                className={css.iconContentSelect}
                                                active={isPlaylistItemSelected(id)}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                    isPlaylistItemSelected(id)
                                                        ? unselectPlaylistItem(id)
                                                        : selectPlaylistItem(id);
                                                }}
                                            />
                                        )}
                                    </div>
                                </div>
                            );
                        })}
                    </div>

                    {scenario && (
                        <div className={css.wrapperTriggerComponentText1}>
                            {scenarioSeconds > 6 && (
                                <>
                                    <div
                                        className={css.wrapTextScenario}
                                        style={{ width: scenarioSeconds * 10 * 0.65 }}
                                    >
                                        <img src={iconTrigger} alt="" />
                                        <p className={css.triggerComponentItem1}>{scenario.name}</p>
                                    </div>
                                </>
                            )}
                        </div>
                    )}
                </div>
            )}
        </Draggable>
    );
};

export default TimeLineScenarioHorizontal;
