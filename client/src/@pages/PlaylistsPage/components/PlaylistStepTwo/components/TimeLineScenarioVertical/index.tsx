import React from 'react';
import { Draggable } from '@hello-pangea/dnd';
import { Globe } from '@phosphor-icons/react';
import { clsx } from 'clsx';

import { CONTENT_TYPE, PlaylistItemType, ScenarioType } from '../../../../../../../../common/types';
import { useTimeLineScenario } from '../../hooks/useTimeLineScenario';
import { formatSecondsToTimeString } from '../../../../../../../../common/helpers';
import { SelectButton, Tooltip } from '../../../../../../ui-kit';

import iconTrigger from '../../../../../../assets/iconGitFork_22x22.svg';

import css from './style.m.css';

type Props = {
    index: number;
    scenario: ScenarioType;
    playlistItems: PlaylistItemType[];
    vertical?: boolean;
};

const TimeLineScenarioVertical: React.FC<Props> = ({ scenario, playlistItems, index }) => {
    const {
        setActivePlaylistItem,
        setActiveScenario,
        selectPlaylistItem,
        unselectPlaylistItem,
        isPlaylistItemActive,
        isScenarioActive,
        isPlaylistItemSelected,
        scenarioSeconds,
        t,
    } = useTimeLineScenario({ playlistItems });

    return (
        <Draggable draggableId={scenario.id} index={index}>
            {(provided) => (
                <div
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    className={css.containerVerticalTrigger}
                >
                    <div
                        className={clsx(
                            css.wrapperTriggerScriptBlock,
                            scenario && css.withTrigger,
                            isScenarioActive(scenario.id) && css.active,
                        )}
                        onClick={(e) => {
                            e.stopPropagation();
                            setActiveScenario(scenario.id);
                        }}
                    >
                        <span className={css.timeLineSliderEnd} />
                        <div style={{ display: 'flex' }}>
                            {playlistItems.map(({ id, content, delay }) => {
                                const playlistItemSeconds =
                                    content?.type === CONTENT_TYPE.IMAGE || content?.type === CONTENT_TYPE.WEB
                                        ? delay
                                        : (content?.duration ?? 0);

                                return (
                                    <div
                                        key={id}
                                        className={css.wrapperTriggerScript}
                                        onClick={(e) => {
                                            e.stopPropagation();
                                            setActivePlaylistItem(id);
                                        }}
                                    >
                                        <div
                                            className={clsx(
                                                css.wrapperTriggerComponentItem,
                                                (isPlaylistItemActive(id) || isPlaylistItemSelected(id)) && css.active,
                                            )}
                                        >
                                            <div className={css.wrapperTriggerComponentImg}>
                                                {content?.type === CONTENT_TYPE.IMAGE ? (
                                                    <img src={content?.src} alt="" />
                                                ) : content?.type === CONTENT_TYPE.VIDEO ? (
                                                    <video src={content?.src} />
                                                ) : content?.type === CONTENT_TYPE.WEB ? (
                                                    <Globe size={30} weight="thin" />
                                                ) : null}
                                            </div>

                                            <Tooltip
                                                title={`${content?.originFilename} (${formatSecondsToTimeString(
                                                    playlistItemSeconds,
                                                )})`}
                                                placement="top"
                                            >
                                                <div className={css.wrapperTriggerComponentText}>
                                                    <p className={css.triggerComponentItem}>
                                                        {content?.originFilename}
                                                    </p>
                                                </div>
                                            </Tooltip>

                                            <SelectButton
                                                className={css.iconContentSelect}
                                                active={isPlaylistItemSelected(id)}
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                    isPlaylistItemSelected(id)
                                                        ? unselectPlaylistItem(id)
                                                        : selectPlaylistItem(id);
                                                }}
                                            />
                                        </div>
                                    </div>
                                );
                            })}
                        </div>

                        {scenario && (
                            <div className={css.wrapperTriggerComponentText1}>
                                <div className={css.wrapTextScenario}>
                                    <img src={iconTrigger} alt="" />
                                    <p className={css.triggerComponentItem1}>{scenario.name}</p>
                                </div>
                            </div>
                        )}
                    </div>
                    <div>
                        <h3>{t('contentTime')}</h3>
                        <p className={css.triggerScriptTimeItem}>{formatSecondsToTimeString(scenarioSeconds)}</p>
                    </div>
                </div>
            )}
        </Draggable>
    );
};

export default TimeLineScenarioVertical;
