import React from 'react';
import { Droppable } from '@hello-pangea/dnd';
import { clsx } from 'clsx';

import { TimelineSlider } from '../../../../../../ui-kit';
import TimeLineScenarioHorizontal from '../TimeLineScenarioHorizontal';
import TimeLinePlaylistItemHorizontal from '../TimeLinePlaylistItemHorizontal';
import { useTimeLineHorizontal } from '../../hooks/useTimeLineHorizontal';
import { useTimeLineRef } from '../../hooks/useTimeLIneRef';
import { formatSecondsToTimeString } from '../../../../../../../../common/helpers';
import { SLIDER_STEP_VALUE } from '../../constants';

import css from '../../style.m.css';

const TimeLineHorizontal: React.FC = () => {
    const {
        timeLineWrapperRef,

        playlistDtoItems,
        playlistDtoItemsWithScenario,
        timeLineArray,
        lastElementWidth,
        lastElementDisplay,
        is20SecDividerRequired,
        is10SecDividerRequired,
        totalPlaylistDuration,
        onSliderChange,
        currentTimelineSeconds,
        isTimelineSliderShown,
        onSliderTouchStart,
        onSliderTouchEnd,
        onSliderMove,
        t,
    } = useTimeLineHorizontal();

    const { addTimeLineRef, scrollToLeft, scrollToRight } = useTimeLineRef();

    return (
        <Droppable droppableId="playlistItems" direction="horizontal">
            {(provided) => (
                <div style={{ display: 'flex' }} ref={provided.innerRef} {...provided.droppableProps}>
                    <div style={{ flexShrink: 0, flexBasis: '2px' }} onMouseOver={scrollToLeft} />
                    <div className={css.wrapperTimeLine} ref={timeLineWrapperRef}>
                        {playlistDtoItems.length > 0 ? (
                            <div className={css.wrapperTimeLineFull} ref={addTimeLineRef}>
                                {isTimelineSliderShown && (
                                    <TimelineSlider
                                        timelineWidth={totalPlaylistDuration * 10}
                                        value={currentTimelineSeconds || 0}
                                        step={SLIDER_STEP_VALUE}
                                        min={0}
                                        max={totalPlaylistDuration || 0}
                                        onChange={onSliderChange}
                                        onMouseUp={onSliderTouchEnd}
                                        onMouseDown={onSliderTouchStart}
                                        onMouseMove={onSliderMove}
                                    />
                                )}

                                <div className={css.wrapperTriggerScriptTime}>
                                    {timeLineArray.map((_, index) => (
                                        <div
                                            key={index}
                                            className={css.wrapTriggerScriptTimeItem}
                                            style={{ width: lastElementWidth(index) }}
                                        >
                                            <p
                                                style={{ display: lastElementDisplay(index) }}
                                                className={clsx(css.triggerScriptTimeItem, {
                                                    [css.triggerScriptTimeItemFirstDivider]: index === 0,
                                                    [css.triggerScriptTimeItem20secDivider]:
                                                        is20SecDividerRequired(index),
                                                    [css.triggerScriptTimeItem10secDivider]:
                                                        is10SecDividerRequired(index),
                                                })}
                                            >
                                                {formatSecondsToTimeString(index * 20)}
                                            </p>
                                        </div>
                                    ))}
                                </div>

                                <div className={css.wrapperTriggerComponent}>
                                    {playlistDtoItemsWithScenario.map((item, index) =>
                                        'playlistItems' in item ? (
                                            <TimeLineScenarioHorizontal
                                                key={item.scenario.id}
                                                index={index}
                                                scenario={item.scenario}
                                                playlistItems={item.playlistItems}
                                            />
                                        ) : (
                                            <TimeLinePlaylistItemHorizontal
                                                key={item.id}
                                                index={index}
                                                playlistItem={item}
                                            />
                                        ),
                                    )}
                                </div>
                            </div>
                        ) : (
                            <div className={css.wrapperTimeLineEmpty}>
                                <div className={css.wrapperTimeLineItem}>
                                    <p className={css.timeLineItem}>{t('textSix')}</p>
                                    <p className={css.timeLineItem}>{t('textSeven')}</p>
                                </div>
                            </div>
                        )}
                        <div style={{ display: 'none' }}>{provided.placeholder}</div>
                    </div>
                    <div style={{ flexShrink: 0, flexBasis: '2px' }} onMouseOver={scrollToRight} />
                </div>
            )}
        </Droppable>
    );
};

export default TimeLineHorizontal;
