import React, { memo } from 'react';
import { PatternFormat } from 'react-number-format';
import { clsx } from 'clsx';

import { Tooltip } from '../../../../../../ui-kit';
import { useScenarioContentSettingMenu } from '../../hooks/useScenarioContentSettingMenu';
import { isTimeAllowed } from '../../../../../../helpers';

import css from '../../style.m.css';

const ScenarioContentSettingMenu: React.FC = () => {
    const {
        playlistItemSrc,
        playlistItemName,
        playlistItemFileTypeAndExtension,
        playlistItemFileSize,
        playlistItemDimensions,
        playlistItemOrientation,
        isVideoType,
        isImageType,
        isWebType,

        playlistItemDuration,
        onPlaylistItemDurationChange,
        placeIndex,
        onPlaylistItemPlaceIndexChange,
        deletePlaylistItem,

        isContentInfoShown,
        showContentInfo,
        showContentViewModal,

        t,
    } = useScenarioContentSettingMenu();

    return (
        <div className={css.wrapMainRightMenuSetup124}>
            <h2>
                {t('scriptContentSettings')}
                {/* Настройки контента в сценарии */}
            </h2>
            <div className={css.wrapperImgVideo}>
                {isVideoType ? (
                    <div className={css.wrapperVideo}>
                        <video src={playlistItemSrc} width="292" height="164" />
                    </div>
                ) : isImageType ? (
                    <img src={playlistItemSrc} width="292" height="164" alt="" />
                ) : isWebType ? (
                    <iframe src={playlistItemSrc} width="292" height="164" />
                ) : null}
                <div className={css.wrapperIconFullScreen}>
                    <Tooltip
                        title={t('fullScreen')}
                        // "Во весь экран"
                        placement="top-end"
                    >
                        <button
                            className={css.iconFullScreen}
                            onClick={(event) => {
                                event.stopPropagation();
                                showContentViewModal();
                            }}
                        ></button>
                    </Tooltip>
                </div>
            </div>

            <div className={css.wrapMainRightMenuInfoAll}>
                <h3>{playlistItemName}</h3>
                <span
                    className={isContentInfoShown ? css.iconArrowInfoUp : css.iconArrowInfoDown}
                    onClick={showContentInfo}
                />
                {isContentInfoShown && (
                    <div className={css.mainRightMenuInfoAll}>
                        <ul>
                            <li>
                                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItemOne)}>
                                    {/* Тип: */}
                                    {t('type')}
                                    <span>{playlistItemFileTypeAndExtension}</span>
                                </p>
                            </li>
                            <li>
                                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItemOne)}>
                                    {/* Размер: */}
                                    {t('size')}
                                    <span>{playlistItemFileSize}</span>
                                </p>
                            </li>
                            <li>
                                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItemOne)}>
                                    {/* Разрешение: */}
                                    {t('resolutionR')}
                                    <span>{playlistItemDimensions}</span>
                                </p>
                            </li>
                            <li>
                                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItemOne)}>
                                    {/* Ориентация: */}
                                    {t('orientationR')}
                                    <span>{playlistItemOrientation}</span>
                                </p>
                            </li>
                        </ul>
                    </div>
                )}
            </div>

            <div className={css.wrapperDisplayDuration}>
                <label>
                    {t('showDuration')}
                    {/* Длительность показа */}
                </label>
                <br />
                <PatternFormat
                    className={css.rightMenuNameContent}
                    format="##:##:##"
                    placeholder="00:00:00"
                    isAllowed={isTimeAllowed}
                    value={playlistItemDuration}
                    onChange={onPlaylistItemDurationChange}
                    disabled={isVideoType}
                />
                {isVideoType && (
                    <div className={css.wrapNoChange}>
                        {t('textNine')}
                        {/* Длительность видео нельзя изменить */}
                    </div>
                )}
            </div>
            <div className={css.wrapperPlacePlaylist}>
                <label>
                    {t('placeContentPlaylist')}
                    {/* Место контента в сценарии */}
                </label>
                <br />
                <input
                    className={css.rightMenuNameContent}
                    placeholder={t('numberPlacePlaylist') as string}
                    // "Номер места в сценарии"
                    value={placeIndex}
                    onChange={onPlaylistItemPlaceIndexChange}
                    maxLength={125}
                />
            </div>

            <button className={css.btnDeleteContent} onClick={deletePlaylistItem}>
                <img alt="" />
                {t('btnDeleteScriptContent')}
                {/* Удалить контент из сценария */}
            </button>
        </div>
    );
};

export default memo(ScenarioContentSettingMenu);
