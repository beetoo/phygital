import React, { Dispatch, memo, SetStateAction } from 'react';
import { clsx } from 'clsx';

import { MenuItem, Select } from '../../../../../../../../ui-kit';
import { useWeatherRow } from '../../../../hooks/useWeatherRow';
import { ScenarioRuleType, WeatherAndTrafficData } from '../../../../../../../../../../common/types';
import { ParameterValue } from '../../types';

import css from '../../style.m.css';

type Props = {
    weatherRowId: string;
    rulesList: ScenarioRuleType[];
    data?: WeatherAndTrafficData;
    updateRulesList: (data: { [key: string]: string }) => void;
    setRowIdsWithError: Dispatch<SetStateAction<string[]>>;
};

const WeatherRow: React.FC<Props> = (props) => {
    const {
        conditionParameter,
        onConditionParameterSelect,
        parameterOperator,
        onParameterOperatorSelect,
        parameterValue,
        parameterValues,
        onParameterValueSelect,
        onParameterValueChange,
        isOperatorRequired,
        isConditionParameterDisabled,
        isParameterValueDisabled,
        t,
        weatherConditionParameters,
        parameterOperators,
    } = useWeatherRow(props);

    return (
        <>
            <div className={css.wrapperTriggerBlock1}>
                <Select
                    style={{ margin: '0', background: '#fff', borderRadius: '6px' }}
                    placeholder={t('selectValue') as string}
                    width={231}
                    value={conditionParameter}
                    onChange={onConditionParameterSelect}
                >
                    {weatherConditionParameters.map((parameter) => (
                        <MenuItem key={parameter} value={parameter} disabled={isConditionParameterDisabled(parameter)}>
                            {parameter}
                        </MenuItem>
                    ))}
                </Select>
            </div>
            {conditionParameter && (
                <div className={clsx(css.wrapperTriggerBlock1, css.wrapperTriggerBlock2)}>
                    <Select
                        style={{ margin: '0', background: '#fff', borderRadius: '6px' }}
                        placeholder={isOperatorRequired ? undefined : (t('selectValue') as string)}
                        width={isOperatorRequired ? 120 : 280}
                        value={isOperatorRequired ? parameterOperator : parameterValue}
                        onChange={isOperatorRequired ? onParameterOperatorSelect : onParameterValueSelect}
                    >
                        {(isOperatorRequired ? parameterOperators : parameterValues).map((parameter) => (
                            <MenuItem
                                key={parameter}
                                value={parameter}
                                disabled={
                                    isOperatorRequired ? false : isParameterValueDisabled(parameter as ParameterValue)
                                }
                            >
                                {parameter}
                            </MenuItem>
                        ))}
                    </Select>
                </div>
            )}
            {isOperatorRequired && (
                <div className={css.wrapperTriggerBlock1}>
                    <input
                        className={css.inputWeatherTraffic}
                        value={parameterValue}
                        onChange={onParameterValueChange}
                        maxLength={125}
                    />
                    <div className={css.wrapInputWeatherTraffic}>
                        <span className={css.inputWeatherTrafficItem}>
                            {[t('temperature'), t('waterTemperature')].includes(conditionParameter)
                                ? `°C`
                                : conditionParameter === t('wind')
                                  ? t('ms')
                                  : conditionParameter === t('atmosphericPressure')
                                    ? t('mmHg')
                                    : conditionParameter === t('airHumidity')
                                      ? '%'
                                      : ''}
                        </span>
                    </div>
                </div>
            )}
        </>
    );
};

export default memo(WeatherRow);
