import { TFunction } from 'i18next';

import {
    ConditionParameter,
    ParameterOperator,
    ParameterValue,
    RuleCondition,
    ScenarioCondition,
    WeatherAndTrafficOperator,
    WeatherParameter,
    WeatherAndTrafficValue,
} from '../types';
import { ScenarioRuleType, TimeData, WeatherAndTrafficData } from '../../../../../../../../../common/types';

export const tSelectCondition = (value: RuleCondition, t: TFunction): ScenarioCondition => {
    switch (value) {
        case t('weather'):
            return 'weather';
        case t('traffic'):
            return 'traffic';
        case t('time'):
            return 'time';
        default:
            return 'weather';
    }
};

export const tCondition = (value: ScenarioCondition | undefined, t: TFunction): RuleCondition | '' => {
    switch (value) {
        case 'weather':
            return t('weather');
        case 'traffic':
            return t('traffic');
        case 'time':
            return t('time');
        default:
            return '';
    }
};

export const tSelectParameter = (value: ConditionParameter | '', t: TFunction): WeatherParameter | undefined => {
    switch (value) {
        case t('temperature'):
            return 'temperature';
        case t('waterTemperature'):
            return 'water_temperature';
        case t('cloudiness'):
            return 'cloud_cover';
        case t('precipitation'):
            return 'rain_fall';
        case t('wind'):
            return 'wind';
        case t('atmosphericPressure'):
            return 'atmospheric_pressure';
        case t('airHumidity'):
            return 'humidity';
        case '':
            return undefined;
    }
};

export const tParameter = (value: WeatherParameter | '', t: TFunction): ConditionParameter | '' => {
    switch (value) {
        case 'temperature':
            return t('temperature');
        case 'water_temperature':
            return t('waterTemperature');
        case 'cloud_cover':
            return t('cloudiness');
        case 'rain_fall':
            return t('precipitation');
        case 'wind':
            return t('wind');
        case 'atmospheric_pressure':
            return t('atmosphericPressure');
        case 'humidity':
            return t('airHumidity');
        default:
            return '';
    }
};

export const tSelectOperator = (value: ParameterOperator | '', t: TFunction): WeatherAndTrafficOperator | undefined => {
    switch (value) {
        case t('less'):
            return 'less';
        case t('more'):
            return 'more';
        case t('equal'):
            return 'equally';
        case '':
            return undefined;
    }
};

export const tOperator = (value: WeatherAndTrafficOperator | '', t: TFunction): ParameterOperator | '' => {
    switch (value) {
        case 'less':
            return t('less');
        case 'more':
            return t('more');
        case 'equally':
            return t('equal');
        default:
            return '';
    }
};

export const tSelectParameterValue = (value: ParameterValue | '', t: TFunction): WeatherAndTrafficValue => {
    switch (value) {
        // cloudCover
        case t('overcast'):
            return 'overcast';
        case t('cloudy'):
            return 'cloudy';
        case t('clearly'):
            return 'clear';

        // rainFall
        case t('drizzle'):
            return 'drizzle';
        case t('rain'):
            return 'rain';
        case t('sleet'):
            return 'wet-snow';
        case t('snow'):
            return 'snow';
        case t('snowfall'):
            return 'snow-showers';
        case t('hailThunderstorm'):
            return 'thunderstorm';

        // wind
        case t('light'):
            return '[0,5]';
        case t('moderate'):
            return '[5.1,14]';
        case t('strong'):
            return '[14.1,24]';
        case t('veryStrong'):
            return '[24.1,32]';
        case t('hurricane'):
            return '[32.1,100]';

        // humidity
        case t('dry'):
            return '[0,50]';
        case t('normal'):
            return '[50,60]';
        case t('moist'):
            return '[61,75]';
        case t('wet'):
            return '[76,100]';
        default:
            return value;
    }
};

export const tParameterValue = (value: WeatherAndTrafficValue | '', t: TFunction): ParameterValue => {
    switch (value) {
        case 'overcast':
            return t('overcast');
        case 'cloudy':
            return t('cloudy');
        case 'clear':
            return t('clearly');
        case 'drizzle':
            return t('drizzle');
        case 'rain':
            return t('rain');
        case 'wet-snow':
            return t('sleet');
        case 'snow':
            return t('snow');
        case 'snow-showers':
            return t('snowfall');
        case 'thunderstorm':
            return t('hailThunderstorm');

        case '[0,5]':
            return t('light');
        case '[5.1,14]':
            return t('moderate');
        case '[14.1,24]':
            return t('strong');
        case '[24.1,32]':
            return t('veryStrong');
        case '[32.1,100]':
            return t('hurricane');

        case '[0,50]':
            return t('dry');
        case '[50,60]':
            return t('normal');
        case '[61,75]':
            return t('moist');
        case '[76,100]':
            return t('wet');
        default:
            return value;
    }
};

export const getEnabledRules = (rulesList: ScenarioRuleType[]): ScenarioRuleType[] =>
    rulesList.filter((rule) => {
        if (rule.condition !== 'time') {
            const data = rule.data as WeatherAndTrafficData;

            if (data?.parameter) {
                const isOperatorRequired = (
                    ['temperature', 'water_temperature', 'atmospheric_pressure'] as WeatherParameter[]
                ).includes(data.parameter);

                if (isOperatorRequired) {
                    return data && data.parameter && data.operator && data.value;
                } else {
                    return data && data.parameter && data.value;
                }
            } else {
                return data && data.operator && data.value;
            }
        } else {
            const data = rule.data as TimeData;
            return (
                data &&
                data.startDay &&
                data.startTime &&
                data.endDay &&
                data.endTime &&
                typeof data.byDay === 'string' &&
                typeof data.frequency === 'string'
            );
        }
    });

export const getRuleListFilterByParameter =
    (ruleList: ScenarioRuleType[]) =>
    (weatherParameter: WeatherParameter): ScenarioRuleType[] =>
        ruleList.filter(
            ({ data }) => data && 'parameter' in data && (data?.parameter as WeatherParameter) === weatherParameter,
        );

type ValueErrorProps = {
    prevOperator: ParameterOperator | undefined | '';
    prevValue: ParameterValue | undefined;
    operator: ParameterOperator | '';
    value: ParameterValue | '';
};

export const isOperatorAndValueMixError = (props: ValueErrorProps, t: TFunction): boolean => {
    const { prevOperator, prevValue, operator, value } = props;

    if (!prevOperator || !prevValue || !operator || !value) {
        return false;
    }

    const prevValueNum = Number(prevValue);
    const valueNum = Number(value);

    if (prevOperator === t('less')) {
        const enabled =
            (operator === t('more') && valueNum < prevValueNum - 1) ||
            (operator === t('equal') && valueNum === prevValueNum);

        return !enabled;
    }
    if (prevOperator === t('more')) {
        const enabled =
            (operator === t('less') && valueNum > prevValueNum + 1) ||
            (operator === t('equal') && valueNum === prevValueNum);

        return !enabled;
    }

    if (prevOperator === t('equal')) {
        const enabled = (operator === t('more') || operator === t('less')) && valueNum === prevValueNum;

        return !enabled;
    }

    return false;
};

// export const prepareRuleListForCompare = (rulesList: ScenarioRuleType[]): ScenarioRuleType[] =>
//     rulesList.map(({ data, ...rest }) => ({
//         ...rest,
//         ...data,
//         startDate: data && 'startDate' in data && data.startDate ? new Date(data.startDate) : undefined,
//         endDate: data && 'endDate' in data && data.endDate ? new Date(data.endDate) : undefined,
//     }));
