import {
    HumidityParameterValueRus,
    HumidityParameterValueEng,
    WindParameterValueEng,
    WindParameterValueRus,
    RainFallParameterValueEng,
    RainFallParameterValueRus,
    CloudCoverParameterValueRus,
    CloudCoverParameterValueEng,
    ParameterOperatorRus,
    ParameterOperatorEng,
    ConditionParameterEng,
    ConditionParameterRus,
    RuleConditionEng,
    RuleConditionRus,
    ConditionParameterEs,
    ParameterOperatorEs,
    CloudCoverParameterValueEs,
    RainFallParameterValueEs,
    WindParameterValueEs,
    HumidityParameterValueEs,
    RuleConditionEs,
} from '../types';

// правила
export const ruleConditionsRus = ['Погода', 'Время'] as RuleConditionRus[]; // 'Пробки'
export const ruleConditionsEng = ['Weather', 'Time'] as RuleConditionEng[];
export const ruleConditionsEs = ['Clima', 'Tiempo'] as RuleConditionEs[];

// параметры
export const weatherConditionParametersRus = [
    'Температура',
    // 'Температура воды',
    'Облачность',
    'Осадки',
    // 'Ветер',
    // 'Атмосферное давление',
    // 'Влажность воздуха',
] as ConditionParameterRus[];
export const weatherConditionParametersEng = [
    'Temperature',
    // 'Water Temperature',
    'Cloudiness',
    'Precipitation',
    // 'Wind',
    // 'Atmospheric Pressure',
    // 'Air Himidity',
] as ConditionParameterEng[];
export const weatherConditionParametersEs = [
    'Temperatura',
    // 'Temperatura de Agua',
    'Nubosidad',
    'Precipitación',
    // 'Viento',
    // 'Presión atmosférica',
    // 'Humedad del aire',
] as ConditionParameterEs[];

export const parameterOperatorsRus = ['Меньше', 'Больше', 'Равно'] as ParameterOperatorRus[];
export const parameterOperatorsEng = ['Less', 'More', 'Equal'] as ParameterOperatorEng[];
export const parameterOperatorsEs = ['Menos', 'Más', 'Igual'] as ParameterOperatorEs[];

// условия
export const cloudCoverConditionsRus = ['Пасмурно', 'Облачно', 'Ясно'] as CloudCoverParameterValueRus[];
export const cloudCoverConditionsEng = ['Overcast', 'Cloudy', 'Clearly'] as CloudCoverParameterValueEng[];
export const cloudCoverConditionsEs = ['Encapotado', 'Nublado', 'Claramente'] as CloudCoverParameterValueEs[];

export const rainFallConditionsRus = [
    'Моросит',
    'Дождь',
    'Дождь со снегом',
    'Снег',
    'Снегопад',
    'Град / Гроза',
] as RainFallParameterValueRus[];
export const rainFallConditionsEng = [
    'Drizzle',
    'Rain',
    'Sleet',
    'Snow',
    'Snowfall',
    'Hail / Thunderstorm',
] as RainFallParameterValueEng[];
export const rainFallConditionsEs = [
    'Llovizna',
    'Lluvia',
    'Aguanieve',
    'Nieve',
    'Nevada',
    'Granizo / Tormenta',
] as RainFallParameterValueEs[];

export const windConditionsRus = [
    'Слабый (до 5,0 м/c)',
    'Умеренный (5,1 - 14,0 м/c)',
    'Сильный (14,1 - 24,0 м/c)',
    'Очень сильный (24,1 - 32,0 м/c)',
    'Ураганный (от 32,1 м/c)',
    'Задать значение вручную...',
] as WindParameterValueRus[];
export const windConditionsEng = [
    'Light (up to 5.0 m/s)',
    'Moderate (5.1 - 14.0 m/s)',
    'Strong (14.1 - 24.0 m/s)',
    'Very Strong (24.1 - 32.0 m/s)',
    'Hurricane (from 32.1 m/s)',
    'Custom...',
] as WindParameterValueEng[];
export const windConditionsEs = [
    'Ligero (hasta 5,0 m/s)',
    'Moderado (5.1 - 14.0 m/s)',
    'Fuerte (14.1 - 24.0 m/s)',
    'Muy fuerte (24.1 - 32.0 m/s)',
    'Huracán (desde 32.1 m/s)',
    'Personalizado...',
] as WindParameterValueEs[];

export const humidityConditionsRus = [
    'Сухой (до 50%)',
    'Нормальный (50% - 60%)',
    'Влажный (61% - 75%)',
    'Мокрый (от 76%)',
    'Задать значение вручную...',
] as HumidityParameterValueRus[];
export const humidityConditionsEng = [
    'Dry (up to 50%)',
    'Normal (50% - 60%)',
    'Moist (61% - 75%)',
    'Wet (from 76%)',
    'Custom...',
] as HumidityParameterValueEng[];
export const humidityConditionsEs = [
    'Seco (hasta el 50%)',
    'Normal (50% - 60%)',
    'Húmedo (61% - 75%)',
    'Mojado (from 76%)',
    'Personalizado...',
] as HumidityParameterValueEs[];
