import React from 'react';

import NewScenarioTab from './components/NewScenarioTab';
// import FavouritesTab from './components/FavouritesTab';
import { useTriggerScenarioSettingModal } from '../../hooks/useTriggerScenarioSettingModal';

import css from './style.m.css';

const TriggerScenarioSettingModal: React.FC = () => {
    const { rulesList, setRulesList, triggerTab, t } = useTriggerScenarioSettingModal();

    return (
        <div className={css.wrapModal}>
            <div className={css.modalTrigger}>
                <div className={css.wrapModalTrigger}>
                    <h3 className={css.modalTriggerItem}>{t('triggerScriptSettings')}</h3>
                    {/*<div className={css.wrapperTriggerContentTab}>*/}
                    {/*    <button*/}
                    {/*        className={clsx(css.btnNewScript, triggerTab === 'new_scenario' && css.btnActive)}*/}
                    {/*        onClick={chooseTriggerTab('new_scenario')}*/}
                    {/*    >*/}
                    {/*        <img alt="" />*/}
                    {/*        {t('newScript')}*/}
                    {/*    </button>*/}

                    {/*    <div className={css.fiedlBtnFavourites}>*/}
                    {/*        <button*/}
                    {/*            className={clsx(css.btnFavourites, triggerTab === 'favourites' && css.btnActive)}*/}
                    {/*            onClick={undefined}*/}
                    {/*        >*/}
                    {/*            <img alt="" />*/}
                    {/*            {t('btnFavorites')}*/}
                    {/*        </button>*/}
                    {/*        <span className={css.wrapperCreateTriggerStatus}>{t('soon')}</span>*/}
                    {/*    </div>*/}
                    {/*    <div>*/}
                    {/*        <button className={css.btnReadyTemplates}>*/}
                    {/*            <img alt="" />*/}
                    {/*            {t('btnTemplates')}*/}
                    {/*        </button>*/}
                    {/*        <span className={css.wrapperCreateTriggerStatus}>{t('soon')}</span>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    {triggerTab === 'new_scenario' && (
                        <NewScenarioTab
                            {...{
                                rulesList,
                                setRulesList,
                            }}
                        />
                    )}
                    {/*{triggerTab === 'favourites' && <FavouritesTab />}*/}
                </div>
            </div>
        </div>
    );
};

export default TriggerScenarioSettingModal;
