import React from 'react';
import { clsx } from 'clsx';

import { MenuItem, Select } from '../../../../../../../../ui-kit';
import { ScenarioRuleType, WeatherAndTrafficData } from '../../../../../../../../../../common/types';
import { useTrafficRow } from '../../../../hooks/useTrafficRow';

import css from '../../style.m.css';

type Props = {
    data?: WeatherAndTrafficData;
    updateRulesList: (data: Omit<ScenarioRuleType, 'id'>) => void;
};

const TrafficRow: React.FC<Props> = (props) => {
    const { parameterOperator, parameterOperators, onParameterOperatorSelect, parameterValue, onParameterValueChange } =
        useTrafficRow(props);

    return (
        <>
            <div className={clsx(css.wrapperTriggerBlock1, css.wrapperTriggerBlock2)}>
                <Select
                    style={{ margin: '0', background: '#fff', borderRadius: '6px' }}
                    width={120}
                    value={parameterOperator}
                    onChange={onParameterOperatorSelect}
                >
                    {parameterOperators.map((operator) => (
                        <MenuItem key={operator} value={operator}>
                            {operator}
                        </MenuItem>
                    ))}
                </Select>
            </div>
            <div className={css.wrapperTriggerBlock1}>
                <input
                    className={css.inputWeatherTraffic}
                    value={parameterValue}
                    onChange={onParameterValueChange}
                    maxLength={125}
                />
                <div className={css.wrapInputWeatherTraffic}>
                    <span className={css.inputWeatherTrafficItem}>баллов</span>
                </div>
            </div>
        </>
    );
};

export default TrafficRow;
