import React from 'react';
import { useTranslation } from 'react-i18next';

import ModalTemplate from '../../../../../../../../components/ModalTemplate';

import css from './style.m.css';

type Props = {
    isExitScenarioAlertOpen: boolean;
    onExitScenarioSettingSubmit: () => void;
    onExitScenarioSettingCancel: () => void;
};

const ExitTriggerScenarioSettingAlert: React.FC<Props> = ({
    isExitScenarioAlertOpen,
    onExitScenarioSettingSubmit,
    onExitScenarioSettingCancel,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    return (
        <ModalTemplate isOpen={isExitScenarioAlertOpen}>
            <div className={css.popupDeletePlaylist}>
                <h2>
                    {/* Выйти из режима настройки триггерного сценария */}
                    {t('modalScriptOne')}?
                </h2>
                <p>
                    {t('modalScriptTwo')}
                    <br />
                    {t('modalScriptThree')}
                    {/* Все настроенные правила будут удалены. Вы&nbsp;действительно&nbsp;хотите&nbsp;выйти? */}
                </p>
                <div className={css.fieldButtons}>
                    <a className={css.btnCancel}>
                        <button className={css.buttonCancel} onClick={onExitScenarioSettingCancel}>
                            {t('btnCancel')}
                            {/* Отменить */}
                        </button>
                    </a>
                    <button className={css.buttonDelete} onClick={onExitScenarioSettingSubmit} disabled={false}>
                        <img alt="" />
                        {t('btnExit')}
                        {/* Выйти */}
                    </button>
                </div>
            </div>
        </ModalTemplate>
    );
};

export default ExitTriggerScenarioSettingAlert;
