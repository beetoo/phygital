import React from 'react';
import { TextField, InputAdornment } from '@mui/material';
import { PatternFormat } from 'react-number-format';

import { useTimeRow } from '../../../../hooks/useTimeRow';
import { ScenarioRuleType, TimeData } from '../../../../../../../../../../common/types';
import { isTimeAllowed } from '../../../../../../../../helpers';

import css from '../../style.m.css';

type Props = {
    data?: TimeData;
    updateRulesList: (data: Omit<ScenarioRuleType, 'id'>) => void;
};

const TimeRow: React.FC<Props> = (props) => {
    const {
        initialDay,
        startDay,
        endDay,
        onStartDateChange,
        onEndDateChange,

        dayValue,
        onByDayChange,

        startTimeValue,
        endTimeValue,
        onStartTimeChange,
        onEndTimeChange,

        t,
    } = useTimeRow(props);

    return (
        <div style={{ display: 'flex' }}>
            <div>
                <div className={css.wrapperTriggerBlock1}>
                    <TextField
                        type="date"
                        size="small"
                        sx={{ width: 260, marginBottom: '12px' }}
                        value={startDay}
                        onChange={onStartDateChange}
                        inputProps={{ min: initialDay }}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    {t('from')}
                                    {/* с */}
                                </InputAdornment>
                            ),
                        }}
                    />
                </div>
                {/*<div className={css.wrapperTriggerBlock1}>*/}
                {/*    <Select*/}
                {/*        style={{*/}
                {/*            margin: '0',*/}
                {/*            background: '#fff',*/}
                {/*            borderRadius: '6px',*/}
                {/*        }}*/}
                {/*        placeholder={t('noRepeat') as string}*/}
                {/*        width={260}*/}
                {/*        value={frequencyValue}*/}
                {/*        onChange={onTimeFrequencySelect}*/}
                {/*        disabled={isFrequencySelectDisabled}*/}
                {/*    >*/}
                {/*        {frequencyValues.map((frequency) => (*/}
                {/*            <MenuItem key={frequency} value={frequency}>*/}
                {/*                {frequency}*/}
                {/*            </MenuItem>*/}
                {/*        ))}*/}
                {/*    </Select>*/}
                {/*</div>*/}
                <div className={css.wrapInputTriggerTime}>
                    <div>
                        <label />
                        <PatternFormat
                            className={css.inputTriggerTime}
                            format="##:##"
                            placeholder="00:00"
                            isAllowed={isTimeAllowed}
                            value={startTimeValue}
                            onChange={onStartTimeChange}
                        />
                    </div>
                    <div>
                        <label />
                        <PatternFormat
                            className={css.inputTriggerTime}
                            format="##:##"
                            placeholder="00:00"
                            isAllowed={isTimeAllowed}
                            value={endTimeValue}
                            onChange={onEndTimeChange}
                        />
                    </div>
                </div>
            </div>
            <div>
                <div className={css.wrapperTriggerBlock1}>
                    <TextField
                        type="date"
                        size="small"
                        sx={{ width: 260 }}
                        value={endDay}
                        onChange={onEndDateChange}
                        inputProps={{ min: initialDay }}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    {t('to')}
                                    {/* по */}
                                </InputAdornment>
                            ),
                        }}
                    />
                </div>
                <div className={css.triggerSetupWeek}>
                    <div className={css.triggerSetupDay}>
                        <p className={css.triggerSetupDayActive}>
                            {t('dayMo')}
                            {/* Пн */}
                        </p>
                        <span
                            className={dayValue('MO') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                            onClick={onByDayChange('MO')}
                        />
                    </div>
                    <div className={css.triggerSetupDay}>
                        <p className={css.triggerSetupDayActive}>
                            {t('dayTu')}
                            {/* Вт */}
                        </p>
                        <span
                            className={dayValue('TU') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                            onClick={onByDayChange('TU')}
                        />
                    </div>
                    <div className={css.triggerSetupDay}>
                        <p className={css.triggerSetupDayActive}>
                            {t('dayWe')}
                            {/* Ср */}
                        </p>
                        <span
                            className={dayValue('WE') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                            onClick={onByDayChange('WE')}
                        />
                    </div>
                    <div className={css.triggerSetupDay}>
                        <p className={css.triggerSetupDayActive}>
                            {t('dayTh')}
                            {/* Чт */}
                        </p>
                        <span
                            className={dayValue('TH') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                            onClick={onByDayChange('TH')}
                        />
                    </div>
                    <div className={css.triggerSetupDay}>
                        <p className={css.triggerSetupDayActive}>
                            {t('dayFr')}
                            {/* Пт */}
                        </p>
                        <span
                            className={dayValue('FR') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                            onClick={onByDayChange('FR')}
                        />
                    </div>
                    <div className={css.triggerSetupDay}>
                        <p className={css.triggerSetupDayActive}>
                            {t('daySa')}
                            {/* Сб */}
                        </p>
                        <span
                            className={dayValue('SA') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                            onClick={onByDayChange('SA')}
                        />
                    </div>
                    <div className={css.triggerSetupDay}>
                        <p className={css.triggerSetupDayActive}>
                            {t('daySu')}
                            {/* Вс */}
                        </p>
                        <span
                            className={dayValue('SU') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                            onClick={onByDayChange('SU')}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default TimeRow;
