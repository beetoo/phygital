import React, { Dispatch, SetStateAction } from 'react';
import { clsx } from 'clsx';

import { Tooltip, MenuItem, Select } from '../../../../../../../../ui-kit';
import { useNewScenarioTab } from '../../../../hooks/useNewScenarioTab';
import { tCondition } from '../../helpers';

import WeatherRow from '../WeatherRow';
import TrafficRow from '../TrafficRow';
import TimeRow from '../TimeRow';
import ExitTriggerScenarioSettingAlert from '../ExitTriggerScenarioSettingAlert';
import DeleteAllScenarioRulesAlert from '../DeleteAllScenarioRulesAlert';
import { ScenarioRuleType, TimeData, WeatherAndTrafficData } from '../../../../../../../../../../common/types';

import css from '../../style.m.css';

type Props = {
    rulesList: ScenarioRuleType[];
    setRulesList: Dispatch<SetStateAction<ScenarioRuleType[]>>;
};

const NewScenarioTab: React.FC<Props> = ({ rulesList, setRulesList }) => {
    const {
        scenarioName,
        scenarioDescription,
        onScenarioNameChange,
        onScenarioDescriptionChange,

        updateRulesList,
        addNewRule,
        isAddNewRuleDisabled,
        deleteRule,

        ruleConditions,

        onRuleConditionSelect,
        isRuleConditionDisabled,
        rowIdsWithError,
        setRowIdsWithError,
        isSaveButtonDisabled,
        saveScenario,

        isDeleteRulesAlertOpen,
        openDeleteRulesAlert,
        onDeleteRulesSubmit,
        onDeleteRulesCancel,

        isExitScenarioAlertOpen,
        openExitScenarioSettingAlert,
        onExitScenarioSettingSubmit,
        onExitScenarioSettingCancel,

        t,
    } = useNewScenarioTab({ rulesList, setRulesList });

    return (
        <div className={css.conteinerScriptModal}>
            <div className={css.conteinerScriptModalInner}>
                <ExitTriggerScenarioSettingAlert
                    {...{
                        isExitScenarioAlertOpen,
                        onExitScenarioSettingSubmit,
                        onExitScenarioSettingCancel,
                    }}
                />
                <DeleteAllScenarioRulesAlert
                    {...{ isDeleteRulesAlertOpen, onDeleteRulesSubmit, onDeleteRulesCancel }}
                />

                <div className={css.wrapperInfoAboutScript}>
                    <div className={css.wrapperNameScript}>
                        <label>{t('newScriptName')}</label>

                        <input
                            className={css.nameScriptInput}
                            placeholder={t('newScript') as string}
                            value={scenarioName}
                            onChange={onScenarioNameChange}
                            maxLength={125}
                        />
                    </div>
                    <div className={css.wrapperDescriptionScript}>
                        <label>{t('scriptDescription')}</label>

                        <input
                            className={css.descriptionScriptInput}
                            placeholder={t('scriptDescriptionInput') as string}
                            value={scenarioDescription}
                            onChange={onScenarioDescriptionChange}
                            maxLength={125}
                        />
                    </div>
                    {/*<div className={css.wrapperBtnHeart}>*/}
                    {/*    <Tooltip title={t('addScriptToFavorites')} placement="top">*/}
                    {/*        <button className={css.btnHeart} />*/}
                    {/*    </Tooltip>*/}
                    {/*</div>*/}
                </div>
                <div>
                    <p className={css.modalTriggerItem1}>{t('rules')}</p>
                </div>
                <div className={css.wrapperCreateTriggerAll}>
                    {rulesList.map(({ id, condition, data }) => {
                        const ruleCondition = tCondition(condition, t);

                        return (
                            <div
                                key={id}
                                className={clsx(
                                    css.wrapperTriggerAll,
                                    rowIdsWithError.includes(id) && css.wrapperTriggerError,
                                    ruleCondition === t('time') && css.time,
                                )}
                            >
                                <div className={css.wrapperTriggerBlock}>
                                    <div className={css.wrapperTriggerBlock1}>
                                        <Select
                                            style={{
                                                margin: '0',
                                                background: '#fff',
                                                borderRadius: '6px',
                                            }}
                                            placeholder={t('selectOption') as string}
                                            width={292}
                                            value={ruleCondition}
                                            onChange={onRuleConditionSelect(id)}
                                        >
                                            {ruleConditions.map((condition) => (
                                                <MenuItem
                                                    key={condition}
                                                    value={condition}
                                                    disabled={isRuleConditionDisabled(condition)}
                                                >
                                                    {condition}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </div>
                                    {ruleCondition === t('weather') && (
                                        <WeatherRow
                                            weatherRowId={id}
                                            rulesList={rulesList}
                                            data={data as WeatherAndTrafficData}
                                            updateRulesList={updateRulesList(id)}
                                            setRowIdsWithError={setRowIdsWithError}
                                        />
                                    )}
                                    {ruleCondition === t('traffic') && (
                                        <TrafficRow
                                            data={data as WeatherAndTrafficData}
                                            updateRulesList={updateRulesList(id)}
                                        />
                                    )}
                                    {ruleCondition === t('time') && (
                                        <TimeRow data={data as TimeData} updateRulesList={updateRulesList(id)} />
                                    )}
                                </div>

                                <div className={css.wrapperIconDelete}>
                                    <Tooltip title={t('btnDeleteRule')} placement="top">
                                        <button className={css.iconDelete} onClick={deleteRule(id)}></button>
                                    </Tooltip>
                                </div>
                            </div>
                        );
                    })}
                </div>

                <div className={css.wrapBtnRules}>
                    <button className={css.btnAddRules} onClick={addNewRule} disabled={isAddNewRuleDisabled}>
                        <img alt="" />
                        {t('btnAddNewRule')}
                    </button>
                    {rulesList.length > 0 && (
                        <button className={css.btnClearAllRules} onClick={openDeleteRulesAlert}>
                            <img alt="" />
                            {t('btnDeleteAllRules')}
                        </button>
                    )}
                </div>
            </div>

            <div className={css.wrapperBtnSaveCancel}>
                {rowIdsWithError.length > 0 && (
                    <div>
                        <p className={css.errorRules}>{t('textRulesError')}</p>
                    </div>
                )}
                <button className={css.btnCancel} onClick={openExitScenarioSettingAlert}>
                    {t('btnExit')}
                </button>
                <button className={css.btnSave} onClick={saveScenario} disabled={isSaveButtonDisabled}>
                    {t('btnSave')}
                </button>
            </div>
        </div>
    );
};

export default NewScenarioTab;
