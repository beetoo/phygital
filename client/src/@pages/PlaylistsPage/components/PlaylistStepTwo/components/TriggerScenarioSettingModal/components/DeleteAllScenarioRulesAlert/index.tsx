import React from 'react';
import { useTranslation } from 'react-i18next';

import ModalTemplate from '../../../../../../../../components/ModalTemplate';

import css from './style.m.css';

type Props = {
    isDeleteRulesAlertOpen: boolean;
    onDeleteRulesSubmit: () => void;
    onDeleteRulesCancel: () => void;
};

const DeleteAllScenarioRulesAlert: React.FC<Props> = ({
    isDeleteRulesAlertOpen,
    onDeleteRulesSubmit,
    onDeleteRulesCancel,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    return (
        <ModalTemplate isOpen={isDeleteRulesAlertOpen}>
            <div className={css.popupDeletePlaylist}>
                <h2>{t('btnDeleteAllRules')}</h2>
                <p>
                    {t('textDelRulesOne')}
                    <br />
                    {t('textDelRulesTwo')}
                </p>
                <div className={css.fieldButtons}>
                    <a className={css.btnCancel}>
                        <button className={css.buttonCancel} onClick={onDeleteRulesCancel}>
                            {t('btnCancel')}
                        </button>
                    </a>
                    <button className={css.buttonDelete} onClick={onDeleteRulesSubmit}>
                        <img alt="" />
                        {t('btnDelete')}
                    </button>
                </div>
            </div>
        </ModalTemplate>
    );
};

export default DeleteAllScenarioRulesAlert;
