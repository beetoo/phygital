import React from 'react';
import { useTranslation } from 'react-i18next';
import { clsx } from 'clsx';

import { MenuItem, Select, AddIcon } from '../../../../../../../../ui-kit';

import css from '../../style.m.css';

const FavouritesTab: React.FC = () => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    return (
        <div>
            <div className={css.containerScripts}>
                <div>
                    <div className={css.wrapperFavourites}>
                        <div className={css.wrapperFavouritesScript}>
                            <div className={css.favouritesScriptBlock}>
                                <div>
                                    <p className={css.favouritesScriptItem}>Новый сценарий</p>
                                    <p className={css.favouritesScriptItem1}>
                                        Вот такое описание нового сценария мы тут видим сейчас вот да..........
                                    </p>
                                    <span className={css.iconDown} />
                                </div>
                                <div className={css.fieldBtnDeleteScript}>
                                    <button className={css.btnDeleteScript} />
                                </div>
                            </div>
                            <div className={css.wrapperTabFavouritesAll}>
                                <div className={clsx(css.wrapperTriggerAll, css.wrapperTabFavourites)}>
                                    <div className={css.wrapperTriggerBlock}>
                                        <div className={css.wrapperTriggerBlock1}>
                                            <Select
                                                style={{
                                                    margin: '0',
                                                    background: '#fff',
                                                    borderRadius: '6px',
                                                }}
                                                placeholder={t('selectOption')}
                                                width={292}
                                                value={''}
                                            >
                                                <MenuItem value={''}> </MenuItem>
                                            </Select>
                                        </div>
                                        <div className={css.wrapperTriggerBlock1}>
                                            <Select
                                                style={{
                                                    margin: '0',
                                                    background: '#fff',
                                                    borderRadius: '6px',
                                                }}
                                                placeholder={t('selectOption')}
                                                width={231}
                                                value={''}
                                            >
                                                <MenuItem value={''}> </MenuItem>
                                            </Select>
                                        </div>
                                        <div className={clsx(css.wrapperTriggerBlock1, css.wrapperTriggerBlock2)}>
                                            <Select
                                                style={{ margin: '0', background: '#fff', borderRadius: '6px' }}
                                                placeholder={t('selectValue')}
                                                width={120}
                                                value={''}
                                            >
                                                <MenuItem value={''} />
                                            </Select>
                                        </div>
                                        <div className={css.wrapperTriggerBlock1}>
                                            <input className={css.inputWeatherTraffic} maxLength={125} />
                                            <div className={css.wrapInputWeatherTraffic}>
                                                <span className={css.inputWeatherTrafficItem}>℃</span>
                                            </div>
                                        </div>

                                        <span className={css.iconDelete}>
                                            <span className={css.tooltiptext}>Удалить правило</span>
                                        </span>
                                    </div>
                                </div>
                                <div className={css.wrapBtnRules}>
                                    <button className={css.btnAddRules}>
                                        <AddIcon />
                                        Добавить новое правило
                                    </button>
                                    <button className={css.btnSaveChange}>Сохранить изменения</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={clsx(css.wrapperFavourites, css.active)}>
                        <div className={css.favouritesScriptBlock}>
                            <div>
                                <p className={css.favouritesScriptItem}>Новый сценарий</p>
                                <p className={css.favouritesScriptItem1}>
                                    Вот такое описание нового сценария мы тут видим сейчас вот да..........
                                </p>
                                <span className={css.iconDown} />
                            </div>
                            <div className={css.fieldBtnDeleteScript}>
                                <button className={css.btnDeleteScript} />
                            </div>
                        </div>
                        <div className={css.wrapperTabFavouritesAll}>
                            <div className={clsx(css.wrapperTriggerAll, css.wrapperTabFavourites)}>
                                <div className={css.wrapperTriggerBlock}>
                                    <div className={css.wrapperTriggerBlock1}>
                                        <Select
                                            style={{
                                                margin: '0',
                                                background: '#fff',
                                                borderRadius: '6px',
                                            }}
                                            placeholder={t('selectOption')}
                                            width={292}
                                            value={''}
                                        >
                                            <MenuItem value={''}> </MenuItem>
                                        </Select>
                                    </div>
                                    <div className={clsx(css.wrapperTriggerBlock1, css.wrapperTriggerBlock2)}>
                                        <Select
                                            style={{ margin: '0', background: '#fff', borderRadius: '6px' }}
                                            placeholder={t('selectValue')}
                                            width={120}
                                            value={''}
                                        >
                                            <MenuItem value={''} />
                                        </Select>
                                    </div>
                                    <div className={css.wrapperTriggerBlock1}>
                                        <input className={css.inputWeatherTraffic} maxLength={125} />
                                        <div className={css.wrapInputWeatherTraffic}>
                                            <span className={css.inputWeatherTrafficItem}>баллов</span>
                                        </div>
                                    </div>

                                    <span className={css.iconDelete}>
                                        <span className={css.tooltiptext}>Удалить правило</span>
                                    </span>
                                </div>
                            </div>
                            <div className={css.wrapBtnRules}>
                                <button className={css.btnAddRules}>
                                    <AddIcon />
                                    Добавить новое правило
                                </button>
                                <button className={css.btnSaveChange}>Сохранить изменения</button>
                            </div>
                        </div>
                    </div>
                    <div className={css.wrapperFavourites}>
                        <div className={css.wrapperFavouritesScript}>
                            <div className={css.favouritesScriptBlock}>
                                <div>
                                    <p className={css.favouritesScriptItem}>Новый сценарий</p>
                                    <p className={css.favouritesScriptItem1}>
                                        Вот такое описание нового сценария мы тут видим сейчас вот да..........
                                    </p>
                                    <span className={css.iconDown} />
                                </div>
                                <div className={css.fieldBtnDeleteScript}>
                                    <button className={css.btnDeleteScript} />
                                </div>
                            </div>
                            <div className={css.wrapperTabFavouritesAll}>
                                <div
                                    className={clsx(
                                        css.wrapperTriggerAll,
                                        css.wrapperTabFavourites,
                                        css.multipleScenarios,
                                    )}
                                >
                                    <div className={css.wrapperTriggerBlock}>
                                        <div className={css.wrapperTriggerBlock1}>
                                            <Select
                                                style={{
                                                    margin: '0',
                                                    background: '#fff',
                                                    borderRadius: '6px',
                                                }}
                                                placeholder={t('selectOption')}
                                                width={292}
                                                value={''}
                                            >
                                                <MenuItem value={''}> </MenuItem>
                                            </Select>
                                        </div>
                                        <div className={css.wrapperTriggerBlock1}>
                                            <Select
                                                style={{
                                                    margin: '0',
                                                    background: '#fff',
                                                    borderRadius: '6px',
                                                }}
                                                placeholder={t('selectOption')}
                                                width={231}
                                                value={''}
                                            >
                                                <MenuItem value={''}> </MenuItem>
                                            </Select>
                                        </div>
                                        <div className={clsx(css.wrapperTriggerBlock1, css.wrapperTriggerBlock2)}>
                                            <Select
                                                style={{ margin: '0', background: '#fff', borderRadius: '6px' }}
                                                placeholder={t('selectValue')}
                                                width={120}
                                                value={''}
                                            >
                                                <MenuItem value={''} />
                                            </Select>
                                        </div>
                                        <div className={css.wrapperTriggerBlock1}>
                                            <input className={css.inputWeatherTraffic} maxLength={125} />
                                            <div className={css.wrapInputWeatherTraffic}>
                                                <span className={css.inputWeatherTrafficItem}>℃</span>
                                            </div>
                                        </div>

                                        <span className={css.iconDelete}>
                                            <span className={css.tooltiptext}>Удалить правило</span>
                                        </span>
                                    </div>
                                </div>
                                <div
                                    className={clsx(
                                        css.wrapperTriggerAll,
                                        css.wrapperTabFavourites,
                                        css.multipleScenarios,
                                    )}
                                >
                                    <div className={css.wrapperTriggerBlock}>
                                        <div className={css.wrapperTriggerBlock1}>
                                            <Select
                                                style={{
                                                    margin: '0',
                                                    background: '#fff',
                                                    borderRadius: '6px',
                                                }}
                                                placeholder={t('selectOption')}
                                                width={292}
                                                value={''}
                                            >
                                                <MenuItem value={''}> </MenuItem>
                                            </Select>
                                        </div>
                                        <div className={css.wrapperTriggerBlock1}>
                                            <Select
                                                style={{
                                                    margin: '0',
                                                    background: '#fff',
                                                    borderRadius: '6px',
                                                }}
                                                placeholder={t('selectOption')}
                                                width={231}
                                                value={''}
                                            >
                                                <MenuItem value={''}> </MenuItem>
                                            </Select>
                                        </div>
                                        <div className={clsx(css.wrapperTriggerBlock1, css.wrapperTriggerBlock2)}>
                                            <Select
                                                style={{ margin: '0', background: '#fff', borderRadius: '6px' }}
                                                placeholder={t('selectValue')}
                                                width={120}
                                                value={''}
                                            >
                                                <MenuItem value={''} />
                                            </Select>
                                        </div>
                                        <div className={css.wrapperTriggerBlock1}>
                                            <input className={css.inputWeatherTraffic} maxLength={125} />
                                            <div className={css.wrapInputWeatherTraffic}>
                                                <span className={css.inputWeatherTrafficItem}>℃</span>
                                            </div>
                                        </div>

                                        <span className={css.iconDelete}>
                                            <span className={css.tooltiptext}>Удалить правило</span>
                                        </span>
                                    </div>
                                </div>
                                <div
                                    className={clsx(
                                        css.wrapperTriggerAll,
                                        css.wrapperTabFavourites,
                                        css.multipleScenarios,
                                    )}
                                >
                                    <div className={css.wrapperTriggerBlock}>
                                        <div className={css.wrapperTriggerBlock1}>
                                            <Select
                                                style={{
                                                    margin: '0',
                                                    background: '#fff',
                                                    borderRadius: '6px',
                                                }}
                                                placeholder={t('selectOption')}
                                                width={292}
                                                value={''}
                                            >
                                                <MenuItem value={''}> </MenuItem>
                                            </Select>
                                        </div>
                                        <div className={css.wrapperTriggerBlock1}>
                                            <Select
                                                style={{
                                                    margin: '0',
                                                    background: '#fff',
                                                    borderRadius: '6px',
                                                }}
                                                placeholder={t('selectOption')}
                                                width={231}
                                                value={''}
                                            >
                                                <MenuItem value={''}> </MenuItem>
                                            </Select>
                                        </div>
                                        <div className={clsx(css.wrapperTriggerBlock1, css.wrapperTriggerBlock2)}>
                                            <Select
                                                style={{ margin: '0', background: '#fff', borderRadius: '6px' }}
                                                placeholder={t('selectValue')}
                                                width={120}
                                                value={''}
                                            >
                                                <MenuItem value={''} />
                                            </Select>
                                        </div>
                                        <div className={css.wrapperTriggerBlock1}>
                                            <input className={css.inputWeatherTraffic} maxLength={125} />
                                            <div className={css.wrapInputWeatherTraffic}>
                                                <span className={css.inputWeatherTrafficItem}>℃</span>
                                            </div>
                                        </div>

                                        <span className={css.iconDelete}>
                                            <span className={css.tooltiptext}>Удалить правило</span>
                                        </span>
                                    </div>
                                </div>
                                <div className={css.wrapBtnRules}>
                                    <button className={css.btnAddRules}>
                                        <AddIcon />
                                        Добавить новое правило
                                    </button>
                                    <button className={css.btnSaveChange}>Сохранить изменения</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={css.wrapperBtnSaveCancel}>
                <div>
                    <p className={css.errorRules}>
                        Правила противоречат друг другу или не могут быть выполнены одновременно
                    </p>
                </div>
                <button className={css.btnSave} disabled={false}>
                    Выбрать
                </button>
                <button className={css.btnCancel}>Отмена</button>
            </div>
        </div>
    );
};

export default FavouritesTab;
