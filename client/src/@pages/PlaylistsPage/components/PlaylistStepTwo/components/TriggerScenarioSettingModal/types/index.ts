// условия
export type RuleConditionRus = 'Погода' | 'Пробки' | 'Время';
export type RuleConditionEng = 'Weather' | 'Traffic' | 'Time';
export type RuleConditionEs = 'Clima' | 'Tráfico' | 'Tiempo';
export type RuleCondition = RuleConditionRus | RuleConditionEng | RuleConditionEs;

// параметры
export type ConditionParameterRus =
    | 'Температура'
    | 'Температура воды'
    | 'Облачность'
    | 'Осадки'
    | 'Ветер'
    | 'Атмосферное давление'
    | 'Влажность воздуха';

export type ConditionParameterEng =
    | 'Temperature'
    | 'Water Temperature'
    | 'Cloudiness'
    | 'Precipitation'
    | 'Wind'
    | 'Atmospheric Pressure'
    | 'Air Humidity';

export type ConditionParameterEs =
    | 'Temperatura'
    | 'Temperatura de Agua'
    | 'Nubosidad'
    | 'Precipitación'
    | 'Viento'
    | 'Presión atmosférica'
    | 'Humedad del aire';

export type ConditionParameter = ConditionParameterRus | ConditionParameterEng | ConditionParameterEs;

// операторы

export type ParameterOperatorRus = 'Меньше' | 'Больше' | 'Равно';
export type ParameterOperatorEng = 'Less' | 'More' | 'Equal';
export type ParameterOperatorEs = 'Menos' | 'Más' | 'Igual';
export type ParameterOperator = ParameterOperatorRus | ParameterOperatorEng | ParameterOperatorEs;

// условия

export type CloudCoverParameterValueRus = 'Пасмурно' | 'Облачно' | 'Ясно';
export type CloudCoverParameterValueEng = 'Overcast' | 'Cloudy' | 'Clearly';
export type CloudCoverParameterValueEs = 'Encapotado' | 'Nublado' | 'Claramente';
export type CloudCoverParameterValue =
    | CloudCoverParameterValueRus
    | CloudCoverParameterValueEng
    | CloudCoverParameterValueEs;

export type RainFallParameterValueRus = 'Моросит' | 'Дождь' | 'Дождь со снегом' | 'Снег' | 'Снегопад' | 'Град / Гроза';
export type RainFallParameterValueEng = 'Drizzle' | 'Rain' | 'Sleet' | 'Snow' | 'Snowfall' | 'Hail / Thunderstorm';
export type RainFallParameterValueEs = 'Llovizna' | 'Lluvia' | 'Aguanieve' | 'Nieve' | 'Nevada' | 'Granizo / Tormenta';
export type RainFallParameterValue = RainFallParameterValueRus | RainFallParameterValueEng | RainFallParameterValueEs;

export type WindParameterValueRus =
    | 'Слабый (до 5,0 м/c)'
    | 'Умеренный (5,1 - 14,0 м/c)'
    | 'Сильный (14,1 - 24,0 м/c)'
    | 'Очень сильный (24,1 - 32,0 м/c)'
    | 'Ураганный (от 32,1 м/c)'
    | 'Задать значение вручную...';

export type WindParameterValueEng =
    | 'Light (up to 5.0 m/s)'
    | 'Moderate (5.1 - 14.0 m/s)'
    | 'Strong (14.1 - 24.0 m/s)'
    | 'Very Strong (24.1 - 32.0 m/s)'
    | 'Hurricane (from 32.1 m/s)'
    | 'Custom...';

export type WindParameterValueEs =
    | 'Ligero (hasta 5,0 m/s)'
    | 'Moderado (5.1 - 14.0 m/s)'
    | 'Fuerte (14.1 - 24.0 m/s)'
    | 'Muy fuerte (24.1 - 32.0 m/s)'
    | 'Huracán (desde 32.1 m/s)'
    | 'Personalizado...';

export type WindParameterValue = WindParameterValueRus | WindParameterValueEng | WindParameterValueEs;

export type HumidityParameterValueRus =
    | 'Сухой (до 50%)'
    | 'Нормальный (50% - 60%)'
    | 'Влажный (61% - 75%)'
    | 'Мокрый (от 76%)'
    | 'Задать значение вручную...';

export type HumidityParameterValueEng =
    | 'Dry (up to 50%)'
    | 'Normal (50% - 60%)'
    | 'Moist (61% - 75%)'
    | 'Wet (from 76%)'
    | 'Custom...';

export type HumidityParameterValueEs =
    | 'Seco (hasta el 50%)'
    | 'Normal (50% - 60%)'
    | 'Húmedo (61% - 75%)'
    | 'Mojado (from 76%)'
    | 'Personalizado...';

export type HumidityParameterValue = HumidityParameterValueRus | HumidityParameterValueEng | HumidityParameterValueEs;

export type TrafficParameterValue = string;

export type ParameterValue =
    | CloudCoverParameterValue
    | RainFallParameterValue
    | WindParameterValue
    | HumidityParameterValue
    | TrafficParameterValue;

// правила

export type ScenarioCondition = 'weather' | 'traffic' | 'time';
export type WeatherParameter =
    | 'temperature'
    | 'water_temperature'
    | 'cloud_cover'
    | 'rain_fall'
    | 'wind'
    | 'atmospheric_pressure'
    | 'humidity';
export type WeatherAndTrafficOperator = 'more' | 'less' | 'equally';
export type WeatherAndTrafficValue =
    | 'overcast'
    | 'cloudy'
    | 'clear'
    | 'drizzle'
    | 'rain'
    | 'wet-snow'
    | 'snow'
    | 'snow-showers'
    | 'thunderstorm'
    | '[0,5]'
    | '[5.1,14]'
    | '[14.1,24]'
    | '[24.1,32]'
    | '[32.1,150]'
    | '[0,50]'
    | '[50,60]'
    | '[61,75]'
    | '[76,100]'
    | string;
