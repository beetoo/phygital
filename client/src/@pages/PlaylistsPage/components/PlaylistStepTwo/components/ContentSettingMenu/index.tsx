import React, { memo } from 'react';
import { PatternFormat } from 'react-number-format';
import { clsx } from 'clsx';

import ImgOrVideo from '../../../../../../components/ImgOrVideo';
import { Tooltip } from '../../../../../../ui-kit';
import { useContentSettingMenu } from '../../hooks/useContentSettingMenu';
import { isTimeAllowed } from '../../../../../../helpers';

import css from '../../style.m.css';

const ContentSettingMenu: React.FC = () => {
    const {
        playlistItemSrc,
        playlistItemName,
        playlistItemFileTypeAndExtension,
        playlistItemFileSize,
        playlistItemDimensions,
        playlistItemOrientation,
        isVideoType,
        isImageType,
        isWebType,

        playlistItemDuration,
        onPlaylistItemDurationChange,
        placeIndex,
        onPlaylistItemPlaceIndexChange,
        deletePlaylistItem,
        showTriggerContentSettingModal,

        isContentInfoShown,
        showContentInfo,
        showContentViewModal,
        t,
    } = useContentSettingMenu();

    return (
        <div className={css.wrapMainRightMenuSetup124}>
            <h2>{t('contentSettings')}</h2>
            <div className={css.wrapperImgVideo}>
                {isVideoType ? (
                    <div className={css.wrapperVideo}>
                        <ImgOrVideo src={playlistItemSrc} width="292" height="164" />
                    </div>
                ) : isImageType ? (
                    <ImgOrVideo src={playlistItemSrc} width="292" height="164" />
                ) : isWebType ? (
                    <iframe src={playlistItemSrc} width="292" height="164" />
                ) : null}
                <div className={css.wrapperIconFullScreen}>
                    <Tooltip title={t('fullScreen')} placement="top-end">
                        <button
                            className={css.iconFullScreen}
                            onClick={(event) => {
                                event.stopPropagation();
                                showContentViewModal();
                            }}
                        />
                    </Tooltip>
                </div>
            </div>

            <div className={css.wrapMainRightMenuInfoAll}>
                <h3>{playlistItemName}</h3>
                <span
                    className={isContentInfoShown ? css.iconArrowInfoUp : css.iconArrowInfoDown}
                    onClick={showContentInfo}
                />
                {isContentInfoShown && (
                    <div className={css.mainRightMenuInfoAll}>
                        <ul>
                            <li>
                                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItemOne)}>
                                    {t('type')}
                                    <span>{playlistItemFileTypeAndExtension}</span>
                                </p>
                            </li>
                            <li>
                                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItemOne)}>
                                    {t('size')}
                                    <span>{playlistItemFileSize}</span>
                                </p>
                            </li>
                            <li>
                                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItemOne)}>
                                    {t('resolutionR')}
                                    <span>{playlistItemDimensions}</span>
                                </p>
                            </li>
                            <li>
                                <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItemOne)}>
                                    {t('orientationR')}
                                    <span>{playlistItemOrientation}</span>
                                </p>
                            </li>
                        </ul>
                    </div>
                )}
            </div>

            <button className={css.btnCreateTrigger} onClick={showTriggerContentSettingModal}>
                <img alt="" />
                {t('btnCreateTriggerScript')}
            </button>

            <div className={css.wrapperDisplayDuration}>
                <label>{t('showDuration')}</label>
                <br />
                <PatternFormat
                    className={css.rightMenuNameContent}
                    format="##:##:##"
                    placeholder="00:00:00"
                    isAllowed={isTimeAllowed}
                    value={playlistItemDuration}
                    onChange={onPlaylistItemDurationChange}
                    disabled={isVideoType}
                />
                {isVideoType && <div className={css.wrapNoChange}>{t('textNine')}</div>}
            </div>

            <div className={css.wrapperPlacePlaylist}>
                <label>{t('placeContentPlaylist')}</label>
                <br />
                <input
                    className={css.rightMenuNameContent}
                    placeholder={t('numberPlacePlaylist') as string}
                    value={placeIndex}
                    onChange={onPlaylistItemPlaceIndexChange}
                    maxLength={125}
                />
            </div>

            <button className={css.btnDeleteContent} onClick={deletePlaylistItem}>
                <img alt="" />
                {t('deleteContentPlaylist')}
            </button>
        </div>
    );
};

export default memo(ContentSettingMenu);
