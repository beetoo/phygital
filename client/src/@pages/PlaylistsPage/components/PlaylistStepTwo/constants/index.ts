export const SLIDER_STEP_VALUE = 0.1;

export enum TRIGGER_SELECT_RESOLUTIONS {
    FULL_HD = 'Full HD (1920 × 1080 / 16:9)',
    FULL_HD_VERTICAL = 'Full HD (1080 × 1920 / 16:9)',
    HD = 'HD (1280 × 720 / 16:9)',
    HD_VERTICAL = 'HD (720 × 1280 / 16:9)',
    UHD = 'UHD (3840 × 2160 / 16:9)',
    UHD_VERTICAL = 'UHD (2160 × 3840 / 16:9)',
}
