import React, { memo } from 'react';
import { DragDropContext } from '@hello-pangea/dnd';

import StepTwoPlaylistItemsList from './components/StepTwoPlaylistItemsList';
import StepTwoMenuContainer from './components/StepTwoMenuContainer';
import { usePlaylistStepTwo } from './hooks/usePlaylistStepTwo';

import css from './style.m.css';

const PlaylistStepTwo: React.FC = () => {
    const { onDragEnd, onStepTwoAreaClick } = usePlaylistStepTwo();

    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <div className={css.createPlaylistCenterBlock} onClick={onStepTwoAreaClick}>
                <StepTwoPlaylistItemsList />
            </div>
            <StepTwoMenuContainer />
        </DragDropContext>
    );
};

export default memo(PlaylistStepTwo);
