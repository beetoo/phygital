import { TFunction } from 'i18next';

import { TriggerOrientationSelectType, TriggerResolutionSelectType } from '../../../types';
import { TRIGGER_SELECT_RESOLUTIONS } from '../constants';
import { getAspectRatioString } from '../../../../../helpers';
import { OrientationEnum } from '../../../../../../../common/types';

const getCustomWidthAndHeight = (value: string) => {
    const match = value.match(/Custom \((\d+) × (\d+) .+/);

    if (match) {
        const width = parseInt(match[0]);
        const height = parseInt(match[2]);

        if (!isNaN(width) && !isNaN(height)) {
            return { width, height };
        }
    }

    return { width: 1920, height: 1080 };
};

export const triggerResolutionTypeToResolutionValue = (
    resolution: TriggerResolutionSelectType | string,
): { width: number; height: number } => {
    switch (resolution || true) {
        case TRIGGER_SELECT_RESOLUTIONS.FULL_HD:
        case TRIGGER_SELECT_RESOLUTIONS.FULL_HD_VERTICAL:
            return { width: 1920, height: 1080 };
        case TRIGGER_SELECT_RESOLUTIONS.HD:
        case TRIGGER_SELECT_RESOLUTIONS.HD_VERTICAL:
            return { width: 1280, height: 720 };
        case TRIGGER_SELECT_RESOLUTIONS.UHD:
        case TRIGGER_SELECT_RESOLUTIONS.UHD_VERTICAL:
            return { width: 3840, height: 2160 };
        case resolution.includes('Custom'):
            return getCustomWidthAndHeight(resolution);
        default:
            return { width: 1920, height: 1080 };
    }
};

export const resolutionValueToTriggerResolutionType = (
    {
        width,
        height,
    }: {
        width: number;
        height: number;
    },
    isHorizontal: boolean,
): TriggerResolutionSelectType | string => {
    switch (true) {
        case width === 1920 && height === 1080:
            return isHorizontal ? TRIGGER_SELECT_RESOLUTIONS.FULL_HD : TRIGGER_SELECT_RESOLUTIONS.FULL_HD_VERTICAL;
        case width === 1280 && height === 720:
            return isHorizontal ? TRIGGER_SELECT_RESOLUTIONS.HD : TRIGGER_SELECT_RESOLUTIONS.HD_VERTICAL;
        case width === 3840 && height === 2160:
            return isHorizontal ? TRIGGER_SELECT_RESOLUTIONS.UHD : TRIGGER_SELECT_RESOLUTIONS.UHD_VERTICAL;
        default:
            return `Custom (${isHorizontal ? width : height} × ${
                isHorizontal ? height : width
            } / ${getAspectRatioString(width, height)})`;
    }
};

export const triggerOrientationTypeToOrientationValue = (
    orientation: TriggerOrientationSelectType,
): OrientationEnum => {
    switch (orientation) {
        case 'Horizontal':
        case 'Горизонтальная':
            return OrientationEnum.LANDSCAPE;
        case 'Portrait':
        case 'Вертикальная':
            return OrientationEnum.PORTRAIT;
        default:
            return OrientationEnum.LANDSCAPE;
    }
};

export const orientationValueToTriggerOrientationType = (
    orientation: OrientationEnum,
    t: TFunction,
): TriggerOrientationSelectType => {
    switch (true) {
        case orientation.includes(OrientationEnum.LANDSCAPE):
            return t('horizontal');
        case orientation.includes(OrientationEnum.PORTRAIT):
            return t('portrait');
        default:
            return t('horizontal');
    }
};
