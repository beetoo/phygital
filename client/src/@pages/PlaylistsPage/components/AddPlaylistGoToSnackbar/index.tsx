import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { Button as MUiButton, styled } from '@mui/material';

import { getObjectTitleForSnackbar } from '../../../../hooks/getObjectTitleForSnackbar';
import { useCustomRouter } from '../../../../hooks/useCustomRouter';
import { Snackbar } from '../../../../ui-kit';
import { setActivePlaylistId } from '../../actions';
import { AddPlaylistSuccess } from '../../types';

const Button = styled(MUiButton)(`
    font-size: 14px;
    color: #c2c4d4;
    text-transform: none;
`);

export const AddPlaylistGoToSnackbar: React.FC<{ playlist: AddPlaylistSuccess; onClose: () => void }> = ({
    playlist,
    onClose,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const { pushToPage } = useCustomRouter();
    const dispatch = useDispatch();

    const goToScreen = React.useCallback(() => {
        onClose();
        pushToPage('playlists');
        dispatch(setActivePlaylistId(playlist.id));
    }, [dispatch, onClose, playlist.id, pushToPage]);

    const title = getObjectTitleForSnackbar(playlist.playlistDtoInfo.title);

    return (
        <Snackbar
            startAdornment={<CheckCircleIcon color="success" />}
            title={`${t('textOne')} «${title}» ${t('textTwentyNine')}`}
            // {`Плейлист «${title}» загружен!`}
            endAdornment={
                <Button variant="text" onClick={goToScreen}>
                    {t('textThirty')}
                    {/* Перейти к плейлисту */}
                </Button>
            }
            onClose={onClose}
        />
    );
};
