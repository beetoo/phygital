import React from 'react';

import { SelectButton, Tooltip } from '../../../../ui-kit';
import { PlaylistCard } from './components/PlaylistCard';
import { usePlaylistsList } from '../../hooks/usePlaylistsList';

import css from '../../style.m.css';

const PlaylistsList: React.FC = () => {
    const {
        playlists,
        startCreatingPlaylist,
        isAllPlaylistsSelected,
        selectAllPlaylists,
        unselectAllPlaylists,
        isPlaylistSelectDisabled,
        t,
    } = usePlaylistsList();

    return (
        <div className={css.playlistCards}>
            <div className={css.playlistPointsSale}>
                <h2>{t('playlists')}</h2>

                {playlists.length > 0 && (
                    <Tooltip title={t('selectAll')} placement="top" disableHoverListener={isPlaylistSelectDisabled}>
                        <SelectButton
                            active={isAllPlaylistsSelected}
                            disabled={isPlaylistSelectDisabled}
                            onClick={isAllPlaylistsSelected ? unselectAllPlaylists : selectAllPlaylists}
                        />
                    </Tooltip>
                )}
            </div>

            <div className={css.wrapperPlaylistCards}>
                <ul>
                    {playlists.length > 0 ? (
                        playlists.map((playlist) => <PlaylistCard key={playlist.id} playlist={playlist} />)
                    ) : (
                        <div className={css.playlistEmpty}>
                            <p className={css.playlistEmpty1}>
                                {t('textInfoOne')}
                                <br />
                                <a onClick={startCreatingPlaylist}>{t('textInfoTwo')}</a>
                                {t('textInfoThree')}
                            </p>
                        </div>
                    )}
                </ul>
            </div>
        </div>
    );
};

export default PlaylistsList;
