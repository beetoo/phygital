import React from 'react';
import { clsx } from 'clsx';

import { SelectButton, Tooltip } from '../../../../../ui-kit';
import ImgOrVideo from '../../../../../components/ImgOrVideo';
import { getPlaylistStatus } from '../../../helpers';
import { usePlaylistCard } from '../../../hooks/usePlaylistsCard';
import { PlaylistType, CONTENT_TYPE } from '../../../../../../../common/types';

import Playlist_preview from '../../../../../assets/Playlist_preview.svg';
import iconTime from '../../../../../assets/icon22_time.svg';
import iconVideo from '../../../../../assets/iconYoutubeDefault.svg';
import iconImage from '../../../../../assets/icon22.svg';

import css from '../../../style.m.css';

type Props = {
    playlist: PlaylistType;
};

export const PlaylistCard: React.FC<Props> = ({ playlist: { id, title, playlistItems, status } }) => {
    const {
        activePlaylistId,
        isActivePlaylistItem,
        onPlaylistClick,
        onPlaylistDoubleClick,
        firstImage,
        firstItemType,
        getFullPlaylistTime,
        isPlaylistSelected,
        unselectPlaylist,
        selectPlaylist,
        getPlaylistPicturesNumber,
        getPlaylistVideosNumber,
        t,
    } = usePlaylistCard();

    return (
        <li
            className={clsx(
                css.playlistCard,
                (id === activePlaylistId || isPlaylistSelected(id)) && css.active,
                isActivePlaylistItem && css.activePlaylistItem,
            )}
            onClick={onPlaylistClick(id)}
            onDoubleClick={onPlaylistDoubleClick(id)}
        >
            <div className={css.wrapPlaylistCard}>
                <div className={css.fieldPlaylistCardImg}>
                    {firstItemType(playlistItems) === CONTENT_TYPE.IMAGE ? (
                        <ImgOrVideo
                            classNameForImg={clsx(css.playlistCardImage, playlistItems.length > 0 && css.withPicture)}
                            src={playlistItems.length > 0 ? firstImage(playlistItems) : Playlist_preview}
                            width={80}
                            height={80}
                        />
                    ) : firstItemType(playlistItems) === CONTENT_TYPE.VIDEO ? (
                        <ImgOrVideo
                            classNameForVideo={clsx(css.playlistCardImage, playlistItems.length > 0 && css.withPicture)}
                            src={playlistItems.length > 0 ? firstImage(playlistItems) : Playlist_preview}
                            width={80}
                            height={80}
                        />
                    ) : firstItemType(playlistItems) === CONTENT_TYPE.WEB ? (
                        <iframe
                            src={playlistItems.length > 0 ? firstImage(playlistItems) : Playlist_preview}
                            width={80}
                            height={80}
                        />
                    ) : null}
                </div>
                <div className={css.playlistCardStatus}>
                    <div className={css.playlistCardHeader}>
                        <div className={css.playlistCardName}>
                            <Tooltip title={title} placement="top">
                                <span>{title}</span>
                            </Tooltip>
                        </div>

                        <SelectButton
                            className={clsx(css.selectButton, isPlaylistSelected(id) && css.active)}
                            active={isPlaylistSelected(id)}
                            onClick={(event) => {
                                event.stopPropagation();
                                if (isPlaylistSelected(id)) {
                                    unselectPlaylist(id);
                                } else {
                                    selectPlaylist(id);
                                }
                            }}
                            onDoubleClick={(event) => event.stopPropagation()}
                        />
                    </div>

                    <div className={css.descriptionPlaylistCard}>
                        <div className={css.playlistCardStatistics}>
                            <Tooltip title={t('duration')} placement="bottom">
                                <div className={css.wrapPlaylistCardStatistics}>
                                    <img src={iconTime} width="22px" height="22px" alt="" />
                                    <p className={css.playlistCardStatisticsItem}>{getFullPlaylistTime(id)}</p>
                                </div>
                            </Tooltip>
                            <Tooltip title={t('video')} placement="bottom">
                                <div className={css.wrapPlaylistCardStatistics}>
                                    <img src={iconVideo} width="22px" height="22px" alt="" />
                                    <p className={css.playlistCardStatisticsItem}>
                                        {getPlaylistVideosNumber(playlistItems)}
                                    </p>
                                </div>
                            </Tooltip>
                            <Tooltip title={t('images')} placement="bottom">
                                <div className={css.wrapPlaylistCardStatistics}>
                                    <img src={iconImage} width="22px" height="22px" alt="" />
                                    <p className={css.playlistCardStatisticsItem}>
                                        {getPlaylistPicturesNumber(playlistItems)}
                                    </p>
                                </div>
                            </Tooltip>
                        </div>

                        <div className={css.fieldPlaylistCardStatusCaption}>
                            <Tooltip title={getPlaylistStatus(status, t)} placement="top">
                                <p
                                    className={clsx(css.playlistCardStatusCaption, {
                                        [css.playlistCardStatusCaptionUploading]: status === 'uploading',
                                        [css.playlistCardStatusCaptionError]: status === 'upload_error',
                                        [css.playlistCardStatusCaptionActive]: status === 'upload_success',
                                    })}
                                >
                                    {getPlaylistStatus(status, t)}
                                </p>
                            </Tooltip>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    );
};
