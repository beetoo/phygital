import React, { Dispatch, SetStateAction } from 'react';
import { clsx } from 'clsx';

import ImgOrVideo from '../../../../components/ImgOrVideo';
import { formatSecondsToTimeString } from '../../../../../../common/helpers';
import { ContentType, CONTENT_TYPE } from '../../../../../../common/types';
import { usePlaylistItemCard } from '../../hooks/usePlaylistItemsCard';
import { SelectButton } from '../../../../ui-kit';

import css from '../../style.m.css';

type Props = {
    setContentRef: Dispatch<SetStateAction<HTMLLIElement | null>>;
    index: number;
    id: string;
    delay: number;
    name: string;
    content: ContentType;
};

const PlaylistItemCard: React.FC<Props> = ({ index, id, name, delay, content, setContentRef }) => {
    const {
        activePlaylistItemId,
        setActivePlaylistItem,
        openContentViewModal,
        isSomePlaylistItemsSelected,
        selectPlaylistItem,
        unselectPlaylistItem,
        isPlaylistItemSelected,
        openDeletePlaylistItemAlert,
        isSquareScreenOrientation,
        isHorizontalScreenOrientation,
        isPlaylistItemsDisabled,
        playlistsNumber,
    } = usePlaylistItemCard();

    return (
        <div className={css.wrapContentPlaylistItem} onClick={(e) => e.stopPropagation()}>
            <div className={css.wrapContentPlaylistItem1} onClick={setActivePlaylistItem(id)}>
                <li
                    className={clsx(
                        css.contentPlaylistItem,
                        isPlaylistItemsDisabled && css.disabled,
                        isSomePlaylistItemsSelected && css.show,
                        (id === activePlaylistItemId || isPlaylistItemSelected(id)) && css.active,
                    )}
                    ref={(ref) => {
                        if (playlistsNumber === 1 && index === 0) {
                            setContentRef(ref);
                        }
                    }}
                    onDoubleClick={openContentViewModal(id)}
                >
                    {!isSomePlaylistItemsSelected && (
                        <span
                            className={css.overlayTrash}
                            onClick={(event) => {
                                event.stopPropagation();
                                openDeletePlaylistItemAlert(id);
                            }}
                        />
                    )}
                    <SelectButton
                        active={isPlaylistItemSelected(id)}
                        className={clsx(
                            css.overlayCheckCircle,
                            isPlaylistItemSelected(id) && css.overlayCheckCircleActive,
                        )}
                        onClick={(event) => {
                            event.stopPropagation();
                            isPlaylistItemSelected(id) ? unselectPlaylistItem(id) : selectPlaylistItem(id);
                        }}
                    />

                    {!!content?.dimensions && (
                        <>
                            <span
                                className={clsx(css.overlayScreen, {
                                    [css.horizontal]: isHorizontalScreenOrientation(content?.dimensions),
                                    [css.square]: isSquareScreenOrientation(content?.dimensions),
                                })}
                            />
                            <span
                                className={css.overlayScreenResolution}
                            >{`${content?.dimensions?.width}x${content?.dimensions?.height}`}</span>
                        </>
                    )}
                    {content?.type === CONTENT_TYPE.IMAGE ? (
                        <div className={css.wrapPlaylistItemImageRightMenu}>
                            <ImgOrVideo
                                classNameForImg={css.playlistItemImageRightMenu}
                                src={content?.src}
                                width="300"
                                height="400"
                                title={name}
                            />
                        </div>
                    ) : (
                        <div className={clsx(css.wrapPlaylistItemImageRightMenu, css.wrapperVideoCart)}>
                            <ImgOrVideo
                                classNameForVideo={css.playlistItemImageRightMenu}
                                src={content?.src}
                                title={name}
                                width="300"
                                height="400"
                            />
                        </div>
                    )}
                    <div className={css.contentPlaylistImagesName}>
                        <span>{name}</span>
                    </div>
                    <div className={css.contentPlaylistImagesTime}>
                        <span>
                            {formatSecondsToTimeString(
                                content?.type === CONTENT_TYPE.IMAGE ? delay : (content?.duration ?? 0),
                            )}
                        </span>
                    </div>
                    <div className={css.blackoutOverlay} />
                </li>
            </div>
        </div>
    );
};

export default PlaylistItemCard;
