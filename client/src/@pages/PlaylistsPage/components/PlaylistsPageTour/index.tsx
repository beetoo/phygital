import React, { ComponentProps } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { Tour } from '../../../../components/Tour';
import { TourBody } from '../../../../components/Tour/components/TourBody';
import { TourHeader } from '../../../../components/Tour/components/TourHeader';
import { selectPlaylists } from '../../selectors';

import learnerImg from './images/learner.png';
import contentImg from './images/content.png';
import spanner from './images/spanner.png';

type Steps = ComponentProps<typeof Tour>['steps'];

export const PlaylistsPageTour: React.FC<{
    addPlaylist: HTMLElement | null;
    viewContent: HTMLElement | null;
    editPlaylist: HTMLElement | null;
}> = ({ addPlaylist, viewContent, editPlaylist }) => {
    const { t } = useTranslation('translation', { keyPrefix: 'learningHints' });

    const playlists = useSelector(selectPlaylists);

    const steps = React.useMemo<Steps>(
        () => [
            addPlaylist && playlists.length === 0
                ? {
                      name: 'addPlaylist',
                      target: addPlaylist,
                      title: (
                          <TourHeader>
                              <img src={learnerImg} width={32} height={32} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textSixteen')}
                                  {/* Добавьте плейлист */}
                                  {/* было написано раньше - Управляйте плейлистами */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              {t('textSeventeen')}
                              {/* Для создания плейлиста нажмите кнопку «Добавить плейлист». */}
                          </TourBody>
                      ),
                  }
                : null,
            viewContent && playlists.length === 1 && playlists[0].playlistItems.length === 1
                ? {
                      name: 'viewContent',
                      target: viewContent,
                      title: (
                          <TourHeader>
                              <img src={contentImg} width={32} height={32} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textTwentySix')}
                                  {/* Просмотр контента */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              {t('textTwentySeven')}
                              {/* Кликните на картинку или видео, чтобы открыть полноэкранный режим просмотра. */}
                          </TourBody>
                      ),
                  }
                : null,
            editPlaylist
                ? {
                      name: 'editPlaylist',
                      target: editPlaylist,
                      placement: 'left-start',
                      title: (
                          <TourHeader>
                              <img src={spanner} width={32} height={32} style={{ marginRight: '12px' }} alt="" />
                              <div>
                                  {t('textSixteen')}
                                  {/* Добавьте плейлист */}
                                  {/* было написано раньше - Управляйте плейлистами */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              {t('textTwentyEight')}
                              {/* Поменять название или изменить плейлист можно в меню справа. Просто нажмите на кнопку
                              «Редактировать плейлист». */}
                          </TourBody>
                      ),
                  }
                : null,
        ],
        [addPlaylist, editPlaylist, playlists, viewContent, t],
    );

    return <Tour steps={steps} name="playlistsPage" />;
};
