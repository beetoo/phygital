import React from 'react';

import ModalTemplate from '../../../../components/ModalTemplate';
import { useAddPlaylistModal } from '../../hooks/useAddPlaylistModal';
import css from '../../style.m.css';
import { AddPlaylistModalTour } from './components/AddPlaylistModalTour';
import { CancelButton } from '../../../../ui-kit';

const AddPlaylistModal: React.FC = () => {
    const {
        playlistName,
        onPlaylistNameChange,
        isSubmitButtonDisabled,
        startCreatingPlaylist,
        addPlaylistNameInput,
        setAddPlaylistNameInput,
        t,
    } = useAddPlaylistModal();

    return (
        <ModalTemplate isOpen={false}>
            <form className={css.popupAddPlaylist} onSubmit={(e) => e.preventDefault()}>
                <h2>{t('createNewPlaylist')}</h2>
                <label>{t('namePlaylist')}</label>
                <br />
                <input
                    ref={setAddPlaylistNameInput}
                    className={css.enterNamePlaylist}
                    placeholder={t('enterNamePlaylist') as string}
                    value={playlistName}
                    onChange={onPlaylistNameChange}
                    maxLength={125}
                />
                <div className={css.containerBtn}>
                    <a className={css.btnCancel}>
                        <CancelButton text={t('btnCancel')} onClick={() => undefined} />
                    </a>
                    <button
                        type="submit"
                        className={css.buttonCreate}
                        onClick={startCreatingPlaylist}
                        disabled={isSubmitButtonDisabled}
                    >
                        {t('btnCreate')}
                    </button>
                </div>
            </form>
            <AddPlaylistModalTour addPlaylistNameInput={addPlaylistNameInput} />
        </ModalTemplate>
    );
};

export default AddPlaylistModal;
