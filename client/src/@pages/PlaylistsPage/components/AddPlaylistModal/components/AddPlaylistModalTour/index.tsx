import React, { ComponentProps } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { Tour } from '../../../../../../components/Tour';
import { TourBody } from '../../../../../../components/Tour/components/TourBody';
import { TourHeader } from '../../../../../../components/Tour/components/TourHeader';

import addPlaylistModalOnBoardingImage from '../../images/addPlaylistModalOnBoardingImage.png';
import { selectOnBoardingStatus } from '../../../../../../selectors';

type Steps = ComponentProps<typeof Tour>['steps'];

export const AddPlaylistModalTour: React.FC<{ addPlaylistNameInput: HTMLElement | null }> = ({
    addPlaylistNameInput,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'learningHintsModal' });

    const steps = React.useMemo<Steps>(
        () => [
            addPlaylistNameInput
                ? {
                      name: 'addPlaylistNameInput',
                      target: addPlaylistNameInput,
                      placement: 'left-start',
                      title: (
                          <TourHeader>
                              <img src={addPlaylistModalOnBoardingImage} width={32} height={32} alt="" />
                              <div style={{ marginLeft: '15px' }}>
                                  {t('textNineteen')}
                                  {/* Создание плейлиста */}
                              </div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  {t('textTwenty')}
                                  {/* Плейлист — это видео-ролик, состоящий из ваших изображений и видеофайлов. */}
                              </p>
                              <p>
                                  {t('textTwentyOne')}
                                  {/* Для начала задайте название новому плейлисту. Например, «Ланчи». В дальнейшем это
                                  облегчит поиск и настройку плейлистов. */}
                              </p>
                          </TourBody>
                      ),
                  }
                : null,
        ],
        [addPlaylistNameInput, t],
    );

    const isOnBoardingEnabled = useSelector(selectOnBoardingStatus);

    return <Tour steps={steps} name="addPlaylistModal" run={isOnBoardingEnabled} />;
};
