import React from 'react';

import ModalTemplate from '../../../../components/ModalTemplate';
import { useDeletePlaylistItemAlert } from '../../hooks/useDeletePlaylistItemAlert';

import css from '../../style.m.css';

const DeletePlaylistItemAlert: React.FC = () => {
    const { isDeleteContentAlertOpen, deletePlaylistItem, doDeletePlaylistItem, closeDeleteContentAlert, t } =
        useDeletePlaylistItemAlert();

    return (
        <ModalTemplate isOpen={isDeleteContentAlertOpen}>
            <form className={css.popupDeletePlaylist} onSubmit={(e) => e.preventDefault()}>
                <h2>{t('delContent')}?</h2>
                <p>
                    {t('textDelContentOne')}
                    <br />
                    {t('textDelContentTwo')}
                </p>
                <div className={css.containerBtn}>
                    <a className={css.btnCancel}>
                        <button type="button" className={css.buttonCancel} onClick={closeDeleteContentAlert}>
                            {t('btnCancel')}
                        </button>
                    </a>
                    <button
                        type="submit"
                        className={css.buttonDelete}
                        disabled={deletePlaylistItem}
                        onClick={doDeletePlaylistItem}
                    >
                        {t('btnDelete')}
                    </button>
                </div>
            </form>
        </ModalTemplate>
    );
};

export default DeletePlaylistItemAlert;
