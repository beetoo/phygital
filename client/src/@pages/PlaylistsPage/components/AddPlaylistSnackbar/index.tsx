import React from 'react';
import { useTranslation } from 'react-i18next';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

import { getObjectTitleForSnackbar } from '../../../../hooks/getObjectTitleForSnackbar';
import { Snackbar } from '../../../../ui-kit';
import { AddPlaylistSuccess } from '../../types';

export const AddPlaylistSnackbar: React.FC<{ playlist: AddPlaylistSuccess; onClose: () => void }> = ({
    playlist,
    onClose,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const title = getObjectTitleForSnackbar(playlist.playlistDtoInfo.title);

    return (
        <Snackbar
            startAdornment={<CheckCircleIcon color="success" />}
            title={`${t('textOne')} «${title}» ${t('textTwentyNine')}`}
            // {`Плейлист «${title}» загружен!`}
            onClose={onClose}
        />
    );
};
