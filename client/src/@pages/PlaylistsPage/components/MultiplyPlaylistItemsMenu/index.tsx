import React from 'react';
import { clsx } from 'clsx';

import { useMultiplyPlaylistItemsMenu } from '../../hooks/useMultiplyPlaylistItemsMenu';

import css from '../../style.m.css';

const MultiplyPlaylistItemsMenu: React.FC = () => {
    const {
        selectedPlaylistItemsNumber,
        selectedItemsFilesize,
        unselectAllPlaylistItems,
        showDeleteMultiplyPlaylistItemsModal,
        t,
    } = useMultiplyPlaylistItemsMenu();

    return (
        <div className={css.mainRightMenuPlaylist}>
            <div className={css.wrapMainRightMenu}>
                <h2>{t('contentSettings')}</h2>
                <h3>
                    {t('selectedContent')}
                    <span>{selectedPlaylistItemsNumber}</span>
                </h3>
                <a>
                    <button
                        type="button"
                        className={clsx(css.btnPlaylist, css.deletePlaylist)}
                        onClick={showDeleteMultiplyPlaylistItemsModal}
                    >
                        <img alt="" />
                        {t('btnDeleteContent')}
                    </button>
                </a>
                <h3>{t('info')}</h3>
                <p>
                    {t('totalSize')}
                    <span>{selectedItemsFilesize}</span>
                </p>
            </div>

            <div className={css.wrapperFieldBtn}>
                <button
                    type="button"
                    className={clsx(css.btnPlaylist, css.deselect)}
                    onClick={unselectAllPlaylistItems}
                >
                    {t('btnUnselect')}
                </button>
            </div>
        </div>
    );
};

export default MultiplyPlaylistItemsMenu;
