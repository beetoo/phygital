import React from 'react';

import ModalTemplate from '../../../../components/ModalTemplate';
import { useStartPlaylistModal } from '../../hooks/useStartPlaylistModal';

import css from './style.m.css';

const StartPlaylistModal: React.FC = () => {
    const { isStartPlaylistModalOpen, addingPlaylist, savePlaylist, closeStartPlaylistModal, t } =
        useStartPlaylistModal();

    return (
        <ModalTemplate isOpen={isStartPlaylistModalOpen}>
            <div className={css.startPlaylistModal}>
                <h2>{t('textUploadOne')}</h2>
                <p>{t('textUploadTwo')}</p>
                <button className={css.btnEdit} onClick={closeStartPlaylistModal} disabled={addingPlaylist}>
                    {t('btnEdit')}
                </button>
                <button className={css.btnSendToScreens} onClick={savePlaylist} disabled={addingPlaylist}>
                    {t('btnUploadScreens')}
                </button>
            </div>
        </ModalTemplate>
    );
};

export default StartPlaylistModal;
