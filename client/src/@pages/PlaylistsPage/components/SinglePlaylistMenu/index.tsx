import React from 'react';
import { CircularProgress, Stack } from '@mui/material';
import { clsx } from 'clsx';

import ImgOrVideo from '../../../../components/ImgOrVideo';
import Content_preview from '../../../../assets/Content_preview.png';
import { getPlaylistStatus } from '../../helpers';
import { useSinglePlaylistMenu } from '../../hooks/useSinglePlaylistMenu';
import { CONTENT_TYPE } from '../../../../../../common/types';
import { PlaylistHistoryItem } from '../../hooks/useHistoryItems';

import css from '../../style.m.css';

const SinglePlaylistMenu: React.FC = () => {
    const {
        playlistTitle,
        updatingPlaylist,
        playlistStatus,
        onChangePlaylistTitle,
        openDeletePlaylistAlert,
        updatePlaylist,
        isSubmitButtonShown,
        isUpdatePlaylistButtonDisabled,
        openEditPlaylistStep,
        isUploading,
        firstImage,
        firstItemType,
        playlistDuration,
        playlistFilesSize,
        playlistResolution,
        playlistOrientation,
        playlistCreatedAt,
        playlistUpdatedAt,
        playlistScreens,
        isPlaylistScreensOpen,
        showHidePlaylistScreens,
        goToScreen,
        playlistMenuTab,
        setActivePlaylistMenuTab,
        playlistHistoryItems,
        hasHistory,

        isInfoShown,
        showHideInfo,
        t,
    } = useSinglePlaylistMenu();

    return (
        <form className={css.mainRightMenu} onSubmit={(e) => e.preventDefault()}>
            <div className={clsx(css.wrapMainRightMenu, css.wrapMainRightMenu_1)}>
                <h2 style={{ height: '26px' }}>{playlistTitle}</h2>

                {hasHistory && (
                    <div className={css.containerTabs}>
                        <button
                            className={clsx(css.btnTab, playlistMenuTab === 'settings' && css.btnTabActive)}
                            onClick={setActivePlaylistMenuTab}
                        >
                            {t('settings')}
                        </button>
                        <button
                            className={clsx(css.btnTab, playlistMenuTab === 'history' && css.btnTabActive)}
                            onClick={setActivePlaylistMenuTab}
                        >
                            {t('history')}
                        </button>
                    </div>
                )}

                {playlistMenuTab === 'settings' && (
                    <>
                        <p
                            className={clsx(css.playlistCardStatusCaption, {
                                [css.playlistCardStatusCaptionUploading]: playlistStatus === 'uploading',
                                [css.playlistCardStatusCaptionError]: playlistStatus === 'upload_error',
                                [css.playlistCardStatusCaptionActive]: playlistStatus === 'upload_success',
                            })}
                        >
                            {getPlaylistStatus(playlistStatus, t)}
                        </p>

                        {firstItemType === CONTENT_TYPE.IMAGE ? (
                            <ImgOrVideo
                                classNameForImg={css.playlistFirstImageRightMenu}
                                src={firstImage ?? Content_preview}
                                width="292"
                                height="164"
                            />
                        ) : firstItemType === CONTENT_TYPE.VIDEO ? (
                            <div className={css.wrapperVideo}>
                                <ImgOrVideo
                                    classNameForVideo={css.playlistFirstImageRightMenu}
                                    src={firstImage ?? Content_preview}
                                    width="292"
                                    height="164"
                                />
                            </div>
                        ) : firstItemType === CONTENT_TYPE.WEB ? (
                            <iframe
                                className={css.playlistFirstImageRightMenu}
                                src={firstImage ?? Content_preview}
                                width="292"
                                height="164"
                            />
                        ) : null}
                        <div>
                            <label>{t('playlistName')}</label>
                            <br />
                            <input
                                className={css.rightMenuNamePlaylist}
                                placeholder={t('playlistNameInput') as string}
                                value={playlistTitle}
                                onChange={onChangePlaylistTitle}
                                maxLength={125}
                            />
                        </div>
                        <button
                            type="button"
                            className={clsx(css.btnPlaylist, css.editPlaylist)}
                            onClick={openEditPlaylistStep}
                        >
                            <img alt="" />
                            {t('btnEditPlaylist')}
                        </button>
                        <div className={css.wrapperInfo}>
                            <div className={css.infoHeader} onClick={showHideInfo}>
                                <h3 className={css.mainRightMenuInfo}>{t('info')}</h3>
                                <span className={isInfoShown ? css.iconArrowInfoUp : css.iconArrowInfoDown} />
                            </div>
                            {isInfoShown && (
                                <div>
                                    <p className={css.mainRightMenuInfoItem}>
                                        {t('durationRight')}
                                        <span>{playlistDuration}</span>
                                    </p>
                                    <p className={css.mainRightMenuInfoItem}>
                                        {t('size')}
                                        <span>{playlistFilesSize}</span>
                                    </p>
                                    {playlistScreens.length > 0 && (
                                        <p className={css.mainRightMenuInfoItem}>
                                            {t('resolution')}
                                            <span>{playlistResolution}</span>
                                        </p>
                                    )}
                                    <p className={css.mainRightMenuInfoItem}>
                                        {t('orientation')}
                                        <span>{playlistOrientation}</span>
                                    </p>
                                    <p className={css.mainRightMenuInfoItem}>
                                        {t('created')}
                                        <span>{playlistCreatedAt}</span>
                                    </p>
                                    <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItemLast)}>
                                        {t('edited')}
                                        <span>{playlistUpdatedAt}</span>
                                    </p>
                                </div>
                            )}
                        </div>
                        <div className={css.wrapMainRightMenuInfoAll}>
                            {playlistScreens.length > 0 && (
                                <div className={css.infoHeader} onClick={showHidePlaylistScreens}>
                                    <h3>
                                        {t('usedOnScreens')}
                                        &ensp;
                                        <span>{playlistScreens.length}</span>
                                    </h3>

                                    <span
                                        className={isPlaylistScreensOpen ? css.iconArrowInfoUp : css.iconArrowInfoDown}
                                    />
                                </div>
                            )}

                            <div className={css.mainRightMenuInfoAll}>
                                {isPlaylistScreensOpen && (
                                    <ul>
                                        {playlistScreens.map(({ id, name }, index) => (
                                            <li key={index}>
                                                <p>{name}</p>
                                                <a onClick={goToScreen(id)}>{t('goTo')}</a>
                                            </li>
                                        ))}
                                    </ul>
                                )}
                            </div>
                        </div>
                        <a>
                            <button
                                type="button"
                                className={clsx(css.btnPlaylist, css.deletePlaylist)}
                                onClick={openDeletePlaylistAlert}
                                disabled={isUploading}
                            >
                                <img alt="" />
                                {t('btnDeletePlaylist')}
                            </button>
                        </a>
                    </>
                )}

                {playlistMenuTab === 'history' && (
                    <div className={css.containerHistoryDescription}>
                        {(playlistHistoryItems as PlaylistHistoryItem[]).map(({ id, createdAt, type, description }) => (
                            <div key={id} className={css.historyDescription}>
                                <p>{createdAt}</p>
                                <p>
                                    {t(type)}: {type === 'status' ? t(description) : description}
                                </p>
                            </div>
                        ))}
                    </div>
                )}
            </div>

            {isSubmitButtonShown && (
                <div className={css.btnSaveSettingsField}>
                    {playlistMenuTab === 'settings' && (
                        <button
                            type="submit"
                            className={clsx(css.btnPlaylist, css.saveSettings)}
                            onClick={updatePlaylist}
                            disabled={isUpdatePlaylistButtonDisabled}
                        >
                            {updatingPlaylist && (
                                <Stack position="absolute" right="266px" bottom="54px">
                                    <CircularProgress color="inherit" size={30} />
                                </Stack>
                            )}
                            {t('btnSaveSetting')}
                        </button>
                    )}
                </div>
            )}
        </form>
    );
};

export default SinglePlaylistMenu;
