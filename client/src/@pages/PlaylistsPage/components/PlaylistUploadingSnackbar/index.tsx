import React from 'react';
import { useTranslation } from 'react-i18next';

import { getObjectTitleForSnackbar } from '../../../../hooks/getObjectTitleForSnackbar';
import { Snackbar } from '../../../../ui-kit';
import { SnackbarProgress } from '../../../../ui-kit/Snackbar/components/SnackbarProgress';

export const PlaylistUploadingSnackbar: React.FC<{ title: string; onClose?: () => void }> = ({
    title: _title,
    onClose,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const title = getObjectTitleForSnackbar(_title);

    return (
        <Snackbar
            startAdornment={<SnackbarProgress />}
            title={
                <div
                    style={{
                        color: '#fff',
                        fontFamily: 'Inter',
                        fontSize: '14px',
                        lineHeight: '17px',
                        fontWeight: 400,
                    }}
                >
                    <div>
                        {/* Плейлист «<span style={{ fontWeight: 600 }}>{title}</span>» загружается на экраны... */}
                        {t('textOne')} «<span style={{ fontWeight: 600 }}>{title}</span>» {t('textTwo')}
                    </div>
                    <div
                        style={{
                            marginLeft: 0,
                            color: '#C2C4D4',
                            fontSize: '12px',
                            lineHeight: '15px',
                        }}
                    >
                        {t('textThree')}
                        {/* Загрузка может занять до 3-х минут */}
                    </div>
                </div>
            }
            onClose={onClose}
        />
    );
};
