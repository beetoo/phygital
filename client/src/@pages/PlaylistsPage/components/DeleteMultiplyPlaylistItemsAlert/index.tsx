import React from 'react';

import ModalTemplate from '../../../../components/ModalTemplate';
import { useDeleteMultiplyPlaylistItemsAlert } from '../../hooks/useDeleteMultiplyPlaylistItemsAlert';

import css from '../../style.m.css';

const DeleteMultiplyPlaylistItemsAlert: React.FC = () => {
    const {
        isDeletePlaylistItemsAlertOpen,
        selectedPlaylistItemsNumber,
        deletePlaylistItems,
        doDeletePlaylistItems,
        closeDeletePlaylistItemsAlert,
        t,
    } = useDeleteMultiplyPlaylistItemsAlert();

    return (
        <ModalTemplate isOpen={isDeletePlaylistItemsAlertOpen}>
            <form className={css.popupDeletePlaylist} onSubmit={(e) => e.preventDefault()}>
                <h2>
                    {t('delContent')} ({selectedPlaylistItemsNumber})?
                </h2>
                <p>
                    {t('textDelContentOne')}
                    <br />
                    {t('textDelContentThree')}
                </p>
                <div className={css.containerBtn}>
                    <a className={css.btnCancel}>
                        <button type="button" className={css.buttonCancel} onClick={closeDeletePlaylistItemsAlert}>
                            {t('btnCancel')}
                        </button>
                    </a>
                    <button
                        type="submit"
                        className={css.buttonDelete}
                        disabled={deletePlaylistItems}
                        onClick={doDeletePlaylistItems}
                    >
                        {t('btnDelete')}
                    </button>
                </div>
            </form>
        </ModalTemplate>
    );
};

export default DeleteMultiplyPlaylistItemsAlert;
