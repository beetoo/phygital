import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Button as MUiButton, styled, Typography } from '@mui/material';
import { getObjectTitleForSnackbar } from '../../../../hooks/getObjectTitleForSnackbar';
import { Snackbar } from '../../../../ui-kit';
import { SnackbarProgress } from '../../../../ui-kit/Snackbar/components/SnackbarProgress';

const MoreButton = styled(MUiButton)(`
    font-size: 14px;
    color: #c2c4d4;
    text-transform: none;
`);

const List = styled('ol')(`margin: 0;`);
const Extra = styled('div')(`max-width: 555px;`);

export const PlaylistWaitingForUploadingSnackbar: React.FC<{ title: string; onClose?: () => void }> = ({
    title: _title,
    onClose,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'snackBars' });

    const [expanded, setExpanded] = useState(false);
    const title = getObjectTitleForSnackbar(_title);

    return (
        <Snackbar
            title={
                <div
                    style={{
                        color: '#fff',
                        fontFamily: 'Inter',
                        fontSize: '14px',
                        lineHeight: '17px',
                        fontWeight: 400,
                    }}
                >
                    <div>
                        {t('textOne')} «<b>{title}</b>» {t('textTwo')}
                        {/* Плейлист «<span style={{ fontWeight: 600 }}>{title}</span>» загружается на экраны... */}
                    </div>
                    <div
                        style={{
                            marginLeft: 0,
                            color: '#C2C4D4',
                            fontSize: '12px',
                            lineHeight: '15px',
                        }}
                    >
                        {t('textThree')}
                        {/* Загрузка может занять до 3-х минут */}
                    </div>
                </div>
            }
            onClose={onClose}
            startAdornment={<SnackbarProgress />}
            endAdornment={
                <MoreButton variant="text" onClick={() => setExpanded((prevValue) => !prevValue)}>
                    {!expanded ? `${t('more')}` : `${t('hide')}`}
                    {/* {!expanded ? 'Подробнее' : 'Свернуть'} */}
                </MoreButton>
            }
            content={
                expanded && (
                    <Extra>
                        <Typography>
                            {t('textFour')}
                            {/* Идёт подготовка плейлиста к загрузке на экраны. */}
                        </Typography>
                        <Typography>
                            <b>
                                {t('textFive')}
                                {/* Убедитесь, что: */}
                            </b>
                        </Typography>
                        <List>
                            <li>
                                {t('textSix')}
                                {/* интернет-соединение работает */}
                            </li>
                            <li>
                                {t('textSeven')}
                                {/* экраны, на которые загружается плейлист, включены и корректно настроены */}
                            </li>
                        </List>
                    </Extra>
                )
            }
        />
    );
};
