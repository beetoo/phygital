import React from 'react';

import SinglePlaylistMenu from '../SinglePlaylistMenu';
import MultiplyPlaylistsMenu from '../MultiplyPlaylistsMenu';
import MultiplyPlaylistItemsMenu from '../MultiplyPlaylistItemsMenu';
import { useRightPlaylistsMenu } from '../../hooks/useRightPlaylistsMenu';

import css from '../../style.m.css';

const RightPlaylistsMenu: React.FC = () => {
    const { hasNoPlaylists, hasSelectedPlaylist, hasMultiplyPlaylistsMenu, hasMultiplyPlaylistItemsMenu } =
        useRightPlaylistsMenu();

    return !hasNoPlaylists ? (
        <>
            {hasMultiplyPlaylistsMenu && <MultiplyPlaylistsMenu />}
            {hasMultiplyPlaylistItemsMenu && <MultiplyPlaylistItemsMenu />}
            {/*{hasSelectedPlaylistItem && <SinglePlaylistItemMenu />}*/}
            {hasSelectedPlaylist && <SinglePlaylistMenu />}
        </>
    ) : (
        <div className={css.mainRightMenuPlaylist}>
            <div className={css.mainRightMenuPlaylistEmpty} />
        </div>
    );
};

export default RightPlaylistsMenu;
