import React from 'react';

import PlaylistStepOne from '../PlaylistStepOne';
import PlaylistStepTwo from '../PlaylistStepTwo';
import PlaylistStepThree from '../PlaylistStepThree';

import { usePlaylistStepsContainer } from '../../hooks/usePlaylistStepsContainer';
import { useSetDefaultStepsSettings } from '../../hooks/useSetDefaultStepsSettings';

const PlaylistStepsContainer: React.FC = () => {
    const { isPlaylistStepOne, isPlaylistStepTwo, isPlaylistStepThree } = usePlaylistStepsContainer();

    useSetDefaultStepsSettings();

    return (
        <>
            {isPlaylistStepOne && <PlaylistStepOne />}
            {isPlaylistStepTwo && <PlaylistStepTwo />}
            {isPlaylistStepThree && <PlaylistStepThree />}
        </>
    );
};

export default PlaylistStepsContainer;
