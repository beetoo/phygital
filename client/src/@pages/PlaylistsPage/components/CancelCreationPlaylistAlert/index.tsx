import React from 'react';

import ModalTemplate from '../../../../components/ModalTemplate';
import { useCancelCreationPlaylistAlert } from '../../hooks/useCancelCreationPlaylistAlert';

import css from './style.m.css';

const CancelCreationPlaylistAlert: React.FC = () => {
    const { isModalOpen, playlistModeWords, onCancel, onExit, t } = useCancelCreationPlaylistAlert();

    return (
        <ModalTemplate isOpen={isModalOpen}>
            <div className={css.modalPopupExitMode}>
                <h2>
                    {t('exitPlaylistMode', { mode: playlistModeWords[0] })
                        .split(`\n`)
                        .map((text, index) => (
                            <div key={index}>{text}</div>
                        ))}
                </h2>
                <p>{t('allUnsavedDataWillBeLost', { data: playlistModeWords[1] })}</p>
                <div className={css.fieldButtons}>
                    <button className={css.btnCancel} onClick={onCancel}>
                        {t('btnCancel')}
                    </button>

                    <button className={css.btnExitMode} onClick={onExit}>
                        <img alt="" />
                        {t('btnExit')}
                    </button>
                </div>
            </div>
        </ModalTemplate>
    );
};

export default CancelCreationPlaylistAlert;
