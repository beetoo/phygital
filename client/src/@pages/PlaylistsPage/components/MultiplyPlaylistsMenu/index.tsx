import React from 'react';
import { Tooltip } from '@mui/material';
import { clsx } from 'clsx';

import { useMultiplyPlaylistsMenu } from '../../hooks/useMultiplyPlaylistsMenu';

import css from '../../style.m.css';

const MultiplyPlaylistsMenu: React.FC = () => {
    const {
        selectedPlaylistsNumber,
        playlistsFilesSize,
        openDeleteMultiplyPlaylistsAlert,
        unselectAllPlaylists,
        isPlaylistsCouldBeDeleted,
        t,
    } = useMultiplyPlaylistsMenu();

    const Btn = (
        <button
            type="button"
            className={clsx(css.btnPlaylist, css.deletePlaylist)}
            onClick={openDeleteMultiplyPlaylistsAlert}
            disabled={!isPlaylistsCouldBeDeleted}
        >
            <img alt="" />
            {selectedPlaylistsNumber > 1 ? t('deletePlaylists') : t('deletePlaylist')}
        </button>
    );

    return (
        <div className={css.mainRightMenuPlaylist}>
            <div className={css.wrapMainRightMenu}>
                <h2>{t('playlistSettings')}</h2>
                <h3>
                    {t('selectedPlaylists')}
                    <span>{selectedPlaylistsNumber}</span>
                </h3>
                <a>
                    {isPlaylistsCouldBeDeleted ? (
                        Btn
                    ) : (
                        <Tooltip title="Снимите выделение с плейлистов в процессе загрузки" arrow open={true}>
                            {Btn}
                        </Tooltip>
                    )}
                </a>
                <h3>{t('info')}</h3>
                <p>
                    {t('totalSize')}
                    <span>{playlistsFilesSize}</span>
                </p>
            </div>

            <div className={css.wrapperFieldBtn}>
                <button type="button" className={clsx(css.btnPlaylist, css.deselect)} onClick={unselectAllPlaylists}>
                    {t('btnUnselect')}
                </button>
            </div>
        </div>
    );
};

export default MultiplyPlaylistsMenu;
