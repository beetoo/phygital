import React, { memo } from 'react';
import { DragDropContext } from '@hello-pangea/dnd';

import { usePlaylistStepOne } from '../../hooks/usePlaylistStepOne';
import StepOneLocationsList from './components/StepOneLocationsList';
import StepOneScreensList from './components/StepOneScreensList';
import StepOneMenu from './components/StepOneMenu';

import css from './style.m.css';

const PlaylistStepOne: React.FC = () => {
    const { onDragEnd } = usePlaylistStepOne();

    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <div className={css.createPlaylistCenterBlock}>
                <div className={css.centerBlockStepOne}>
                    <StepOneLocationsList />
                    <StepOneScreensList />
                </div>
            </div>
            <StepOneMenu />
        </DragDropContext>
    );
};

export default memo(PlaylistStepOne);
