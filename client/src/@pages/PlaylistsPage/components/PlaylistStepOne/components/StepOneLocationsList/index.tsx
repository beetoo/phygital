import React from 'react';
import { clsx } from 'clsx';

import { useStepOneLocationsList } from '../../../../hooks/useStepOneLocationsList';
import { getScreensInfo } from '../../../../../ScreensPage/helpers';
import { locationCityAddressToString } from '../../../../../../constants/helpers';

import Pin from '../../../../../../assets/Pin4k.png';

import css from '../../style.m.css';

const StepOneLocationsList: React.FC = () => {
    const {
        activeLocationId,
        isScreensActive,
        locations,
        selectLocation,
        allScreensNumber,
        allOnlineScreensNumber,
        allOfflineScreensNumber,
        allErrorScreensNumber,
        t,
    } = useStepOneLocationsList();

    return (
        <div className={css.foldersFrame}>
            <div className={css.screensPointsSale}>
                <h2>{t('points')}</h2>
            </div>
            <div className={css.wrapFoldersFrameList}>
                <ul className={css.foldersFrameList}>
                    <li
                        className={clsx(
                            css.folder,
                            activeLocationId === 'all' && css.active,
                            isScreensActive && css.activeScreen,
                        )}
                        onClick={() => selectLocation('all')}
                    >
                        <h3 className={css.folderName}>
                            {t('screensAll')}

                            <span>({allScreensNumber})</span>
                        </h3>
                        <ul className={css.folderStatistics}>
                            <li className={clsx(css.folderItem, css.folderItemOnline)}>{allOnlineScreensNumber}</li>
                            <li className={clsx(css.folderItem, css.folderItemOffline)}>{allOfflineScreensNumber}</li>
                            <li className={clsx(css.folderItem, css.folderItemError)}>{allErrorScreensNumber}</li>
                        </ul>
                    </li>
                    {locations.map(({ id, name, city, address, screens }) => {
                        const { screensNumber, onlineScreensNumber, offlineScreensNumber, errorScreensNumber } =
                            getScreensInfo(screens);

                        return (
                            <li
                                key={id}
                                className={clsx(
                                    css.folderCard,
                                    activeLocationId === id && css.active,
                                    isScreensActive && css.activeScreen,
                                )}
                                onClick={() => selectLocation(id)}
                            >
                                <div className={css.folderCardInfo}>
                                    <img src={Pin} width="48" height="37" alt="" />
                                    <div className={css.folderCardInfoText}>
                                        <h3 className={css.folderNameCard}>{name}</h3>
                                        <p className={css.folderAddressCard}>
                                            {locationCityAddressToString(city, address)}
                                        </p>
                                    </div>
                                </div>
                                <div>
                                    <p className={css.folderItemCard}>
                                        {t('screensTotal')}

                                        <span>{screensNumber}</span>
                                    </p>
                                    <ul className={css.folderStatistics}>
                                        <li className={clsx(css.folderItem, css.folderItemOnline)}>
                                            {onlineScreensNumber}
                                        </li>
                                        <li className={clsx(css.folderItem, css.folderItemOffline)}>
                                            {offlineScreensNumber}
                                        </li>
                                        <li className={clsx(css.folderItem, css.folderItemError)}>
                                            {errorScreensNumber}
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        );
                    })}
                </ul>
            </div>
        </div>
    );
};

export default StepOneLocationsList;
