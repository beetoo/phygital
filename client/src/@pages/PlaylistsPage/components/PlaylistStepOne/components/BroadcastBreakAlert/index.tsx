import React from 'react';
import { useTranslation } from 'react-i18next';

import ModalTemplate from '../../../../../../components/ModalTemplate';

import css from './style.m.css';

type Props = {
    isShown: boolean;
    onSubmit: () => void;
    onCancel: () => void;
    screensNumber: number;
};

const BroadcastBreakAlert: React.FC<Props> = ({ isShown, onSubmit, onCancel, screensNumber }) => {
    const { t } = useTranslation('translation', { keyPrefix: 'playlistsModal' });

    return (
        <ModalTemplate isOpen={isShown}>
            <div className={css.popupDeletePlaylist}>
                <h2>
                    {screensNumber > 1 ? `${t('textDelOne')}` : `${t('textDelTwo')}`}
                    {screensNumber > 1 ? `(${screensNumber})` : ''}?
                </h2>
                <p>{screensNumber > 1 ? `${t('textDelFour')}` : `${t('textDelThree')}`}</p>
                <div className={css.fieldButtons}>
                    <a className={css.btnCancel}>
                        <button className={css.buttonCancel} onClick={onCancel}>
                            {t('btnCancel')}
                        </button>
                    </a>
                    <button className={css.buttonDelete} onClick={onSubmit}>
                        <img alt="" />
                        {t('btnInterrupt')}
                    </button>
                </div>
            </div>
        </ModalTemplate>
    );
};

export default BroadcastBreakAlert;
