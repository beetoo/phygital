import React from 'react';

import { SelectWithTags } from '../../../../../../ui-kit';
import { useScreenTagsSelect } from '../../hooks/useScreenTagsSelect';

import css from '../../style.m.css';

const ScreenTagsSelect: React.FC = () => {
    const { isAllTagsSelected, tags, filteringTags, onSelectScreenTag, onDeleteScreenTag, isTagsSelectDisabled } =
        useScreenTagsSelect();

    return tags.length > 1 ? (
        <div className={css.selectPlaylistTags}>
            <SelectWithTags
                isAllTagsSelected={isAllTagsSelected}
                tags={tags}
                filteringTags={filteringTags}
                onSelectTags={onSelectScreenTag}
                isTagsSelectDisabled={isTagsSelectDisabled}
                onDeleteTag={onDeleteScreenTag}
            />
        </div>
    ) : null;
};

export default ScreenTagsSelect;
