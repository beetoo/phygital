import React, { ComponentProps } from 'react';
import { useSelector } from 'react-redux';

import { Tour } from '../../../../../../components/Tour';
import { TourBody } from '../../../../../../components/Tour/components/TourBody';
import { TourHeader } from '../../../../../../components/Tour/components/TourHeader';
import { selectLocations } from '../../../../../ScreensPage/selectors';
import { usePlaylistStep } from '../../../../hooks/usePlaylistStep';

import screenImg from './images/screen.png';
import stopSign from './images/stop_sign.png';

type Steps = ComponentProps<typeof Tour>['steps'];

type Props = {
    selectScreen: HTMLElement | null;
    unsuitableScreen: HTMLElement | null;
};

export const PlaylistStepOneTour: React.FC<Props> = ({ selectScreen, unsuitableScreen }: Props) => {
    const { playlistStep } = usePlaylistStep();

    const locations = useSelector(selectLocations);

    const steps = React.useMemo<Steps>(
        () => [
            selectScreen && playlistStep === 'creating_one'
                ? {
                      name: 'selectScreen',
                      target: selectScreen,
                      placement: 'bottom',
                      title: (
                          <TourHeader>
                              <img src={screenImg} style={{ marginRight: '12px' }} alt="" />
                              <div>Выберите экраны</div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              <p>
                                  Выберите экраны, на которые будет запущен плейлист. Чтобы контент транслировался
                                  корректно у выбранных экранов должно быть одинаковое разрешение.
                              </p>
                              <p>Перетащите экраны в правую область. Затем нажмите кнопку «Следующий шаг».</p>
                          </TourBody>
                      ),
                  }
                : null,
            unsuitableScreen && locations.length === 1
                ? {
                      name: 'unsuitableScreen',
                      target: unsuitableScreen,
                      placement: 'bottom',
                      title: (
                          <TourHeader>
                              <img src={stopSign} style={{ marginRight: '12px' }} alt="" />
                              <div>Неподходящие экраны</div>
                          </TourHeader>
                      ),
                      content: (
                          <TourBody>
                              Вы не можете выбрать данный экран. Его разрешение или ориентация отличается от разрешения
                              или ориентации уже выбранных экранов.
                          </TourBody>
                      ),
                  }
                : null,
        ],
        [locations.length, playlistStep, selectScreen, unsuitableScreen],
    );

    return <Tour steps={steps} name="PlaylistStepOne" />;
};
