import React from 'react';
import { Draggable, Droppable } from '@hello-pangea/dnd';
import { TFunction } from 'i18next';
import { clsx } from 'clsx';

import { SelectButton, Tooltip } from '../../../../../../ui-kit';
import ScreenTagsSelect from '../ScreenTagsSelect';
import { useStepOneScreensList } from '../../../../hooks/useStepOneScreensList';
import { getResolutionType } from '../../../../helpers';
import { locationCityAddressToString } from '../../../../../../constants/helpers';
import { OrientationEnum } from '../../../../../../../../common/types';

import iconTags from '../../../../../../assets/iconTags_32x32.svg';

import css from '../../style.m.css';

const orientationTooltip = (orientation: OrientationEnum, t: TFunction) => {
    switch (orientation) {
        case OrientationEnum.LANDSCAPE:
            return t('horizontalOrientation');
        case OrientationEnum.PORTRAIT:
            return t('portraitOrientation');
        case OrientationEnum['LANDSCAPE_FLIPPED']:
            return t('horizontalFlippedOrientation');
        case OrientationEnum['PORTRAIT_FLIPPED']:
            return t('portraitFlippedOrientation');
    }
};

const StepOneScreensList: React.FC = () => {
    const {
        activeScreenId,
        filteredUnselectedScreens,
        selectActiveScreen,
        selectAllScreens,
        selectScreen,
        unselectAllScreens,
        isAllScreensSelectButtonDisabled,
        isAllScreensSelected,
        isScreenWithAnotherResolutionOrOrientation,
        t,
    } = useStepOneScreensList();

    return (
        <div className={css.screensFrame}>
            <div className={css.screensFrameCheckbox}>
                <h2>{t('screens')}</h2>

                <div
                    style={{ display: 'flex', alignItems: 'center', gap: '6px', cursor: 'pointer' }}
                    onClick={
                        isAllScreensSelectButtonDisabled
                            ? undefined
                            : isAllScreensSelected
                              ? unselectAllScreens()
                              : selectAllScreens()
                    }
                >
                    <h2>{t('selectAll')}</h2>
                    <SelectButton
                        active={isAllScreensSelected}
                        disabled={isAllScreensSelectButtonDisabled}
                        onClick={
                            isAllScreensSelectButtonDisabled
                                ? undefined
                                : isAllScreensSelected
                                  ? unselectAllScreens()
                                  : selectAllScreens()
                        }
                    />
                </div>
            </div>

            <ScreenTagsSelect />

            <Droppable droppableId="unselectedScreens">
                {(provided) => (
                    <div className={css.wrapScreensFrameList}>
                        <ul className={css.screensFrameList} ref={provided.innerRef} {...provided.droppableProps}>
                            {filteredUnselectedScreens.map(
                                ({ id, name, status, location, orientation, resolution, tags }, index) => (
                                    <Draggable key={id} draggableId={id} index={index}>
                                        {(provided) => (
                                            <li
                                                className={clsx(
                                                    css.frame,
                                                    activeScreenId === id && css.active,
                                                    isScreenWithAnotherResolutionOrOrientation(id) && css.disabled,
                                                    // status === 'online' && css.folderItemOnline,
                                                    // ['offline', 'pending'].includes(status) && css.folderItemOffline,
                                                    // status === 'error' && css.folderItemError,
                                                )}
                                                ref={provided.innerRef}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                                onClick={selectActiveScreen(id)}
                                            >
                                                <div>
                                                    <div
                                                        className={clsx(
                                                            css.wrapperIconStatus,
                                                            status === 'online' && css.iconStatusOn,
                                                            ['offline', 'pending'].includes(status) &&
                                                                css.iconStatusOff,
                                                            status === 'error' && css.iconStatusError,
                                                        )}
                                                    />
                                                </div>

                                                <div
                                                    className={clsx(
                                                        css.frameInfo,
                                                        isScreenWithAnotherResolutionOrOrientation(id) && css.disabled,
                                                    )}
                                                >
                                                    <p className={css.frameName}>{name}</p>
                                                    <p className={css.frameItem}>{location?.name}</p>
                                                    <p className={css.frameAddress}>
                                                        {locationCityAddressToString(location?.city, location?.address)}
                                                    </p>
                                                    <Tooltip
                                                        placement="right"
                                                        title={orientationTooltip(orientation, t)}
                                                    >
                                                        <p
                                                            className={clsx(
                                                                css.frameScreenResolution,
                                                                orientation === OrientationEnum.PORTRAIT &&
                                                                    css.screenResolutionTablet,
                                                            )}
                                                        >
                                                            {clsx(resolution?.width, 'x', resolution?.height)}{' '}
                                                            <span>
                                                                {getResolutionType({
                                                                    width: resolution?.width,
                                                                    height: resolution?.height,
                                                                })}
                                                            </span>
                                                        </p>
                                                    </Tooltip>
                                                </div>
                                                <div
                                                    className={clsx(
                                                        css.frameTags,
                                                        isScreenWithAnotherResolutionOrOrientation(id) && css.disabled,
                                                    )}
                                                >
                                                    <ul className={css.tagButton}>
                                                        {tags.length > 0 ? (
                                                            tags
                                                                .sort((a, b) => (a.name > b.name ? 1 : -1))
                                                                .map(({ name }, index) => (
                                                                    <li key={index}>
                                                                        <button>{name}</button>
                                                                    </li>
                                                                ))
                                                        ) : (
                                                            <div className={css.fieldTagButton}>
                                                                <img src={iconTags} alt="" />
                                                                <p className={css.tagButtonInfo}>
                                                                    {t('addTagsToTheDeviceInTheScreenSettings')}
                                                                </p>
                                                            </div>
                                                        )}
                                                    </ul>
                                                </div>
                                                {!isScreenWithAnotherResolutionOrOrientation(id) && (
                                                    <Tooltip title="Выбрать экран" placement="left">
                                                        <SelectButton onClick={selectScreen(id)} />
                                                    </Tooltip>
                                                )}
                                                {isScreenWithAnotherResolutionOrOrientation(id) && (
                                                    <Tooltip title={t('youCannotSelectThisScreen')} placement="top">
                                                        <div className={css.iconInfo} />
                                                    </Tooltip>
                                                )}
                                            </li>
                                        )}
                                    </Draggable>
                                ),
                            )}
                            {provided.placeholder}
                        </ul>
                    </div>
                )}
            </Droppable>
        </div>
    );
};

export default StepOneScreensList;
