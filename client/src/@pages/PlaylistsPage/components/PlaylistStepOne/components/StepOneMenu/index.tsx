import React from 'react';
import { X } from '@phosphor-icons/react';
import { Droppable } from '@hello-pangea/dnd';
import { clsx } from 'clsx';

import BroadcastBreakAlert from '../BroadcastBreakAlert';
import { useStepOneMenu } from '../../../../hooks/useStepOneMenu';
import { getResolutionType } from '../../../../helpers';
import { locationCityAddressToString } from '../../../../../../constants/helpers';
import { useUpdatePlaylistButtons } from '../../../../hooks/useUpdatePlaylistButtons';
import { OrientationEnum } from '../../../../../../../../common/types';

import css from '../../style.m.css';

const StepOneMenu: React.FC = () => {
    const {
        selectedScreens,
        unselectAllScreens,
        unselectScreen,
        moveToPlaylistStepTwo,
        isBroadcastBreakAlertShown,
        onSubmitBroadcastBreakAlert,
        onCancelBroadcastBreakAlert,
        unselectedScreensNumber,
        t,
    } = useStepOneMenu();

    const { updatePlaylist, isCreatePlaylistStep } = useUpdatePlaylistButtons();

    return (
        <div className={css.mainRightMenu_2}>
            <BroadcastBreakAlert
                isShown={isBroadcastBreakAlertShown}
                onSubmit={onSubmitBroadcastBreakAlert}
                onCancel={onCancelBroadcastBreakAlert}
                screensNumber={unselectedScreensNumber}
            />
            <div className={css.wrapMainRightMenu_2}>
                <h2>{t('screensSelected')}</h2>
                <Droppable droppableId="selectedScreens">
                    {(provided) => (
                        <div ref={provided.innerRef} className={css.dropArea} {...provided.droppableProps}>
                            {selectedScreens.length > 0 ? (
                                <>
                                    <div className={css.wrapperBtnCancelSelect}>
                                        <button
                                            type="button"
                                            className={css.btnCancelSelect}
                                            onClick={unselectAllScreens}
                                        >
                                            {/* {t('deselect')} */}
                                            {t('unpinAll')}
                                        </button>
                                        <X cursor="pointer" weight="bold" size={24} onClick={unselectAllScreens} />
                                    </div>
                                    <div className={css.mainRightMenu_2_selectScreen}>
                                        {selectedScreens.map(
                                            ({
                                                id,
                                                name,
                                                status,
                                                location,
                                                orientation,
                                                resolution: { width, height },
                                                tags,
                                            }) => (
                                                <div key={id} className={css.frame}>
                                                    <div
                                                        className={clsx(
                                                            css.wrapperIconStatus,
                                                            status === 'online' && css.iconStatusOn,
                                                            ['offline', 'pending'].includes(status) &&
                                                                css.iconStatusOff,
                                                            status === 'error' && css.iconStatusError,
                                                        )}
                                                    />

                                                    <div className={css.frameSelect}>
                                                        <div className={css.frameInfo} style={{ width: 'auto' }}>
                                                            <p className={css.frameName}>{name}</p>
                                                            <p className={css.frameItem}>{location?.name}</p>
                                                            <p className={css.frameAddress}>
                                                                {locationCityAddressToString(
                                                                    location?.city,
                                                                    location?.address,
                                                                )}
                                                            </p>
                                                            <p
                                                                className={clsx(
                                                                    css.frameScreenResolution,
                                                                    orientation === OrientationEnum.PORTRAIT &&
                                                                        css.screenResolutionTablet,
                                                                )}
                                                            >
                                                                {`${width} × ${height} `}
                                                                <span>{getResolutionType({ width, height })}</span>
                                                            </p>
                                                        </div>
                                                        <div className={css.frameTags}>
                                                            <ul className={css.tagButton}>
                                                                {tags.map(({ name }, index) => (
                                                                    <li className={css.tagButtonLine} key={index}>
                                                                        <button>{name}</button>
                                                                    </li>
                                                                ))}
                                                            </ul>
                                                        </div>
                                                    </div>

                                                    <X
                                                        style={{ width: '24px', height: '24px' }}
                                                        cursor={'pointer'}
                                                        weight="bold"
                                                        onClick={unselectScreen(id)}
                                                    />
                                                </div>
                                            ),
                                        )}
                                    </div>
                                </>
                            ) : (
                                <div className={css.mainRightMenu_2Empty}>
                                    <p className={css.mainRightMenu_2_item}>
                                        {t('textOne')}
                                        {t('textTwo')}
                                        {t('textThree')}
                                    </p>
                                </div>
                            )}
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
            </div>
            <div className={css.btnNextStepField}>
                <button className={css.btnSaveChangeExit} onClick={updatePlaylist}>
                    {isCreatePlaylistStep ? `${t('btnSaveDraft')}` : `${t('btnSaveChangesExit')}`}
                </button>
                <button className={css.btnNextStep} onClick={moveToPlaylistStepTwo}>
                    <img alt="" />
                    {t('btnSetUpContent')}
                </button>
            </div>
        </div>
    );
};

export default StepOneMenu;
