import { useEffect, useCallback, useMemo, ReactElement, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';

import { SelectTagType } from '../../../../../ui-kit';
import { selectActiveLocationId, selectScreens, selectSelectedScreensIds } from '../../../../ScreensPage/selectors';
import { selectActivePlaylistId, selectPlaylists } from '../../../selectors';
import { setPlaylistDtoFilteringTags } from '../../../actions';
import { ScreenType } from '../../../../../../../common/types';
import { usePlaylistStep } from '../../../hooks/usePlaylistStep';

type ReturnValue = {
    isAllTagsSelected: boolean;
    tags: SelectTagType[];
    filteringTags: SelectTagType[];
    onSelectScreenTag: (event: SelectChangeEvent) => void;
    onDeleteScreenTag: (tagName: string) => void;
    isTagsSelectDisabled: boolean;
};

export const useScreenTagsSelect = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const { playlistStep } = usePlaylistStep();

    const activePlaylistId = useSelector(selectActivePlaylistId);
    const activeLocationId = useSelector(selectActiveLocationId);
    const playlists = useSelector(selectPlaylists);
    const screens = useSelector(selectScreens);
    const selectedScreensIds = useSelector(selectSelectedScreensIds);

    const unselectedScreens = useMemo(
        () =>
            screens
                .filter(({ location }) =>
                    activeLocationId !== 'all' ? location?.id === activeLocationId : location?.id,
                )
                .filter(({ id }) => !selectedScreensIds.includes(id)),
        [activeLocationId, screens, selectedScreensIds],
    );

    const tags = useMemo(
        () => [
            { id: 'all', name: t('selectAll') },
            ...screens
                .flatMap((screen: ScreenType) => screen.tags.map(({ id, name }) => ({ id, name })))
                .reduce(
                    (acc, curr) =>
                        acc.some(({ name }) => name === curr.name) ? acc : [...acc, { id: curr.id, name: curr.name }],
                    [],
                )
                .sort((a, b) => (a.name > b.name ? 1 : -1)),
        ],
        [screens, t],
    );

    const savedFilteringTagNames = useMemo(
        () =>
            playlistStep.includes('editing')
                ? (playlists.find(({ id }) => id === activePlaylistId)?.filteringTags ?? [])
                : [],
        [activePlaylistId, playlistStep, playlists],
    );

    const [filteringTags, setFilteringTags] = useState<SelectTagType[]>(
        tags.filter(({ name }) => savedFilteringTagNames.includes(name)),
    );

    useEffect(() => {
        dispatch(setPlaylistDtoFilteringTags(filteringTags.map(({ name }) => name)));
    }, [dispatch, filteringTags]);

    const isAllTagsSelected = useMemo(
        () => tags.slice(1).length === filteringTags.length,
        [filteringTags.length, tags],
    );

    const onSelectScreenTag = useCallback(
        (_: SelectChangeEvent, element?: ReactElement) => {
            const screenTagId = element?.props.id;

            if (screenTagId === 'all') {
                setFilteringTags(isAllTagsSelected ? [] : tags.slice(1));
            } else {
                const isTagSelected = filteringTags.some(({ id }) => id === screenTagId);

                setFilteringTags(
                    isTagSelected
                        ? filteringTags.filter(({ id }) => id !== screenTagId)
                        : [...filteringTags, { id: screenTagId, name: element?.props.value }],
                );
            }
        },
        [filteringTags, isAllTagsSelected, tags],
    );

    const onDeleteScreenTag = useCallback(
        (screenTagId: string) => {
            setFilteringTags(filteringTags.filter(({ id }) => id !== screenTagId));
        },
        [filteringTags],
    );

    const isTagsSelectDisabled = useMemo(
        () => unselectedScreens.length === 0 || tags.length === 0,
        [tags.length, unselectedScreens.length],
    );

    return {
        isAllTagsSelected,
        tags,
        filteringTags,
        onSelectScreenTag,
        onDeleteScreenTag,
        isTagsSelectDisabled,
    };
};
