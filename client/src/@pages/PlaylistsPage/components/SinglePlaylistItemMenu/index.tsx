import React from 'react';
import { clsx } from 'clsx';

import contentPreview from '../../../../assets/Content_preview.png';
import { useSinglePlaylistItemMenu } from '../../hooks/useSinglePlaylistItemMenu';

import css from '../../style.m.css';

const SinglePlaylistItemMenu: React.FC = () => {
    const {
        playlistItemSrc,
        isImageType,
        playlistItemName,
        onPlaylistItemNameChange,
        updatePlaylistItemName,
        isUpdatePlaylistItemButtonDisabled,
        openDeleteContentAlert,
        playlistItemFileSize,
        playlistItemFileTypeAndExtension,
        playlistItemDimensions,
        playlistItemOrientation,
        playlistItemDuration,
        playlistItemCreatedAt,
        playlistItemUpdatedAt,
        playlistsInUsage,
        isPlaylistsListOpen,
        showHidePlaylistsList,
        goToPlaylist,
        isInfoShown,
        isDimensionsShown,
        showHideInfo,
        t,
    } = useSinglePlaylistItemMenu();

    return (
        <form className={css.mainRightMenuContent} onSubmit={(e) => e.preventDefault()}>
            <div className={css.wrapMainRightMenu}>
                <h2>{t('contentSettings')}</h2>
                {isImageType && playlistItemSrc ? (
                    <img
                        className={css.playlistItemImageRightMenu}
                        src={String(playlistItemSrc)}
                        width="292"
                        height="164"
                        alt=""
                    />
                ) : !isImageType ? (
                    <div className={css.wrapperVideo}>
                        <video
                            className={css.playlistItemImageRightMenu}
                            src={String(playlistItemSrc)}
                            width="292"
                            height="164"
                        />
                    </div>
                ) : (
                    <img
                        className={css.playlistItemImageRightMenu}
                        src={contentPreview}
                        width="292"
                        height="164"
                        alt=""
                    />
                )}

                <label>{t('name')}</label>
                <br />
                <input
                    className={css.rightMenuNameContent}
                    placeholder={t('nameInput') as string}
                    value={playlistItemName}
                    onChange={onPlaylistItemNameChange}
                    maxLength={125}
                />
                <div className={css.wrapperInfo}>
                    <div className={css.infoHeader} onClick={showHideInfo}>
                        <h3 className={css.mainRightMenuInfo}>{t('info')}</h3>
                        <span className={isInfoShown ? css.iconArrowInfoUp : css.iconArrowInfoDown} />
                    </div>
                    {isInfoShown && (
                        <div>
                            <p className={css.mainRightMenuInfoItem}>
                                {t('type')}
                                <span>{playlistItemFileTypeAndExtension}</span>
                            </p>
                            <p className={css.mainRightMenuInfoItem}>
                                {t('size')}
                                <span>{playlistItemFileSize}</span>
                            </p>
                            <p className={css.mainRightMenuInfoItem}>
                                {t('durationRight')}
                                <span>{playlistItemDuration}</span>
                            </p>
                            {isDimensionsShown && (
                                <>
                                    <p className={css.mainRightMenuInfoItem}>
                                        {t('resolution')}
                                        <span>{playlistItemDimensions}</span>
                                    </p>
                                    <p className={css.mainRightMenuInfoItem}>
                                        {t('orientation')}
                                        <span>{playlistItemOrientation}</span>
                                    </p>
                                </>
                            )}
                            <p className={css.mainRightMenuInfoItem}>
                                {t('created')}
                                <span>{playlistItemCreatedAt}</span>
                            </p>
                            <p className={clsx(css.mainRightMenuInfoItem, css.mainRightMenuInfoItemLast)}>
                                {t('edited')}
                                <span>{playlistItemUpdatedAt}</span>
                            </p>
                        </div>
                    )}
                </div>
                <div className={css.wrapMainRightMenuInfoAll}>
                    <div className={css.infoHeader} onClick={showHidePlaylistsList}>
                        <h3>
                            {t('usedInPlaylists')}
                            &ensp;
                            <span>{playlistsInUsage.length}</span>
                        </h3>
                        <span className={isPlaylistsListOpen ? css.iconArrowInfoUp : css.iconArrowInfoDown} />
                    </div>
                    <div className={css.mainRightMenuInfoAll}>
                        {isPlaylistsListOpen && (
                            <ul>
                                {playlistsInUsage.map(({ id, title }, index) => (
                                    <li key={index}>
                                        <p>{title}</p>
                                        <a onClick={goToPlaylist(id)}>{t('goTo')}</a>
                                    </li>
                                ))}
                            </ul>
                        )}
                    </div>
                </div>
                <a>
                    <button
                        type="button"
                        className={clsx(css.btnPlaylist, css.deletePlaylist, css.deletePlaylistContent)}
                        onClick={openDeleteContentAlert}
                    >
                        <img alt="" />
                        {t('btnDeleteContent')}
                    </button>
                </a>
            </div>
            {!isUpdatePlaylistItemButtonDisabled && (
                <div className={css.btnSaveSettingsField}>
                    <button
                        type="submit"
                        className={clsx(css.btnPlaylist, css.saveSettings)}
                        onClick={updatePlaylistItemName}
                    >
                        {t('btnSaveSetting')}
                    </button>
                </div>
            )}
        </form>
    );
};

export default SinglePlaylistItemMenu;
