import React from 'react';
import { useCustomRouter } from '../../../../hooks/useCustomRouter';

export const PriorityButton: React.FC = () => {
    const { pushToPage } = useCustomRouter();

    return (
        <a
            className="h-10 bg-neutral-100 hover:bg-neutral-200 flex items-center gap-2 px-4 rounded-xl transition font-semibold"
            onClick={() => pushToPage('playlists/priority')}
        >
            <svg xmlns="http://www.w3.org/2000/svg" className="w-6 h-6" viewBox="0 0 20 20">
                <path
                    fill="currentColor"
                    d="M3 3a1 1 0 0 0 0 2h11a1 1 0 1 0 0-2zm0 4a1 1 0 0 0 0 2h5a1 1 0 0 0 0-2zm0 4a1 1 0 1 0 0 2h4a1 1 0 1 0 0-2zm10 5a1 1 0 1 0 2 0v-5.586l1.293 1.293a1 1 0 0 0 1.414-1.414l-3-3a1 1 0 0 0-1.414 0l-3 3a1 1 0 1 0 1.414 1.414L13 10.414z"
                />
            </svg>
            <div>Приоритетность</div>
        </a>
    );
};
