import React from 'react';

import { useDeleteMultiplyPlaylistsAlert } from '../../hooks/useDeleteMultiplyPlaylistsAlert';

import css from '../../style.m.css';

const DeleteMultiplyPlaylistAlert: React.FC = () => {
    const { selectedPlaylistsNumber, deletePlaylist, doDeletePlaylist, closeDeletePlaylistAlert, t } =
        useDeleteMultiplyPlaylistsAlert();

    return (
        <div className={css.popupDeletePlaylist}>
            <h2>
                {t('delPlaylist')} ({selectedPlaylistsNumber})
            </h2>
            <p>
                {t('textDelPlaylistOne')}
                <br />
                {t('textDelPlaylistThree')}
            </p>
            <div className={css.containerBtn}>
                <a className={css.btnCancel}>
                    <button className={css.buttonCancel} onClick={closeDeletePlaylistAlert}>
                        {t('btnCancel')}
                    </button>
                </a>
                <button className={css.buttonDelete} onClick={doDeletePlaylist} disabled={deletePlaylist}>
                    {t('btnDelete')}
                </button>
            </div>
        </div>
    );
};

export default DeleteMultiplyPlaylistAlert;
