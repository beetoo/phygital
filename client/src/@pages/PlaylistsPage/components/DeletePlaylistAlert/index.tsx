import React from 'react';

import ModalTemplate from '../../../../components/ModalTemplate';
import { useDeletePlaylistAlert } from '../../hooks/useDeletePlaylistAlert';

import css from '../../style.m.css';

const DeletePlaylistAlert: React.FC = () => {
    const { isDeletePlaylistAlertOpen, deletePlaylist, doDeletePlaylist, closeDeletePlaylistAlert, t } =
        useDeletePlaylistAlert();

    return (
        <ModalTemplate isOpen={isDeletePlaylistAlertOpen}>
            <div className={css.popupDeletePlaylist}>
                <h2>{t('delPlaylist')}</h2>
                <p>
                    {t('textDelPlaylistOne')}
                    <br />
                    {t('textDelPlaylistTwo')}
                </p>
                <div className={css.containerBtn}>
                    <a className={css.btnCancel}>
                        <button className={css.buttonCancel} onClick={closeDeletePlaylistAlert}>
                            {t('btnCancel')}
                        </button>
                    </a>
                    <button className={css.buttonDelete} onClick={doDeletePlaylist} disabled={deletePlaylist}>
                        {t('btnDelete')}
                    </button>
                </div>
            </div>
        </ModalTemplate>
    );
};

export default DeletePlaylistAlert;
