import React, { Dispatch, SetStateAction } from 'react';

import { SelectButton, Tooltip } from '../../../../ui-kit';

import { usePlaylistItemsList } from '../../hooks/usePlaylistItemsList';
import PlaylistItemCard from '../PlaylistItemCard';

import css from '../../style.m.css';

type Props = {
    setContentRef: Dispatch<SetStateAction<HTMLLIElement | null>>;
};

const PlaylistItemsList: React.FC<Props> = ({ setContentRef }) => {
    const {
        playlistItems,
        isAllPlaylistItemsSelected,
        selectAllPlaylistItems,
        unselectAllPlaylistItems,
        isPlaylistItemDisabled,
        t,
    } = usePlaylistItemsList();

    return (
        <div className={css.contentPlaylist}>
            <div className={css.contentSelectAll}>
                <h2>{t('content')}</h2>

                <Tooltip
                    title={t('selectAll')}
                    placement="top"
                    disableHoverListener={playlistItems.length === 0 || isPlaylistItemDisabled}
                >
                    <SelectButton
                        active={isAllPlaylistItemsSelected}
                        disabled={isPlaylistItemDisabled}
                        onClick={isAllPlaylistItemsSelected ? unselectAllPlaylistItems : selectAllPlaylistItems}
                    />
                </Tooltip>
            </div>
            <div className={css.contentPlaylistImages}>
                <ul className={css.contentPlaylistImage}>
                    {playlistItems.map(({ id, name, delay, content }, index) => (
                        <PlaylistItemCard key={id} {...{ id, name, delay, content, index, setContentRef }} />
                    ))}
                </ul>
            </div>
        </div>
    );
};

export default PlaylistItemsList;
