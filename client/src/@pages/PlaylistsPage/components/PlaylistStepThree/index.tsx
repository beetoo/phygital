import React, { memo } from 'react';

import StepThreeScheduleTable from './components/StepThreeScheduleTable';
import StepThreeMenu from './components/StepThreeMenu';

import css from './style.m.css';

const PlaylistStepThree: React.FC = () => (
    <>
        <div className={css.createPlaylistCenterBlock}>
            <StepThreeScheduleTable />
        </div>
        <StepThreeMenu />
    </>
);

export default memo(PlaylistStepThree);
