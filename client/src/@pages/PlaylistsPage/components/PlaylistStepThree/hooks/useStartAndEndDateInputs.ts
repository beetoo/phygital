import { ChangeEvent, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { format, differenceInCalendarDays } from 'date-fns';

import { selectPlaylistDto } from '../../../selectors';
import { selectActiveScheduleScreenId } from '../../../../DxSchedulePage/selectors';
import { setPlaylistDtoSchedules } from '../../../actions';
import { CreatePlaylistScheduleDto } from '../../../../../../../server/src/models/playlist-schedule/dto/create-playlist-schedule.dto';

type ReturnValue = {
    initialDate: string;
    startDay: string;
    endDay: string;
    broadcastAlways: boolean;
    onBroadcastAlwaysChange: () => void;
    onStartDateChange: (e: ChangeEvent<HTMLTextAreaElement>) => void;
    onEndDateChange: (e: ChangeEvent<HTMLTextAreaElement>) => void;
    isEndDateLessThanStartDate: boolean;
};

export const useStartAndEndDateInputs = (): ReturnValue => {
    const initialDate = format(new Date(), 'yyyy-MM-dd');

    const dispatch = useDispatch();

    const { playlistDtoSchedules } = useSelector(selectPlaylistDto);
    const activeScheduleScreenId = useSelector(selectActiveScheduleScreenId);

    const activePlaylistSchedule = useMemo(
        () => playlistDtoSchedules.find(({ screenId }) => screenId === activeScheduleScreenId),
        [activeScheduleScreenId, playlistDtoSchedules],
    );

    const activeBroadcastAlways = useMemo(
        () => activePlaylistSchedule?.broadcastAlways ?? false,
        [activePlaylistSchedule?.broadcastAlways],
    );

    const [startDay, setStartDay] = useState(activePlaylistSchedule?.startDay ?? '');
    const [endDay, setEndDay] = useState(activePlaylistSchedule?.endDay ?? '');
    const [broadcastAlways, setBroadcastAlways] = useState(false);

    const updateBroadcastAlways = useCallback(() => {
        const updatedPlaylistDto = playlistDtoSchedules.map((schedule) => ({
            ...schedule,
            broadcastAlways: !broadcastAlways,
        })) as CreatePlaylistScheduleDto[];

        dispatch(setPlaylistDtoSchedules(updatedPlaylistDto));
    }, [broadcastAlways, dispatch, playlistDtoSchedules]);

    const onBroadcastAlwaysChange = useCallback(() => {
        setBroadcastAlways((prev) => !prev);
        updateBroadcastAlways();
    }, [updateBroadcastAlways]);

    const updateStartDate = useCallback(
        (startDayValue: string) => {
            const updatedPlaylistDto = playlistDtoSchedules.map((schedule) => ({
                ...schedule,
                startDay: startDayValue,
            }));

            dispatch(setPlaylistDtoSchedules(updatedPlaylistDto));
        },
        [dispatch, playlistDtoSchedules],
    );

    const onStartDateChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLTextAreaElement>) => {
            const match = value.match(/20[2-9]\d-[0-1]\d-[0-3]\d/);
            if (match) {
                setStartDay(value);
                updateStartDate(value);
            }
        },
        [updateStartDate],
    );

    const updateEndDate = useCallback(
        (endDayValue: string) => {
            const updatedPlaylistDto = playlistDtoSchedules.map((schedule) => {
                return {
                    ...schedule,
                    endDay: endDayValue,
                };
            });

            dispatch(setPlaylistDtoSchedules(updatedPlaylistDto));
        },

        [dispatch, playlistDtoSchedules],
    );

    const onEndDateChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLTextAreaElement>) => {
            const match = value.match(/20[2-9]\d-[0-1]\d-[0-3]\d/);
            if (match) {
                setEndDay(value);
                updateEndDate(value);
            }
        },
        [updateEndDate],
    );

    const prevScheduleScreenId = useRef('');

    useEffect(() => {
        if (prevScheduleScreenId.current !== activeScheduleScreenId) {
            prevScheduleScreenId.current = activeScheduleScreenId;

            setStartDay(activePlaylistSchedule?.startDay ?? '');
            setEndDay(activePlaylistSchedule?.endDay ?? '');
            setBroadcastAlways(activeBroadcastAlways);
        }
    }, [
        activeBroadcastAlways,
        activePlaylistSchedule?.endDay,
        activePlaylistSchedule?.startDay,
        activeScheduleScreenId,
    ]);

    const isEndDateLessThanStartDate = useMemo(
        () => differenceInCalendarDays(new Date(endDay), new Date(startDay)) < 0,
        [endDay, startDay],
    );

    return {
        initialDate,
        startDay,
        endDay,
        broadcastAlways,
        onBroadcastAlwaysChange,
        onStartDateChange,
        onEndDateChange,
        isEndDateLessThanStartDate,
    };
};
