import { ChangeEvent, useCallback, useState, useMemo, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { setPlaylistDtoSchedules } from '../../../actions';
import { selectPlaylistDto } from '../../../selectors';
import { selectActiveScheduleScreenId } from '../../../../DxSchedulePage/selectors';
import { isTimeMatch } from '../../../helpers';

type ReturnValue = {
    startTimeValue: string | undefined;
    endTimeValue: string | undefined;
    isAllDayTranslation: boolean;
    onStartTimeChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onEndTimeChange: (e: ChangeEvent<HTMLInputElement>) => void;
    onAllDayCheckboxChange: () => void;
};

export const useStartAndEndPlaylistTimeInputs = (): ReturnValue => {
    const dispatch = useDispatch();

    const activeScheduleScreenId = useSelector(selectActiveScheduleScreenId);
    const { playlistDtoSchedules } = useSelector(selectPlaylistDto);

    const activePlaylistSchedule = useMemo(
        () => playlistDtoSchedules.find(({ screenId }) => screenId === activeScheduleScreenId),
        [activeScheduleScreenId, playlistDtoSchedules],
    );

    const [startTimeValue, setStartTimeValue] = useState<string>(activePlaylistSchedule?.startTime ?? '');
    const [endTimeValue, setEndTimeValue] = useState<string>(activePlaylistSchedule?.endTime ?? '');
    const [isAllDayTranslation, setIsAllDayTranslation] = useState(true);

    const updateStartTime = useCallback(
        (startTimeValue: string) => {
            const updatedDtoSchedules = playlistDtoSchedules.map((schedule) => ({
                ...schedule,
                startTime: startTimeValue,
            }));

            dispatch(setPlaylistDtoSchedules(updatedDtoSchedules));
        },
        [dispatch, playlistDtoSchedules],
    );

    const onStartTimeChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            setStartTimeValue(value);

            if (isTimeMatch(value)) {
                updateStartTime(value);
            }
        },
        [updateStartTime],
    );

    const updateEndTime = useCallback(
        (endTimeValue: string) => {
            const updatedDtoSchedules = playlistDtoSchedules.map((schedule) => ({
                ...schedule,
                endTime: endTimeValue,
            }));

            dispatch(setPlaylistDtoSchedules(updatedDtoSchedules));
        },
        [dispatch, playlistDtoSchedules],
    );

    const onEndTimeChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            setEndTimeValue(value);

            if (isTimeMatch(value)) {
                updateEndTime(value);
            }
        },
        [updateEndTime],
    );

    const updateStartAndEndTime = useCallback(
        (startTimeValue: string, endTimeValue: string) => {
            const updatedDtoSchedules = playlistDtoSchedules.map((schedule) => ({
                ...schedule,
                startTime: startTimeValue,
                endTime: endTimeValue,
            }));

            dispatch(setPlaylistDtoSchedules(updatedDtoSchedules));
        },
        [dispatch, playlistDtoSchedules],
    );

    const onAllDayCheckboxChange = useCallback(() => {
        setIsAllDayTranslation((prev) => !prev);
        if (!isAllDayTranslation) {
            setStartTimeValue('00:00');
            setEndTimeValue('00:00');
            updateStartAndEndTime('00:00', '00:00');
        }
    }, [isAllDayTranslation, updateStartAndEndTime]);

    const prevScheduleScreenId = useRef('');

    useEffect(() => {
        if (prevScheduleScreenId.current !== activeScheduleScreenId) {
            prevScheduleScreenId.current = activeScheduleScreenId;
            setStartTimeValue(activePlaylistSchedule?.startTime ?? '');
            setEndTimeValue(activePlaylistSchedule?.endTime ?? '');
        }
    }, [activePlaylistSchedule?.endTime, activePlaylistSchedule?.startTime, activeScheduleScreenId]);

    useEffect(() => {
        const matched = isTimeMatch(startTimeValue) && isTimeMatch(endTimeValue);

        if (matched) {
            setIsAllDayTranslation(startTimeValue === '00:00' && endTimeValue === '00:00');
        }
    }, [endTimeValue, startTimeValue]);

    return {
        startTimeValue,
        endTimeValue,
        isAllDayTranslation,
        onStartTimeChange,
        onEndTimeChange,
        onAllDayCheckboxChange,
    };
};
