import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectModalData } from '../../../../../components/ModalsContainer/selectors';
import { setHideModal } from '../../../../../components/ModalsContainer/actions';
import { useCustomRouter } from '../../../../../hooks/useCustomRouter';

type ReturnValue = {
    savePlaylistOnEditStep: () => void;
    goToSelectScreens: () => void;
    t: TFunction;
};

export const useDraftPlaylistSaveModal = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const dispatch = useDispatch();

    const { pushToPage } = useCustomRouter();

    const { savePlaylistOnEditStep } = (useSelector(selectModalData) as { savePlaylistOnEditStep: () => void }) || {
        savePlaylistOnEditStep: undefined,
    };

    const goToSelectScreens = useCallback(() => {
        pushToPage('playlists/editing_one');
        dispatch(setHideModal());
    }, [dispatch, pushToPage]);

    return {
        savePlaylistOnEditStep,
        goToSelectScreens,
        t,
    };
};
