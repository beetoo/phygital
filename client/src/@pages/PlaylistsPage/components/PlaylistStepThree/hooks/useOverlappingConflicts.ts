import { useCallback, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { addDays, areIntervalsOverlapping, format, isBefore, isWithinInterval, startOfDay } from 'date-fns';

import { formatPlaylistSchedules } from '../../../helpers';
import { selectPlaylistDto } from '../../../selectors';
import { selectActiveScheduleScreenId } from '../../../../DxSchedulePage/selectors';
import { selectScreens } from '../../../../ScreensPage/selectors';
import { addTimeFromAnotherDate, toDate, startTimeMoreThanEndTime } from '../../../../../helpers';
import { usePlaylistStep } from '../../../hooks/usePlaylistStep';
import { frequencyFromByDay } from '../../../../../../../common/helpers';

type ReturnValue = {
    isOverlappingConflict: boolean;
};

export const useOverlappingConflicts = (): ReturnValue => {
    const { isCreatePlaylistStep, isEditPlaylistStep } = usePlaylistStep();

    const activeScheduleScreenId = useSelector(selectActiveScheduleScreenId);
    const screens = useSelector(selectScreens);
    const { playlistDtoSchedules } = useSelector(selectPlaylistDto);

    const activeScreen = useMemo(
        () => screens.find(({ id }) => id === activeScheduleScreenId),
        [activeScheduleScreenId, screens],
    );

    const { startDay, endDay, startTime, endTime, byDay } = useMemo(
        () =>
            playlistDtoSchedules.find(({ screenId }) => screenId === activeScheduleScreenId) ?? {
                startDay: '',
                endDay: '',
                startTime: '',
                endTime: '',
                byDay: '',
            },
        [activeScheduleScreenId, playlistDtoSchedules],
    );

    const startDate = toDate(startDay, startTime);
    const endDate = toDate(endDay, endTime);
    const isEndDateBeforeStartDate = isBefore(endDate, startDate);

    const comparisonSchedules = useMemo(
        () => formatPlaylistSchedules(activeScreen?.schedules ?? []),
        [activeScreen?.schedules],
    );

    const areOverlapping = useCallback(
        (scheduleStartDate: Date, scheduleEndDate: Date) => {
            const scheduleStartTime = format(scheduleStartDate, 'HH:mm');
            const scheduleEndTime = format(scheduleEndDate, 'HH:mm');

            if (scheduleStartTime === scheduleEndTime) {
                return true;
            }

            const isActiveStartTimeMoreThanEndTime = startTimeMoreThanEndTime(startDate, endDate);
            const isScheduleStartTimeMoreThanEndTime = startTimeMoreThanEndTime(scheduleStartDate, scheduleEndDate);

            if (isActiveStartTimeMoreThanEndTime && isScheduleStartTimeMoreThanEndTime) {
                return true;
            }

            scheduleStartDate = addTimeFromAnotherDate(startDay, scheduleStartTime);
            scheduleEndDate = addTimeFromAnotherDate(endDay, scheduleEndTime);

            if (!isActiveStartTimeMoreThanEndTime) {
                return areIntervalsOverlapping(
                    { start: startDate, end: endDate },
                    {
                        start: scheduleStartDate,
                        end: addDays(scheduleEndDate, isScheduleStartTimeMoreThanEndTime ? 1 : 0),
                    },
                );
            }

            if (isActiveStartTimeMoreThanEndTime && !isScheduleStartTimeMoreThanEndTime) {
                const thisDayOverlapping = areIntervalsOverlapping(
                    { start: startDate, end: endDate },
                    { start: scheduleStartDate, end: scheduleEndDate },
                );

                const nextDayOverlapping = areIntervalsOverlapping(
                    { start: startDate, end: endDate },
                    { start: addDays(scheduleStartDate, 1), end: addDays(scheduleEndDate, 1) },
                );

                return thisDayOverlapping || nextDayOverlapping;
            }

            return false;
        },
        [endDate, endDay, startDate, startDay],
    );

    const isOverlappingConflict = useMemo(() => {
        const frequency = frequencyFromByDay(byDay);

        if (isCreatePlaylistStep) {
            if (comparisonSchedules.length === 0) {
                return false;
            }

            if (startTime === endTime) {
                return true;
            }

            if (frequency === 'WEEKLY') {
                const withoutFrequencyOverlapping = comparisonSchedules
                    // если частота Без повтора
                    .some((schedule) => {
                        return areIntervalsOverlapping(
                            {
                                start: startDate,
                                end: addDays(endDate, isEndDateBeforeStartDate ? 1 : 0),
                            },
                            {
                                start: toDate(schedule.startDay, schedule.startTime),
                                end: toDate(schedule.endDay, schedule.endTime),
                            },
                        );
                    });
                if (withoutFrequencyOverlapping) return true;

                // если частота Каждый день
                const dailyOverlapping = comparisonSchedules
                    // дата нового расписания входит в интервал дней работы расписаний с частотой Каждый день
                    .filter((schedule) =>
                        isWithinInterval(startDate, {
                            start: startOfDay(toDate(schedule.startDay, schedule.startTime)),
                            end: startOfDay(toDate(schedule.endDay, schedule.endTime)),
                        }),
                    )
                    .some((schedule) =>
                        areOverlapping(
                            toDate(schedule.startDay, schedule.startTime),
                            toDate(schedule.endDay, schedule.endTime),
                        ),
                    );
                if (dailyOverlapping) return true;

                // если частота Каждую неделю
                // const weeklyOverlapping = comparisonSchedules
                //     .filter(({ frequency }) => frequency === 'WEEKLY')
                //     // дата нового расписания входит в интервал дней работы других расписаний
                //     .filter((schedule) =>
                //         isWithinInterval(startDate, {
                //             start: startOfDay(schedule.startDate),
                //             end: startOfDay(addDays(schedule.endDate, 1)),
                //         }),
                //     )
                //     .some((schedule) => areOverlapping(schedule.startDate, schedule.endDate));
            }
        }

        if (isEditPlaylistStep) {
            if (comparisonSchedules.length === 1) {
                return false;
            }
        }

        return false;
    }, [
        areOverlapping,
        byDay,
        comparisonSchedules,
        endDate,
        endTime,
        isCreatePlaylistStep,
        isEditPlaylistStep,
        isEndDateBeforeStartDate,
        startDate,
        startTime,
    ]);

    return {
        isOverlappingConflict,
    };
};
