import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { AppointmentModel } from '@devexpress/dx-react-scheduler';

import { selectActiveScheduleScreenId } from '../../../../DxSchedulePage/selectors';
import { selectPlaylists } from '../../../selectors';
import { formatPlaylistSchedules } from '../../../helpers';
import { schedulesToAppointments } from '../../../../../helpers';
import { selectCurrentOrganization } from '../../../../ProfilePage/selectors';
import { isProductionEngClient } from '../../../../../constants/environments';

type ReturnValue = {
    appointments: AppointmentModel[];
    locale: 'ru-RU' | 'en-EN' | 'es-ES';
};

export const useDxScheduleTable = (): ReturnValue => {
    const { language } = useSelector(selectCurrentOrganization);
    const activeScreenId = useSelector(selectActiveScheduleScreenId);
    const playlists = useSelector(selectPlaylists);

    const playlistSchedules = useMemo(
        () =>
            formatPlaylistSchedules(
                playlists.filter((playlist) => !playlist.draft).flatMap(({ schedules }) => schedules),
            ),
        [playlists],
    );

    const allAppointments = useMemo(() => schedulesToAppointments(playlistSchedules), [playlistSchedules]);

    const appointments = useMemo(
        () => allAppointments.filter(({ screenId }) => screenId === activeScreenId),
        [activeScreenId, allAppointments],
    );

    const locale = isProductionEngClient || language === 'en' ? 'en-EN' : language === 'ru' ? 'ru-RU' : 'es-ES';

    return {
        appointments,
        locale,
    };
};
