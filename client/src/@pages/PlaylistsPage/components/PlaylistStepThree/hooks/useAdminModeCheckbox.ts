import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { selectAuthUserRole } from '../../../../../selectors';
import { USER_ROLE } from '../../../../../constants';
import { selectPlaylistDto } from '../../../selectors';
import { setPlaylistDtoLog, setPlaylistDtoSync } from '../../../actions';

type ReturnValue = {
    isAdmin: boolean;
    isSyncEnable: boolean;
    onSyncChange: () => void;
    isLogEnable: boolean;
    onLogChange: () => void;
};

export const useAdminModeCheckbox = (): ReturnValue => {
    const dispatch = useDispatch();
    const userRole = useSelector(selectAuthUserRole);
    const isAdmin = userRole === USER_ROLE.ADMIN;
    const { playlistDtoSync, playlistDtoLog } = useSelector(selectPlaylistDto);

    const isSyncEnable = useMemo(() => {
        return playlistDtoSync;
    }, [playlistDtoSync]);

    const isLogEnable = useMemo(() => {
        return playlistDtoLog;
    }, [playlistDtoLog]);

    const onSyncChange = useCallback(() => {
        dispatch(setPlaylistDtoSync(!isSyncEnable));
    }, [dispatch, isSyncEnable]);

    const onLogChange = useCallback(() => {
        dispatch(setPlaylistDtoLog(!isLogEnable));
    }, [dispatch, isLogEnable]);

    return {
        isAdmin,
        isSyncEnable,
        onSyncChange,
        isLogEnable,
        onLogChange,
    };
};
