import { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { selectPlaylistDtoScreens } from '../../../selectors';
import { selectScreens } from '../../../../ScreensPage/selectors';
import { setActiveScheduleScreenId } from '../../../../DxSchedulePage/actions';

type ReturnValue = {
    screensNumber: number;
};

export const useLocationsAndScreensSelects = (): ReturnValue => {
    const dispatch = useDispatch();

    const screens = useSelector(selectScreens);
    const playlistDtoScreens = useSelector(selectPlaylistDtoScreens);

    const firstActiveScreenId = useMemo(
        () => screens.filter(({ id }) => playlistDtoScreens.includes(id))[0]?.id,
        [playlistDtoScreens, screens],
    );

    const screensNumber = playlistDtoScreens.length;

    useEffect(() => {
        if (firstActiveScreenId) {
            dispatch(setActiveScheduleScreenId(firstActiveScreenId));
        }
    }, [dispatch, firstActiveScreenId]);

    return {
        screensNumber,
    };
};
