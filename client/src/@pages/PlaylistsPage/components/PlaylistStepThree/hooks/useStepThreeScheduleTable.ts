import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { AppointmentModel } from '@devexpress/dx-react-scheduler';

import { selectActiveScheduleScreenId } from '../../../../DxSchedulePage/selectors';
import { selectActivePlaylistId, selectPlaylistDto, selectPlaylists } from '../../../selectors';
import { schedulesToAppointments } from '../../../../../helpers';
import { formatPlaylistSchedules } from '../../../helpers';
import { selectCurrentOrganization } from '../../../../ProfilePage/selectors';
import { isProductionEngClient } from '../../../../../constants/environments';
import { usePlaylistStep } from '../../../hooks/usePlaylistStep';

type ReturnValue = {
    appointments: AppointmentModel[];
    locale: 'ru-RU' | 'en-EN' | 'es-ES';
};

export const useStepThreeScheduleTable = (): ReturnValue => {
    const { isEditPlaylistStep } = usePlaylistStep();

    const { language } = useSelector(selectCurrentOrganization);
    const activeScheduleScreenId = useSelector(selectActiveScheduleScreenId);
    const activePlaylistId = useSelector(selectActivePlaylistId);
    const { playlistDtoSchedules } = useSelector(selectPlaylistDto);
    const playlists = useSelector(selectPlaylists);

    const playlistSchedules = useMemo(
        () =>
            formatPlaylistSchedules(
                playlists
                    .filter((playlist) => !playlist.draft)
                    .filter(({ id }) => (isEditPlaylistStep ? id !== activePlaylistId : true))
                    .flatMap(({ schedules }) => schedules),
            ),
        [activePlaylistId, isEditPlaylistStep, playlists],
    );

    const allAppointments = useMemo(
        () => schedulesToAppointments([...playlistDtoSchedules, ...playlistSchedules]),
        [playlistDtoSchedules, playlistSchedules],
    );

    const appointments = useMemo(
        () => allAppointments.filter(({ screenId }) => screenId === activeScheduleScreenId),
        [activeScheduleScreenId, allAppointments],
    );

    const locale = isProductionEngClient || language === 'en' ? 'en-EN' : language === 'ru' ? 'ru-RU' : 'es-ES';

    return {
        appointments,
        locale,
    };
};
