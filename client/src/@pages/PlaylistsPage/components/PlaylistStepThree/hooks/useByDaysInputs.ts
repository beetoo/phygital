import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { selectPlaylistDto } from '../../../selectors';
import { selectActiveScheduleScreenId } from '../../../../DxSchedulePage/selectors';
import { setPlaylistDtoSchedules } from '../../../actions';

const initialByDaysList: ByDaysList = [
    { MO: false },
    { TU: false },
    { WE: false },
    { TH: false },
    { FR: false },
    { SA: false },
    { SU: false },
];

export const byDaysValueToByDaysString = (byDaysList: ByDaysList): string =>
    byDaysList
        .filter((dayValue) => Object.values(dayValue)[0])
        .map((dayValue) => Object.keys(dayValue)[0])
        .join(',');

export const byDaysStringToByDaysValue = (byDaysString: string): ByDaysList => {
    const daysList = byDaysString ? byDaysString.split(',') : [];

    return initialByDaysList.map((dayValue) =>
        daysList.includes(Object.keys(dayValue)[0]) ? { [Object.keys(dayValue)[0]]: true } : dayValue,
    ) as ByDaysList;
};

type ByDay = 'MO' | 'TU' | 'WE' | 'TH' | 'FR' | 'SA' | 'SU';

type ByDaysList = [
    { MO: boolean },
    { TU: boolean },
    { WE: boolean },
    { TH: boolean },
    { FR: boolean },
    { SA: boolean },
    { SU: boolean },
];

type ReturnValue = {
    dayValue: (day: ByDay) => boolean;
    onByDayChange: (day: ByDay) => () => void;
};

export const useByDaysInputs = (): ReturnValue => {
    const dispatch = useDispatch();

    const { playlistDtoSchedules } = useSelector(selectPlaylistDto);
    const activeScheduleScreenId = useSelector(selectActiveScheduleScreenId);

    const activePlaylistSchedule = useMemo(
        () => playlistDtoSchedules.find(({ screenId }) => screenId === activeScheduleScreenId),
        [activeScheduleScreenId, playlistDtoSchedules],
    );

    const activeByDaysList = useMemo(
        () => byDaysStringToByDaysValue(activePlaylistSchedule?.byDay ?? ''),
        [activePlaylistSchedule?.byDay],
    );

    const [byDays, setByDays] = useState<ByDaysList>(activeByDaysList);

    const updateByDay = useCallback(
        (byDayList: ByDaysList) => {
            const updatedPlaylistDto = playlistDtoSchedules.map((scheduleDto) => {
                const byDaysString = byDaysValueToByDaysString(byDayList);

                return {
                    ...scheduleDto,
                    byDay: byDaysString,
                };
            });

            dispatch(setPlaylistDtoSchedules(updatedPlaylistDto));
        },
        [dispatch, playlistDtoSchedules],
    );

    const onByDayChange = useCallback(
        (day: ByDay) => () => {
            const changedByDays = byDays.map((dayValue) =>
                day in dayValue ? { [day]: !Object.values(dayValue)[0] } : dayValue,
            ) as ByDaysList;

            setByDays(changedByDays);
            updateByDay(changedByDays);
        },
        [byDays, updateByDay],
    );

    const dayValue = useCallback(
        (day: ByDay) => {
            const requiredDay = byDays.find((dayValue) => day in dayValue);
            if (requiredDay) {
                return Object.values(requiredDay)[0];
            } else {
                return false;
            }
        },
        [byDays],
    );

    const prevScheduleScreenId = useRef('');

    useEffect(() => {
        if (prevScheduleScreenId.current !== activeScheduleScreenId) {
            prevScheduleScreenId.current = activeScheduleScreenId;
            setByDays(activeByDaysList);
        }
    }, [activeByDaysList, activeScheduleScreenId]);

    return {
        dayValue,
        onByDayChange,
    };
};
