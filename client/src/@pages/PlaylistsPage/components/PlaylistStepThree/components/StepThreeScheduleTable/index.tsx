import React from 'react';
import { ViewState } from '@devexpress/dx-react-scheduler';
import { Paper } from '@mui/material';
import {
    Scheduler,
    Toolbar,
    DateNavigator,
    MonthView,
    Appointments,
    AppointmentTooltip,
} from '@devexpress/dx-react-scheduler-material-ui';

import { DayScaleRow } from '../../../../../../ui-kit/DayScaleRow';
import { currentDate } from '../../../../../DxSchedulePage/helpers';
import { useStepThreeScheduleTable } from '../../hooks/useStepThreeScheduleTable';

const StepThreeScheduleTable: React.FC = () => {
    const { appointments, locale } = useStepThreeScheduleTable();

    return (
        <Paper style={{ marginLeft: '24px', marginRight: '24px' }}>
            <Scheduler height="auto" data={appointments} locale={locale} firstDayOfWeek={1}>
                <ViewState currentDate={currentDate()} currentViewName="Month" />
                <MonthView dayScaleRowComponent={DayScaleRow} />
                <Toolbar />
                <DateNavigator />
                <Appointments />
                <AppointmentTooltip showCloseButton />
                {/*<AppointmentForm readOnly />*/}
            </Scheduler>
        </Paper>
    );
};

export default StepThreeScheduleTable;
