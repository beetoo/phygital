import React from 'react';

import { useDraftPlaylistSaveModal } from '../../hooks/useDraftPlaylistSaveModal';

import css from './style.m.css';

const DraftPlaylistSaveModal: React.FC = () => {
    const { savePlaylistOnEditStep, goToSelectScreens, t } = useDraftPlaylistSaveModal();

    return (
        <form className={css.popupDeletePlaylist} onSubmit={(e) => e.preventDefault()}>
            <h2>{t('savingPlaylist')}</h2>
            <p>
                {t('noneOfScreens')}
                <br />
                {t('selectScreens')}
            </p>
            <div className={css.containerBtn}>
                <a className={css.btnCancel}>
                    <button type="button" className={css.buttonCancel} onClick={savePlaylistOnEditStep}>
                        {t('btnSaveDraft')}
                    </button>
                </a>
                <button type="submit" className={css.buttonCreate} onClick={goToSelectScreens}>
                    {t('selectScreensBtn')}
                </button>
            </div>
        </form>
    );
};

export default DraftPlaylistSaveModal;
