import React from 'react';
import { useTranslation } from 'react-i18next';
import { TextField } from '@mui/material';
import { PatternFormat } from 'react-number-format';
import { clsx } from 'clsx';

import { useUpdatePlaylistButtons } from '../../../../hooks/useUpdatePlaylistButtons';
import { useStartAndEndDateInputs } from '../../hooks/useStartAndEndDateInputs';
import { useStartAndEndPlaylistTimeInputs } from '../../hooks/useStartAndEndPlaylistTimeInputs';
import { useByDaysInputs } from '../../hooks/useByDaysInputs';
import { useAdminModeCheckbox } from '../../hooks/useAdminModeCheckbox';
import { useLocationsAndScreensSelects } from '../../hooks/useLocationsAndScreensSelects';
import { isTimeAllowed } from '../../../../../../helpers';

import css from '../../style.m.css';

const StepThreeMenu: React.FC = () => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const { screensNumber } = useLocationsAndScreensSelects();

    const {
        initialDate,
        startDay,
        endDay,
        onStartDateChange,
        onEndDateChange,
        broadcastAlways,
        onBroadcastAlwaysChange,
        isEndDateLessThanStartDate,
    } = useStartAndEndDateInputs();

    const { dayValue, onByDayChange } = useByDaysInputs();

    const {
        startTimeValue,
        endTimeValue,
        isAllDayTranslation,
        onStartTimeChange,
        onEndTimeChange,
        onAllDayCheckboxChange,
    } = useStartAndEndPlaylistTimeInputs();

    const { openStartPlaylistModal, updatePlaylist, isCreatePlaylistStep, isSubmitButtonDisabled } =
        useUpdatePlaylistButtons();

    const { isAdmin, isLogEnable, onLogChange, isSyncEnable, onSyncChange } = useAdminModeCheckbox();

    const isOneButton = isCreatePlaylistStep ? screensNumber === 0 && isSubmitButtonDisabled : true;

    return (
        <div className={css.mainRightMenuSetup}>
            <div className={css.wrapMainRightMenuSetup}>
                <div className={clsx(css.wrapRightMenuScheduleSetup, isOneButton && css.oneButton)}>
                    <h2>
                        {t('scheduleSettings')}
                        {/* Настройка расписания */}
                    </h2>

                    <p className={css.scheduleSetupParagraph}>
                        {t('playlistSchedule')}
                        {/* Расписание плейлиста */}
                    </p>
                    {/*<div className={css.fieldScheduleSetupPriority}>*/}
                    {/*    <img alt="" />*/}
                    {/*    <p className={css.scheduleSetupParagraph1}>*/}
                    {/*        {t('priority')}*/}
                    {/*        /!* Приоритет *!/*/}
                    {/*    </p>*/}
                    {/*    <span className={css.iconStarSchedule} />*/}
                    {/*</div>*/}
                    {/*<div className={css.fieldScheduleSetupSelect}>*/}
                    {/*    <p className={css.scheduleSetupParagraph2}>*/}
                    {/*        {t('priorityPlaylistHigh')}*/}
                    {/*        /!* Самый высокий приоритет плейлиста&nbsp;на&nbsp;выбранном экране *!/*/}
                    {/*    </p>*/}
                    {/*    <span*/}
                    {/*        className={playlistPriority ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}*/}
                    {/*        onClick={screensNumber > 0 ? onPlaylistPriorityChange : undefined}*/}
                    {/*    />*/}
                    {/*    <div className={css.fieldScheduleHint3}>*/}
                    {/*        <span className={css.iconInfoSchedule} />*/}
                    {/*        <span className={css.tooltiptext}>*/}
                    {/*            {t('scheduleTooltipThree')}*/}
                    {/*            /!* Чтобы плейлист транслировался поверх остальных плейлистов вне зависимости от их*/}
                    {/*            расписания, назначьте ему самый высокий приоритет *!/*/}
                    {/*        </span>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                    <div className={css.fieldScheduleSetupSelect}>
                        <p className={clsx(css.scheduleSetupParagraph1, css.scheduleSetupParagraphDate)}>
                            {t('startDate')}
                            {/* Дата начала */}
                        </p>
                        <TextField
                            type="date"
                            size="small"
                            sx={{ width: 292 }}
                            value={startDay}
                            onChange={onStartDateChange}
                            inputProps={{ min: initialDate }}
                            disabled={screensNumber === 0}
                        />
                        <p className={clsx(css.scheduleSetupParagraph1, css.scheduleSetupParagraphDate)}>
                            {t('endDate')}
                            {/* Дата окончания */}
                        </p>
                        <TextField
                            type="date"
                            size="small"
                            sx={{ width: 292 }}
                            value={endDay}
                            onChange={onEndDateChange}
                            inputProps={{ min: initialDate }}
                            disabled={screensNumber === 0 || broadcastAlways}
                        />
                        {isEndDateLessThanStartDate && !broadcastAlways && (
                            <p className={css.errorScheduleSelectDate}>
                                {t('textTwelve')}
                                {/* Дата окончания должна быть позже даты начала */}
                            </p>
                        )}
                    </div>

                    <div className={clsx(css.fieldScheduleSetupSelect, css.fieldScheduleSetupSelectItem)}>
                        <p className={css.scheduleSetupParagraph2}>
                            {t('textThirteen')}
                            {/* Транслировать плейлист всегда */}
                        </p>
                        <span
                            className={broadcastAlways ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                            onClick={screensNumber > 0 ? onBroadcastAlwaysChange : undefined}
                        />
                    </div>
                    <p className={css.scheduleSetupParagraph1}>
                        {t('selectRepetition')}
                        {/* Выбрать частоту повтора по дням недели */}
                    </p>

                    <div className={css.scheduleSetupWeek}>
                        <div className={css.scheduleSetupDay}>
                            <p className={css.scheduleSetupDayActive}>
                                {t('dayMo')}
                                {/* Пн */}
                            </p>
                            <span
                                className={dayValue('MO') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                                onClick={screensNumber > 0 ? onByDayChange('MO') : undefined}
                            />
                        </div>
                        <div className={css.scheduleSetupDay}>
                            <p className={css.scheduleSetupDayActive}>
                                {t('dayTu')}
                                {/* Вт */}
                            </p>
                            <span
                                className={dayValue('TU') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                                onClick={screensNumber > 0 ? onByDayChange('TU') : undefined}
                            />
                        </div>
                        <div className={css.scheduleSetupDay}>
                            <p className={css.scheduleSetupDayActive}>
                                {t('dayWe')}
                                {/* Ср */}
                            </p>
                            <span
                                className={dayValue('WE') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                                onClick={screensNumber > 0 ? onByDayChange('WE') : undefined}
                            />
                        </div>
                        <div className={css.scheduleSetupDay}>
                            <p className={css.scheduleSetupDayActive}>
                                {t('dayTh')}
                                {/* Чт */}
                            </p>
                            <span
                                className={dayValue('TH') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                                onClick={screensNumber > 0 ? onByDayChange('TH') : undefined}
                            />
                        </div>
                        <div className={css.scheduleSetupDay}>
                            <p className={css.scheduleSetupDayActive}>
                                {t('dayFr')}
                                {/* Пт */}
                            </p>
                            <span
                                className={dayValue('FR') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                                onClick={screensNumber > 0 ? onByDayChange('FR') : undefined}
                            />
                        </div>
                        <div className={css.scheduleSetupDay}>
                            <p className={css.scheduleSetupDayActive}>
                                {t('daySa')}
                                {/* Сб */}
                            </p>
                            <span
                                className={dayValue('SA') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                                onClick={screensNumber > 0 ? onByDayChange('SA') : undefined}
                            />
                        </div>
                        <div className={css.scheduleSetupDay}>
                            <p className={css.scheduleSetupDayActive}>
                                {t('daySu')}
                                {/* Вс */}
                            </p>
                            <span
                                className={dayValue('SU') ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                                onClick={screensNumber > 0 ? onByDayChange('SU') : undefined}
                            />
                        </div>
                    </div>
                    <p className={css.scheduleSetupParagraph1}>
                        {t('time')}
                        {/* Время */}
                    </p>
                    <div className={css.fieldScheduleSetupSelectClock}>
                        <div className={css.customResolutionInputContainer}>
                            <div className={css.fieldWidth}>
                                <label />
                                <PatternFormat
                                    className={css.customResolutionInput}
                                    format="##:##"
                                    placeholder="00:00"
                                    isAllowed={isTimeAllowed}
                                    value={startTimeValue}
                                    onChange={onStartTimeChange}
                                    disabled={isAllDayTranslation || screensNumber === 0}
                                />
                            </div>
                            <div>
                                <label />
                                <PatternFormat
                                    className={css.customResolutionInput}
                                    format="##:##"
                                    placeholder="00:00"
                                    isAllowed={isTimeAllowed}
                                    value={endTimeValue}
                                    onChange={onEndTimeChange}
                                    disabled={isAllDayTranslation || screensNumber === 0}
                                />

                                <div className={css.fieldScheduleSetupNextDay}>
                                    <p
                                        className={clsx(
                                            css.scheduleSetupParagraph2,
                                            css.scheduleSetupParagraph2NextDay,
                                        )}
                                    >
                                        {t('twentyFourHours')}
                                        {/* 24 часа */}
                                    </p>
                                    <span
                                        className={
                                            screensNumber > 0 && isAllDayTranslation
                                                ? css.checkboxFreeSlotOn
                                                : css.checkboxFreeSlotOff
                                        }
                                        onClick={screensNumber > 0 ? onAllDayCheckboxChange : undefined}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>

                    {isAdmin && (
                        <div>
                            <p className={css.scheduleSetupParagraph}>Тех.Настройки</p>
                            <div className={clsx(css.fieldScheduleSetupSelect, css.fieldScheduleSetupSelectItem)}>
                                <p className={css.scheduleSetupParagraph2}>Синхронизация</p>
                                <span
                                    className={isSyncEnable ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                                    onClick={onSyncChange}
                                />
                            </div>
                            <div className={clsx(css.fieldScheduleSetupSelect, css.fieldScheduleSetupSelectItem)}>
                                <p className={css.scheduleSetupParagraph2}>Логирование</p>
                                <span
                                    className={isLogEnable ? css.checkboxFreeSlotOn : css.checkboxFreeSlotOff}
                                    onClick={onLogChange}
                                />
                            </div>
                        </div>
                    )}
                </div>
            </div>
            <div className={css.btnStartScreensField}>
                {isCreatePlaylistStep ? (
                    <>
                        <button className={clsx(css.btnSaveChangeExit, css.btnSaveDraft)} onClick={updatePlaylist}>
                            {t('btnSaveDraft')}
                            {/* Сохранить как черновик */}
                        </button>

                        {!(screensNumber === 0 || isSubmitButtonDisabled) && (
                            <button className={css.btnStartScreens} onClick={openStartPlaylistModal}>
                                <img alt="" />
                                {t('btnLaunchOnScreens')}
                                {/* Запустить на экраны */}
                            </button>
                        )}
                    </>
                ) : (
                    <>
                        {!isSubmitButtonDisabled && (
                            <button className={clsx(css.btnSaveChangeExit, css.editStep)} onClick={updatePlaylist}>
                                {t('btnSaveChangesExit')}
                                {/* Сохранить изменения и выйти */}
                            </button>
                        )}
                    </>
                )}
            </div>
        </div>
    );
};

export default StepThreeMenu;
