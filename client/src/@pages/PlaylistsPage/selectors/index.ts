import { createSelector } from 'reselect';

import { StoreType } from '../../../types';
import {
    AddPlaylistSuccess,
    ContentDisplayType,
    PlaylistDtoScreen,
    PlaylistStepDto,
    StepTwoMenuMode,
    StepTwoPlayMode,
} from '../types';
import { PlaylistType } from '../../../../../common/types';

export const selectSelectedPlaylistIds = ({
    pages: {
        organization: {
            playlists: { selectedPlaylistIds },
        },
    },
}: StoreType): string[] => selectedPlaylistIds;

export const selectSelectedPlaylistItemIds = ({
    pages: {
        organization: {
            playlists: { selectedPlaylistItemIds },
        },
    },
}: StoreType): string[] => selectedPlaylistItemIds;

export const selectDeletePlaylistAlertStatus = ({
    pages: {
        organization: {
            playlists: { isDeletePlaylistAlertOpen },
        },
    },
}: StoreType): boolean => isDeletePlaylistAlertOpen;

export const selectDeletePlaylistItemAlertStatus = ({
    pages: {
        organization: {
            playlists: { isDeletePlaylistItemAlertOpen },
        },
    },
}: StoreType): boolean => isDeletePlaylistItemAlertOpen;

export const selectActivePlaylistId = ({
    pages: {
        organization: {
            playlists: { activePlaylistId },
        },
    },
}: StoreType): string => activePlaylistId;

export const selectActivePlaylistItemId = ({
    pages: {
        organization: {
            playlists: { activePlaylistItemId },
        },
    },
}: StoreType): string => activePlaylistItemId;

export const selectDeletablePlaylistItemId = ({
    pages: {
        organization: {
            playlists: { deletablePlaylistItemId },
        },
    },
}: StoreType): string => deletablePlaylistItemId;

// playlists

export const selectPlaylists = ({
    pages: {
        organization: {
            playlists: { playlists },
        },
    },
}: StoreType): PlaylistType[] => playlists;

export const selectPlaylistDto = ({
    pages: {
        organization: {
            playlists: { playlistDto },
        },
    },
}: StoreType): PlaylistStepDto => playlistDto;

export const selectPlaylistDtoScreens = createSelector(
    (state: StoreType) => state.pages.organization.playlists,
    ({ playlistDtoScreens }) => playlistDtoScreens.map(({ id }) => id),
);

export const selectPlaylistDtoScreensOrdered = ({
    pages: {
        organization: {
            playlists: { playlistDtoScreens },
        },
    },
}: StoreType): PlaylistDtoScreen[] => playlistDtoScreens;

export const selectStartPlaylistModalStatus = ({
    pages: {
        organization: {
            playlists: { isStartPlaylistModalOpen },
        },
    },
}: StoreType): boolean => isStartPlaylistModalOpen;

export const selectAddingPlaylistStatus = ({
    pages: {
        organization: {
            playlists: { addingPlaylist },
        },
    },
}: StoreType): boolean => addingPlaylist;

export const selectAddPlaylistSuccessStatus = ({
    pages: {
        organization: {
            playlists: { addPlaylistSuccess },
        },
    },
}: StoreType): AddPlaylistSuccess | false => addPlaylistSuccess;

export const selectUpdatePlaylistSuccessStatus = ({
    pages: {
        organization: {
            playlists: { updatePlaylistSuccess },
        },
    },
}: StoreType): boolean => updatePlaylistSuccess;

export const selectUpdatePlaylistTitleSuccessStatus = ({
    pages: {
        organization: {
            playlists: { isUpdatePlaylistTitleSuccess },
        },
    },
}: StoreType): boolean => isUpdatePlaylistTitleSuccess;

export const selectUpdatingPlaylistStatus = ({
    pages: {
        organization: {
            playlists: { updatingPlaylist },
        },
    },
}: StoreType): boolean => updatingPlaylist;

export const selectUpdatePlaylistTitleStatus = ({
    pages: {
        organization: {
            playlists: { updatingPlaylistTitle },
        },
    },
}: StoreType): boolean => updatingPlaylistTitle;

export const selectDeletePlaylistStatus = ({
    pages: {
        organization: {
            playlists: { deletePlaylist },
        },
    },
}: StoreType): boolean => deletePlaylist;

export const selectDeletePlaylistsSuccessStatus = ({
    pages: {
        organization: {
            playlists: { isDeletePlaylistSuccess },
        },
    },
}: StoreType): boolean => isDeletePlaylistSuccess;

// playlistItems

export const selectUpdatingPlaylistItemNameStatus = ({
    pages: {
        organization: {
            playlists: { updatingPlaylistItemName },
        },
    },
}: StoreType): boolean => updatingPlaylistItemName;

export const selectDeletePlaylistItemStatus = ({
    pages: {
        organization: {
            playlists: { deletePlaylistItem },
        },
    },
}: StoreType): boolean => deletePlaylistItem;

export const selectDeletePlaylistItemSuccessStatus = ({
    pages: {
        organization: {
            playlists: { isDeletePlaylistItemSuccess },
        },
    },
}: StoreType): boolean => isDeletePlaylistItemSuccess;

export const selectStepTwoMenuMode = ({
    pages: {
        organization: {
            playlists: { stepTwoMenuMode },
        },
    },
}: StoreType): StepTwoMenuMode => stepTwoMenuMode;

export const selectStepTwoActivePlaylistItemId = ({
    pages: {
        organization: {
            playlists: { stepTwoActivePlaylistItemId },
        },
    },
}: StoreType): string => stepTwoActivePlaylistItemId;

export const selectStepTwoPlayablePlaylistItemId = ({
    pages: {
        organization: {
            playlists: { stepTwoPlayablePlaylistItemId },
        },
    },
}: StoreType): string => stepTwoPlayablePlaylistItemId;

export const selectStepTwoActiveScenarioId = ({
    pages: {
        organization: {
            playlists: { stepTwoActiveScenarioId },
        },
    },
}: StoreType): string => stepTwoActiveScenarioId;

export const selectStepTwoSelectedPlaylistItemIds = ({
    pages: {
        organization: {
            playlists: { stepTwoSelectedPlaylistItemIds },
        },
    },
}: StoreType): string[] => stepTwoSelectedPlaylistItemIds;

export const selectStepTwoSelectedContentItemIds = ({
    pages: {
        organization: {
            playlists: { stepTwoSelectedContentItemIds },
        },
    },
}: StoreType): string[] => stepTwoSelectedContentItemIds;

export const selectStepTwoScenariosFilterOpen = ({
    pages: {
        organization: {
            playlists: { stepTwoScenariosFilterOpen },
        },
    },
}: StoreType): boolean => stepTwoScenariosFilterOpen;

export const selectStepTwoShowedScenariosIds = ({
    pages: {
        organization: {
            playlists: { stepTwoShowedScenariosIds },
        },
    },
}: StoreType): string[] => stepTwoShowedScenariosIds;

export const selectStepTwoPlayMode = ({
    pages: {
        organization: {
            playlists: { stepTwoPlayMode },
        },
    },
}: StoreType): StepTwoPlayMode => stepTwoPlayMode;

export const selectStepTwoSliderMoving = ({
    pages: {
        organization: {
            playlists: { stepTwoSliderMoving },
        },
    },
}: StoreType): boolean => stepTwoSliderMoving;

export const selectStepTwoCurrentTimelineSeconds = ({
    pages: {
        organization: {
            playlists: { stepTwoCurrentTimelineSeconds },
        },
    },
}: StoreType): number => stepTwoCurrentTimelineSeconds;

export const selectTimeLineRef = ({
    pages: {
        organization: {
            playlists: { timeLineRef },
        },
    },
}: StoreType): HTMLDivElement | null => timeLineRef;

export const selectContentDisplayType = ({
    pages: {
        organization: {
            playlists: { contentDisplayType },
        },
    },
}: StoreType): ContentDisplayType => contentDisplayType;
