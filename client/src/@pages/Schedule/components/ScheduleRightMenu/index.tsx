import React from 'react';
import { clsx } from 'clsx';

import css from '../../style.m.css';

const ScheduleRightMenu: React.FC = () => (
    <div className={css.rightBlockCalendar}>
        <div className={css.rightBlockEmptyCalendar}>
            <h2>Расписание</h2>
            <h3>19 декабря 2021</h3>
            <div className={css.fieldFreeSlot}>
                <p>Показать свободные временные слоты выбранного экрана на календаре</p>
                <span className={css.checkboxFreeSlotOn} />
                <span className={css.checkboxFreeSlotOff} />
            </div>
            <div className={css.wrappCardPlaylistCalendar}>
                <div className={css.cardPlaylistCalendarPriority}>
                    <div className={css.cardPriority}>
                        <h3>Приоритет</h3>
                    </div>
                    <div className={css.fieldCardPlaylistCalendarStatus}>
                        <p className={css.statusOn}>Активный</p>
                        <span className={css.statusArrowBendUpRight}>
                            <span className={css.tooltiptext}>Перейти в плейлист</span>
                        </span>
                        <span className={css.statusPencil}>
                            <span className={css.tooltiptext}>Редактировать расписание</span>
                        </span>
                    </div>
                    <div className={css.cardPlaylistCalendarName}>
                        <h2>Плейлист 6</h2>
                    </div>
                    <div className={css.fieldCardPlaylistCalendarScreenAddress}>
                        <p className={css.cardPlaylistCalendarScreen}>Sumsung</p>
                        <p className={css.cardPlaylistCalendarAddress}>
                            Точка 1, г. Москва, пр. Вознесенский123456789987654321
                        </p>
                        <span className={css.tooltiptext}>Точка 1, г. Москва, пр. Вознесенский проспект, д. 22</span>
                    </div>
                    <div className={css.fieldCardPlaylistCalendarDateTime}>
                        <div className={css.cardPlaylistCalendarDate}>
                            <p>19.12.21-19.12.21</p>
                            <span className={css.tooltiptext}>Дата трансляции</span>
                        </div>
                        <div className={css.cardPlaylistCalendarTime}>
                            <p>10:00–12:00</p>
                            <span className={css.tooltiptext}>Время трансляции</span>
                        </div>
                    </div>
                    <div className={css.fieldCardPlaylistCalendarReplay}>
                        <p>
                            <span>Частота:</span> без повтора
                        </p>
                    </div>
                </div>
                <div className={css.cardPlaylistCalendar}>
                    <div className={css.fieldCardPlaylistCalendarStatus}>
                        <p className={css.statusOn}>Активный</p>
                        <span className={css.statusArrowBendUpRight}>
                            <span className={css.tooltiptext}>Перейти в плейлист</span>
                        </span>
                        <span className={css.statusPencil}>
                            <span className={css.tooltiptext}>Редактировать расписание</span>
                        </span>
                    </div>
                    <div className={clsx(css.cardPlaylistCalendarName, css.cardPlaylistCalendarNameItem1)}>
                        <h2>Плейлист 7</h2>
                    </div>
                    <div className={css.fieldCardPlaylistCalendarScreenAddress}>
                        <p className={css.cardPlaylistCalendarScreen}>Sumsung</p>
                        <p className={css.cardPlaylistCalendarAddress}>
                            Точка 1, г. Москва, пр. Вознесенский123456789987654321
                        </p>
                        <span className={css.tooltiptext}>Точка 1, г. Москва, пр. Вознесенский проспект, д. 22</span>
                    </div>
                    <div className={css.fieldCardPlaylistCalendarDateTime}>
                        <div className={css.cardPlaylistCalendarDate}>
                            <p>19.12.21-19.12.21</p>
                            <span className={css.tooltiptext}>Дата трансляции</span>
                        </div>
                        <div className={css.cardPlaylistCalendarTime}>
                            <p>12:00–16:00</p>
                            <span className={css.tooltiptext}>Время трансляции</span>
                        </div>
                    </div>
                    <div className={css.fieldCardPlaylistCalendarReplay}>
                        <p>
                            <span>Частота:</span> без повтора
                        </p>
                    </div>
                </div>
                <div className={css.cardPlaylistCalendar}>
                    <div className={css.fieldCardPlaylistCalendarStatus}>
                        <p className={css.statusOn}>Активный</p>
                        <span className={css.statusArrowBendUpRight}>
                            <span className={css.tooltiptext}>Перейти в плейлист</span>
                        </span>
                        <span className={css.statusPencil}>
                            <span className={css.tooltiptext}>Редактировать расписание</span>
                        </span>
                    </div>
                    <div className={clsx(css.cardPlaylistCalendarName, css.cardPlaylistCalendarNameItem2)}>
                        <h2>Плейлист 8</h2>
                    </div>
                    <div className={css.fieldCardPlaylistCalendarScreenAddress}>
                        <p className={css.cardPlaylistCalendarScreen}>Sumsung</p>
                        <p className={css.cardPlaylistCalendarAddress}>
                            Точка 1, г. Москва, пр. Вознесенский123456789987654321
                        </p>
                        <span className={css.tooltiptext}>Точка 1, г. Москва, пр. Вознесенский проспект, д. 22</span>
                    </div>
                    <div className={css.fieldCardPlaylistCalendarDateTime}>
                        <div className={css.cardPlaylistCalendarDate}>
                            <p>19.12.21-19.12.21</p>
                            <span className={css.tooltiptext}>Дата трансляции</span>
                        </div>
                        <div className={css.cardPlaylistCalendarTime}>
                            <p>10:00–12:00</p>
                            <span className={css.tooltiptext}>Время трансляции</span>
                        </div>
                    </div>
                    <div className={css.fieldCardPlaylistCalendarReplay}>
                        <p>
                            <span>Частота:</span> без повтора
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

export default ScheduleRightMenu;
