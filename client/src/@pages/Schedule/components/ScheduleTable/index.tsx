import React from 'react';
import { clsx } from 'clsx';

import { MenuItem, Select } from '../../../../ui-kit';

import css from '../../style.m.css';

const ScheduleTable: React.FC = () => (
    <>
        <div className={css.selectListCalendar}>
            <div className={css.blockCalendarSmall}>
                <button className={css.btnArrowLeft} />
                <div className={css.selectCalendarDate}>
                    <h3>Сентябрь 2021</h3>
                </div>
                <button className={css.btnArrowRight} />
            </div>
            <div className={css.selectCalendarPoinAndScreen}>
                <div className={css.selectCalendarAddressPoint}>
                    <Select
                        width="100%"
                        style={{ marginTop: 0, marginBottom: 0 }}
                        onChange={() => undefined}
                        value=""
                        placeholder="Название локации г. Москва, Вознесенский пр. 2"
                    >
                        <MenuItem>
                            Название точки 2<br />
                            г. Москва, Луначарского пр. 133
                        </MenuItem>
                        <MenuItem>
                            Название точки 3<br />
                            г. Москва, Вознесенский пр. 10
                        </MenuItem>
                    </Select>
                    <span className={css.tooltiptext}>Название точки, г. Москва, Вознесенский пр, д.22, корпус 5</span>
                </div>
                <div className={css.selectCalendarScreen}>
                    <Select
                        width="100%"
                        style={{ marginTop: 0, marginBottom: 0 }}
                        onChange={() => undefined}
                        value=""
                        placeholder="Экран с длинным названием"
                    >
                        <MenuItem>Экран 1</MenuItem>
                        <MenuItem>Экран с длинным названием</MenuItem>
                        <MenuItem>Экран 3</MenuItem>
                        <MenuItem>Экран 4</MenuItem>
                    </Select>
                    <span className={css.tooltiptext}>Экран с длинным названием</span>
                </div>
            </div>
        </div>
        <div className={css.blockCalendar}>
            <div className={css.blockDays}>
                <div className={css.itemDay}>Пн</div>
                <div className={css.itemDay}>Вт</div>
                <div className={css.itemDay}>Ср</div>
                <div className={css.itemDay}>Чт</div>
                <div className={css.itemDay}>Пт</div>
                <div className={css.itemDay}>Сб</div>
                <div className={css.itemDay}>Вс</div>
            </div>
            <div className={css.blockWeek}>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>1</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>2</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>3</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>4</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>5</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>6</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>7</h2>
                    </div>
                </div>
            </div>
            <div className={css.blockWeek}>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>8</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>9</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>10</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>11</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>12</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>13</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>14</h2>
                    </div>
                </div>
            </div>
            <div className={css.blockWeek}>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>15</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>16</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={clsx(css.dayWeek, css.dayWeekDefault)}>17</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={clsx(css.dayWeek, css.dayWeekActive)}>18</h2>
                        <span className={css.dayWeekNotification} />
                        <span className={css.tooltiptext}>Пробелы в расписании</span>
                    </div>
                    <div className={css.wrappPlaylistCalendar}>
                        <div className={css.playlistCalendar}>
                            <p className={clsx(css.playlistCalendarTime, css.playlistCalendarTimePriority)}>
                                10:00
                                <span className={css.playlistCalendarPriority} />
                            </p>
                            <p className={css.playlistCalendarName}>Плейлист 6</p>
                            <span className={css.tooltiptext}>Плейлист 6</span>
                        </div>
                        <div className={clsx(css.playlistCalendar, css.cardPlaylistCalendarNameItem1)}>
                            <p className={css.playlistCalendarTime}>12:00</p>
                            <p className={css.playlistCalendarName}>Плейлист 7</p>
                            <span className={css.tooltiptext}>Плейлист 7</span>
                        </div>
                        <div className={clsx(css.playlistCalendar, css.cardPlaylistCalendarNameItem2)}>
                            <p className={css.playlistCalendarTime}>16:00</p>
                            <p className={css.playlistCalendarName}>Плейлист 8</p>
                            <span className={css.tooltiptext}>Плейлист 8</span>
                        </div>
                        <div className={css.playlistCalendar}>
                            <p className={css.playlistCalendarTime}>10:00</p>
                            <p className={css.playlistCalendarName}>Плейлист 6</p>
                            <span className={css.tooltiptext}>Плейлист 6</span>
                        </div>
                        <div>
                            <span className={css.btnPlaylistCalendar}>ещё 2</span>
                        </div>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>19</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>20</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>21</h2>
                    </div>
                </div>
            </div>
            <div className={css.blockWeek}>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>22</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>23</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>24</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>25</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>26</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>27</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>28</h2>
                    </div>
                </div>
            </div>
            <div className={css.blockWeek}>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>29</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>30</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={css.dayWeek}>31</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={clsx(css.dayWeek, css.dayWeekNextMonth)}>1 окт</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={clsx(css.dayWeek, css.dayWeekNextMonth)}>2</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={clsx(css.dayWeek, css.dayWeekNextMonth)}>3</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={clsx(css.dayWeek, css.dayWeekNextMonth)}>4</h2>
                    </div>
                </div>
            </div>
            <div className={css.blockWeek}>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={clsx(css.dayWeek, css.dayWeekNextMonth)}>5</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={clsx(css.dayWeek, css.dayWeekNextMonth)}>6</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={clsx(css.dayWeek, css.dayWeekNextMonth)}>7</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={clsx(css.dayWeek, css.dayWeekNextMonth)}>8</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={clsx(css.dayWeek, css.dayWeekNextMonth)}>9</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={clsx(css.dayWeek, css.dayWeekNextMonth)}>10</h2>
                    </div>
                </div>
                <div className={css.itemWeek}>
                    <div className={css.wrapDayWeek}>
                        <h2 className={clsx(css.dayWeek, css.dayWeekNextMonth)}>11</h2>
                    </div>
                </div>
            </div>
        </div>
    </>
);

export default ScheduleTable;
