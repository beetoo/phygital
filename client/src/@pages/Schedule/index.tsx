import React from 'react';

import { AddIcon } from '../../ui-kit';
import ScheduleRightMenu from './components/ScheduleRightMenu';
import ScheduleTable from './components/ScheduleTable';

import css from './style.m.css';

const SchedulePage: React.FC = () => (
    <div className={css.containerCalendar}>
        <div className={css.centerBlockCalendar}>
            <div className={css.fieldBtnBlockScreen}>
                <button className={css.btnBlockScreen}>
                    <AddIcon />
                    Добавить плейлист
                </button>
            </div>

            <ScheduleTable />
        </div>
        <ScheduleRightMenu />
    </div>
);

export default SchedulePage;
