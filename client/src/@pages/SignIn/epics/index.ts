import { of, Observable } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { ofType } from 'redux-observable';

import { SIGN_IN_PROCESS, setSignInSuccess, setSignInFail } from '../actions';
import { fromSignIn } from '../../../resolvers/authResolvers';
import { SignInFailAction, SignInProcessAction, SignInSuccessAction } from '../types';
import { setAuthTokenToLocalStorage } from '../../../helpers/httpService/setAuthTokenToLocalStorage';

export const signInEpic = (
    action$: Observable<SignInProcessAction>,
): Observable<SignInSuccessAction | SignInFailAction> =>
    action$.pipe(
        ofType(SIGN_IN_PROCESS),
        mergeMap(({ payload: { email, password } }) =>
            fromSignIn(email, password).pipe(
                map((response) => {
                    setAuthTokenToLocalStorage(response);
                    return setSignInSuccess(response);
                }),
                catchError((error) => of(setSignInFail(error))),
            ),
        ),
    );
