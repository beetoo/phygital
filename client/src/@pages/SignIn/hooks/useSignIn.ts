import { ChangeEvent, useCallback, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import isEmail from 'validator/es/lib/isEmail';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectAuthProcessingStatus, selectAuthErrorMessage } from '../../../selectors';
import { setSignInProcess } from '../actions';
import { setRecoveryPasswordEmail } from '../../RecoveryPassword/actions';
import { selectRecoveryPasswordEmail } from '../../RecoveryPassword/selectors';
import { setClearAuthErrors } from '../../../actions';

type Inputs = {
    email: string;
};

type ReturnType = {
    email: string;
    password: string;
    emailInputError: string | false;
    passwordInputError: string | false;
    recoveryPasswordEmail: string;
    onEmailInputChange: ({ target: { value } }: ChangeEvent<HTMLInputElement>) => void;
    onPasswordInputChange: ({ target: { value } }: ChangeEvent<HTMLInputElement>) => void;
    onSubmit: () => void;
    isPasswordShow: boolean;
    showHidePassword: () => void;
    authProcessing: boolean;
    t: TFunction;
};

export const useSignIn = (): ReturnType => {
    const { t } = useTranslation('translation', { keyPrefix: 'signIn' });

    const dispatch = useDispatch();

    const authProcessing = useSelector(selectAuthProcessingStatus);

    const authErrorMessage = useSelector(selectAuthErrorMessage);
    const recoveryPasswordEmail = useSelector(selectRecoveryPasswordEmail);

    const [email, setEmail] = useState(recoveryPasswordEmail);
    const [password, setPassword] = useState('');

    const [emailInputError, setEmailInputError] = useState<string | false>(false);
    const [passwordInputError, setPasswordInputError] = useState<string | false>(false);

    const [isPasswordShow, setIsPasswordShow] = useState(false);

    useEffect(() => {
        if (authErrorMessage === 'Not found') {
            setEmailInputError(t('emailIsNotFound'));
            setPassword('');
        }
        if (authErrorMessage === 'Invalid password') {
            setPasswordInputError(t('incorrectPassword'));
        }
    }, [authErrorMessage, t]);

    // если вбита корректная почта, то автоматически добавляем на страницу Забыли пароль
    useEffect(() => {
        if (isEmail(email)) {
            if (!recoveryPasswordEmail) {
                dispatch(setRecoveryPasswordEmail(email));
            }
        } else {
            if (recoveryPasswordEmail) {
                dispatch(setRecoveryPasswordEmail());
            }
        }
    }, [dispatch, email, recoveryPasswordEmail]);

    const isInputsValid = useCallback(
        ({ email }: Inputs) => {
            let isValid = true;

            email = email.trim();

            setEmailInputError(false);
            setPasswordInputError(false);

            const emptyInputs = [{ email }].filter((item) => Object.values(item)[0] === '');

            if (emptyInputs.length > 0) {
                emptyInputs.forEach((item) => {
                    const key = Object.keys(item)[0];

                    if (key === 'email') {
                        setEmailInputError(t('theFieldCannotBeEmpty'));
                        isValid = false;
                    }
                });
            } else if (!isEmail(email)) {
                setEmailInputError(t('incorrectEmail'));
                setPassword('');
                isValid = false;
            }

            return isValid;
        },
        [t],
    );

    const onEmailInputChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            if (emailInputError) {
                setEmailInputError(false);
            }
            if (authErrorMessage) {
                dispatch(setClearAuthErrors());
            }

            setEmail(value);
        },
        [authErrorMessage, dispatch, emailInputError],
    );

    const onPasswordInputChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            if (passwordInputError) {
                setPasswordInputError(false);
            }
            if (authErrorMessage) {
                dispatch(setClearAuthErrors());
            }
            setPassword(value);
        },
        [authErrorMessage, dispatch, passwordInputError],
    );

    const onSubmit = useCallback(() => {
        if (isInputsValid({ email })) {
            dispatch(setSignInProcess(email, password));
        }
    }, [dispatch, email, isInputsValid, password]);

    const showHidePassword = useCallback(() => {
        setIsPasswordShow((prevValue) => !prevValue);
    }, []);

    return {
        email,
        password,
        emailInputError,
        passwordInputError,
        onEmailInputChange,
        onPasswordInputChange,
        recoveryPasswordEmail,
        onSubmit,
        isPasswordShow,
        showHidePassword,
        authProcessing,
        t,
    };
};
