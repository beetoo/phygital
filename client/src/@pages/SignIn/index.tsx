import React from 'react';
import { Link } from 'react-router-dom';
import isEmail from 'validator/es/lib/isEmail';
import { clsx } from 'clsx';

import { Tooltip } from '../../ui-kit';
import imageBoy1x from '../../assets/img_boy1x.png';
import { useSignIn } from './hooks/useSignIn';

import css from '../SignUp/style.m.css';

const SignInPage: React.FC = () => {
    const {
        email,
        password,
        emailInputError,
        passwordInputError,
        onEmailInputChange,
        onPasswordInputChange,
        onSubmit,
        isPasswordShow,
        authProcessing,
        showHidePassword,
        t,
    } = useSignIn();

    return (
        <div className={css.mainRegister}>
            <div className={css.bgLeftContainer}>
                <div className={css.bgLeft}>
                    <h2>
                        {t('mainTextOne')}
                        <br />
                        <span>{t('mainTextTwo')}</span>
                    </h2>
                    <img src={imageBoy1x} alt="" />
                </div>
            </div>
            <div className={css.bgRightContainer}>
                <div className={css.bgRight}>
                    <div className={css.bgRightSignIn}>
                        <p>{t('noAccountYet')}</p>
                        <Link to="/signup">{t('signUp')}</Link>
                    </div>
                    <form className={css.bgRightFormContainer} onSubmit={(e) => e.preventDefault()}>
                        <div className={css.bgRightForm}>
                            <h2>{t('logIn')}</h2>
                            <div className={css.fieldEmail}>
                                <div className={css.fieldLabelWrapper}>
                                    <label className={css.fieldLabel} htmlFor="email">
                                        {t('email')}
                                    </label>
                                </div>

                                <input
                                    className={clsx(emailInputError ? css.inputError : css.input, {
                                        [css.valid]: isEmail(email),
                                        [css.iconWarning]: emailInputError,
                                    })}
                                    type="text"
                                    placeholder="myemail@adress.ru"
                                    value={email}
                                    onChange={onEmailInputChange}
                                    autoComplete="username"
                                    maxLength={125}
                                />

                                {emailInputError && <div className={css.errorEmail}>{emailInputError}</div>}
                            </div>
                            <div className={css.fieldPassword}>
                                <div className={css.fieldLabelWrapper}>
                                    <label className={css.fieldLabel} htmlFor="password">
                                        {t('password')}
                                    </label>
                                </div>

                                <input
                                    className={clsx(passwordInputError ? css.inputError : css.input, {
                                        [css.passwordValid]: isEmail(email) && password,
                                    })}
                                    type={isPasswordShow ? 'text' : 'password'}
                                    placeholder={t('enterPassword') as string}
                                    value={password}
                                    onChange={onPasswordInputChange}
                                    autoComplete="current-password"
                                    maxLength={125}
                                />
                                <Tooltip title={t(isPasswordShow ? 'hidePassword' : 'viewPassword')} placement="right">
                                    <span
                                        className={clsx(css.iconEye, {
                                            [css.iconEyeOpened]: isPasswordShow,
                                            [css.iconEyeClosed]: !isPasswordShow,
                                        })}
                                        onClick={showHidePassword}
                                    />
                                </Tooltip>

                                <div className={css.fieldErrorPassword}>
                                    {passwordInputError && (
                                        <div className={clsx(css.errorPassword, css.signIn)}>{passwordInputError}</div>
                                    )}

                                    <div className={css.bgRightFormPsw}>
                                        <Link to="/recovery-password">{t('forgotPassword')}</Link>
                                    </div>
                                </div>
                            </div>
                            <button
                                type="submit"
                                className={css.registerBtn}
                                onClick={onSubmit}
                                disabled={authProcessing}
                            >
                                {t('enter')}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default SignInPage;
