import { AuthResponse, ErrorResponse } from '../../../types';

export const SIGN_IN_PROCESS = 'SIGN_IN_PROCESS';
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export const SIGN_IN_FAIL = 'SIGN_IN_FAIL';

export const setSignInProcess = (email: string, password: string) =>
    ({
        type: SIGN_IN_PROCESS,
        payload: { email, password },
    }) as const;
export const setSignInSuccess = ({ id, role, email, name, surname, status }: AuthResponse) =>
    ({ type: SIGN_IN_SUCCESS, payload: { id, role, email, name, surname, status } }) as const;
export const setSignInFail = (errors: ErrorResponse) =>
    ({
        type: SIGN_IN_FAIL,
        payload: { errors },
    }) as const;
