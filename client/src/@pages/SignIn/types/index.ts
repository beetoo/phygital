import { setSignInProcess, setSignInSuccess, setSignInFail } from '../actions';

export type SignInProcessAction = ReturnType<typeof setSignInProcess>;
export type SignInSuccessAction = ReturnType<typeof setSignInSuccess>;
export type SignInFailAction = ReturnType<typeof setSignInFail>;

export type SignInAction = SignInProcessAction | SignInSuccessAction | SignInFailAction;
