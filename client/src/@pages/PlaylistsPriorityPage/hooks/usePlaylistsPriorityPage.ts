import { Dispatch, SetStateAction, useCallback, useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { orderBy } from 'lodash';

import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { httpService } from '../../../helpers/httpService';
import { getPlaylists } from '../../../resolvers/playlistsResolvers';
import { PlaylistType } from '../../../../../common/types';

type ReturnValue = {
    items: PlaylistType[];
    setItems: Dispatch<SetStateAction<PlaylistType[]>>;
    orderChanged: boolean;
    revertChanges: () => void;
    savePlaylistsPriority: () => void;
};

export const usePlaylistsPriorityPage = (): ReturnValue => {
    const { id: organizationId } = useSelector(selectCurrentOrganization);

    const playlistsRef = useRef<PlaylistType[]>([]);

    const [items, setItems] = useState<PlaylistType[]>([]);

    const revertChanges = useCallback(() => {
        setItems(playlistsRef.current);
    }, []);

    const [orderChanged, setOrderChanged] = useState(false);

    const fetchPlaylists = useCallback(() => {
        if (organizationId) {
            getPlaylists(organizationId).then((playlists) => {
                playlists = orderBy(playlists, ['priority'], ['desc']);

                playlistsRef.current = playlists;
                setItems(playlists);
            });
        }
    }, [organizationId]);

    const savePlaylistsPriority = useCallback(() => {
        httpService
            .put(`organization/${organizationId}/playlists/priority`, {
                priorityInfo: items.reverse().map(({ id }, index) => ({ id, priority: index })),
            })
            .then(() => {
                fetchPlaylists();
            });
    }, [fetchPlaylists, items, organizationId]);

    useEffect(() => {
        setOrderChanged(JSON.stringify(playlistsRef.current) !== JSON.stringify(items));
    }, [items]);

    useEffect(() => {
        fetchPlaylists();
    }, [fetchPlaylists]);

    return {
        items,
        setItems,
        orderChanged,
        revertChanges,
        savePlaylistsPriority,
    };
};
