import React from 'react';
import { Reorder } from 'framer-motion';

import { BackButton } from './components/BackButton';
import { usePlaylistsPriorityPage } from './hooks/usePlaylistsPriorityPage';

const PlaylistsPriorityPage: React.FC = () => {
    const { items, setItems, orderChanged, revertChanges, savePlaylistsPriority } = usePlaylistsPriorityPage();

    return (
        <div className="w-full p-10 mr-10">
            <div className="mb-5">
                <BackButton to="playlists" name="Плейлисты" />
            </div>
            <div className="relative">
                <div className="flex justify-between items-center sticky top-0 py-6 bg-white z-50">
                    <div className="h-10 flex items-center">
                        <h1 className="text-3xl font-black">Приоритетность плейлистов</h1>
                    </div>
                    <div></div>
                    {orderChanged && (
                        <div className="flex items-center gap-4">
                            <button
                                onClick={revertChanges}
                                className="h-10 hover:bg-neutral-50 text-neutral-600 flex items-center gap-2 px-4 rounded-xl transition font-semibold"
                            >
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 20 20"
                                    fill="currentColor"
                                    className="w-5 h-5"
                                >
                                    <path d="M6.28 5.22a.75.75 0 0 0-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 1 0 1.06 1.06L10 11.06l3.72 3.72a.75.75 0 1 0 1.06-1.06L11.06 10l3.72-3.72a.75.75 0 0 0-1.06-1.06L10 8.94 6.28 5.22Z" />
                                </svg>
                                <div>Отменить изменения</div>
                            </button>
                            <button
                                className="h-10 hover:bg-blue-500 text-white bg-blue-600 flex items-center gap-2 px-4 rounded-xl transition font-semibold"
                                onClick={savePlaylistsPriority}
                            >
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 20 20"
                                    fill="currentColor"
                                    className="w-5 h-5"
                                >
                                    <path
                                        fillRule="evenodd"
                                        d="M16.704 4.153a.75.75 0 0 1 .143 1.052l-8 10.5a.75.75 0 0 1-1.127.075l-4.5-4.5a.75.75 0 0 1 1.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 0 1 1.05-.143Z"
                                        clipRule="evenodd"
                                    />
                                </svg>
                                <div>Сохранить</div>
                            </button>
                        </div>
                    )}
                </div>
                <div>При одинаковом расписании на экранах будет отображаться тот плейлист, который выше по списку.</div>
                <div
                    style={{ height: 'calc(100vh - 270px)' }}
                    className="h-screen my-10 overflow-scroll overflow-x-hidden"
                >
                    <Reorder.Group axis="y" values={items} onReorder={setItems} className="flex flex-col gap-2">
                        {items.map((item) => (
                            <Reorder.Item
                                key={item.id}
                                value={item}
                                className="w-full p-5 px-7 bg-neutral-50 cursor-grab focus:cursor-grabbing rounded-lg text-lg font-bold"
                            >
                                {item.title}
                            </Reorder.Item>
                        ))}
                    </Reorder.Group>
                </div>
            </div>
        </div>
    );
};

export default PlaylistsPriorityPage;
