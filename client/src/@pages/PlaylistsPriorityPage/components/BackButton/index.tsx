import React from 'react';

import { useCustomRouter } from '../../../../hooks/useCustomRouter';
import { ActivePage } from '../../../../components/OrganizationContainer/types';

type Props = { to: ActivePage; name: string };

export const BackButton: React.FC<Props> = ({ to, name }) => {
    const { pushToPage } = useCustomRouter();

    return (
        <button
            onClick={() => pushToPage(to)}
            className="inline-flex gap-2 h-10 hover:bg-neutral-100 text-neutral-600 hover:text-neutral-800 px-3 rounded-lg font-bold items-center transition-colors"
        >
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" className="w-5 h-5">
                <path
                    fillRule="evenodd"
                    d="M11.78 5.22a.75.75 0 0 1 0 1.06L8.06 10l3.72 3.72a.75.75 0 1 1-1.06 1.06l-4.25-4.25a.75.75 0 0 1 0-1.06l4.25-4.25a.75.75 0 0 1 1.06 0Z"
                    clipRule="evenodd"
                />
            </svg>
            {name}
        </button>
    );
};
