import { StoreType } from '../../../types';

export const selectRecoveryPasswordEmail = ({ auth: { recoveryPasswordEmail } }: StoreType): string =>
    recoveryPasswordEmail;

export const selectSendEmailSuccessStatus = ({ auth: { isSendEmailSuccess } }: StoreType): boolean =>
    isSendEmailSuccess;

export const selectRecoveryPasswordSuccessStatus = ({ auth: { isRecoveryPasswordSuccess } }: StoreType): boolean =>
    isRecoveryPasswordSuccess;

export const selectCheckTokenSuccessStatus = ({ auth: { isCheckTokenSuccess } }: StoreType): boolean =>
    isCheckTokenSuccess;
