import {
    setChangePassword,
    setChangePasswordFail,
    setChangePasswordSuccess,
    setCheckToken,
    setCheckTokenFail,
    setCheckTokenSuccess,
    setClearRecoveryPasswordState,
    setRecoveryPasswordEmail,
    setSendRecoveryPasswordEmail,
    setSendRecoveryPasswordEmailFail,
    setSendRecoveryPasswordEmailSuccess,
} from '../actions';

export type CheckTokenAction = ReturnType<typeof setCheckToken>;
export type CheckTokenSuccessAction = ReturnType<typeof setCheckTokenSuccess>;
export type CheckTokenFailAction = ReturnType<typeof setCheckTokenFail>;

export type SendRecoveryPasswordEmailAction = ReturnType<typeof setSendRecoveryPasswordEmail>;
export type SendRecoveryPasswordEmailSuccessAction = ReturnType<typeof setSendRecoveryPasswordEmailSuccess>;
export type SendRecoveryPasswordEmailFailAction = ReturnType<typeof setSendRecoveryPasswordEmailFail>;

export type ChangePasswordAction = ReturnType<typeof setChangePassword>;
export type ChangePasswordSuccessAction = ReturnType<typeof setChangePasswordSuccess>;
export type ChangePasswordFailAction = ReturnType<typeof setChangePasswordFail>;

export type SetRecoveryPasswordEmailAction = ReturnType<typeof setRecoveryPasswordEmail>;

export type ClearRecoveryPasswordStateAction = ReturnType<typeof setClearRecoveryPasswordState>;

export type ResetPasswordAction =
    | CheckTokenAction
    | CheckTokenSuccessAction
    | CheckTokenFailAction
    | SendRecoveryPasswordEmailAction
    | SendRecoveryPasswordEmailSuccessAction
    | SendRecoveryPasswordEmailFailAction
    | ChangePasswordAction
    | ChangePasswordSuccessAction
    | ChangePasswordFailAction
    | SetRecoveryPasswordEmailAction
    | ClearRecoveryPasswordStateAction;
