import { AuthResponse, ErrorResponse } from '../../../types';

export const CHECK_TOKEN = 'CHECK_TOKEN';
export const CHECK_TOKEN_SUCCESS = 'CHECK_TOKEN_SUCCESS';
export const CHECK_TOKEN_FAIL = 'CHECK_TOKEN_FAIL';

export const SEND_RECOVERY_PASSWORD_EMAIL = 'SEND_RECOVERY_PASSWORD_EMAIL';
export const SEND_RECOVERY_PASSWORD_EMAIL_SUCCESS = 'SEND_RECOVERY_PASSWORD_EMAIL_SUCCESS';
export const SEND_RECOVERY_PASSWORD_EMAIL_FAIL = 'SEND_RECOVERY_PASSWORD_EMAIL_FAIL';

export const CHANGE_PASSWORD = 'CHANGE_PASSWORD';
export const CHANGE_PASSWORD_SUCCESS = 'CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_FAIL = 'CHANGE_PASSWORD_FAIL';

export const CLEAR_RECOVERY_PASSWORD_STATE = 'CLEAR_RECOVERY_PASSWORD_STATE';

export const SET_RECOVERY_PASSWORD_EMAIL = 'SET_RECOVERY_PASSWORD_EMAIL';

export const setCheckToken = (token: string) => ({ type: CHECK_TOKEN, payload: { token } }) as const;
export const setCheckTokenSuccess = (email: string) => ({ type: CHECK_TOKEN_SUCCESS, payload: { email } }) as const;
export const setCheckTokenFail = (errors: ErrorResponse) => ({ type: CHECK_TOKEN_FAIL, payload: { errors } }) as const;

export const setSendRecoveryPasswordEmail = (email: string) =>
    ({ type: SEND_RECOVERY_PASSWORD_EMAIL, payload: { email } }) as const;
export const setSendRecoveryPasswordEmailSuccess = () => ({ type: SEND_RECOVERY_PASSWORD_EMAIL_SUCCESS }) as const;
export const setSendRecoveryPasswordEmailFail = (errors: ErrorResponse) =>
    ({ type: SEND_RECOVERY_PASSWORD_EMAIL_FAIL, payload: { errors } }) as const;

export const setChangePassword = (password: string, token: string) =>
    ({ type: CHANGE_PASSWORD, payload: { password, token } }) as const;
export const setChangePasswordSuccess = ({ id, role, name, surname, email }: AuthResponse) =>
    ({ type: CHANGE_PASSWORD_SUCCESS, payload: { id, role, name, surname, email } }) as const;
export const setChangePasswordFail = (errors: ErrorResponse) =>
    ({ type: CHANGE_PASSWORD_FAIL, payload: { errors } }) as const;

export const setRecoveryPasswordEmail = (email = '') =>
    ({ type: SET_RECOVERY_PASSWORD_EMAIL, payload: { email } }) as const;

export const setClearRecoveryPasswordState = () => ({ type: CLEAR_RECOVERY_PASSWORD_STATE }) as const;
