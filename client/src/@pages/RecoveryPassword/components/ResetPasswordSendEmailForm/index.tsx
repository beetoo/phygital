import React from 'react';
import { Link } from 'react-router-dom';
import { clsx } from 'clsx';

import ModalTemplate from '../../../../components/ModalTemplate';
import { useSendEmailForm } from '../../hooks/useSendEmailForm';

import css from './style.m.css';

const ResetPasswordSendEmailForm: React.FC = () => {
    const { email, emailInputError, onChange, handleSubmit, isSubmitButtonDisabled, goToSignInPage, t } =
        useSendEmailForm();

    return (
        <ModalTemplate isOpen={true} overlayBackground="#f7f7f9">
            <div className={css.wrapRecoveryPassword}>
                <div className={css.recoveryPassword}>
                    <h2>{t('resetOldPassword')}</h2>
                    <p className={css.recoveryPasswordParagraph}>{t('textOne')}</p>
                    <label />
                    <form className={css.fieldEmailRecovery} onSubmit={(e) => e.preventDefault()}>
                        <div className={css.fieldEmail}>
                            <div>
                                <input
                                    className={clsx(
                                        css.recoveryEmail,
                                        emailInputError ? css.emailInputError : !isSubmitButtonDisabled && css.valid,
                                    )}
                                    onChange={onChange}
                                    value={email}
                                    type="text"
                                    placeholder={t('enterEmail') as string}
                                    maxLength={125}
                                />
                            </div>
                            {emailInputError && <div className={css.errorEmail}>{emailInputError}</div>}
                        </div>
                        <button
                            type="submit"
                            className={css.btnGetLink}
                            onClick={handleSubmit}
                            disabled={isSubmitButtonDisabled}
                        >
                            {t('btnSendResetLink')}
                        </button>
                    </form>
                    <p className={css.recoveryPasswordItem}>
                        {t('noAccountYet')}
                        <Link to="/signup">{t('signUp')}</Link>
                    </p>
                    <span className={css.iconX} onClick={goToSignInPage} />
                </div>
            </div>
        </ModalTemplate>
    );
};

export default ResetPasswordSendEmailForm;
