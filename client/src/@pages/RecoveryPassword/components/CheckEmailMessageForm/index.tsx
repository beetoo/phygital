import React from 'react';

import ModalTemplate from '../../../../components/ModalTemplate';
import { useCheckEmailMessageForm } from '../../hooks/useCheckEmailMessageForm';

import css from './style.m.css';

const CheckEmailMessageForm: React.FC = () => {
    const { recoveryPasswordEmail, goToSignInPage, clearRecoveryPasswordState, t } = useCheckEmailMessageForm();

    return (
        <ModalTemplate isOpen={true} overlayBackground="#f7f7f9">
            <div className={css.wrapCheckEmail}>
                <div className={css.checkEmail}>
                    <h2>{t('checkYourEmail')}</h2>
                    <p className={css.checkEmailParagraph}>
                        {t('textTwo')} <span>{recoveryPasswordEmail}</span> {t('textThree')}
                    </p>
                    <div className={css.checkEmailBlock}>
                        <button className={css.btnCheckEmail} onClick={goToSignInPage}>
                            {t('btnOk')}
                        </button>
                        <p className={css.checkEmailItem}>
                            {t('notYourEmail')}
                            <a onClick={clearRecoveryPasswordState}>{t('changeData')}</a>
                        </p>
                    </div>
                </div>
            </div>
        </ModalTemplate>
    );
};

export default CheckEmailMessageForm;
