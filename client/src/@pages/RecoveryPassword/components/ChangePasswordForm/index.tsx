import React from 'react';
import { Navigate } from 'react-router-dom';
import { clsx } from 'clsx';

import ModalTemplate from '../../../../components/ModalTemplate';
import { useChangePasswordForm } from '../../hooks/useChangePasswordForm';

import css from './style.m.css';

const ChangePasswordForm: React.FC = () => {
    const {
        activeEmail,
        password,
        confirmPassword,
        onChangePassword,
        onChangeConfirmPassword,
        handleSubmit,
        newPasswordError,
        matchPasswordError,
        closeChangePasswordForm,
        tokenChecking,
        hasCheckTokenSuccess,
        isSubmitDisabled,
        isFirstPasswordShow,
        isSecondPasswordShow,
        showHideFirstPassword,
        showHideSecondPassword,
        t,
    } = useChangePasswordForm();

    return tokenChecking ? (
        hasCheckTokenSuccess ? (
            <ModalTemplate isOpen={true} overlayBackground="#f7f7f9">
                <form className={css.passwordNew} onSubmit={(e) => e.preventDefault()}>
                    <h2>{t('setNewPassword')}</h2>
                    <p className={css.passwordNewParagraph}>
                        {t('email')}: <span>{activeEmail}</span>
                    </p>
                    <label />
                    <div className={clsx(css.enterPass, css.tooltip)}>
                        <input
                            className={clsx(css.newPasswordInput, !isSubmitDisabled && css.valid)}
                            type={isFirstPasswordShow ? 'text' : 'password'}
                            placeholder={t('enterPassword') as string}
                            value={password}
                            onChange={onChangePassword}
                            maxLength={125}
                        />
                        <span
                            className={clsx(css.iconEye, {
                                [css.iconEyeOpened]: isFirstPasswordShow,
                                [css.iconEyeClosed]: !isFirstPasswordShow,
                            })}
                            onClick={showHideFirstPassword}
                        />
                        <span className={css.tooltiptext}>
                            {isFirstPasswordShow ? `${t('hidePassword')}` : `${t('viewPassword')}`}
                        </span>
                        {newPasswordError && <div className={css.errorPassword}>{newPasswordError}</div>}
                    </div>
                    <label />
                    <div className={clsx(css.confirmPass, css.tooltip)}>
                        <input
                            className={clsx(css.newPasswordInput, !isSubmitDisabled && css.valid)}
                            type={isSecondPasswordShow ? 'text' : 'password'}
                            placeholder={t('enterPassword1') as string}
                            value={confirmPassword}
                            onChange={onChangeConfirmPassword}
                            maxLength={125}
                        />
                        <span
                            className={clsx(css.iconEye, {
                                [css.iconEyeOpened]: isSecondPasswordShow,
                                [css.iconEyeClosed]: !isSecondPasswordShow,
                            })}
                            onClick={showHideSecondPassword}
                        />
                        <span className={css.tooltiptext}>
                            {isSecondPasswordShow ? `${t('hidePassword')}` : `${t('viewPassword')}`}
                        </span>
                        {matchPasswordError && <div className={css.errorPassword}>{matchPasswordError}</div>}
                    </div>
                    <div className={css.fieldBtnChangePassword}>
                        <button
                            type="submit"
                            className={css.btnChangePassword}
                            onClick={handleSubmit}
                            disabled={isSubmitDisabled}
                        >
                            {t('btnChangePassword')}
                        </button>
                        <span className={css.iconX} onClick={closeChangePasswordForm} />
                    </div>
                </form>
            </ModalTemplate>
        ) : (
            <Navigate to="/recovery-password" />
        )
    ) : null;
};

export default ChangePasswordForm;
