import { ChangeEvent, useCallback, useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import isEmail from 'validator/es/lib/isEmail';

import { selectAuthProcessingStatus, selectAuthErrorMessage } from '../../../selectors';
import { setSendRecoveryPasswordEmail } from '../actions';
import { setClearAuthErrors } from '../../../actions';
import { selectRecoveryPasswordEmail } from '../selectors';

type ReturnType = {
    email: string;
    emailInputError: string | false;
    onChange: ({ target: { value } }: ChangeEvent<HTMLInputElement>) => void;
    handleSubmit: () => void;
    isSubmitButtonDisabled: boolean;
    goToSignInPage: () => void;
    t: TFunction;
};

export const useSendEmailForm = (): ReturnType => {
    const { t } = useTranslation('translation', { keyPrefix: 'signIn' });

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const authProcessing = useSelector(selectAuthProcessingStatus);
    const authErrorMessage = useSelector(selectAuthErrorMessage);
    const recoveryPasswordEmail = useSelector(selectRecoveryPasswordEmail);

    const [email, setEmail] = useState(recoveryPasswordEmail);
    const [emailInputError, setEmailInputError] = useState<string | false>(false);

    useEffect(() => {
        if (authErrorMessage === 'Organization is not exist') {
            setEmailInputError(t('emailIsNotFound'));
        }
    }, [authErrorMessage, t]);

    const onChange = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            if (emailInputError) {
                setEmailInputError(false);
                dispatch(setClearAuthErrors());
            }
            setEmail(value);
        },
        [dispatch, emailInputError],
    );

    const handleSubmit = useCallback(() => {
        dispatch(setSendRecoveryPasswordEmail(email));
    }, [dispatch, email]);

    const goToSignInPage = useCallback(() => {
        navigate('/signin');
    }, [navigate]);

    const isSubmitButtonDisabled = authProcessing || email.trim() === '' || !isEmail(email);

    return {
        email,
        emailInputError,
        onChange,
        handleSubmit,
        isSubmitButtonDisabled,
        goToSignInPage,
        t,
    };
};
