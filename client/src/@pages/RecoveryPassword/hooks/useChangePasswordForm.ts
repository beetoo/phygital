import { ChangeEvent, useCallback, useEffect, useState } from 'react';
import { useNavigate, useMatch } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectAuthEmail, selectTokenCheckingStatus } from '../../../selectors';
import { setChangePassword, setCheckToken } from '../actions';
import { selectCheckTokenSuccessStatus, selectRecoveryPasswordSuccessStatus } from '../selectors';
import { useCustomRouter } from '../../../hooks/useCustomRouter';

type ReturnType = {
    activeEmail: string;
    password: string;
    confirmPassword: string;
    onChangePassword: ({ target: { value } }: ChangeEvent<HTMLInputElement>) => void;
    onChangeConfirmPassword: ({ target: { value } }: ChangeEvent<HTMLInputElement>) => void;
    handleSubmit: () => void;
    newPasswordError: string | false;
    matchPasswordError: string | false;
    closeChangePasswordForm: () => void;
    tokenChecking: boolean;
    hasCheckTokenSuccess: boolean;
    isSubmitDisabled: boolean;
    isFirstPasswordShow: boolean;
    isSecondPasswordShow: boolean;
    showHideFirstPassword: () => void;
    showHideSecondPassword: () => void;
    t: TFunction;
};

export const useChangePasswordForm = (): ReturnType => {
    const { t } = useTranslation('translation', { keyPrefix: 'signIn' });
    const { t: tProfileTab } = useTranslation('translation', { keyPrefix: 'profileTab' });

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const { pushToPage } = useCustomRouter();

    const {
        params: { token },
    } = (useMatch('/recovery-password/:token') as {
        params: { token: string };
    }) ?? { params: { token: '' } };

    const tokenChecking = useSelector(selectTokenCheckingStatus);
    const hasCheckTokenSuccess = useSelector(selectCheckTokenSuccessStatus);
    const hasChangePasswordSuccess = useSelector(selectRecoveryPasswordSuccessStatus);

    const activeEmail = useSelector(selectAuthEmail);

    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const [isFirstPasswordShow, setIsFirstPasswordShow] = useState(false);
    const [isSecondPasswordShow, setIsSecondPasswordShow] = useState(false);
    const [newPasswordError, setNewPasswordError] = useState<string | false>(false);
    const [matchPasswordError, setMatchPasswordError] = useState<string | false>(false);

    useEffect(() => {
        if (token) {
            dispatch(setCheckToken(token));
        }
    }, [dispatch, token]);

    useEffect(() => {
        if (hasChangePasswordSuccess) {
            pushToPage('signin');
        }
    }, [hasChangePasswordSuccess, pushToPage]);

    const onChangePassword = useCallback(
        ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
            value = value.split(' ').join('');

            setNewPasswordError(value.length < 8 ? t('passwordMustContainCharacters') : false);

            setPassword(value);
        },
        [t],
    );

    const onChangeConfirmPassword = useCallback(({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
        setMatchPasswordError(false);
        setConfirmPassword(value);
    }, []);

    const isValid = useCallback(
        (password: string, confirmPassword: string): boolean => {
            password = password.trim();
            confirmPassword = confirmPassword.trim();

            setMatchPasswordError('');

            let isValid = true;
            if (password && confirmPassword && password !== confirmPassword) {
                setMatchPasswordError(tProfileTab('passwordsDontMatch') as string);
                isValid = false;
            }

            return isValid;
        },
        [tProfileTab],
    );

    const handleSubmit = useCallback(() => {
        if (isValid(password, confirmPassword)) {
            dispatch(setChangePassword(password, token));
        }
    }, [confirmPassword, dispatch, isValid, password, token]);

    const closeChangePasswordForm = useCallback(() => {
        navigate('/signin');
    }, [navigate]);

    const showHideFirstPassword = useCallback(() => {
        setIsFirstPasswordShow((prevValue) => !prevValue);
    }, []);

    const showHideSecondPassword = useCallback(() => {
        setIsSecondPasswordShow((prevValue) => !prevValue);
    }, []);

    const isSubmitDisabled = !!newPasswordError || !!matchPasswordError || !password.trim() || !confirmPassword.trim();

    return {
        activeEmail,
        password,
        confirmPassword,
        onChangePassword,
        onChangeConfirmPassword,
        handleSubmit,
        newPasswordError,
        matchPasswordError,
        closeChangePasswordForm,
        tokenChecking,
        hasCheckTokenSuccess,
        isSubmitDisabled,
        isFirstPasswordShow,
        isSecondPasswordShow,
        showHideFirstPassword,
        showHideSecondPassword,
        t,
    };
};
