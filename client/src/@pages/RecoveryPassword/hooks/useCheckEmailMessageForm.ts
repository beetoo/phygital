import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';

import { selectRecoveryPasswordEmail } from '../selectors';
import { setClearRecoveryPasswordState, setRecoveryPasswordEmail } from '../actions';

type ReturnValue = {
    recoveryPasswordEmail: string;
    goToSignInPage: () => void;
    clearRecoveryPasswordState: () => void;
    t: TFunction;
};

export const useCheckEmailMessageForm = (): ReturnValue => {
    const { t } = useTranslation('translation', { keyPrefix: 'signIn' });

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const recoveryPasswordEmail = useSelector(selectRecoveryPasswordEmail);

    const goToSignInPage = useCallback(() => {
        dispatch(setClearRecoveryPasswordState());
        navigate('/signin');
    }, [dispatch, navigate]);

    const clearRecoveryPasswordState = useCallback(() => {
        dispatch(setRecoveryPasswordEmail());
        dispatch(setClearRecoveryPasswordState());
    }, [dispatch]);

    return {
        recoveryPasswordEmail,
        goToSignInPage,
        clearRecoveryPasswordState,
        t,
    };
};
