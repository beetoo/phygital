import { of, Observable } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { combineEpics, ofType } from 'redux-observable';

import {
    CHANGE_PASSWORD,
    setChangePasswordSuccess,
    setChangePasswordFail,
    CHECK_TOKEN,
    setCheckTokenSuccess,
    setCheckTokenFail,
    SEND_RECOVERY_PASSWORD_EMAIL,
    setSendRecoveryPasswordEmailSuccess,
    setSendRecoveryPasswordEmailFail,
} from '../actions';
import { fromRecoveryPassword, fromCheckToken, fromSendRecoveryPasswordEmail } from '../../../resolvers/authResolvers';
import {
    ChangePasswordAction,
    ChangePasswordFailAction,
    ChangePasswordSuccessAction,
    CheckTokenAction,
    CheckTokenFailAction,
    CheckTokenSuccessAction,
    SendRecoveryPasswordEmailAction,
    SendRecoveryPasswordEmailFailAction,
    SendRecoveryPasswordEmailSuccessAction,
} from '../types';
import { setAuthTokenToLocalStorage } from '../../../helpers/httpService/setAuthTokenToLocalStorage';

const sendRecoveryPasswordEmailEpic = (
    action$: Observable<SendRecoveryPasswordEmailAction>,
): Observable<SendRecoveryPasswordEmailSuccessAction | SendRecoveryPasswordEmailFailAction> =>
    action$.pipe(
        ofType(SEND_RECOVERY_PASSWORD_EMAIL),
        mergeMap(({ payload: { email } }) =>
            fromSendRecoveryPasswordEmail(email).pipe(
                map(setSendRecoveryPasswordEmailSuccess),
                catchError((error) => of(setSendRecoveryPasswordEmailFail(error))),
            ),
        ),
    );

const checkTokenEpic = (
    action$: Observable<CheckTokenAction>,
): Observable<CheckTokenSuccessAction | CheckTokenFailAction> =>
    action$.pipe(
        ofType(CHECK_TOKEN),
        mergeMap(({ payload: { token } }) =>
            fromCheckToken(token).pipe(
                map(({ email }) => setCheckTokenSuccess(email)),
                catchError((error) => of(setCheckTokenFail(error))),
            ),
        ),
    );

const changePasswordEpic = (
    action$: Observable<ChangePasswordAction>,
): Observable<ChangePasswordSuccessAction | ChangePasswordFailAction> =>
    action$.pipe(
        ofType(CHANGE_PASSWORD),
        mergeMap(({ payload: { password, token } }) =>
            fromRecoveryPassword(password, token).pipe(
                map((response) => {
                    setAuthTokenToLocalStorage(response);
                    return setChangePasswordSuccess(response);
                }),
                catchError((error) => of(setChangePasswordFail(error))),
            ),
        ),
    );

type EpicsActions =
    | SendRecoveryPasswordEmailAction
    | SendRecoveryPasswordEmailSuccessAction
    | SendRecoveryPasswordEmailFailAction
    | CheckTokenAction
    | CheckTokenSuccessAction
    | CheckTokenFailAction
    | ChangePasswordAction
    | ChangePasswordSuccessAction
    | ChangePasswordFailAction;

export const resetPasswordEpics = combineEpics<EpicsActions>(
    sendRecoveryPasswordEmailEpic,
    checkTokenEpic,
    changePasswordEpic,
);
