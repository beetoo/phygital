import React from 'react';
import { useSelector } from 'react-redux';

import ResetPasswordSendEmailForm from './components/ResetPasswordSendEmailForm';
import CheckEmailMessageForm from './components/CheckEmailMessageForm';

import { selectSendEmailSuccessStatus } from './selectors';

const ResetPasswordPage: React.FC = () => {
    const hasSendEmailSuccess = useSelector(selectSendEmailSuccessStatus);

    return hasSendEmailSuccess ? <CheckEmailMessageForm /> : <ResetPasswordSendEmailForm />;
};

export default ResetPasswordPage;
