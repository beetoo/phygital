import { format } from 'date-fns';
import { TFunction } from 'i18next';

import { FrequencyType } from '../../../../../common/types';
import { TimeFrequency } from '../../PlaylistsPage/hooks/useFrequencySelectValues';
import { toDate } from '../../../helpers';

export const currentDate = (tzHours = 3): Date => {
    const tzOffsetMilliseconds = new Date().getTimezoneOffset() * 60 * 1000;
    const tzMilliseconds = tzHours * 60 * 60 * 1000;

    return new Date(Date.now() + tzOffsetMilliseconds + tzMilliseconds);
};

export const tSelectFrequency = (frequency: FrequencyType, t: TFunction): TimeFrequency => {
    switch (frequency) {
        case 'DAILY':
            return t('everyDay');
        case 'WEEKLY':
            return t('everyWeek');
        default:
            return t('noRepeat');
    }
};

export const tFrequency = (frequencyValue: string, t: TFunction): FrequencyType => {
    switch (frequencyValue) {
        case t('everyDay'):
            return 'DAILY';
        case t('everyWeek'):
            return 'WEEKLY';
        default:
            return '';
    }
};

export const toDatesInfo = (
    startDay: string,
    endDay: string,
    startTime: string,
    endTime: string,
    broadcastAlways: boolean,
): string => {
    const startDate = toDate(startDay, startTime);
    const endDate = toDate(endDay, endTime);

    return `${format(startDate, 'dd.MM.yy')}-${broadcastAlways ? '\u221e' : format(endDate, 'dd.MM.yy')}`;
};

export const toTimesInfo = (startDay: string, endDay: string, startTime: string, endTime: string): string => {
    const startDate = toDate(startDay, startTime);
    const endDate = toDate(endDay, endTime);

    return `${format(startDate, 'HH:mm')}-${format(endDate, 'HH:mm')}`;
};
