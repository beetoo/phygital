import { GET_SCHEDULES, GET_SCHEDULES_FAIL, GET_SCHEDULES_SUCCESS, SET_ACTIVE_SCHEDULE_SCREEN_ID } from '../actions';

import { ScheduleAction, SchedulePageState } from '../type';

const schedulePageInitialState: SchedulePageState = {
    gettingSchedules: false,
    isGetSchedulesSuccess: false,
    schedules: [],
    gettingSchedulesError: null,

    activeScheduleScreenId: '',
    transition: false,
};

export default function schedulePageReducer(
    state = schedulePageInitialState,
    action: ScheduleAction,
): SchedulePageState {
    switch (action.type) {
        case SET_ACTIVE_SCHEDULE_SCREEN_ID:
            return {
                ...state,
                activeScheduleScreenId: action.payload.screenId,
                transition: action.payload.transition,
            };

        case GET_SCHEDULES:
            return {
                ...state,
                gettingSchedules: true,
                isGetSchedulesSuccess: false,
                gettingSchedulesError: null,
            };
        case GET_SCHEDULES_SUCCESS:
            return {
                ...state,
                gettingSchedules: false,
                isGetSchedulesSuccess: true,
                schedules: action.payload.schedules,
            };
        case GET_SCHEDULES_FAIL:
            return {
                ...state,
                gettingSchedules: false,
                gettingSchedulesError: action.payload.errors,
            };

        default:
            return state;
    }
}
