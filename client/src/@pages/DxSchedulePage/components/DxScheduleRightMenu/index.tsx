import React from 'react';
import { clsx } from 'clsx';

import { useScheduleRightMenu } from '../../hooks/useSheduleRightMenu';
import { useOpenEditPlaylistStepThreeButton } from '../../hooks/useOpenEditPlaylistStepThreeButton';
import { locationInfoToString } from '../../../../constants/helpers';
import { tSelectFrequency, toDatesInfo, toTimesInfo } from '../../helpers';
import { getPlaylistStatus } from '../../../PlaylistsPage/helpers';
import { frequencyFromByDay } from '../../../../../../common/helpers';

import css from '../../style.m.css';

const DxScheduleRightMenu: React.FC = () => {
    const { currentDate, activeScreen, goToPlaylist, tSchedule, tPlaylists, tCreatePlaylist } = useScheduleRightMenu();

    const { openEditPlaylistStepThree } = useOpenEditPlaylistStepThreeButton();

    return (
        <div className={css.rightBlockCalendar}>
            <div className={css.rightBlockEmptyCalendar}>
                <h2>{tSchedule('schedule')}</h2>
                <h3>{currentDate}</h3>
                {/*<div className={css.fieldFreeSlot}>*/}
                {/*    <p>Показать свободные временные слоты выбранного экрана на календаре</p>*/}
                {/*    <span className={css.checkboxFreeSlotOn} />*/}
                {/*    <span className={css.checkboxFreeSlotOff} />*/}
                {/*</div>*/}

                <div className={css.wrappCardPlaylistCalendar}>
                    {activeScreen?.schedules.map(
                        ({ id, startDay, endDay, startTime, endTime, broadcastAlways, playlist, byDay }) => (
                            <div key={id} className={css.cardPlaylistCalendar}>
                                <div className={css.cardPriority}>
                                    <h3>{tSchedule('priority')}</h3>
                                </div>

                                <div className={css.fieldCardPlaylistCalendarStatus}>
                                    <p
                                        className={clsx(css.statusOn, {
                                            [css.playlistCardStatusCaptionUploading]: playlist?.status === 'uploading',
                                            [css.playlistCardStatusCaptionError]: playlist?.status === 'upload_error',
                                            [css.playlistCardStatusCaptionActive]:
                                                playlist?.status === 'upload_success',
                                        })}
                                    >
                                        {getPlaylistStatus(playlist?.status, tPlaylists)}
                                    </p>

                                    <span className={css.statusArrowBendUpRight} onClick={goToPlaylist(playlist?.id)}>
                                        <span className={css.tooltiptext}>{tSchedule('btnGotoPlaylist')}</span>
                                    </span>
                                    <span className={css.statusPencil} onClick={openEditPlaylistStepThree(playlist)}>
                                        <span className={css.tooltiptext}>{tSchedule('btnEditSchedule')}</span>
                                    </span>
                                </div>
                                <div className={css.cardPlaylistCalendarName}>
                                    <h2>{playlist?.title}</h2>
                                </div>
                                <div className={css.fieldCardPlaylistCalendarScreenAddress}>
                                    <p className={css.cardPlaylistCalendarScreen}>{activeScreen?.name}</p>
                                    <p className={css.cardPlaylistCalendarAddress}>
                                        {locationInfoToString(
                                            activeScreen?.location?.name,
                                            activeScreen?.location?.city,
                                            activeScreen?.location?.address,
                                        )}
                                    </p>
                                    <span className={css.tooltiptext}>
                                        {locationInfoToString(
                                            activeScreen?.location?.name,
                                            activeScreen?.location?.city,
                                            activeScreen?.location?.address,
                                        )}
                                    </span>
                                </div>
                                <div className={css.fieldCardPlaylistCalendarDateTime}>
                                    <div className={css.cardPlaylistCalendarDate}>
                                        <p>{toDatesInfo(startDay, endDay, startTime, endTime, broadcastAlways)}</p>
                                        <span className={css.tooltiptext}>{tSchedule('broadcastDate')}</span>
                                    </div>
                                    <div className={css.cardPlaylistCalendarTime}>
                                        <p>{toTimesInfo(startDay, endDay, startTime, endTime)}</p>
                                        <span className={css.tooltiptext}>{tSchedule('broadcastTime')}</span>
                                    </div>
                                </div>
                                <div className={css.fieldCardPlaylistCalendarReplay}>
                                    <p>
                                        <span>{tSchedule('repeatRate')}</span>{' '}
                                        {tSelectFrequency(frequencyFromByDay(byDay), tCreatePlaylist)}
                                    </p>
                                </div>
                            </div>
                        ),
                    )}
                </div>
            </div>
        </div>
    );
};

export default DxScheduleRightMenu;
