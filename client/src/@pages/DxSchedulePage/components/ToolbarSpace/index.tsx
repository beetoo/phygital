import React, { memo } from 'react';

import { MenuItem, Select } from '../../../../ui-kit';
import { useToolbarSpace } from '../../hooks/useToolbarSpace';
import { locationCityAddressToString } from '../../../../constants/helpers';

import css from '../../style.m.css';

const ToolbarSpace: React.FC = () => {
    const {
        locations,
        activeScreens,
        selectLocation,
        selectScreen,
        activeLocationName,
        activeScreenName,
        activeLocationTooltip,
    } = useToolbarSpace();

    return (
        <div className={css.selectCalendarPointAndScreen}>
            <div className={css.selectCalendarAddressPoint}>
                <Select
                    width="100%"
                    style={{ marginTop: 0, marginBottom: 0 }}
                    value={activeLocationName}
                    onChange={selectLocation}
                    disabled={locations.length < 2}
                >
                    {locations.map(({ id, name, city, address }) => (
                        <MenuItem
                            id={id}
                            key={id}
                            style={{ display: 'block', fontSize: '14px', padding: '10px', height: '100%' }}
                            value={name}
                        >
                            {name}
                            <br />
                            {locationCityAddressToString(city, address)}
                        </MenuItem>
                    ))}
                </Select>

                {activeLocationTooltip && <span className={css.tooltiptext}>{activeLocationTooltip}</span>}
            </div>
            <div className={css.selectCalendarScreen}>
                <Select
                    width="100%"
                    style={{ marginTop: 0, marginBottom: 0 }}
                    value={activeScreenName}
                    onChange={selectScreen}
                    disabled={activeScreens.length < 2}
                >
                    {activeScreens.map(({ id, name }) => (
                        <MenuItem
                            id={id}
                            key={id}
                            style={{ display: 'block', fontSize: '14px', padding: '10px', height: '100%' }}
                            value={name}
                        >
                            {name}
                        </MenuItem>
                    ))}
                </Select>
                {activeScreenName && <span className={css.tooltiptext}>{activeScreenName}</span>}
            </div>
        </div>
    );
};

export default memo(ToolbarSpace);
