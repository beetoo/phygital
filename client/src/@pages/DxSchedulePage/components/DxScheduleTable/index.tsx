import React from 'react';
import { ViewState } from '@devexpress/dx-react-scheduler';
import { Paper } from '@mui/material';
import {
    Scheduler,
    Toolbar,
    DateNavigator,
    MonthView,
    Appointments,
    AppointmentTooltip,
} from '@devexpress/dx-react-scheduler-material-ui';

import ToolbarSpace from '../ToolbarSpace';
import { currentDate } from '../../helpers';
import { useDxScheduleTable } from '../../../PlaylistsPage/components/PlaylistStepThree/hooks/useDxScheduleTable';
import { DayScaleRow } from '../../../../ui-kit/DayScaleRow';

const DxScheduleTable: React.FC = () => {
    const { appointments, locale } = useDxScheduleTable();

    return (
        <Paper style={{ margin: '24px 24px 0 24px' }}>
            <Scheduler height="auto" data={appointments} locale={locale} firstDayOfWeek={1}>
                <ViewState currentDate={currentDate()} currentViewName="Month" />
                <MonthView dayScaleRowComponent={DayScaleRow} />
                <Toolbar flexibleSpaceComponent={ToolbarSpace} />
                <DateNavigator />
                <Appointments />
                <AppointmentTooltip showCloseButton />
                {/*<AppointmentForm />*/}
            </Scheduler>
        </Paper>
    );
};

export default DxScheduleTable;
