import React from 'react';

import { usePlaylistsPage } from '../PlaylistsPage/hooks/usePlaylistsPage';
import PlaylistStepsContainer from '../PlaylistsPage/components/PlaylistStepsContainer';
import DxScheduleRightMenu from './components/DxScheduleRightMenu';
import DxScheduleTable from './components/DxScheduleTable';

import css from './style.m.css';

const DxSchedulePage: React.FC = () => {
    const { isPlaylistStepMode } = usePlaylistsPage();

    return isPlaylistStepMode ? (
        <PlaylistStepsContainer />
    ) : (
        <div className={css.containerCalendar}>
            <div className={css.centerBlockCalendar}>
                <div className={css.centerBlockCalendarInner}>
                    {/*<div className={css.fieldBtnBlockScreen}>*/}
                    {/*    <button className={css.btnBlockScreen} onClick={openAddPlaylistModal}>*/}
                    {/*        <AddIcon />*/}
                    {/*        {tSchedule('btnAddPlaylist')}*/}
                    {/*    </button>*/}
                    {/*</div>*/}

                    <DxScheduleTable />
                </div>
            </div>
            <DxScheduleRightMenu />
        </div>
    );
};

export default DxSchedulePage;
