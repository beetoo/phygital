import { setActiveScheduleScreenId, setGetSchedules, setGetSchedulesFail, setGetSchedulesSuccess } from '../actions';
import { PlaylistScheduleType } from '../../../../../common/types';
import { ErrorResponse } from '../../../types';

export type SchedulePageState = {
    gettingSchedules: boolean;
    isGetSchedulesSuccess: boolean;
    schedules: PlaylistScheduleType[];
    gettingSchedulesError: ErrorResponse | null;

    activeScheduleScreenId: string;
    transition: boolean;
};

export type SetGetSchedulesAction = ReturnType<typeof setGetSchedules>;
export type SetGetSchedulesSuccessAction = ReturnType<typeof setGetSchedulesSuccess>;
export type SetGetSchedulesFailAction = ReturnType<typeof setGetSchedulesFail>;

export type SetActiveScheduleScreenIdAction = ReturnType<typeof setActiveScheduleScreenId>;

export type ScheduleAction =
    | SetGetSchedulesAction
    | SetGetSchedulesSuccessAction
    | SetGetSchedulesFailAction
    | SetActiveScheduleScreenIdAction;
