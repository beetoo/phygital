import { StoreType } from '../../../types';
import { PlaylistScheduleType } from '../../../../../common/types';

export const selectActiveScheduleScreenId = ({
    pages: {
        organization: {
            schedule: { activeScheduleScreenId },
        },
    },
}: StoreType): string => activeScheduleScreenId;

export const selectTransitionStatus = ({
    pages: {
        organization: {
            schedule: { transition },
        },
    },
}: StoreType): boolean => transition;

// расписания

export const selectSchedules = ({
    pages: {
        organization: {
            schedule: { schedules },
        },
    },
}: StoreType): PlaylistScheduleType[] => schedules;
