import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { combineEpics, ofType } from 'redux-observable';

import { SetGetSchedulesAction, SetGetSchedulesFailAction, SetGetSchedulesSuccessAction } from '../type';
import { GET_SCHEDULES, setGetSchedulesFail, setGetSchedulesSuccess } from '../actions';
import { fromGetSchedules } from '../../../resolvers/schedulesResolvers';

const getSchedulesEpic = (
    action$: Observable<SetGetSchedulesAction>,
): Observable<SetGetSchedulesSuccessAction | SetGetSchedulesFailAction> =>
    action$.pipe(
        ofType(GET_SCHEDULES),
        mergeMap(({ payload: { organizationId } }) =>
            fromGetSchedules(organizationId).pipe(
                map(setGetSchedulesSuccess),
                catchError((error) => of(setGetSchedulesFail(error))),
            ),
        ),
    );

type EpicsActions = SetGetSchedulesAction | SetGetSchedulesSuccessAction | SetGetSchedulesFailAction;

export const schedulesEpics = combineEpics<EpicsActions>(getSchedulesEpic);
