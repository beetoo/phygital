import { PlaylistScheduleType } from '../../../../../common/types';
import { ErrorResponse } from '../../../types';

export const GET_SCHEDULES = 'GET_SCHEDULES';
export const GET_SCHEDULES_SUCCESS = 'GET_SCHEDULES_SUCCESS';
export const GET_SCHEDULES_FAIL = 'GET_SCHEDULES_FAIL';

export const SET_ACTIVE_SCHEDULE_SCREEN_ID = 'SET_ACTIVE_SCHEDULE_SCREEN_ID';

export const setGetSchedules = (organizationId: string) =>
    ({ type: GET_SCHEDULES, payload: { organizationId } }) as const;
export const setGetSchedulesSuccess = (schedules: PlaylistScheduleType[]) =>
    ({ type: GET_SCHEDULES_SUCCESS, payload: { schedules } }) as const;
export const setGetSchedulesFail = (errors: ErrorResponse) =>
    ({ type: GET_SCHEDULES_FAIL, payload: { errors } }) as const;

export const setActiveScheduleScreenId = (screenId = '', transition = false) =>
    ({ type: SET_ACTIVE_SCHEDULE_SCREEN_ID, payload: { screenId, transition } }) as const;
