import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { TFunction } from 'i18next';
import { format } from 'date-fns';
import { ru, enUS, es } from 'date-fns/locale';

import { selectActiveScheduleScreenId } from '../selectors';
import { ScreenType } from '../../../../../common/types';
import { selectScreens } from '../../ScreensPage/selectors';
import { setActivePlaylistId } from '../../PlaylistsPage/actions';
import { useCustomRouter } from '../../../hooks/useCustomRouter';
import { selectCurrentOrganization } from '../../ProfilePage/selectors';
import { isProductionEngClient } from '../../../constants/environments';

type ReturnValue = {
    currentDate: string;
    activeScreen?: ScreenType;
    goToPlaylist: (playlistId: string) => () => void;
    tSchedule: TFunction;
    tPlaylists: TFunction;
    tCreatePlaylist: TFunction;
};

export const useScheduleRightMenu = (): ReturnValue => {
    const { t: tSchedule } = useTranslation('translation', { keyPrefix: 'schedule' });
    const { t: tPlaylists } = useTranslation('translation', { keyPrefix: 'playlists' });
    const { t: tCreatePlaylist } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const { language } = useSelector(selectCurrentOrganization);
    const locale = isProductionEngClient || language === 'en' ? enUS : language === 'ru' ? ru : es;

    const currentDate = format(new Date(), 'd MMMM yyyy', { locale });
    const activeScreenId = useSelector(selectActiveScheduleScreenId);
    const screens = useSelector(selectScreens);

    const activeScreen = useMemo(() => screens.find(({ id }) => id === activeScreenId), [activeScreenId, screens]);

    const dispatch = useDispatch();

    const { pushToPage } = useCustomRouter();

    // переход в плейлист
    const goToPlaylist = useCallback(
        (playlistId: string) => () => {
            dispatch(setActivePlaylistId(playlistId));
            pushToPage('playlists');
        },
        [dispatch, pushToPage],
    );

    return {
        currentDate,
        activeScreen,
        goToPlaylist,
        tSchedule,
        tPlaylists,
        tCreatePlaylist,
    };
};
