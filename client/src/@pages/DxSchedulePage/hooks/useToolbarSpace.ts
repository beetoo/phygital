import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';

import { selectLocations, selectScreens } from '../../ScreensPage/selectors';
import { LocationType, ScreenType } from '../../../../../common/types';
import { setActiveScheduleScreenId } from '../actions';
import { locationInfoToString } from '../../../constants/helpers';
import { selectActiveScheduleScreenId, selectTransitionStatus } from '../selectors';

type ReturnValue = {
    activeLocationName: string | '';
    activeScreenName: string | '';
    locations: LocationType[];
    activeScreens: ScreenType[];
    selectLocation: (e: SelectChangeEvent, child: JSX.Element) => void;
    selectScreen: (e: SelectChangeEvent, child: JSX.Element) => void;
    activeLocationTooltip: string;
};

export const useToolbarSpace = (): ReturnValue => {
    const dispatch = useDispatch();

    const locations = useSelector(selectLocations);
    const screens = useSelector(selectScreens);
    const activeScheduleScreenId = useSelector(selectActiveScheduleScreenId);
    const transition = useSelector(selectTransitionStatus);

    const [activeLocationId, setActiveLocationId] = useState<string>('');

    const activeLocation = useMemo(
        () => locations.find(({ screens }) => screens.some(({ id }) => id === activeScheduleScreenId)),
        [activeScheduleScreenId, locations],
    );

    const activeScreens = useMemo(
        () => screens.filter(({ location }) => location?.id === activeLocationId) ?? [],
        [activeLocationId, screens],
    );

    const activeScreen = useMemo(
        () => activeScreens.find(({ id }) => id === activeScheduleScreenId),
        [activeScheduleScreenId, activeScreens],
    );

    const firstScreenName = useMemo(() => (activeScreens.length > 0 ? activeScreens[0].name : ''), [activeScreens]);

    const [activeLocationName, setActiveLocationName] = useState<string | ''>('');
    const [activeScreenName, setActiveScreenName] = useState<string | ''>('');

    const selectLocation = useCallback(({ target: { value } }: SelectChangeEvent, { props: { id } }: JSX.Element) => {
        setActiveLocationId(id);
        setActiveLocationName(value);
    }, []);

    const selectScreen = useCallback(
        ({ target: { value } }: SelectChangeEvent, { props: { id } }: JSX.Element) => {
            dispatch(setActiveScheduleScreenId(id));
            setActiveScreenName(value);
        },
        [dispatch],
    );

    const activeLocationTooltip = useMemo(() => {
        const location = locations.find(({ id }) => activeLocationId === id);
        return locationInfoToString(location?.name, location?.city, location?.address);
    }, [activeLocationId, locations]);

    useEffect(() => {
        if (!activeLocationId && locations.length > 0) {
            if (activeScheduleScreenId === '') {
                setActiveLocationId(locations[0].id);
                setActiveLocationName(locations[0].name);
            } else {
                setActiveLocationId(activeLocation?.id ?? '');
                setActiveLocationName(activeLocation ? activeLocation.name : '');
            }
        }
    }, [activeLocation, activeLocationId, activeScheduleScreenId, locations]);

    const prevLocationId = useRef('');

    useEffect(() => {
        if (prevLocationId.current !== activeLocationId) {
            prevLocationId.current = activeLocationId;

            if (activeScheduleScreenId === '' || !activeScreen) {
                setActiveScreenName(firstScreenName);
                dispatch(setActiveScheduleScreenId(activeScreens[0]?.id));
            } else {
                setActiveScreenName(activeScreen?.name);
            }
        }
    }, [activeLocationId, activeScheduleScreenId, activeScreen, activeScreens, dispatch, firstScreenName]);

    useEffect(() => {
        if (transition && activeScheduleScreenId) {
            dispatch(setActiveScheduleScreenId(activeScheduleScreenId, false));
        }
    }, [activeScheduleScreenId, dispatch, transition]);

    return {
        locations,
        activeScreens,
        selectLocation,
        selectScreen,
        activeLocationName,
        activeScreenName,
        activeLocationTooltip,
    };
};
