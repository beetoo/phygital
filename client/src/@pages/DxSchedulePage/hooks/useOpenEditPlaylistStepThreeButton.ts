import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import sortBy from 'lodash/sortBy';

import {
    setActivePlaylistId,
    setPlaylistDtoInfo,
    setPlaylistDtoLog,
    setPlaylistDtoPlaylistItems,
    setPlaylistDtoSchedules,
    setPlaylistDtoScreens,
    setPlaylistDtoSync,
} from '../../PlaylistsPage/actions';
import { selectPlaylists } from '../../PlaylistsPage/selectors';
import { selectScreens } from '../../ScreensPage/selectors';
import { formatPlaylistSchedules } from '../../PlaylistsPage/helpers';
import { PlaylistType } from '../../../../../common/types';
import { useCustomRouter } from '../../../hooks/useCustomRouter';

type ReturnValue = {
    openEditPlaylistStepThree: (playlist: PlaylistType) => () => void;
};

export const useOpenEditPlaylistStepThreeButton = (): ReturnValue => {
    const dispatch = useDispatch();

    const { pushToPage } = useCustomRouter();

    const playlists = useSelector(selectPlaylists);
    const screens = useSelector(selectScreens);

    const activePlaylist = useCallback(
        (activePlaylistId: string) => playlists.find(({ id }) => id === activePlaylistId),
        [playlists],
    );

    // находим экраны, использующие этот плейлист
    const playlistScreensIds = useCallback(
        (activePlaylistId: string) =>
            screens
                .filter(({ schedules }) => schedules.some(({ playlist }) => playlist?.id === activePlaylistId))
                .map(({ id }) => id),
        [screens],
    );

    // находим playlistItems, которые использует плейлист
    const currentPlaylistItems = useCallback(
        (activePlaylistId: string) =>
            sortBy(activePlaylist(activePlaylistId)?.playlistItems || [], (o) => o.orderIndex),
        [activePlaylist],
    );

    // находим расписания, которые используются этим плейлистом
    const currentPlaylistSchedules = useCallback(
        (activePlaylistId: string) => formatPlaylistSchedules(activePlaylist(activePlaylistId)?.schedules ?? []),
        [activePlaylist],
    );

    const openEditPlaylistStepThree = useCallback(
        (playlist: PlaylistType) => () => {
            const { id, title, sync, log } = playlist;
            const screenIds = playlistScreensIds(id).map((id) => ({ id }));

            dispatch(setActivePlaylistId(id));
            dispatch(setPlaylistDtoInfo({ title, draft: false }));
            dispatch(setPlaylistDtoScreens(screenIds));
            dispatch(setPlaylistDtoPlaylistItems(currentPlaylistItems(id)));
            dispatch(setPlaylistDtoSchedules(currentPlaylistSchedules(id)));
            dispatch(setPlaylistDtoSync(sync));
            dispatch(setPlaylistDtoLog(log));

            pushToPage('playlists/editing_three');
        },
        [currentPlaylistItems, currentPlaylistSchedules, dispatch, playlistScreensIds, pushToPage],
    );

    return {
        openEditPlaylistStepThree,
    };
};
