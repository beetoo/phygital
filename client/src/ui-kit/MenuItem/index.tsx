import React from 'react';
import { MenuItem as MenuItemBase, MenuItemProps, styled } from '@mui/material';

const StyledMenuItem = styled(MenuItemBase)({
    '&.Mui-selected': {
        backgroundColor: 'white',
        color: '#566dec',
    },
});

export const MenuItem: React.FC<MenuItemProps> = (props: MenuItemProps) => (
    <StyledMenuItem {...props} autoFocus={false} />
);
