import React from 'react';
import { useTranslation } from 'react-i18next';

import css from './style.m.css';

export const DayScaleRow: React.FC = () => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const weekDays = [t('dayMo'), t('dayTu'), t('dayWe'), t('dayTh'), t('dayFr'), t('daySa'), t('daySu')];

    return (
        <tr className={css.blockDays}>
            {weekDays.map((day) => (
                <td className={css.itemDay} key={day}>
                    {day}
                </td>
            ))}
        </tr>
    );
};
