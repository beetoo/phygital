import React, { PropsWithChildren } from 'react';
import { Button as MuiButton, ButtonProps, styled } from '@mui/material';

export const Button: any = styled((props: PropsWithChildren<Omit<ButtonProps, 'variant'>>) => (
    <MuiButton variant="text" {...props} />
))(`
    font-size: 14px;
    color: #c2c4d4;
    text-transform: none;
`);
