import React, { forwardRef } from 'react';
import { CircularProgress, CircularProgressProps } from '@mui/material';

// @ts-ignore
export const SnackbarProgress: React.FC<CircularProgressProps> = forwardRef((props, ref) => (
    <CircularProgress size="25px" thickness={4} {...props} ref={ref} />
));

SnackbarProgress.displayName = 'SnackbarProgress';
