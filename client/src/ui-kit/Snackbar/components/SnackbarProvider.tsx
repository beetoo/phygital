import React, { ComponentProps, FC } from 'react';
import { SnackbarProvider as SnackbarProviderBase } from 'notistack';
import { SnackbarItem } from './SnackbarItem';

export const SnackbarProvider: FC<ComponentProps<typeof SnackbarProviderBase>> = (props) => (
    <SnackbarProviderBase
        {...props}
        preventDuplicate={true}
        content={(key, message) => <SnackbarItem id={key} message={message} />}
    />
);
