import { SnackbarKey, SnackbarMessage } from 'notistack';
import React, { forwardRef } from 'react';

export const SnackbarItem = forwardRef<HTMLDivElement, { id: SnackbarKey; message: SnackbarMessage }>(
    function MyCustomChildren({ id, message }, ref) {
        return (
            <div id={id.toString()} ref={ref}>
                {message}
            </div>
        );
    },
);
