import React, { ReactNode, CSSProperties } from 'react';
import { makeStyles } from '@mui/styles';
import { SnackbarContent } from 'notistack';
import CloseIcon from '@mui/icons-material/Close';
import {
    Box,
    Card as CardBase,
    CardActions,
    Collapse,
    IconButton,
    Paper,
    PaperProps,
    styled,
    Typography,
} from '@mui/material';

export * from './components/SnackbarProvider';
export * from './components/Button';

const useClasses = makeStyles(() => ({
    root: {},
}));

const Card = styled(CardBase)(`background: transparent`);

export const Snackbar: React.FC<{
    title: ReactNode;
    onClose?: () => void;
    isCloseButtonDisabled?: boolean;
    startAdornment?: ReactNode;
    endAdornment?: ReactNode;
    content?: ReactNode;
    PaperStyle?: CSSProperties;
    PaperProps?: PaperProps;
}> = ({ title, onClose, isCloseButtonDisabled, startAdornment, endAdornment, content, PaperStyle, PaperProps }) => {
    const classes = useClasses();
    return (
        <SnackbarContent className={classes.root}>
            <Paper elevation={4} style={PaperStyle} {...PaperProps}>
                <Card raised={false}>
                    <CardActions
                        sx={{
                            backgroundColor: ({ palette }) => palette.background.default,
                            display: 'flex',

                            boxSizing: 'border-box',

                            height: '50px',
                            padding: '12px',
                        }}
                    >
                        {startAdornment && <Box sx={{ marginRight: 1 }}>{startAdornment}</Box>}
                        <Typography
                            variant="subtitle2"
                            color="white"
                            sx={{
                                flex: 1,
                                fontFamily: 'Inter',
                                fontSize: '14px',
                                fontWeight: 400,
                                fontStyle: 'normal',
                                lineHeight: '17px',
                                marginRight: '5px',
                            }}
                        >
                            {title}
                        </Typography>
                        {endAdornment && <Box sx={{ marginLeft: 1 }}>{endAdornment}</Box>}
                        {onClose && (
                            <IconButton
                                aria-label="close"
                                onClick={onClose}
                                sx={{
                                    '&.MuiButtonBase-root.MuiIconButton-root': {
                                        padding: 0,
                                    },
                                }}
                                disabled={isCloseButtonDisabled}
                            >
                                <CloseIcon
                                    htmlColor="#c2c4d4"
                                    sx={{
                                        '&.MuiSvgIcon-root': {
                                            width: '22px',
                                            height: '22px',
                                        },
                                    }}
                                />
                            </IconButton>
                        )}
                    </CardActions>
                    <Collapse in={!!content} timeout="auto" unmountOnExit={true}>
                        <Box
                            margin="auto"
                            padding="0 18px"
                            // p={2}
                        >
                            {content}
                        </Box>
                    </Collapse>
                </Card>
            </Paper>
        </SnackbarContent>
    );
};
