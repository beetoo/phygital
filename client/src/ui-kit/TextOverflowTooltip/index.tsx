import React from 'react';
import { TooltipProps } from '@mui/material';

import { Tooltip } from '../Tooltip';
import { useTextOverflow } from '../../hooks/useTextOverflow';

export type OverflowTooltipFontProps = {
    fontSize: CSSStyleDeclaration['fontSize'];
    fontWeight: CSSStyleDeclaration['fontWeight'];
    fontFamily?: CSSStyleDeclaration['fontFamily'];
    fontStyle?: CSSStyleDeclaration['fontStyle'];
};

interface Props extends TooltipProps {
    fontProps: OverflowTooltipFontProps;
}

export const TextOverflowTooltip: React.FC<Props> = ({
    children,
    title,
    disableHoverListener,
    fontProps,
    ...props
}) => {
    const { isTextOverflow } = useTextOverflow(children, fontProps);

    return (
        <Tooltip
            {...props}
            title={title}
            disableHoverListener={disableHoverListener ? true : !isTextOverflow}
            disableFocusListener={true}
            disableTouchListener={true}
        >
            {children}
        </Tooltip>
    );
};
