import React from 'react';
import { Popover, PopoverProps, styled } from '@mui/material';

const StyledPopover = styled(Popover)`
    .MuiPaper-root {
        // box-shadow: none;
        filter: drop-shadow(2px 4px 20px rgba(166, 167, 195, 0.3));
    }
`;

export const PopoverCustom: React.FC<PopoverProps> = (props: PopoverProps) => (
    <StyledPopover
        sx={{ left: '2px' }}
        anchorOrigin={{
            vertical: 'center',
            horizontal: 'right',
        }}
        transformOrigin={{
            vertical: 'center',
            horizontal: 'left',
        }}
        {...props}
    >
        {props.children}
    </StyledPopover>
);
