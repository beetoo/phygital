import React, { useEffect, useState } from 'react';
import PhoneInput, { isPossiblePhoneNumber } from 'react-phone-number-input';
import { iso1A2Code } from '@rapideditor/country-coder';
import { clsx } from 'clsx';

import { httpService } from '../../helpers/httpService';
import { isProductionEngClient } from '../../constants/environments';

import 'react-phone-number-input/style.css';
import css from './style.m.css';

type Props = {
    phone: string;
    onPhoneInputChange: (value: string) => void;
    phoneInputError: string | false;
    style?: React.CSSProperties;
    className?: string;
    countryCheck?: boolean;
    validCheck?: boolean;
};

const getCountry = (): Promise<string | null> =>
    new Promise((resolve) => {
        window.navigator.geolocation.getCurrentPosition(({ coords: { latitude, longitude } }) => {
            const country = iso1A2Code([longitude, latitude]);
            resolve(country);
        });
    });

const PhoneInputCustom: React.FC<Props> = ({
    phone,
    onPhoneInputChange,
    phoneInputError,
    className,
    style,
    countryCheck = false,
    validCheck = false,
}) => {
    const defaultCountryValue = window.navigator.language.match(/[a-z]{2}-[a-z]{2}/i)
        ? (window.navigator.language.slice(3).toUpperCase() ?? (isProductionEngClient ? 'US' : 'RU'))
        : window.navigator.language.toUpperCase();

    const [defaultCountry, setDefaultCountry] = useState(defaultCountryValue);

    useEffect(() => {
        if (countryCheck) {
            httpService
                .get('country')
                .then(async (data) => {
                    const country = (data as { country: string }).country || (await getCountry());
                    if (country) {
                        setDefaultCountry(country);
                    }
                })
                .catch(async () => {
                    const country = await getCountry();
                    if (country) {
                        setDefaultCountry(country);
                    }
                });
        }
    }, [countryCheck]);

    return (
        <PhoneInput
            style={style}
            defaultCountry={defaultCountry as any}
            className={clsx(
                css.PhoneInput,
                phoneInputError && css.error,
                {
                    [css.valid]: validCheck ? phone && isPossiblePhoneNumber(phone) : false,
                    [css.iconWarning]: phoneInputError,
                },
                className,
            )}
            // @ts-ignore
            value={phone}
            // @ts-ignore
            onChange={onPhoneInputChange}
            international={true}
            countryCallingCodeEditable={true}
            maxLength={20}
        />
    );
};

export default PhoneInputCustom;
