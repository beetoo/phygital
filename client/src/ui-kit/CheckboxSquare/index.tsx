import React from 'react';
import { Checkbox, CheckboxProps } from '@mui/material';

export const CheckboxSquare: React.FC<CheckboxProps> = (props) => (
    <Checkbox
        sx={{
            padding: 0,

            '&:hover': {
                bgcolor: 'transparent',
            },
        }}
        icon={
            <svg width="20" height="20" viewBox="0 0 20 20" fill="none">
                <rect x="0.5" y="0.5" width="19" height="19" rx="1.5" fill="white" stroke="#DDDFEE" />
            </svg>
        }
        checkedIcon={
            <svg width="20" height="20" viewBox="0 0 20 20" fill="none">
                <path
                    d="M18 0H2C0.895431 0 0 0.895431 0 2V18C0 19.1046 0.895431 20 2 20H18C19.1046 20 20 19.1046 20 18V2C20 0.895431 19.1046 0 18 0Z"
                    fill="#3B51F9"
                />
                <path
                    d="M16.875 5.62549L8.125 14.3751L3.75 10.0005"
                    stroke="white"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                />
            </svg>
        }
        {...props}
    />
);
