import React from 'react';
import { InputBase, InputBaseProps, styled } from '@mui/material';

export const CustomInput: React.FC<InputBaseProps> = styled(InputBase)({
    boxSizing: 'border-box',
    width: '360px',
    height: '42px',
    padding: '10px 20px',

    border: '1px solid #dddfee',
    borderRadius: '6px',

    outline: 'none',
    background: '#fff',

    color: '#101431',

    fontFamily: 'Inter, sans-serif',
    fontSize: '14px',
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: '22px',

    '& input::placeholder': {
        color: '#101431',
        opacity: 0.8,
    },

    '&:hover': {
        border: '1px solid #bec1d2',
    },

    '&:active, &:focus-within': {
        border: '1px solid #566dec',
    },
});
