import React from 'react';
import { styled } from '@mui/material/styles';
import { Tooltip as MuiTooltip, TooltipProps, tooltipClasses } from '@mui/material';

const StyledTooltip: React.FC<TooltipProps> = styled(({ className, ...props }: TooltipProps) => (
    <MuiTooltip {...props} arrow classes={{ popper: className }}>
        {props.children}
    </MuiTooltip>
))(() => ({
    [`& .${tooltipClasses.arrow}`]: {
        color: '#101431',
    },
    [`& .${tooltipClasses.tooltip}`]: {
        color: '#fff',
        backgroundColor: '#101431',
        borderRadius: '6px',
        fontFamily: 'Inter, sans-serif',
        fontSize: '12px',
        fontWeight: 'normal',
        fontStyle: 'normal',
        lineHeight: '16px',
    },
}));

export const Tooltip: React.FC<TooltipProps> = (props) => <StyledTooltip {...props}>{props.children}</StyledTooltip>;
