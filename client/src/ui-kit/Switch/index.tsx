import * as React from 'react';
import { clsx } from 'clsx';
import { styled } from '@mui/system';
import { useSwitch, UseSwitchInputSlotProps } from '@mui/base';
import { FC } from 'react';

const BasicSwitchRoot = styled('span')(`
  font-size: 0;
  position: relative;
  display: inline-block;

  width: 36px;
height: 20px;
left: 36px;
top: 0px;

background: #3B51F9;
border-radius: 90.9091px;

  margin: 10px;
  cursor: pointer;

  &.Switch-disabled {
    opacity: 0.4;
    cursor: not-allowed;
  }

  &.Switch-checked {
    background: #007FFF;
  }
`);

const BasicSwitchInput = styled('input')(`
  cursor: inherit;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  opacity: 0;
  z-index: 1;
  margin: 0;
`);

const BasicSwitchThumb = styled('span')(`
  display: block;

  width: 16px;
    height: 16px;
left: 34px;
top: 2px;
background: #FFFFFF;

  border-radius: 100%;
  position: relative;
  transition: all 200ms ease;

  &.Switch-focusVisible {
    background-color: rgba(255,255,255,1);
    box-shadow: 0 0 1px 8px rgba(0,0,0,0.25);
  }

  &.Switch-checked {
    left: 14px;
    top: 3px;
    background-color: #FFF;
  }
`);

//TODO: sync with Figma/UI-kit/ "Radio / Toggle / Check"
export const SwitchCustom: FC<UseSwitchInputSlotProps> = (props) => {
    const { getInputProps, checked, disabled, focusVisible } = useSwitch(props);

    const stateClasses = {
        'Switch-checked': checked,
        'Switch-disabled': disabled,
        'Switch-focusVisible': focusVisible,
    };

    return (
        <BasicSwitchRoot className={clsx(stateClasses)}>
            <BasicSwitchThumb className={clsx(stateClasses)} />
            <BasicSwitchInput {...getInputProps()} aria-label="Demo switch" />
        </BasicSwitchRoot>
    );
};
