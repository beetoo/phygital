import React from 'react';
import { SvgIconProps } from '@mui/material';

export const CircleIcon: React.FC<SvgIconProps> = (props) => (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
        <path
            d="M12 21.4991C17.2465 21.4991 21.4996 17.246 21.4996 11.9995C21.4996 6.75309 17.2465 2.5 12 2.5C6.75358 2.5 2.50049 6.75309 2.50049 11.9995C2.50049 17.246 6.75358 21.4991 12 21.4991Z"
            strokeWidth="1.3"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </svg>
);
