import React from 'react';
import { SvgIcon, SvgIconProps } from '@mui/material';

export const SelectIcon: React.FC<SvgIconProps> = (props) => (
    <SvgIcon width="26" height="26" viewBox="0 0 26 26" fill="none" stroke="#101431" {...props}>
        {/* <path d="M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z" /> */}
        <path d="M 16.25 12 L 12 16.25 L 8 12 L 6 10 l 6 6 l 6-6 Z" />
    </SvgIcon>
);
