import React, { forwardRef } from 'react';
import { SvgIcon, SvgIconProps } from '@mui/material';

// @ts-ignore
export const UploadCancelIcon: React.FC<SvgIconProps> = forwardRef((props, ref) => (
    <SvgIcon
        width="22"
        height="22"
        viewBox="0 0 22 22"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
        ref={ref}
    >
        <path
            d="M17.1875 4.8125L4.8125 17.1875"
            stroke="#9093AE"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            d="M17.1875 17.1875L4.8125 4.8125"
            stroke="#9093AE"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </SvgIcon>
));

UploadCancelIcon.displayName = 'UploadCancelIcon';
