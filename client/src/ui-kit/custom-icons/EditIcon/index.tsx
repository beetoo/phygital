import React from 'react';
import { SvgIconProps } from '@mui/material';

export const EditIcon: React.FC<SvgIconProps> = (props) => (
    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
        <path
            d="M7.96523 18.5624H4.125C3.94266 18.5624 3.7678 18.49 3.63886 18.361C3.50993 18.2321 3.4375 18.0572 3.4375 17.8749V14.0347C3.4375 13.9444 3.45528 13.855 3.48983 13.7716C3.52438 13.6882 3.57502 13.6124 3.63886 13.5485L13.9514 3.23603C14.0803 3.1071 14.2552 3.03467 14.4375 3.03467C14.6198 3.03467 14.7947 3.1071 14.9236 3.23603L18.7639 7.07626C18.8928 7.20519 18.9652 7.38006 18.9652 7.5624C18.9652 7.74473 18.8928 7.9196 18.7639 8.04853L8.45136 18.361C8.38752 18.4249 8.31173 18.4755 8.22832 18.5101C8.14491 18.5446 8.05551 18.5624 7.96523 18.5624Z"
            // stroke="#6C6E82"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            d="M11.6875 5.5L16.5 10.3125"
            // stroke="#6C6E82"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            d="M8.20629 18.5185L3.48145 13.7937"
            // stroke="#6C6E82"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </svg>
);
