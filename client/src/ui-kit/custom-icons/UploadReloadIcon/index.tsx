import React, { forwardRef } from 'react';
import { SvgIcon, SvgIconProps } from '@mui/material';

// @ts-ignore
export const UploadReloadIcon: React.FC<SvgIconProps> = forwardRef((props, ref) => (
    <SvgIcon width="22" height="22" viewBox="0 0 22 22" xmlns="http://www.w3.org/2000/svg" {...props} ref={ref}>
        <path
            d="M15.1394 8.56923H19.2644V4.44423"
            stroke="#9093AE"
            strokeWidth="1.3"
            strokeLinecap="round"
            strokeLinejoin="round"
            fill="none"
        />
        <path
            d="M16.3475 16.3475C15.2899 17.4051 13.9424 18.1254 12.4754 18.4172C11.0084 18.709 9.48782 18.5592 8.10596 17.9868C6.72409 17.4145 5.54299 16.4452 4.71201 15.2015C3.88103 13.9579 3.4375 12.4957 3.4375 11C3.4375 9.50428 3.88103 8.04215 4.71201 6.7985C5.54299 5.55486 6.72409 4.58555 8.10596 4.01316C9.48782 3.44078 11.0084 3.29101 12.4754 3.58281C13.9424 3.87462 15.2899 4.59487 16.3475 5.65251L19.2643 8.56932"
            stroke="#9093AE"
            strokeWidth="1.3"
            strokeLinecap="round"
            strokeLinejoin="round"
            fill="none"
        />
    </SvgIcon>
));

UploadReloadIcon.displayName = 'UploadReloadIcon';
