import React from 'react';
import { SvgIcon, SvgIconProps } from '@mui/material';

export const VerticalDesktopIcon: React.FC<SvgIconProps> = (props) => (
    <SvgIcon width="22" height="22" style={{ marginRight: '8px' }} viewBox="0 0 22 22" {...props}>
        <path
            d="M5.04199 2.94157L5.04199 16.3097C5.04199 17.048 5.6348 17.6465 6.36607 17.6465L15.6346 17.6465C16.3659 17.6465 16.9587 17.048 16.9587 16.3097L16.9587 2.94157C16.9587 2.20327 16.3659 1.60476 15.6346 1.60476L6.36607 1.60476C5.6348 1.60476 5.04199 2.20327 5.04199 2.94157Z"
            stroke="#101431"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
            fill="none"
        />
        <path
            d="M13.75 20.3965H8.25"
            stroke="#101431"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
            fill="none"
        />
        <path
            d="M11 17.6465V20.3965"
            stroke="#101431"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
            fill="none"
        />
    </SvgIcon>
);
