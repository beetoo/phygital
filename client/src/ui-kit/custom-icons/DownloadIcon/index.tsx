import React from 'react';
import { SvgIconProps } from '@mui/material';

export const DownloadIcon: React.FC<SvgIconProps> = (props) => (
    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
        <path
            d="M17.1877 19.25H4.81201C4.62968 19.25 4.45481 19.1776 4.32588 19.0486C4.19694 18.9197 4.12451 18.7448 4.12451 18.5625V3.4375C4.12451 3.25516 4.19694 3.0803 4.32588 2.95136C4.45481 2.82243 4.62968 2.75 4.81201 2.75H13.0627L17.8752 7.5625V18.5625C17.8752 18.6528 17.8574 18.7422 17.8229 18.8256C17.7883 18.909 17.7377 18.9848 17.6738 19.0486C17.61 19.1125 17.5342 19.1631 17.4508 19.1977C17.3674 19.2322 17.278 19.25 17.1877 19.25Z"
            // stroke="#6C6E82"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            d="M13.0625 2.75V7.5625H17.8757"
            // stroke="#6C6E82"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            d="M8.59375 13.4062L11 15.8125L13.4062 13.4062"
            // stroke="#6C6E82"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            d="M11 10.3125V15.8125"
            // stroke="#6C6E82"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </svg>
);
