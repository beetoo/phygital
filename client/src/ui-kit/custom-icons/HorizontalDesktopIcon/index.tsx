import React from 'react';
import { SvgIcon, SvgIconProps } from '@mui/material';

export const HorizontalDesktopIcon: React.FC<SvgIconProps> = (props) => (
    <SvgIcon
        width="22"
        height="22"
        style={{ marginRight: '8px' }}
        viewBox="0 0 22 22"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
    >
        <path
            d="M3.08125 15.584L18.9187 15.584C19.7934 15.584 20.5024 14.9912 20.5024 14.2599V4.99139C20.5024 4.26013 19.7934 3.66732 18.9187 3.66732L3.08125 3.66732C2.20657 3.66732 1.49751 4.26013 1.49751 4.99139V14.2599C1.49751 14.9912 2.20657 15.584 3.08125 15.584Z"
            stroke="#101431"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
            fill="none"
        />
        <path
            d="M13.75 18.334H8.25"
            stroke="#101431"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
            fill="none"
        />
        <path
            d="M11 15.584V18.334"
            stroke="#101431"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
            fill="none"
        />
    </SvgIcon>
);
