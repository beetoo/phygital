import React from 'react';
import { SvgIconProps } from '@mui/material';

export const CheckIcon: React.FC<SvgIconProps> = (props) => (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
        <path
            d="M12 21.4995C17.2465 21.4995 21.4996 17.2464 21.4996 12C21.4996 6.7535 17.2465 2.5004 12 2.5004C6.75358 2.5004 2.50049 6.7535 2.50049 12C2.50049 17.2464 6.75358 21.4995 12 21.4995Z"
            fill="#3B51F9"
            stroke="#3B51F9"
            strokeWidth="1.3"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            d="M16.125 9.75L10.625 15L7.875 12.375"
            stroke="white"
            strokeWidth="1.3"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </svg>
);
