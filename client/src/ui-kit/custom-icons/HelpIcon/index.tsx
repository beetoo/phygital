import React, { forwardRef } from 'react';
import { SvgIcon, SvgIconProps } from '@mui/material';

// @ts-ignore
export const HelpIcon: React.FC<SvgIconProps> = forwardRef((props, ref) => (
    <SvgIcon width="24" height="24" viewBox="0 0 24 24" {...props} ref={ref}>
        <path
            d="M11.9999 21.5007C17.2468 21.5007 21.5003 17.2473 21.5003 12.0004C21.5003 6.75346 17.2468 2.5 11.9999 2.5C6.75297 2.5 2.49951 6.75346 2.49951 12.0004C2.49951 17.2473 6.75297 21.5007 11.9999 21.5007Z"
            strokeWidth="1.3"
            strokeLinecap="round"
            strokeLinejoin="round"
            fill="none"
        />
        <path
            d="M12 13.958V13.208C12.5933 13.208 13.1734 13.0321 13.6667 12.7024C14.1601 12.3728 14.5446 11.9042 14.7716 11.3561C14.9987 10.8079 15.0581 10.2047 14.9424 9.62274C14.8266 9.0408 14.5409 8.50625 14.1213 8.08669C13.7018 7.66713 13.1672 7.38141 12.5853 7.26565C12.0033 7.1499 11.4001 7.20931 10.8519 7.43637C10.3038 7.66343 9.83524 8.04795 9.50559 8.5413C9.17595 9.03465 9 9.61467 9 10.208"
            strokeWidth="1.3"
            strokeLinecap="round"
            strokeLinejoin="round"
            fill="none"
        />
        <path
            d="M12 17.8955C12.5178 17.8955 12.9375 17.4758 12.9375 16.958C12.9375 16.4402 12.5178 16.0205 12 16.0205C11.4822 16.0205 11.0625 16.4402 11.0625 16.958C11.0625 17.4758 11.4822 17.8955 12 17.8955Z"
            fill="#101431"
        />
    </SvgIcon>
));

HelpIcon.displayName = 'HelpIcon';
