import React, { forwardRef } from 'react';
import { SvgIcon, SvgIconProps } from '@mui/material';

// @ts-ignore
export const UploadErrorIcon: React.FC<SvgIconProps> = forwardRef((props, ref) => (
    <SvgIcon
        width="32"
        height="32"
        viewBox="0 0 32 32"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
        ref={ref}
    >
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M16 29C23.1797 29 29 23.1797 29 16C29 8.8203 23.1797 3 16 3C8.8203 3 3 8.8203 3 16C3 23.1797 8.8203 29 16 29ZM20.5303 11.4697C20.8232 11.7626 20.8232 12.2374 20.5303 12.5303L17.0607 16L20.5303 19.4697C20.8232 19.7626 20.8232 20.2374 20.5303 20.5303C20.2374 20.8232 19.7626 20.8232 19.4697 20.5303L16 17.0607L12.5303 20.5303C12.2374 20.8232 11.7626 20.8232 11.4697 20.5303C11.1768 20.2374 11.1768 19.7626 11.4697 19.4697L14.9393 16L11.4697 12.5303C11.1768 12.2374 11.1768 11.7626 11.4697 11.4697C11.7626 11.1768 12.2374 11.1768 12.5303 11.4697L16 14.9393L19.4697 11.4697C19.7626 11.1768 20.2374 11.1768 20.5303 11.4697Z"
            fill="#F95050"
        />
    </SvgIcon>
));

UploadErrorIcon.displayName = 'UploadErrorIcon';
