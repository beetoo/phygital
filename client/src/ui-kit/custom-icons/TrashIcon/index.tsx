import React from 'react';
import { SvgIconProps } from '@mui/material';

export const TrashIcon: React.FC<SvgIconProps> = (props) => (
    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
        <path
            d="M18.562 4.8125L3.43701 4.8125"
            // stroke="#6C6E82"
            strokeWidth="1.19167"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            d="M8.9375 8.9375V14.4375"
            // stroke="#6C6E82"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            d="M13.0625 8.9375V14.4375"
            // stroke="#6C6E82"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            d="M17.187 4.8125V17.875C17.187 18.0573 17.1146 18.2322 16.9856 18.3611C16.8567 18.4901 16.6818 18.5625 16.4995 18.5625H5.49951C5.31718 18.5625 5.14231 18.4901 5.01338 18.3611C4.88444 18.2322 4.81201 18.0573 4.81201 17.875V4.8125"
            // stroke="#6C6E82"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            d="M14.4375 4.8125V3.4375C14.4375 3.07283 14.2926 2.72309 14.0348 2.46523C13.7769 2.20737 13.4272 2.0625 13.0625 2.0625H8.9375C8.57283 2.0625 8.22309 2.20737 7.96523 2.46523C7.70737 2.72309 7.5625 3.07283 7.5625 3.4375V4.8125"
            // stroke="#6C6E82"
            strokeWidth="1.2"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </svg>
);
