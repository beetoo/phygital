import React, { forwardRef } from 'react';
import { SvgIcon, SvgIconProps } from '@mui/material';

// @ts-ignore
export const UploadWarningIcon: React.FC<SvgIconProps> = forwardRef((props, ref) => (
    <SvgIcon
        width="32"
        height="32"
        viewBox="0 0 32 32"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...props}
        ref={ref}
    >
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M14.1355 4.08492L2.28982 24.7369C2.10027 25.0674 2.00031 25.4424 2 25.8242C1.99969 26.206 2.09903 26.5812 2.28803 26.912C2.47704 27.2427 2.74903 27.5174 3.07666 27.7084C3.40428 27.8994 3.77598 28 4.15434 28H27.8457C28.224 28 28.5957 27.8994 28.9233 27.7084C29.251 27.5174 29.523 27.2427 29.712 26.912C29.901 26.5812 30.0003 26.206 30 25.8242C29.9997 25.4424 29.8997 25.0674 29.7102 24.7369L17.8645 4.08492C17.6753 3.755 17.4034 3.48109 17.0761 3.29066C16.7489 3.10024 16.3778 3 16 3C15.6222 3 15.2511 3.10024 14.9239 3.29066C14.5966 3.48109 14.3247 3.755 14.1355 4.08492ZM15 12C15 11.4477 15.4477 11 16 11C16.5523 11 17 11.4477 17 12V18C17 18.5523 16.5523 19 16 19C15.4477 19 15 18.5523 15 18V12ZM17 22C17 22.5523 16.5523 23 16 23C15.4477 23 15 22.5523 15 22C15 21.4477 15.4477 21 16 21C16.5523 21 17 21.4477 17 22Z"
            fill="#F99724"
        />
    </SvgIcon>
));

UploadWarningIcon.displayName = 'UploadWarningIcon';
