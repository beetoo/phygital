import React from 'react';
import { Box } from '@mui/material';
import { styled, alpha } from '@mui/system';
import { Slider, sliderClasses, SliderProps } from '@mui/base/Slider';

// @ts-ignore
import sliderIcon from '../../assets/iconTimeLineSlider.svg';

const StyledSlider = styled(Slider)(
    ({ theme }) => `
  // color: ${theme.palette.mode === 'light' ? '#1976d2' : '#90caf9'};
  color: '#3B51F9';
  width: 100%;
  display: block;
  position: relative;
  cursor: pointer;
  touch-action: none;
  -webkit-tap-highlight-color: transparent;
  // opacity: 0.75;
  z-index: 1;

  // &:hover {
  //   opacity: 1;
  // }

  &.${sliderClasses.disabled} {
    pointer-events: none;
    cursor: default;
    color: #bdbdbd;
  }

  & .${sliderClasses.rail} {
    display: none;
  }

  & .${sliderClasses.track} {
    display: none;
  }

  & .${sliderClasses.thumb} {
    position: absolute;
    margin-left: -13px;
    content: url(${sliderIcon});

    :hover,
    &.${sliderClasses.focusVisible} {
      // box-shadow: 0 0 0 0.25rem ${alpha(theme.palette.mode === 'light' ? '#1976d2' : '#90caf9', 0.15)};
    }

    &.${sliderClasses.active} {
      // box-shadow: 0 0 0 0.25rem ${alpha(theme.palette.mode === 'light' ? '#1976d2' : '#90caf9', 0.3)};
    }
  }
`,
);

interface Props extends SliderProps {
    timelineWidth: number;
}

export const TimelineSlider: React.FC<Props> = ({ timelineWidth, ...props }: Props) => (
    <Box width={timelineWidth || 0}>
        <StyledSlider {...props} />
    </Box>
);
