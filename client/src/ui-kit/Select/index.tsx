import React, { forwardRef, ReactNode, useState } from 'react';
import {
    Select as SelectBase,
    FormControl,
    InputBaseProps,
    InputBase,
    SelectChangeEvent,
    BaseSelectProps,
    styled,
} from '@mui/material';

import { SelectIcon } from '../custom-icons';

const Input = forwardRef(({ ...props }: InputBaseProps, ref) => (
    <InputBase inputProps={{ maxLength: 125 }} ref={ref} {...props} />
));

Input.displayName = 'Input';

interface InputProps extends InputBaseProps {
    shown: 'true' | 'false';
}

const StyledInput: React.FC<InputProps> = styled(Input)(
    ({ theme: { spacing } }) =>
        ({ shown, disabled, startAdornment }: InputProps) => ({
            [startAdornment ? '&' : '& .MuiInputBase-input']: {
                padding: spacing(0.7, 2),

                border: `1px solid ${shown === 'true' ? '#566dec' : '#dddfee'}`,
                borderRadius: 6,

                fontFamily: ['Inter', 'sans-serif'].join(','),
                fontSize: 14,

                '&:hover': {
                    borderColor: disabled ? '#dddfee' : '#bec1d2',
                },

                '& .MuiSvgIcon-root path': {
                    stroke: disabled ? '#dddfee' : '#101431',
                },
                // '&:focus': {
                //     borderColor: '#566dec',
                //     borderRadius: 6,
                // },
                // '&:active': {
                //     borderColor: '#566dec',
                //     borderRadius: 6,
                // },
            },

            '& .MuiSvgIcon-root': {
                stroke: disabled ? '#dddfee' : '#101431',
            },
        }),
);

const Label = styled('label')(({ theme: { spacing } }) => ({
    marginBottom: spacing(1),
}));

interface Props extends BaseSelectProps {
    label?: string;
    placeholder?: string;
    onChange?: (event: SelectChangeEvent<any>, child: ReactNode) => void;
    shown?: 'true' | 'false';
    onOpen?: () => void;
    onClose?: () => void;
    value?: string | string[];
    width?: number | string;
    style?: React.CSSProperties;
    startAdornment?: ReactNode;
    endAdornment?: ReactNode;
    children: ReactNode;
    nativeRender?: boolean;
}

export const Select = forwardRef(({ nativeRender, style, ...props }: Props, ref) => {
    const [shown, setShown] = useState<'true' | 'false'>('false');

    return (
        <div>
            {props.label ? <Label>{props.label}</Label> : null}
            <FormControl
                style={{ marginTop: '10px', marginBottom: '10px', ...style }}
                sx={{
                    width: props.width,
                }}
            >
                <SelectBase
                    ref={ref}
                    {...props}
                    displayEmpty
                    value={props.value ?? ''}
                    input={
                        <StyledInput
                            shown={props.shown ? props.shown : shown}
                            startAdornment={props.startAdornment}
                            endAdornment={props.endAdornment}
                        />
                    }
                    onChange={props.onChange}
                    onOpen={props.onOpen ? props.onOpen : () => setShown('true')}
                    onClose={props.onClose ? props.onClose : () => setShown('false')}
                    MenuProps={{
                        disableScrollLock: false,
                        PaperProps: {
                            style: {
                                marginTop: props.startAdornment ? '12px' : '4px',
                                marginLeft: props.startAdornment ? '-17px' : undefined,
                                width: props.startAdornment ? props.width : undefined,
                                ...props.MenuProps?.PaperProps?.style,
                            },
                        },
                    }}
                    IconComponent={SelectIcon}
                    renderValue={
                        nativeRender
                            ? undefined
                            : (selected) => {
                                  if (selected === '' || (Array.isArray(selected) && selected.length === 0)) {
                                      return props?.placeholder ?? '';
                                  }
                                  if (props.renderValue) {
                                      return props.renderValue(selected);
                                  }
                                  if (Array.isArray(selected)) {
                                      return selected.join(',');
                                  }
                                  return selected;
                              }
                    }
                    // @ts-ignore
                    notched={undefined}
                >
                    {props.children}
                </SelectBase>
            </FormControl>
        </div>
    );
});

Select.displayName = 'Select';
