import React from 'react';
import { styled, Tooltip, tooltipClasses, TooltipProps } from '@mui/material';

import { DataSuggestion } from '../../resolvers/screensResolvers';
import { getSuggestionName } from '../../@pages/ScreensPage/helpers';

export const Listbox: React.FC<any> = styled('ul')(({ theme }) => ({
    width: 336,
    margin: '-12px 0 0',
    padding: '16px 12px',
    zIndex: 1,
    position: 'absolute',
    listStyle: 'none',
    backgroundColor: theme.palette.background.paper,

    boxShadow: '0px 8px 22px rgba(161, 164, 176, 0.2)',
    borderRadius: '6px',

    '& li:hover': {
        color: '#3B51F9',
        cursor: 'pointer',

        borderRadius: '4px',
        backgroundColor: '#EBEEFD',
    },
}));

export const Option: React.FC<any> = styled('li')(() => ({
    margin: 0,
    padding: '4px 8px',

    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',

    color: '#9093AE',

    fontFamily: 'Inter, sans-serif',
    fontSize: '14px',
    fontWeight: 400,
    lineHeight: '22px',
}));

export const Span: React.FC<any> = styled('span')(() => ({
    color: '#3B51F9',
}));

export const SuggestionTooltip: React.FC<TooltipProps> = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip
        {...props}
        classes={{ popper: className }}
        placement="top"
        disableFocusListener={true}
        disableTouchListener={true}
    >
        {props.children}
    </Tooltip>
))(({ theme }) => ({
    [`& .${tooltipClasses.tooltip}`]: {
        backgroundColor: theme.palette.common.white,
        color: 'rgba(0, 0, 0, 0.87)',
        boxShadow: theme.shadows[1],
        fontSize: 11,
    },
}));

type Props = {
    queryValue: string;
    queryAddition: string;
    suggestList: DataSuggestion[];
    onSuggestSelect: (suggestion: DataSuggestion) => () => void;
};

export const SuggestListBox: React.FC<Props> = ({ queryValue, queryAddition, suggestList, onSuggestSelect }: Props) =>
    suggestList.length > 0 ? (
        <Listbox>
            {suggestList.map((suggestion, index) => {
                const { fullName, startNamePart, middleNamePart, lastNamePart } = getSuggestionName(
                    suggestion,
                    queryValue,
                    queryAddition,
                );

                return (
                    <SuggestionTooltip key={index} title={fullName} disableHoverListener={fullName.length < 42}>
                        <Option onClick={onSuggestSelect(suggestion)}>
                            <Span>
                                {startNamePart}
                                <strong>{middleNamePart}</strong>
                                {lastNamePart}
                            </Span>
                        </Option>
                    </SuggestionTooltip>
                );
            })}
        </Listbox>
    ) : null;
