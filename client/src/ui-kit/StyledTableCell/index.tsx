import React from 'react';
import { styled, TableCell, TableCellProps } from '@mui/material';

interface StyledTableCellProps extends TableCellProps {
    borderBottom?: boolean;
}

export const StyledTableCell: React.FC<StyledTableCellProps> = ({ ...props }) => {
    const StyledCell = styled(TableCell)({});

    return <StyledCell {...props} />;
};
