import React from 'react';

import css from './style.m.css';

type Props = {
    text: string;
    onClick: () => void;
    disabled?: boolean;
};

export const CancelButton: React.FC<Props> = ({ text, onClick, disabled }) => {
    return (
        <button type="button" className={css.btnClose} onClick={onClick} disabled={disabled}>
            {text}
        </button>
    );
};
