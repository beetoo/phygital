import React, { forwardRef, DetailedHTMLProps, ButtonHTMLAttributes } from 'react';
import { clsx } from 'clsx';

import { CheckIcon, CircleIcon } from '../../custom-icons';

// @ts-ignore
import css from './style.m.css';

interface Props extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLSpanElement>, HTMLSpanElement> {
    active?: boolean | undefined;
    disabled?: boolean | undefined;
}

export const SelectButton: React.FC<Props> = forwardRef<HTMLSpanElement, Props>(
    ({ active, disabled, className, ...props }, ref) => (
        <span
            {...props}
            ref={ref}
            className={clsx(className, css.circleButton, { [css.active]: active, [css.disabled]: disabled })}
        >
            <CircleIcon className={css.circleIcon} />
            <CheckIcon className={css.checkIcon} />
        </span>
    ),
);

SelectButton.displayName = 'CircleButton';
