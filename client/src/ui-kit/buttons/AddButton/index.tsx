import React, { forwardRef, DetailedHTMLProps, ButtonHTMLAttributes } from 'react';
import { clsx } from 'clsx';

import { AddIcon } from '../../custom-icons';

// @ts-ignore
import css from './style.m.css';

type Props = DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>;

export const AddButton: React.FC<Props> = forwardRef<HTMLButtonElement, Props>(({ disabled, ...props }, ref) => (
    <button {...props} ref={ref} className={clsx(css.addButton, disabled && css.disabled)} disabled={disabled}>
        <AddIcon />
    </button>
));

AddButton.displayName = 'AddButton';
