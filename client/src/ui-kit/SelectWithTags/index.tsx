import React from 'react';
import { useLocation } from 'react-router-dom';
import { Box, Checkbox, Chip } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { SelectChangeEvent } from '@mui/material/Select/SelectInput';
import { useTranslation } from 'react-i18next';

import { Select } from '../Select';
import { MenuItem } from '../MenuItem';

export type SelectTagType = { id: string; name: string };

type Props = {
    isAllTagsSelected: boolean;
    tags: SelectTagType[];
    filteringTags: SelectTagType[];
    onSelectTags: (event: SelectChangeEvent) => void;
    onDeleteTag: (tag: string) => void;
    isTagsSelectDisabled: boolean;
};

export const SelectWithTags: React.FC<Props> = ({
    isAllTagsSelected,
    tags,
    filteringTags,
    onSelectTags,
    onDeleteTag,
    isTagsSelectDisabled,
}) => {
    const { t } = useTranslation('translation', { keyPrefix: 'createPlaylist' });

    const { pathname } = useLocation();

    return (
        <Select
            width="100%"
            style={{
                marginTop: 0,
                marginBottom: 0,
            }}
            sx={{
                '& .MuiSelect-select.MuiSelect-outlined.MuiSelect-multiple.MuiInputBase-input': {
                    boxSizing: 'border-box',
                    height: '42px',
                },
            }}
            placeholder={pathname.includes('content') ? t('selectPlaylists') : t('selectTags')}
            onChange={onSelectTags}
            value={filteringTags.map(({ name }) => name)}
            multiple={true}
            disabled={isTagsSelectDisabled}
            renderValue={() => (
                <Box
                    sx={{
                        display: 'flex',

                        flexWrap: 'wrap',
                        gap: 0.5,
                    }}
                >
                    {filteringTags.map(({ id, name }) => (
                        <Chip
                            style={{
                                boxSizing: 'border-box',
                                height: '24px',

                                borderRadius: '3px',
                                background: '#F0F0F5',
                            }}
                            sx={{
                                '& .MuiChip-label.MuiChip-labelMedium': {
                                    fontWeight: 400,
                                    fontSize: '12px',
                                    lineHeight: '15px',
                                },
                            }}
                            key={id}
                            label={name}
                            deleteIcon={
                                <CloseIcon
                                    style={{
                                        width: 12,
                                        height: 12,

                                        color: '#6C6E82',
                                    }}
                                    onMouseDown={(event) => {
                                        event.stopPropagation();
                                        onDeleteTag(id);
                                    }}
                                />
                            }
                            onDelete={() => ''}
                        />
                    ))}
                </Box>
            )}
        >
            {tags.map(({ id, name }) => (
                <MenuItem
                    key={id}
                    id={id}
                    value={name}
                    sx={{
                        boxSizing: 'border-box',
                        height: '28px',
                        marginBottom: '4px',
                        paddingLeft: 0,
                        fontWeight: id === 'all' ? 'medium' : 'normal',
                    }}
                >
                    <Checkbox
                        checked={id === 'all' ? isAllTagsSelected : filteringTags.some((tag) => tag.id === id)}
                        icon={
                            <svg
                                width="20"
                                height="20"
                                viewBox="0 0 20 20"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <rect x="0.5" y="0.5" width="19" height="19" rx="1.5" fill="white" stroke="#DDDFEE" />
                            </svg>
                        }
                        checkedIcon={
                            <svg
                                width="20"
                                height="20"
                                viewBox="0 0 20 20"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M18 0H2C0.895431 0 0 0.895431 0 2V18C0 19.1046 0.895431 20 2 20H18C19.1046 20 20 19.1046 20 18V2C20 0.895431 19.1046 0 18 0Z"
                                    fill="#3B51F9"
                                />
                                <path
                                    d="M16.875 5.62549L8.125 14.3751L3.75 10.0005"
                                    stroke="white"
                                    strokeWidth="2"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                            </svg>
                        }
                    />
                    {name}
                </MenuItem>
            ))}
        </Select>
    );
};
