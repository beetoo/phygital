import { SET_PREVIOUS_PAGE } from '../../actions';
import { GlobalAction, GlobalState } from '../../types';

export const globalInitialState: GlobalState = {
    previousPage: '',
};

export default function globalReducer(state = globalInitialState, action: GlobalAction): GlobalState {
    switch (action.type) {
        case SET_PREVIOUS_PAGE:
            return {
                ...state,
                previousPage: action.payload.prevPage,
            };

        default:
            return state;
    }
}
