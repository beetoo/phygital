import { SIGN_IN_FAIL, SIGN_IN_PROCESS, SIGN_IN_SUCCESS } from '../../@pages/SignIn/actions';
import {
    CLOSE_REGISTER_WELCOME_PAGE,
    CONFIRM_REGISTRATION,
    CONFIRM_REGISTRATION_FAIL,
    CONFIRM_REGISTRATION_SUCCESS,
    SIGN_UP_FAIL,
    SIGN_UP_PROCESS,
    SIGN_UP_SUCCESS,
} from '../../@pages/SignUp/actions';
import {
    AUTH_CHECK_FAIL,
    AUTH_CHECK_PROCESS,
    AUTH_CHECK_SUCCESS,
    CLEAR_AUTH_ERRORS,
    SIGN_OUT_PROCESS,
    SIGN_OUT_SUCCESS,
} from '../../actions';
import { AuthAction, AuthState } from '../../types';
import {
    CHANGE_PASSWORD,
    CHANGE_PASSWORD_FAIL,
    CHANGE_PASSWORD_SUCCESS,
    CHECK_TOKEN,
    CHECK_TOKEN_FAIL,
    CHECK_TOKEN_SUCCESS,
    CLEAR_RECOVERY_PASSWORD_STATE,
    SEND_RECOVERY_PASSWORD_EMAIL,
    SEND_RECOVERY_PASSWORD_EMAIL_FAIL,
    SEND_RECOVERY_PASSWORD_EMAIL_SUCCESS,
    SET_RECOVERY_PASSWORD_EMAIL,
} from '../../@pages/RecoveryPassword/actions';

export const authInitialState: AuthState = {
    authProcessing: false,
    authChecking: !(
        window.location.href.includes('confirm-registration') || window.location.href.includes('recovery-password')
    ),
    tokenChecking: false,
    isAuth: false,
    id: '',
    userId: '',
    role: '',
    email: '',
    name: '',
    surname: '',
    status: 'active',

    isRegistrationSuccess: false,
    isConfirmRegistrationSuccess: false,
    firstConfirmation: false,

    recoveryPasswordEmail: '',
    isSendEmailSuccess: false,
    isCheckTokenSuccess: false,
    isRecoveryPasswordSuccess: false,

    errors: null,
};

export default function authReducer(state = authInitialState, action: AuthAction): AuthState {
    switch (action.type) {
        case AUTH_CHECK_PROCESS:
            return {
                ...state,
                authChecking: true,
            };
        case AUTH_CHECK_SUCCESS:
            return {
                ...state,
                authChecking: false,
                isAuth: !!action.payload,
                ...action.payload,
            };
        case AUTH_CHECK_FAIL:
            return {
                ...state,
                authChecking: false,
                isAuth: false,
                role: '',
                id: '',
                email: '',
                name: '',
                surname: '',
            };

        case SIGN_UP_PROCESS:
            return {
                ...state,
                authProcessing: true,
                email: '',
                errors: null,
            };
        case SIGN_UP_SUCCESS:
            return {
                ...state,
                authProcessing: false,
                isAuth: true,
                ...action.payload,
            };
        case SIGN_UP_FAIL:
            return {
                ...state,
                authProcessing: false,
                errors: action.payload.errors,
            };

        case SIGN_IN_PROCESS:
            return {
                ...state,
                authProcessing: true,
                errors: null,
            };
        case SIGN_IN_SUCCESS:
            return {
                ...state,
                authProcessing: false,
                isAuth: true,
                ...action.payload,
            };
        case SIGN_IN_FAIL:
            return {
                ...state,
                authProcessing: false,
                isAuth: false,
                id: '',
                role: '',
                email: '',
                name: '',
                surname: '',
                errors: action.payload.errors,
            };

        case CONFIRM_REGISTRATION:
            return {
                ...state,
                authProcessing: true,
                isConfirmRegistrationSuccess: false,
                errors: null,
            };
        case CONFIRM_REGISTRATION_SUCCESS:
            return {
                ...state,
                isConfirmRegistrationSuccess: true,
                authProcessing: false,
                isAuth: action.payload.firstConfirmation || false,
                ...action.payload,
            };
        case CONFIRM_REGISTRATION_FAIL:
            return {
                ...state,
                isConfirmRegistrationSuccess: false,
                authProcessing: false,
                isAuth: false,
                id: '',
                role: '',
                email: '',
                name: '',
                surname: '',
                firstConfirmation: false,
                errors: action.payload.errors,
            };

        case CHANGE_PASSWORD:
            return {
                ...state,
                authProcessing: true,
                errors: null,
            };
        case CHANGE_PASSWORD_SUCCESS:
            return {
                ...state,
                authProcessing: false,
                isAuth: true,
                isRecoveryPasswordSuccess: true,
                errors: null,
                ...action.payload,
            };

        case CHANGE_PASSWORD_FAIL:
            return {
                ...state,
                authProcessing: false,
                isAuth: false,
                id: '',
                role: '',
                email: '',
                name: '',
                surname: '',
                errors: action.payload.errors,
            };

        case CHECK_TOKEN:
            return {
                ...state,
                tokenChecking: false,
                errors: null,
            };
        case CHECK_TOKEN_SUCCESS:
            return {
                ...state,
                tokenChecking: true,
                isCheckTokenSuccess: true,
                email: action.payload.email,
            };
        case CHECK_TOKEN_FAIL:
            return {
                ...state,
                tokenChecking: true,
                errors: action.payload.errors,
            };

        case SEND_RECOVERY_PASSWORD_EMAIL:
            return {
                ...state,
                authProcessing: true,
                recoveryPasswordEmail: action.payload.email,
                errors: null,
            };
        case SEND_RECOVERY_PASSWORD_EMAIL_SUCCESS:
            return {
                ...state,
                authProcessing: false,
                isSendEmailSuccess: true,
                errors: null,
            };
        case SEND_RECOVERY_PASSWORD_EMAIL_FAIL:
            return {
                ...state,
                authProcessing: false,
                errors: action.payload.errors,
            };

        case CLOSE_REGISTER_WELCOME_PAGE:
            return {
                ...state,
                isConfirmRegistrationSuccess: false,
            };

        case SET_RECOVERY_PASSWORD_EMAIL:
            return {
                ...state,
                recoveryPasswordEmail: action.payload.email,
            };

        case CLEAR_RECOVERY_PASSWORD_STATE:
            return {
                ...state,
                isSendEmailSuccess: false,
                isCheckTokenSuccess: false,
                isRecoveryPasswordSuccess: false,
                errors: null,
            };

        case SIGN_OUT_PROCESS:
            return {
                ...state,
                authProcessing: true,
                authChecking: false,
            };
        case SIGN_OUT_SUCCESS:
            return {
                ...state,
                authProcessing: false,
                authChecking: false,
                isAuth: false,
                role: '',
                id: '',
                email: '',
                name: '',
                surname: '',
            };

        case CLEAR_AUTH_ERRORS:
            return {
                ...state,
                errors: null,
            };
        default:
            return state;
    }
}
