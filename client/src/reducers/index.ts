import { Action, combineReducers, Reducer } from 'redux';

import { pagesReducers } from '../@pages';
import { StoreType } from '../types';
import { SIGN_OUT_SUCCESS } from '../actions';
import authReducer from './authReducer';
import onboardingReducer from './onboardingReducer';
import globalReducer from './globalReducer';

export const appReducer: Reducer<StoreType> = combineReducers({
    pages: combineReducers(pagesReducers),
    global: globalReducer,
    auth: authReducer,
    onboarding: onboardingReducer,
});

export const rootReducer = (state: StoreType, action: Action): StoreType => {
    if (action.type === SIGN_OUT_SUCCESS) return appReducer(undefined, action);

    return appReducer(state, action);
};
