import {
    ONBOARDING_DISABLED,
    ONBOARDING_ENABLED,
    SET_HELP_ICON_REF,
    SET_ONBOARDING_TOGGLE_REF,
    SET_PLAYLISTS_PAGE_LINK_REF,
    SET_SCREENS_PAGE_LINK_REF,
    SET_SKIP_ONBOARDING,
} from '../../actions';
import { OnBoardingAction, OnBoardingState } from '../../types';

export const onboardingInitialState: OnBoardingState = {
    isEnabled: localStorage.getItem('ONBOARDING_LOCAL_STORAGE_ITEM') === 'true',
    isOnBoardingSkipped: false,

    onBoardingToggleRef: null,
    helpIconRef: null,
    screensPageLinkRef: null,
    playlistsPageLinkRef: null,
};

export default function onboardingReducer(state = onboardingInitialState, action: OnBoardingAction): OnBoardingState {
    switch (action.type) {
        case ONBOARDING_ENABLED:
            return {
                ...state,
                isEnabled: true,
            };
        case ONBOARDING_DISABLED:
            return {
                ...state,
                isEnabled: false,
            };
        case SET_ONBOARDING_TOGGLE_REF:
            return {
                ...state,
                onBoardingToggleRef: action.payload.onBoardingToggleRef,
            };
        case SET_HELP_ICON_REF:
            return {
                ...state,
                helpIconRef: action.payload.helpIconRef,
            };
        case SET_SCREENS_PAGE_LINK_REF:
            return {
                ...state,
                screensPageLinkRef: action.payload.screensPageLinkRef,
            };
        case SET_PLAYLISTS_PAGE_LINK_REF:
            return {
                ...state,
                playlistsPageLinkRef: action.payload.playlistsPageLinkRef,
            };
        case SET_SKIP_ONBOARDING:
            return {
                ...state,
                isOnBoardingSkipped: action.payload.skip,
            };

        default:
            return state;
    }
}
