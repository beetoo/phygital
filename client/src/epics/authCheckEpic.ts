import { of, Observable } from 'rxjs';
import { ofType } from 'redux-observable';
import { mergeMap, map, catchError } from 'rxjs/operators';

import { AUTH_CHECK_PROCESS, setAuthCheckSuccess, setAuthCheckFail } from '../actions';
import { fromAuthCheck } from '../resolvers/authResolvers';
import { AuthCheckFailAction, AuthCheckProcessAction, AuthCheckSuccessAction } from '../types';
import { setAuthTokenToLocalStorage } from '../helpers/httpService/setAuthTokenToLocalStorage';

export const authCheckEpic = (
    action$: Observable<AuthCheckProcessAction>,
): Observable<AuthCheckSuccessAction | AuthCheckFailAction> =>
    action$.pipe(
        ofType(AUTH_CHECK_PROCESS),
        mergeMap(() =>
            fromAuthCheck().pipe(
                map((response) => {
                    setAuthTokenToLocalStorage(response);
                    return setAuthCheckSuccess(response);
                }),
                catchError((error) => of(setAuthCheckFail(error))),
            ),
        ),
    );
