import { combineEpics } from 'redux-observable';

import { pagesEpic } from '../@pages';
import { authCheckEpic } from './authCheckEpic';
import { onboardingEpic } from './onboardingEpic';
import { signOutEpic } from './signOutEpic';
import { modalsEpics } from '../components/ModalsContainer/epics';

export const rootEpic = combineEpics(authCheckEpic, pagesEpic, signOutEpic, onboardingEpic, modalsEpics);
