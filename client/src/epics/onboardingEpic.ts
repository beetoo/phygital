import { ofType } from 'redux-observable';
import { map, Observable } from 'rxjs';
import { DISABLE_ONBOARDING, ENABLE_ONBOARDING, setOnboardingDisabled, setOnboardingEnabled } from '../actions';
import { clearAllShowSteps } from '../components/Tour/utils/clearAllShowSteps';
import { DisableOnboardingAction, EnableOnboardingAction, SetOnBoardingAction } from '../types';

export const onboardingEpic = (
    action$: Observable<EnableOnboardingAction | DisableOnboardingAction>,
): Observable<SetOnBoardingAction> =>
    action$.pipe(
        ofType(ENABLE_ONBOARDING, DISABLE_ONBOARDING),
        map((action) => {
            if (action.type === ENABLE_ONBOARDING) {
                localStorage.setItem('ONBOARDING_LOCAL_STORAGE_ITEM', 'true');
                return setOnboardingEnabled();
            } else if (action.type === DISABLE_ONBOARDING) {
                localStorage.setItem('ONBOARDING_LOCAL_STORAGE_ITEM', 'false');
                clearAllShowSteps();
                return setOnboardingDisabled();
            } else {
                throw new Error('Unexpected onboarding action');
            }
        }),
    );
