import { Observable } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { ofType } from 'redux-observable';

import { SIGN_OUT_PROCESS, setSignOutSuccess } from '../actions';
import { fromSignOut } from '../resolvers/authResolvers';
import { SignOutProcessAction, SignOutSuccessAction } from '../types';

export const signOutEpic = (action$: Observable<SignOutProcessAction>): Observable<SignOutSuccessAction> =>
    action$.pipe(
        ofType(SIGN_OUT_PROCESS),
        switchMap(() =>
            fromSignOut().pipe(
                map(() => {
                    localStorage.removeItem('token');
                    return setSignOutSuccess();
                }),
            ),
        ),
    );
