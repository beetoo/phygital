import { from, Observable } from 'rxjs';

import { AuthResponse } from '../../types';
import { httpService } from '../../helpers/httpService';
import { SignUpClientDto } from '../../@pages/SignUp/hooks/useSignUp';

const signIn = async (email: string, password: string): Promise<AuthResponse> =>
    (await httpService.post('/auth/signin', { email, password })) as AuthResponse;

const signUp = async (signUpClientDto: SignUpClientDto): Promise<AuthResponse> =>
    (await httpService.post('/auth/registration', { ...signUpClientDto })) as AuthResponse;

// подтверждение регистрации
export const sendConfirmRegistrationEmail = async (userId: string): Promise<AuthResponse> =>
    (await httpService.post(`user/${userId}/email/verification/resend`, {})) as AuthResponse;

const confirmRegistration = async (confirmationCode: string): Promise<AuthResponse> =>
    (await httpService.get(`auth/confirm-registration/${confirmationCode}/check`)) as AuthResponse;

// восстановление пароля
const sendRecoveryPasswordEmail = async (email: string): Promise<AuthResponse> =>
    (await httpService.post('auth/recovery-password', { email })) as AuthResponse;

const checkToken = async (token: string): Promise<{ email: string }> =>
    (await httpService.get(`auth/recovery-password/${token}/check`)) as {
        email: string;
    };

const recoveryPassword = async (password: string, token: string): Promise<AuthResponse> =>
    (await httpService.post(`auth/recovery-password/${token}`, {
        password,
        timezoneOffset: -1 * new Date().getTimezoneOffset(),
    })) as AuthResponse;

const authCheck = async (): Promise<AuthResponse> => (await httpService.get('/auth/check')) as AuthResponse;

const signOut = async (): Promise<AuthResponse> => (await httpService.get('/auth/signout')) as AuthResponse;

export const fromSignIn = (email: string, password: string): Observable<AuthResponse> => from(signIn(email, password));

export const fromSignUp = (signUpClienDto: SignUpClientDto): Observable<AuthResponse> => from(signUp(signUpClienDto));

export const fromConfirmRegistration = (confirmationCode: string): Observable<AuthResponse> =>
    from(confirmRegistration(confirmationCode));

export const fromSendRecoveryPasswordEmail = (email: string): Observable<AuthResponse> =>
    from(sendRecoveryPasswordEmail(email));

export const fromCheckToken = (token: string): Observable<{ email: string }> => from(checkToken(token));

export const fromRecoveryPassword = (password: string, token: string): Observable<AuthResponse> =>
    from(recoveryPassword(password, token));

export const fromAuthCheck = (): Observable<AuthResponse> => from(authCheck());

export const fromSignOut = (): Observable<AuthResponse> => from(signOut());
