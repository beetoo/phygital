import { from, Observable } from 'rxjs';

import { httpService, HttpServiceResponse } from '../../helpers/httpService';
import { ContentType, FolderType } from '../../../../common/types';
import { UpdateFolderDto } from '../../../../server/src/models/folder/dto/update-folder.dto';
import { AddFolderPayload, UpdateContentItemPayload, UpdateContentItemsPayload } from '../../../../common/types/client';

const getFolders = async (organizationId: string): Promise<FolderType[]> =>
    (await httpService.get(`organization/${organizationId}/folders`)) as FolderType[];

const addFolder = async ({ organizationId, name }: AddFolderPayload): Promise<{ id: string }> =>
    (await httpService.post(`organization/${organizationId}/folders`, { name })) as { id: string };

const updateFolder = async ({ id: folderId, ...rest }: UpdateFolderDto): Promise<HttpServiceResponse> =>
    (await httpService.patch(`folder/${folderId}`, { ...rest })) as HttpServiceResponse;

const deleteFolder = async (folderId: string | string[]): Promise<HttpServiceResponse> =>
    (await httpService.delete(`folder`, { params: { folderId } })) as HttpServiceResponse;

const getContentItems = async (organizationId: string) =>
    (await httpService.get(`organization/${organizationId}/content`)) as ContentType[];

const uploadContentItem = async (
    organizationId: string,
    folderId: string,
    files: File[],
    uploadedItemsInfo: Pick<ContentType, 'originFilename' | 'dimensions' | 'duration'>[],
): Promise<void> => {
    if (files.length === 0) return;

    const formData = new FormData();

    formData.append('organizationId', organizationId);
    formData.append('folderId', folderId);

    for (const file of files) {
        formData.append('files', file);
    }

    await httpService.post(`content`, formData, {
        headers: { 'Content-Type': 'multipart/form-data' },
        params: { uploadedItemsInfo },
    });
};

const updateContentItem = async (
    contentId: string,
    updateContentDto: UpdateContentItemPayload,
): Promise<HttpServiceResponse> =>
    (await httpService.patch(`content/${contentId}`, { ...updateContentDto })) as HttpServiceResponse;

const updateContentItems = async (
    contentIds: string[],
    updateContentDto: UpdateContentItemsPayload,
): Promise<HttpServiceResponse> =>
    (await httpService.patch(`content/update`, { contentIds, ...updateContentDto })) as HttpServiceResponse;

const deleteContentItem = async (contentIds: string[]): Promise<HttpServiceResponse> =>
    await httpService.post(`content/delete`, { contentIds });

export const fromGetFolders = (organizationId: string): Observable<HttpServiceResponse> =>
    from(getFolders(organizationId));

export const fromAddFolder = (createFolderDto: AddFolderPayload): Observable<{ id: string }> =>
    from(addFolder(createFolderDto));

export const fromUpdateFolder = (updateFolderDto: UpdateFolderDto): Observable<HttpServiceResponse> =>
    from(updateFolder(updateFolderDto));

export const fromDeleteFolder = (folderId: string | string[]): Observable<HttpServiceResponse> =>
    from(deleteFolder(folderId));

export const fromGetContentItems = (organizationId: string): Observable<ContentType[]> =>
    from(getContentItems(organizationId));

export const fromUploadContentItem = (
    organizationId: string,
    folderId: string,
    files: File[],
    uploadedItemsInfo: Pick<ContentType, 'originFilename' | 'dimensions' | 'duration'>[],
): Observable<void> => from(uploadContentItem(organizationId, folderId, files, uploadedItemsInfo));

export const fromUpdateContentItem = (
    contentId: string,
    updateContentDto: UpdateContentItemPayload,
): Observable<HttpServiceResponse> => from(updateContentItem(contentId, updateContentDto));

export const fromUpdateContentItems = (
    contentIds: string[],
    updateContentDto: UpdateContentItemsPayload,
): Observable<HttpServiceResponse> => from(updateContentItems(contentIds, updateContentDto));

export const fromDeleteContentItem = (contentIds: string[]): Observable<HttpServiceResponse> =>
    from(deleteContentItem(contentIds));
