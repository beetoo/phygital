import { from, Observable } from 'rxjs';

import { httpService, HttpServiceResponse } from '../../helpers/httpService';
import { ClientOrganizationType, TakeSkipAndSearchType } from '../../../../common/types';
import { CreateEmailDto } from '../../../../server/src/models/email/dto/create-email.dto';
import { UpdateUserAndOrganizationDto } from '../../../../server/src/models/organization/dto/update-user-and-organization.dto';
import { GetOrganizationsResponseType } from '../../@pages/AdminPage/types';

export const sendEmailMessageViaEmailForm = async (createEmailDto: CreateEmailDto): Promise<HttpServiceResponse> =>
    await httpService.post('/email/message', { ...createEmailDto });

export const sendBalanceRequestViaEmailForm = async (createEmailDto: CreateEmailDto): Promise<HttpServiceResponse> =>
    await httpService.post('/email/balance', { ...createEmailDto });

export const sendTvBoxRequestViaEmailForm = async (createEmailDto: CreateEmailDto): Promise<HttpServiceResponse> =>
    await httpService.post('/email/tv-box', { ...createEmailDto });

export const sendStorageAmountRequest = async (createEmailDto: CreateEmailDto): Promise<HttpServiceResponse> =>
    await httpService.post('/email/increase-storage-capacity', { ...createEmailDto });

const getOrganizations = async ({ skip, take, search }: TakeSkipAndSearchType): Promise<GetOrganizationsResponseType> =>
    (await httpService.get('/admin/organizations', { params: { skip, take, search } })) as GetOrganizationsResponseType;

const getOrganization = async (id: string): Promise<ClientOrganizationType> =>
    (await httpService.get(`organization/old/${id}`)) as ClientOrganizationType;

export const updateOrganization = async ({ id, ...rest }: UpdateUserAndOrganizationDto): Promise<HttpServiceResponse> =>
    await httpService.put(`organization/${id}`, { ...rest });

export const deleteOrganization = async (id: string): Promise<HttpServiceResponse> =>
    await httpService.delete(`organization/${id}`);

export const fromGetOrganizations = (params: TakeSkipAndSearchType): Observable<GetOrganizationsResponseType> =>
    from(getOrganizations(params));

export const fromGetOrganization = (id: string): Observable<ClientOrganizationType> => from(getOrganization(id));

export const fromUpdateOrganization = (
    updateOrganizationDto: UpdateUserAndOrganizationDto,
): Observable<HttpServiceResponse> => from(updateOrganization(updateOrganizationDto));

export const fromDeleteOrganization = (id: string): Observable<HttpServiceResponse> => from(deleteOrganization(id));
