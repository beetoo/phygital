import { from, Observable } from 'rxjs';
import axios, { AxiosResponse } from 'axios';

import { CreateLocationDto } from '../../../../server/src/models/location/dto/create-location.dto';
import { UpdateLocationDto } from '../../../../server/src/models/location/dto/update-location.dto';
import { CreateScreenDto } from '../../../../server/src/models/screen/dto/create-screen.dto';
import { UpdateScreenDto } from '../../../../server/src/models/screen/dto/update-screen.dto';
import { LocationType, ScreenType, DeleteScreenTagDto } from '../../../../common/types';

import { httpService, HttpServiceResponse } from '../../helpers/httpService';
import { ScreenLogsClientType } from '../../@pages/AdminPage/types';
import { UpdateScreensDto } from '../../../../server/src/models/screen/dto/update-screens.dto';

export const getScreenLogs = async (): Promise<ScreenLogsClientType[]> =>
    (await httpService.get(`logger`)) as ScreenLogsClientType[];

const getLocations = async (organizationId: string): Promise<LocationType[]> =>
    (await httpService.get(`organization/${organizationId}/locations`)) as LocationType[];

export const updateLocation = async ({ locationId, ...restDto }: UpdateLocationDto): Promise<HttpServiceResponse> =>
    await httpService.patch(`location/${locationId}`, { ...restDto });

export const deleteLocation = async (location: LocationType): Promise<HttpServiceResponse> =>
    await httpService.delete(`location/${location.id}`);

export const cancelDeleteLocation = async (cancelId: string): Promise<HttpServiceResponse> =>
    await httpService.post('/location/cancel_delete', { cancelId });

export const addLocation = async (createLocationDto: CreateLocationDto): Promise<HttpServiceResponse> =>
    await httpService.post(`organization/${createLocationDto.organizationId}/locations`, { ...createLocationDto });

const getScreens = async (organizationId: string, locationId: string, isAdmin: boolean): Promise<ScreenType[]> =>
    (await httpService.get(`organization/${organizationId}/screens`, {
        params: { locationId, isAdmin },
    })) as ScreenType[];

const updateScreen = async (screenId: string, updateScreenDto: UpdateScreenDto): Promise<HttpServiceResponse> =>
    await httpService.patch(`screen/${screenId}`, { ...updateScreenDto });

const updateScreens = async (updateScreenDto: UpdateScreenDto): Promise<HttpServiceResponse> =>
    await httpService.patch(`screen/update/screens`, { ...updateScreenDto });

const addScreen = async (createScreenDto: CreateScreenDto): Promise<HttpServiceResponse> =>
    (await httpService.post('screen', { ...createScreenDto })) as Promise<{ id: string }>;

export const deleteScreens = async (screens: ScreenType[]): Promise<HttpServiceResponse> =>
    await httpService.patch(`screen/delete/screens`, { screenIds: screens.map(({ id }) => id) });

export const cancelDeleteScreen = async (cancelId: string): Promise<HttpServiceResponse> =>
    await httpService.post('screen/cancel_delete', { cancelId });

const deleteScreenTags = async (deleteScreenTagDto: DeleteScreenTagDto): Promise<HttpServiceResponse> =>
    await httpService.patch(`screen/delete/tags`, { deleteScreenTagDto });

export const fromGetLocations = (organizationId: string): Observable<LocationType[]> =>
    from(getLocations(organizationId));

export const fromUpdateLocation = (updateLocationDto: UpdateLocationDto): Observable<HttpServiceResponse> =>
    from(updateLocation(updateLocationDto));

export const fromAddLocation = (createLocationDto: CreateLocationDto): Observable<HttpServiceResponse> =>
    from(addLocation(createLocationDto));

export const fromDeleteLocation = (location: LocationType): Observable<HttpServiceResponse> =>
    from(deleteLocation(location));

export const fromGetScreens = (
    organizationId: string,
    locationId: string,
    isAdmin: boolean,
): Observable<ScreenType[]> => from(getScreens(organizationId, locationId, isAdmin));

export const fromUpdateScreen = (screenId: string, updateScreenDto: UpdateScreenDto): Observable<HttpServiceResponse> =>
    from(updateScreen(screenId, updateScreenDto));

export const fromUpdateScreens = (updateScreenDto: UpdateScreensDto): Observable<HttpServiceResponse> =>
    from(updateScreens(updateScreenDto));

export const fromAddScreens = (createScreenDto: CreateScreenDto): Observable<HttpServiceResponse> =>
    from(addScreen(createScreenDto));

export const fromDeleteScreens = (screens: ScreenType[]): Observable<HttpServiceResponse> =>
    from(deleteScreens(screens));

export const fromDeleteScreenTags = (deleteScreenTagDto: DeleteScreenTagDto): Observable<HttpServiceResponse> =>
    from(deleteScreenTags(deleteScreenTagDto));

// location suggest list

export type DataSuggestion = {
    value: string;
    unrestricted_value: string;
    data: {
        street_type_full: string | null;
        city_type_full: string | null;
        settlement_type_full: string | null;
        postal_code: string;
        geo_lat: string;
        geo_lon: string;
    };
};

const getDaDataLocationSuggestList = async (query: string, queryAddition = '') =>
    (await axios.post(
        'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
        JSON.stringify({
            query: queryAddition ? queryAddition + ' ' + query : query,
        }),
        {
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                Authorization: 'Token ' + 'ccbed8ae3d36f53e867862b23da41fd8d177ef51',
            },
        },
    )) as AxiosResponse<{ suggestions: DataSuggestion[] }>;

export const fromGetDaDataLocationSuggestList = ({
    query,
    queryAddition,
}: {
    query: string;
    queryAddition: string;
}): Observable<AxiosResponse<{ suggestions: DataSuggestion[] }>> =>
    from(getDaDataLocationSuggestList(query, queryAddition));

const getOSMLocationSuggestList = async (query: string, queryAddition = '') => {
    return (await axios.get(
        `https://nominatim.openstreetmap.org/search?${queryAddition ? 'q' : 'city'}=${
            queryAddition ? query + ', ' + queryAddition : query
        }&format=json&accept-language=en`,
    )) as AxiosResponse<{
        suggestions: DataSuggestion[];
    }>;
};

export const fromGetOSMLocationSuggestList = ({
    query,
    queryAddition,
}: {
    query: string;
    queryAddition: string;
}): Observable<AxiosResponse<unknown>> => from(getOSMLocationSuggestList(query, queryAddition));
