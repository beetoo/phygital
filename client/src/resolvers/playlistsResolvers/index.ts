import { from, Observable } from 'rxjs';

import { httpService, HttpServiceResponse } from '../../helpers/httpService';
import {
    UpdatePlaylistTitleDto,
    UpdatePlaylistItemNameDto,
    CreatePlaylistDtoClient,
    UpdatePlaylistDtoClient,
} from '../../@pages/PlaylistsPage/types';
import { AddContentToPlaylistDto } from '../../@pages/ContentPage/type';
import { PlaylistItemType, PlaylistType } from '../../../../common/types';

export const getPlaylists = async (organizationId: string): Promise<PlaylistType[]> =>
    (await httpService.get(`organization/${organizationId}/playlists`)) as PlaylistType[];

const addPlaylist = async (organizationId: string, playlistDto: CreatePlaylistDtoClient) =>
    (await httpService.post(`playlist/${organizationId}`, playlistDto)) as HttpServiceResponse;

const updatePlaylist = async (playlistId: string, playlistDto: UpdatePlaylistDtoClient) =>
    (await httpService.put(`playlist/${playlistId}`, playlistDto)) as HttpServiceResponse;

const addContentToPlaylists = async ({ playlistIds, contentIds }: AddContentToPlaylistDto) =>
    (await httpService.put(`playlist/content/add`, { playlistIds, contentIds })) as HttpServiceResponse;

const updatePlaylistTitle = async ({ playlistId, ...rest }: UpdatePlaylistTitleDto) =>
    (await httpService.patch(`playlist/${playlistId}`, { ...rest })) as PlaylistType[];

const deletePlaylists = async (playlistIds: string | string[]) =>
    await httpService.patch(`playlist/delete/playlists`, { playlistIds });

const getPlaylistItems = async (playlistId: string) =>
    (await httpService.get(`playlist-item/${playlistId}`)) as PlaylistItemType[];

const updatePlaylistItemName = async ({ playlistItemId, name }: UpdatePlaylistItemNameDto) =>
    await httpService.patch(`playlist-item/${playlistItemId}/name`, { name });

// const updatePlaylistItemDelay = async ({ playlistItemId, delay }: UpdatePlaylistItemDelayDto) =>
//     await service.patch(`playlist-item/${playlistItemId}/delay`, { delay });

const deletePlaylistItems = async (playlistItemIds: string | string[]) =>
    await httpService.patch(`playlist-item/delete/playlist-items`, { playlistItemIds });

export const fromGetPlaylists = (organizationId: string): Observable<PlaylistType[]> =>
    from(getPlaylists(organizationId));

export const fromAddPlaylist = (
    organizationId: string,
    playlistDto: CreatePlaylistDtoClient,
): Observable<HttpServiceResponse> => from(addPlaylist(organizationId, playlistDto));

export const fromUpdatePlaylist = (
    playlistId: string,
    playlistDto: UpdatePlaylistDtoClient,
): Observable<HttpServiceResponse> => from(updatePlaylist(playlistId, playlistDto));

export const fromAddContentToPlaylists = ({
    playlistIds,
    contentIds,
}: AddContentToPlaylistDto): Observable<HttpServiceResponse> =>
    from(addContentToPlaylists({ playlistIds, contentIds }));

export const fromUpdatePlaylistTitle = ({
    playlistId,
    ...rest
}: UpdatePlaylistTitleDto): Observable<HttpServiceResponse> => from(updatePlaylistTitle({ playlistId, ...rest }));

export const fromDeletePlaylists = (playlistIds: string | string[]): Observable<HttpServiceResponse> =>
    from(deletePlaylists(playlistIds));

export const fromGetPlaylistItems = (playlistId: string): Observable<PlaylistItemType[]> =>
    from(getPlaylistItems(playlistId));

export const fromUpdatePlaylistItemName = ({
    playlistItemId,
    name,
}: UpdatePlaylistItemNameDto): Observable<HttpServiceResponse> =>
    from(updatePlaylistItemName({ playlistItemId, name }));

// export const fromUpdatePlaylistItemDelay = ({
//     playlistItemId,
//     delay,
// }: UpdatePlaylistItemDelayDto): Observable<ServiceResponse> => from(updatePlaylistItemDelay({ playlistItemId, delay }));

export const fromDeletePlaylistItems = (playlistItemIds: string | string[]): Observable<HttpServiceResponse> =>
    from(deletePlaylistItems(playlistItemIds));
