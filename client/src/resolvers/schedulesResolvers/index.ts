import { from, Observable } from 'rxjs';

import { httpService, HttpServiceResponse } from '../../helpers/httpService';
import { PlaylistScheduleType } from '../../../../common/types';

const getSchedules = async (organizationId: string): Promise<PlaylistScheduleType[]> =>
    (await httpService.get(`playlist-schedule/${organizationId}`)) as PlaylistScheduleType[];

export const fromGetSchedules = (organizationId: string): Observable<HttpServiceResponse> =>
    from(getSchedules(organizationId));
