import { AuthResponse, ErrorResponse } from '../types';
import { ActivePage } from '../components/OrganizationContainer/types';

export const AUTH_CHECK_PROCESS = 'AUTH_CHECK_PROCESS';
export const AUTH_CHECK_SUCCESS = 'AUTH_CHECK_SUCCESS';
export const AUTH_CHECK_FAIL = 'AUTH_CHECK_FAIL';

export const SIGN_OUT_PROCESS = 'SIGN_OUT_PROCESS';
export const SIGN_OUT_SUCCESS = 'SIGN_OUT_SUCCESS';

export const SET_PREVIOUS_PAGE = 'SET_PREVIOUS_PAGE';

export const CLEAR_AUTH_ERRORS = 'CLEAR_AUTH_ERRORS';

// Обучение

export const ENABLE_ONBOARDING = 'ENABLE_ONBOARDING';
export const DISABLE_ONBOARDING = 'DISABLE_ONBOARDING';

export const ONBOARDING_ENABLED = 'ONBOARDING_ENABLED';
export const ONBOARDING_DISABLED = 'ONBOARDING_DISABLED';

export const SET_SKIP_ONBOARDING = 'SET_SKIP_ONBOARDING';

export const SET_ONBOARDING_TOGGLE_REF = 'SET_ONBOARDING_TOGGLE_REF';
export const SET_HELP_ICON_REF = 'SET_HELP_ICON_REF';
export const SET_SCREENS_PAGE_LINK_REF = 'SET_SCREENS_PAGE_LINK_REF';
export const SET_PLAYLISTS_PAGE_LINK_REF = 'SET_PLAYLISTS_PAGE_LINK_REF';

export const setAuthCheckProcess = () => ({ type: AUTH_CHECK_PROCESS }) as const;
export const setAuthCheckSuccess = (payload: AuthResponse) => ({ type: AUTH_CHECK_SUCCESS, payload }) as const;
export const setAuthCheckFail = (errors: ErrorResponse) => ({ type: AUTH_CHECK_FAIL, payload: { errors } }) as const;

export const setSignOutProcess = () => ({ type: SIGN_OUT_PROCESS }) as const;
export const setSignOutSuccess = () => ({ type: SIGN_OUT_SUCCESS }) as const;

export const setPreviousPage = (prevPage: ActivePage = '') =>
    ({ type: SET_PREVIOUS_PAGE, payload: { prevPage } }) as const;

export const setClearAuthErrors = () => ({ type: CLEAR_AUTH_ERRORS }) as const;

// Обучение

export const setEnableOnboarding = () => ({ type: ENABLE_ONBOARDING }) as const;
export const setDisableOnboarding = () => ({ type: DISABLE_ONBOARDING }) as const;

export const setOnboardingEnabled = () => ({ type: ONBOARDING_ENABLED }) as const;
export const setOnboardingDisabled = () => ({ type: ONBOARDING_DISABLED }) as const;

export const setSkipOnBoarding = (skip: boolean) => ({ type: SET_SKIP_ONBOARDING, payload: { skip } }) as const;

export const setOnBoardingToggleRef = (onBoardingToggleRef: HTMLElement | null) =>
    ({ type: SET_ONBOARDING_TOGGLE_REF, payload: { onBoardingToggleRef } }) as const;
export const setHelpIconRef = (helpIconRef: SVGSVGElement | null) =>
    ({ type: SET_HELP_ICON_REF, payload: { helpIconRef } }) as const;
export const setScreensPageLinkRef = (screensPageLinkRef: HTMLElement | null) =>
    ({ type: SET_SCREENS_PAGE_LINK_REF, payload: { screensPageLinkRef } }) as const;
export const setPlaylistsPageLinkRef = (playlistsPageLinkRef: HTMLElement | null) =>
    ({ type: SET_PLAYLISTS_PAGE_LINK_REF, payload: { playlistsPageLinkRef } }) as const;
